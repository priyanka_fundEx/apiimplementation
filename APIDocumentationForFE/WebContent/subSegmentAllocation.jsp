<%@page import="com.fundexpert.dao.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.fundexpert.dao.Stocks"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<style>
		.center {
		           display: block;
		           margin-top:20px;
		           width: 100%;
		           text-align: center;
                }
                .month-box {
					  color:black;
					  font-weight:bold;               
					  height:50px;
					  line-height:40px;
					  z-index:200;
					  background-color:orange;
					  width:300px;
					  
					  }
		</style>
		
</head>
<body class="container-fluid" >
 <div style="position: fixed; top:0; left:0; width: 100%; background-color:black; display: block;   color: white;
   text-align: center;"> welcome</div>
 <h1 align="center" style="margin-top: 20px">Something</h1>
   
   		 <div class="col-sm-6 col-xs-12" >
   		  <h3 align="center" style="margin-top: 10px">Add</h3>
   		  <div class="row">
	   		 	<div class="col-sm-12 col-xs-12" >
		   		 	  <div class="col-sm-3" id="riskDivId"> 
		   		 	  <div class="form-group">
		   		 	  	<label class="control-label">Risk Profile</label>
				    	<input type="text" class="form-control"    id="riskProfile" onkeypress="return isNumber(event)">
			            
		   		 	  </div>
		   		 	 </div>
		   		 	 <div class="col-sm-3" id="allocationDivId"> 
		   		 	  <div class="form-group">
		   		 	  	<label class="control-label">Allocation</label>
				    	<input type="text" class="form-control"   id="allocationId" maxlength=2 onkeypress="return isNumber(event)">
			            
		   		 	  </div>
		   		 	 </div>
		   		 	 <div class="col-sm-3" id="subSegmentDivId"> 
		   		 	  <div class="form-group">
		   		 	  	<label class="control-label">subSegment</label>
				    	 <select  class="form-control" id="subSegmentId"  >
	            		 <option disabled="" value="" selected="" > SELECT </option>
	            			<option value="1" >LARGE CAP</option>
					    	<option value="2" >MID CAP</option>
					    	<option value="3" >SMALL CAP</option>
					    	<option value="4" >LONG TERM</option>
					    	<option value="5" >SHORT TERM</option>
											
											
						 </select>
											
			            
		   		 	  </div>
		   		 	 </div>
		   		 	
			 </div>
   		 </div>
   		  <table class="table table-hover table-condensed" id="selectedDataTable">
   		  
                        <tbody  id="selectedDataTableBody"></tbody>
          </table> 
                 <label id=labelId style="color:red;"></label><br>
   		   <input type="submit" id="submit" class="btn btn-primary btn-sm" style="margin-left: 10px"  value="submit" onclick='myFunctionSubmit()'><br>
   		 </div>
   		 <div class="col-sm-6 col-xs-12" >
	   		  <h3 align="center" style="margin-top: 10px">View</h3>
	   		  <div class="row">
		   		 	<div class="col-sm-12 col-xs-12" >
			   		 	<div class="col-sm-3" id="viewdiv"> 
				   		 	  <div class="form-group">
				   		 	  	<label class="control-label">Choose RiskProfile</label>
	    						<select  class="form-control" id="viewRiskProfileId"  >
						    		    <option disabled="" value="" selected="" > SELECT </option>
							    		<%
							    		Session hSession=null;
							    		User user=null;
							    		try
							    		{
							    		hSession= HibernateBridge.getSessionFactory().openSession();
										long consumerId=0;
							    		user=(User)request.getSession().getAttribute("user");
										
										String loggedIn=(String)request.getSession().getAttribute("login");
										
										if(loggedIn==null || loggedIn.equals("false") || user==null)
										{
											consumerId=2;
									   /* throw new FundexpertException("You are not logged in.");*/
										}
										else {
											consumerId=user.getConsumerId();
										}
				                			Query query2= hSession.createQuery("select distinct riskProfile from SubSegmentAllocation   where consumerId=?").setLong(0, consumerId);  
							    		    List<Long> list2=query2.list();  
											for (int i = 0; i < list2.size(); i++) 
											{
								       			System.out.println(list2.get(i)); 
								       			%>
							         	    	   <option value="<%=list2.get(i)%>"><%=list2.get(i)%></option> 
							         	    	<%
											}
												%>
							  </select>
							         	  	    
							         	  	    <% 
											
							         	  	    }
											catch(Exception e){
												e.printStackTrace();
											}finally{
												hSession.close();	
											}
												%>
											
			  			  
			   					
					            
				   		 	  </div>
			   		 	 </div>
			   		 	 <div class="col-sm-4" id="submitDivId">	 
			   		 		 	<div class="form-group"> 
			   		 		 		<input type="button" id="viewId" class="btn btn-primary btn-sm" style="margin-top:25px"  value="view" onclick='myFunctionView()'><br>
			   		 		 	</div>
   		 				</div>
   		 		 		
		   		 	
		   	  		</div>
		   	  		
		   	  </div>
		   	  <table class="table table-hover table-condensed" id="viewTable" width="100%" border="0" cellpadding="0" cellspacing="0" ">
	   		 		 		
	   		 		 		 <thead id="viewTableHead" style="display:hidden"></thead> 
	   		 		 		
			         	    <tbody id="viewTableBody" style= "display:hidden">	   		
			                </tbody>
		     </table>
		  </div>
   		 </body>
</html>
<script>
var subSegmentAllocationData=[];
var subSegmentArray=[];
var i=0;
var tAllocation=0;
$(document).ready(function()
		
{
	 $("#subSegmentId").on("change",function()
			{
				var riskProfile=$("#riskProfile").val();
				console.log("riskProfile",riskProfile);
				var allocationId=$("#allocationId").val();
				console.log("allocationId",allocationId);
				if(typeof riskProfile=="undefined"||riskProfile==""||riskProfile==null)
				{
					alert("enter riskprofile");
					$("#subSegmentId").val("");
					
				}
				else if(typeof allocationId=="undefined"||allocationId==""||allocationId==null){
					alert("enter allocation");
					$("#subSegmentId").val("");
				}
				else{
					var  optionText = $('option:selected', this).text();
					var idxb = $.inArray(optionText,subSegmentArray);
						 
					console.log("idxb",idxb);
					if(idxb==-1)
					{
						/* <td><button onclick='deletefunction(this)' class='btn btn-primary btn-sm'  id="+i +">delete</button></td> */
						$("#selectedDataTableBody").append("<tr><td>"+riskProfile+"</td><td>"+allocationId+"</td><td>"+optionText+"</td><td><button onclick='deletefunction(this)' class='btn btn-primary btn-sm'  id="+i +">delete</button></td> </tr>");
						$("#riskProfile").attr('readonly', true);
						 i++;	
						var newobj={
								riskProfile:riskProfile,
						        allocation:allocationId,
						        subSegment:optionText,
						        
						
						};
						subSegmentAllocationData.push(newobj);
						subSegmentArray.push(optionText);
					}
				}
				
					
			});
			 
		});
function myFunctionSubmit()
{
	var totalSum=0;
	var riskProfile=$("#riskProfile").val();
	console.log("riskProfile",riskProfile);
	var allocationId=$("#allocationId").val();
	var subSegmentId=$("#subSegmentId").val();
	if(subSegmentAllocationData.length>0)
	{
		for(var i=0;i<subSegmentAllocationData.length;i++)
		{
			totalSum=(subSegmentAllocationData[i].allocation*1)+(totalSum*1);
			console.log("subSegmentAllocationData",subSegmentAllocationData);
		}
	}
	console.log("total sum",totalSum);
    if(totalSum>100){
		alert("total allocation can't be more than 100");
		window.location.reload();
	}else if(totalSum<100){
		alert("total allocation can't be less than 100");	
		window.location.reload();
	}
	
	else{
		$.ajax({
    	    url:"FundsServlet?method=submitSubSegmentAllocation",
    	    data:{subSegmentAllocationData:subSegmentAllocationData},
			method: "POST",
			dataType:"json",
			
			success: function (data)
			{
				if(data.success==false)
					{
						alert("exception in servlet");
						alert(data.error);
						
					}
				if(data.success==true){
					alert("success");
					window.location.reload();
				}
		       			
			}		
	
     });
	}
	
}
function deletefunction(element)
{
	 $elem = $(element);
	 $elem.parent().parent().remove();
	 var deleteId=$elem.attr('id');
	 console.log("deleteId",deleteId);
	 
	/*  subSegmentAllocationData.splice(deleteId, 1);
	 
     console.log("after deleting array is: ",subSegmentAllocationData);
     subSegmentArray.splice(deleteId, 1);
     console.log("after deleting array is: ",subSegmentArray); */
     subSegmentArray.splice(deleteId, 1,null);
	 console.log("subSegmentArray",subSegmentArray);
	 var newobj={
				riskProfile:null,
		        allocation:null,
		        subSegment:null,
		        
		
		};
	 console.log("before deleting array",subSegmentAllocationData);
	 subSegmentAllocationData.splice(deleteId, 1,newobj);
	 
     console.log("after deleting array is: ",subSegmentAllocationData);
     
    	 
   
	 
} 
function myFunctionView()
{
	$("#viewTableBody").html(" ");
	$("#viewTableBody").empty();
	
	var viewRiskProfileId=$("#viewRiskProfileId").val();
	console.log("riskprofile is",viewRiskProfileId);
	if(typeof viewRiskProfileId!="undefined"||viewRiskProfileId!=null||viewRiskProfileId!="")
		{
				$.ajax({
				    url:"FundsServlet?method=viewSubSegmentAllocation",
				    data:{riskProfile:viewRiskProfileId},
					method: "POST",
					dataType:"json",
					
					success: function (data)
					{
						if(data.success==false)
							{
								alert("exception in servlet");
								alert(data.error);
								
							}
						if(data.success==true){
							alert("success");
							console.log("data is ",data.viewSAlloaction);
							for(i=0;i<data.viewSAlloaction.length;i++)
							{
								if(i==0)
								{
									$("#viewTableHead").append("<tr><td>"+"Allocation"+"</td><td>"+"Sector"+"</td><tr>")
								}
							console.log("data.viewSAlloaction[i].allocation",data.viewSAlloaction[i].allocation);
							console.log("data.viewSAlloaction[i].subSegment",data.viewSAlloaction[i].subSegment);
							
							$("#viewTableBody").append("<tr><td>"+data.viewSAlloaction[i].allocation+"</td><td>"+data.viewSAlloaction[i].subSegment+"</td></tr>");
							 document.getElementById("viewTableBody").style.display='block'; 
							 document.getElementById("viewTableHead").style.display='block'; 
							 
							}
						}
				       			
					}		
			
			 });
		}
	else{
		alert("choose riskprofile first");
	}
}
function isNumber(evt) {
	console.log("isNum");
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>