<%@page import="com.fundexpert.config.Config"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.OutputStreamWriter"%>
<%@page import="java.net.URLConnection"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URL"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edelweiss GumptionLabs Reader Test Framework</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

<div class="container">
		<div class="col-sm-12 col-xs-12 text-center">
			<h3>Edelweiss GumptionLabs Reader Test Framework</h3>
		</div>
		<div class="col-sm-offset-4 col-sm-4 col-xs-12">
		<form name="portfolio" action="PortfolioRequestSubmit.jsp" class=""  method="post">
			<div class="form-group">
				<label class="control-label">Password</label>
				<input type="text" name="pass" class="form-control" />
			</div>
			<div class="form-group">
				<label class="control-label">Email</label>
				<input type="text" name="emailId" class="form-control" />
			</div>
			<input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
		</div>
		</form>
</div>

<%

	JSONObject respAppId=null;
	JSONObject resp=null;
	if(request.getParameterMap().containsKey("submit") && request.getParameter("submit").equals("Submit"))
	{
		Config configuration=new Config();
		String email=request.getParameter("emailId");
		String pan=request.getParameter("pass");
		System.out.println("Email id="+email+" Pan="+pan);
		
		//Make a request to http://localhost:8081/APIDocumentationForFE/rest/consumer/login
		String urlPath;
		if(configuration.getProperty(Config.ENVIRONMENT).equals(Config.DEVELOPMENT))
		{
			urlPath="http://localhost:8081/APIDocumentationForFE/rest/consumer/login";
		}
		else
		{
			urlPath="http://api.fundexpert.in/rest/consumer/login";
		}
		String appId="e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc";
		String secretKey="ef7p8lcgnl2nnffp2iln"; 
		JSONObject json=new JSONObject();
		json.put("appId", appId);
		json.put("secretKey",secretKey);
		
		URL url = new URL(urlPath);
		URLConnection con=url.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setConnectTimeout(500000);
		con.setReadTimeout(500000);
		System.out.println("After opening connection to Login request on API server.");
		
		OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
		writer.write(json.toString());
		writer.close(); 
		System.out.println("Sending Login data as : "+json.toString(1));
		
		BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
		String line=br.readLine();
	
		StringBuilder builder=new StringBuilder();
		while(line!=null)
		{
			builder.append(line);
			line=br.readLine();
		}
		br.close();
		json=new JSONObject(builder.toString());
		System.out.print("Received JSON size="+json.toString(1));
		respAppId=json;
		%>
			<%-- <%=json.toString(1)%> --%>
		<%
		if(json.getBoolean("success"))
		{
			String sessionId=json.getString("sessionId");
			json=new JSONObject();
			//Make a request to http://localhost:8081/APIDocumentationForFE/rest/request/MutualfundPortfolio
			if(configuration.getProperty(Config.ENVIRONMENT).equals(Config.DEVELOPMENT))
			{
				System.out.println("Making Request to Portfolio in Development Environment.");
				urlPath="http://localhost:8081/APIDocumentationForFE/rest/request/RequestMutualfundPortfolio";
			}
			else
			{
				System.out.println("Making Request to Portfolio in Production Environment.");
				urlPath="http://api.fundexpert.in/rest/request/RequestMutualfundPortfolio";
			}
			json.put("sessionId", sessionId);
			json.put("emailId",email);
			json.put("pan",pan);
			json.put("appId", appId);

			url = new URL(urlPath);
			con=url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(500000);
			con.setReadTimeout(500000);
			System.out.println("After opening connection to Make a request to API server.");
			
			writer=new OutputStreamWriter(con.getOutputStream());
			writer.write(json.toString());
			writer.close(); 
			System.out.println("Sending Portfolio Request Data as : "+json.toString(1));
			
			br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			line=br.readLine();
		
			builder=new StringBuilder();
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			br.close();
			json=new JSONObject(builder.toString());
			resp=json;
			System.out.println("Received JSON on making a request : "+json.toString(1));
			%>
			<%-- <%=json.toString(1) %> --%>
			<%
		}
		
	}
	%>

<script src="assets/plugins/jquery.min.js"></script>
<script>
$(document).ready(function(){

	console.log("done");
	var resp1=<%=respAppId%>
	var resp2=<%=resp%>
	console.log(resp1);
	console.log(resp2);
	if(resp1 != null){
		
		if(!resp1.success){
			alert("Not able to login");
		}
	}
	if(resp1 != null && resp1.success && resp2.success){
		alert("Request submitted successfully");
	}
	else if(resp1 != null && resp1.success && !resp2.success){
		alert("Request failed."+resp2.error.error);
	}
});
</script>
</body>
</html>