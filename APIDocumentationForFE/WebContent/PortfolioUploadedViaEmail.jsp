<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.fundexpert.dao.User"%>
<%@page import="com.fundexpert.controller.RegisterUserController"%>
<%@page import="com.apidoc.util.JSON"%>
<%@page import="com.fundexpert.config.Config"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URLConnection"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.net.URL"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <meta http-equiv="refresh" content="10"> -->
<title>Recent PF Upload</title>
<script src="assets/plugins/jquery.min.js"></script>
</head>
<body>
<%
		Config configuration=new Config();
		Map<Long,String> map=new HashMap<Long,String>();
		//Make a request to app/portfolioreqest?action=getRequest to get Users with PortfolioRequest
		String urlPath;
		if(configuration.getProperty(Config.ENVIRONMENT).equals(Config.DEVELOPMENT))
		{
			//urlPath="https://www.fundexpert.in/app/portfoliorequest?action=getRequest";
			urlPath="http://localhost:8081/APIDocumentationForFE/app/portfoliorequest?action=getAllRequest";
		}
		else
		{
			urlPath="http://api.fundexpert.in/app/portfoliorequest?action=getAllRequest";
		}
		URL url = new URL(urlPath);
		URLConnection con=url.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setConnectTimeout(500000);
		con.setReadTimeout(500000);
		System.out.println("After opening connection get request for Portfolio on API server.");
	
		BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
		String line=br.readLine();
	
		JSONObject json=null;
		StringBuilder builder=new StringBuilder();
		while(line!=null)
		{
			builder.append(line);
			line=br.readLine();
		}
		br.close();
		System.out.print("Received JSON size="+builder.toString());
		json=new JSONObject(builder.toString());
		JSONArray arr=json.getJSONArray("pr");
		%>
		<table cellpadding="2" border="1px">
			<tr>
				<th>Email</th>
				<th>Submitted</th>
				<th>Portfolio</th>
				<th>StockPortfolio</th>
				<th>removeMfHolding</th>
				<th>removeStockHolding</th>
			</tr>
		<%
		
		for(int i=0;i<arr.length();i++)
		{
			JSONObject j=arr.getJSONObject(i);
			long userId=j.getLong("userId");
			RegisterUserController ruc=new RegisterUserController();
			User u=ruc.getUser(userId);
			map.put(u.getId(), u.getConsumer().getAppId());
			JSONObject userMfJson=JSON.toJSONObject(u, true);
			JSONObject userStockJson=JSON.toJSONObject(u, false);
			if(userMfJson.has("portfolio") || userStockJson.has("transactions"))
			{
				%>
				<tr>
					<td><%=j.getString("email") %></td>
					<td><%=j.getBoolean("submitted") %></td>
					<td><textArea rows="10" cols="70" name="portfolio" id="<%=userId%>"><%=userMfJson.toString(1) %></textArea></td>
					<td><textArea rows="10" cols="70" name="portfolio" id="<%=userId%>"><%=userStockJson.toString(1) %></textArea></td>
					<td><input type="button" value="Delete" id="<%=userId %>" onclick="removeMF(this)"></td>
					<td><input type="button" value="Delete" id="<%=userId %>" onclick="removeStock(this)"></td>
				</tr>
				<%
			}
		}
		
		%>
		</table>
		


</div>
<script>
function removeMF(userid)
{
	console.log("REMOVE MF.");
	var userIdToSubmit = $(userid).attr("id");
	
	console.log(userIdToSubmit);
	var obj={
			appId:'e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc',
			action:'deleteHolding',
			userId:userIdToSubmit
	}
	$.ajax(
			{
				url: "holding",
				type: 'POST',
				data: obj,
				async: true,
				contentType: "application/x-www-form-urlencoded",
				dataType: "json",
			  	success: function(data)
			  	{
			  		console.log(data);
				   	if(data.success)
					{
				   		alert("Successfully deleted MutualFund Holdings.");
				   		location.reload();
					}
				   	else
					{
						alert.log("Error Occurred while trying to delete Holding:"+data);   
				   	}
				},
				error: function(data) 
				{
				 	alert("Failed in deleting Holdings.");
				}
			}
		);
}
function removeStock(userid)
{
	console.log("REMOVE STOCK.");
	var userIdToSubmit = $(userid).attr("id");
	
	console.log(userIdToSubmit);
	var obj={
			appId:'e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc',
			action:'deleteStockHoldings',
			userId:userIdToSubmit
	}
	$.ajax(
			{
				url: "stockHolding",
				type: 'POST',
				data: obj,
				async: true,
				contentType: "application/x-www-form-urlencoded",
				dataType: "json",
			  	success: function(data)
			  	{
			  		console.log(data);
				   	if(data.success)
					{
				   		alert("Successfully deleted Stock Holdings.");
				   		location.reload();
					}
				   	else
					{
						alert("Error Occurred while trying to delete StockHolding:"+data);   
				   	}
				},
				error: function(data) 
				{
				 	alert("Failed in deleting StockHoldings.");
				}
			}
		);
}
</script>
</body>
</html>