<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="com.fundexpert.dao.User"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<style>
		.center {
		           display: block;
		           margin-top:20px;
		           width: 100%;
		           text-align: center;
                }
                .month-box {
					  color:black;
					  font-weight:bold;               
					  height:50px;
					  line-height:40px;
					  z-index:200;
					  background-color:orange;
					  width:300px;
					  
					  }
		</style>
		
</head>
<body class="container-fluid" >
 <div style="position: fixed; top:0; left:0; width: 100%; background-color:black; display: block;   color: white;
   text-align: center;"> welcome</div>
 <h1 align="center" style="margin-top: 20px">ADD STOCKS AND SEGMENT</h1>
   
   		 <div class="col-sm-8 col-xs-12" >
   		 <h3 align="center" style="margin-top: 10px">ADD STOCKS</h3>
    		<form class="form-inline" id="form">
    		<div class="form-group"> 
    			<label class="control-label">SEARCH SECTOR</label>
	            
	           <select  class="form-control" id="searchStocksId"  >
	             <option disabled="" value="" selected="" > SELECT </option>
	             </select>
	        
	          
	           <label class="control-label" >Risk Profile Number</label>
	           <select  class="form-control" id="riskProfile2Id"  >
	             <option disabled="" value="" selected="" > SELECT </option>
	          <%
							    		Session hSession=null;
							    		User user=null;
							    		try
							    		{
							    		hSession= HibernateBridge.getSessionFactory().openSession();
										long consumerId=0;
							    		user=(User)request.getSession().getAttribute("user");
										
										String loggedIn=(String)request.getSession().getAttribute("login");
										
										if(loggedIn==null || loggedIn.equals("false") || user==null)
										{
											consumerId=2;
									   /* throw new FundexpertException("You are not logged in.");*/
										}
										else {
											consumerId=user.getConsumerId();
										}
				                			Query query2= hSession.createQuery("select distinct riskProfile from SectorAllocation where consumerId=?").setLong(0, consumerId);  
							    		    List<Long> list2=query2.list();  
											for (int i = 0; i < list2.size(); i++) 
											{
								       			System.out.println(list2.get(i)); 
								       			%>
							         	    	   <option value="<%=list2.get(i)%>"><%=list2.get(i)%></option> 
							         	    	<%
											}
												%>
							  </select>
							         	  	    
							         	  	    <% 
											
							         	  	    }
											catch(Exception e){
												e.printStackTrace();
											}finally{
												hSession.close();	
											}
												%>
	            
	           
                <table class="table table-hover table-condensed" id="selectedStockTable">
                   <tbody  id="selectedStockTableBody"></tbody>
                </table>  
                  
                <input type="button" id="submit" class="btn btn-primary btn-sm" value="submit" onclick='myFunctionSubmit()'><br>
                <table class="table table-hover table-condensed" id="sectorTable">
		         	    		 
				  <tbody id="sectorTableBody" style="height:300px; width:300px; overflow-y:scroll; display:hidden">	   		
		          </tbody>
		        </table>              
    
    		</div>
   			</form>
 		</div>   			
		<div class="col-sm-4 col-xs-12">
		
		 	<h3 align="center" style="margin-top: 10px">Existing Stocks</h3>
		 	<h5 id="capType" align="right" style="margin-top: 10px; color:red;"></h5>
			 <section id="displayData"></section>
        </div>			        	    		 
</body>
</html>
<script>
var search_stock =$("#search_stock").val();

console.log(riskProfile2Id);
var stocksDataArray=[];
$(document).ready(function()
	{ 
	console.log("hey");
	 $("#riskProfile2Id").on("change",function()
	{
	var riskProfile2Id=$("#riskProfile2Id").val();
	console.log(riskProfile2Id);
	console.log("riskProfile2Id............................................................",riskProfile2Id);
	if(typeof riskProfile2Id!="undefined"||riskProfile2Id!=null||riskProfile2Id!="")
	{
	     $.ajax({
	    	    url: "StocksServlet?method=autocompeleteSegment",
				method: "POST",
				data:{riskProfile:riskProfile2Id},
				dataType:"json",
				success: function (data)
				{
					if(data.success==false)
						{
							alert("exception in servlet");
							
						}
					
					if(data.success==true)
						{
						alert("hello");
			    	     console.log("data is", data.sectorArray);
			    	     for(var i=0;i<data.sectorArray.length;i++){
			    	    	 $("#searchStocksId").append("<option value="+data.sectorArray[i].segmentName+" >"+data.sectorArray[i].segmentName+"</option>"); 
			    	    	 
			    	     }
			    	    
			    	         /* stocksDataArray.length=0; */
							/* itemLabel = "";
							itemId = 0;
							
		    				var mfs=$.map(data.success, function (obj)
		   					  { */
		    					  /*  $("#houseId").append("<option value="+obj.segmentName+" >"+obj.segmentName+"</option>"); */
		    					
		    			    	  
	         				  /*  return {
	               						 label: obj.segmentName,
	                					
	                                  };
	   		       			 });
		  					 $('#search_stock').autocomplete({
	           				    minLength:0,
		    					source: function (request, response) 
		    					{
		    						a = $.ui.autocomplete.filter(mfs, request.term);
		            				response(a.slice(0,30));	 
		            			},
		    					focus: function(event, ui) 
		    					{
		               				 $('#search_stock').val(ui.item.label);
		               				 return false;
		           				},
		            			select: function(event, ui) 
		          			    {
		                         itemLabel = $('#search_stock').val(ui.item.label);
					              
					             console.log(itemLabel);  */
					         //	$("#selectedStockTableBody").html("");
					        	//stocksDataArray.length=0;
					        	/* var search_stock =$("#search_stock").val();
					        	console.log("search_stock",search_stock)
					        	if(typeof search_stock!="undefined" && $("#search_stock").val()!='')
					        	{
					        		$.ajax({
					        	    	
					        	    	 
					        			url:"StocksServlet?method=getstocks",
					        			method: "POST",
					        			dataType:"json",
					        			data:{search_stock:search_stock},
					        			success: function (data)
					        			{
					        				if(data.success==false||data.success=="")
					        					{
					        						alert("exception in servlet");
					        						
					        					}
					        				
					        		       else 
					        					{
					        		    	   $("#sectorTableBody").html("");
					        		    	   $.each(data.success, function(key, item) 
					        		    			   {
					        		    			      console.log("scNname",item.scName);
					        		    			      console.log("scCode",item.scCode);
					        		    			     
					        		    			      $("#sectorTableBody").append("<tr><td>"+item.scName+"</td><td ><button onclick='myFunction3(this)' class='btn btn-primary btn-sm'  id="+item.scCode+">add</button></td></tr>"); 
					        		    			      document.getElementById("sectorTableBody").style.display='block';
					        		    			   });
					        					}				
					        			}		
					        	
					             });	
					        		
					        	}
					        	else{
					        		alert("choose one sector");
					        	} */
					             
		          				/* }			              
			                    }); */
						}	
				}
						
	     }); 
	     }
	     });
	 });
var  optionText =null;
$("#searchStocksId").on("change",function()
	    {
	$("#selectedStockTableBody").html("");
	stocksDataArray.length=0;
	
	$("#sectorTableBody").html("");
	    	 alert("hello");
	    	var searchStocksId=$("#searchStocksId").val();
	    	alert(searchStocksId);
	    	 optionText = $('option:selected', this).text();
	    	alert(optionText);
	    	$.ajax({
    	    	
   	    	 
    			url:"StocksServlet?method=getstocks",
    			method: "POST",
    			dataType:"json",
    			data:{search_stock:optionText},
    			success: function (data)
    			{
    				if(data.success==false||data.success=="")
    					{
    						alert("exception in servlet");
    						
    					}
    				
    		       else 
    					{
    		    	   $("#sectorTableBody").html("");
    		    	   $.each(data.success, function(key, item) 
    		    			   {
    		    			      console.log("scNname",item.scName);
    		    			      console.log("scCode",item.scCode);
    		    			     
    		    			      $("#sectorTableBody").append("<tr><td>"+item.scName+"</td><td ><button onclick='myFunction3(this)' class='btn btn-primary btn-sm'  id="+item.scCode+">add</button></td></tr>"); 
    		    			      document.getElementById("sectorTableBody").style.display='block';
    		    			   });
    					}				
    			}		
    	
         });	
    		
	    });
	     $.ajax({
	    	    url: "StocksServlet?method=displayAllData",
				method: "POST",
				dataType:"json",
				success: function (data)
				{
					if(data.empty=="empty"){
						alert("empty array");
					}
					if(data.success==false)
						{
							alert("exception in servlet while displaying existing record");
							
						}
					 
			       else{
			    	   var segment="";
			    	   var newsegment="";
			    	   var first="";
			    	   var flag=0;
			    	   var firstStockString="";
			    	   var firstSegmentString="";
			    	   var stockString="";
			    	   var midString="";
			    	   var largeCap=0;
			    	   var smallCap=0;
			    	   var midCap=0;
			    	   
			    	   var firstStockStringAfterOne = "";
			    	   
			    	   console.log(data);
			    	   
			    	   var rashiPreviousSegmentName = "";
			    	   
			    	   mainBoxStartString = "";
		    		   mainBoxMiddleString = "";
		    		   mainBoxEndString = "";
		    		   
		    		   console.log(data.success.length);
			    	   
			    	   for(var i=0; i < data.success.length;i++){
			    		   
			    		   console.log("-----------*******" + i);
			    		   console.log(data.success[i]);

			    		   mianBoxString = ""
			    		   
			    		   var rashiSegmentName = data.success[i].segment;
			    		   
			    		   //when segment name changes
			    		   if(rashiPreviousSegmentName != rashiSegmentName){
				    		   mainBoxStartString = rashiPreviousSegmentName;
				    		   mainBoxEndString = "</div>";

				    		   console.log(mainBoxStartString);
				    		   console.log(mainBoxMiddleString);
				    		   console.log(mainBoxEndString);
				    		   firstSegmentString="";
				    		   firstSegmentString= "<div>"+
				    		   "<label  class='center text-center month-box' onclick='$(this).next().slideToggle(500)' for='toggle'>"
	    			    	    +mainBoxStartString	
	    			    	    
   			    	    	 	+"<i style='font-size:30px; color:black;' class='fa fa-caret-down pull-right'></i>"
   			    	    	    +"</label>"	
   			    	    	    +"<div style='display:none;'>"+mainBoxMiddleString+"</div>"
		                        + "</div>";
                                 if(mainBoxMiddleString!="")   				         
		                        $("#displayData").append(firstSegmentString);   
				                    
				    		   
			    			   rashiPreviousSegmentName = rashiSegmentName;
				    		   mainBoxMiddleString = "";
				    		   console.log("H1: rashiSegmentName: ", rashiSegmentName);
			    		   }
			    		   
			    		   mainBoxMiddleString +="<div>"+data.success[i].scName+"<div>";
			    		   
			    		   console.log("stock: ", data.success[i].scName);
			    		   
			    		   if(i == data.success.length-1){
			    			   
			    			   mainBoxStartString = rashiPreviousSegmentName;
				    		   mainBoxEndString = "</div>";
				    		   firstSegmentString="";
				    		   firstSegmentString= "<div>"+
				    		   "<label  class='center text-center month-box' onclick='$(this).next().slideToggle(500)' for='toggle'>"
	    			    	    +mainBoxStartString	
	    			    	    
   			    	    	 	+"<i style='font-size:30px; color:black;' class='fa fa-caret-down pull-right'></i>"
   			    	    	    +"</label>"	
   			    	    	    +"<div style='display:none;'>"+mainBoxMiddleString+"</div>"
		                        + "</div>";
                                 if(mainBoxMiddleString!="")   				         
		                        $("#displayData").append(firstSegmentString);   

				    		   console.log(mainBoxStartString);
				    		   console.log(mainBoxMiddleString);
				    		   console.log(mainBoxEndString);
			    			   
			    		   }
			    		   
			    		   
			    	   }
			    	   
			    	   
			    	
			    	 /*   $.each(data.success, function(key, item) 
			    			   {
					    		   if(item.type=="LARGE CAP"){
			    			    		largeCap+=1;
			    			    		
			    			    	}else if(item.type=="MID CAP"){
			    			    		midCap+=1;
			    			    	}else if(item.type=="SMALL CAP"){
			    			    		smallCap+=1;
			    			    	}else
			    			    	{
			    			    		
				    			    }
				    		        console.log("*******************largecap*************",largeCap);
				    		        console.log("*******************smallcap*************",smallCap);
				    		        console.log("*******************midcap*************",midCap);
						    		  
						    		 
				    		  		 
			    			      if(key==0){
			    			    	segmentString="";
		   			    		    stockString="";
		   			    		    
			    			    	segment=item.segment;
			    			    	stockString+=item.scName;
			    			    	
			    			    	
			    		         
			    			    	/*
			    			    	only for first segment nd its stock(firstSegmentString,firstStockString)
			    			    	*/
			    			    	/*firstSegmentString+=item.segment;
			    			    	firstStockString+=item.scName;
			    			    	console.log("firstSegmentString and firstStockString",firstSegmentString+","+ firstStockString);
			    			    	
			    			    				    			    	
			    			    	console.log("for key=0");
			    			    	console.log("...........SEGMENT IS.........",item.segment)
			    			    	console.log("scName is",item.scName);
			    			    	console.log("scCode  is",item.scCode);
			    			    	console.log("type is",item.type);
			    			    	
			    			    	newsegment=segment;
			    			      }else
			    			      {
			    			    	  
			    			    	  if(newsegment==item.segment)
			    			    	  {
				    			    		if(item.segment==segment)
				    			    		{
				    			    		firstStockString+="<div>"+item.scName+"</div>";
				    			    		console.log("if equals then firstStockString",firstStockString);
				    			    		console.log("item.segment",item.segment);
				    			    		console.log("segment",segment);
				    			    		}
				    			    		
				    			    		if(item.segment!==segment)
				    			    		{
					    			    		stockString+="<div>"+item.scName+"</div>";
					    			    		midString+=stockString;
					    			    		console.log("equals");
					    			    		console.log("stockString is",stockString);
					    			    		console.log("scName is",item.scName);
					    			    		console.log("scCode  is",item.scCode);
					    			    		console.log("type is",item.type);
				    			    		}
			    			    	  }
			    			    	  	
			    			    	  		//RUNIING WHEN THE NAME IS CHANGING FOR 2nd OBJ Onwards
			    			    	     else
			    			    	     {
			    			    	    	 flag+=1;
			    			    	    	 console.log("flag is",flag);
			    			    	    	 if(flag == 1)
			    			    	    	 {
			    			    	    		 
			    			    	    	 firstSegmentString= "<label  class='center text-center month-box' onclick='$(this).next().slideToggle(500)' for='toggle'>"
			    			    	    	
			    			    	    	 +firstSegmentString
			    			    	    	 +"<i style='font-size:30px; color:black;' class='fa fa-caret-down pull-right'></i>"
			    			    	    	 +"</label>"
						                     +"<div style='display:none;'>"+firstStockString+"</div>"
						                     + "</div>"; 				         
						                     console.log(" we will see",firstSegmentString);
						                      $("#displayData").append(firstSegmentString);
						                      segmentString="";
				   			    		      stockString="";
						                     // newsegment=firstSegmentString;
						                   
				   			    		        			    	      
						                     }    
			    			    	       	
			    			    	    	   firstSegmentString="";
						                       console.log("................SEGMENT IS..............",item.segment);
					    			    	   console.log("else scName is",item.scName);
					    			    	   console.log("scCode  is",item.scCode);
					    			    	   console.log("type is",item.type);
					    			    	   console.log("before display stockString",stockString);
					    			    	   console.log("segment string is",segmentString)
					    			    	   console.log("flag is",flag);	
					    			    	   console.log("stockString is null or not",stockString);
					    			    	   
					    			    	   midString="<div style='display:none;'id='a'>"+stockString+"</div>";
			    			    	    	   stockString="";
					    			    	   segmentEndString="</div>"; 
					    			    	   if(segmentString!="")
					    			    	   $("#displayData").append(segmentString+midString+segmentEndString);
					    			    	   segmentString="";
				   			    		       segmentString+="<div>"+
					    			    	  "<label class='center text-center month-box' onclick='$(this).next().slideToggle(500)' for='toggle' >"+item.segment
					    			    	  +"<i style='font-size:30px; color:black;' class='fa fa-caret-down pull-right'></i>"
					    			    	  +"</label>";
					    			    	 
					    			    	  
					    			    	   stockString+="<div>"+item.scName+"</div>";	    			    	         
					    			    	   console.log("segmentString",segmentString)
					    			    	   newsegment=item.segment;		    			    		 
			    			    	         			  	  
			    			    	   	  }
			    			      }
			    			     
			    			   }); */		    			    	   
			  		 
			    	   /* if(largeCap==0&&smallCap==0){
		    			   $("#capType").append("large and small cap missing");
		    		   }else if(smallCap==0&& midCap==0){
		    			   $("#capType").append("small cap  and midCap is missing"); 
		    		   }else if(midCap==0&& largeCap==0){
		    			   $("#capType").append("mid cap  and large missing"); 
		    			   
		    		   }else if(largeCap==0){
		    			   $("#capType").append("large cap missing");
		    		   }else if(smallCap==0){
		    			   $("#capType").append(" small cap missing");
		    		   }else if(midCap ==0){
		    			   $("#capType").append(" mid cap missing");
		    		   }
		    		   else{
		    			   $("#capType").append("All types of caps"); 
		    		   }  */

			       }
						
				}		
		
	     });	
		// });			   			    		      
					   			    		     	   			    		          
						    			    		 
							    			    	  
								                     
							                     
							                      				    			    		 
                  				    			    		 					   			    		     
					    			    		 
					   			    		      
					    			    		  
					    			    	
			    	

function myFunction3(element)
{
	 $elem = $(element);
	 $elem.parent().parent().remove();		  
	 var movedScCode=$elem.attr('id');
	 var movedScName= $elem.closest('tr').find('td:eq(0)').text();
	 console.log("dd",movedScName);
	 $("#selectedStockTableBody").append("<tr><td>"+movedScName+"</td><td ><button onclick='myFunction4(this)' class='btn btn-primary btn-sm'  id="+movedScCode+">Delete</button></td></tr>");
	 stocksDataArray.push(movedScCode);
	 console.log("stocksDataArray is",stocksDataArray);
	 
	
	
	
	  
}
function myFunction4(element)
{
	 $elem = $(element);
	 $elem.parent().parent().remove();
	 var deleteId=$elem.attr('id');
	 console.log("delete function",stocksDataArray)
	 var idxb = $.inArray(deleteId,stocksDataArray);
	 console.log("var is "+idxb);
	
	  if(idxb == -1)
	  {
		  
	  }
	  else
	  {
		  stocksDataArray.splice(idxb, 1);
		  console.log("after deleting array is: "+stocksDataArray);
		  console.log("stocksDataArray"+idxb);
	  }
}

function myFunctionSubmit()
{
	
	var riskProfile  =$("#riskProfile2Id").val();
	
	
	var search_stock=optionText;
	
  
	
	console.log("stocksDataArray.length",stocksDataArray.length);
	console.log("stocksDataArray............................",stocksDataArray);
	
	//console.log("search_stock",$("#search_stock").val());
	var obj={};
	obj.stocksDataArray=stocksDataArray;
	obj.riskProfile=riskProfile;
	obj.search_stock=optionText;
	
	console.log("obj is------------------------------------------------",obj.stocksDataArray);
	console.log("optionText............................................",optionText);
	console.log("riskProfile............................",riskProfile);
	console.log("search_stock............................",search_stock);
	if(stocksDataArray.length==0){
		alert("choose atleast one sector");
	}
	
	if(typeof search_stock!="undefined" && search_stock!="" &&typeof riskProfile!="undefined"&&stocksDataArray.length!=0 && riskProfile!=""){
		
		$.ajax({
    	    url:"StocksServlet?method=submit",
    	    data:obj,
			method: "POST",
			dataType:"json",
			
			success: function (data)
			{
				if(data.success==false)
					{
						alert("exception in servlet");
						
					}
				
		       else
					{
		    	      window.location.reload(); 
					}				
			}		
	
     });	
	}else
	{
		
		alert("all fields are mandatory");
		console.log("hey");
	
		
	}
}

function isNumber(evt) {
	console.log("isNum");
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>		
		
		
