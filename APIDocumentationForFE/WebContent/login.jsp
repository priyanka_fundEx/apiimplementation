<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body class="container-fluid">
<%
	HttpSession session2=request.getSession();
	Enumeration enum1=session2.getAttributeNames();
	
	System.out.println("enumsize="+enum1);
	while(enum1.hasMoreElements())
	{
		String s=(String)enum1.nextElement();
		System.out.println("Enum attr="+s);
	}
	int role=100;
	System.out.println("Session2 : login="+session2.getAttribute("login"));
	if(session2.getAttribute("login")!=null && session2.getAttribute("login").equals("true"))
	{
		response.sendRedirect("data-fillup/link.jsp");
		return;
	}
%>
<div class="col-sm-6 col-xs-12">
<h3 align="center" style="margin-top: 30px">LOGIN PAGE</h3>
<form >
  <div class="form-group">
    <label >APP ID</label>
    <input type="text" class="form-control" id="app_id"  placeholder="Enter APP id">
    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="password_id" placeholder="Password">
    <input type="checkbox" onclick="myFunction3()">Show Password
  </div>
  <!-- <div class="form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> -->
  <input type="button" id="submit" class="btn btn-primary btn-sm" value="Login">
  
</form>
</div>
</body>
</html>
<script>
function myFunction3() {
    var x = document.getElementById("password_id");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
$(document).ready(function()
		{
	 $("#submit").on("click",function()
			{
		var app_id=$("#app_id").val();
		var password_id=$("#password_id").val();
		
		console.log(app_id);
		console.log(password_id);
		
		if(app_id==0|password_id==0)
			{
			alert("all fields are mandatory");
			}
		else{
			$.ajax({
			url: "user?method=adminLogin",
			data: {app_id: app_id,password_id:password_id},
			method: "GET",
			dataType:"json",
			
			success: function (data)
			{   
				console.log(data.status1);
				if(typeof data.success!='undefined' && data.success==false)
				{
					alert(data.error);
				}
				if(data.success==true)
				{
					if(data.role>=4)//editor(who can edit lists) and viewer(can only view) have 6 and 4 roles
					{
						var link="data-fillup/link.jsp";
						window.location.href=link;
					}
					else
					{
						alert("Not a valid User.");
					}
				}
				
			},
	         statusCode: {
	               404: function() {
	                   alert("Server Down.");
	               }
	         }
				 });
			}
			});
        }); 
</script>