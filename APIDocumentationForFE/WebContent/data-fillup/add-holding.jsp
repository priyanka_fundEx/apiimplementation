<%@page import="com.fundexpert.dao.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.fundexpert.controller.MutualFundController"%>
<%@page import="com.fundexpert.dao.FundHouse"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%
	/* User user=null; 
	if(session.getAttribute("login")==null || session.getAttribute("login").equals("false"))
	{
		response.sendRedirect("../../login.jsp");
		return;
	}
	else
	{
		user=(User)session.getAttribute("user");
	} */
%> 
<meta charset="ISO-8859-1">
<title>Add Holdings For Rebalance</title>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<div class="col-sm-12 col-xs-12">
<div class="col-sm-6 col-xs-12">

	<select  id="houseId" class="form-control">
	   <option disabled="" value="" selected="">  SELECT fundhouse </option>
	   <%/* populating dropdown from db(fundhouse table) so that user can delete by fundhouse  */
		MutualFundController mfc=new MutualFundController();
		List<FundHouse> list=mfc.getFundHouseList();
		Iterator<FundHouse> itr=list.iterator(); 
		itr=list.iterator(); 
		while(itr.hasNext())
		{
			FundHouse fh1=itr.next();
			%>
			<option value="<%=fh1.getId()%>"><%=fh1.getName()%></option>
			<% 
		} 
		%>
	</select>
	</div>
	<div class="col-sm-6 col-xs-12">
	<select id="capitalType"  class="form-control">
		<option disabled="" value="" selected="">Choose Capital Type</option>
		<option value="LARGE CAP">Large cap</option>
		<option value="MID CAP">Mid cap</option>
		<option value="SMALL CAP">Small cap</option>
		<option value="LONG TERM">Long term</option>
		<option value="SHORT TERM">Short term</option>
	</select>
	</div>
</div>
</br></br></br>
<input type="button" name="getFunds" id="getFunds" value="GetMF" style="float: center;" onclick='getFunds()'/>
<br>
 <div class="col-sm-6 col-xs-8">
	<div class="form-group">
		<input type="text" class="form-control input-md ui-widget ui-autocomplete-input" id="search-funds" mfId="" type="text" placeholder="Search Mutual Funds.." onfocus="this.value=''">
	</div>
</div>
<div class="col-sm-2 col-xs-2">
<div class="form-group">
		<input type="number" class="form-control" id="units" value="1"/>
</div>
</div>

<br>
<input type="button" name="addMF" id="addMF" value="addHolding"/>
<input type="button" name="deleteExistingHoldings" id="deleteH" value="DeleteExistingHolding" onclick='deleteHoldings()'/>
<table style="width:70%" border="2px" id="myTable">
	<tr>
		<th style="text-align: center">MF</th>
		<th style="text-align: center">units</th>
		<th style="text-align: center"></th>
	</tr>
</table>
<button onclick="createHolding()" class="btn btn-primary btn-sm" id='createHolding'>CreateHolding</button>
<script>
var tempMF={};
var globalMF=[];
$(document).ready(function(){

	$('#addMF').click(function() {
			var units = Number($('#units').val());
			console.log("Units = "+units);
			if(typeof units=='undefined' || units==0)
				alert('Units is mandatory.');
			else
			{
				var mfIdToBeAdded=$('#search-funds').attr('mfId');
				console.log("MFAdded = "+mfIdToBeAdded);
				var flag=false;
				for(var j=0;j<globalMF.length;j++)
				{
					var json=globalMF[j];
					if(json.id==mfIdToBeAdded)
					{
						console.log("Existing funds id = "+json.id+" name = "+json.name);
						flag=true;
						break;						
					}
				}
				if(!flag)
				{
					tempMF.units=units;
					globalMF.push(tempMF);
					console.log("globalMF = ",globalMF);
					console.log("gmf  = "+globalMF);
				   $('#myTable tbody').append('<tr class="child"><td>'+tempMF.name+'</td><td>'+units+'</td><td><button onclick="myDeleteFunction(this)" class="btn btn-primary btn-sm" id='+tempMF.id+'>Remove</button></td></tr>');
					tempMF={};
				}
				else
				{
					alert("MutualFund already added.Try deleting existing one.");
				}
			}
		});
});

function getFunds()
{
	var capitalType=$('#capitalType').val();
	var houseId=$('#houseId').val();
	console.log(capitalType+" houseid "+houseId);
	if(typeof capitalType=='undefined' || typeof houseId=='undefined' || capitalType==null || houseId==null)
		alert("Please select houseId and capital type.");
	else
	{
		var houseId=$("#houseId").val();
		$.ajax({
			url: "/mf?action=getFundByHouseId",
			data: {houseId: houseId,capitalType:capitalType},
			method: "GET",
			dataType:"json",
	    	error: function () 
			{
	   	     	 alert("Server Down.");
			},
			success: function (data)
			{
				var success=data.success;
				console.log("success="+success);
				console.log(data);
				if(typeof success==='undefined' || success==false)
				{
					alert("Ajax failed to get MutualFund for FundHouse = "+houseId+" CapitalType = "+capitalType);
				}
				else
				{
					var mfMap=$.map(data.mfData,function(obj)
					{
						return {label:obj.name,id:obj.id};
					});
					$('#search-funds').autocomplete({
           	 			minLength: 2,
           	 			delay: 0,
						source: function (request, response) 
						{
	    					a = $.ui.autocomplete.filter(mfMap, request.term);
	            			response(a.slice(0,15));
            			},
    					focus: function(event, ui)
    					{
	                		$('#search-funds').val(ui.item.label);
	                		return true;
  				 		},
            			// Once a value in the drop down list is selected, do the following:
            			select: function(event, ui) 
            			{
                			//add a row to table which contains MF and 
                			itemLabel = $('#search-funds').val(ui.item.label);
                			$('#search-funds').attr("mfId",ui.item.id);
                			console.log("itemLabel = "+ui.item.id);
                			tempMF.name=$("#search-funds").val();
                			tempMF.id=ui.item.id;
                			
                			console.log("tempMF = "+JSON.stringify(tempMF)); 
                		}
       		 			});
				}
			}
		});
	}
	console.log("HouseId="+houseId);
	console.log("CapitalType",capitalType);
		
}
function myDeleteFunction(element)
{
	console.log("Remove single mutualfund.");
	var tableid= document.getElementById("myTable");
	$elem = $(element);
	console.log("element:",element);
	console.log("$element:",$elem);
	console.log($elem.parent());
	/* remove table row when click on remove button */
	$elem.parent().parent().remove();
	var fundidd=$elem.attr('id');
	alert("You re removing id = "+fundidd);
	/*if button id that is fundidd exists in fundidarray then remove it from array  */
	for(var j=0;j<globalMF.length;j++)
	{
		var json=globalMF[j];
		if(json.id==fundidd)
		{
			console.log("Removing id found in globalMF.");
			globalMF.splice(j,1);
			break;						
		}
	}
	console.log("globalMF=",globalMF);
}
function createHolding()
{
	$.ajax({
		url: "/holding?action=createHolding&appId=e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc&userId=3",
		data: JSON.stringify(globalMF),
		method: "POST",
		dataType:"json",
		contentType:"application/json; charset=utf-8",
    	error: function () 
		{
   	     	 alert("Server Down.");
		},
		success: function (data)
		{
			if(typeof data.success!='undefined' && data.success)
			{
				alert("Successfully created the holdings.");
				console.log("Success = "+data.success);
				console.log("After creating holding emptying the backend variable globalMF.");
				globalMF.splice(0,globalMF.length);
				$('#myTable').empty();
			}
			else
			{
				if(typeof data.success=='undefined')
					alert('Something Failed');
				else
					alert(data.error);
			}
		}
	});
}
function deleteHoldings()
{
	var data;
	$.ajax({
		url: "/holding?action=deleteHolding&appId=e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc&userId=3",
		data: data,
		method: "POST",
		dataType:"json",
    	error: function () 
		{
   	     	 alert("Server Down.");
		},
		success: function (data)
		{
			if(data.success)
			{
				alert("Successfully deleted your mf holdings.");
				console.log("Success = "+data.success);
			}
		}
	});
}
</script>

</body>
</html>