<%@page import="com.fundexpert.dao.User"%>
<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Existing Recommended Table</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<style>
         table tbody {overflow-y:scroll; height:300px; display:block;}
        </style>
</head>
<body class="container-fluid">
<%
User user=null;
/*  if user is logged out then go to login page(sendRedirect to loginpage)*/
if(session.getAttribute("login")==null || session.getAttribute("login").equals("false"))
{
	response.sendRedirect("/login.jsp");
	return;
}
else if(session.getAttribute("user")==null)
{
	response.sendRedirect("/login.jsp");
	return;
}
else
{
	 user=(User)session.getAttribute("user");
}
if(user==null)
{
	 response.sendRedirect("/login.jsp");
	 return;
}
long consumerId=user.getConsumerId();   

     
   
%> 

<h1 align="center" style="margin-top: 40px">EXISTING DATA</h1>
<div class="col-sm-6 col-xs-12">
<h3 align="center" style="margin-top: 30px">Edelweiss Recommended Stocks </h3>
<form id="form">

<table class="table table-hover" id="table1">

 <caption align="top" style="background-color:black;
    color: white;">EXISTING STOCKS</caption> 
 <%
Session hSession=null;  
List<Object[]> rows=null;
try
{ 
	hSession=HibernateBridge.getSessionFactory().openSession();
	Query query=hSession.createQuery("SELECT sk.scName,sk.scCode FROM Stocks sk ,RecommendedPortfolio rp where sk.scCode=rp.scCode and rp.consumerId=? ORDER BY sk.scName").setLong(0, consumerId);
 	rows = query.list();
 	for(Object[] row :rows )
 	{
	  //  Mutualfund mf = new Mutualfund();
		String name=(row[0].toString());
		long scCode=Long.parseLong(row[1].toString());
	
		System.out.println(name);
		System.out.println(scCode);
		%>
		 <tr>
		<td width="10%"><%=name%></td>
		</tr>
	<%
	} 
	%>
		
</table>
</form>
</div>

		<%
		}catch(Exception e){
			e.printStackTrace();
		}
 finally{
	 hSession.close();
 }
		%>
</table>
</form>
</div>
<div class="footer" style=" position: fixed;
   left: 0;
   bottom:0;
   width: 100%;">
 <div style="float:left">
<form align="left" name="form1" method="post" action="../link.jsp">
  <input name="prvious" type="submit" id="previous" class="btn btn-primary btn-sm" value="<<Previous Page">
</div>


</body>
</html>