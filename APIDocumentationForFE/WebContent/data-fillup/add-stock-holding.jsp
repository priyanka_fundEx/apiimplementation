<%@page import="com.fundexpert.dao.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.fundexpert.controller.MutualFundController"%>
<%@page import="com.fundexpert.dao.FundHouse"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%
	/* User user=null; 
	if(session.getAttribute("login")==null || session.getAttribute("login").equals("false"))
	{
		response.sendRedirect("../../login.jsp");
		return;
	}
	else
	{
		user=(User)session.getAttribute("user");
	} */
%>
<meta charset="ISO-8859-1">
<title>Add Stock Holdings For Rebalance</title>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<link
	href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"
	rel="Stylesheet"></link>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
</head>
<body>
<h1 align="center">Add Stock Holdings</h1>
	<div class="col-sm-12 col-xs-12">
		<div class="col-sm-4 col-xs-5">
			<select id="capitalType" class="form-control">
				<option disabled="" value="" selected="selected">Choose Type</option>
				<option value="LARGE CAP">Large cap</option>
				<option value="MID CAP">Mid cap</option>
				<option value="SMALL CAP">Small cap</option>
			</select>
		</div>
		<div class="col-sm-6 col-xs-5">
			<div class="form-group">
				<input type="text"
					class="form-control input-md ui-widget ui-autocomplete-input"
					id="search-funds" stockId="" type="text"
					placeholder="Search Stocks.." onfocus="this.value=''">
			</div>
		</div>
		<div class="col-sm-2 col-xs-2">
			<div class="form-group">
				<input type="number" class="form-control" id="units"  placeholder="Enter Units.."/>
			</div>
		</div>
	</div>
	</br>
</br>
<input type="button" name="addStock" id="addStock" value="addHolding"/>
<input type="button" name="deleteExistingHoldings" id="deleteH" value="DeleteExistingHolding" onclick='deleteHoldings()'/>
<table style="width:70%" border="2px" id="myTable">
	<tr>
		<th style="text-align: center">Stock</th>
		<th style="text-align: center">units</th>
		<th style="text-align: center"></th>
	</tr>
</table>
<button onclick="createHolding()" class="btn btn-primary btn-sm" id='createHolding'>CreateHolding</button>
<script>
var tempStock={};
var globalStock=[];
$(document).ready(function(){

	var capitalTypeValue;
	$('#capitalType').on('change',function(){
		console.log("change");
		capitalTypeValue=$(this).val();
		getStocks(capitalTypeValue);
	});
	$('#addStock').click(function(){
		console.log("tempStock = ",tempStock)
		if(typeof tempStock.scCode!='undefined')
		{
			console.log("Adding Stock holding temporarily in globalStock.");
			var units = Number($('#units').val());
			if(/^[0-9]+$/.test(units)==false)
			{
				console.log("Units = "+units);
				alert("Units cannot be in decimal.");
			}
			else
			{
				console.log("Units = "+units);
				if(typeof units=='undefined' || units==0)
					alert('Units is mandatory.');
				else
				{
					var stockIdToBeAdded=$('#search-funds').attr('stockId');
					console.log("StockAdded = "+stockIdToBeAdded);
					var flag=false;
					for(var j=0;j<globalStock.length;j++)
					{
						var json=globalStock[j];
						if(json.id==stockIdToBeAdded)
						{
							console.log("Existing stocks id = "+json.id+" name = "+json.name);
							flag=true;
							break;						
						}
					}
					if(!flag)
					{
						tempStock.units=units;
						globalStock.push(tempStock);
						console.log("globalStock = ",globalStock);
						console.log("gmf  = "+globalStock);
					   $('#myTable tbody').append('<tr class="child"><td>'+tempStock.name+'</td><td>'+units+'</td><td><button onclick="myDeleteFunction(this)" class="btn btn-primary btn-sm" id='+tempStock.scCode+'>Remove</button></td></tr>');
						tempStock={};
						$('#units').val(0);
						$('#search-funds').val('');
					}
					else
					{
						alert("Stock already added.Try after deleting existing one.");
					}
				}
				
			}
		}
		else
			alert("First select stock and units to add.");
	});
	
});
function getStocks(capitalType)
{
	var data;
	$.ajax({
		url: "/stocks?action=getStocksByCapitalType",
		data: {capitalType:capitalType},
		method: "GET",
		dataType:"json",
    	error: function () 
		{
   	     	 alert("Server Down.");
		},
		success: function (data)
		{
			var success=data.success;
			console.log("success="+success);
			console.log(data);
			if(typeof success==='undefined' || success==false)
			{
				alert("Ajax failed to get Stocks for capital type = "+capitalType);
			}
			else
			{
				var mfMap=$.map(data.stocks,function(obj)
				{
					return {label:obj.name,id:obj.scCode};
				});
				$('#search-funds').autocomplete({
       	 			minLength: 2,
       	 			delay: 0,
					source: function (request, response) 
					{
    					a = $.ui.autocomplete.filter(mfMap, request.term);
            			response(a.slice(0,15));
        			},
					focus: function(event, ui)
					{
                		$('#search-funds').val(ui.item.label);
                		return false;
				 	},
        			// Once a value in the drop down list is selected, do the following:
        			select: function(event, ui) 
        			{
            			//add a row to table which contains MF and 
            			itemLabel = $('#search-funds').val(ui.item.label);
            			$('#search-funds').attr("stockId",ui.item.id);
            			console.log("itemLabel = "+ui.item.id);
            			tempStock.name=$("#search-funds").val();
            			tempStock.scCode=ui.item.id;
            			
            			console.log("after searching stock selected :tempStock = "+JSON.stringify(tempStock)); 
            		}
   		 			});
			}
		}
	});
}
function myDeleteFunction(element)
{
	console.log("Remove single Stock.");
	var tableid= document.getElementById("myTable");
	$elem = $(element);
	console.log("element:",element);
	console.log("$element:",$elem);
	console.log($elem.parent());
	/* remove table row when click on remove button */
	$elem.parent().parent().remove();
	var fundidd=$elem.attr('id');
	alert("You re removing id = "+fundidd);
	/*if button id that is fundidd exists in fundidarray then remove it from array  */
	for(var j=0;j<globalStock.length;j++)
	{
		var json=globalStock[j];
		if(json.scCode==fundidd)
		{
			console.log("Removing id found in globalStock.");
			globalStock.splice(j,1);
			break;						
		}
	}
	console.log("globalStock=",globalStock);
}
function createHolding()
{
	if(globalStock.length>0)
	{
		$.ajax({
			url: "/stockHolding?action=createStockHoldings&userId=3&appId=e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc",
			data: JSON.stringify(globalStock),
			method: "POST",
			dataType:"json",
			contentType:"application/json; charset=utf-8",
	    	error: function () 
			{
	   	     	 alert("Server Down.");
			},
			success: function (data)
			{
				if(typeof data.success!='undefined' && data.success)
				{
					alert("Successfully created the holdings.");
					console.log("Success = "+data.success);
					console.log("After creating stock holding emptying the backend variable globalStock.");
					globalStock.splice(0,globalStock.length);
					$('#myTable').empty();
				}
				else
				{
					if(typeof data.success=='undefined')
						alert('Something Failed');
					else
						alert(data.error);
				}
			}
		});
	}
	else
		alert("Add atleast one stock holding to table.");
}
function deleteHoldings()
{
	var data;
	$.ajax({
		url: "/deleteStockHoldings?action=deleteHolding&appId=e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc&userId=3",
		data: data,
		method: "POST",
		dataType:"json",
    	error: function () 
		{
   	     	 alert("Server Down.");
		},
		success: function (data)
		{
			if(data.success)
			{
				alert("Successfully deleted your stock holdings.");
				console.log("Success = "+data.success);
			}
			else
				alert("Not able to delete stock holdings with error = "+data.error);
		}
	});
} 
</script>

</body>
</html>