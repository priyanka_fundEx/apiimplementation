<%@page import="java.util.Enumeration"%>
<%@page import="com.fundexpert.dao.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<%
	HttpSession session2=request.getSession();
	Enumeration enum1=session2.getAttributeNames();
	System.out.println("enumsize="+enum1);
	while(enum1.hasMoreElements())
	{
		String s=(String)enum1.nextElement();
		System.out.println("Enum attr="+s+" value="+session2.getAttribute(s));
	}
	int role=100;
	if(session2.getAttribute("login")==null || session2.getAttribute("login").equals("false"))
	{
		System.out.println("No Session Exists redirecting to login page from link.jsp");
		response.sendRedirect("../login.jsp");
		return;
	}
	else
	{
		User u=(User)session2.getAttribute("user");
		if(u!=null)
			role=u.getRole();
		System.out.println("Users role="+role);
	}
	
%>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	<body class="container-fluid">
		<div style="float:right">
		
 			<form align="right" name="form1" >
  				<label class="logoutLblPos">
  				<input name="logout" type="button" id="logout" onclick='myFunction()' class="btn btn-primary btn-sm" value="log out">
 			    </label>
  			</form>
		</div>
		<h1 align="center" style="margin-top: 40px">PREQUISITE FOR REBALANCE MODULE</h1>
			<div class="col-sm-4 col-xs-12">
				<h4 style="margin-top: 20px">Blacklist</h4>
				<h5 style="margin-top: 20px">Choose one link</h5>
				<ol>
				<% 
				if(role>=5)
				{
				%>
					<h5></h5>
					<li><a href="blacklist/index.jsp" style="font-size:13px" target="_blank">Add/Remove BLACKLIST Funds</a></li>
					<h5></h5>  
					<li><a href="blacklist/stock-index.jsp" style="font-size:13px">Add/Remove BLACKLIST Stocks</a></li>
				<%
				}
				%>
				<h5></h5>  
				<li><a href="blacklist/currenttable.jsp" style=" font-size:13px">View Existing Records</a></li>
				</ol>
			</div>
			    <div class="col-sm-4 col-xs-12">
					<h4 style="margin-top: 20px">Edelweiss Recommended Portfolio</h4>
					<h5 style="margin-top: 20px">Choose one link</h5>
					<ol>
					<%
					if(role>=5)
					{
					%>
						<h5></h5>
						<li><a href="recommended/stock-index.jsp" style="font-size:13px">Add/Remove EDELWEISS RECOMMENDED PORTFOLIO Funds</a></li>
					<%
					}
					%>
					<h5></h5>
					<li><a href="recommended/currenttable.jsp" style="font-size:13px">View Existing Records</a></li>
					</ol>
				</div>
					<div class="col-sm-4 col-xs-12">
						<h4 style="margin-top: 20px">Edelweiss Reco</h4>
						<h5 style="margin-top: 20px">Choose one link</h5>
						<ol>
						<%
						if(role>=5)
						{
						%>
							
							<h5></h5>
							<li><a href="reco/index.jsp" style="font-size:13px">Add/Remove EDELWEISS RECO Funds</a></li>
							<h5></h5>
							<li><a href="reco/stock-index.jsp" style="font-size:13px">Add/Remove EDELWEISS RECO Stocks</a></li>
						<%
						}
						%>
						<h5></h5>
						<li><a href="reco/currenttable.jsp" style="font-size:13px">View Existing Records</a></li>
						</ol>
						
					</div>


	</body>
<script>
function myFunction()
{		 
				$.ajax({
				url: "../user?method=logout",
				method: "GET",
				dataType:"json",
				success: function (data)
				{   
					if(data.success==true)
					{
						var link="../login.jsp";
						window.location.href=link;
					}
					else
					{
						alert("Not able to logout.");
					}
				}
				});
	       
}
</script>


</html>
