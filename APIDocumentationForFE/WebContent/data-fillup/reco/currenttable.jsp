<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="com.fundexpert.dao.User"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Existing Reco Table</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<style>
         table tbody {overflow-y:scroll; height:300px; display:block;}
        </style>
</head>
<body class="container-fluid">
<%
	User user=null;
	/*  if user is logged out then go to login page(sendRedirect to loginpage)*/
	if(session.getAttribute("login")==null || session.getAttribute("login").equals("false"))
	{
		response.sendRedirect("/login.jsp");
		return;
	}
	else if(session.getAttribute("user")==null)
	{
		response.sendRedirect("/login.jsp");
		return;
	}
	else
	{
		 user=(User)session.getAttribute("user");
	}
	if(user==null)
	{
		 response.sendRedirect("/login.jsp");
		 return;
	}
	long consumerId=user.getConsumerId();   
%> 

	<h1 align="center" style="margin-top: 40px">EXISTING DATA</h1>
	<div class="col-sm-6 col-xs-12">
		<h3 align="center" style="margin-top: 30px">RECO FUNDS </h3>
		<form id="form">
		<table class="table table-hover" id="table1">
	    <caption align="top" style="background-color:black;
        color: white;">EXISTING MUTUALFUNDS</caption> 
 <%
	Session hSession=null;  
	List<Object[]> rows=null;
	try
	{ 
		/* populating existing mutualfunds table from db */
		hSession=HibernateBridge.getSessionFactory().openSession();
		Query query3=hSession.createQuery("SELECT mf.name ,mf.id FROM MutualFund mf ,Reco blf where mf.id=blf.mutualFundId and blf.consumerId=? ORDER BY mf.name").setLong(0, consumerId);
		rows = query3.list();
		for(Object[] row :rows )
 		{
			String name=(row[0].toString());
			long mfid=Long.parseLong(row[1].toString());
		
			System.out.println(name);
			System.out.println(mfid);
		%>
		<tr>
		<td width="10%"><%=name%></td>
		</tr>
		<%
		} 
		%>
		
		</table>
		</form>
	</div>
		<div class="col-sm-6 col-xs-12">
			<h3 align="center" style="margin-top: 30px"> RECO STOCKS </h3>
			<form id="form">
			<table class="table table-hover" id="table2">
 			<caption align="top" style=" background-color: black;
    		color: white;">EXISTING STOCKS</caption>
 <%
 /* populating existing stocks table from db */
Query query=hSession.createQuery("SELECT sk.scName,sk.scCode FROM Stocks sk ,Reco mf where sk.scCode=mf.scCode and mf.consumerId=? ORDER BY sk.scName").setLong(0, consumerId);
List<Object[]> rows1 = query.list();
for(Object[] row :rows1 ){
		String name=(row[0].toString());
		int scCode=Integer.parseInt(row[1].toString());
		System.out.println(name);
		System.out.println(scCode); 
		%>
		 <tr>
		<td width="10%"><%=name%></td>
		</tr> 
		<%} 
		}catch(Exception e){
			e.printStackTrace();
		}
 finally{
	 hSession.close();
 }
		%>
		</table>
		</form>
	</div>
<div class="footer" style="position:fixed;left:0;bottom:0;width: 100%;">
 <div style="float:left">
<form align="left" name="form1" method="post" action="../link.jsp">
  <input name="prvious" type="submit" id="previous" class="btn btn-primary btn-sm" value="<<Previous Page">
</div>


</body>
</html>