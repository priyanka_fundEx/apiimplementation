<%@page import="com.fundexpert.dao.MutualFund"%>
<%@page import="com.fundexpert.dao.User"%>
<%@page import="com.fundexpert.dao.FundHouse"%>
<%@page import="com.fundexpert.controller.MutualFundController"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.*"%>

<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" import="java.sql.*" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html>
<%
	User user=null; 
	if(session.getAttribute("login")==null || session.getAttribute("login").equals("false"))
	{
		response.sendRedirect("../login.jsp");
		return;
	}
	else
	{
		user=(User)session.getAttribute("user");
	}
%> 
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		<!-- <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script> -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	   <body class="container-fluid">
	  <!-- if user is logged out then go to login page(sendRedirect to loginpage) -->
			
			 <div class="footer" style=" position: fixed;left: 0;bottom:0;width: 100%;">
                <div style="float:left">
                   <form align="left" name="form1" method="post" action="/data-fillup/link.jsp">
                   <input name="prvious" type="submit" id="previous" class="btn btn-primary btn-sm" value="<<Previous Page">
                   </form>
                </div>
            </div>
          <h1 align="center" style="margin-top: 10px">RECO FUNDS</h1>
            <div class="col-sm-6 col-xs-12">
		        <h3 align="center" style="margin-top: 5px">ADD FUNDS </h3>
		             <form id="form" >
			            <div class="form-group">
			            <label class="control-label">SELECT FUNDHOUSE</label>
			            <select  class="form-control" id="houseId" >
			            <option disabled="" value="" selected="">  SELECT fundhouse </option>
		                <%
		                /* populating dropdown from db(fundhouse table) */
							
							List<Long> List=null;
							try
							{
								MutualFundController mfc=new MutualFundController();
								
								List<FundHouse> list=mfc.getFundHouseList(); 
								Iterator<FundHouse> itr=list.iterator();  
								while(itr.hasNext())
								{
									FundHouse fh=itr.next();
						%>
		              	<option value="<%=fh.getId()%>"><%=fh.getName()%></option>
                        <%
                        		}
						%>
		  			  </select>
		              </div>
 				         <div class="form-group">
					     <label >SELECT MUTUALFUND</label>
					     <input type="text" class="form-control input-md ui-widget ui-autocomplete-input" id="search-funds" type="text" placeholder="Search Mutual Funds.." onfocus="this.value=''">
				         </div>
			 <table class="table table-hover" id="table">
			 <caption hidden id ="caption" align="top">Selected Funds</caption> 
			 </table>
             <input type="button" id="submit" class="btn btn-primary btn-sm" value="ADD FUNDS">
             <input hidden id="fundid" type="text">
		           </form>
	     </div> 
         <div class="col-sm-6 col-xs-12">
			<h3 align="center" style="margin-top: 5px">REMOVE FUNDS</h3>
			<h4 style="margin-top: 20px">REMOVE BY FUNDHOUSE</h4>
            <form class="form-inline">
                <div class="form-group">
                <label>SELECT FUNDHOUSE</label>
                <select  id="fhdelete" class="form-control">
                   <option disabled="" value="" selected="">  SELECT fundhouse </option>
                   <%/* populating dropdown from db(fundhouse table) so that user can delete by fundhouse  */
						  
						itr=list.iterator(); 
						while(itr.hasNext())
						{
							FundHouse fh1=itr.next();
				   %>
                  <option value="<%=fh1.getId()%>"><%=fh1.getName()%></option>
                     <% } %>
               </select>
               </div>
          <input type="button" style="font-size:9px" id="deletefh"  class="btn btn-primary btn-sm" value="REMOVE BY FUNDHOUSE">
          <h4 style="margin-top: 30px;display:inline-block">REMOVE All</h4>
          <input type="button" id="delete_all" class="btn btn-primary btn-sm" value="REMOVE ALL" style="display:inline-block"></td>  
          </form>
		  <table class="table table-hover" id="table2" >
		  <!-- <caption align="top">BLACKLIST TABLE</caption> -->
		    <thead>
		      <tr>
		        <th style="background-color:black;  color: white;"> EXISTING MUTUAL FUNDS</th>
		         <!--  <th style="background-color:black;  color: white;">Action</th>  -->
		     </tr>
		   </thead>
		    <tbody id="body1" style="height:300px; overflow-y:scroll; display:block">
            <%/*  populating table2 that is existing data in db*/
            /* storing db data in a List also */
		            List = new ArrayList(); 
		            List<MutualFund> rows = mfc.getRecoList(user.getConsumerId());
			        for(MutualFund row :rows )
			        {
						String name=row.getName();
						long mfid=row.getId();
					    System.out.println("MfId = "+mfid+" Name = "+name);
			            List.add(mfid); 
					%>
						<tr>
							<td width="90%"><%=name %> </td>
							<td width="10%"><button onclick="myFunction2(this)" class="btn btn-primary btn-sm" id="<%=mfid %>">REMOVE</button></td>
						</tr>
						
					<% 
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}

            %>
           </tbody>
         </table>
      </div>
    </body>
</html>

<script >
/*creating an array table3array and storing list into it  */
var table3= <%=List %>;
table3array=[];
table3array=table3;
console.log("list is"+table3);
console.log(table3array);
/* creating an empty array fundidarray which will store selected mutualfund and later send to servlet on click on submit button */
var fundidarray=[];
$(document).ready(function(){
	  	  /* on selecting fundhouse there will be an ajax call  */
	   $("#houseId").on("change",function()
			{
			 	$("#search-funds").val("");	
			    var houseId=$("#houseId").val();
				$.ajax({
					url: "/mf?action=getFundByHouseId",
					data: {houseId: houseId},
					method: "GET",
					dataType:"json",
			    	error: function () 
					{
			   	     	 alert("Server Down.");
					},
					success: function (data)
					{
						var success=data.success;
						console.log("success="+success);
						console.log(data);
						if(typeof success===undefined || success==false)
						{
							alert("Try after some time.");
						}
						else
						{
						/* autocomplete fundhouse */
							itemLabel = "";
							itemId = 0;
						/* mfData array coming from servlet as a jsonresponse */
					    	var mfs=$.map(data.mfData, function (obj)
					    	{
					            return {
					                label: obj.name,
					                id: obj.id
					                   };
					        });
			  	 			$('#search-funds').autocomplete({
		           	 			minLength: 2,
		           	 			delay: 0,
								source: function (request, response) 
								{
			    					a = $.ui.autocomplete.filter(mfs, request.term);
			            			response(a.slice(0,15));
		            			},
		    					focus: function(event, ui)
		    					{
			                		$('#search-funds').val(ui.item.label);
			                		return false;
		  				 		},
		            			// Once a value in the drop down list is selected, do the following:
		            			select: function(event, ui) 
		            			{
		                			// place the value into the textfield called 'search-funds'...
		                			itemLabel = $('#search-funds').val(ui.item.label);
		                			// and place the id into the hidden textfield 'fundid' 
		                			$('#fundid').val(ui.item.id);
		                			itemId = ui.item.id;
		                			console.log(itemId);
		                			console.log(itemLabel);
                        			var fundid=$("#fundid").val();
                        			console.log("table3array: ", table3array);
                        			console.log(fundid);
                        			/* if fundid is not in table3array(existing data in db) then add fundid in fundidarray and in #table(selected mutual fund before submit button) else already exists*/
                        			var idxDB = $.inArray(itemId, table3array);
                        			console.log(idxDB);
                        			if(idxDB == -1)
                        			{
            							var idx = $.inArray(fundid,  fundidarray);
    		               	 			if(idx == -1)
    		                			{
	    		                			fundidarray.push(fundid);
	    		                			console.log(fundidarray)
	    		               				console.log($('#search-funds').val()); 
	    		                    		$("#table").append("<tr><td>"+$("#search-funds").val()+"</td><td><button onclick='myFunction(this)' class='btn btn-primary btn-sm' id="+fundid+">delete</button></td></tr>");
	    		                   			document.getElementById("caption").style.display='block';
	    		               	 		}
    		               	 			else
	    		                		{
	    		                			alert("already selected select another one");
	    		                		}
    		               	 			$("#search-funds").val("");
                        			}
			                        else
			                        {
			                        	alert("Already exists in Reco List.");
			                        }
		                		}
		       		 			});
							}
						}
					});
		
				});
	
});  
  
</script>
<script>	
/* onclick remove button */
/*  removes mutual fund before submit button */
function myFunction(element)
{
	console.log("Remove single mutualfund.");
	var tableid= document.getElementById("table");
	$elem = $(element);
	console.log("element:",element);
	console.log("$element:",$elem);
	console.log($elem.parent());
	/* remove table row when click on remove button */
	$elem.parent().parent().remove();
	var fundidd=$elem.attr('id');
	/*if button id that is fundidd exists in fundidarray then remove it from array  */
	var idxx = $.inArray(fundidd,  fundidarray);
	if(idxx != -1)
	{
		fundidarray.splice(idxx, 1);
	}
}
/* on click submit button */
/* sending fundidarray to servlet to store in db */
$(document).ready(function()
			{ 
 		 		$("#submit").on("click",function()
				{
		        var houseId=$("#houseId").val();
				var fundid1=$("#fundid").val();
				//alert(fundidarray);
				if(houseId==null|fundid1==0|fundidarray==0)
				{
					alert("Select atleast one mutualfund to add.");
				}
				else
				{
					$.ajax({
					url: "/mf?action=saveReco",
					data: {fundidarray:fundidarray},
					method: "POST",
					dataType:"json",
					error: function () 
				    {
		   	         	alert("Network Error.");
				    }, 
					success: function (data)
					{  
						console.log(data.success);
						if(typeof data.success===undefined)
						{
							alert("Not able to persist Reco fund.");
						}
						if(data.success==false)
						{
							alert(data.error);	
						}
						else
						{
							window.location.reload();
						}
					}
				 	});
				}
			});
        }); //document closed.
/* onclick remove button of existing table */
/* ajax call to remove particular row from db*/
//in success of ajax call remove row from existing table and particular button id from table3array
function myFunction2(element)
{
	var table2= document.getElementById("table2");
	console.log("function called to remove single fund from DB.");
	$elem = $(element);
	console.log("element:",element);
	console.log("element.parent",$elem.parent());

  	var mfid=$elem.attr('id');
	if(confirm("Are you sure you want to remove?"))
	{
	  	$.ajax({
		url: "/mf?action=deleteSingleReco",
		data: {mfid:mfid},
		method: "POST",
		dataType:"json",
		success: function (data)
		{   
			console.log("success result after removing fund.:",data.success);
			if(typeof data.success===undefined || data.success==false)
			{
				alert("Not able to delete.");
			} 
			else
			{
				$elem.parent().parent().remove();
				var idxb = $.inArray(parseInt(mfid),table3array);
			 	console.log("index of mfid="+idxb);
			  	console.log("table 2 array is: "+table3array);
				if(idxb != -1)
				{
					 table3array.splice(idxb, 1);
					 console.log("removed 1 element from table3array at index = "+idxb);
				}
			}
		}
		});
	}
}
     
/* ajax call on deleteall button remove  all the data from db */
/* in success of ajax call remove all row from existing table(#table2) and and empty table3array*/
 $("#delete_all").on("click",function(e)
		{
			console.log("Delete all.");
			if(confirm("do you really want to delete all of the existing Reco Funds?"))
			{
				$.ajax({
	  				url: "/mf?action=deleteAllReco",
	  				method: "POST",
	  				dataType:"json",
	  				error: function () 
				    {
		   	         	alert("Network Error.");
				    }, 
	  				success: function (data)
	  				{
	  					
	  					var success=data.success;
	  					console.log("data.success",success);
	  					if(typeof success === undefined || !success)
	  					{
	  						alert("Not able to delete all Reco Funds.");
	  					}
	  					else
	  					{
	  						console.log("undefined or true");
							console.log($("#body1"));
							$("#body1").empty();
							table3array.length=0;
	  					}
				   		 e.preventDefault();
	  				 }
  					 });
				}
			});
/* on click removebyfundhouse button*/
/*  ajax call on remove by fundhouse,sending particular fundhouse id to servlet and removing all the mutual fund associated with that fundid*/
$("#deletefh").on("click",function()
{
	console.log("Delete by FundHouse.");
	var fhdelete =$("#fhdelete").val();
	if(fhdelete==null)
	{
		alert("Choose one fund house.");	
	}
	else
	{
		var t=confirm("Do you really want to delete all Reco Funds related to selected FundHouse?");
	    if(t==true)
	    {
	    	
			$.ajax({
				url: "/mf?action=deleteRecoByFundHouse",
				method: "POST",
				data:{fhdelete:fhdelete},
				dataType:"json",
				error:function()
				{
					alert("Network Error.");
				},
				success: function (data)
				{
 					console.log(data.success);
 					if(typeof data.success===undefined || data.success==false)
 					{
 						alert("Not able to delete.");
 					}
 					else
 					{
 						window.location.reload();
 					}
 				}
 			});
		}
	}
});
</script>
