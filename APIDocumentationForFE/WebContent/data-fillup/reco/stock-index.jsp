<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.fundexpert.dao.User"%>
<%@page import="com.fundexpert.dao.Stocks"%>
<%@page import="com.fundexpert.controller.StocksController"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.*"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<html>
<%
 User user=null;
 /*  if user is logged out then go to login page(sendRedirect to loginpage)*/
 if(session.getAttribute("login")==null || session.getAttribute("login").equals("false"))
 {
 	response.sendRedirect("../login.jsp");
 	return;
 }
 else if(session.getAttribute("user")==null)
 {
 	response.sendRedirect("../login.jsp");
 	return;
 }
 else
 {
	 user=(User)session.getAttribute("user");
 }
 if(user==null)
 {
	 response.sendRedirect("../login.jsp");
	 return;
 }
%>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
          
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		<!-- <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script> -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!--   <style>
         table tbody {overflow-y:scroll; height:300px; display:block;}
        </style> -->
		</head>
<body class="container-fluid">
<h1 align="center" style="margin-top: 10px">RECO STOCKS</h1>
 
  
   <div class="footer" style=" position: fixed;
	   left: 0;
	   bottom:0;
	   width: 100%;">
	   <div style="float:left">
		<form align="left" name="form1" method="post" action="../link.jsp">
		  <input name="prvious" type="submit" id="previous" class="btn btn-primary btn-sm" value="<<Previous Page">
		</form>
		</div>
   </div>
       <div class="col-sm-6 col-xs-12">
 
       <h3 align="center" style="margin-top: 5px">ADD STOCKS</h3>
       <!-- <h1 align="center" style="margin-top: 40px"></h1> -->
        <form id="form" >
	          <div class="form-group">
	           <label class="control-label">SEARCH STOCKS</label>
	           <input id="search_stock" type="text" class="form-control" placeholder="Search stocks" onfocus="this.value=''" >
	           <!-- onfocus=" this.value='' -->
	          </div>
       <table class="table table-hover table-condensed" id="table">
        <caption hidden id ="caption" align="top">Selected Funds</caption> 
       </table>
       <input type="button" id="submit" class="btn btn-primary btn-sm" value="ADD">

        <input hidden id="sc_code" type="text">
       </form>
      </div> 
<div class="col-sm-6 col-xs-12">
  <h3 align="center" style="margin-top:5px">REMOVE STOCKS</h3>
  <form class="form-inline">
     <h4 style="margin-top: 20px;display:inline-block" align="center">REMOVE All</h4>
     <input type="button" id="delete_all" class="btn btn-primary btn-sm" value="REMOVE ALL" style="display:inline-block"></td>  
  </form>
   <table class="table table-hover table-condensed" id="table2" >
   <thead>
    <tr>
    <th style="background-color:black;  color: white;" colspan="2">EXISTING STOCKS NAME</th>
    <!--  <th>Action</th>  -->
     </tr>
   </thead>
      <tbody id=body overflow-y:scroll; height:300px; display:block;>
  
<%
JSONArray stockData=null;
JSONObject stockJSON=new JSONObject();
List<String> table2list=null;
try{
	/* populating table2(existing table) from db */
	/* creating arraylist which is storing scCode of existing table */
	table2list = new ArrayList(); 
	StocksController stocksController=new StocksController();  
	List<Stocks> list2=stocksController.getRecoList(user.getConsumerId());
	List<Stocks> allStocks=stocksController.getAllStocks();
	stockData=new JSONArray(allStocks);
	stockJSON.put("stockData", stockData);
	//System.out.println("StockJSON="+stockJSON.toString(1));
	Iterator<Stocks> itr1=list2.iterator();  
	while(itr1.hasNext())
	{
		Stocks stock=itr1.next();
		if(stock!=null)
		{
			int scCode=stock.getScCode();
			table2list.add(Integer.toString(scCode));
			 %> 
				<tr>
			 	<td width="90%"><%=stock.getScName() %> </td>
			 	<td width="10%"><button onclick="myFunction2(this)" class="btn btn-primary btn-sm" id="<%=scCode %>" >REMOVE</button></td>
				</tr>  
		 	 <% 
		}
	}  
}
catch(Exception e)
{
	e.printStackTrace();
}
%>
</tbody>
</table>
</div>
</body>
</html>
<script>
/* creating an array table2array and storing table2list into it  */ 
var table2= <%=table2list %>;
var stockData=<%=stockJSON%>;
console.log(stockData);
table2array=[];
table2array=table2;
console.log("list is"+table2);
console.log(table2array);
/* creating an empty array stockidarray which will store selected stock and later send to servlet on click on submit button */
var stockidarray=[];
$(document).ready(function()
		{ 
	     
			/* jquery autocomplete */
			itemLabel = "";
			itemId = 0;
			/* stockdata array coming from servlet as a jsonresponse */
			var mfs=$.map(stockData.stockData, function (obj)
			{
   				   return {
         						label: obj.scName,
          						id: obj.scCode
                            };
      		});
		 	$('#search_stock').autocomplete({
     				 minLength: 2,
			source: function (request, response) 
			{
				a = $.ui.autocomplete.filter(mfs, request.term);
      				response(a.slice(0,10));	 
      		},
			focus: function(event, ui) 
			{
  				 $('#search_stock').val(ui.item.label);
  				 return false;
     		},
      		// Once a value in the drop down list is selected, do the following:
      		select: function(event, ui) 
		    {
          // place the  value into the textfield called 'search_stock'...
            // alert("hello");
             	itemLabel = $('#search_stock').val(ui.item.label);
             // and place the id into the hidden textfield called 'sc_code'. 
             	$('#sc_code').val(ui.item.id);
             	itemId = ui.item.id;
             	console.log("stockScCode=",itemId);
             	console.log(itemLabel);
                var sc_code=$("#sc_code").val();
                console.log("table2array: ", table2array);
                console.log(sc_code);
                /* if scCode is not in table2array(existing data in db) then add scCode in stockidarray and in #table(selected stocks before submit button) else already exists*/
                var idxDB = $.inArray(itemId, table2array);
             
                if(idxDB == -1)
                {
             		var idx = $.inArray(sc_code,stockidarray); 
					if(idx == -1)
					{
						stockidarray.push(sc_code);
						//alert(stockidarray);
						console.log("stock added in temporary list",$('#search_stock').val()); 
					    $("#table").append("<tr><td>"+$("#search_stock").val()+"</td><td><button onclick='myFunction(this)' class='btn btn-primary btn-sm' id="+sc_code+">REMOVE</button></td></tr>");
						document.getElementById("caption").style.display='block';
					}
	              	else
	              	{
	              		alert("already selected ,select another one.")
	              	}
				}
                else
                {
                	alert("already exsits in current Reco Funds.");
                }
			}
		 });
		 	
});
/* onclick remove button */
/*  removes scCode before submit button */
 function myFunction(element)
 {
	  var tableid= document.getElementById("table");
	  $elem = $(element);
	  console.log("element:",element);
	  console.log("$element:",$elem);
	  console.log($elem.parent());
	  /* remove table row when click on remove button */
	  $elem.parent().parent().remove();
	  var stockidd=$elem.attr('id');
	  /*if button id that is stockidd exists in stockidarray then remove it from array  */
	  var idxx = $.inArray(stockidd,stockidarray);
	  if(idxx != -1)
	  {
		  stockidarray.splice(idxx, 1);
	  }
 }
 /* onclick remove button of existing table */
 /* ajax call to remove particular row from db*/
 //in success of ajax call remove row from existing table and particular button id from table2array
function myFunction2(element)
{
	 console.log("MyFunction2");
	  var table2= document.getElementById("table2");
	  console.log("hey");
	  $elem = $(element);
	  console.log("element:",element);
	  console.log("$element:",$elem);
	  console.log($elem.parent());
	  
	 // $elem.parent().parent().remove();
	  var status = $elem.attr('id');
	  console.log(status);
      var scCode=$elem.attr('id');
   
        if(confirm("Are you sure you want to delete this stock from existing Reco?"))
        {
		     $.ajax({
				url: "/stocks?action=deleteSingleStockFromReco",
				data: {scCode:scCode},
				method: "POST",
				dataType:"json",
				success: function (data)
				{
					if(typeof data.success===undefined)
					{
						alert("Something failed.");
					} 
					else if(data.success==false)
					{
						alert(data.error);	
					}
					else
					{
						$elem.parent().parent().remove();
						var idxb = $.inArray(parseInt(status),table2array);
					    console.log("var is "+idxb);
					    console.log("scCode is"+scCode)
					    console.log("table 2 array is: "+table2array);
					    if(idxb != -1)
						{
							  table2array.splice(idxb, 1);
							  console.log("table2array"+idxb);
						}
					}
				}
			});
				
		}
 	}

		 
	        
</script>
<script>
/* on click submit button */
/* sending stockididarray to servlet to store in db */
$("#submit").on("click",function()
{
	var sc_code=$("#sc_code").val();
	//alert("while submitting stock scCode to backend = "+ stockidarray);
	if(sc_code==0|stockidarray==0)
	{
		alert("all fields are mandatory");
	}
	else
	{
		$.ajax({
		url: "/stocks?action=saveStockReco",
		data: {stockidarray:stockidarray},
		method: "POST",
		dataType:"json",
		success: function (data)
		{
			console.log("save stock reco response data = ",data);
			if(typeof data.success===undefined || data.success==false)
			{
				alert("Action not performed successfully.");
			}
			else
			{
				window.location.reload();
			}
		}
		});
	}
});
/* ajax call on deleteall button remove  all the data from db */
/* in success of ajax call remove all row from existing table(#table2) and empty table3array*/
 $("#delete_all").on("click",function()
		{
	var r=confirm("do you really want to delete the whole table?");
	if(r==true)
	{
		$.ajax({
			url: "/stocks?action=deleteAllStockFromReco",
			method: "POST",
			success: function (data)
			{
				console.log(data+" data.success=",data.success);
				if(typeof data.success===undefined || data.success==false)
				{
					alert("Delete action not performed.");
				}
				else
				{
					$("#body").remove();
					table2array.length=0;
				}
			}
			});
   	}
	
});
</script>