<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.fundexpert.dao.Stocks"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
 <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body class="container-fluid">
<%  Session session2=null;
    Transaction t=null;

try{
	
    session2=HibernateBridge.getSessionFactory().openSession();
    t=(Transaction) session2.beginTransaction();
	Query query=session2.createQuery("from Stocks order by scName");  
	List<Stocks> list=query.list();  
	  %>							
	
	<form class="form-inline" id="form" >
	
	  <div class="form-group">
 		 <label class="control-label">SELECT Stock</label>
		 <select  class="form-control" id="stockId" >
				<option disabled="" value="" selected="">  SELECT Stock </option>
		                <% 
		               	 
		                Iterator<Stocks> itr=list.iterator();    
						while(itr.hasNext())
							{
								Stocks st=itr.next();
						
		                %>
		                <option value="<%=st.getScCode()%>"><%=st.getScName()%></option>
		                
                         <% }%>
	
                        	
                        
                         
		  			  </select>
no of shares: <input type="text" class="control-label" name="no of shares" id=sharesText>
user ID: <input type="text" class="control-label" name="userId" id="userIdText">

<label class="control-label">SELECT Type</label>
<select class="form-control" id="type">
<option disabled="" value="" selected="">  SELECT TYPE </option>
<option value="purchased">purchased</option>
<option value="redeemed">Redeemed</option><br>
</select>              
  </div>
 <!--  </form>
  
  <form class="form-inline" id="form1" > -->
	
	  <div class="form-group">
 		 <label class="control-label">SELECT Stock</label>
		 <select  class="form-control" id="stockId1" >
				<option disabled="" value="" selected="">  SELECT Stock </option>
		                <% 
		               	 
		                Iterator<Stocks> itr1=list.iterator();     
						while(itr1.hasNext())
							{
								Stocks st1=itr1.next();
						
		                %>
		                <option value="<%=st1.getScCode()%>"><%=st1.getScName()%></option>
		                
                         <% }%>
	
                        	
                        
                         
		  </select>
no of shares: <input type="text" class="control-label" name="no of shares" id=sharesText1>
user ID: <input type="text" class="control-label" name="userId" id="userIdText1">  
<label class="control-label">SELECT Type</label>
<select class="form-control" id="type1">
<option disabled="" value="" selected="">  SELECT TYPE </option>
<option value="purchased">purchased</option>
<option value="redeemed">Redeemed</option><br>
</select>
          
  </div>
  <!-- </form>
  
  <form class="form-inline" id="form2" > -->
	
	  <div class="form-group">
 		 <label class="control-label">SELECT Stock</label>
		 <select  class="form-control" id="stockId2" >
				<option disabled="" value="" selected="">  SELECT Stock </option>
		                <% 
		               	 
		                Iterator<Stocks> itr2=list.iterator();       
						while(itr2.hasNext())
							{
								Stocks st2=itr2.next();
						
		                %>
		                <option value="<%=st2.getScCode()%>"><%=st2.getScName()%></option>
		                
                         <% }%>
	
                        	
                        
                         
		  </select>
no of shares: <input type="text" class="control-label" name="no of shares" id=sharesText2>
user ID: <input type="text" class="control-label" name="userId" id="userIdText2"> 
<label class="control-label">SELECT Type</label>
<select class="form-control" id="type2">
<option disabled="" value="" selected="">  SELECT TYPE </option>
<option value="purchased">purchased</option>
<option value="redeemed">Redeemed</option><br>
</select>             
  </div>
  <!-- </form>
  
  <form class="form-inline" id="form3" > -->
	
	  <div class="form-group">
 		 <label class="control-label">SELECT Stock</label>
		 <select  class="form-control" id="stockId3" >
				<option disabled="" value="" selected="">  SELECT Stock </option>
		                <% 
		               	 
		                Iterator<Stocks> itr3=list.iterator();          
						while(itr3.hasNext())
							{
								Stocks st3=itr3.next();
						
		                %>
		                <option value="<%=st3.getScCode()%>"><%=st3.getScName()%></option>
		                
                         <% }%>
	
                        	
                        
                         
		 </select>
no of shares: <input type="text" class="control-label" name="no of shares" id=sharesText3>
user ID: <input type="text" class="control-label" name="userId" id="userIdText3">  
<label class="control-label">SELECT Type</label>
<select class="form-control" id="type3">

<option disabled="" value="" selected="">  SELECT TYPE </option>
<option value="purchased">purchased</option>
<option value="redeemed">Redeemed</option><br>
</select>          
  </div>
 <!--  </form>
  
  <form class="form-inline" id="form4" > -->
	
	  <div class="form-group">
 		 <label class="control-label">SELECT Stock</label>
		 <select  class="form-control" id="stockId4" >
				<option disabled="" value="" selected="">  SELECT Stock </option>
		                <% 
		               	 
		                Iterator<Stocks> itr4=list.iterator();      
						while(itr4.hasNext())
							{
								Stocks st4=itr4.next();
						
		                %>
		                <option value="<%=st4.getScCode()%>"><%=st4.getScName()%></option>
		                 
                         <% }%>
	
                        	
                        
                         
		  			  </select>
no of shares: <input type="text" class="control-label" name="no of shares" id=sharesText4>
user ID: <input type="text" class="control-label" name="userId" id="userIdText4">
<label class="control-label">SELECT Type</label> 
<select class="form-control" id="type4">
<option disabled="" value="" selected="">  SELECT TYPE </option>
<option value="purchased">purchased</option>
<option value="redeemed">Redeemed</option>
</select>             
  </div>
  </form>
  <% 
}
catch(Exception e)
{
e.printStackTrace();
}finally{
session2.close();
}%>	
	               

<input type="button" id="submit" class="btn btn-primary btn-sm" value="add" onclick='myFunction()'>




</body>
</html>
	
 <script>
function myFunction()
{
	  console.log("submit function");
	  var userId=$("#userIdText").val();
	  var scCode=$("#stockId").val();
	  var noOfShares=$("#sharesText").val();
	  var type=$("#type").val();
	  
	  var userId1=$("#userIdText1").val();
	  var scCode1=$("#stockId1").val();
	  var noOfShares1=$("#sharesText1").val();
	  var type1=$("#type1").val();
	  
	  var userId2=$("#userIdText2").val();
	  var scCode2=$("#stockId2").val();
	  var noOfShares2=$("#sharesText2").val();
	  var type2=$("#type2").val();
	  
	  var userId3=$("#userIdText3").val();
	  var scCode3=$("#stockId3").val();
	  var noOfShares3=$("#sharesText3").val();
	  var type3=$("#type3").val();
	  
	  var userId4=$("#userIdText4").val();
	  var scCode4=$("#stockId4").val();
	  var noOfShares4=$("#sharesText4").val();
	  var type4=$("#type4").val();
	  
	  
	  var sendingArray=[
		  {userId:userId,scCode:scCode,noOfShares:noOfShares,type:type},
		  {userId:userId1,scCode:scCode1,noOfShares:noOfShares1,type:type1},
		  {userId:userId2,scCode:scCode2,noOfShares:noOfShares2,type:type2},
		  {userId:userId3,scCode:scCode3,noOfShares:noOfShares3,type:type3},
		  {userId:userId4,scCode:scCode4,noOfShares:noOfShares4,type:type4}];
	  
	  
	  var s={s:sendingArray}
	  console.log(sendingArray);
	  console.log(s);
	var array1=[];
	if(scCode!=null){
	array1.push(scCode);}
	if(scCode1!=null){
	array1.push(scCode1);}
	if(scCode2!=null){
	array1.push(scCode2);}
	if(scCode3!=null){
	array1.push(scCode3);}
	if(scCode4!=null){
	array1.push(scCode4);}
	console.log(array1);
	
	
	  
	  
	  $.ajax({
			url: "AddStocksInPortfolio",
			data:{ array1:array1,s:s,userId:userId,scCode:scCode,noOfShares:noOfShares,type:type,userId1:userId1,scCode1:scCode1,noOfShares1:noOfShares1,type1:type1,userId2:userId2,scCode2:scCode2,noOfShares2:noOfShares2,type2:type2,userId3:userId3,scCode3:scCode3,noOfShares3:noOfShares3,type3:type3,userId4:userId4,scCode4:scCode4,noOfShares4:noOfShares4,type4:type4}, 
			
			method: "POST",
			dataType:"json",
			success: function (data)
			{  
				
				if(data.success==false)
				{
					alert("exception in servlet");
				}
				else
				{
					window.location.reload();
				}
				
			}
				 	});
	 
	
}


</script> 