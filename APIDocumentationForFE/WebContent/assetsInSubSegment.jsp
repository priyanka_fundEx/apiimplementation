<%@page import="com.fundexpert.dao.AssetsInSubSegment"%>
<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.fundexpert.dao.FundHouse"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		
</head>
<body class="container-fluid"> 
<div style="position: fixed; top:0; left:0; width: 100%; background-color:black; display: block;   color: white;
 text-align: center;"> welcome</div>
<h1 align="center" style="margin-top: 20px">ADD STOCKS AND Funds</h1>
   
<div class="col-sm-8 col-xs-12" >
   		 <h3 align="center" style="margin-top: 10px">ADD</h3>
	   		 <div class="row">
	   		 	<div class="col-sm-12 col-xs-12" >
		   		 	<!-- <div class="col-sm-3" id="radioDivId">  -->
		   		 	  <div class="form-group">
		   		 	  	<label class="control-label"  style="margin-left: 0px">Funds or Stocks</label>
				    	<input type="radio" name="radioButton1" value="1" />Funds
			            <input type="radio"name="radioButton1" value="2" />Stocks<br>
		   		 	  </div>
		   		 	<!-- </div> -->
		   		 	
			 </div>
   		 </div>
		 <div class="row">
	   		 	<div class="col-sm-12 col-xs-12"  >
	   		 	<div class="col-sm-3" id="fundHouseDivId" style="display:none;">	 
	   		 		 	<div class="form-group">          
		    				<label class="control-label">Fundhouse</label>
    						<select  class="form-control" id="fundHouseId"  >
					    		    <option disabled="" value="" selected="" > SELECT </option>
						    		<%
			                
										 Session hSession=null;  
						    	       	

						    		/* 	Session session2=null;   */
										
										
										try{
										/* session2=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession(); */
										 hSession= HibernateBridge.getSessionFactory().openSession(); 
										Query query= hSession.createQuery("from FundHouse order by name");  
										List<FundHouse> list=query.list();  
										Iterator<FundHouse> itr=list.iterator();  
											while(itr.hasNext())
											{
											FundHouse fh=itr.next();
											%>
						             		<option value="<%=fh.getId()%>"><%=fh.getName()%></option>
					                        <% 
					                        } 
											%>
										
		  			  
		   					</select>
					 	 </div>	
					</div>	
	   		 		<div class="col-sm-2" id="subSegmentDivId">	 
	   		 		 	<div class="form-group">          
		    				<label class="control-label">Subsegment</label>
    						<select  class="form-control" id="subSegmentId"  >
					    		<option disabled="" value="" selected="" > SELECT </option>
					    		<option value="1" >LARGE CAP</option>
					    		<option value="2" >MID CAP</option>
					    		<option value="3" >SMALL CAP</option>
					    		<option value="4" >LONG TERM</option>
					    		<option value="5" >SHORT TERM</option>
		   					</select>
					 	 </div>	
					</div>	
						<div class="col-sm-3" id="riskProfileDivId">	 
		   		 		 	<div class="form-group">     		   					
					   			 <label class="control-label" >Risk Profile Number</label>
			            		 <input type="text" class="form-control"   size="5" id=riskProfile onkeypress="return isNumber(event)">
			            	</div>
		            	</div>
		            	<!-- <div class="col-sm-2" id="allocationDivId">	 
		   		 		 	<div class="form-group">
		   		 		 		 <label class="control-label" >Allocation</label>
		            			<input type="text" class="form-control"  size="5" id="allocation" onkeypress="return isNumber(event)">
		   		 		 	</div>
		   		 		</div>   --> 	 
		         </div>
		  </div>
		  <table class="table table-hover table-condensed" id="selectedStockTable">
                        <tbody  id="selectedStockTableBody"></tbody>
                </table>  
                <input type="button" id="submit" class="btn btn-primary btn-sm" style="margin-left: 25px"  value="submit" onclick='myFunctionSubmit()'><br>
                <table class="table table-hover table-condensed" id="sectorTable">
		         	    <tbody id="sectorTableBody" style="height:300px; width:300px; overflow-y:scroll; display:hidden">	   		
		                </tbody>
		        </table>
</div>
		         
	    		
    		
    	
    	<div class="col-sm-4 col-xs-12">
    	<h3 align="center" style="margin-top: 10px">View</h3>
	   	  <div class="row">
	   		 	<div class="col-sm-12 col-xs-12" >
		   		 	<!-- <div class="col-sm-3" id="radioDivId">  -->
		   		 	  <div class="form-group">
		   		 	  	<label class="control-label"  style="margin-left: 0px">Funds or Stocks</label>
				    	<input type="radio" name="radioButton2" value="1" />Funds
			            <input type="radio"name="radioButton2" value="2" />Stocks<br>
		   		 	  </div>
		   		 	<!-- </div> -->
		   		 	
			 </div>
   		 </div>
   		 <div class="row">
   		 	<div class="col-sm-12 col-xs-12" >
   		 		
				<div class="col-sm-5" id="riskProfileDivId2">	 
   		 		 	<div class="form-group">     		   					
			   			 <label class="control-label" >Risk Profile Number</label>
	            		 <select  class="form-control" id="riskProfile2Id"  >
	            		 <option disabled="" value="" selected="" > SELECT </option>
	            		 <% 
					    	/* Query query2= hSession.createQuery("select distinct riskProfile from AssetsInSubSegment where consumerId=2 and assetType=2");  
			            		
			         	    List<Integer> list1=query2.list();
			         	    for (int i = 0; i < list1.size(); i++) {
			       			System.out.println(list1.get(i)); */
			       			%>
		         	    	<%-- <option value="<%=list1.get(i)%>"><%=list1.get(i)%></option> --%>
		         	    	<%
							//}
		         	  	    %>
			         	   
			         	    	
			         	  
							</select>
							<% 
										}
										catch(Exception e){
											e.printStackTrace();
										}
										finally{
										 hSession.close();
											
										}
				                        %> 
			         			
										
											
	            	</div>
            	</div>
            	<div class="col-sm-4" id="submitDivId">	 
   		 		 	<div class="form-group"> 
   		 		 		<input type="button" id="viewId" class="btn btn-primary btn-sm" style="margin-top:25px"  value="view" onclick='myFunctionView()'><br>
   		 		 	</div>
   		 		</div>
   		 		 <table class="table table-hover table-condensed" id="viewTable">
		         	    <tbody id="viewTableBody" style="height:300px; width:300px; overflow-y:scroll; display:hidden">	   		
		                </tbody>
		        </table>
   		 	
   		 	</div>
   		 
   		 </div>
    	 
    	</div>
    	

</body>
</html>

<script>

	

dataArray=[];

var fundHouseId=0;
$(document).ready(function()
		{
	
	 $('input[name="radioButton2"]').on("click", function(){
		
		var assetType=0;
		if($('input[name=radioButton2]:checked').val()==1 )
	    {
			assetType=1;
	    }
		if($('input[name=radioButton2]:checked').val()==2 )
	    {
			assetType=2;
	    }
		 
		$.ajax({
		    url:"StocksServlet?method=getViewRiskProfile",
		    data:{assetType:assetType},
			method: "POST",
			dataType:"json",
			
			success: function (data)
			{
				if(data.success==false)
					{
						
						alert(data.error);
						console.log(data.error);
						
					}
				
		       if(data.success==true)
					{
		    	     
		    	      console.log(data.riskProfileArray);
		    	      for(var i=0;i<data.riskProfileArray.length;i++)
		    	      {
		    	   		   $("#riskProfile2Id").append("<option value="+data.riskProfileArray[i].riskProfile+" >"+data.riskProfileArray[i].riskProfile+"</option>");
		    	   		 
					  }
					}				
			}		
		
	});
	

 }); 
	        
	        
			$('input[name="radioButton1"]').on("click", function()
			{
				dataArray.length=0;
			    $("#selectedStockTableBody").html("");
			    $("#sectorTableBody").html("");
			    $("#sectorTableBody").hide();
			    if($('input[name=radioButton1]:checked').val()==1 )
			    {
			    	$("#fundHouseDivId").show();
			    	 
			    }else
			    	{
			    		$("#fundHouseDivId").hide();
			    	}
			   
			});
			 $("#fundHouseId").on("change",function()
			{
				// dataArray.length=0;
				 //$("#selectedStockTableBody").html("");
				 $("#sectorTableBody").html("");
				 $("#sectorTableBody").hide();
				 fundHouseId=$("#fundHouseId").val();
				 $("#subSegmentId").val('');
		 		 console.log("fundHouseId  is",fundHouseId); 
			});
	 	   $("#subSegmentId").on("change",function()
			{
	 		    //dataArray.length=0;
	 		    //$("#selectedStockTableBody").html("");
		 		var radioId =$('input[name=radioButton1]:checked').val(); 
		 	    console.log("radio id  is",radioId);
		 	    
		 	   
	 		    
		 	    if(typeof radioId=="undefined")
		 	    {
		         alert("choose fund or stock");
		         $("#subSegmentId").val('');
		    	 }
		 	     if(radioId==1)
		 	     {
		 	    	     console.log("radio id  is",radioId);
		 	    	   if(typeof fundHouseId!="undefined"&& fundHouseId!=null&&fundHouseId!=0&&fundHouseId!="")
					  	 {
			 		    
					  
							  	 var  optionText = $('option:selected', this).text();
							  
							  	 console.log("option  is",optionText);
							  	 $.ajax({
							    	url:"FundsServlet?method=getFunds",
									method: "POST",
									dataType:"json",
									data:{optionText:optionText,fundHouseId:fundHouseId},
									success: function (data)
									{
										if(data.success==false||data.success=="")
											{
												alert(data.error);
												console.log("data.error",data.error);
												$("#sectorTableBody").html("");
												$("#sectorTableBody").hide();
											}
										
								       else 
											{
								    	  /*  alert("hey"); */
								    	 
								    	   $("#sectorTableBody").html("");
								    	 
								    	   
								    	   $.each(data.success, function(key, item) 
								    			   {
								    			      console.log("scNname",item.id);
								    			      console.log("scCode",item.name);
								    			     
								    			      $("#sectorTableBody").append("<tr><td>"+item.name+"</td><td ><button onclick='add(this)' class='btn btn-primary btn-sm'  id="+item.id+">add</button></td></tr>"); 
								    			      document.getElementById("sectorTableBody").style.display='block';
								    			   });
											}				
									}		
							
						     });
					  	 }
		 	    	   else{
		 	    		   alert("select fundhouse");
		 	    		  $("#subSegmentId").val('');
		 	    	   }
		 	     }
		 	     if(radioId==2){
		 	        console.log("radio id  is",radioId);
				  	 var subSegmentId=$("#subSegmentId").val();
				  
				  	 var optionText = $('option:selected', this).text();
				  
				  	 console.log("option  is",optionText);
				  	 $.ajax({
				    	url:"StocksServlet?method=getStocksOfType",
						method: "POST",
						dataType:"json",
						data:{optionText:optionText},
						success: function (data)
						{
							if(data.success==false||data.success=="")
								{
								if(data.success==""){
									alert("data does not exists choose another type");
									 $("#sectorTableBody").html("");
									    $("#sectorTableBody").hide();
									
								}else
									alert("exception in servlet",data.error);
								    console.log("exception in servlet",data.error);
									
								}
							
					       else 
								{
					    	   
					    	 
					    	   $("#sectorTableBody").html("");
					    	   
					    	   $.each(data.success, function(key, item) 
					    			   {
					    			      console.log("scName",item.scName);
					    			      console.log("scCode",item.scCode);
					    			     
					    			      $("#sectorTableBody").append("<tr><td>"+item.scName+"</td><td ><button onclick='add(this)' class='btn btn-primary btn-sm'  id="+item.scCode+">add</button></td></tr>"); 
					    			      document.getElementById("sectorTableBody").style.display='block';
					    			   });
								}				
						}		
				
			     });
		 	     } 
			});
	 	
		
	});  
 
 function add(element)
{
	 $elem = $(element);
	 $elem.parent().parent().remove();		  
	 var movedRowId=$elem.attr('id');
	 var movedRowName= $elem.closest('tr').find('td:eq(0)').text();
	 console.log("movedRowName",movedRowName);
	 $("#selectedStockTableBody").append("<tr><td>"+movedRowName+"</td><td ><button onclick='deleteFunction(this)' class='btn btn-primary btn-sm'  id="+ movedRowId+">Delete</button></td></tr>");
	 dataArray.push(movedRowId);
	 
	 console.log("stocksDataArray is",dataArray);
	 
}
function deleteFunction(element)
{
	 $elem = $(element);
	 $elem.parent().parent().remove();
	 var deleteId=$elem.attr('id');
	 console.log("in delete function before deleting array is",dataArray)
	 var idxb = $.inArray(deleteId,dataArray);
	 console.log("var is "+idxb);
	
	  if(idxb == -1)
	  {
		  
	  }
	  else
	  {
		  dataArray.splice(idxb, 1);
		  console.log("after deleting array is: "+dataArray);
		  console.log("index is "+idxb);
	  }
} 
function myFunctionSubmit()
{
	var riskProfile  =$("#riskProfile").val();
	
	//var allocation =$("#allocation").val();
	var radioId =$('input[name=radioButton1]:checked').val(); 
    var subSegmentId = $('#subSegmentId option:selected').text();
	//console.log("allocation",allocation);
	console.log("riskProfile",riskProfile);
	console.log("DataArray.length",dataArray.length);
	console.log("dataArray",dataArray);
	console.log("subSegmentId",subSegmentId);
	console.log("radioId",radioId);

	var obj={};
	obj.dataArray=dataArray;
	obj.riskProfile=riskProfile;
	obj.subSegmentId =subSegmentId ;
	//obj.allocation=allocation;
	

	console.log("obj is",obj);
	
	if(dataArray.length==0){
		alert("choose atleast one fund or stock ");
		return;
	}
	else if(typeof radioId!="undefined"&&typeof riskProfile!="undefined"&&dataArray.length!=0 && $("#riskProfile").val()!=''&& subSegmentId!=null)
	{
		if(radioId==2)
		{
			
			$.ajax({
	    	    url:"StocksServlet?method=assestsSubmit",
	    	    data:obj,
				method: "POST",
				dataType:"json",
				
				success: function (data)
				{
					if(data.success==false)
						{
							alert("exception in servlet");
							
						}
					
			       else
						{
			    	      window.location.reload(); 
						}				
				}		
		
	     });
		}
		if(radioId==1)
			{
			$.ajax({
	    	    url:"FundsServlet?method=assestsSubmit",
	    	    data:obj,
				method: "POST",
				dataType:"json",
				
				success: function (data)
				{
					if(data.success==false)
						{
							alert("exception in servlet");
							
						}
					
			       else
						{
			    	      window.location.reload(); 
						}				
				}		
		
	     });
			}
	}else
	{
		
		alert("all fields are mandatory");
		
	
		
	} 
}
function myFunctionView()
{
	  $("#viewTableBody").html(" "); 
	  $("#viewTableBody").empty(); 
	  
	  
    var riskProfile  =$("#riskProfile2Id").val();
	
	
	var viewradioId =$('input[name=radioButton2]:checked').val();  
	if(typeof viewradioId=="undefined"|| viewradioId==null|| viewradioId=="")
	{
		alert("select stocks or funds");
	}
	else if(typeof riskProfile=="undefined"|| riskProfile==null|| riskProfile=="")
	{
		alert("select risk profile");
	}else
	{
		if( viewradioId==2)
	     {
			$.ajax({
	    	    url:"StocksServlet?method=getViewStocks",
	    	    data:{riskProfile:riskProfile,viewradioId:viewradioId},
				method: "POST",
				dataType:"json",
				
				success: function (data)
				{
					if(data.success==true)
						{
						  console.log("data is",data);
						  alert("success");
						  for(var i=0;i<data.view.length;i++)
						  {
							  console.log("data.view[i].scName",data.view[i].scName);
							  console.log("data.view[i].assetId",data.view[i].assetId);
							  console.log("data.view[i].segment",data.view[i].segment);
							  document.getElementById("viewTableBody").style.display='block';
							
							  $("#viewTableBody").append("<tr><td>"+data.view[i].scName+"</td><td>"+data.view[i].segment +"</td><td ><button onclick='viewDeleteFunction(this)' class='btn btn-primary btn-sm'  id="+data.view[i].assetId+">Delete</button></td></tr>");
						  }
							
						}
					
			       else
						{
			    	   alert("data.error");
						}				
				}		
		
	     });
	     }
		if( viewradioId==1)
	     {
			$.ajax({
	    	    url:"FundsServlet?method=getViewFunds",
	    	    data:{riskProfile:riskProfile,viewradioId:viewradioId},
				method: "POST",
				dataType:"json",
				
				success: function (data)
				{
					if(data.success==true)
						{
						  console.log("data is",data);
						  alert("success");
						  for(var i=0;i<data.view.length;i++)
						  {
							  console.log("data.view[i].name",data.view[i].name);
							  console.log("data.view[i].assetId",data.view[i].assetId);
							  console.log("data.view[i].segment",data.view[i].segment);
							  document.getElementById("viewTableBody").style.display='block';
							
							  $("#viewTableBody").append("<tr><td>"+data.view[i].name+"</td><td>"+data.view[i].segment +"</td><td ><button onclick='viewDeleteFunction(this)' class='btn btn-primary btn-sm'  id="+data.view[i].assetId+">Delete</button></td></tr>");
						  }
							
						}
					
			       else
						{
			    	   alert("data.error");
						}				
				}		
		
	     });
	     }
	}
}
function viewDeleteFunction(element)
{
	 $elem = $(element);
	 var deleteId=$elem.attr('id');
	 console.log("deleteId",deleteId);
	 if($('input[name=radioButton2]:checked').val()==2)
	 {
		 $.ajax({
	 	    url:"StocksServlet?method=deleteView",
	 	    data:{assetId:deleteId},
				method: "POST",
				dataType:"json",
				
				success: function (data)
				{
					if(data.success==true)
						{
						  console.log("data is",data);
						  alert("success");
						  
						   $elem.parent().parent().remove(); 
						}
					
			       else
						{
			    	   		alert("error",data.error);
						}				
				}		
		
	  });
	 }
	 if($('input[name=radioButton2]:checked').val()==1)
	 {
		 $.ajax({
		 	    url:"FundsServlet?method=deleteView",
		 	    data:{assetId:deleteId},
					method: "POST",
					dataType:"json",
					
					success: function (data)
					{
						if(data.success==true)
							{
							  console.log("data is",data);
							  alert("success");
							  
							   $elem.parent().parent().remove(); 
							}
						
				       else
							{
				    	   		alert("error",data.error);
							}				
					}		 
		 });
    
	 }	  
	 
} 
function isNumber(evt) {
	console.log("isNum");
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>