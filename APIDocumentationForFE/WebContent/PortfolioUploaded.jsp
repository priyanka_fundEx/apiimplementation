<%@page import="com.fundexpert.config.Config"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URLConnection"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.net.URL"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <meta http-equiv="refresh" content="10"> -->
<title>Recent PF Upload</title>
</head>
<body>
<%
		Config configuration=new Config();
		//Make a request to app/portfolioreqest?action=getRequest to get Users with PortfolioRequest
		String urlPath;
		if(configuration.getProperty(Config.ENVIRONMENT).equals(Config.DEVELOPMENT))
		{
			//urlPath="https://www.fundexpert.in/app/portfoliorequest?action=getRequest";
			urlPath="http://localhost:8081/APIDocumentationForFE/app/portfoliorequest?action=getAllRequest";
		}
		else
		{
			urlPath="http://api.fundexpert.in/app/portfoliorequest?action=getAllRequest";
		}
		URL url = new URL(urlPath);
		URLConnection con=url.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setConnectTimeout(500000);
		con.setReadTimeout(500000);
		System.out.println("After opening connection get request for Portfolio on API server.");
	
		BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
		String line=br.readLine();
	
		JSONObject json=null;
		StringBuilder builder=new StringBuilder();
		while(line!=null)
		{
			builder.append(line);
			line=br.readLine();
		}
		br.close();
		System.out.print("Received JSON size="+builder.toString());
		json=new JSONObject(builder.toString());
%>

<div id="portfoliorequests">
</div>
<script src="assets/plugins/jquery.min.js"></script>
<script>
$(document).ready(function(){
	var json=<%=json.toString(1)%>
	if(json.success)
	{
		console.log("Inside success");
		var array=json.pr;
		for(var j=0;j<array.length;j++)
		{
			var jsonObject=array[j];
			var emailId=jsonObject.email;
			var userId=jsonObject.userId;
			var pan=jsonObject.panNumber;
			var submitted=jsonObject.submitted;
			var prDiv=document.getElementById("portfoliorequests");
			var button=document.createElement("BUTTON");
			var breakElement=document.createElement("BR");
			button.setAttribute("value", pan);
			button.setAttribute("id", j);
			var email=document.createTextNode(emailId);
			button.appendChild(email);
			prDiv.appendChild(button);
			
			var checkBox=document.createElement("INPUT");
			checkBox.setAttribute("type","checkbox");
			checkBox.setAttribute("name", "isRequestSubmitted");
			if(submitted)
				checkBox.checked=true;
			var label = document.createElement('label')
			label.htmlFor = "id";
			label.appendChild(document.createTextNode('submitted'));
			prDiv.appendChild(checkBox);
			prDiv.appendChild(label);
			prDiv.appendChild(breakElement);
			
			
			
			var text='{"appId":"e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc","secretKey":"ef7p8lcgnl2nnffp2iln"}';
			
		    
		    var obj = {
		    		appId : 'e7b781a46a1989577c437ba1dd1aa5c3dcd53ccc',
		    		secretKey: 'ef7p8lcgnl2nnffp2iln'
		    }
		    
		    obj = JSON.stringify(obj);
		    var mainData;
		    console.log(obj);
		    var url='rest/consumer/login';
		    var url1='rest/request/GetMFPortfolioFromPDF';
		    var url2='rest/request/GetStockPortfolioFromPDF';
		    if(submitted)
		    {
			    getData(url,url1);
			    
			    console.log("mainData2"+mainData);
			    var textArea = document.createElement("textarea");
			 	textArea.cols = "100";
			 	textArea.rows = "20";
			 	textArea.maxLength="8000";
			 	textArea.value = mainData;
			 	prDiv.appendChild(textArea);
			 	prDiv.appendChild(breakElement);
			 	prDiv.appendChild(breakElement);
			}
		    getStockData(url1,url2);
		}
	}
function getData(url,url1)
{
	$.ajax(
	{
			url: url,
			type: 'POST',
			data: obj,
			async: false,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
		   	success: function(data)
		   	{
		   		console.log(data);
			   	if(data.success)
				{
			   		var sessionId=data.sessionId;
			   		console.log("EmailID:"+emailId);
			   		var obj = 
			   		{
				    		appId : 'portfolio',
				    		sessionId : data.sessionId,
				    		emailId : emailId,
				    		pan : pan,
				    		mutualfundPDFPath : 'C:/opt/mutualfundpdfpath/37531505201808025861830681457090.pdf'
				    }
				    
				    obj = JSON.stringify(obj);
				   	$.ajax({
						url: url1,
						type: 'POST',
						data: obj,
						async : false,
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						   success: function(data) {
						   mainData=JSON.stringify(data);
						   console.log("mainData1"+mainData);
					   	},
					   	error: function(data) {
						   mainData=JSON.stringify(data);
						   console.log("Error Occurred while fetching Portfolio:"+data);
					     	
					   }
					});
				}
			   	else
				{
			   		mainData=JSON.stringify(data);
					console.log("Error Occurred while trying to LOGIN:"+data);   
			   	}
		   },
		   error: function(data) {
			   
			   console.log("No Successful Ajax Call: "+data);
		   }
		}
	);
}
/* function getStockData(url,url1)
{
	$.ajax(
	{
		url: url,
		type: 'POST',
		data: obj,
		async: false,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
	   	success: function(data)
	   	{
	   		console.log("loginSuccess"+data);
		   	if(data.success)
			{
		   		var sessionId=data.sessionId;
		   		console.log("EmailID:"+emailId);
		   		var obj = 
		   		{
			    		appId : 'portfolio',
			    		sessionId : data.sessionId,
			    		emailId : emailId,
			    		pan : pan,
			    }
			    
			    obj = JSON.stringify(obj);
			   	$.ajax({
					url: url1,
					type: 'POST',
					data: obj,
					async : false,
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					   success: function(data) {
					   mainData=JSON.stringify(data);
					   console.log("mainData1"+mainData);
				   	},
				   	error: function(data) {
					   mainData=JSON.stringify(data);
					   console.log("Error Occurred while fetching Portfolio:"+data);
				     	
				   }
				});
			}
	   		}
	   	});
	}*/
}); 
</script>
</body>
</html>