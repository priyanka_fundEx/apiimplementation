<%@page import="com.fundexpert.controller.NavController"%>
<%@page import="com.fundexpert.controller.MutualFundController"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>UpdateMF/NAV</title>
</head>
<body>
<h1>To be run everytime scheme master/Nav is uploaded on fundexpert</h1>
<p>Four Task will be executed after submitting password :</p>
<ul>
	<li>Adding new MF.</li>
	<li>Updation of existing MF for change in field values(Changes will reflect only if this process if run once per day).</li>
	<li>Adding all Navs for new MF.</li>
	<li>Updating existing NAV, after last updation.</li>
</ul>
<ol>
	<li>Process which is not included in this task is updating missing nav's of all MF.Click below to access that.</li>
	<li><a href="addMissingNav.jsp">Click Here</a></li>
</ol>
<ol>
	<li>You can close the page since it will take much time.To view the status of each process, corresponding to date, click below link.</li>
	<li><a href="mfStatus.jsp">Click Here</a></li>
</ol>
<form name="form" action="syncMfNav.jsp"  method="post">
<input type="password" name="pass"/>
<input type="submit" name="submit" value="Submit"/>
</form>
<%
	try
	{
		if(request.getParameterMap().containsKey("submit") && request.getParameter("submit").equals("Submit"))
		{
			final String p="fundexpertfeapi";
			
			if(!request.getParameterMap().containsKey("pass") || !request.getParameter("pass").equals(p))
			{
				out.write("Password invalid/empty.");
			}
			
			String receivedP=request.getParameter("pass");
			if(receivedP.equals(p))
			{
				MutualFundController updateFund=new MutualFundController();
				NavController navController=new NavController();
				try
				{
					updateFund.addNewMutualFund();
				}
				catch(Exception e)
				{
					e.printStackTrace(response.getWriter());
				}
				try
				{
					updateFund.updateExistingMutualFund();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				try
				{
					updateFund.mapId();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				try
				{
					navController.addNewNav();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				try
				{
					navController.updateNav(null);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();	
	}
%>
</body>
</html>