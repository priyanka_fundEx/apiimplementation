<%@page import="org.json.JSONArray"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.fundexpert.util.NavStatus"%>
<%@page import="java.util.Map"%>
<%@page import="com.fundexpert.util.MutualFundStatus"%>
<%@page import="com.fundexpert.util.SerializedObjectToFile"%>
<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Result of MfNav SyncUp utility</title>
</head>
<body>
<%
String prodName="";
JSONObject mainJson=new JSONObject();

File mutualFundsFile=new File("/opt/feapi/MutualFund/mfSyncStatus.txt");
File navFile=new File("/opt/feapi/Nav/navSyncStatus.txt");

SerializedObjectToFile sotf=new SerializedObjectToFile();

try {
	Map<String,String> map=null;
	
	MutualFundStatus mf=null;
	try
	{
		mf=(MutualFundStatus)sotf.getSerializedFile(mutualFundsFile);
		map=mf.getUpdateMFIDMap();
		
		JSONArray updateMfIdJsonArray=new JSONArray();
		if(map!=null)
		{
			for(Map.Entry<String,String> m:map.entrySet())
			{
				JSONObject json=new JSONObject();
				json.put("d", m.getKey());
				json.put("s",m.getValue());
				updateMfIdJsonArray.put(json);
			}
		}
		mainJson.put("mfIdMap", updateMfIdJsonArray);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	try
	{
		map=mf.getAddNewMFMap();
		JSONArray addNewJsonArray=new JSONArray();
		if(map!=null)
		{
			for(Map.Entry<String,String> m:map.entrySet())
			{
				JSONObject json=new JSONObject();
				json.put("d", m.getKey());
				json.put("s",m.getValue());
				addNewJsonArray.put(json);
			}
		}
		mainJson.put("addNewMf", addNewJsonArray);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	try
	{
		map=mf.getUpdateExistingMFMap();
		JSONArray updateExistingMFJsonArray=new JSONArray();
		if(map!=null)
		{
			for(Map.Entry<String,String> m:map.entrySet())
			{
				JSONObject json=new JSONObject();
				json.put("d", m.getKey());
				json.put("s",m.getValue());
				updateExistingMFJsonArray.put(json);
			}
		}
		mainJson.put("updateExistingMf", updateExistingMFJsonArray);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	NavStatus nav=null;
	Map<String,String> navMap=null;
	try
	{
		nav=(NavStatus)sotf.getSerializedFile(navFile);
		navMap=nav.getAddNewNavMap();
		JSONArray addNewNavJsonArray=new JSONArray();
		if(navMap!=null)
		{
			for(Map.Entry<String,String> m:navMap.entrySet())
			{
				JSONObject json=new JSONObject();
				json.put("d", m.getKey());
				json.put("s",m.getValue());
				addNewNavJsonArray.put(json);
			}
		}
		mainJson.put("addNewNav", addNewNavJsonArray);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	try
	{
		navMap=nav.getUpdateExistingNavMap();
		JSONArray updateExistingNavJsonArray=new JSONArray();
		if(navMap!=null)
		{
			for(Map.Entry<String,String> m:navMap.entrySet())
			{
				JSONObject json=new JSONObject();
				json.put("d", m.getKey());
				json.put("s",m.getValue());
				updateExistingNavJsonArray.put(json);
			}
		}
		mainJson.put("updateExistingNav", updateExistingNavJsonArray);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	mainJson.put("success", true);
	out.print(mainJson.toString(1));
		
} catch (Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	mainJson.put("success", false);
	out.print(mainJson.toString(1));
}
%>
</body>
</html>