<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.fundexpert.controller.StocksController"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sync Stocks</title>
</head>
<body>
<%
	try
	{
		boolean updatedStocks=false;
		boolean allSuccess=false;
		JSONObject json=new JSONObject();
		StocksController shc=new StocksController();
		System.out.println("getting details in a moment from another server");
		json=shc.getDetails(null);
		if(json.has("success") && json.getString("success").equals("true"))
		{
			JSONArray jsonArray=json.getJSONArray("stockData");
			System.out.println("received JSON size="+jsonArray.length());
			allSuccess=shc.updateStock(jsonArray);
		}
		else
		{
			throw new Exception("Error Occurred(success not true while getting stocks data from FE db) ERROR="+json.getString("error"));
		}
		System.out.println("Success updating stocks table="+allSuccess);
		out.print("Success of all Stocks : "+allSuccess);
	}
	catch(Exception e)
	{
		e.printStackTrace();
		e.printStackTrace(response.getWriter());
	}

%>
</body>
</html>