<%@page import="com.fundexpert.controller.MutualFundController"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update existing MF</title>
</head>
<body>	
<h1>Update Existing Mutual Fund</h1>
<%
	if(request.getParameterMap().containsKey("submit") && request.getParameter("submit").equals("Submit"))
	{
		final String p="fundexpert1234";
		
		if(!request.getParameterMap().containsKey("pass"))
		{
			out.write("Password empty.");
		}
		
		String receivedP=request.getParameter("pass");
		if(receivedP.equals(p))
		{
			try
			{
				MutualFundController updateFund=new MutualFundController();
				boolean result=false;
				
				result=updateFund.updateExistingMutualFund();
				
				if(result)
					out.print("<br>successfully updated MutualFund.");
				else
					out.print("<br>Something failed at backened");
			}
			catch(Exception e)
			{
				e.printStackTrace(response.getWriter());
			}
		}
		else
		{
			out.write("Invalid Password.");
		}
	}
	else
	{
		%>
		<form name="form" action="updateExistingFunds.jsp"  method="post">
		<input type="password" name="pass"/>
		<input type="submit" name="submit" value="Submit"/>
		</form>
		<%
	}
%>
</body>
</html>