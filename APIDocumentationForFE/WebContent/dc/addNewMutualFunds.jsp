<%@page import="com.fundexpert.controller.MutualFundController"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add new MF</title>
</head>
<body>
<h1>To be run everytime scheme master is uploaded on fundexpert</h1>
<form name="form" action="addNewMutualFunds.jsp"  method="post">
<input type="password" name="pass"/>
<input type="submit" name="submit" value="Submit"/>
</form>
<%
	if(request.getParameterMap().containsKey("submit") && request.getParameter("submit").equals("Submit"))
	{
		final String p="fundexpert123";
		
		if(!request.getParameterMap().containsKey("pass") || !request.getParameter("pass").equals(p))
		{
			out.write("Password invalid/empty.");
		}
		
		String receivedP=request.getParameter("pass");
		if(receivedP.equals(p))
		{
			try
			{
				MutualFundController updateFund=new MutualFundController();
				boolean result=false;
				
				result=updateFund.addNewMutualFund();
				
				if(result)
					out.print("<br>successfully updated MutualFund.");
				else
					out.print("<br>Something failed at backened");
			}
			catch(Exception e)
			{
				e.printStackTrace(response.getWriter());
			}
			
		}
	}
%>
</body>
</html>