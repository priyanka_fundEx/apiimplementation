<%@page import="com.fundexpert.controller.NavController"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add new Nav</title>
</head>
<body>
<h1>To Add New Nav from Fundexpert</h1>
<form name="addNav" action="addNewNav.jsp" method="post">
<input type="password" name="pass"/>
<input type="submit" name="submit" value="Submit"/>
</form>
<%
	if(request.getParameterMap().containsKey("submit") && request.getParameter("submit").equals("Submit"))
	{
		final String p="fundexpert12345";
		
		if(!request.getParameterMap().containsKey("pass") || !request.getParameter("pass").equals(p))
		{
			out.write("Password invalid/empty.");
		}
		
		String receivedP=request.getParameter("pass");
		if(receivedP.equals(p))
		{
			try
			{
				NavController updateNav=new NavController();
				boolean result=false;
				
				result=updateNav.addNewNav();
				
				if(result)
					out.print("successfully updated Nav.");
				else
					out.print("Something failed at backened");
			}
			catch(Exception e)
			{
				e.printStackTrace(response.getWriter());
			}
		}
	}
%>
</body>
</html>