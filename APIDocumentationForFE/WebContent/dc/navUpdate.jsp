<%@page import="com.fundexpert.controller.NavController"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Existing Nav</title>
</head>
<body>
<h1>To Update Existing Nav from Fundexpert (Select start date if missing funds(from data.fundexpert.in ) found on fundexpert)</h1>

<form name="updateNav" action="navUpdate.jsp"  method="post">
<input type="password" name="pass"/>
<input name="startDate" type="date"/>
<input type="submit" name="submit" value="Submit"/>
</form>
<%
	if(request.getParameterMap().containsKey("submit") && request.getParameter("submit").equals("Submit"))
	{
		final String p="fundexpert123456";
		
		if(!request.getParameterMap().containsKey("pass") || !request.getParameter("pass").equals(p))
		{
			out.write("Password invalid/empty.");
		}
		
		String receivedP=request.getParameter("pass");
		if(receivedP.equals(p))
		{
			try
			{
				NavController updateNav=new NavController();
				boolean result=false;
				if(request.getParameterMap().containsKey("startDate"))
				{
					Date sDate=null;
					if(!request.getParameter("startDate").equals(""))
					{
						sDate=new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("startDate"));
					}
					result=updateNav.updateNav(sDate);
				}
				else
				{
					result=updateNav.updateNav(null);
				}
				if(result)
					out.print("successfully updated Nav.");
				else
					out.print("Something failed at backened");
			}
			catch(Exception e)
			{
				e.printStackTrace(response.getWriter());
			}
		}
	}
%>
</body>
</html>