<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>File upload</title>
<script src="../assets/plugins/jquery.min.js"></script>
</head>
<body>

<h2>Upload CAS pdf</h2>
<form id="data" action="uploadFile" method="post" enctype="multipart/form-data">
MutualFund :<input type="file" name="mffile" multiple/>
MF Password :<input type="password" name="mpan" value=""/>
Stock File :<input type="file" name="stockfile" multiple/>
Stock Password<input type="password" name="span" value=""/>
<br>
<input type="submit">
</form>

</body>
<script>
$(document).ready(function(){

	$("form#data").submit(function(e) {
	    e.preventDefault();    
	    var formData = new FormData(this);
		console.log("Making ajax.");
	    $.ajax({
	        url: "../uploadFile",
	        type: 'POST',
	        data: formData,
	        success: function (data) {
	        	data=JSON.parse(data);
	        	console.log("Success = "+data.success);
	        	console.log("mfsuccess= "+data.mfSuccess);
	        	console.log("stockSucess="+data.stockSuccess);
        		if(typeof data.success!='undefined' && data.success && ((typeof data.mfSuccess!='undefined' && !data.mfSuccess) && (typeof data.stockSuccess!='undefined' && !data.stockSuccess) || (typeof data.mfSuccess=='undefined' && typeof data.stockSuccess=='undefined')))
           		{
       				alert("Not able to upload files.Try uploading individually.");
           		}
        		else if(typeof data.success!='undefined' && data.success && data.stockUpload && !data.stockSuccess)
           		{
       				alert("Not able to upload Stock file.");
           		}
        		else if(typeof data.success!='undefined' && data.success && data.mfUpload && !data.mfSuccess)
           		{
       				alert("Not able to upload MutualFund file.");
           		}
        		else if(typeof data.success!='undefined' && !data.success)
           		{
        			if(typeof data.basicerror!='undefined')
       					alert("File upload failed. "+data.basicerror);
        			else 
        				alert("Internal Error occurred.");
           		}
        		else if(typeof data.success!='undefined' && data.success)
           		{
       				alert("Successfully uploaded the files.");
       				window.location.href="index.html";
           		}
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	});
});
</script>
</html>