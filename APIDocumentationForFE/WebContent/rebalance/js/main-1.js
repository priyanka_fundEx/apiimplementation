$(document).ready(function() {

	$.getJSON( "json/data.json", function( data ) {

		console.log(data);

		var dynamicVariables = [];
		var dataForChart = [];

		var equityMidCap = {
			name : "Mid Cap (Mutual Fund)",
			y: 0
		};
		var equitySmallCap = {
			name : "Small Cap (Mutual Fund)",
			y: 0
		};
		var equityLargeCap = {
			name : "Large Cap (Mutual Fund)",
			y: 0
		};

		var debtLongTerm = {
			name : "Long Term (Mutual Fund)",
			y: 0
		};
		var debtShortTerm = {
			name : "Short Term (Mutual Fund)",
			y: 0
		};

		for (var i = 0; i < data.portfolio.length; i++) {
			console.log(data.portfolio[i]);


			if(data.portfolio[i].assetType == 1){
				//Mutualfund
				if(data.portfolio[i].segment.toLowerCase() == "equity" && data.portfolio[i].subSegment.toLowerCase() == "mid cap"){
					equityMidCap.y += data.portfolio[i].allocation;
				}
				else if(data.portfolio[i].segment.toLowerCase() == "equity" && data.portfolio[i].subSegment.toLowerCase() == "small cap"){
					equitySmallCap.y += data.portfolio[i].allocation;
				}
				else if(data.portfolio[i].segment.toLowerCase() == "equity" && data.portfolio[i].subSegment.toLowerCase() == "large cap"){
					equityLargeCap.y += data.portfolio[i].allocation;
				}
				else if(data.portfolio[i].segment.toLowerCase() == "debt" && data.portfolio[i].subSegment.toLowerCase() == "long term"){
					debtLongTerm.y += data.portfolio[i].allocation;
				}
				else if(data.portfolio[i].segment.toLowerCase() == "debt" && data.portfolio[i].subSegment.toLowerCase() == "short term"){
					debtShortTerm.y += data.portfolio[i].allocation;
				}
			}
			else if(data.portfolio[i].assetType == 2){
				//Stocks

				if(dynamicVariables.indexOf("equity" + data.portfolio[i].segment.replace(/\s/g, '')) > 0){

					var addedVariable = eval("equity" + data.portfolio[i].segment.replace(/\s/g, '') + ".y");
					addedVariable += data.portfolio[i].allocation;

					eval("window.equity" + data.portfolio[i].segment.replace(/\s/g, '')+ ".y =addedVariable");
				}
				else{

					var addedVariable = 0;
					addedVariable += data.portfolio[i].allocation;

					eval("window.equity" + data.portfolio[i].segment.replace(/\s/g, '') + "={}");
					eval("window.equity" + data.portfolio[i].segment.replace(/\s/g, '') + ".name = data.portfolio[i].segment");
					eval("window.equity" + data.portfolio[i].segment.replace(/\s/g, '') + ".y = addedVariable");
				}

				dynamicVariables.push("equity" + data.portfolio[i].segment.replace(/\s/g, ''));

			}
		}

		console.log(dynamicVariables);

		console.log(equityMidCap);
		console.log(equitySmallCap);
		console.log(equityLargeCap);
		console.log(debtLongTerm);
		console.log(debtShortTerm);


		if(equityMidCap.y > 0){
			dataForChart.push(equityMidCap);
		}

		if(equitySmallCap.y > 0){
			dataForChart.push(equitySmallCap);
		}

		if(equityLargeCap.y > 0){
			dataForChart.push(equityLargeCap);
		}

		if(debtLongTerm.y > 0){
			dataForChart.push(debtLongTerm);
		}

		if(debtShortTerm.y > 0){
			dataForChart.push(debtShortTerm);
		}

		for(var i = 0; i < dynamicVariables.length; i++){
			dataForChart.push(eval(dynamicVariables[i]));
		}

		console.log("DATA FOR CHART: ", dataForChart);



		// // ASSET TYPE Allocation Pie Chart
		// Highcharts.chart('asset-type-chart-container', {
		//     chart: {
		//         plotBackgroundColor: null,
		//         plotBorderWidth: null,
		//         plotShadow: false,
		//         type: 'pie'
		//     },
		//     title: {
		//         text: 'Browser market shares in January, 2018'
		//     },
		//     tooltip: {
		//         pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		//     },
		//     plotOptions: {
		//         pie: {
		//             allowPointSelect: true,
		//             cursor: 'pointer',
		//             dataLabels: {
		//                 enabled: true,
		//                 format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		//                 style: {
		//                     color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		//                 }
		//             }
		//         }
		//     },
		//     series: [{
		//         name: 'Brands',
		//         colorByPoint: true,
		//         data: dataForChart
		//     }]
		// });


		Highcharts.chart('chart-container', {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: 'Browser market shares in January, 2018'
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                style: {
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                }
		            }
		        }
		    },
		    series: [{
		        name: 'Brands',
		        colorByPoint: true,
		        data: dataForChart
		    }]
		});


	});



})
