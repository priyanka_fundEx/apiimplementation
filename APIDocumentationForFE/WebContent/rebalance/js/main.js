// classification 1 (Asset type)
//
// a) Debt MF
// b) Equity MF
// c) Direct Equity
// d) Cash (post rebalance it will be there for sure)
//
// classification 2
// separate pie chart for each
// a) equity mf - large/mid/small
// b) equity - sector wise
// c) Debt - long term/short term
//
// classification 3
// Bar chart  for each holidng on x axis and value on y axis



$(document).ready(function() {

	
	 var pdfURL = "/app/getPortfolioAllocation?action=getInitialAllocation&id=3"
	 var data;
	 $.ajax({
	   url: pdfURL,
	   data: data,
	   dataType: "json",
	   success: function(data) { 				//replace this code snip in place of the below $.getJSON line. Make sure uRL is fine and uncomment the line

	//$.getJSON( "json/data.json", function( data ) {

		console.log(data);

		var assetTypeDirectEquities = 0.0;
		var assetTypeMutualFunds = 0.0;
		var assetTypeMutualEquity = 0.0;
		var assetTypeMutualDebt = 0.0;
		var assetTypeMutualOther = 0.0;
		var assetTypeCash = 0.0;


		var subSegmentTypeLargeCap = 0.0;
		var subSegmentTypeSmallCap = 0.0;
		var subSegmentTypeMidCap = 0.0;

		var subSegmentTypeLargeCapPercent = 0.0;
		var subSegmentTypeSmallCapPercent = 0.0;
		var subSegmentTypeMidCapPercent = 0.0;

		var subSegmentTypeLongTerm = 0.0;
		var subSegmentTypeShortTerm = 0.0;

		var subSegmentTypeLongTermPercent = 0.0;
		var subSegmentTypeShortTermPercent = 0.0;

		var equitySubTypeLargeCap = 0.0;

		var directEquitiesSectors = [];

		var dataForDirectEquitySubTypeBarChart = [];
		var dataForDirectEquitySubTypePieChart = [];

		var equityMutualFunds = [];
		var debtMutualFunds = [];
		var equityStocks = [];

		var allHoldingsList = [];
		var allHoldingsBarChartList = [];

		var equityCurrentValueTotal = 0.0;
		var debtCurrentValueTotal = 0.0;
		var stockCurrentValueTotal = 0.0;


		var holdingBarChartHeight = 500;

		for (var i = 0; i < data.portfolio.length; i++) {
			if(data.portfolio[i].assetType == 1){
				//Mutual Fund

				//Adding to all holdings

				if(allHoldingsList.length <= 0){

					var allHoldingsObj = {
						name: data.portfolio[i].mfName,
						data: [data.portfolio[i].allocation]
					};

					allHoldingsBarChartList.push(allHoldingsObj);
					allHoldingsList.push(data.portfolio[i].mfName);
				}

				else{
					if(allHoldingsBarChartList.indexOf(data.portfolio[i].mfName) != -1){

						var index = allHoldingsBarChartList.indexOf(data.portfolio[i].mfName);

						allHoldingsBarChartList[index].data[0] += data.portfolio[i].allocation;
					}
					else{
						var allHoldingsObj = {
							name: data.portfolio[i].mfName,
							data: [data.portfolio[i].allocation]
						};

						allHoldingsBarChartList.push(allHoldingsObj);
						allHoldingsList.push(data.portfolio[i].mfName);
					}
				}


				assetTypeMutualFunds += data.portfolio[i].allocation;

				if(data.portfolio[i].segment.toLowerCase() == "equity"){
					assetTypeMutualEquity += data.portfolio[i].allocation;

					equityCurrentValueTotal += data.portfolio[i].allocation;

					// subSegmentTypeLargeCapPercent = (data.portfolio[i].allocation * 100) /

					// EQUITY SUB TYPE CALCLULATION
					if(data.portfolio[i].subSegment.toLowerCase() == "large cap"){
						subSegmentTypeLargeCap += data.portfolio[i].allocation;
					}
					else if(data.portfolio[i].subSegment.toLowerCase() == "mid cap"){
						subSegmentTypeMidCap += data.portfolio[i].allocation;
					}
					else if(data.portfolio[i].subSegment.toLowerCase() == "small cap"){
						subSegmentTypeSmallCap += data.portfolio[i].allocation;
					}

				}
				else if(data.portfolio[i].segment.toLowerCase() == "debt"){
					assetTypeMutualDebt += data.portfolio[i].allocation;

					debtCurrentValueTotal += data.portfolio[i].allocation;

					// DEBT SUB TYPE CALCLULATION
					if(data.portfolio[i].subSegment.toLowerCase() == "long term"){
						subSegmentTypeLongTerm += data.portfolio[i].allocation;
					}
					else if(data.portfolio[i].subSegment.toLowerCase() == "short term"){
						subSegmentTypeShortTerm += data.portfolio[i].allocation;
					}

				}
				else{
					assetTypeMutualOther += data.portfolio[i].allocation;
				}

			}
			else if(data.portfolio[i].assetType == 2){
				//Stock

				stockCurrentValueTotal += data.portfolio[i].allocation;
				//Adding to all holdings
				// console.log("data.portfolio[i].stockName: ", data.portfolio[i].stockName);
				// console.log("allHoldingsList: ", allHoldingsList);

				if(allHoldingsList.length <= 0){
					allHoldingsList.push(data.portfolio[i].stockName);
					var allHoldingsObj = {
						name: data.portfolio[i].stockName,
						data: [data.portfolio[i].allocation]
					};

					allHoldingsBarChartList.push(allHoldingsObj);
				}

				else{
					if(allHoldingsList.indexOf(data.portfolio[i].stockName) != -1){

						var index = allHoldingsBarChartList.indexOf(data.portfolio[i].stockName);

						console.log("index: ", index);
						console.log("allHoldingsList: ",allHoldingsList);
						allHoldingsBarChartList[index].data[0] += data.portfolio[i].allocation;
					}
					else{
						allHoldingsList.push(data.portfolio[i].stockName);
						var allHoldingsObj = {
							name: data.portfolio[i].stockName,
							data: [data.portfolio[i].allocation]
						};

						allHoldingsBarChartList.push(allHoldingsObj);
					}
				}


				assetTypeDirectEquities += data.portfolio[i].allocation;

				//DIRECT EQUITIES SUB TYPE
				if(directEquitiesSectors.length <= 0){
					//add new sector
					directEquitiesSectors.push(data.portfolio[i].segment);
					var directEqObj = {
						name: data.portfolio[i].segment,
						data: [data.portfolio[i].allocation]
					};
					dataForDirectEquitySubTypeBarChart.push(directEqObj);


					var directEqObjPie = {
						name: data.portfolio[i].segment,
						y: data.portfolio[i].allocation
					};
					dataForDirectEquitySubTypePieChart.push(directEqObjPie);
				}
				else{
					if(directEquitiesSectors.indexOf(data.portfolio[i].segment) != -1){
						// already exists
						var index = directEquitiesSectors.indexOf(data.portfolio[i].segment);
						dataForDirectEquitySubTypeBarChart[index].data[0] += data.portfolio[i].allocation;


						var index = directEquitiesSectors.indexOf(data.portfolio[i].segment);
						dataForDirectEquitySubTypePieChart[index].y += data.portfolio[i].allocation;
					}
					else{
						directEquitiesSectors.push(data.portfolio[i].segment);
						var directEqObj = {
							name: data.portfolio[i].segment,
							data: [data.portfolio[i].allocation]
						};
						dataForDirectEquitySubTypeBarChart.push(directEqObj);

						var directEqObjPie = {
							name: data.portfolio[i].segment,
							y: data.portfolio[i].allocation
						};
						dataForDirectEquitySubTypePieChart.push(directEqObjPie);
					}
				}

			}
		}


		for (var i = 0; i < dataForDirectEquitySubTypeBarChart.length; i++) {
			dataForDirectEquitySubTypeBarChart[i].data[0] = (dataForDirectEquitySubTypeBarChart[i].data[0] * 100) / stockCurrentValueTotal;
		}


		// ASSET CLASS CLASSIFICATION
		var dataForAssetClassBarChart = [{
			name: 'Direct Equity',
			data: [assetTypeDirectEquities]
		}, {
			name: 'Debt MF',
			data: [assetTypeMutualDebt]
		}, {
			name: 'Equity MF',
			data: [assetTypeMutualEquity]
		}, {
			name: 'Cash',
			data: [assetTypeCash]
		}];


		// ASSET CLASS CLASSIFICATIO PIE
		var dataForAssetClassPieChart = [{
			name : 'Direct Equity',
			y: assetTypeDirectEquities
		}, {
			name: 'Debt MF',
			y: assetTypeMutualDebt
		}, {
			name: 'Equity MF',
			y: assetTypeMutualEquity
		}, {
			name: 'Cash',
			y: assetTypeCash
		}];


		subSegmentTypeLargeCapPercent = (subSegmentTypeLargeCap * 100) / equityCurrentValueTotal;
		subSegmentTypeSmallCapPercent = (subSegmentTypeSmallCap * 100) / equityCurrentValueTotal;
		subSegmentTypeMidCapPercent = (subSegmentTypeMidCap * 100) / equityCurrentValueTotal;

		// EQUITY SUB TYPE CLASSIFICATION
		var dataForEquitySubTypeBarChart = [{
			name: 'Equity Large Cap',
			data: [subSegmentTypeLargeCapPercent]
		}, {
			name: 'Equity Mid Cap',
			data: [subSegmentTypeSmallCapPercent]
		}, {
			name: 'Equity Small Cap',
			data: [subSegmentTypeMidCapPercent]
		}];

		var dataForEquitySubTypePieChart = [{
			name: 'Equity Large Cap',
			y: subSegmentTypeLargeCapPercent
		}, {
			name: 'Equity Mid Cap',
			y: subSegmentTypeSmallCapPercent
		}, {
			name: 'Equity Small Cap',
			y: subSegmentTypeMidCapPercent
		}];


		subSegmentTypeLongTermPercent = (subSegmentTypeLongTerm * 100) / debtCurrentValueTotal;
		subSegmentTypeShortTermPercent = (subSegmentTypeShortTerm * 100) / debtCurrentValueTotal;

		// DEBT SUB TYPE CLASSIFICATION
		var dataForDebtSubTypeBarChart = [{
			name: 'Debt Long Term',
			data: [subSegmentTypeLongTermPercent]
		}, {
			name: 'Debt Short Term',
			data: [subSegmentTypeShortTermPercent]
		}];

		var dataForDebtSubTypePieChart = [{
			name: 'Debt Long Term',
			y: subSegmentTypeLongTermPercent
		}, {
			name: 'Debt Short Term',
			y: subSegmentTypeShortTermPercent
		}];


		console.log("allHoldingsBarChartList: ", allHoldingsBarChartList);

		if(allHoldingsBarChartList.length > 8){
			var newHeightMultiplier = allHoldingsBarChartList.length - 8;

			var addedHeight = newHeightMultiplier * 35;
			holdingBarChartHeight += addedHeight;
		}




		Highcharts.chart('asset-class-bar-chart-container', {
		    chart: {
		        type: 'bar',
				marginBottom: 100
		    },
		    title: {
		        text: 'Portfolio Breakup'
		    },
		    xAxis: {
		        // categories: ['Direct Equity', 'Debt MF', 'Equity Mf', 'Cash'],
		        title: {
		            text: 'Categories'
		        },
				labels: {
		            rotation: -45,
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Percentage',
		            align: 'high'
		        },
		        labels: {
		            overflow: 'justify'
		        }
		    },
		    tooltip: {
		        valueSuffix: ' %'
		    },
		    plotOptions: {
		        bar: {
		            dataLabels: {
		                enabled: true
		            }
		        }
		    },
		    legend: {
		        layout: 'horizontal',
		        align: 'center',
		        verticalAlign: 'bottom',
		        x: 0,
		        y: 0,
		        floating: true,
		        borderWidth: 1,
		        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
		        shadow: true
		    },
		    credits: {
		        enabled: false
		    },
		    series: dataForAssetClassBarChart
		});


		Highcharts.chart('equity-mf-chart-container', {
		    chart: {
		        type: 'bar',
				marginBottom: 100
		    },
		    title: {
		        text: 'Equity MF Breakup'
		    },
		    // subtitle: {
		    //     text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
		    // },
		    xAxis: {
		        // categories: ['Equity Large Cap', 'Equity Mid Cap', 'Equity Small Cap'],
		        title: {
		            text: 'Categories'
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Percentage',
		            align: 'high'
		        },
		        labels: {
		            overflow: 'justify'
		        },
				visible: true
		    },
		    tooltip: {
		        valueSuffix: ' %'
		    },
		    plotOptions: {
		        bar: {
		            dataLabels: {
		                enabled: true
		            }
		        }
		    },
		    legend: {
		        layout: 'horizontal',
		        align: 'center',
		        verticalAlign: 'bottom',
		        x: 0,
		        y: 0,
		        floating: true,
		        borderWidth: 1,
		        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
		        shadow: true
		    },
		    credits: {
		        enabled: false
		    },
		    series: dataForEquitySubTypeBarChart
		});


		// DEBT DISTRIBUTION BAR CHART
		Highcharts.chart('debt-mf-chart-container', {
		    chart: {
		        type: 'bar',
				marginBottom: 100
		    },
		    title: {
		        text: 'Debt MF Breakup'
		    },
		    xAxis: {
		        // categories: ['Debt Long Term', 'Debt Short Term'],
		        title: {
		            text: 'Categories'
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Percentage',
		            align: 'high'
		        },
		        labels: {
		            overflow: 'justify'
		        }
		    },
		    tooltip: {
		        valueSuffix: ' %'
		    },
		    plotOptions: {
		        bar: {
		            dataLabels: {
		                enabled: true
		            }
		        }
		    },
		    legend: {
		        layout: 'horizontal',
		        align: 'center',
		        verticalAlign: 'bottom',
		        x: 0,
		        y: 0,
		        floating: true,
		        borderWidth: 1,
		        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
		        shadow: true
		    },
		    credits: {
		        enabled: false
		    },
		    series: dataForDebtSubTypeBarChart
		});


		// DIRECT EQUITY BAR CHART
		Highcharts.chart('direct-equity-chart-container', {
		    chart: {
		        type: 'bar',
				height: 500,
				marginBottom: 150
		    },
		    title: {
		        text: 'Direct Equity Breakup'
		    },
		    xAxis: {
		        // categories: ['Direct Equity', 'Debt MF', 'Equity Mf', 'Cash'],
		        title: {
		            text: 'Categories'
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Percentage',
		            align: 'high'
		        },
		        labels: {
		            overflow: 'justify'
		        }
		    },
		    tooltip: {
		        valueSuffix: ' %'
		    },
		    plotOptions: {
		        bar: {
		            dataLabels: {
		                enabled: true
		            }
		        }
		    },
		    legend: {
		        layout: 'horizontal',
		        align: 'center',
		        verticalAlign: 'bottom',
		        x: 0,
		        y: 0,
		        floating: true,
		        borderWidth: 1,
		        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
		        shadow: true
		    },
		    credits: {
		        enabled: false
		    },
		    series: dataForDirectEquitySubTypeBarChart
		});


		// EQUITY DISTRIBUTION PIE CHART
		Highcharts.chart('equity-mf-pie-chart-container', {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: 'Equity Breakup'
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                style: {
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                }
		            }
		        }
		    },
			credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Brands',
		        colorByPoint: true,
		        data: dataForEquitySubTypePieChart
		    }]
		});

		Highcharts.chart('debt-mf-pie-chart-container', {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: 'Equity Breakup'
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                style: {
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                }
		            }
		        }
		    },
			credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Brands',
		        colorByPoint: true,
		        data: dataForDebtSubTypePieChart
		    }]
		});


		Highcharts.chart('direct-equity-pie-chart-container', {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: 'Equity Breakup'
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                style: {
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                }
		            }
		        }
		    },
			credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Brands',
		        colorByPoint: true,
		        data: dataForDirectEquitySubTypePieChart
		    }]
		});


		// DIRECT EQUITY BAR CHART
		Highcharts.chart('holding-chart-container', {
		    chart: {
		        type: 'bar',
				height: holdingBarChartHeight,
				marginBottom: 150
		    },
		    title: {
		        text: 'Portfolio Breakup'
		    },
		    xAxis: {
		        // categories: ['Direct Equity', 'Debt MF', 'Equity Mf', 'Cash'],
		        title: {
		            text: 'Categories'
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Percentage',
		            align: 'high'
		        },
		        labels: {
		            overflow: 'justify'
		        }
		    },
		    tooltip: {
		        valueSuffix: ' %'
		    },
		    plotOptions: {
		        bar: {
		            dataLabels: {
		                enabled: true
		            }
		        },
				series: {
		            pointWidth: 20
		        }
		    },
		    legend: {
		        layout: 'horizontal',
		        align: 'center',
		        verticalAlign: 'bottom',
		        x: 0,
		        y: 0,
		        floating: true,
		        borderWidth: 1,
		        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
		        shadow: true
		    },
		    credits: {
		        enabled: false
		    },
		    series: allHoldingsBarChartList
		});

	   }
	});


	 equity60 = function(){

			var equityPercentRiskProfile = 60.0;
			var debtPercentRiskProfile = 40.0;
			rebalancePDF(equityPercentRiskProfile, debtPercentRiskProfile);
		}

		equity70 = function(){
			var equityPercentRiskProfile = 70.0;
			var debtPercentRiskProfile = 30.0;
			rebalancePDF(equityPercentRiskProfile, debtPercentRiskProfile);
		}

		equity80 = function(){
			var equityPercentRiskProfile = 80.0;
			var debtPercentRiskProfile = 20.0;
			rebalancePDF(equityPercentRiskProfile, debtPercentRiskProfile);
		}


		rebalancePDF = function(equityPercentRiskProfile, debtPercentRiskProfile){

			console.log("equityPercentRiskProfile", equityPercentRiskProfile);
			console.log("debtPercentRiskProfile", debtPercentRiskProfile);			
			var resultURL = "/app/getPortfolioAllocation?action=getOptimizedAllocation&eP="+equityPercentRiskProfile+"&dP="+equityPercentRiskProfile+"&id=3";		

			// var resultURL = "/app/optimize?equityPercent="+equityPercentRiskProfile+"&debtPercent="+debtPercentRiskProfile;

			 $.ajax({
				   url: resultURL,
				   data: data,
				   dataType: "json",
				   success: function(data) { 				//replace this code snip in place of the below $.getJSON line. Make sure uRL is fine and uncomment that line
					


			//$.getJSON( "json/result.json", function( data ) {				// replace this line with

				console.log("RESULT: ", data);

				var totalCount = 0;
				var resultDataInBarChart = [];

				var assetAllocationArray = [];

				var assetAllocationArrayFirst = [];
				var assetAllocationArraySecond = [];

				var tableRowString = "";

				var equityTableRowString = "";
				var debtTableRowString = "";

				var equitySubClassTableString = "";

				var largeCapEquityPercentage = 0.0;
				var smallCapEquityPercentage = 0.0;
				var midCapEquityPercentage = 0.0;

				var largeCapEquityPercentageBefore = 0.0;
				var smallCapEquityPercentageBefore = 0.0;
				var midCapEquityPercentageBefore = 0.0;

				var longTermDebtPercentage = 0.0;
				var shortTermDebtPercentage = 0.0;

				var longTermDebtPercentageBefore = 0.0;
				var shortTermDebtPercentageBefore = 0.0;

				if(typeof data.stock!='undefined' && data.stock.length>0)
				{
					for (var i = 0; i < data.stock.length; i++) {
	
						assetAllocationArrayFirst.push(parseFloat(data.stock[i].allocationBeforeOptimizer));
						assetAllocationArraySecond.push(parseFloat(data.stock[i].allocationAfterOptimizer));
	
						assetAllocationArray.push(data.stock[i].name);
	
						totalCount++;
	
						tableRowString += "<tr>"
											  + "<td>"+data.stock[i].name+"</td>"
											  + "<td>"+data.stock[i].allocationBeforeOptimizer+"</td>"
											  + "<td>"+data.stock[i].allocationAfterOptimizer+"</td>"
										  + "</tr>";
	
					  	if(data.stock[i].subSegment.toLowerCase() == "small cap"){
							smallCapEquityPercentageBefore += data.stock[i].allocationBeforeOptimizer;
							smallCapEquityPercentage += data.stock[i].allocationAfterOptimizer;
						}
						else if(data.stock[i].subSegment.toLowerCase() == "mid cap"){
							midCapEquityPercentageBefore += data.stock[i].allocationBeforeOptimizer;
							midCapEquityPercentage += data.stock[i].allocationAfterOptimizer;
						}
						else if(data.stock[i].subSegment.toLowerCase() == "large cap"){
							largeCapEquityPercentageBefore += data.stock[i].allocationBeforeOptimizer;
							largeCapEquityPercentage += data.stock[i].allocationAfterOptimizer;
						}
	
					}
				}
				if(typeof data.mf!='undefined' && data.mf.length>0)
				{
					for (var i = 0; i < data.mf.length; i++) {
	
						assetAllocationArrayFirst.push(parseFloat(data.mf[i].allocationBeforeOptimizer));
						assetAllocationArraySecond.push(parseFloat(data.mf[i].allocationAfterOptimizer));
	
						assetAllocationArray.push(data.mf[i].name);
						totalCount++;
	
	
						tableRowString += "<tr>"
											  + "<td>"+data.mf[i].name+"</td>"
											  + "<td>"+data.mf[i].allocationBeforeOptimizer+"</td>"
											  + "<td>"+data.mf[i].allocationAfterOptimizer+"</td>"
										  + "</tr>";
	
						if(data.mf[i].segment.toLowerCase() == "equity"){
	
							if(data.mf[i].subSegment.toLowerCase() == "small cap"){
								smallCapEquityPercentageBefore += data.mf[i].allocationBeforeOptimizer;
								smallCapEquityPercentage += data.mf[i].allocationAfterOptimizer;
							}
							else if(data.mf[i].subSegment.toLowerCase() == "mid cap"){
								midCapEquityPercentageBefore += data.mf[i].allocationBeforeOptimizer;
								midCapEquityPercentage += data.mf[i].allocationAfterOptimizer;
							}
							else if(data.mf[i].subSegment.toLowerCase() == "large cap"){
								largeCapEquityPercentageBefore += data.mf[i].allocationBeforeOptimizer;
								largeCapEquityPercentage += data.mf[i].allocationAfterOptimizer;
							}
						}
						else if(data.mf[i].segment.toLowerCase() == "debt"){
	
							if(data.mf[i].subSegment.toLowerCase() == "long term"){
								longTermDebtPercentageBefore += data.mf[i].allocationBeforeOptimizer;
								longTermDebtPercentage += data.mf[i].allocationAfterOptimizer;
							}
							else if(data.mf[i].subSegment.toLowerCase() == "short term"){
								shortTermDebtPercentageBefore += data.mf[i].allocationBeforeOptimizer;
								shortTermDebtPercentage += data.mf[i].allocationAfterOptimizer;
							}
						}
					}
				}
				console.log("largecap="+largeCapEquityPercentage);
				largeCapEquityPercentage = largeCapEquityPercentage.toFixed(2);
				smallCapEquityPercentage = smallCapEquityPercentage.toFixed(2);
				midCapEquityPercentage = midCapEquityPercentage.toFixed(2);
				longTermDebtPercentage = longTermDebtPercentage.toFixed(2);
				shortTermDebtPercentage = shortTermDebtPercentage.toFixed(2);

				largeCapEquityPercentageBefore = largeCapEquityPercentageBefore.toFixed(2);
				smallCapEquityPercentageBefore = smallCapEquityPercentageBefore.toFixed(2);
				midCapEquityPercentageBefore = midCapEquityPercentageBefore.toFixed(2);
				longTermDebtPercentageBefore = longTermDebtPercentageBefore.toFixed(2);
				shortTermDebtPercentageBefore = shortTermDebtPercentageBefore.toFixed(2);



				equityTableRowString += "<tr>"
									  + "<td>"+largeCapEquityPercentageBefore+"</td>"
									  + "<td>"+largeCapEquityPercentage+"</td>"
									  + "<td>"+midCapEquityPercentageBefore+"</td>"
									  + "<td>"+midCapEquityPercentage+"</td>"
									  + "<td>"+smallCapEquityPercentageBefore+"</td>"
									  + "<td>"+smallCapEquityPercentage+"</td>"
								  + "</tr>";

			  debtTableRowString += "<tr>"
									  + "<td>"+longTermDebtPercentageBefore+"</td>"
									  + "<td>"+longTermDebtPercentage+"</td>"
									  + "<td>"+shortTermDebtPercentageBefore+"</td>"
									  + "<td>"+shortTermDebtPercentage+"</td>"
								  + "</tr>";

			  $("#asset-allocation-before-and-after-debt-table").append(debtTableRowString);
			  $("#asset-allocation-before-and-after-equity-table").append(equityTableRowString);


				var height = (totalCount > 8) ? (500 + (totalCount * 30)) : 500;

				Highcharts.chart('asset-allocation-before-and-after-chart-container', {
				    chart: {
				        type: 'bar',
						height: height
				    },
				    title: {
				        text: 'Before and After Allocation'
				    },
				    subtitle: {
				        text: ''
				    },
				    xAxis: {
				        categories: assetAllocationArray,
				        title: {
				            text: null
				        }
				    },
				    yAxis: {
				        min: 0,
				        title: {
				            text: 'Population (millions)',
				            align: 'high'
				        },
				        labels: {
				            overflow: 'justify'
				        }
				    },
				    tooltip: {
				        valueSuffix: ' millions'
				    },
				    plotOptions: {
				        bar: {
				            dataLabels: {
				                enabled: true
				            }
				        }
				    },
				    legend: {
				        layout: 'vertical',
				        align: 'right',
				        verticalAlign: 'top',
				        x: -40,
				        y: 80,
				        floating: true,
				        borderWidth: 1,
				        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
				        shadow: true
				    },
				    credits: {
				        enabled: false
				    },
				    series: [{
				        name: 'Before',
				        data: assetAllocationArrayFirst
				    }, {
				        name: 'After',
				        data: assetAllocationArraySecond
				    }]
				});


				$("#asset-allocation-before-and-after-table").append(tableRowString);
				}//success closed
			});//ajax closed
		}
	 
});
