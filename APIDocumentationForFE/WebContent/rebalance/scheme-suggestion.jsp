<%@page import="com.fundexpert.dao.Stocks"%>
<%@page import="com.fundexpert.controller.StocksController"%>
<%@page import="com.fundexpert.exception.FundexpertException"%>
<%@page import="java.util.Map"%>
<%@page import="com.rebalance.util.SchemeLevelSuggestion"%>
<%@page import="com.fundexpert.config.Config"%>
<%@page import="com.fundexpert.dao.Asset"%>
<%@page import="com.fundexpert.dao.MutualFund"%>
<%@page import="com.fundexpert.controller.MutualFundController"%>
<%@page import="com.fundexpert.dao.UserPortfolioState"%>
<%@page import="com.fundexpert.controller.UserPortfolioStateController"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.fundexpert.dao.User"%>
<%@page import="java.util.List"%>
<%@page import="com.apidoc.util.HibernateBridge"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
HttpSession session2=request.getSession();
int role=100;
System.out.println("Session2 : login="+session2.getAttribute("login"));

if(session2.getAttribute("login")==null || session2.getAttribute("login").equals("false"))
{
	System.out.println("Inside.");
	response.sendRedirect("../login.jsp");
	return;
}
else
{
	role=(Integer)session2.getAttribute("role");
	if(role<5)
		response.sendRedirect("../login.jsp");
	System.out.println("Users role="+role);
}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Scheme Level Suggestions</title>
</head>
<body>

<%
	User loggedInUser=(User)session2.getAttribute("user");
	String submit=request.getParameter("submit");
	Long userId=null;
	if(submit!=null)
	{
		if(!request.getParameterMap().containsKey("user"))
		{
			
			throw new FundexpertException("Please select User.");
		}
		else
			userId=Long.valueOf(request.getParameter("user"));
	}
	User user=null;
	Session hSession=null;
	long consumerId=loggedInUser.getConsumerId();
	try
	{
		hSession=HibernateBridge.getSessionFactory().openSession();
		List<User> usersList=hSession.createQuery("from User where consumerId=?").setLong(0, consumerId).list();
		Iterator<User> itr=usersList.iterator();
	%>
		<form name="from" action="scheme-suggestion.jsp">
		<select name="user" id="select">
	<%
		while(itr.hasNext())
		{
			User u=itr.next();
			System.out.println("Uemail="+u.getEmail());
			if(userId!=null && (long)userId==(long)u.getId())
			{
				user=u;
			}
	%>
			<option value="<%=u.getId()%>" <%=(userId!=null?((long)userId==(long)u.getId()?"selected":""):"")%>><%=u.getEmail()%></option>
	<%
		}
	%>
			</select>
		<input type="submit" name="submit" />
		</form>
	<%
		if(submit!=null && submit.equals("Submit"))
		{
			MutualFundController mfc=new MutualFundController();
			
			List<Long> mfBlackListIdList=hSession.createQuery("select mutualFundId from BlackListFunds where consumerId=? and mutualFundId is not null").setLong(0,consumerId).list();
			List<Integer> stockBlackListIdList=hSession.createQuery("select scCode from BlackListFunds where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			List<Long> stockRecommendedList=hSession.createQuery("select scCode from RecommendedPortfolio where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			List<Long> mfRecoList=hSession.createQuery("select mutualFundId from Reco where consumerId=? and mutualFundId is not null").setLong(0, consumerId).list();
			List<Integer> stockRecoList=hSession.createQuery("select scCode from Reco where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			Config config1=new Config();
			double mfRating=Double.valueOf(config1.getProperty(Config.MF_RATING));
			double stockRating=Double.valueOf(config1.getProperty(Config.STOCK_RATING));
			List<Long> mfRating_mfId_List=hSession.createQuery("select id from MutualFund where ourRating>=?").setDouble(0, mfRating).list();
			List<Integer> stock_rating_List=hSession.createQuery("select scCode from Stocks where rating>=?").setDouble(0, stockRating).list();
			SchemeLevelSuggestion sls=new SchemeLevelSuggestion(userId,0);
			sls.getFirstLevelSuggestion();
			List<Long> mfKeep=sls.getMfKeep();
			List<Integer> stockKeep=sls.getStockKeep();
			Map<Long,String> mfRemoveMap=sls.getMfRemoveMap();
			Map<Integer,String> stockRemoveMap=sls.getStockRemoveMap();
			
			UserPortfolioStateController upsc=new UserPortfolioStateController();
			
			List<UserPortfolioState> upsList=upsc.getUserPortfolioState(userId);
			Iterator<UserPortfolioState> itr1=upsList.iterator();
			
	%>
			<table border="2px">
			<tr>
				<th>Name</th>
				<!-- <th>ISIN</th> -->
				<th>AssetAllocation</th>
				<th>PortfolioAllocation</th>
				<th>Segment</th>
				<th>SubSegment</th>
				<th>Blacklist</th>
				<th>Recommeded</th>
				<th>Reco</th>
				<th>Rating</th>
				<th>keep/remove</th>
				<th>CAV</th>
			<tr>
	<%
			
			
			while(itr1.hasNext())
			{
				UserPortfolioState ups=itr1.next();
				
				if(ups.getAssetType()==Asset.MUTUALFUND_ASSET_TYPE)
				{
					MutualFund mf=mfc.getFundById(ups.getAssetId());
					%>
						<tr>
							<td><%=mf.getName() %></td>
							<%-- <td><%=mf.getIsin() %></td> --%>
							<td><%=Math.round(ups.getAssetLevelAllocation()) %></td>
							<td><%=Math.round(ups.getPortfolioLevelAllocation()) %></td>
							<td><%=ups.getSegment() %></td>
							<td><%=ups.getSubSegment() %></td>
							<td><%=mfBlackListIdList.contains(ups.getAssetId())?"Y":"N" %></td>
							<td>N</td>
							<td><%=mfRecoList.contains(ups.getAssetId())?"Y":"N" %></td>
							<td><%=(mfRating_mfId_List.contains(ups.getAssetId())?"Y":"N")+"-"+mf.getOurRating() %></td>
							<td><%=mfKeep.contains(ups.getAssetId())?"KEEP":("REMOVE-"+mfRemoveMap.get(ups.getAssetId())) %></td>
							<td><%=ups.getCurrentAssetValue() %></td>
						</tr>
					<%
				}
			}
	%>
			</table>
			<br>
			<br>
			<table border="2px">
			<tr>
				<th>Name</th>
				<!-- <th>ISIN</th> -->
				<th>AssetAllocation</th>
				<th>PortfolioAllocation</th>
				<th>Segment</th>
				<th>SubSegment</th>
				<th>Blacklist</th>
				<th>Recommeded</th>
				<th>Reco</th>
				<th>Rating</th>
				<th>keep/remove</th>
				<th>CAV</th>
			<tr>
			<%
			StocksController sc=new StocksController();
			itr1=upsList.iterator();
			while(itr1.hasNext())
			{
				UserPortfolioState ups=itr1.next();
				
				if(ups.getAssetType()==Asset.STOCK_ASSET_TYPE)
				{
					Stocks stock=sc.getStockByScCode((int)ups.getAssetId());
					%>
						<tr>
							<td><%=stock.getScName() %></td>
							<%-- <td><%=stock.getIsinCode() %></td> --%>
							<td><%=Math.round(ups.getAssetLevelAllocation())%></td>
							<td><%=Math.round(ups.getPortfolioLevelAllocation())%></td>
							<td><%=ups.getSegment() %></td>
							<td><%=ups.getSubSegment() %></td>
							<td><%=stockBlackListIdList.contains((int)ups.getAssetId())?"Y":"N" %></td>
							<td><%=stockRecommendedList.contains((int)ups.getAssetId())?"Y":"N" %></td>
							<td><%=stockRecoList.contains(ups.getAssetId())?"Y":"N" %></td>
							<td><%=(stock_rating_List.contains((int)ups.getAssetId())?"Y":"N")+"-"+stock.getRating()*2 %></td>
							<td><%=stockKeep.contains((int)ups.getAssetId())?"KEEP":("REMOVE-"+stockRemoveMap.get((int)ups.getAssetId())) %></td>
							<td><%=ups.getCurrentAssetValue() %></td>
						</tr>
					<%
				}
			}
		%>
			</table>
		<%
		}
	}
	catch(FundexpertException fe)
	{
		fe.printStackTrace();
		out.print(fe.getMessage());
	}
	catch(Exception e)
	{
		e.printStackTrace();
		out.print("Internal Error.");
	}
	finally
	{
		hSession.close();
	}
%>
</body>
</html>