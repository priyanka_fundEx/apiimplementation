package com.rebalance.util;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.exception.FundexpertException;
import com.rebalance.pojo.IdealSegmentPercentage;
import com.rebalance.pojo.IdealStockSegmentPercentage;
import com.rebalance.pojo.IdealSubSegmentPercentage;
import com.rebalance.util.Utilities.FilesAction;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

public class IdealPercentageDistribution {
	
	
	String dirPath="/opt/rebalancing/config";
	final String idealSubSegmentPercentageFileName="idealSubSegment.txt",idealStockSegmentPercentageFileName="idealStockSegment.txt",idealSegmentPercentageFileName="idealSegment.txt";
	File dir=null,file=null;
	Utilities utilities=null;
	FilesAction filesAction=null;
	List<String> validSubSegmentList =null;
	List<String> validSegmentList = null;
	
	public IdealPercentageDistribution()
	{
		Session hSession=null;
		try
		{
			dir=new File(dirPath);
			if(!dir.isDirectory())
			{
				dir.mkdirs();
			}
			filesAction=new Utilities().new FilesAction();
			hSession=HibernateBridge.getSessionFactory().openSession();
			validSegmentList = hSession.createQuery("select distinct broaderSchemeType from MutualFund").list();
			validSubSegmentList = hSession.createQuery("select distinct broaderType from MutualFund").list();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}
	
	public boolean saveIdealSubSegmentPercentage(IdealSubSegmentPercentage idealSubSegment)
	{
		try
		{
			
			file=new File(dir.getAbsolutePath()+File.separator+idealSubSegmentPercentageFileName);
			if(!file.exists())
			{
				if(!file.createNewFile())
					throw new Exception("Not able to make file for IdealSubSegmentPercentageFileName.");
			}
			filesAction.serializeObject(idealSubSegment, file);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean saveIdealStockSegmentPercentage(IdealStockSegmentPercentage idealStockSegment)
	{
		try
		{
			
			file=new File(dir.getAbsolutePath()+File.separator+idealStockSegmentPercentageFileName);
			if(!file.exists())
			{
				if(!file.createNewFile())
					throw new Exception("Not able to make file for IdealStockSegmentPercentageFileName.");
			}
			filesAction.serializeObject(idealStockSegment, file);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean saveIdealSegmentPercentage(IdealSegmentPercentage idealSegment)
	{
		try
		{
			
			file=new File(dir.getAbsolutePath()+File.separator+idealSegmentPercentageFileName);
			if(!file.exists())
			{
				if(!file.createNewFile())
					throw new Exception("Not able to make file for IdealSegmentPercentageFileName.");
			}
			filesAction.serializeObject(idealSegment, file);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public IdealSubSegmentPercentage getCurrentIdealSubSegmentPercentage() throws FundexpertException
	{
		try
		{
			File file=new File(dir.getAbsoluteFile()+File.separator+idealSubSegmentPercentageFileName);
			if(!file.exists())
				saveIdealSubSegmentPercentage(new IdealSubSegmentPercentage());
			Object object=filesAction.readFile(file);
			if(object==null)
				throw new FundexpertException("Could not read IdealSubSegmentPercentage file.");
			return (IdealSubSegmentPercentage)object;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public IdealSegmentPercentage getCurrentIdealSegmentPercentage() throws FundexpertException
	{
		try
		{
			File file=new File(dir.getAbsoluteFile()+File.separator+idealSegmentPercentageFileName);
			if(!file.exists())
				saveIdealSegmentPercentage(new IdealSegmentPercentage());
			Object object=filesAction.readFile(file);
			if(object==null)
				throw new FundexpertException("Could not read IdealSegmentPercentage file.");
			return (IdealSegmentPercentage)object;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public IdealStockSegmentPercentage getCurrentIdealStockSegmentPercentage() throws FundexpertException
	{
		try
		{
			File file=new File(dir.getAbsoluteFile()+File.separator+idealStockSegmentPercentageFileName);
			if(!file.exists())
				saveIdealStockSegmentPercentage(new IdealStockSegmentPercentage());
			Object object=filesAction.readFile(file);
			if(object==null)
				throw new FundexpertException("Could not read IdealMutualFundSegmentPercentage file.");
			return (IdealStockSegmentPercentage)object;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public boolean saveIdealSubSegment(double lc,double sc,double mc,double lt,double st,int riskProfile,long consumerId,String subSegment)
	{
		IdealSubSegmentPercentage issp;
		String segment=null;
		try {
			if(!validSubSegmentList.contains(subSegment))
				throw new FundexpertException("Invalid SubSegment passed.");
			if(subSegment.equalsIgnoreCase("large cap") || subSegment.equalsIgnoreCase("small cap") || subSegment.equalsIgnoreCase("mid cap"))
				segment="EQUITY";
			else
				segment="DEBT";
			issp = getCurrentIdealSubSegmentPercentage();
			
			Map<String,Double> map=null;
			Map<Long,Map<Integer,Map<String,Map<String,Double>>>> map3=issp.getMap();
			Map<Integer,Map<String,Map<String,Double>>> map21=map3.get(consumerId);
			if(map21==null)
				map21=new HashMap<Integer,Map<String,Map<String,Double>>>();
			Map<String,Map<String,Double>> map22=map21.get(riskProfile);
			if(map22==null)
				map22=new HashMap<String,Map<String,Double>>();
			map=new HashMap<String,Double>();
			map.put("LONG TERM",lt);
			map.put("SHORT TERM",st);
			map22.put("DEBT", map);
			map=new HashMap<String,Double>();
			map.put("LARGE CAP",lc);
			map.put("MID CAP",mc);
			map.put("SMALL CAP",sc);
			map22.put("EQUITY", map);
			map21.put(riskProfile, map22);
			map3.put(consumerId, map21);
			System.out.println(" map3 = "+map3);
		
			return saveIdealSubSegmentPercentage(issp);
		} 
		catch (FundexpertException e) {
			e.printStackTrace();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public Map<String,Double> getCurrentIdealStockSegmentPercentage(long consumerId,int riskProfile) throws FundexpertException
	{
		Map<String,Double> map=new HashMap<String,Double>();
		Session hSession=null;
		int sumAllocation=0;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Object[]> list=hSession.createQuery("select allocation,sector from SectorAllocation where consumerId=? and riskProfile=?").setLong(0, consumerId).setInteger(1, riskProfile).list();
			for(Object[] obj:list)
			{
				int allocation=(Integer)obj[0];
				String sector=(String)obj[1];
				sumAllocation+=allocation;
				map.put(sector, (double)allocation);
			}
			if(sumAllocation!=100)
				throw new FundexpertException("Allocation for Stock Segments does not seem to be 100.");
			return map;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new FundexpertException("Not able to fetch sector alloction.");
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String,Map<String,Double>> getCurrentIdealSubSegmentPercentage(long consumerId,int riskProfile) throws FundexpertException
	{
		Map<String,Map<String,Double>> map=new HashMap<String,Map<String,Double>>();
		Map<String,Double> map1=null;
 		Session hSession=null;
 		int sumAllocation=0;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Object[]> list=hSession.createQuery("select allocation,subSegment from SubSegmentAllocation where consumerId=? and riskProfile=?").setLong(0, consumerId).setInteger(1, riskProfile).list();
			for(Object[] obj:list)
			{
				int allocation=(Integer)obj[0];
				sumAllocation+=allocation;
				String subSegment=(String)obj[1];
				if(subSegment.equalsIgnoreCase("long term") || subSegment.equalsIgnoreCase("short term"))
				{
					map1=map.get("DEBT");
					if(map1==null)
					{
						map1=new HashMap<String,Double>();
						map.put("DEBT", map1);
					}
					map1.put(subSegment, (double)allocation);
				}
				else
				{
					map1=map.get("EQUITY");
					if(map1==null)
					{
						map1=new HashMap<String,Double>();
						map.put("EQUITY", map1);
					}
					map1.put(subSegment, (double)allocation);
				}
			}
			if(sumAllocation!=100)
				throw new FundexpertException("Allocation for SubSegment does not seem to be 100.");
			return map;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new FundexpertException("Not able to fetch sector alloction.");
		}
		finally
		{
			hSession.close();
		}
	}
	
}
//according to edelweiss rebalance module, we look at subSegment overall i.e largeCap=(totalLargeCap(MF+Direct equity)/totalPortfolio)*100
