package com.rebalance.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.config.Config;
import com.fundexpert.controller.MutualFundController;
import com.fundexpert.controller.StocksController;
import com.fundexpert.controller.UserPortfolioStateController;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.StocksInStockSegment;
import com.fundexpert.dao.User;
import com.fundexpert.dao.UserPortfolioState;
import com.fundexpert.exception.FundexpertException;
import com.rebalance.pojo.IdealSegmentPercentage;
import com.rebalance.pojo.IdealStockSegmentPercentage;
import com.rebalance.pojo.IdealSubSegmentPercentage;

public class SchemeLevelSuggestion {

	long userId=0;
	int riskProfile=0;
	Config config=null;
	User user=null;
	List<Long> mfKeep=null;
	List<Long> mfRemove=null;
	Map<Long,String> mfRemoveMap=null;
	List<Integer> stockKeep=null;
	List<Integer> stockRemove=null;
	Map<Integer,String> stockRemoveMap=null;
	List<Integer> stockRecommendedList=null;
	List<Long> mfRecoList=null;
	List<Integer> stockRecoList=null;
	List<Long> mfRating_mfId_List=null;
	List<Integer> stock_rating_List=null;
	List<Long> mfBlackListIdList=null;
	List<Integer> stockBlackListIdlList=null;
	
	IdealSubSegmentPercentage idealSubSegment=null;
	IdealStockSegmentPercentage idealStockSegment=null;
	IdealSegmentPercentage idealSegment=null;
	/*Map<Long,UserPortfolioState> mfInitialAssetMap=null;
	Map<Integer,UserPortfolioState> stockInitialAssetMap=null;
	Map<Long,UserPortfolioState> mfAfterSuggestionAssetMap=null;
	Map<Integer,UserPortfolioState> stockAfterSuggestionAssetMap=null;*/
	
	final String REMOVAL_REASON_1="BLACKLISTED",REMOVAL_REASON_2="ASSET_ALLOCATION",REMOVAL_REASON_3="EDELWEISS_RECOMMENDED_PORTFOLIO",REMOVAL_REASON_4="EDELWEISS_RECO",REMOVAL_REASON_5="FE_MUTUALFUND_RATING",REMOVAL_REASON_6="FE_STOCK_RATING";
	double totalDE=0,totalEMF=0,totalDMF=0,totalCash=0,totalPortfolioAmount=0;
	double totalDEAfterSuggestion=0,totalEMFAfterSuggestion=0,totalDMFAfterSuggestion=0,totalCashAfterSuggestionForStock=0,totalCashAfterSuggestionForMF=0,totalPortfolioAmountAfterSuggestion=0;
	//below 6 maps stores percentageAllocation
	//contains both DE and MF(Equity/Debt)
	Map<String,Double> segmentInitialAssetAllocationMap=null;
	Map<String,Double> segmentAfterSuggestionAllocationMap=null;
	//contains EQUITY and its LC,SC,MC and LT,ST(Debt) and LC,MC,SC(DIRECTEQUITY)
	Map<String,Map<String,Double>> subSegmentInitialAssetAllocationMap=null;
	Map<String,Map<String,Double>> subSegmentAfterSuggestionAllocationMap=null;
	//contains Segments of stocks or say sectors
	Map<String,Double> stockSegmentInitialAssetAllocationMap=null;
	Map<String,Double> stockSegmentAfterSuggestionAllocationMap=null;
	
	//currently made to test values.
	//below 6 maps store current amountDistribution in absolute value
	Map<String,Double> segmentInitialAssetValueMap=null;
	Map<String,Double> segmentAfterSuggestionAssetValueMap=null;
	//contains both LC,SC,MC(DirectEquity/MF) and LT,ST(Debt)
	Map<String,Map<String,Double>> subSegmentInitialAssetValueMap=null;
	Map<String,Map<String,Double>> subSegmentAfterSuggestionAssetValueMap=null;
	//contains Segments of stocks or say sectors
	Map<String,Double> stockSegmentInitialAssetValueMap=null;
	Map<String,Double> stockSegmentAfterSuggestionAssetValueMap=null;
	
	//list contains
	Map<String,List<UserPortfolioState>> segmentWiseUserPortfolioStateStockMap=null;//E/D
	Map<String,List<UserPortfolioState>> subSegmentWiseUserPortfolioStateStockMap=null;//LC/MC/SC/LT/ST where subSegments include type stock
	Map<String,List<UserPortfolioState>> stockSegmentWiseUserPortfolioStateMap=null;//IT/AGRI etc.
	
	//list contains
	Map<String,List<UserPortfolioState>> segmentWiseUserPortfolioStateMFMap=null;//E/D
	Map<String,List<UserPortfolioState>> subSegmentWiseUserPortfolioStateMFMap=null;//LC/MC/SC/LT/ST where subSegments includetype MF
	
	//gives all stocks corresponding to key which is stock sector 
	Map<String,List<Stocks>> segmentWiseStockMap=null;
	//gives all mf corresponding to key which is subSegment i.e LARGE CAP,MID CAP,SMALL CAP
	Map<String,List<MutualFund>> subSegmentWiseMFMap=null;
	//gives all stocks corresponding to key which is subSegment i.e LARGE CAP,MID CAP,SMALL CAP
	//most probably we will not use below map but might use in future without removing this comment
	Map<String,List<Stocks>> subSegmentWiseStockMap=null;
	
	//usersStockMap is mapping of scCode with stock which are in UserPortfolioState which is used at the time of adding Stocks to blacklist while buying extra stocks in 'ALGO' class
	Map<Integer,Stocks> usersStockMap=null;
	Map<Long,MutualFund> usersMutualFundMap=null;
	
	List<UserPortfolioState> userPortfolioStateList=null;
	
	public SchemeLevelSuggestion(long userId,int riskProfile)
	{
		try {
			this.userId=userId;
			config=new Config();
			mfKeep=new ArrayList<Long>();
			mfRemove=new ArrayList<Long>();
			mfRemoveMap=new HashMap<Long,String>();
			stockKeep=new ArrayList<Integer>();
			stockRemove=new ArrayList<Integer>();
			stockRemoveMap=new HashMap<Integer,String>();
			
			//mfInitialAssetMap=new HashMap<Long,UserPortfolioState>();
			//stockInitialAssetMap=new HashMap<Integer,UserPortfolioState>();
			//mfAfterSuggestionAssetMap=new HashMap<Long,UserPortfolioState>();
			//stockAfterSuggestionAssetMap=new HashMap<Integer,UserPortfolioState>();
			
			segmentInitialAssetAllocationMap=new HashMap<String,Double>();
			segmentAfterSuggestionAllocationMap=new HashMap<String,Double>();
			subSegmentInitialAssetAllocationMap=new HashMap<String,Map<String,Double>>();
			subSegmentAfterSuggestionAllocationMap=new HashMap<String,Map<String,Double>>();
			stockSegmentInitialAssetAllocationMap=new HashMap<String,Double>();
			stockSegmentAfterSuggestionAllocationMap=new HashMap<String,Double>();
			
			
			segmentInitialAssetValueMap=new HashMap<String,Double>();
			segmentAfterSuggestionAssetValueMap=new HashMap<String,Double>();
			subSegmentInitialAssetValueMap=new HashMap<String,Map<String,Double>>();
			subSegmentAfterSuggestionAssetValueMap=new HashMap<String,Map<String,Double>>();
			stockSegmentInitialAssetValueMap=new HashMap<String,Double>();
			stockSegmentAfterSuggestionAssetValueMap=new HashMap<String,Double>();
			
			segmentWiseUserPortfolioStateStockMap=new HashMap<String,List<UserPortfolioState>>();
			subSegmentWiseUserPortfolioStateStockMap=new HashMap<String,List<UserPortfolioState>>();
			stockSegmentWiseUserPortfolioStateMap=new HashMap<String,List<UserPortfolioState>>();
			
			segmentWiseUserPortfolioStateMFMap=new HashMap<String,List<UserPortfolioState>>();
			subSegmentWiseUserPortfolioStateMFMap=new HashMap<String,List<UserPortfolioState>>();
			
			segmentWiseStockMap=new HashMap<String,List<Stocks>>();
			subSegmentWiseMFMap=new HashMap<String,List<MutualFund>>();
			
			usersStockMap=new HashMap<Integer,Stocks>();
			usersMutualFundMap=new HashMap<Long,MutualFund>();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getList(long consumerId) throws Exception
	{
		Session hSession=null;
		try
		{
			double mf_rating=Double.valueOf(config.getProperty(Config.MF_RATING));
			double stock_rating=Double.valueOf(config.getProperty(Config.STOCK_RATING));
			hSession=HibernateBridge.getSessionFactory().openSession();
			mfBlackListIdList=hSession.createQuery("select mutualFundId from BlackListFunds where consumerId=? and mutualFundId is not null").setLong(0,consumerId).list();
			stockBlackListIdlList=hSession.createQuery("select scCode from BlackListFunds where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			stockRecommendedList=hSession.createQuery("select scCode from RecommendedPortfolio where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			mfRecoList=hSession.createQuery("select mutualFundId from Reco where consumerId=? and mutualFundId is not null").setLong(0, consumerId).list();
			stockRecoList=hSession.createQuery("select scCode from Reco where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			mfRating_mfId_List=hSession.createQuery("select id from MutualFund where ourRating>=? order by ourRating").setDouble(0, mf_rating).list();
			stock_rating_List=hSession.createQuery("select scCode from Stocks where rating>=? order by rating").setDouble(0, stock_rating).list();
			
			//below patch will give us list of stocks for each distinct sector 
			IdealPercentageDistribution ipd=new IdealPercentageDistribution();
			
			Map<String,Double> map=ipd.getCurrentIdealStockSegmentPercentage(consumerId,riskProfile);
			for(Map.Entry<String, Double> entry:map.entrySet())
			{
				System.out.println("Sector="+entry.getKey()+" percentage Allcoation = "+entry.getValue());
				List<Stocks> stockList=hSession.createQuery("select s from StocksInStockSegment ss,Stocks s where s.scCode=ss.scCode and ss.segment=? and ss.consumerId=? and ss.riskProfile = 0").setString(0,entry.getKey()).setLong(1, consumerId).list();
				if(stockList==null || stockList.size()==0)
				{
					throw new FundexpertException("No Stock exists in "+entry.getKey()+" sector.Atleast 1 stock in each sector should be there for each sector in ideal sector allocation.");
				}
				else
				{
					segmentWiseStockMap.put(entry.getKey(), stockList);
				}
			}
			
			//below patch will give us list of MF for each distinct subSegment
			
			Map<String,Map<String,Double>> map1 =ipd.getCurrentIdealSubSegmentPercentage(consumerId,riskProfile); 
			Map<String,Double> idealSubSegmentMap = map1.get("EQUITY");
			idealSubSegmentMap.putAll(map1.get("DEBT"));
			for(Map.Entry<String, Double> entry:idealSubSegmentMap.entrySet())
			{
				System.out.println("SubSegment = "+entry.getKey()+" percentage Allcoation = "+entry.getValue());
				List<MutualFund> mfList=hSession.createQuery("select mf from AssetsInSubSegment ais,MutualFund mf where ais.assetType=? and ais.riskProfile=? and ais.consumerId=? and ais.subSegment=? and mf.id=ais.assetId").setInteger(0,Asset.MUTUALFUND_ASSET_TYPE).setInteger(1, 0).setLong(2, consumerId).setString(3, entry.getKey()).list();
				if(mfList==null || mfList.size()==0)
					throw new FundexpertException("No MF exists in "+entry.getKey()+" subSegment.Atleast 1 Mf in each subSegment should be there for each SubSegment in ideal subSegment allocation.");
				else
					subSegmentWiseMFMap.put(entry.getKey(),mfList);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Exception Occurred.");
		}
		finally
		{
			hSession.close();
		}
	}
	
	public void setInitialCash(double cashValue)
	{
		totalCash+=cashValue;
	}
	
	public void getFirstLevelSuggestion() throws FundexpertException,Exception
	{
		Session hSession=null;
		Transaction tx=null;
		long consumerId=0;
		
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			//get distinct segments 
			List<String> segmentList=hSession.createQuery("select distinct broaderSchemeType from MutualFund where broaderSchemeType is not null and broaderSchemeType != ''").list();
			List<String> subSegmentList=hSession.createQuery("select distinct broaderType from MutualFund where broaderType is not null and broaderType != ''").list();
			List<String> stockSegmentList=hSession.createQuery("select distinct segment from Stocks where segment is not null and segment !=''").list();
			List<String> stockSubSegmentList=hSession.createQuery("select distinct type from Stocks where type is not null and type !=''").list();
			
			User user=(User)hSession.get(User.class, userId);
			if(user==null)
				throw new FundexpertException("No user selected.");
			//when consumer calls for rebalance we do filtering on the basis of blacklist,recommended,reco list for this user call this
			//transfer his existing userPortfolioState to ArchiveUserPortfolioState
			//then delete existing UserPortfolioState and make new UserPortfolioState so that currentAssetAllocation(current NAV) can be visible
			
			tx=hSession.beginTransaction();
			String insertQuery="insert into ArchiveUserPortfolioState(userId,assetType,assetId,liability,assetLevelAllocation,portfolioLevelAllocation,currentAssetValue,lastUpdatedOn,assetName,insertedIntoArchiveOn)"+
			" select userId,assetType,assetId,liability,assetLevelAllocation,portfolioLevelAllocation,currentAssetValue,lastUpdatedOn,assetName,now() from UserPortfolioState where userId=?";
			int updatedRows=hSession.createQuery(insertQuery).setParameter(0, userId).executeUpdate();
			System.out.println("Update rows="+updatedRows);
			tx.commit();
			if(updatedRows>0)
			{
				tx=hSession.beginTransaction();
				String deleteQuery="delete from UserPortfolioState where userId=?";
				int deletedRows=hSession.createQuery(deleteQuery).setLong(0, userId).executeUpdate();
				System.out.println("Deleted rwos="+deletedRows);
				tx.commit();
			}
			UserPortfolioStateController upsc=new UserPortfolioStateController();
			//TODO uncomment below line, and also tx.commit() and delete query in if(updatedRows>0)
			upsc.insertDataIntoUserPortfolioState(userId);
			consumerId=user.getConsumerId();
			getList(consumerId);
			
			userPortfolioStateList=hSession.createQuery("from UserPortfolioState where userId=?").setLong(0, userId).list();
			System.out.println("Mutualfund Black list size="+mfBlackListIdList.size()+"  Stock Black list size="+stockBlackListIdlList.size()+" Size of userPortfolioState="+userPortfolioStateList.size());
			double portfolioLevelAllocation=0,currentAssetValue=0,assetLevelAllocation=0;
			
			StocksController stocksController=null;
			MutualFundController mfController=null;
			String segment;
			for(UserPortfolioState ups:userPortfolioStateList)
			{
				System.out.println("ups id="+ups.getId());
				portfolioLevelAllocation=ups.getPortfolioLevelAllocation();currentAssetValue=ups.getCurrentAssetValue();assetLevelAllocation=ups.getAssetLevelAllocation();
				totalPortfolioAmount+=currentAssetValue;
				System.out.println("CurrentAssetValue="+currentAssetValue);
				if(ups.getAssetType()==Asset.MUTUALFUND_ASSET_TYPE)
				{
					if(ups.getSubSegment().equalsIgnoreCase("LARGE CAP") || ups.getSubSegment().equalsIgnoreCase("MID CAP") || ups.getSubSegment().equalsIgnoreCase("SMALL CAP"))
					{
						totalEMF+=ups.getCurrentAssetValue();
					}
					else
					{
						totalDMF+=ups.getCurrentAssetValue();
					}
					if(mfController==null)
						mfController=new MutualFundController();
					String broaderSegment=ups.getSegment(),broaderSubSegment=ups.getSubSegment();
					segment=broaderSegment;	
					
					if(segmentList.contains(broaderSegment))
					{
						Double thisSegmentAllocation=segmentInitialAssetAllocationMap.get(broaderSegment);
						if(thisSegmentAllocation!=null)
						{
							thisSegmentAllocation+=portfolioLevelAllocation;
						}
						else 
						{
							thisSegmentAllocation=portfolioLevelAllocation;
						}
						segmentInitialAssetAllocationMap.put(broaderSegment, thisSegmentAllocation);
						Double thisSegmentCurrentValue=segmentInitialAssetValueMap.get(broaderSegment);
						if(thisSegmentCurrentValue!=null)
						{
							System.out.println("Got value="+thisSegmentCurrentValue+" becoz of segment = "+broaderSegment);
							thisSegmentCurrentValue+=currentAssetValue;
							System.out.println("thisSegmentCurrentValue="+thisSegmentCurrentValue);
						}
						else
						{
							thisSegmentCurrentValue=currentAssetValue;
						}
						segmentInitialAssetValueMap.put(broaderSegment, thisSegmentCurrentValue);
					}
					else
					{
						throw new FundexpertException("In FirstLevelSuggestion Mf's Segment in user's portfolio state does not qualify for E/D.");
					}
					if(subSegmentList.contains(broaderSubSegment))
					{
						Map<String,Double> subSegmentMap=subSegmentInitialAssetAllocationMap.get(segment);
						Double thisSubSegmentAllocation=0d;
						if(subSegmentMap!=null)
						{
							thisSubSegmentAllocation=subSegmentMap.containsKey(broaderSubSegment)?subSegmentMap.get(broaderSubSegment):0d;
							thisSubSegmentAllocation+=portfolioLevelAllocation;
						}
						else
						{
							subSegmentMap=new HashMap<String,Double>();
							subSegmentInitialAssetAllocationMap.put(segment, subSegmentMap);
							thisSubSegmentAllocation=portfolioLevelAllocation;
						}
						subSegmentMap.put(broaderSubSegment, thisSubSegmentAllocation);
						
						Map<String,Double> subSegmentValueMap=subSegmentInitialAssetValueMap.get(segment);
						Double thisSubSegmentCurrentValue=0d;
						if(subSegmentValueMap!=null)
						{
							thisSubSegmentCurrentValue=subSegmentValueMap.containsKey(broaderSubSegment)?subSegmentValueMap.get(broaderSubSegment):0d;
							thisSubSegmentCurrentValue+=currentAssetValue;
						}
						else
						{
							subSegmentValueMap=new HashMap<String,Double>();
							subSegmentInitialAssetValueMap.put(segment, subSegmentValueMap);
							thisSubSegmentCurrentValue=currentAssetValue;
						}
						subSegmentValueMap.put(broaderSubSegment, thisSubSegmentCurrentValue);
					}
					else
					{
						throw new FundexpertException("In FirstLevelSuggestion Mf's SubSegment in user's portfolio state does not qualify for LC/SC/MC/LT/ST.");
					}
					long assetId=ups.getAssetId();
					System.out.println("Asset ID = "+assetId);
					if(mfBlackListIdList.contains(assetId))
					{
						mfRemove.add(assetId);
						mfRemoveMap.put(assetId, REMOVAL_REASON_1);
						System.out.println("Rmoved mf bcoz of blacklisting.");
						totalCashAfterSuggestionForMF+=currentAssetValue;
						//since we are removing such MF we'll use userPortfolioStateList to deliver final output
						ups.setCurrentAssetValue(0.0);
						ups.setAssetLevelAllocation(0.0);
						ups.setPortfolioLevelAllocation(0.0);
						continue;
					}
					else
					{
						if(ups.getAssetLevelAllocation()>50 || ups.getAssetLevelAllocation()<5)
						{
							mfRemove.add(assetId);
							mfRemoveMap.put(assetId, REMOVAL_REASON_2);
							System.out.println("Removed because of size allocation with assetLevel Allocation="+ups.getAssetLevelAllocation());
							totalCashAfterSuggestionForMF+=currentAssetValue;
							ups.setCurrentAssetValue(0.0);
							ups.setAssetLevelAllocation(0.0);
							ups.setPortfolioLevelAllocation(0.0);
							continue;
						}
						else
						{
							if(mfRecoList.contains(assetId) || mfRating_mfId_List.contains(assetId))
							{
								mfKeep.add(assetId);
								totalPortfolioAmountAfterSuggestion+=currentAssetValue;
								System.out.println("Keep the MF.");
								//now since we are keeping this mutualFund add its asset value to 'segmentAfterSuggestionAssetValueMap' which contains ("EQUITY"/"DEBT") and 'subSegmentAfterSuggestionAssetValueMap' whcih contains ("LC"/"MC"/"SM"/"LT"/"ST")
								//for Segment
								Double currentSegmentValueAfterSuggestion=segmentAfterSuggestionAssetValueMap.get(broaderSegment);
								if(currentSegmentValueAfterSuggestion!=null)
								{
									currentSegmentValueAfterSuggestion+=currentAssetValue;
								}
								else
								{
									currentSegmentValueAfterSuggestion=currentAssetValue;
								}
								segmentAfterSuggestionAssetValueMap.put(broaderSegment, currentSegmentValueAfterSuggestion);
								//for SubSegment
								Map<String,Double> subSegmentValueMap=subSegmentAfterSuggestionAssetValueMap.get(segment);
								Double currentSubSegmentValueAfterSuggestion=0d;
								if(subSegmentValueMap!=null)
								{
									currentSubSegmentValueAfterSuggestion=subSegmentValueMap.containsKey(broaderSubSegment)?subSegmentValueMap.get(broaderSubSegment):0d;
									currentSubSegmentValueAfterSuggestion+=currentAssetValue;
								}
								else
								{
									subSegmentValueMap=new HashMap<String,Double>();
									subSegmentAfterSuggestionAssetValueMap.put(segment, subSegmentValueMap);
									currentSubSegmentValueAfterSuggestion=currentAssetValue;
								}
								subSegmentValueMap.put(broaderSubSegment, currentSubSegmentValueAfterSuggestion);
								
								getMutualFundWiseUserPortfolioStateAfterSuggestion(ups,broaderSegment,broaderSubSegment);
								usersMutualFundMap.put(assetId, mfController.getFundById(assetId));
								continue;
							}
							else
							{
								if(!mfRecoList.contains(assetId))
								{
									mfRemove.add(assetId);
									mfRemoveMap.put(assetId, REMOVAL_REASON_4);
									System.out.println("Removed bcoz it is not in reco list.");
									totalCashAfterSuggestionForMF+=currentAssetValue;
									ups.setCurrentAssetValue(0.0);
									ups.setAssetLevelAllocation(0.0);
									ups.setPortfolioLevelAllocation(0.0);
									continue;
								}
								else if(!mfRating_mfId_List.contains(assetId))
								{
									mfRemove.add(assetId);
									mfRemoveMap.put(assetId, REMOVAL_REASON_5);
									System.out.println("Removed bcoz of bad rating.");
									totalCashAfterSuggestionForMF+=currentAssetValue;
									ups.setCurrentAssetValue(0.0);
									ups.setAssetLevelAllocation(0.0);
									ups.setPortfolioLevelAllocation(0.0);
									continue;
								}
							}
						}
					}
				}
				else if(ups.getAssetType()==Asset.STOCK_ASSET_TYPE)
				{
					segment="DIRECTEQUITY";
					totalDE+=ups.getCurrentAssetValue();
					if(stocksController==null)
						stocksController=new StocksController();
					//we will take stocks as 'DIRECTEQUITY' as segment
					Double currentSegmentAllocation=0d;
					if(segmentInitialAssetAllocationMap.containsKey(segment))
					{
						currentSegmentAllocation=segmentInitialAssetAllocationMap.get(segment);
						currentSegmentAllocation+=ups.getPortfolioLevelAllocation();
					}
					else
					{
						currentSegmentAllocation=ups.getPortfolioLevelAllocation();
					}
					segmentInitialAssetAllocationMap.put(segment,currentSegmentAllocation);
					Double currentSegmentValue=0d;
					if(segmentInitialAssetValueMap.containsKey(segment))
					{
						currentSegmentValue=segmentInitialAssetValueMap.get(segment);
						currentSegmentValue+=ups.getCurrentAssetValue();
					}
					else
					{
						currentSegmentValue=ups.getCurrentAssetValue();
					}
					segmentInitialAssetValueMap.put(segment,currentSegmentValue);
					
					//stockSegment is sector
					String stockSegment=ups.getSegment(),subSegment=ups.getSubSegment();
					if(stockSubSegmentList.contains(subSegment))
					{
						Map<String,Double> stockSubSegmentMap=subSegmentInitialAssetAllocationMap.get(segment);
						Double stockSubSegmentAllocation=0d;
						if(stockSubSegmentMap!=null)
						{
							stockSubSegmentAllocation=stockSubSegmentMap.containsKey(subSegment)?stockSubSegmentMap.get(subSegment):0d;
							stockSubSegmentAllocation+=portfolioLevelAllocation;
						}
						else
						{
							stockSubSegmentMap=new HashMap<String,Double>();
							subSegmentInitialAssetAllocationMap.put(segment, stockSubSegmentMap);
							stockSubSegmentAllocation=portfolioLevelAllocation;
						}
						stockSubSegmentMap.put(subSegment,stockSubSegmentAllocation);
						Map<String,Double> stockSubSegmentValueMap=subSegmentInitialAssetValueMap.get(segment);
						Double stockSubSegmentCurrentValue=0d;
						if(stockSubSegmentValueMap!=null)
						{
							stockSubSegmentCurrentValue=stockSubSegmentValueMap.containsKey(subSegment)?stockSubSegmentValueMap.get(subSegment):0d;
							stockSubSegmentCurrentValue+=currentAssetValue;
						}
						else
						{
							stockSubSegmentValueMap=new HashMap<String,Double>();
							subSegmentInitialAssetValueMap.put(segment, stockSubSegmentValueMap);
							stockSubSegmentCurrentValue=currentAssetValue;
						}
						stockSubSegmentValueMap.put(subSegment,stockSubSegmentCurrentValue);
					}
					else
					{
						throw new FundexpertException("In FirstLevelSuggestion where stock in user's portfolio state does not contain valid SubSegment.");
					}
					if(stockSegmentList.contains(stockSegment))
					{
						Double stockSegmentAllocation=stockSegmentInitialAssetAllocationMap.get(stockSegment);
						if(stockSegmentAllocation!=null)
						{
							stockSegmentAllocation+=assetLevelAllocation;
						}
						else
						{
							stockSegmentAllocation=assetLevelAllocation;
						}
						stockSegmentInitialAssetAllocationMap.put(stockSegment, stockSegmentAllocation);
						Double stockSegmentCurrentValue=stockSegmentInitialAssetValueMap.get(stockSegment);
						if(stockSegmentCurrentValue!=null)
						{
							stockSegmentCurrentValue+=currentAssetValue;
						}
						else
						{
							stockSegmentCurrentValue=currentAssetValue;
						}
						stockSegmentInitialAssetValueMap.put(stockSegment,stockSegmentCurrentValue);
					}
					else
					{
						throw new FundexpertException("In FirstLevelSuggestion where stock in user's portfolio state does not contain valid Segment.");
					}
					
					int assetId=(int)ups.getAssetId();
					if(stockBlackListIdlList.contains(assetId))
					{
						stockRemove.add(assetId);
						stockRemoveMap.put(assetId, REMOVAL_REASON_1);
						System.out.println("Removed stock with scCode = "+assetId+" REASON="+REMOVAL_REASON_1);
						totalCashAfterSuggestionForStock+=currentAssetValue;
						ups.setCurrentAssetValue(0.0);
						ups.setAssetLevelAllocation(0.0);
						ups.setPortfolioLevelAllocation(0.0);
						continue;
					}
					else
					{
						if(ups.getAssetLevelAllocation()>50 || ups.getAssetLevelAllocation()<1)
						{
							stockRemove.add(assetId);
							stockRemoveMap.put(assetId, REMOVAL_REASON_2);
							System.out.println("Rmoved bcoz = "+REMOVAL_REASON_2);
							totalCashAfterSuggestionForStock+=currentAssetValue;
							ups.setCurrentAssetValue(0.0);
							ups.setAssetLevelAllocation(0.0);
							ups.setPortfolioLevelAllocation(0.0);
							continue;
							
						}
						else
						{
							if(stockRecommendedList.contains(assetId) || stockRecoList.contains(assetId) || stock_rating_List.contains(assetId))
							{
								stockKeep.add(assetId);
								totalPortfolioAmountAfterSuggestion+=currentAssetValue;
								totalDEAfterSuggestion+=currentAssetValue;
								System.out.println("stock keep");
								//since we have kept this stock , we will add its currentStockValue to 'DIRECTEQUITY' key in 'segmentAfterSuggestionAssetValueMap'
								//also we'll add it to subSegment (if LC then to largeCap similarly for SC,MC) in 'subSegmentAfterSuggestionAssetValueMap'
								//also we'll add it to stockSegment map which is actually stock sectors
								Double currentSegmentValueAfterSuggestion=segmentAfterSuggestionAssetValueMap.get(segment);
								if(currentSegmentValueAfterSuggestion!=null)
								{
									currentSegmentValueAfterSuggestion+=currentAssetValue;
								}
								else
								{
									currentSegmentValueAfterSuggestion=currentAssetValue;
								}
								segmentAfterSuggestionAssetValueMap.put(segment, currentSegmentValueAfterSuggestion);
								Map<String,Double> subSegmentMap=subSegmentAfterSuggestionAssetValueMap.get(segment);
								Double currentSubSegmentValue=0d;
								if(subSegmentMap!=null)
								{
									currentSubSegmentValue=subSegmentMap.containsKey(subSegment)?subSegmentMap.get(subSegment):0d;
									currentSubSegmentValue+=currentAssetValue;
									System.out.println("SubSegment was null");
								}
								else
								{
									System.out.println("SubSegment was not null");
									subSegmentMap=new HashMap<String,Double>();
									subSegmentAfterSuggestionAssetValueMap.put(segment, subSegmentMap);
									currentSubSegmentValue=currentAssetValue;
								}
								System.out.println("Setting value in subSegment = "+subSegment+"as "+currentSubSegmentValue);
								subSegmentMap.put(subSegment,currentSubSegmentValue);
								Double currentSectorValueAfterSuggestion=stockSegmentAfterSuggestionAssetValueMap.get(stockSegment);
								if(currentSectorValueAfterSuggestion!=null)
								{
									currentSectorValueAfterSuggestion+=currentAssetValue;
								}
								else
								{
									currentSectorValueAfterSuggestion=currentAssetValue;
								}
								stockSegmentAfterSuggestionAssetValueMap.put(stockSegment, currentSectorValueAfterSuggestion);
								getStockWiseUserPortfolioStateAfterSuggestion(ups,segment,stockSegment,subSegment);
								
								System.out.println("Inserting assetId="+assetId+" in usersStockMap");
								usersStockMap.put((Integer)assetId, stocksController.getStockByScCode((int)assetId));
								continue;
							}
							else
							{
								if(!stockRecommendedList.contains(assetId))
								{
									stockRemove.add(assetId);
									stockRemoveMap.put(assetId, REMOVAL_REASON_3);
									System.out.println("Removal Reason ="+REMOVAL_REASON_3);
									totalCashAfterSuggestionForStock+=currentAssetValue;
									ups.setCurrentAssetValue(0.0);
									ups.setAssetLevelAllocation(0.0);
									ups.setPortfolioLevelAllocation(0.0);
									continue;
								}
								else if(!stockRecoList.contains(assetId))
								{
									stockRemove.add(assetId);
									stockRemoveMap.put(assetId, REMOVAL_REASON_4);
									System.out.println("Removal Reason = "+REMOVAL_REASON_4);
									totalCashAfterSuggestionForStock+=currentAssetValue;
									ups.setCurrentAssetValue(0.0);
									ups.setAssetLevelAllocation(0.0);
									ups.setPortfolioLevelAllocation(0.0);
									continue;
								}
								else if(!stock_rating_List.contains(assetId))
								{
									stockRemove.add(assetId);
									stockRemoveMap.put(assetId, REMOVAL_REASON_6);
									System.out.println("Removal Reason = "+REMOVAL_REASON_6);
									totalCashAfterSuggestionForStock+=currentAssetValue;
									ups.setCurrentAssetValue(0.0);
									ups.setAssetLevelAllocation(0.0);
									ups.setPortfolioLevelAllocation(0.0);
									continue;
								}
							}
						}
					}
					
				}
			}
			
			//after successfully completion of firsstLevelSuggestion we generate new percentage Allocation
			segmentAfterSuggestionAllocationMap=getPercentageAllocationMap(totalPortfolioAmount,segmentAfterSuggestionAssetValueMap);
			subSegmentAfterSuggestionAllocationMap=getSubSegmentPercentageAllocationMap(totalPortfolioAmount,subSegmentAfterSuggestionAssetValueMap);
			stockSegmentAfterSuggestionAllocationMap=getPercentageAllocationMap(totalDE,stockSegmentAfterSuggestionAssetValueMap);
			if(segmentAfterSuggestionAllocationMap==null || subSegmentAfterSuggestionAllocationMap==null || stockSegmentAfterSuggestionAllocationMap==null)
				throw new FundexpertException("Error Occurred while calculating First Level Scheme Suggestion Allocation Percentage Distribution in FirstLevelSuggestion.");
	
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			if(tx!=null && tx.isActive())
				tx.rollback();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null && tx.isActive())
				tx.rollback();
			throw e;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String,Double> getPercentageAllocationMap(Double base,Map<String,Double> assetValueMap)
	{
		Map<String,Double> percentageAllocationMap=null;
		try
		{
			percentageAllocationMap=new HashMap<String,Double>(assetValueMap.size());
			for(Map.Entry<String, Double> m:assetValueMap.entrySet())
			{
				percentageAllocationMap.put(m.getKey(), ((m.getValue()/base)*100.0));
			}
			return percentageAllocationMap;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public Map<String,Map<String,Double>> getSubSegmentPercentageAllocationMap(Double base,Map<String,Map<String,Double>> assetValueMap)
	{
		
		Map<String,Map<String,Double>> percentageAllocationMap=null;
		try
		{
			percentageAllocationMap=new HashMap<String,Map<String,Double>>(assetValueMap.size());
			
			for(Map.Entry<String, Map<String,Double>> m:assetValueMap.entrySet())
			{
				Map<String,Double> subSegmentMap=m.getValue();
				Map<String,Double> smallMap=new HashMap<String,Double>(subSegmentMap.size());
				percentageAllocationMap.put(m.getKey(),smallMap);
				for(Map.Entry<String, Double> map:subSegmentMap.entrySet())
				{
					smallMap.put(map.getKey(),((map.getValue()/base)*100.0));
				}
			}
			return percentageAllocationMap;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Long> getMfKeep() {
		return mfKeep;
	}

	public List<Long> getMfRemove() {
		return mfRemove;
	}

	public Map<Long, String> getMfRemoveMap() {
		return mfRemoveMap;
	}

	public List<Integer> getStockKeep() {
		return stockKeep;
	}

	public double getTotalDE() {
		return totalDE;
	}

	public double getTotalEMF() {
		return totalEMF;
	}

	public double getTotalDMF() {
		return totalDMF;
	}

	public double getTotalCash() {
		return totalCash;
	}

	public double getTotalPortfolioAmount() {
		return totalPortfolioAmount;
	}

	public void setTotalDE(double totalDE) {
		this.totalDE = totalDE;
	}

	public void setTotalEMF(double totalEMF) {
		this.totalEMF = totalEMF;
	}

	public void setTotalDMF(double totalDMF) {
		this.totalDMF = totalDMF;
	}

	public void setTotalCash(double totalCash) {
		this.totalCash = totalCash;
	}

	public void setTotalPortfolioAmount(double totalPortfolioAmount) {
		this.totalPortfolioAmount = totalPortfolioAmount;
	}

	public Map<String, Double> getSegmentInitialAssetAllocationMap() {
		return segmentInitialAssetAllocationMap;
	}

	public Map<String, Double> getSegmentAfterSuggestionAllocationMap() {
		return segmentAfterSuggestionAllocationMap;
	}

	public Map<String, Map<String,Double>> getSubSegmentInitialAssetAllocationMap() {
		return subSegmentInitialAssetAllocationMap;
	}

	public Map<String, Map<String,Double>> getSubSegmentAfterSuggestionAllocationMap() {
		return subSegmentAfterSuggestionAllocationMap;
	}

	public Map<String, Double> getStockSegmentInitialAssetAllocationMap() {
		return stockSegmentInitialAssetAllocationMap;
	}

	public Map<String, Double> getStockSegmentAfterSuggestionAllocationMap() {
		return stockSegmentAfterSuggestionAllocationMap;
	}

	public List<Integer> getStockRemove() {
		return stockRemove;
	}

	public Map<Integer, String> getStockRemoveMap() {
		return stockRemoveMap;
	}

	public Map<String, Double> getSegmentInitialAssetValueMap() {
		return segmentInitialAssetValueMap;
	}

	public Map<String, Map<String,Double>> getSubSegmentInitialAssetValueMap() {
		return subSegmentInitialAssetValueMap;
	}

	public Map<String, Double> getStockSegmentInitialAssetValueMap() {
		return stockSegmentInitialAssetValueMap;
	}

	public double getTotalCashAfterSuggestionForStock() {
		return totalCashAfterSuggestionForStock;
	}

	public double getTotalCashAfterSuggestionForMF() {
		return totalCashAfterSuggestionForMF;
	}

	public Map<String, Double> getSegmentAfterSuggestionAssetValueMap() {
		return segmentAfterSuggestionAssetValueMap;
	}

	public Map<String, Map<String,Double>> getSubSegmentAfterSuggestionAssetValueMap() {
		return subSegmentAfterSuggestionAssetValueMap;
	}

	public Map<String, Double> getStockSegmentAfterSuggestionAssetValueMap() {
		return stockSegmentAfterSuggestionAssetValueMap;
	}

	public double getTotalDEAfterSuggestion() {
		return totalDEAfterSuggestion;
	}

	public double getTotalEMFAfterSuggestion() {
		return totalEMFAfterSuggestion;
	}

	public double getTotalDMFAfterSuggestion() {
		return totalDMFAfterSuggestion;
	}

	public double getTotalPortfolioAmountAfterSuggestion() {
		return totalPortfolioAmountAfterSuggestion;
	}

	public Map<String, List<UserPortfolioState>> getSegmentWiseUserPortfolioStateStockMap() {
		return segmentWiseUserPortfolioStateStockMap;
	}

	public Map<String, List<UserPortfolioState>> getSubSegmentWiseUserPortfolioStateStockMap() {
		return subSegmentWiseUserPortfolioStateStockMap;
	}
	
	public Map<String, List<UserPortfolioState>> getStockSegmentWiseUserPortfolioStateMap() {
		return stockSegmentWiseUserPortfolioStateMap;
	}
	
	public Map<String, List<UserPortfolioState>> getSegmentWiseUserPortfolioStateMFMap() {
		return segmentWiseUserPortfolioStateMFMap;
	}

	public Map<String, List<UserPortfolioState>> getSubSegmentWiseUserPortfolioStateMFMap() {
		return subSegmentWiseUserPortfolioStateMFMap;
	}

	public void getStockWiseUserPortfolioStateAfterSuggestion(UserPortfolioState ups,String segment,String stockSegment,String subSegment) throws Exception
	{
		try
		{
			//there is only 1 segment for stock which is DirectEquity hence 1 segment has many funds thats why we use list.
			List<UserPortfolioState> segmentWiseUserPortfolioStateList=segmentWiseUserPortfolioStateStockMap.get(segment);
			if(segmentWiseUserPortfolioStateList!=null)
			{
				segmentWiseUserPortfolioStateList.add(ups);
			}
			else
			{
				segmentWiseUserPortfolioStateList=new ArrayList<UserPortfolioState>();
				segmentWiseUserPortfolioStateList.add(ups);
				segmentWiseUserPortfolioStateStockMap.put(segment,segmentWiseUserPortfolioStateList);
			}
			//there might be more than 1 stock per subSegment thats why we make list
			List<UserPortfolioState> subSegmentWiseUserPortfolioStateList=subSegmentWiseUserPortfolioStateStockMap.get(subSegment);
			if(subSegmentWiseUserPortfolioStateList!=null)
			{
				subSegmentWiseUserPortfolioStateList.add(ups);	
			}
			else
			{
				subSegmentWiseUserPortfolioStateList=new ArrayList<UserPortfolioState>();
				subSegmentWiseUserPortfolioStateList.add(ups);
				subSegmentWiseUserPortfolioStateStockMap.put(subSegment, subSegmentWiseUserPortfolioStateList);
			}
			//there might be more than 1 stock in particular sector that why we make list
			List<UserPortfolioState> stockSegmentWiseUserPortfolioStateList=stockSegmentWiseUserPortfolioStateMap.get(stockSegment);
			if(stockSegmentWiseUserPortfolioStateList!=null)
			{
				stockSegmentWiseUserPortfolioStateList.add(ups);
			}
			else
			{
				stockSegmentWiseUserPortfolioStateList=new ArrayList<UserPortfolioState>();
				stockSegmentWiseUserPortfolioStateList.add(ups);
				stockSegmentWiseUserPortfolioStateMap.put(stockSegment, stockSegmentWiseUserPortfolioStateList);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace(); 
			throw new Exception("Error Occurred.");
		}
	}
	
	public void getMutualFundWiseUserPortfolioStateAfterSuggestion(UserPortfolioState ups,String broaderSegment,String broaderSubSegment) throws Exception
	{
		try
		{
			//
			List<UserPortfolioState> segmentWiseUserPortfolioStateList=segmentWiseUserPortfolioStateMFMap.get(broaderSegment);
			if(segmentWiseUserPortfolioStateList!=null)
			{
				segmentWiseUserPortfolioStateList.add(ups);
			}
			else
			{
				segmentWiseUserPortfolioStateList=new ArrayList<UserPortfolioState>();
				segmentWiseUserPortfolioStateList.add(ups);
				segmentWiseUserPortfolioStateMFMap.put(broaderSegment,segmentWiseUserPortfolioStateList);
			}
			//
			List<UserPortfolioState> subSegmentWiseUserPortfolioStateList=subSegmentWiseUserPortfolioStateMFMap.get(broaderSubSegment);
			if(subSegmentWiseUserPortfolioStateList!=null)
			{
				subSegmentWiseUserPortfolioStateList.add(ups);	
			}
			else
			{
				subSegmentWiseUserPortfolioStateList=new ArrayList<UserPortfolioState>();
				subSegmentWiseUserPortfolioStateList.add(ups);
				subSegmentWiseUserPortfolioStateMFMap.put(broaderSubSegment, subSegmentWiseUserPortfolioStateList);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Error Occurred.");
		}
		
	}

	public List<Integer> getStockRecommendedList() {
		return stockRecommendedList;
	}

	public List<Long> getMfRecoList() {
		return mfRecoList;
	}

	public List<Integer> getStockRecoList() {
		return stockRecoList;
	}

	public List<Long> getMfRating_mfId_List() {
		return mfRating_mfId_List;
	}

	public List<Integer> getStock_rating_List() {
		return stock_rating_List;
	}

	public Map<String, List<Stocks>> getSegmentWiseStockMap() {
		return segmentWiseStockMap;
	}

	public Map<Integer, Stocks> getUsersStockMap() {
		return usersStockMap;
	}

	public Map<String, List<MutualFund>> getSubSegmentWiseMFMap() {
		return subSegmentWiseMFMap;
	}

	public Map<String, List<Stocks>> getSubSegmentWiseStockMap() {
		return subSegmentWiseStockMap;
	}

	public Map<Long, MutualFund> getUsersMutualFundMap() {
		return usersMutualFundMap;
	}
	
	public List<UserPortfolioState> getUserPortfolioStateList()
	{
		return userPortfolioStateList;
	}
}
