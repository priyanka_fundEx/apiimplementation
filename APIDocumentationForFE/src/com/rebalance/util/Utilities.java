package com.rebalance.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Utilities {

	class FilesAction
	{
		public void serializeObject(Object obj,File file) throws Exception
		{
			FileOutputStream fout = null;
			ObjectOutputStream oos = null;
			try {
	
				fout = new FileOutputStream(file);
				oos = new ObjectOutputStream(fout);
				
				oos.writeObject(obj);
				
				System.out.println("File Writing Done.");
	
			} catch (Exception ex) {
	
				ex.printStackTrace();
				throw new Exception("Not able to write new MutualFunds to file : " +file.getAbsolutePath()+ " : Exception : "+ex.getMessage());
			} finally {
	
				if (fout != null) {
					try {
						fout.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
	
				if (oos != null) {
					try {
						oos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
	
			}
		}
		
		public Object readFile(File file)
		{
			FileInputStream fis = null;
			ObjectInputStream is = null;
			Object obj=null;
			try
			{
				fis = new FileInputStream(file);
	            is = new ObjectInputStream(fis);
	            obj =  is.readObject();
	            return obj;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				try 
				{
					if(is!=null)
						is.close();
				} catch (IOException e1) 
				{
					e1.printStackTrace();
				}
				try 
				{
					if(fis!=null)
						fis.close();
				} catch (IOException e1) 
				{
					e1.printStackTrace();
				}
			}
			return null;
		}
	}
}
