package com.rebalance.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.UserPortfolioState;
import com.fundexpert.exception.FundexpertException;

public class AssetUtil {

	List<Holding> userHoldingList=new LinkedList<Holding>();
	List<Stocks> userStockList=new LinkedList<Stocks>();
	Map<Long,String> mfBroaderSegmentMap=new HashMap<Long,String>();
	Map<Long,String> mfBroaderSubSegmentMap=new HashMap<Long,String>();
	Map<Integer,String> stockSegmentMap=new HashMap<Integer,String>();
	Map<Integer,String> stockSubSegmentMap=new HashMap<Integer,String>();
	Map<Long,String> mfNameMap=new HashMap<Long,String>();
	Map<Integer,String> stockNameMap=new HashMap<Integer,String>();
	double currentTotalMutualFundValue=0;
	double currentTotalStocksValue=0;
	double currentTotalPortfolioValue=0;
	
	long userId=0;
	Session hSession=null;
	
	public AssetUtil(long userId,Session hSession)
	{
		this.userId=userId;
		try
		{
			this.hSession=hSession;
			userHoldingList=hSession.createQuery("from Holding where userId=?").setLong(0,userId).list();
			userStockList=hSession.createQuery("from StocksHoldings where userId=?").setLong(0,userId).list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public Map<Long,Double> getMutualFundAssetAllocation()
	{
		Map<Long,Double> map=new HashMap();
		try
		{
			List<Object[]> list=hSession.createQuery("SELECT h.mutualfundId,sum(h.units*m.nav),m.schemeType,m.type,m.broaderSchemeType,m.broaderType,m.name FROM Holding h,MutualFund m where m.id=h.mutualfundId and h.userId=? group by h.mutualfundId").setLong(0,userId).list();
			if(list!=null)
			{
				for(Object[] obj:list)
				{
					long mfId=(Long)obj[0];
					double sum=(Double)obj[1];
					String schemeType=(String)obj[2];
					String type=(String)obj[3];
					String broaderSchemeType=(String)obj[4];
					String broaderType=(String)obj[5];
					String name = (String)obj[6];
					mfBroaderSegmentMap.put(mfId, broaderSchemeType);
					mfBroaderSubSegmentMap.put(mfId, broaderType);
					mfNameMap.put(mfId, name);
					currentTotalMutualFundValue+=sum;
					map.put(mfId, sum);
				}
				currentTotalPortfolioValue+=currentTotalMutualFundValue;
			}
			return map;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public Map<Integer,Double> getStockAssetAllocation()
	{
		Map<Integer,Double> map=new HashMap();
		try
		{
			List<Object[]> list=hSession.createQuery("SELECT h.scCode,sum(h.numberOfShares*s.closePrice),s.segment,s.type,s.scName FROM StocksHoldings h,Stocks s where s.scCode=h.scCode and h.userId=? group by h.scCode").setLong(0,userId).list();
			if(list!=null)
			{
				for(Object[] obj:list)
				{
					Integer scCode=(Integer)obj[0];
					double sum=(Double)obj[1];
					String segment=(String)obj[2];
					String type=(String)obj[3];
					String name=(String)obj[4];

					stockSegmentMap.put(scCode, segment);
					stockSubSegmentMap.put(scCode,type);
					stockNameMap.put(scCode, name);
					currentTotalStocksValue+=sum;
					map.put(scCode, sum);
				}
				currentTotalPortfolioValue+=currentTotalStocksValue;
			}
			return map;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public List<Holding> getUserHoldingList() {
		return userHoldingList;
	}

	public List<Stocks> getUserStockList() {
		return userStockList;
	}

	public double getCurrentTotalMutualFundValue() {
		return currentTotalMutualFundValue;
	}

	public double getCurrentTotalStocksValue() {
		return currentTotalStocksValue;
	}

	public double getCurrentTotalPortfolioValue() {
		return currentTotalPortfolioValue;
	}


	public Map<Long, String> getMfBroaderSegmentMap() {
		return mfBroaderSegmentMap;
	}
	

	public Map<Long, String> getMfBroaderSubSegmentMap() {
		return mfBroaderSubSegmentMap;
	}

	
	public Map<Integer, String> getStockSegmentMap() {
		return stockSegmentMap;
	}
	
	public Map<Integer, String> getStockSubSegmentMap() {
		return stockSubSegmentMap;
	}

	public Map<Long, String> getMfNameMap() {
		return mfNameMap;
	}

	public void setMfNameMap(Map<Long, String> mfNameMap) {
		this.mfNameMap = mfNameMap;
	}

	public Map<Integer, String> getStockNameMap() {
		return stockNameMap;
	}

	public void setStockNameMap(Map<Integer, String> stockNameMap) {
		this.stockNameMap = stockNameMap;
	}
	
}
