package com.rebalance.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.controller.MutualFundController;
import com.fundexpert.controller.StocksController;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.UserPortfolioState;

public class JSON {

	static DecimalFormat df=new DecimalFormat("####.###");
	//map contains final rebalancing subSegment wise List of UserPortfolioState
	public static JSONArray makeJSONForHoldingsOnly(Map<String,List<UserPortfolioState>> map,long userId)
	{
		Session hSession=null;
		try
		{
			if(map==null)
				return null;
			List<UserPortfolioState> list=new ArrayList<UserPortfolioState>();
			hSession=HibernateBridge.getSessionFactory().openSession();
			//combine the list in Map
			Iterator itr=map.keySet().iterator();
			while(itr.hasNext())
			{
				String subSegment=(String)itr.next();
				List<UserPortfolioState> subList=map.get(subSegment);
				list.addAll(subList);
				System.out.println("SUBSegment from JSON = "+subSegment);
				subList.forEach(data->System.out.println("UPS id ="+data.getId()+" MutualFund mfId = "+data.getAssetId()+" UPS assetValue = "+data.getCurrentAssetValue()));
			}
			
			List<UserPortfolioState> userPortfolioStateList=hSession.createQuery("from UserPortfolioState where userId=? and assetType=?").setLong(0, userId).setInteger(1, Asset.MUTUALFUND_ASSET_TYPE).list();
			MutualFundController mfc=new MutualFundController();
			JSONArray jsonArray=new JSONArray();
			for(int i=0;i<userPortfolioStateList.size();i++)
			{
				JSONObject mutualFundJson=null;
				UserPortfolioState beforeOptimizer=userPortfolioStateList.get(i);
				UserPortfolioState afterOptimizer=null;
				long mfAssetId=beforeOptimizer.getAssetId();
				MutualFund mutualFund=mfc.getFundById(mfAssetId);
				System.out.println("Before Optimizer id = "+mfAssetId);
				for(int j=0;j<list.size();j++)
				{
					UserPortfolioState ups1=list.get(j);
					if(ups1.getAssetId()==mfAssetId)
					{
						afterOptimizer=ups1;
						break;
					}
				}
				mutualFundJson=new JSONObject();
				mutualFundJson.put("isin", mutualFund.getIsin());
				mutualFundJson.put("segment", mutualFund.getBroaderSchemeType());
				mutualFundJson.put("mCap", mutualFund.getBroaderType());
				mutualFundJson.put("valueIn", Double.valueOf(df.format(beforeOptimizer.getCurrentAssetValue())));
				mutualFundJson.put("unitsIn", Double.valueOf(beforeOptimizer.getCurrentAssetValue()/mutualFund.getNav()));
				mutualFundJson.put("nav", mutualFund.getNav());
				mutualFundJson.put("name",mutualFund.getName());
				mutualFundJson.put("aIn", Double.valueOf(df.format(beforeOptimizer.getPortfolioLevelAllocation())));
				//mutualFundJson.put("assetAllocationBeforeOptimizer", Double.valueOf(df.format(beforeOptimizer.getAssetLevelAllocation())));
				mutualFundJson.put("arn", "ARN-70892");
				mutualFundJson.put("arnName","EDELWEISS BROKING LIMITED");
				mutualFundJson.put("state", "existing");
				if(afterOptimizer==null)
				{
					System.out.println("After optimizer is null");
					mutualFundJson.put("valueOut", 0.0);
					mutualFundJson.put("unitsOut", 0);
					mutualFundJson.put("action", "sell");
					mutualFundJson.put("aOut", 0.0);
					//mutualFundJson.put("assetAllocationAfterOptimizer", 0.0);
				}
				else
				{
					System.out.println("After Optimizer is not null");
					mutualFundJson.put("valueOut", Double.valueOf(df.format(afterOptimizer.getCurrentAssetValue())));
					mutualFundJson.put("unitsOut", afterOptimizer.getCurrentAssetValue()/mutualFund.getNav());
					mutualFundJson.put("action", afterOptimizer.getCurrentAssetValue()>beforeOptimizer.getCurrentAssetValue()?"buy":"sell");
					mutualFundJson.put("aOut", Double.valueOf(df.format(afterOptimizer.getPortfolioLevelAllocation())));
					//mutualFundJson.put("assetAllocationAfterOptimizer", Double.valueOf(df.format(afterOptimizer.getAssetLevelAllocation())));
				}
				jsonArray.put(mutualFundJson);
			}
			//System.out.println("In between JSONArray = "+jsonArray.toString(1));
			//to find that UserPortfolioState objects which were not there initially in UserPortfolioState but are now because of Buying
			for(int i=0;i<list.size();i++)
			{
				JSONObject mutualFundJson=null;
				UserPortfolioState afterOptimizer=list.get(i);
				long mfAssetId=afterOptimizer.getAssetId();
				MutualFund mutualFund=mfc.getFundById(mfAssetId);
				boolean flag=false;
				for(int j=0;j<userPortfolioStateList.size();j++)
				{
					UserPortfolioState ups1=userPortfolioStateList.get(j);
					if(ups1.getAssetId()==mfAssetId)
					{
						flag=true;
						break;
					}
				}
				if(!flag)
				{
					mutualFundJson=new JSONObject();
					mutualFundJson.put("isin", mutualFund.getIsin());
					mutualFundJson.put("segment", mutualFund.getBroaderSchemeType());
					mutualFundJson.put("mCap", mutualFund.getBroaderType());
					mutualFundJson.put("valueOut", Double.valueOf(df.format(afterOptimizer.getCurrentAssetValue())));
					mutualFundJson.put("unitsOut", afterOptimizer.getCurrentAssetValue()/mutualFund.getNav());
					mutualFundJson.put("nav", mutualFund.getNav());
					mutualFundJson.put("valueIn", 0.0);
					mutualFundJson.put("unitsIn", 0);
					mutualFundJson.put("action", "buy");
					mutualFundJson.put("state","new");
					mutualFundJson.put("name",mutualFund.getName());
					mutualFundJson.put("aIn", 0.0);
					//mutualFundJson.put("assetAllocationBeforeOptimizer", 0.0);
					mutualFundJson.put("aOut", Double.valueOf(df.format(afterOptimizer.getPortfolioLevelAllocation())));
					//mutualFundJson.put("assetAllocationAfterOptimizer", Double.valueOf(df.format(afterOptimizer.getAssetLevelAllocation())));
					jsonArray.put(mutualFundJson);
				}
			}
			//System.out.println(jsonArray.toString(1));
			return jsonArray;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	
	//map contains final rebalancing stock segment wise List of UserPortfolioState
	public static JSONArray makeJSONForStockHoldingsOnly(Map<String,List<UserPortfolioState>> map,long userId)
	{
		Session hSession=null;
		try
		{
			if(map==null)
				return null;
			List<UserPortfolioState> list=new ArrayList<UserPortfolioState>();
			hSession=HibernateBridge.getSessionFactory().openSession();
			//combine the list in Map
			Iterator itr=map.keySet().iterator();
			while(itr.hasNext())
			{
				String segment=(String)itr.next();
				List<UserPortfolioState> subList=map.get(segment);
				list.addAll(subList);
				System.out.println("Segment = "+segment);
				subList.forEach(data->System.out.println("UPS id ="+data.getId()+" Stock scCode= "+data.getAssetId()+" UPS assetValue = "+data.getCurrentAssetValue()));
				
			}
			
			List<UserPortfolioState> userPortfolioStateList=hSession.createQuery("from UserPortfolioState where userId=? and assetType=?").setLong(0, userId).setInteger(1, Asset.STOCK_ASSET_TYPE).list();
			StocksController sc=new StocksController();
			JSONArray jsonArray=new JSONArray();
			for(int i=0;i<userPortfolioStateList.size();i++)
			{
				JSONObject stockJson=null;
				UserPortfolioState beforeOptimizer=userPortfolioStateList.get(i);
				UserPortfolioState afterOptimizer=null;
				long stockAssetId=beforeOptimizer.getAssetId();
				Stocks stock=sc.getStockByScCode((int)stockAssetId);
				System.out.println("Before Optimizer id ="+stockAssetId);
				for(int j=0;j<list.size();j++)
				{
					UserPortfolioState ups1=list.get(j);
					if(ups1.getAssetId()==stockAssetId)
					{
						afterOptimizer=ups1;
						break;
					}
				}
				System.out.println("Stock ="+stock);
				stockJson=new JSONObject();
				//stockJson.put("i", stock.getScCode());
				stockJson.put("isin", stock.getIsinCode());
				stockJson.put("segment", stock.getSegment());
				stockJson.put("mCap", stock.getType());
				stockJson.put("valueIn", Double.valueOf(df.format(beforeOptimizer.getCurrentAssetValue())));
				stockJson.put("unitsIn", Math.round(beforeOptimizer.getCurrentAssetValue()/stock.getClosePrice()));
				stockJson.put("aIn", Double.valueOf(df.format(beforeOptimizer.getPortfolioLevelAllocation())));
				//stockJson.put("assetAllocationBeforeOptimizer", Double.valueOf(df.format(beforeOptimizer.getAssetLevelAllocation())));
				stockJson.put("nav", stock.getClosePrice());
				stockJson.put("name", stock.getScName());
				stockJson.put("broker","EDELWEISS BROKING LIMITED");
				stockJson.put("state", "existing");
				if(afterOptimizer==null)
				{
					System.out.println("After optimizer is null");
					stockJson.put("valueOut", 0.0);
					stockJson.put("unitsOut", 0);
					stockJson.put("aOut", 0.0);
					//stockJson.put("assetAllocationAfterOptimizer", 0.0);
					stockJson.put("action", "sell");
				}
				else
				{
					System.out.println("After Optimizer is not null");
					stockJson.put("valueOut", Double.valueOf(df.format(afterOptimizer.getCurrentAssetValue())));
					stockJson.put("unitsOut", Math.round(afterOptimizer.getCurrentAssetValue()/stock.getClosePrice()));
					stockJson.put("action", afterOptimizer.getCurrentAssetValue()>beforeOptimizer.getCurrentAssetValue()?"buy":"sell");
					stockJson.put("aOut", Double.valueOf(df.format(afterOptimizer.getPortfolioLevelAllocation())));
					//stockJson.put("assetAllocationAfterOptimizer", Double.valueOf(df.format(afterOptimizer.getAssetLevelAllocation())));
				}
				jsonArray.put(stockJson);
			}
			//System.out.println("In between JSONArray = "+jsonArray.toString(1));
			//to find that UserPortfolioState objects which were not there initially in UserPortfolioState but are now because of Buying
			for(int i=0;i<list.size();i++)
			{
				JSONObject stockJson=null;
				UserPortfolioState afterOptimizer=list.get(i);
				long stockAssetId=afterOptimizer.getAssetId();
				Stocks stock=sc.getStockByScCode((int)stockAssetId);
				boolean flag=false;
				for(int j=0;j<userPortfolioStateList.size();j++)
				{
					UserPortfolioState ups1=userPortfolioStateList.get(j);
					if(ups1.getAssetId()==stockAssetId)
					{
						flag=true;
						break;
					}
				}
				if(!flag)
				{
					stockJson=new JSONObject();
					//stockJson.put("i", stock.getScCode());
					stockJson.put("isin", stock.getIsinCode());
					stockJson.put("segment", stock.getSegment());
					stockJson.put("mCap", stock.getType());
					stockJson.put("valueOut", Double.valueOf(df.format(afterOptimizer.getCurrentAssetValue())));
					stockJson.put("unitsOut", Math.round(afterOptimizer.getCurrentAssetValue()/stock.getClosePrice()));
					stockJson.put("nav", stock.getClosePrice());
					stockJson.put("valueIn", 0.0);
					stockJson.put("unitsIn", 0);
					stockJson.put("action", "buy");
					stockJson.put("name",stock.getScName());
					stockJson.put("aIn", 0.0);
					stockJson.put("state", "new");
					//stockJson.put("assetAllocationBeforeOptimizer", 0.0);
					stockJson.put("aOut", Double.valueOf(df.format(afterOptimizer.getPortfolioLevelAllocation())));
					//stockJson.put("assetAllocationAfterOptimizer", Double.valueOf(df.format(afterOptimizer.getAssetLevelAllocation())));
					jsonArray.put(stockJson);
				}
			}
			System.out.println(jsonArray.toString(1));
			return jsonArray;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
}
