package com.rebalance.test;

import java.util.Map;

import com.fundexpert.exception.FundexpertException;
import com.rebalance.util.SchemeLevelSuggestion;

public class SchemeLevelSuggestionTest {

	public static void main(String[] args) {
		int riskProfile=0;
		SchemeLevelSuggestionTest slst=new SchemeLevelSuggestionTest();
		SchemeLevelSuggestion sls=new SchemeLevelSuggestion(8l,riskProfile);
		try {
			sls.getFirstLevelSuggestion();
			//double cash=sls.getTotalCash();
			//System.out.println("TotalCash Before Suggestion = "+cash);
			System.out.println("------------------Before Suggestion Percentage ALLOCATION---------------------");
			slst.printCurrentPercentageAllocation(sls);
			System.out.println("------------------Before Suggestion Value-------------------------------------");
			slst.printCurrentAssetValue(sls);
			System.out.println("------------------After Suggestion Value--------------------------------------");
			slst.printAfterSuggestionAssetValue(sls);
			System.out.println("------------------After Suggestion Percentage ALLOCATION-----------------------");
			slst.printAfterSuggestionPercentageAllocation(sls);
			double currentPortfolioAmount=sls.getTotalPortfolioAmount();
			
			double totalPortfolioAmountAfterSuggestion=sls.getTotalPortfolioAmountAfterSuggestion();
			double totalCashAfterSuggestionForMF=sls.getTotalCashAfterSuggestionForMF();
			double totalCashAfterSuggestionForStock=sls.getTotalCashAfterSuggestionForStock();
			System.out.println("totalCash left after schemeLevelsuggestion = "+(totalCashAfterSuggestionForMF+totalCashAfterSuggestionForStock));
			//System.out.println("currentPortfolioAmountBefore Suggestion ="+currentPortfolioAmount+"  Total Cash Remained = "+totalCashAfterSuggestion+" currentPortfolioAmountAfterSuggestion= "+totalPortfolioAmountAfterSuggestion);*/
		
			//sls.getList(2l);
			
		} catch (FundexpertException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	void printCurrentPercentageAllocation(SchemeLevelSuggestion sls)
	{
		Map<String,Double> segmentAllocation=sls.getSegmentInitialAssetAllocationMap();
		Map<String,Map<String,Double>> subSegmentAllocation=sls.getSubSegmentInitialAssetAllocationMap();
		Map<String,Double> stockSegmentAllocation=sls.getStockSegmentInitialAssetAllocationMap();
		for(Map.Entry<String, Double> map:segmentAllocation.entrySet())
		{
			double value=map.getValue();
			System.out.println("Segment = "+map.getKey()+" % ALLOCATION="+value);
		}
		for(Map.Entry<String, Map<String,Double>> map:subSegmentAllocation.entrySet())
		{
			Map<String,Double> value=map.getValue();
			for(Map.Entry<String, Double> map1:value.entrySet())
			{
				double value1=map1.getValue();
				System.out.println("SubSegment = "+map1.getKey()+" Value1 ="+value1);
			}
		}
		double stockSegmentAllocationSUM=0;
		for(Map.Entry<String, Double> map:stockSegmentAllocation.entrySet())
		{
			double value=map.getValue();
			stockSegmentAllocationSUM+=value;
			System.out.println("STOCK Segment = "+map.getKey()+" % ALLOCATION="+value);
		}
		System.out.println("Stock Sum of ALLOCATION = "+stockSegmentAllocationSUM);
	}
	
	void printCurrentAssetValue(SchemeLevelSuggestion sls)
	{
		Map<String,Double> segmentValueMap=sls.getSegmentInitialAssetValueMap();
		Map<String,Map<String,Double>> subSegmentValueMap=sls.getSubSegmentInitialAssetValueMap();
		Map<String,Double> stockSegmentValueMap=sls.getStockSegmentInitialAssetValueMap();
		double segmentSum=0,subSegmentSum=0,stockSegmentSum=0;
		for(Map.Entry<String, Double> map:segmentValueMap.entrySet())
		{
			double value=map.getValue();
			String key=map.getKey();
			segmentSum+=value;
			System.out.println("Segment =" +key+" Value="+value);
		}
		for(Map.Entry<String, Map<String,Double>> map:subSegmentValueMap.entrySet())
		{
			Map<String,Double> value=map.getValue();
			for(Map.Entry<String, Double> map1:value.entrySet())
			{
				double value1=map1.getValue();
				System.out.println("SubSegment = "+map1.getKey()+" Value1 ="+value1);
			}
		}
		for(Map.Entry<String, Double> map:stockSegmentValueMap.entrySet())
		{
			double value=map.getValue();
			stockSegmentSum+=value;
			System.out.println("Stock Segment = "+map.getKey()+" Value = "+value);
		}
		System.out.println("Segment Sum="+segmentSum+" SubSegmentSum="+subSegmentSum+" StockSegmentSum = "+stockSegmentSum);
	}

	void printAfterSuggestionAssetValue(SchemeLevelSuggestion sls)
	{
		Map<String,Double> segmentValue=sls.getSegmentAfterSuggestionAssetValueMap();
		Map<String,Map<String,Double>> subSegmentValue=sls.getSubSegmentAfterSuggestionAssetValueMap();
		Map<String,Double> stockSegmentValue=sls.getStockSegmentAfterSuggestionAssetValueMap();
		for(Map.Entry<String, Double> map:segmentValue.entrySet())
		{
			double value=map.getValue();
			System.out.println("Segment = "+map.getKey()+" Value ="+value);
		}
		for(Map.Entry<String, Map<String,Double>> map:subSegmentValue.entrySet())
		{
			Map<String,Double> value=map.getValue();
			for(Map.Entry<String, Double> map1:value.entrySet())
			{
				double value1=map1.getValue();
				System.out.println("SubSegment = "+map1.getKey()+" Value1 ="+value1);
			}
		}
		for(Map.Entry<String, Double> map:stockSegmentValue.entrySet())
		{
			double value=map.getValue();
			System.out.println("STOCK Segment = "+map.getKey()+" Value ="+value);
		}
	}
	
	void printAfterSuggestionPercentageAllocation(SchemeLevelSuggestion sls)
	{
		Map<String,Double> segmentAllocation=sls.getSegmentAfterSuggestionAllocationMap();
		Map<String,Map<String,Double>> subSegmentAllocation=sls.getSubSegmentAfterSuggestionAllocationMap();
		Map<String,Double> stockSegmentAllocation=sls.getStockSegmentAfterSuggestionAllocationMap();
		for(Map.Entry<String, Double> map:segmentAllocation.entrySet())
		{
			double value=map.getValue();
			System.out.println("Segment = "+map.getKey()+" Value ="+value);
		}
		for(Map.Entry<String, Map<String,Double>> map:subSegmentAllocation.entrySet())
		{
			Map<String,Double> value=map.getValue();
			for(Map.Entry<String, Double> map1:value.entrySet())
			{
				double value1=map1.getValue();
				System.out.println("SubSegment = "+map1.getKey()+" Value1 ="+value1);
			}
		}
		double stockSegmentAllocationSUM=0;
		for(Map.Entry<String, Double> map:stockSegmentAllocation.entrySet())
		{
			double value=map.getValue();
			stockSegmentAllocationSUM+=value;
			System.out.println("STOCK Segment = "+map.getKey()+" Value ="+value);
		}
		System.out.println("Total Stock Segment Allocation = "+stockSegmentAllocationSUM);
	}
	
}
