package com.rebalance.test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.controller.MutualFundController;
import com.fundexpert.controller.StocksController;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.RecommendedPortfolio;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.UserPortfolioState;
import com.fundexpert.exception.FundexpertException;
import com.rebalance.main.Algo;
import com.rebalance.util.SchemeLevelSuggestion;

public class AlgoTest {

	long userId=8,consumerId=2;int riskProfile=0;
	
	public static void main(String[] args) {
		
		AlgoTest algoTest=new AlgoTest();
		//algoTest.getStockSegmentMoneyOutFromUserPortfolioForStocks();
		//algoTest.addStockSegmentsWhichAreNotInUsersStockSegments();
		//algoTest.getNumberOfAssetOnBasisOfThresholdPercent();
		//algoTest.putStockSegmentMoneyIntoUserPortfolio();
		//algoTest.getMutualFundSubSegmentMoneyOutFromMutualFund();
		//algoTest.putMutualFundSubSegmentMoneyIntoUserPortfolio();
		
		
		
		//algoTest.bestStockToTakeMoneyOutFromBlackList();
		//algoTest.doStockSectorDistribution();
		
		//algoTest.doMFSubSegmentDistribution();
		
		JSONObject json=algoTest.doRebalance();
		//loop through json and get totalAmountInvested
		//double totalAmountInvested=algoTest.loopJSON(json);
		//System.out.println("total Portfolio Amount After Rebalance = "+totalAmountInvested);
		
		//algoTest.findIfAmountCanStillBeInvested();
	}
	
	void doPrerequisiteCheck()
	{
		Algo algo=new Algo(userId,consumerId,riskProfile);
		try {
			algo.preRequsiteCheck();
		} catch (FundexpertException e) {
			e.printStackTrace();
		}
	}
	
	void getStockSegmentMoneyOutFromUserPortfolioForStocks()
	{
		//for this function to work we need data so to generate data follow below steps
		/*
		 * 1)All stocks should have segment
		 * 2)For userid=10 make stockholdings data which contains stocks from segments - Cars & Utility Vehicles,Construction & Engineering,Housing Finance
		 * 3)Corresponding to his stockholding when we run schemeLevelSuggestion we generate his UserPortfolioState on which we will run our algo
		 */
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Algo algo=new Algo(userId,consumerId,riskProfile);
			List<Integer> stockRecommendedList=hSession.createQuery("select scCode from RecommendedPortfolio where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			List<Integer> stockRecoList=hSession.createQuery("select scCode from Reco where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			System.out.println("StockRecommendedSize="+stockRecommendedList.size()+" stockRecoListSize="+stockRecoList.size());
			System.out.println("scCode"+(stockRecommendedList.get(0)));
			String stockSegment="Construction & Engineering";
			SchemeLevelSuggestion sls=algo.getFirstSchemeLevelSuggestion();
			System.out.println("stocks segment Allocation map="+sls.getStockSegmentAfterSuggestionAllocationMap());
			System.out.println("Total DE amount after suggestion = "+sls.getTotalDEAfterSuggestion());
			Map<String,List<UserPortfolioState>> stockSegmentWiseUserPortfolioMap=sls.getStockSegmentWiseUserPortfolioStateMap();
			List<UserPortfolioState> list=stockSegmentWiseUserPortfolioMap.get(stockSegment);
			double amountRemained=algo.getStockSegmentMoneyOutFromUserPortfolio(sls.getTotalPortfolioAmount(), sls.getTotalDEAfterSuggestion() , 90000.0, list, sls.getStockSegmentAfterSuggestionAllocationMap(),stockRecommendedList,null,stockRecoList,stockSegment,sls);
			System.out.println("Amount left after not able to redeem = "+amountRemained);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

	void addStockSegmentsWhichAreNotInUsersStockSegments()
	{
		Map<String,Double> idealAllocationMap=new HashMap<String,Double>();
		idealAllocationMap.put("OIL", 30.0);
		idealAllocationMap.put("BANK", 40.0);
		idealAllocationMap.put("AGRI",20.0);
		idealAllocationMap.put("PHARMA",10.0);
		
		
		
		Map<String,Double> usersAllocationMap=new HashMap<String,Double>();
		usersAllocationMap.put("OIL",20.0);
		usersAllocationMap.put("BANK",30.0);
		usersAllocationMap.put("AGRI", 20.0);
		
		Algo algo=new Algo(userId,consumerId,riskProfile);
		algo.addStockSegmentsWhichAreNotInUsersStockSegments(idealAllocationMap, usersAllocationMap);
		
		for(Map.Entry<String, Double> entry:usersAllocationMap.entrySet())
		{
			System.out.println("Key = "+entry.getKey()+"\\ Value = "+entry.getValue()+"\n");
		} 
	}
	
	void getNumberOfAssetOnBasisOfThresholdPercent()
	{
		double investmentAmount=40.0;
		double totalPortfolio=1000.0;
		double threshold=4.1;
		Algo algo=new Algo(userId, consumerId,riskProfile);
		int n=algo.getNumberOfAssetOnBasisOfThresholdPercent(investmentAmount, totalPortfolio, threshold);
		System.out.println("Number="+n);
	}
	
	void putStockSegmentMoneyIntoUserPortfolio()
	{
		Session hSession=null;
		Algo algo=new Algo(userId,consumerId,riskProfile);
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			
			List<Integer> stockRecommendedList=hSession.createQuery("select scCode from RecommendedPortfolio where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			List<Integer> stockRecoList=hSession.createQuery("select scCode from Reco where consumerId=? and scCode is not null").setLong(0,consumerId).list();
			System.out.println("StockRecommendedSize="+stockRecommendedList.size()+" stockRecoListSize="+stockRecoList.size());
			
			
			String stockSegment="Housing Finance";
			SchemeLevelSuggestion sls=algo.getFirstSchemeLevelSuggestion();
			double totalAmountInDE=sls.getTotalDEAfterSuggestion();
			double totalAmountAfterSuggestion=sls.getTotalPortfolioAmountAfterSuggestion();
			Map<String,List<UserPortfolioState>> stockSegmentWiseUserPortfolioMap =	sls.getStockSegmentWiseUserPortfolioStateMap();
			List<UserPortfolioState> list=stockSegmentWiseUserPortfolioMap.get(stockSegment);
			Map<String,Double> stockSegmentDistributionMap=sls.getStockSegmentAfterSuggestionAllocationMap();
			
			//double cashLeftAfterPossibleBuyingOfStocks=algo.putStockSegmentMoneyIntoUserPortfolio(sls.getTotalPortfolioAmount(),totalAmountInDE, 5000, list, stockSegmentDistributionMap, stockRecommendedList, null, stockRecoList, stockSegment, sls);
			//System.out.println("Cash left after spending all on stocks = "+cashLeftAfterPossibleBuyingOfStocks);
		
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

	void putMutualFundSubSegmentMoneyIntoUserPortfolio()
	{
		Session hSession=null;
		Algo algo=new Algo(userId,consumerId,riskProfile);
		try
		{
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			String subSegment="MID CAP";
			SchemeLevelSuggestion sls=algo.getFirstSchemeLevelSuggestion();
			double totalAmount=sls.getTotalPortfolioAmount();
			double totalAmountAfterSuggestion=sls.getTotalPortfolioAmountAfterSuggestion();
			Map<String,List<UserPortfolioState>> list=sls.getSubSegmentWiseUserPortfolioStateMFMap();
			List<UserPortfolioState> subSegmentWiseUserPortfolioList=list.get(subSegment);
		
			//now we need % allocation of each subSegment i.e LC,MC,LT,ST,SC including both MF and Equity
			Map<String,Double> afterSubSegmentDistributionPercentageMap=new HashMap<String,Double>();
			Map<String,Map<String,Double>> subSegmentMap=sls.getSubSegmentAfterSuggestionAllocationMap();
			Map<String,Double> stockSubSegmentAllocation=subSegmentMap.get("DIRECTEQUITY");
			Map<String,Double> mfSubSegmentAllocation=subSegmentMap.get("EQUITY");
			afterSubSegmentDistributionPercentageMap=algo.addValuesForSameKey(stockSubSegmentAllocation, mfSubSegmentAllocation);
			if(afterSubSegmentDistributionPercentageMap==null)
				throw new FundexpertException("No subSegments exists in UserPortfolioState");
			System.out.println("afterSubsegmentDistributionAllocationMap="+afterSubSegmentDistributionPercentageMap.toString());
			if(subSegmentWiseUserPortfolioList==null)
				throw new FundexpertException("UserPortfolioState list is empty!");
			subSegmentWiseUserPortfolioList.forEach(i->System.out.println("1)MutualFund id in UserPortfoliotate after firstLevelSchemeSuggeestion = "+i.getAssetId()));
			double amountToBuy=1200.0;
			double totalDebtMFAmount=sls.getTotalDMF();
			double totalEquityMFAmount=sls.getTotalEMF(); 
			double amountLeftAsCash=algo.putMutualFundSubSegmentMoneyIntoUserPortfolio(totalAmount, (totalDebtMFAmount+totalEquityMFAmount) , amountToBuy, subSegmentWiseUserPortfolioList, afterSubSegmentDistributionPercentageMap, null, subSegment, sls);
			System.out.println("After putting money into MFs afterSubSegmentDistributionAllocationMap = "+afterSubSegmentDistributionPercentageMap.toString()+" cash left after investment = "+amountLeftAsCash);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}
	
	void getMutualFundSubSegmentMoneyOutFromMutualFund()
	{
		Session hSession=null;
		Algo algo=new Algo(userId,consumerId,riskProfile);
		String subSegment="LARGE CAP";
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> mfRecoList=null;//hSession.createQuery("select mutualFundId from Reco where consumerId=? and mutualFundId is not null").setLong(0,consumerId).list();
			SchemeLevelSuggestion sls=algo.getFirstSchemeLevelSuggestion();
			Map<String,List<UserPortfolioState>> subSegmentWiseUserPortfolioMap=sls.getSubSegmentWiseUserPortfolioStateMFMap();
			List<UserPortfolioState> list=subSegmentWiseUserPortfolioMap.get(subSegment);
			
			System.out.println("*********************\n*********************USER PORTFOLIO STATE WHICH HAVE PASSED SCHEMELEVEL SUGGESTION");
			Iterator<UserPortfolioState> itr=list.iterator();
			while(itr.hasNext())
			{
				UserPortfolioState ups = (UserPortfolioState)itr.next();
				System.out.println("ID="+ups.getId()+" assetId="+ups.getAssetId());
			}
			System.out.println("*********************\n*********************");
			Map<String,Map<String,Double>> map=sls.getSubSegmentAfterSuggestionAllocationMap();
			Map<String,Double> equityMap=map.get("EQUITY");
			double totalDebtMFAmount=sls.getTotalDMF();
			double totalEquityMFAmount=sls.getTotalEMF();
			double totalMFAmount=totalDebtMFAmount+totalEquityMFAmount;
			Double amountCannotBeRedeemed=algo.getMutualFundSubSegmentMoneyOutFromUserPortfolio(sls.getTotalPortfolioAmount(),totalMFAmount, 5000.0, list, equityMap, mfRecoList, subSegment, sls);
			System.out.println("Amount cannot be redeemed = "+amountCannotBeRedeemed);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

	void bestStockToTakeMoneyOutFromBlackList()
	{
		Session hSession=null;
		try
		{
			Algo algo=new Algo(userId,consumerId,riskProfile);
			String stockSegment="Pharmaceuticals";
			SchemeLevelSuggestion sls=new SchemeLevelSuggestion(userId,riskProfile);
			sls.getFirstLevelSuggestion();
			Map<String,List<UserPortfolioState>> stockSegmentWiseUserPortfolioMap=sls.getStockSegmentWiseUserPortfolioStateMap();
			System.out.println(stockSegmentWiseUserPortfolioMap.toString());
			List<UserPortfolioState> list=stockSegmentWiseUserPortfolioMap.get(stockSegment);
			Map<Integer,Stocks> usersStockMap=sls.getUsersStockMap();
			
			//order blackList on the basis of highest NAV
			list.sort(new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					UserPortfolioState ups1=(UserPortfolioState)o1;
					UserPortfolioState ups2=(UserPortfolioState)o2;
					Stocks s1=usersStockMap.get((int)ups1.getAssetId());
					Stocks s2=usersStockMap.get((int)ups2.getAssetId());
					if(s1.getClosePrice()<s2.getClosePrice())
						return 1;
					else
						return -1;
				}
			});
			list.forEach(i->System.out.println("after order in ascending order "+i.getAssetId()+" subSegment = "+i.getSubSegment()));
			Map<UserPortfolioState,Double> map=algo.bestStockToTakeMoneyOutFromBlackList(list, 641250, usersStockMap);
			for(Map.Entry<UserPortfolioState, Double> ma:map.entrySet())
			{
				System.out.println("Stocks ="+usersStockMap.get((int)(ma.getKey().getAssetId())).getScCode()+" value = "+ma.getValue());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	Map<String,List<UserPortfolioState>> doStockSectorDistribution()
	{
		Session hSession = null;
		Algo algo=new Algo(userId,consumerId,riskProfile);
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			algo.preRequsiteCheck();
			SchemeLevelSuggestion sls=algo.getFirstSchemeLevelSuggestion();
			double totalAmountInDE=sls.getTotalDE();
			double totalAmountInDEAfterSuggestion=sls.getTotalDEAfterSuggestion();
			Map<Integer,Stocks> usersStockMap=sls.getUsersStockMap();
			Map<String,List<UserPortfolioState>> userPortfolioState=sls.getStockSegmentWiseUserPortfolioStateMap();
			/*for(Map.Entry<String, List<UserPortfolioState>> map1:userPortfolioState.entrySet())
			{
				System.out.println("Segment from AlgoTest = "+map1.getKey());
				List<UserPortfolioState> list=map1.getValue();
				list.forEach(i->System.out.println("Stocks in this segment = "+usersStockMap.get((int)i.getAssetId()).getScName()+" id = "+i.getAssetId()+" AssetValue = "+i.getCurrentAssetValue()+" assetLevelAllocation = "+i.getAssetLevelAllocation()));
			}*/
			
			System.out.println("Total amt in DE = "+totalAmountInDE+" totalAmountInDEAfterSuggestion = "+totalAmountInDEAfterSuggestion);
			StocksController sc=new StocksController();
			
			//List<UserPortfolioState> afterSectorDistributionList=sls.getUserPortfolioStateList();
			//afterSectorDistributionList.forEach(i->System.out.println("#@!StockName = "+sc.getStockByScCode((int)i.getAssetId()).getScName()+" stock Code = "+i.getAssetId()+" AssetValue = "+i.getCurrentAssetValue()+" assetLevelAllocation = "+i.getAssetLevelAllocation()));
			double amountLeft = algo.doStockSectorDistribution(totalAmountInDE, sls);
			
			
			System.out.println("Entring into miscellaneous function with cash in hand = "+amountLeft);
			double cashStillLeft = algo.miscellaneousFunctionToMakeIdealPercentageForStocksInSegments(amountLeft,sls.getStockSegmentAfterSuggestionAllocationMap());
			for(Map.Entry<String, List<UserPortfolioState>> map1:userPortfolioState.entrySet())
			{
				System.out.println("Segment from AlgoTest After running Sector Distribution = "+map1.getKey());
				List<UserPortfolioState> list=map1.getValue();
				
				list.forEach(i->System.out.println("Stocks in this segment = "+sc.getStockByScCode((int)i.getAssetId()).getScName()+" id = "+i.getAssetId()+" AssetValue = "+i.getCurrentAssetValue()+" assetLevelAllocation = "+i.getAssetLevelAllocation()));
			}
			System.out.println("Cash Still left="+cashStillLeft);
			//afterSectorDistributionList=sls.getUserPortfolioStateList();
			//afterSectorDistributionList.forEach(i->System.out.println("!@#StockName = "+sc.getStockByScCode((int)i.getAssetId()).getScName()+" stock Code = "+i.getAssetId()+" AssetValue = "+i.getCurrentAssetValue()+" assetLevelAllocation = "+i.getAssetLevelAllocation()));
			userPortfolioState=sls.getStockSegmentWiseUserPortfolioStateMap();
			makeJSONForStockHoldingsOnly(userPortfolioState);
			
			return userPortfolioState;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	//map contains final rebalancing segment wise List of UserPortfolioState
	JSONArray makeJSONForStockHoldingsOnly(Map<String,List<UserPortfolioState>> map)
	{
		Session hSession=null;
		try
		{
			
			List<UserPortfolioState> list=new ArrayList<UserPortfolioState>();
			hSession=HibernateBridge.getSessionFactory().openSession();
			//combine the list in Map
			Iterator itr=map.keySet().iterator();
			while(itr.hasNext())
			{
				String segment=(String)itr.next();
				List<UserPortfolioState> subList=map.get(segment);
				list.addAll(subList);
				System.out.println("Segment = "+segment);
				subList.forEach(data->System.out.println("UPS id ="+data.getId()+" Stock scCode= "+data.getAssetId()+" UPS assetValue = "+data.getCurrentAssetValue()));
				
			}
			
			List<UserPortfolioState> userPortfolioStateList=hSession.createQuery("from UserPortfolioState where userId=? and assetType=2").setLong(0, userId).list();
			StocksController sc=new StocksController();
			JSONArray jsonArray=new JSONArray();
			for(int i=0;i<userPortfolioStateList.size();i++)
			{
				JSONObject stockJson=null;
				UserPortfolioState beforeOptimizer=userPortfolioStateList.get(i);
				UserPortfolioState afterOptimizer=null;
				long stockAssetId=beforeOptimizer.getAssetId();
				Stocks stock=sc.getStockByScCode((int)stockAssetId);
				System.out.println("Before Optimizer id ="+stockAssetId);
				for(int j=0;j<list.size();j++)
				{
					UserPortfolioState ups1=list.get(j);
					if(ups1.getAssetId()==stockAssetId)
					{
						afterOptimizer=ups1;
						break;
					}
				}
				System.out.println("Stock ="+stock);
				stockJson=new JSONObject();
				stockJson.put("i", stock.getScCode());
				stockJson.put("isin", stock.getIsinCode());
				stockJson.put("segment", stock.getSegment());
				stockJson.put("subSegment", stock.getType());
				stockJson.put("valueBeforeOptimizer", beforeOptimizer.getCurrentAssetValue());
				stockJson.put("unitsBeforeOptimizer", Math.round(beforeOptimizer.getCurrentAssetValue()/stock.getClosePrice()));
				stockJson.put("allocationBeforeOptimizer", beforeOptimizer.getPortfolioLevelAllocation());
				stockJson.put("assetAllocationBeforeOptimizer", beforeOptimizer.getAssetLevelAllocation());
				stockJson.put("nav", stock.getClosePrice());
				stockJson.put("name", stock.getScName());
				if(afterOptimizer==null)
				{
					System.out.println("After optimizer is null");
					stockJson.put("valueAfterOptimizer", 0.0);
					stockJson.put("unitsAfterOptimizer", 0);
					stockJson.put("allocationAfterOptimizer", 0.0);
					stockJson.put("assetAllocationAfterOptimizer", 0.0);
					stockJson.put("action", "sell");
				}
				else
				{
					System.out.println("After Optimizer is not null");
					stockJson.put("valueAfterOptimizer", afterOptimizer.getCurrentAssetValue());
					stockJson.put("unitsAfterOptimizer", Math.round(afterOptimizer.getCurrentAssetValue()/stock.getClosePrice()));
					stockJson.put("action", afterOptimizer.getCurrentAssetValue()>beforeOptimizer.getCurrentAssetValue()?"buy":"sell");
					stockJson.put("allocationAfterOptimizer", afterOptimizer.getPortfolioLevelAllocation());
					stockJson.put("assetAllocationAfterOptimizer", afterOptimizer.getAssetLevelAllocation());
				}
				jsonArray.put(stockJson);
			}
			//System.out.println("In between JSONArray = "+jsonArray.toString(1));
			//to find that UserPortfolioState objects which were not there initially in UserPortfolioState but are now because of Buying
			for(int i=0;i<list.size();i++)
			{
				JSONObject stockJson=null;
				UserPortfolioState afterOptimizer=list.get(i);
				long stockAssetId=afterOptimizer.getAssetId();
				Stocks stock=sc.getStockByScCode((int)stockAssetId);
				boolean flag=false;
				for(int j=0;j<userPortfolioStateList.size();j++)
				{
					UserPortfolioState ups1=userPortfolioStateList.get(j);
					if(ups1.getAssetId()==stockAssetId)
					{
						flag=true;
						break;
					}
				}
				if(!flag)
				{
					stockJson=new JSONObject();
					stockJson.put("i", stock.getScCode());
					stockJson.put("isin", stock.getIsinCode());
					stockJson.put("segment", stock.getSegment());
					stockJson.put("subSegment", stock.getType());
					stockJson.put("valueAfterOptimizer", afterOptimizer.getCurrentAssetValue());
					stockJson.put("unitsAfterOptimizer", Math.round(afterOptimizer.getCurrentAssetValue()/stock.getClosePrice()));
					stockJson.put("nav", stock.getClosePrice());
					stockJson.put("valueBeforeOptimizer", 0.0);
					stockJson.put("unitsBeforeOptimizer", 0);
					stockJson.put("action", "buy");
					stockJson.put("name",stock.getScName());
					stockJson.put("allocationBeforeOptimizer", 0.0);
					stockJson.put("assetAllocationBeforeOptimizer", 0.0);
					stockJson.put("allocationAfterOptimizer", afterOptimizer.getPortfolioLevelAllocation());
					stockJson.put("assetAllocationAfterOptimizer", afterOptimizer.getAssetLevelAllocation());
					jsonArray.put(stockJson);
				}
			}
			System.out.println(jsonArray.toString(1));
			return jsonArray;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	void doMFSubSegmentDistribution()
	{
		Session hSession = null;
		Algo algo=new Algo(userId,consumerId,riskProfile);
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			algo.preRequsiteCheck();
			SchemeLevelSuggestion sls=algo.getFirstSchemeLevelSuggestion();
			double totalAmountInDE=sls.getTotalDE();
			double totalAmountInDEAfterSuggestion=sls.getTotalDEAfterSuggestion();
			
			double totalPortfolioAmountBeforeSuggestion=sls.getTotalPortfolioAmount();
			System.out.println("Total amountin DE="+totalAmountInDE+" total Amount in DE after suggestion ="+totalAmountInDEAfterSuggestion);
			double totalDebtMFAmount=sls.getTotalDMF();
			double totalEquityMFAmount=sls.getTotalEMF();
			System.out.println("Total amount in debt = "+totalDebtMFAmount+" Total amount in equity = "+totalEquityMFAmount);
			double cashLeft=algo.doMFSubSegmentDistribution(totalPortfolioAmountBeforeSuggestion, (totalDebtMFAmount+totalEquityMFAmount),0, sls);
			
			
			Map<String,List<UserPortfolioState>> mfSubSegmentWiseUserPortfolioStateMap=sls.getSubSegmentWiseUserPortfolioStateMFMap();
			for(Map.Entry<String, List<UserPortfolioState>> map:mfSubSegmentWiseUserPortfolioStateMap.entrySet())
			{
				String subSegment = map.getKey();
				List<UserPortfolioState> list=map.getValue();
				System.out.println("!@#$SubSegment From AlgoTest = "+subSegment);
				list.forEach(i->System.out.println("UPS id="+i.getId()+" MF ID ="+i.getAssetId()+" currentAssetValue = "+i.getCurrentAssetValue()+" AssetLevelAllocation = "+i.getAssetLevelAllocation()+" portfolioLevelAllocation = "+i.getPortfolioLevelAllocation()));
			}
				
			makeJSONForHoldingsOnly(mfSubSegmentWiseUserPortfolioStateMap);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

	//map contains final rebalancing subSegment wise List of UserPortfolioState
	JSONArray makeJSONForHoldingsOnly(Map<String,List<UserPortfolioState>> map)
	{
		Session hSession=null;
		try
		{
			List<UserPortfolioState> list=new ArrayList<UserPortfolioState>();
			hSession=HibernateBridge.getSessionFactory().openSession();
			//combine the list in Map
			Iterator itr=map.keySet().iterator();
			while(itr.hasNext())
			{
				String subSegment=(String)itr.next();
				List<UserPortfolioState> subList=map.get(subSegment);
				list.addAll(subList);
				System.out.println("SUBSegment from JSON = "+subSegment);
				subList.forEach(data->System.out.println("UPS id ="+data.getId()+" MutualFund mfId = "+data.getAssetId()+" UPS assetValue = "+data.getCurrentAssetValue()));
			}
			
			List<UserPortfolioState> userPortfolioStateList=hSession.createQuery("from UserPortfolioState where userId=? and assetType=1").setLong(0, userId).list();
			MutualFundController mfc=new MutualFundController();
			JSONArray jsonArray=new JSONArray();
			for(int i=0;i<userPortfolioStateList.size();i++)
			{
				JSONObject mutualFundJson=null;
				UserPortfolioState beforeOptimizer=userPortfolioStateList.get(i);
				UserPortfolioState afterOptimizer=null;
				long mfAssetId=beforeOptimizer.getAssetId();
				MutualFund mutualFund=mfc.getFundById(mfAssetId);
				System.out.println("Before Optimizer id = "+mfAssetId);
				for(int j=0;j<list.size();j++)
				{
					UserPortfolioState ups1=list.get(j);
					if(ups1.getAssetId()==mfAssetId)
					{
						afterOptimizer=ups1;
						break;
					}
				}
				System.out.println("MutualFund = "+mutualFund);
				mutualFundJson=new JSONObject();
				mutualFundJson.put("i", mutualFund.getId());
				mutualFundJson.put("isin", mutualFund.getIsin());
				mutualFundJson.put("segment", mutualFund.getBroaderSchemeType());
				mutualFundJson.put("subSegment", mutualFund.getBroaderType());
				mutualFundJson.put("valueBeforeOptimizer", beforeOptimizer.getCurrentAssetValue());
				mutualFundJson.put("unitsBeforeOptimizer", beforeOptimizer.getCurrentAssetValue()/mutualFund.getNav());
				mutualFundJson.put("nav", mutualFund.getNav());
				mutualFundJson.put("name",mutualFund.getName());
				mutualFundJson.put("allocationBeforeOptimizer", beforeOptimizer.getPortfolioLevelAllocation());
				mutualFundJson.put("assetAllocationBeforeOptimizer", beforeOptimizer.getAssetLevelAllocation());
				if(afterOptimizer==null)
				{
					System.out.println("After optimizer is null");
					mutualFundJson.put("valueAfterOptimizer", 0.0);
					mutualFundJson.put("unitsAfterOptimizer", 0);
					mutualFundJson.put("action", "sell");
					mutualFundJson.put("allocationAfterOptimizer", 0.0);
					mutualFundJson.put("assetAllocationAfterOptimizer", 0.0);
				}
				else
				{
					System.out.println("After Optimizer is not null");
					mutualFundJson.put("valueAfterOptimizer", afterOptimizer.getCurrentAssetValue());
					mutualFundJson.put("unitsAfterOptimizer", afterOptimizer.getCurrentAssetValue()/mutualFund.getNav());
					mutualFundJson.put("action", afterOptimizer.getCurrentAssetValue()>beforeOptimizer.getCurrentAssetValue()?"buy":"sell");
					mutualFundJson.put("allocationAfterOptimizer", afterOptimizer.getPortfolioLevelAllocation());
					mutualFundJson.put("assetAllocationAfterOptimizer", afterOptimizer.getAssetLevelAllocation());
				}
				jsonArray.put(mutualFundJson);
			}
			//System.out.println("In between JSONArray = "+jsonArray.toString(1));
			//to find that UserPortfolioState objects which were not there initially in UserPortfolioState but are now because of Buying
			for(int i=0;i<list.size();i++)
			{
				JSONObject mutualFundJson=null;
				UserPortfolioState afterOptimizer=list.get(i);
				long mfAssetId=afterOptimizer.getAssetId();
				MutualFund mutualFund=mfc.getFundById(mfAssetId);
				boolean flag=false;
				for(int j=0;j<userPortfolioStateList.size();j++)
				{
					UserPortfolioState ups1=userPortfolioStateList.get(j);
					if(ups1.getAssetId()==mfAssetId)
					{
						flag=true;
						break;
					}
				}
				if(!flag)
				{
					mutualFundJson=new JSONObject();
					mutualFundJson.put("i", mutualFund.getId());
					mutualFundJson.put("isin", mutualFund.getIsin());
					mutualFundJson.put("segment", mutualFund.getBroaderSchemeType());
					mutualFundJson.put("subSegment", mutualFund.getBroaderType());
					mutualFundJson.put("valueAfterOptimizer", afterOptimizer.getCurrentAssetValue());
					mutualFundJson.put("unitsAfterOptimizer", afterOptimizer.getCurrentAssetValue()/mutualFund.getNav());
					mutualFundJson.put("nav", mutualFund.getNav());
					mutualFundJson.put("valueBeforeOptimizer", 0.0);
					mutualFundJson.put("unitsBeforeOptimizer", 0);
					mutualFundJson.put("action", "buy");
					mutualFundJson.put("name",mutualFund.getName());
					mutualFundJson.put("allocationBeforeOptimizer", 0.0);
					mutualFundJson.put("assetAllocationBeforeOptimizer", 0.0);
					mutualFundJson.put("allocationAfterOptimizer", afterOptimizer.getPortfolioLevelAllocation());
					mutualFundJson.put("assetAllocationAfterOptimizer", afterOptimizer.getAssetLevelAllocation());
					jsonArray.put(mutualFundJson);
				}
			}
			//System.out.println(jsonArray.toString(1));
			return jsonArray;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	JSONObject doRebalance()
	{
		Session hSession=null;
		Algo algo=new Algo(userId,consumerId,riskProfile);
		JSONObject jsonObject=new JSONObject();
		try
		{
			algo.preRequsiteCheck();
			SchemeLevelSuggestion sls=algo.getFirstSchemeLevelSuggestion();
			double totalAmountInDE=sls.getTotalDE();
			double totalAmountInDEAfterSuggestion=sls.getTotalDEAfterSuggestion();
			Map<Integer,Stocks> usersStockMap=sls.getUsersStockMap();
			
			Map<String,Double> afterSectorDistributionAllocationMap=sls.getStockSegmentAfterSuggestionAllocationMap();
			System.out.println("In Algo Test before suggestion Allocation Map = "+afterSectorDistributionAllocationMap);
			
			double cashLeftFromStocks=algo.doStockSectorDistribution(totalAmountInDE, sls);
			System.out.println("Cash leftafter sector distribution = "+cashLeftFromStocks);
			afterSectorDistributionAllocationMap=sls.getStockSegmentAfterSuggestionAllocationMap();
			System.out.println("In Algo Test after suggestion Allocation Map = "+afterSectorDistributionAllocationMap);
			
			
			StocksController sc=new StocksController();
			System.out.println("Total amt in DE = "+totalAmountInDE+" totalAmountInDEAfterSuggestion = "+totalAmountInDEAfterSuggestion);
			Map<String,List<UserPortfolioState>> userPortfolioState=sls.getStockSegmentWiseUserPortfolioStateMap();
			for(Map.Entry<String, List<UserPortfolioState>> map1:userPortfolioState.entrySet())
			{
				System.out.println("Segment from AlgoTest After running Sector Distribution = "+map1.getKey());
				List<UserPortfolioState> list=map1.getValue();
				
				list.forEach(i->System.out.println("Stocks in this segment = "+sc.getStockByScCode((int)i.getAssetId()).getScName()+" id = "+i.getAssetId()+" AssetValue = "+i.getCurrentAssetValue()+" assetLevelAllocation = "+i.getAssetLevelAllocation()));
			}
			
			double totalPortfolioAmountBeforeSuggestion=sls.getTotalPortfolioAmount();
			double totalDebtMFAmount=sls.getTotalDMF();
			double totalEquityMFAmount=sls.getTotalEMF();
			
			System.out.println("*********************************\n*******************************\n**************************");
			System.out.println("*********************************");
			
			double cashLeftAfterMFSubSegmentDistribution=algo.doMFSubSegmentDistribution(totalPortfolioAmountBeforeSuggestion, (totalDebtMFAmount+totalEquityMFAmount),cashLeftFromStocks, sls);
			System.out.println("Cash left after subsegment after MF SubSegment Distribution = "+cashLeftAfterMFSubSegmentDistribution);
			Map<String,List<UserPortfolioState>> mfSubSegmentWiseUserPortfolioStateMap=sls.getSubSegmentWiseUserPortfolioStateMFMap();
			for(Map.Entry<String, List<UserPortfolioState>> map:mfSubSegmentWiseUserPortfolioStateMap.entrySet())
			{
				String subSegment = map.getKey();
				List<UserPortfolioState> list=map.getValue();
				System.out.println("!@#$SubSegment From AlgoTest = "+subSegment);
				list.forEach(i->System.out.println("UPS id="+i.getId()+" MF ID ="+i.getAssetId()+" currentAssetValue = "+i.getCurrentAssetValue()+" AssetLevelAllocation = "+i.getAssetLevelAllocation()+" portfolioLevelAllocation = "+i.getPortfolioLevelAllocation()));
			}
			Map<String,List<UserPortfolioState>> stockSegmentWiseUserPortfolioStateMap=sls.getStockSegmentWiseUserPortfolioStateMap();
			for(Map.Entry<String, List<UserPortfolioState>> map:stockSegmentWiseUserPortfolioStateMap.entrySet())
			{
				String subSegment = map.getKey();
				List<UserPortfolioState> list=map.getValue();
				System.out.println("!@#$SubSegment From AlgoTest = "+subSegment);
				list.forEach(i->System.out.println("UPS id="+i.getId()+" Stocks ID ="+i.getAssetId()+" currentAssetValue = "+i.getCurrentAssetValue()+" AssetLevelAllocation = "+i.getAssetLevelAllocation()+" portfolioLevelAllocation = "+i.getPortfolioLevelAllocation()));
			}
			
			jsonObject.put("mf", makeJSONForHoldingsOnly(mfSubSegmentWiseUserPortfolioStateMap));
			jsonObject.put("stock", makeJSONForStockHoldingsOnly(stockSegmentWiseUserPortfolioStateMap));
			jsonObject.put("success", true);
			System.out.println(jsonObject.toString(1));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			jsonObject.put("success", false);
		}
		return jsonObject;
	}

	double loopJSON(JSONObject jsonO)
	{
		double totalPortflioAmountAfterRebalancing = 0;
		double totalPortflioAmountBeforeRebalancing= 0;
		if(jsonO.has("mf"))
		{
			JSONArray mfArray=jsonO.getJSONArray("mf");
			for(Object obj:mfArray)
			{
				JSONObject json1=(JSONObject)obj;
				totalPortflioAmountAfterRebalancing+=json1.getDouble("valueAfterOptimizer");
				totalPortflioAmountBeforeRebalancing+=json1.getDouble("valueBeforeOptimizer");
				System.out.println(json1.get("i")+" | "+json1.getDouble("valueBeforeOptimizer")+" | "+json1.getDouble("valueAfterOptimizer"));
			}
		}
		if(jsonO.has("stock"))
		{
			JSONArray stockArray=jsonO.getJSONArray("stock");
			for(Object obj:stockArray)
			{
				JSONObject json1=(JSONObject)obj;
				totalPortflioAmountAfterRebalancing+=json1.getDouble("valueAfterOptimizer");
				totalPortflioAmountBeforeRebalancing+=json1.getDouble("valueBeforeOptimizer");
				System.out.println(json1.get("i")+" | "+json1.getDouble("valueBeforeOptimizer")+" | "+json1.getDouble("valueAfterOptimizer"));
			}
		}
		System.out.println("total Portfolio Amount before Rebalancing = "+totalPortflioAmountBeforeRebalancing);
		return totalPortflioAmountAfterRebalancing;
	}
	
	void findIfAmountCanStillBeInvested()
	{
		List<Stocks> existingWhiteList=null;
		Session hSession=null;
		Algo algo=new Algo(userId, consumerId, riskProfile);
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			//stocks of same segment but different subSegment
			existingWhiteList=hSession.createQuery("from Stocks where scCode in (500028,500510,534309)").list();
			double amountToBuy=2000;
			Map<String,Double> idealStockSubSegmentValueMap=new HashMap<String,Double>();
			Map<String,Double> currentStockSubSegmentPercentageAllocationMap=new HashMap<String,Double>();
		
			idealStockSubSegmentValueMap.put("LARGE CAP", 26000.0);
			idealStockSubSegmentValueMap.put("SMALL CAP", 27000.0);
			idealStockSubSegmentValueMap.put("MID CAP", 25000.0);
			
			currentStockSubSegmentPercentageAllocationMap.put("LARGE CAP",25.0);
			currentStockSubSegmentPercentageAllocationMap.put("SMALL CAP",25.0);
			currentStockSubSegmentPercentageAllocationMap.put("MID CAP",25.0);
			boolean canIInvest=algo.findIfAmountCanStillBeInvested(amountToBuy, existingWhiteList, idealStockSubSegmentValueMap, currentStockSubSegmentPercentageAllocationMap, 100000.0);
			System.out.println("canIInvest="+canIInvest);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}
	
}
