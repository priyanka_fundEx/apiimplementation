package com.rebalance.test;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.controller.HoldingController;
import com.fundexpert.controller.StockHoldingController;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.StocksHoldings;
import com.fundexpert.dao.UserPortfolioState;
import com.rebalance.util.AssetUtil;

public class UserPortfolioStateTest {

	public static void main(String[] args) {
		UserPortfolioStateTest upst=new UserPortfolioStateTest();
		upst.insertDataIntoUserPortfolioState(10l);
	}
	
	public void insertDataIntoUserPortfolioState(long userId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			//when user request for rebalance 
			//transfer his existing userPortfolioState to ArchiveUserPortfolioState
			//then delete existing UserPortfolioState and make new UserPortfolioState 
			tx=hSession.beginTransaction();
			String insertQuery="insert into ArchiveUserPortfolioState(userId,assetType,assetId,liability,assetLevelAllocation,portfolioLevelAllocation,currentAssetValue,lastUpdatedOn,insertedIntoArchiveOn)"+
			" select userId,assetType,assetId,liability,assetLevelAllocation,portfolioLevelAllocation,currentAssetValue,lastUpdatedOn,now() from UserPortfolioState where userId=?";
			int updatedRows=hSession.createQuery(insertQuery).setParameter(0, userId).executeUpdate();
			System.out.println("Update rows="+updatedRows);
			tx.commit();
			if(updatedRows>0)
			{
				tx=hSession.beginTransaction();
				String deleteQuery="delete from UserPortfolioState where userId=?";
				int deletedRows=hSession.createQuery(deleteQuery).setLong(0, userId).executeUpdate();
				System.out.println("Deleted rwos="+deletedRows);
				tx.commit();
			}
			AssetUtil au=new AssetUtil(userId,hSession);
			Map<Long,Double> map=au.getMutualFundAssetAllocation();
			Map<Integer,Double> stockMap=au.getStockAssetAllocation();
			Map<Long,String> mfSegment=au.getMfBroaderSegmentMap();
			Map<Long,String> mfSubSegment=au.getMfBroaderSubSegmentMap();
			Map<Integer,String> stockSegment=au.getStockSegmentMap();
			Map<Integer,String> stockSubSegment=au.getStockSubSegmentMap();
			for(Map.Entry<Integer, String> e:stockSubSegment.entrySet())
			{
				System.out.println("scCode="+e.getKey()+" Type="+e.getValue()+"!@$!$!@");
			}
			double mfTotalPortfolioValue=au.getCurrentTotalMutualFundValue();
			double stockTotalPortfolioValue=au.getCurrentTotalStocksValue();
			double totalValue=au.getCurrentTotalPortfolioValue();
			double sumOfIndivdualMfAllocation=0,sumOfIndividualStockAllocation=0,sumOfIndividualAllAllocation=0;
			
			UserPortfolioState ups=null;
			tx=hSession.beginTransaction();
			for(Map.Entry<Long,Double> m:map.entrySet())
			{
				long mfId=m.getKey();
				double value=m.getValue();
				sumOfIndivdualMfAllocation+=(value/mfTotalPortfolioValue)*100;
				sumOfIndividualAllAllocation+=(value/totalValue)*100;
				System.out.println("mfId="+mfId+"  value="+value+" mflevelAllocation="+(value/mfTotalPortfolioValue)+"  totalAllocation="+(value/totalValue));

				ups=new UserPortfolioState();
				ups.setAssetId(mfId);
				ups.setAssetLevelAllocation((value/mfTotalPortfolioValue)*100);
				ups.setAssetType(Asset.MUTUALFUND_ASSET_TYPE);
				ups.setCurrentAssetValue(value);
				ups.setLastUpdatedOn(new Date());
				ups.setLiability(false);
				ups.setPortfolioLevelAllocation((value/totalValue)*100);
				ups.setSegment(mfSegment.get(mfId));
				ups.setSubSegment(mfSubSegment.get(mfId));
				ups.setUserId(userId);
				hSession.save(ups);
			}
			tx.commit();
			tx=hSession.beginTransaction();
			System.out.println("===================================Sum of Mf portfolio = "+au.getCurrentTotalMutualFundValue()+"  sum of percentage of mflevelAllocation="+sumOfIndivdualMfAllocation);
			for(Map.Entry<Integer, Double> m:stockMap.entrySet())
			{
				long scCode=m.getKey();
				double value=m.getValue();
				sumOfIndividualStockAllocation+=(value/stockTotalPortfolioValue)*100;
				sumOfIndividualAllAllocation+=(value/totalValue)*100;
				System.out.println("scCode="+scCode+"  value="+value+" stocklevelAllocation="+(value/stockTotalPortfolioValue)+"  totalAllocation="+(value/totalValue));
			
				ups=new UserPortfolioState();
				ups.setAssetId(scCode);
				ups.setAssetLevelAllocation((value/stockTotalPortfolioValue)*100);
				ups.setAssetType(Asset.STOCK_ASSET_TYPE);
				ups.setCurrentAssetValue(value);
				ups.setLastUpdatedOn(new Date());
				ups.setLiability(false);
				ups.setPortfolioLevelAllocation((value/totalValue)*100);
				ups.setUserId(userId);
				ups.setSegment(stockSegment.get((int)scCode));
				ups.setSubSegment(stockSubSegment.get((int)scCode));
				hSession.save(ups);
			}
			tx.commit();
			System.out.println("===================================Sum of Stock portfolio = "+au.getCurrentTotalStocksValue()+"   sum of percentage of stocklevelAllocation="+sumOfIndividualStockAllocation);
			System.out.println("===================================Sum of All portfolio = "+au.getCurrentTotalPortfolioValue()+"  sum of mf+stock out of portfolio Allocation= "+sumOfIndividualAllAllocation);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}

}
