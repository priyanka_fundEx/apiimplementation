package com.rebalance.test;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.JSON;
import com.fundexpert.controller.UserController;
import com.fundexpert.controller.UserPortfolioStateController;
import com.fundexpert.dao.User;

public class JSONTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		User user=new UserController().getUser(8l);
		UserPortfolioStateController upsc=new UserPortfolioStateController();
		JSONArray array=JSON.toJSONArray(upsc.getUserPortfolioState(user.getId()));
		JSONObject json_response1=new JSONObject();
		json_response1.put("portfolio", array);
		json_response1.put("userName", user.getEmail());
		System.out.println(json_response1.toString());
	}

}
