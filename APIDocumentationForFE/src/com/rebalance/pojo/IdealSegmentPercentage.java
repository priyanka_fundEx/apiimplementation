package com.rebalance.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class IdealSegmentPercentage implements Serializable{
	
	//map of consumers each consumer have different riskProfile and % allocation on each riskprofile
	Map<Long,Map<Integer,Map<String,Double>>> map=new HashMap<Long,Map<Integer,Map<String,Double>>>();

	public Map<Long, Map<Integer, Map<String, Double>>> getMap() {
		return map;
	}

	public void setMap(Map<Long, Map<Integer, Map<String, Double>>> map) {
		this.map = map;
	}
}
