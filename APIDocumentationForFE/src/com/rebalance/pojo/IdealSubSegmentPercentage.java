package com.rebalance.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

//according to edelweiss rebalance module, we look at subSegment overall i.e largeCap=(totalLargeCap(MF+Direct equity)/totalPortfolio)*100
public class IdealSubSegmentPercentage implements Serializable{

	//map of consumers each consumer have different riskProfile and Segment type on  each riskprofile and % allocation on each segment type
	Map<Long,Map<Integer,Map<String,Map<String,Double>>>> map=new HashMap<Long,Map<Integer,Map<String,Map<String,Double>>>>();

	public Map<Long, Map<Integer, Map<String, Map<String, Double>>>> getMap() {
		return map;
	}

	public void setMap(Map<Long, Map<Integer, Map<String, Map<String, Double>>>> map) {
		this.map = map;
	}

}
