package com.rebalance.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fundexpert.controller.SectorAllocationForRebalance;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.SectorAllocation;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;



/**
 * Servlet implementation class StocksServlet
 */
/*@WebServlet(name = "StockServlet", urlPatterns = { "/StockReblanceServlet" })*/
public class StocksServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 	 
			JSONObject json_response= new JSONObject();
			User user=null;
			long consumerId=0;
			response.setContentType("application/Json");
			try {
				if(request.getParameterMap().isEmpty())
				{
					throw new FundexpertException("No Parameters.");
				}
				if(request.getParameterMap().containsKey("method"))
				{
					
					user=(User)request.getSession().getAttribute("user");
					
					String loggedIn=(String)request.getSession().getAttribute("login");
					
					if(loggedIn==null || loggedIn.equals("false") || user==null)
					{
						consumerId=2;
				   /* throw new FundexpertException("You are not logged in.");*/
					}
					else {
						consumerId=user.getConsumerId();
					}
					
				String method = request.getParameter("method");
				if(method.equals("autocompeleteSegment"))				
				{					 
				    System.out.println("heyyyy.......................................................");
					JSONArray jsonArray= new JSONArray();
					int riskProfile=Integer.parseInt(request.getParameter("riskProfile"));
					SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					List<SectorAllocation> list=sectorAllocationForRebalance.getDistinctSegment(riskProfile);
					if(list.size()==0) 
					{
						System.out.println("list is null");	
						throw new FundexpertException("list null,choose another subsegment");
					}
					System.out.println("list is"+list);
					Iterator<SectorAllocation> itr=list.iterator();  
					
					while(itr.hasNext())
					{
						SectorAllocation sectorAllocation=itr.next();
						//System.out.println("stockobject "+itr.next());
						JSONObject jsonObject=new JSONObject();
						jsonObject.put("segmentName",sectorAllocation.getSector());
						jsonArray.put(jsonObject);
						
					}							
					System.out.println(jsonArray);
					json_response.put("sectorArray",jsonArray);
					json_response.put("success",true);
					
				}else if(method.equals("getstocks"))
				{
					 String segment=request.getParameter("search_stock");
					 JSONArray jsonArray= new JSONArray();
					 SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					 List<Stocks> list =sectorAllocationForRebalance.getStocks(segment);
					 if(list.size()==0) 
						{
							System.out.println("list is null");	
						}
					 System.out.println("list is"+list);
					 Iterator<Stocks> itr=list.iterator();  
						
						while(itr.hasNext())
						{
							Stocks stocks=itr.next();
							JSONObject jsonObject=new JSONObject();
							jsonObject.put("scName",stocks.getScName());
							jsonObject.put("scCode",stocks.getScCode());
							jsonArray.put(jsonObject);
							
							
						}
						System.out.println(jsonArray);
						json_response.put("success",jsonArray);
				}else if(method.equals("submit"))
				{
					 String[] stocksDataArray=request.getParameterValues("stocksDataArray[]");
					 //System.out.println("array"+Integer.parseInt(stocksDataArray[1]));
					 String search_stock=request.getParameter("search_stock");
					 System.out.println("search_stock"+search_stock);
					 int riskProfile=Integer.parseInt(request.getParameter("riskProfile"));
					 System.out.println("riskProfile is"+riskProfile);
					 SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					 sectorAllocationForRebalance.submit(search_stock, riskProfile, stocksDataArray,consumerId);
					 //int allocation=Integer.parseInt(request.getParameter("allocation"));
					 json_response.put("success",true);
					 
					 
				}else if(method.equals("displayAllData"))
				{
					SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					List<Object[]> rows=sectorAllocationForRebalance.getAllExistingStocks();
					JSONArray jsonArray= new JSONArray();
					for(Object[] row :rows ){
						System.out.println("iterating list");
						int scCode=Integer.parseInt(row[0].toString());
						String segment=(row[1].toString());
						String scName=(row[2].toString());
						String type=(row[3].toString());
					    System.out.println("scCode"+scCode);
						System.out.println("scName"+scName);
						System.out.println("segment"+segment);
						JSONObject jsonObject=new JSONObject();
						jsonObject.put("scName",scName);
						jsonObject.put("scCode",scCode);
						jsonObject.put("segment",segment);
						jsonObject.put("type",type);
						
						
						jsonArray.put(jsonObject);
						
				 }
				System.out.println("length is"+jsonArray.length());
				if(jsonArray.length()==0) {
					json_response.put("empty","empty");
				}else {
					 json_response.put("success",jsonArray);
					 }
				}
				else if(method.equals("getStocksOfType"))
				{
					JSONArray jsonArray= new JSONArray();
					String type=request.getParameter("optionText");
					System.out.println("type is"+type);
					SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					List<Stocks> list=sectorAllocationForRebalance.getStocksOfType(type);
					if(list==null) 
					{
						System.out.println("list is null");	
						//throw new IOException("list is null") ;
					}
					System.out.println("list is"+list);
					Iterator<Stocks> itr=list.iterator();  
						
						while(itr.hasNext())
						{
							Stocks stocks=itr.next();
							JSONObject jsonObject=new JSONObject();
							jsonObject.put("scCode",stocks.getScCode());
							jsonObject.put("scName",stocks.getScName());
							jsonArray.put(jsonObject);
							
							
						}
						System.out.println(jsonArray);
						json_response.put("success",jsonArray);
					
				}
				else if(method.equals("assestsSubmit")) {
					System.out.println("heyyyyyyyyyyyyyyyyyyyy");
					 String[] dataArray=request.getParameterValues("dataArray[]");
					 String subSegment=request.getParameter("subSegmentId");
					 System.out.println("subSegment"+subSegment);
					 int riskProfile=Integer.parseInt(request.getParameter("riskProfile"));
					 System.out.println("riskProfile is"+riskProfile);
					 SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					 sectorAllocationForRebalance.assestsStockSubmit(subSegment, riskProfile, dataArray,consumerId);
					// int allocation=Integer.parseInt(request.getParameter("allocation"));
					 json_response.put("success",true);
					
				}
				else if(method.equals("submitSectorAllocation")) 
				{
					System.out.println("servlet.......................");
					int i=0;
					int j=0;
					
					while (request.getParameterMap().containsKey("sectorAllocationData[" + i + "][riskProfile]"))
					{
						System.out.println("servlet.......................");
						String sector=request.getParameter("sectorAllocationData[" + i + "][sector]");
						System.out.println("sector"+sector);
						/*List<String> list= new ArrayList<String>();
						list.add(sector);*/
						
						if( (request.getParameter("sectorAllocationData[" + i + "][riskProfile]")!="")&&
								( request.getParameter("sectorAllocationData[" + i + "][allocation]")!="" )&&
								(request.getParameter("sectorAllocationData[" + i + "][sector]")!=""))
						{
							j++;	
							int riskProfile=Integer.parseInt(request.getParameter("sectorAllocationData[" + i + "][riskProfile]"));
							System.out.println("riskProfile"+riskProfile);
							int allocation=Integer.parseInt(request.getParameter("sectorAllocationData[" + i + "][allocation]"));
							System.out.println("allocation"+allocation);
							SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
							boolean result=sectorAllocationForRebalance.submitSectorAllocation(consumerId, riskProfile, allocation, sector,j); 
							
							if( result==false) {
								throw new FundexpertException("exception in controller"
						    			+i);
							}
							
						}
						
						i++;
					}
					 json_response.put("success",true);
				}
				else if(method.equals("getViewStocks"))
				{
					int riskProfile=Integer.parseInt(request.getParameter("riskProfile"));
					
					SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					List<Object[]> rows=sectorAllocationForRebalance.getViewStocks(riskProfile);
					JSONArray jsonArray= new JSONArray();
					for(Object[] row :rows ){
						System.out.println("iterating list");
						
						String segment=(row[0].toString());
						long assetId=Long.parseLong(row[1].toString());
						String scName=(row[2].toString());
						
						JSONObject jsonObject=new JSONObject();
						jsonObject.put("scName",scName);
						jsonObject.put("segment",segment);
						jsonObject.put("assetId",assetId);
						
						
						jsonArray.put(jsonObject);
						
				 }
					 json_response.put("success",true);
					 json_response.put("view",jsonArray);
					
				}
				else if(method.equals("deleteView")) 
				{
					long assetId=Long.parseLong(request.getParameter("assetId"));
					SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					boolean result=sectorAllocationForRebalance.deleteView(assetId);
					
						if( result==false) 
						{
							throw new FundexpertException("exception in controller");
						}
						json_response.put("success", true);
				}
				else if(method.equals("getViewRiskProfile"))
				{
					int assetType=Integer.parseInt(request.getParameter("assetType"));
					SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					 List<Integer> list1=sectorAllocationForRebalance.getViewRiskProfile(assetType,consumerId);
					 if(list1.size()==0) 
					 {
						 throw new FundexpertException("list size is zero"); 
					 }
					 JSONArray jsonArray= new JSONArray();
					 for (int i = 0; i < list1.size(); i++)
					 {
			       			System.out.println(list1.get(i));
			       			JSONObject jsonObject=new JSONObject();
							jsonObject.put("riskProfile",list1.get(i));
							jsonArray.put(jsonObject);
							
							
							
			       			
					 }	
					 json_response.put("success", true);
					 json_response.put("riskProfileArray", jsonArray);
				}else if(method.equals("viewSectorAllocation")) 
				{
					int riskProfile =Integer.parseInt(request.getParameter("riskProfile"));
					System.out.println("riskProfile is"+ riskProfile);
					SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
					List<SectorAllocation> list=sectorAllocationForRebalance.viewSectorAllocation(riskProfile);
					JSONArray jsonArray= new JSONArray();
					if(list.size()==0) {
						throw new FundexpertException("list size zero.");
					}
					Iterator<SectorAllocation> itr=list.iterator();  
					
					while(itr.hasNext())
					{
						SectorAllocation sectorAllocation=itr.next();
						JSONObject jsonObject=new JSONObject();
						jsonObject.put("allocation",sectorAllocation.getAllocation());
						jsonObject.put("riskProfile",sectorAllocation.getRiskProfile());
						jsonObject.put("sector",sectorAllocation.getSector());
						jsonArray.put(jsonObject);
						
						
					}
					System.out.println(jsonArray);
					json_response.put("viewSAlloaction",jsonArray);
					json_response.put("success",true);
				}
				else
				{
					throw new FundexpertException("action not passed.");
				}
			}	
			}
			catch(FundexpertException fe)
			{
				fe.printStackTrace();
				json_response.put("success", false);
				json_response.put("error", fe.getMessage());
			}
			catch(Exception e) 
			{
				e.printStackTrace();
				json_response.put("success",false);
			}
			response.getWriter().write(json_response.toString());
		}
	
	}
						
						

			
		
	
