package com.rebalance.servlet;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fundexpert.controller.SectorAllocationForRebalance;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.SectorAllocation;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.SubSegmentAllocation;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;


@WebServlet("/FundsServlet")
public class FundsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject json_response= new JSONObject();
		response.setContentType("application/Json");
		User user=null;
		long consumerId=0;
		try {
			if(request.getParameterMap().isEmpty())
			{
				throw new FundexpertException("No Parameters.");
			}
			if(request.getParameterMap().containsKey("method"))
			{
				user=(User)request.getSession().getAttribute("user");
				
				String loggedIn=(String)request.getSession().getAttribute("login");
				if(loggedIn==null || loggedIn.equals("false") || user==null)
				{
			   /* throw new FundexpertException("You are not logged in.");*/
					consumerId=2;
				}
				else {
					consumerId=user.getConsumerId();
				}
			String method = request.getParameter("method");
			if(method.equals("getFunds"))				
			{
				JSONArray jsonArray= new JSONArray();
				String type=request.getParameter("optionText");
				Long id=Long.parseLong(request.getParameter("fundHouseId"));
				System.out.println("type is"+type);
				SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
				List<MutualFund> list=sectorAllocationForRebalance.getFunds(type,id);
				if(list.size()==0) 
				{
					System.out.println("list is null");	
					throw new FundexpertException("list is null,choose another fundhouse or subsegment") ;
				}
				System.out.println("list is"+list);
				Iterator<MutualFund> itr=list.iterator();  
					
					while(itr.hasNext())
					{
						MutualFund mutualFund=itr.next();
						JSONObject jsonObject=new JSONObject();
						jsonObject.put("id",mutualFund.getId());
						jsonObject.put("name",mutualFund.getName());
						jsonArray.put(jsonObject);
						
						
					}
					System.out.println(jsonArray);
					json_response.put("success",jsonArray);
				
			}else if(method.equals("assestsSubmit")) {
				
				 String[] dataArray=request.getParameterValues("dataArray[]");
				// System.out.println("array"+Integer.parseInt(dataArray[1]));
				 String subSegment=request.getParameter("subSegmentId");
				 System.out.println("subSegment"+subSegment);
				 int riskProfile=Integer.parseInt(request.getParameter("riskProfile"));
				 System.out.println("riskProfile is"+riskProfile);
				 SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
				 sectorAllocationForRebalance.assestsFundsSubmit(subSegment, riskProfile, dataArray,consumerId);
				 //int allocation=Integer.parseInt(request.getParameter("allocation"));
				 json_response.put("success",true);
				
			}else if(method.equals("submitSubSegmentAllocation")) 
			{
				System.out.println("servlet.......................");
				int i=0;
				int j=0;
				while (request.getParameterMap().containsKey("subSegmentAllocationData[" + i + "][riskProfile]"))
				{
					System.out.println("servlet.......................");
					if( (request.getParameter("subSegmentAllocationData[" + i + "][riskProfile]")!="")&&
							( request.getParameter("subSegmentAllocationData[" + i + "][allocation]")!="" )&&
							(request.getParameter("subSegmentAllocationData[" + i + "][subSegment]")!=""))
					{
							j++;
						String subSegment=request.getParameter("subSegmentAllocationData[" + i + "][subSegment]");
						System.out.println("subSegment"+subSegment);
						int riskProfile=Integer.parseInt(request.getParameter("subSegmentAllocationData[" + i + "][riskProfile]"));
						System.out.println("riskProfile"+riskProfile);
						int allocation=Integer.parseInt(request.getParameter("subSegmentAllocationData[" + i + "][allocation]"));
						System.out.println("allocation"+allocation);
						SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
						boolean result=sectorAllocationForRebalance.submitsubSegmentAllocation(consumerId, riskProfile, allocation, subSegment,j); 
						
						if( result==false)
						{
							throw new FundexpertException("exception in controller"
					    			+i);
						}
					}
				i++;	
				}
				 json_response.put("success",true);
			}
			else if(method.equals("getViewFunds"))
			{
				int riskProfile=Integer.parseInt(request.getParameter("riskProfile"));
				
				SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
				List<Object[]> rows=sectorAllocationForRebalance.getViewFunds(riskProfile);
				JSONArray jsonArray= new JSONArray();
				for(Object[] row :rows ){
					System.out.println("iterating list");
					
					String segment=(row[0].toString());
					long assetId=Long.parseLong(row[1].toString());
					String name=(row[2].toString());
					
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("name",name);
					jsonObject.put("segment",segment);
					jsonObject.put("assetId",assetId);
					
					
					jsonArray.put(jsonObject);
					
			 }
				 json_response.put("success",true);
				 json_response.put("view",jsonArray);
				
			}
			else if(method.equals("deleteView")) 
			{
				long assetId=Long.parseLong(request.getParameter("assetId"));
				SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
				boolean result=sectorAllocationForRebalance.deleteView(assetId);
				
				if( result==false) 
				{
					throw new FundexpertException("exception in controller");
				}
				json_response.put("success", true);
			}
			else if(method.equals("viewSubSegmentAllocation")) 
			{
				int riskProfile =Integer.parseInt(request.getParameter("riskProfile"));
				System.out.println("riskProfile is"+ riskProfile);
				SectorAllocationForRebalance sectorAllocationForRebalance = new SectorAllocationForRebalance();
				List<SubSegmentAllocation> list=sectorAllocationForRebalance.viewSubSegmentAllocation(riskProfile);
				JSONArray jsonArray= new JSONArray();
				if(list.size()==0) {
					throw new FundexpertException("list size zero.");
				}
				Iterator<SubSegmentAllocation> itr=list.iterator();  
				
				while(itr.hasNext())
				{
					SubSegmentAllocation subSegmentAllocation=itr.next();
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("allocation",subSegmentAllocation.getAllocation());
					jsonObject.put("riskProfile",subSegmentAllocation.getRiskProfile());
					jsonObject.put("subSegment",subSegmentAllocation.getSubSegment());
					jsonArray.put(jsonObject);
					
					
				}
				System.out.println(jsonArray);
				json_response.put("viewSAlloaction",jsonArray);
				json_response.put("success",true);
			}
			else
			{
				
				throw new FundexpertException("No Parameters.");
			}
			
			
		}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			json_response.put("success", false);
			json_response.put("error", fe.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			json_response.put("success",Boolean.FALSE);
			
			
		}
		response.getWriter().write(json_response.toString());
	}

}
