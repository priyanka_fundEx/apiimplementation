package com.rebalance.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.JSON;
import com.fundexpert.controller.UserController;
import com.fundexpert.controller.UserPortfolioStateController;
import com.fundexpert.dao.User;
import com.fundexpert.dao.UserPortfolioState;
import com.fundexpert.exception.FundexpertException;
import com.rebalance.main.Algo;

/**
 * Servlet implementation class Rebalance
 */

public class RebalanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Inside Rebalance Servlet.");
		JSONObject resp=new JSONObject();
		PrintWriter pw=response.getWriter();
		try
		{
			HttpSession session=request.getSession();
			/*if(session.getAttribute("userId")==null)
				throw new FundexpertException("You are not logged in.");
			User adminUser=(User)session.getAttribute("user");
			if(adminUser.getRole()<5)
				throw new FundexpertException("You are not authorized.");*/
			if(!request.getParameterMap().containsKey("action"))
				throw new FundexpertException("Not a valid URL.");
			else if(!request.getParameterMap().containsKey("id"))
				throw new FundexpertException("Not a valid URL.");
			else
			{
				long userId=0;
				if(request.getParameter("id")==null)
					throw new FundexpertException("Please pass all parameters.");
				userId=Long.valueOf(request.getParameter("id"));
				if(request.getParameter("action").equals("getInitialAllocation"))
				{
					UserPortfolioStateController upsc=new UserPortfolioStateController();
					//upsc.insertDataIntoUserPortfolioState(userId);
					//Now make JSON out of data
					JSONArray array=JSON.toJSONArray(upsc.getUserPortfolioStateList(userId));
					resp.put("portfolio", array);
					resp.put("success", true);
				}
				else if(request.getParameter("action").equals("getOptimizedAllocation"))
				{
					Map<Integer,Integer> riskProfileMap=new HashMap<Integer,Integer>();
					riskProfileMap.put(60, 1);
					riskProfileMap.put(70, 0);
					riskProfileMap.put(80, 2);
					if(request.getParameter("eP")==null)
						throw new FundexpertException("Please pass all parameters.");
					Integer equityPercent=Integer.valueOf(request.getParameter("eP"));
					System.out.println("EQUITY Percentage = "+equityPercent);
					Integer riskProfile=riskProfileMap.get(equityPercent);
					if(riskProfile==null)
						throw new FundexpertException("Please choose a valid risk profile.");
					UserController uc=new UserController();
					User user=uc.getUser(userId);
					
					Algo algo=new Algo(userId,user.getConsumerId(),riskProfile);
					Map<String,Map<String,List<UserPortfolioState>>> map=algo.doRebalance();
					
					JSONArray mfArray=com.rebalance.util.JSON.makeJSONForHoldingsOnly(map.get("mf"), userId);
					JSONArray stockArray=com.rebalance.util.JSON.makeJSONForStockHoldingsOnly(map.get("stock"), userId);
					resp.put("success", true);
					resp.put("mf", mfArray);
					resp.put("stock", stockArray);
					System.out.println("Response !#$!@$#!@#!@#&&&&&&&&&&&&&&&&&&&&&&&&&"+resp.toString(1));
				}
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			resp.put("success", false);
			resp.put("error",fe.getMessage());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("success", false);
			resp.put("error", "Internal Error.");
		}
		pw.write(resp.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
