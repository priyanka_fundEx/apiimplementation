package com.rebalance.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ValidationException;

import org.hibernate.Session;
import org.json.JSONObject;

import com.fundexpert.controller.StocksController;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.StocksInStockSegment;
import com.fundexpert.dao.UserPortfolioState;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.exception.UnConditionalException;
import com.rebalance.pojo.Action;
import com.rebalance.pojo.IdealSegmentPercentage;
import com.rebalance.pojo.IdealStockSegmentPercentage;
import com.rebalance.pojo.IdealSubSegmentPercentage;
import com.rebalance.util.IdealPercentageDistribution;
import com.rebalance.util.SchemeLevelSuggestion;

public class Algo {

	//for this User run ALGO
	long userId,consumerId=0;
	double totalDEAfterSuggestion=0,totalPortfolioAmountBeforeSuggestion=0,totalPortfolioAmountAfterSuggestion=0,totalCashAfterSuggestionForStock=0,totalCashAfterSuggestionForMF=0;
	
	IdealSubSegmentPercentage idealSubSegment=null;
	IdealStockSegmentPercentage idealStockSegment=null;
	IdealSegmentPercentage idealSegment=null;
	
	//ideal percentage distribution fields
	double equity_segment_percent,debt_segment_percent,sum_segment_percent;
	double equity_LC,equity_MC,equity_SC,debt_LT,debt_ST,sum_equity_subSegment_percent,sum_debt_subSegment_percent;
	Map<String,Double> stock_segment_percent=null;double sum_stock_segment_percent=0;
	
	//schemeLevelSuggestion data
	List<Long> mfKeep=null;
	List<Integer> stockKeep=null;
	Map<Long,String> mfRemoveMap=null;
	Map<Integer,String> stockRemoveMap=null;
	
	Map<String,Double> stockSegmentAfterSuggestionAssetValueMap=null;
	Map<String,Double> stockSegmentAfterSuggestionAllocationMap=null;
	
	SchemeLevelSuggestion sls=null;
	
	List<Integer> stockRecommendedList=null;
	List<Long> mfRecoList=null;
	List<Integer> stockRecoList=null;
	List<Long> mfRating_mfId_List=null;
	List<Integer> stock_rating_List=null;
	
	//get list of stocks of each segment
	Map<String,List<Stocks>> segmentWiseStockMap=null;
	Map<String,List<MutualFund>> subSegmentWiseMFMap=null;
	
	//percentage map
	Map<String,Double> stocksSubSegmentPercentageAllocationMap=null;
	Map<String,Double> mfSubSegmentPercentageAllocationMap=null;
	//value map for their corresponding percentage counterparts above.
	Map<String,Double> stocksSubSegmentValueMap=null;
	Map<String,Double> mfSubSegmentValueMap=null;
	
	
	/*
	 * desc thresholdPercentage
	 * 1)(value/totalPortfolioAmount)*100; will give some percentage out of totalPortfolioAmount say we get assetAllocation
	 * 2)then if(assetAllocation<thresholdPercentage) then invest amount you want to spend in existing assets of those type
	 * 3)else if(assetAllocation>thresholdPercentage) but less than then first multiple of threshold i.e 4 then buy single asset of that type
	 * 4)else if(assetAllocation>thresholdPercentage) but more than then first multiple(but less than second multiple i.e 6) of threshold i.e 4 then buy two asset of that type
	 * 5)similarly base on its multiple type
	 */
	double stockThresholdPercentage=2.0;
	double mfThresholdPercentage=2.0;
	
	int riskProfile=0;
	IdealPercentageDistribution ipd=null;
	Map<String,Map<String,Double>> idealSubSegmentAllocationMap=null;
	Map<String,Double> idealStockSegmentAllocationMap=null;
	Map<String,Double> idealStockSubSegmentValueMap=null;

	//Map<String,Double> idealStockSubSegmentAllocationMap=null;
	
	public Algo(long userId,long consumerId,int riskProfile)
	{
		this.userId=userId;
		this.consumerId=consumerId;
		this.riskProfile=riskProfile;
		sls=new SchemeLevelSuggestion(userId,riskProfile);
	}
	
	public void getList() throws Exception
	{
		sls.getList(consumerId);
		stockRecommendedList=sls.getStockRecommendedList();
		mfRecoList=sls.getMfRecoList();
		stockRecoList=sls.getStockRecoList();
		mfRating_mfId_List=sls.getMfRating_mfId_List();
		stock_rating_List=sls.getStock_rating_List();
	}
	
	//check for ideal percentage distribution check from text files
	public void preRequsiteCheck() throws FundexpertException
	{
		ipd=new IdealPercentageDistribution();
		Map<String,Double> map=null;
		try
		{
			idealSubSegmentAllocationMap=ipd.getCurrentIdealSubSegmentPercentage(consumerId, riskProfile);
			double sumSubSegmentAllocation=0;
			for(Map.Entry<String, Map<String,Double>> entry:idealSubSegmentAllocationMap.entrySet())
			{
				String key=entry.getKey();
				map=entry.getValue();
				System.out.println("Segment : "+key);
				for(Map.Entry<String, Double> entry1:map.entrySet())
				{
					System.out.println("SubSegment : "+entry1.getKey()+" Allocation : "+entry1.getValue());
					sumSubSegmentAllocation+=entry1.getValue();
				}
			}
			if(sumSubSegmentAllocation!=100.0)
				throw new FundexpertException("Subsegment allocation does not seem to be 100% for riskProfile = "+riskProfile);
		
			idealStockSegmentAllocationMap=ipd.getCurrentIdealStockSegmentPercentage(consumerId, riskProfile);
			double sumStockSegment=0;
			for(Map.Entry<String, Double> entry:idealStockSegmentAllocationMap.entrySet())
			{
				sumStockSegment+=entry.getValue();
				System.out.println(entry.getKey()+"  |  "+entry.getValue());
			}
			if(sumStockSegment!=100.0)
				throw new ValidationException("Sum of Stock's Segment percentage distribution is not equal to 100.0%");
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public SchemeLevelSuggestion getFirstSchemeLevelSuggestion() throws Exception
	{
		Map<String,Map<String,Double>> subSegmentMap=null;
		Map<String,Map<String,Double>> subSegmentValueMap=null;
		try
		{
			sls.getFirstLevelSuggestion();
			
			//
			System.out.println("!@#equity from idealSubSegmentAllocationMap= "+idealSubSegmentAllocationMap.get("EQUITY"));
			idealStockSubSegmentValueMap=makeAlocationOnBaseOf100(idealSubSegmentAllocationMap.get("EQUITY"),sls.getTotalPortfolioAmount());
			System.out.println("8769306504"+idealStockSubSegmentValueMap);
			segmentWiseStockMap=sls.getSegmentWiseStockMap();
			subSegmentMap=sls.getSubSegmentAfterSuggestionAllocationMap();
			stocksSubSegmentPercentageAllocationMap=new HashMap<String,Double>();
			mfSubSegmentPercentageAllocationMap=new HashMap<String,Double>();
			System.out.println("Subsegment MAP ="+subSegmentMap);
			if(subSegmentMap.containsKey("EQUITY"))
			{
				mfSubSegmentPercentageAllocationMap.putAll(subSegmentMap.get("EQUITY"));
			}
			if(subSegmentMap.containsKey("DEBT"))
			{
				mfSubSegmentPercentageAllocationMap.putAll(subSegmentMap.get("DEBT"));	
			}
			if(subSegmentMap.containsKey("DIRECTEQUITY"))
			{
				stocksSubSegmentPercentageAllocationMap.putAll(subSegmentMap.get("DIRECTEQUITY"));
			}
			
			//add those subSegment which are not in stocksSubSegmentPercentageAllocationMap but  are in idealStocksSubSegmentPercentageAllocationMap
			for(Map.Entry<String,Double> map:idealStockSubSegmentValueMap.entrySet())
			{
				if(!stocksSubSegmentPercentageAllocationMap.containsKey(map.getKey()))
				{
					stocksSubSegmentPercentageAllocationMap.put(map.getKey(), 0.0);
				}
			}

			subSegmentValueMap=sls.getSubSegmentAfterSuggestionAssetValueMap();
			stocksSubSegmentValueMap=new HashMap<String,Double>();
			mfSubSegmentValueMap=new HashMap<String,Double>();
			if(subSegmentValueMap.containsKey("EQUITY"))
			{
				mfSubSegmentValueMap.putAll(subSegmentValueMap.get("EQUITY"));
			}
			if(subSegmentValueMap.containsKey("DEBT"))
			{
				mfSubSegmentValueMap.putAll(subSegmentValueMap.get("DEBT"));	
			}
			if(subSegmentValueMap.containsKey("DIRECTEQUITY"))
			{
				stocksSubSegmentValueMap.putAll(subSegmentValueMap.get("DIRECTEQUITY"));
			}
			
			subSegmentWiseMFMap=sls.getSubSegmentWiseMFMap();
			
			stockSegmentAfterSuggestionAssetValueMap=sls.getStockSegmentAfterSuggestionAssetValueMap();
			stockSegmentAfterSuggestionAllocationMap=sls.getStockSegmentAfterSuggestionAllocationMap();
			totalDEAfterSuggestion=sls.getTotalDEAfterSuggestion();
			totalPortfolioAmountAfterSuggestion=sls.getTotalPortfolioAmountAfterSuggestion();
			totalCashAfterSuggestionForStock=sls.getTotalCashAfterSuggestionForStock();
			totalCashAfterSuggestionForMF=sls.getTotalCashAfterSuggestionForMF();
			totalPortfolioAmountBeforeSuggestion=sls.getTotalPortfolioAmount();
			
			System.out.println("After SchemeLevelSuggestion but Before sector distribution, stocks subSegment percentage allocation = "+stocksSubSegmentPercentageAllocationMap);
			System.out.println("After SchemeLevelSuggestion but Before SubSegment distribution, mf subSegment percentage allocation = "+mfSubSegmentPercentageAllocationMap);
			System.out.println("After SchemeLevelSuggestion but Before SubSegment distribution, stocks subSegment percentage allocation = "+stocksSubSegmentPercentageAllocationMap);
			System.out.println("totalAmountInPortfolioBeforeSuggestion = "+totalPortfolioAmountBeforeSuggestion+"  :  totalAmountInportfolioAfterSuggestion = "+totalPortfolioAmountAfterSuggestion+" diff is totalCashLeft = "+(totalPortfolioAmountBeforeSuggestion-totalPortfolioAmountAfterSuggestion));
			System.out.println("TotalCash left from MF="+totalCashAfterSuggestionForMF+" totalCash left from Stock="+totalCashAfterSuggestionForStock);
			return sls;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
	
	//if totalCashAfterRedemptionOfStocks!=0 then return it which will be added to totalCash that we had before calling this function
	public Double doStockSectorDistribution(double totalDirectEquityAmount,SchemeLevelSuggestion suggestion) throws Exception
	{
		Map<String,Double> afterSectorDistributionValueMap=null;//ew HashMap<String,Double>();
		Map<String,Double> afterSectorDistributionPercentageMap=null;//ew HashMap<String,Double>();
		Map<String,List<UserPortfolioState>> stockSegmentWiseUserPortfolioMap=suggestion.getStockSegmentWiseUserPortfolioStateMap();
		double totalCashAfterRedemptionOfStocks=suggestion.getTotalCashAfterSuggestionForStock();
		double totalCashAfterBuyingStocks=0;
		try
		{
			afterSectorDistributionValueMap = stockSegmentAfterSuggestionAssetValueMap;
			afterSectorDistributionPercentageMap = stockSegmentAfterSuggestionAllocationMap;
			//loop through each sector from stockSegmentAfterSuggestionAllocationMap
			//and take out the money from that sector if that sector % allocation is more than idealOne and add it to cash balance
			Map<String,Double> idealStockAllocation=idealStockSegmentAllocationMap;
			for(Map.Entry<String, Double> map:afterSectorDistributionPercentageMap.entrySet())
			{
				String stockSegment=map.getKey();
				double stockSegmentAllocation=map.getValue();
				if(idealStockAllocation.containsKey(stockSegment))
				{
					double idealStockSegmentAllocation=idealStockAllocation.get(stockSegment);
					if(stockSegmentAllocation > idealStockSegmentAllocation)
					{
						System.out.println("Taking money out for segment = "+stockSegment);
						//take money out equal to difference in percentage 
						double differencePercentage=stockSegmentAllocation - idealStockSegmentAllocation;
						double currentValueInSegment=afterSectorDistributionValueMap.get(stockSegment);
						double amountToRedeem=(totalDirectEquityAmount*differencePercentage)/100.0;
						System.out.println("Amount to redeem in taking all money out = "+amountToRedeem +" to bring % to ideal Allocation = "+idealStockSegmentAllocation);
						//Now we need to decide from which stocks and how much percentage from each to redeem this value
						List<UserPortfolioState> list=stockSegmentWiseUserPortfolioMap.get(stockSegment);					
						
						double amountNotAbleToRedeem=getStockSegmentMoneyOutFromUserPortfolio(totalPortfolioAmountBeforeSuggestion,totalDirectEquityAmount,amountToRedeem,list,afterSectorDistributionPercentageMap,stockRecommendedList,mfRecoList,stockRecoList,stockSegment,suggestion);
						System.out.println("Amount left after redeeming = "+amountToRedeem);
						double afterRedemptionCurrentSegmentValue=currentValueInSegment-amountToRedeem;
						totalCashAfterRedemptionOfStocks+=amountToRedeem-amountNotAbleToRedeem;
					}
					else if(stockSegmentAllocation < idealStockSegmentAllocation)
					{
						//we will not run add money to stocks here becoz it might be possible that when we enter this function we straight come into 'else if' condition and at that time we might not have redeemed money and hence we might not have any cash to invest 
					}
					else
						System.out.println("Allocation in "+stockSegment+" is equal to "+idealStockSegmentAllocation);
				}
				else
				{
					System.out.println("Taking every money out from stock segment = "+stockSegment);
					//redeem everything from this stock since there is no ideal segment allocation for this segment
					//take out complete money from those stocks which have segment set as 'stockSegment' 
					double differencePercentage=stockSegmentAllocation - 0;
					double currentValueInSegment=afterSectorDistributionValueMap.get(stockSegment);
					double amountToRedeem=currentValueInSegment;//we didn't use this formula((totalDirectEquityAmount*differencePercentage)/100.0) as in above if case becoz here we are redeeming complete stockSegment
					System.out.println("Amount to redeem in taking all money out = "+amountToRedeem);
					//Now we need to decide from which stocks and how much percentage from each to redeem this value
					List<UserPortfolioState> list=stockSegmentWiseUserPortfolioMap.get(stockSegment);
					double amountNotAbleToRedeem=getStockSegmentMoneyOutFromUserPortfolio(totalPortfolioAmountBeforeSuggestion, totalDirectEquityAmount,amountToRedeem,list,afterSectorDistributionPercentageMap,stockRecommendedList,mfRecoList,stockRecoList,stockSegment,suggestion);
					System.out.println("Amount left in stocks after redeeming which were not redeemed= "+amountNotAbleToRedeem);
					double afterRedemptionCurrentSegmentValue=currentValueInSegment-amountToRedeem;
					if(afterRedemptionCurrentSegmentValue>0)
					{
						//this case might come because of that condition that stocks can be sold in whole Number units.
						//throw new FundexpertException("This case should not come bcoz we are redeem complete units.");
					}
					totalCashAfterRedemptionOfStocks+=amountToRedeem-amountNotAbleToRedeem;
				}
			}
			System.out.println("Adding those stocks which were in ideal allocation = "+afterSectorDistributionPercentageMap);
			System.out.println("totalCashAfterRedemptionOfStocks = "+totalCashAfterRedemptionOfStocks);
			
			totalCashAfterBuyingStocks=totalCashAfterRedemptionOfStocks;
			
			//we need to add those segments in afterDistributionPercentageMap which are not already there but are in idealStockAllocation
			//so that we can bring users stock segment percentage allocation to one in idealStockAllocation Percentage
			addStockSegmentsWhichAreNotInUsersStockSegments(idealStockAllocation,afterSectorDistributionPercentageMap);
			
			//we also need to remove Percentage Allocation for that segment which is not in IdealSegmentAllocation but exists in afterSectorDistributionPercentageMap
			removeStockSegmentsWhichAreInUsersStockSegments(idealStockAllocation,afterSectorDistributionPercentageMap);
			System.out.println("After updating Value MapAsPerPercentageMap and adding those stocks which were in ideal allocation but not in userPortfolio = "+afterSectorDistributionPercentageMap);
			
			//since we have manipulated 'afterSectorDistributionPercentageMap' in 'getStockSegmentMoneyOutFromUserPortfolio' function we need to update its corresponding value
			//now since we are buying Stocks we need to bring ratio equal to ideal percentage allocation
			//
			updateValueMapAsPerPercentageMap(totalDirectEquityAmount,afterSectorDistributionValueMap,afterSectorDistributionPercentageMap);
			System.out.println("########################################\n####################################33\n####################################3\n###########################################\n#############################################");
			//below for loop will loop through each stock segment and add money to individual segment if that segment's allocation is less than the ideal one
			//we can also introduce new segment if idealStockSegment has the segment but user has no stock in that segment - in that case we have written a function 'addStockSegmentsWhichAreNotInUsersStockSegments' 
			for(Map.Entry<String, Double> map:afterSectorDistributionPercentageMap.entrySet())
			{
				String stockSegment=map.getKey();
				double stockSegmentAllocation=map.getValue();
				if(idealStockAllocation.containsKey(stockSegment))
				{
					double idealStockSegmentAllocation=idealStockAllocation.get(stockSegment);
					if(stockSegmentAllocation < idealStockSegmentAllocation)
					{
						//users stockSegments percentageAllocation is less than idealAllocation 
						double differencePercentage=idealStockSegmentAllocation - stockSegmentAllocation;
						System.out.println("Difference in Percentage = "+differencePercentage+" for stock segment = "+stockSegment);
						double currentValueInSegment=afterSectorDistributionValueMap.get(stockSegment);
						double amountToBuy=(totalDirectEquityAmount*differencePercentage)/100.0;
						System.out.println("Amount to be spend = "+amountToBuy);
						//Now we need to decide into which stocks and how much percentage into each to buy this value
						List<UserPortfolioState> list=stockSegmentWiseUserPortfolioMap.get(stockSegment);					
						if(list==null)
						{
							list=new ArrayList<UserPortfolioState>();
							stockSegmentWiseUserPortfolioMap.put(stockSegment, list);
							System.out.println("No stocks exists in UserPortfolioState in segment = "+stockSegment);
						}
						
						double amountNotAbleToInvestAnywhere=putStockSegmentMoneyIntoUserPortfolio(totalPortfolioAmountBeforeSuggestion,totalDirectEquityAmount,amountToBuy,list,afterSectorDistributionPercentageMap,stockRecommendedList,mfRecoList,stockRecoList,stockSegment,suggestion);
						//double afterBuyingCurrentValueInSegment=currentValueInSegment+(amountToBuy-amountNotAbleToInvestAnywhere);
						
						totalCashAfterBuyingStocks-=(amountToBuy-amountNotAbleToInvestAnywhere);
						System.out.println("Total cash after buying stocks = "+totalCashAfterBuyingStocks);
					}
					else if(stockSegmentAllocation > idealStockSegmentAllocation)
					{
						//this case might arrive bcoz we might not be able to sell units in getStockSegmentMoneyOutFromUserPortfolio bcoz of higher Nav of Stock
						//this case should not arrive becoz we have already taken money out from each stock's whose percentage allocation was greater than ideal percentage allocation 
						//throw new FundexpertException("Unconditional State arises.State - there are still some stock segments left from which we have to take money out, which is not possible bcoz currently we are buying stocks and already done with redeeming them.");
					}
					else
						System.out.println("Allocation in "+stockSegment+" is equal to "+idealStockSegmentAllocation);
				}
				else
				{
					//this case should not come here instead it would have already been executed when we are redeeming stocks in above code
				}
			}
			System.out.println("Stock subsegment % allocation map = "+stocksSubSegmentPercentageAllocationMap);
			System.out.println("MF subsegment % allocation map = "+mfSubSegmentPercentageAllocationMap);
			//since we have manipulated 'afterSectorDistributionPercentageMap' in 'putStockSegmentMoneyIntoUserPortfolio' function we need to update its corresponding value
			//TODO when we are done with above for loop make sure that we make changes in ('stockSegmentAfterSuggestionAllocationMap'/'stockSegmentAfterSuggestionAssetValueMap')
			updateValueMapAsPerPercentageMap(totalDirectEquityAmount,afterSectorDistributionValueMap,afterSectorDistributionPercentageMap);
			
			return totalCashAfterBuyingStocks;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		
	}
	
	//things to remember when taking money out from stock Segment
	/* 0)totalPortfolioAmount is the original portfolio amount
	 * 1)totalAssetAmount is sum of Direct Equities Only
	 * 2)list contains only those userPortfolioState which are stocks and which have same segment/sectors
	 * 3)distributionAllocationMap has 'sector'-'percent' distribution where percent is calculated using only directEquityAmount and not MutualFunds Equities.
	 * 4)amountToRedeem will come out as cash at the point where this function is called.
	 */
	public Double getStockSegmentMoneyOutFromUserPortfolio(double totalPortfolioAmountBeforeSuggestion,double totalAssetAmount,double amountToRedeem,List<UserPortfolioState> list,Map<String,Double> distributionAllocationMap,List<Integer> stockRecommendedList,List<Long> mfRecoList,List<Integer> stockRecoList,String stockSegment,SchemeLevelSuggestion sls) throws Exception
	{
		//when we are using this function to take money out from stock sectors then totalAssetAmount is totalAmountInDE and distributionAllocationMap contains percentage allocation of sectors where percentage is calculated using totalAmountInDE
		//in case of stock do not take money out from those which are in stock_RecommendedList/stock_reco_list except when there is nothing left in BlackList
		//if there are no stocks that belong to stock_RecommendedList/stock_reco_list then take money out equally
		//if after taking all money out from blackList funds there is still amount left to redeem then take out money from whiteListFunds
		//if ideal is 30% and we have reached 32% and there are some stocks in blackList then take money out using that stock which brings % closer to 30%
		//remove those UserPortfolioState from blackList and whiteList which have been completely redeemed
		List<Action> actionList=new ArrayList<Action>();
		Map<Integer,Stocks> usersStockMap=sls.getUsersStockMap();
		List<Stocks> stocksInThisSegment=segmentWiseStockMap.get(stockSegment);
		
		System.out.println("Distribution Allocation Map for Stock Segment = "+distributionAllocationMap+" amountToRedeem = "+amountToRedeem);
		System.out.println("Stocks SubSegment Allocation Map for Stock SubSegment = "+stocksSubSegmentPercentageAllocationMap);
		System.out.println("IdealStockSubSegment allocation Map = "+idealStockSubSegmentValueMap);
		for(Map.Entry<String, Double> map:stocksSubSegmentPercentageAllocationMap.entrySet())
		{
			System.out.print(map.getKey()+"="+(map.getValue()*totalPortfolioAmountBeforeSuggestion/100.0));
		}
		try
		{
			List<UserPortfolioState> blackList=new ArrayList<UserPortfolioState>(list.size()/2);
			List<UserPortfolioState> whiteList=new ArrayList<UserPortfolioState>(list.size()/2);
			
			if(list!=null && list.size()!=0)
			{
				for(int i=0;i<list.size();i++)
				{
					UserPortfolioState ups=(UserPortfolioState)list.get(i);
					int scCode=(int)(ups.getAssetId());
					Stocks matchedStock=null;
					if(stocksInThisSegment!=null)
					{
						for(Stocks stock:stocksInThisSegment)
						{
							if(scCode==stock.getScCode())
							{
								matchedStock=stock;
								break;
							}
						}
					}
					if(matchedStock!=null)
						whiteList.add(ups);
					else 
					{
						blackList.add(ups);
					}
				}
			}
			//order blackList on the basis of highest NAV
			blackList.sort(new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					UserPortfolioState ups1=(UserPortfolioState)o1;
					UserPortfolioState ups2=(UserPortfolioState)o2;
					Stocks s1=usersStockMap.get((int)ups1.getAssetId());
					Stocks s2=usersStockMap.get((int)ups2.getAssetId());
					if(s1.getClosePrice()<s2.getClosePrice())
						return 1;
					else
						return -1;
				}
			});
			
			if(!distributionAllocationMap.containsKey(stockSegment))
			{
				throw new FundexpertException("distributionAllocationMap does not conain Stock Segment present in User's Portfolio while redeeming stocks.");
			}
			System.out.println("WhiteList.size()="+whiteList.size()+" blacklist.size()="+blackList.size());
			double percentageToRedeem=0,percentageToRedeemFromTotalPortfolio=0;
			double totalPercentageToRedeem=0;
			if(blackList.size()!=0)
			{
				int scCode=0;
				double amountToRedeemFromEachAsset=amountToRedeem/blackList.size();
				//System.out.println("Amount to redeem from each asset before fractal calculation = "+amountToRedeemFromEachAsset);
				double fractionalPartOfAmountToRedeemFromEachAsset=0;//amountToRedeemFromEachAsset-Math.round(amountToRedeemFromEachAsset);
				//we need to remove fraction part bcoz if for eg say 'amountToRedeemFromEachAsset'=3.333 then it will be difficult to redeem any whole number units
				//amountToRedeemFromEachAsset=Math.round(amountToRedeemFromEachAsset);
				//System.out.println("Amount to redeem from each asset after fractal calcultion = "+amountToRedeemFromEachAsset);
				for(int i=0;i<blackList.size();i++)
				{
					if(amountToRedeem>0)
					{
						//before above for loop we have made 'amountToRedeemFromAsset' a whole number , so remaining fractional part will be added back
						fractionalPartOfAmountToRedeemFromEachAsset=amountToRedeemFromEachAsset-Math.round(amountToRedeemFromEachAsset);
						System.out.println("Fractal part will be added inot amountToRedeem whose val = "+fractionalPartOfAmountToRedeemFromEachAsset);
						amountToRedeemFromEachAsset=Math.round(amountToRedeemFromEachAsset);
						amountToRedeem+=fractionalPartOfAmountToRedeemFromEachAsset;
						
						UserPortfolioState ups=blackList.get(i);
						scCode=(int)ups.getAssetId();
						Stocks stock=usersStockMap.get(scCode);
						double currentAssetValue=ups.getCurrentAssetValue();
						System.out.println("scCode= "+scCode+" CurrentAssetValue = "+currentAssetValue+" amountToRedeemFromEachAsset = "+amountToRedeemFromEachAsset+" totalAssetAmount = "+totalAssetAmount);
						if(currentAssetValue >= amountToRedeemFromEachAsset)
						{
							//below if case decides if closePrice<amountToRedeemFromEachAsset
							//>=0 makes sure that atleast 1 units can be bought using amountToRedeemFromEachAsset 
							if(amountToRedeemFromEachAsset/stock.getClosePrice()>=1 && amountToRedeemFromEachAsset%stock.getClosePrice()>0)
							{
								System.out.println("We will redeem this stock bcoz we can get whole units from it.");
								//this means we cannot redeem whole number units for this price 'amountToRedeemFromEachAsset'
								//but we can redeem whole number units by adjusting 'amountToRedeemFromEachAsset' on the basis of stock's closePrice
								if(amountToRedeemFromEachAsset%stock.getClosePrice()>0)
								{
									System.out.print("Need to find new amountToRedeemFromEachAsset so that we can redeem whole number units.New amountToRedeemFromEachAsset = ");
									//we will not redeem more than 'amountToRedeemFromEachAsset' money but can redeem less
									double value=amountToRedeemFromEachAsset%stock.getClosePrice();
									
									System.out.println("AmountToRdeem in whole units calculation = "+amountToRedeem+" value to be deducted="+value);
									//this value 
									//amountToRedeem+=value;
									System.out.println("AmountToRdeem in whole units calculation after deducting value = "+amountToRedeem);
									//amountToRedeemFromEachAsset=Math.round(amountToRedeemFromEachAsset-(value));
									amountToRedeemFromEachAsset=amountToRedeemFromEachAsset-value;
									System.out.println(amountToRedeemFromEachAsset);
								}
								else
								{
									System.out.println("Close price is multiple of amountToRedeemFromEachAsset.");
									//here we will redeem complete 'amountToRedeemFromEachAsset' because of this condition 'amountToRedeemFromEachAsset%stock.getClosePrice()=0'
								}
							}
							else
							{
								System.err.println("Cannot redeem whole number units from this stock.ScCode="+scCode);
								amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1)-i);
								continue;
							}
								
							//here 'amountToRedeemFromEachAsset' is a double value which after redeeming from this stock will give us whole number to redeem
							percentageToRedeem=(amountToRedeemFromEachAsset/totalAssetAmount)*100.0;
							percentageToRedeemFromTotalPortfolio=(amountToRedeemFromEachAsset/totalPortfolioAmountBeforeSuggestion)*100.0;
							
							/*Double existingStockSubSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(stock.getType());
							if(existingStockSubSegmentPercentage!=null)
							{
								double idealStockSubSegmentValue=idealStockSubSegmentValueMap.get(stock.getType());
								System.out.println("While selling , Segment = "+stockSegment+" Existing stockSubSegment "+stock.getType()+" has "+(existingStockSubSegmentPercentage*totalPortfolioAmountBeforeSuggestion/100.0)+" money, while  ideal allocation in this subgment is= "+idealStockSubSegmentValue);
								if(existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0 <= idealStockSubSegmentValue)
								{
									System.out.println("cannot redeem amt due to already less subSegment allocation hence, New amountToSpendInEachStock = "+amountToRedeemFromEachAsset);
									amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1)-i);
									continue;
								}
								else if((existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0)-amountToRedeemFromEachAsset<idealStockSubSegmentValue)
								{
									//loop through whiteList and throw exception if no stock found with this 'stock.getSubSegment()'
									boolean flag=false;//no Stock exists in whitelist with subSegment=stock.getSibSegment()
									for(UserPortfolioState ups1:blackList)
									{
										System.out.println("stock1 type = "+ups1.getSubSegment()+" stock type ="+stock.getType());
										if(!ups1.getSubSegment().equals(stock.getType()) && (int)ups.getAssetId()!=stock.getScCode())
										{
											flag=true;
											break;
										}
									}
									if(!flag)
										throw new FundexpertException("There should be atleast 3 stocks in each subSegment for each segment(sector) for proper allocation of Assets.");
									System.out.println("percentage to Redeem in totalPortfolio comes out to be = "+percentageToRedeemFromTotalPortfolio+" initially.");
								
									percentageToRedeemFromTotalPortfolio=((existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0-idealStockSubSegmentValue)*100.0)/totalPortfolioAmountBeforeSuggestion;
									System.out.println("Now total percentageToRedeemFromTotalPortfolio = "+percentageToRedeemFromTotalPortfolio);
									
									amountToRedeemFromEachAsset=(totalPortfolioAmountBeforeSuggestion*percentageToRedeemFromTotalPortfolio)/100.0;
									percentageToRedeem=(amountToRedeemFromEachAsset/totalAssetAmount)*100.0;
									System.out.println("1.-1)New Partial Percentage to redeem from blackList= "+percentageToRedeem+" new amountToSpendInEachStock = "+amountToRedeemFromEachAsset);
								}
							}*/
							
							System.out.println("1)Percentage to Redeem ="+percentageToRedeem);
							Action action=new Action();
							action.setAction(2);
							action.setAssetType(ups.getAssetType());
							action.setAssetId(ups.getAssetId());
							action.setAmount(amountToRedeemFromEachAsset);
							actionList.add(action);
							System.out.println("Amount to rdeeem before = "+amountToRedeem+" from each asset value="+amountToRedeemFromEachAsset);
							amountToRedeem-=amountToRedeemFromEachAsset;
							System.out.println("Amount to rdeeem, after = "+amountToRedeem+" from each asset value="+amountToRedeemFromEachAsset);
							
							//update ups by currentAssetValue-=amountToRedeemFromEachAsset; and this update value will be used in 'bestStockToTakeMoneyOutFromBlackList'
							updateUserPortfolioState(stock, null, amountToRedeemFromEachAsset, ups, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
							
							amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1)-i);
							System.out.println("123$ amountToRedeemFromEachAsset ="+amountToRedeemFromEachAsset);
							double newPercentage=distributionAllocationMap.get(ups.getSegment())-percentageToRedeem;
							System.out.println(" Old Percentage from Segment("+stockSegment+") Distribution = "+(distributionAllocationMap.get(ups.getSegment())-totalPercentageToRedeem));
							totalPercentageToRedeem+=percentageToRedeem;
							System.out.println(" New Percentage in Segement("+stockSegment+") Distribution  = "+(distributionAllocationMap.get(ups.getSegment())-totalPercentageToRedeem));
							
						}
						else
						{
							//here we are redeeming all units of this stock
							amountToRedeemFromEachAsset=currentAssetValue;
							percentageToRedeem=(amountToRedeemFromEachAsset/totalAssetAmount)*100.0;
							percentageToRedeemFromTotalPortfolio=(amountToRedeemFromEachAsset/totalPortfolioAmountBeforeSuggestion)*100.0;
							
							/*Double existingStockSubSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(stock.getType());
							if(existingStockSubSegmentPercentage!=null)
							{
								double idealStockSubSegmentValue=idealStockSubSegmentValueMap.get(stock.getType());
								System.out.println("1.2While selling , Segment = "+stockSegment+" Existing stockSubSegment "+stock.getType()+" has "+(existingStockSubSegmentPercentage*totalPortfolioAmountBeforeSuggestion/100.0)+" money, while  ideal allocation in this subgment is= "+idealStockSubSegmentValue);
								if(existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0 <= idealStockSubSegmentValue)
								{
									System.out.println("1.2cannot redeem amt due to already less subSegment allocation hence, New amountToSpendInEachStock = "+amountToRedeemFromEachAsset);
									amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1)-i);
									continue;
								}
								else if((existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0)-amountToRedeemFromEachAsset<idealStockSubSegmentValue)
								{
									//loop through whiteList and throw exception if no stock found with this 'stock.getSubSegment()'
									boolean flag=false;//false means no Stock exists in blackList with subSegment=stock.getSibSegment()
									for(UserPortfolioState ups1:blackList)
									{
										System.out.println("stock1 type = "+ups1.getSubSegment()+" stock type ="+stock.getType());
										if(!ups1.getSubSegment().equals(stock.getType()) && (int)ups.getAssetId()!=stock.getScCode())
										{
											flag=true;
											break;
										}
									}
									if(!flag)
										throw new FundexpertException("There should be atleast 3 stocks in each subSegment for each segment(sector) for proper allocation of Assets.");
									System.out.println("percentage to Redeem in totalPortfolio comes out to be = "+percentageToRedeemFromTotalPortfolio+" initially.");
								
									percentageToRedeemFromTotalPortfolio=((existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0-idealStockSubSegmentValue)*100.0)/totalPortfolioAmountBeforeSuggestion;
									System.out.println("Now total percentageToRedeemFromTotalPortfolio = "+percentageToRedeemFromTotalPortfolio);
									
									amountToRedeemFromEachAsset=(totalPortfolioAmountBeforeSuggestion*percentageToRedeemFromTotalPortfolio)/100.0;
									percentageToRedeem=(amountToRedeemFromEachAsset/totalAssetAmount)*100.0;
									System.out.println("1.-2)New Partial Percentage to redeem from blackList= "+percentageToRedeem+" new amountToSpendInEachStock = "+amountToRedeemFromEachAsset);
								}
							}*/
							
							
							System.out.println("2)Percentage to Redeem ="+percentageToRedeem);
							Action action=new Action();
							action.setAction(2);
							action.setAssetType(ups.getAssetType());
							action.setAssetId(ups.getAssetId());
							action.setAmount(ups.getCurrentAssetValue());
							actionList.add(action);
							amountToRedeem-=amountToRedeemFromEachAsset;
							
							updateUserPortfolioState(stock, null, currentAssetValue, ups, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
							amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1)-i);
							System.out.println("AmountToRedeemFromEachAsset = "+amountToRedeemFromEachAsset+"  . If this value is infinity then ignore it we are not using this value any further.");
							System.out.println(" Old Percentage from Segment("+stockSegment+") Distribution = "+(distributionAllocationMap.get(ups.getSegment())-totalPercentageToRedeem));
							totalPercentageToRedeem+=percentageToRedeem;
							//blackList.remove(i);
							System.out.println(" New Percentage in Segement("+stockSegment+") Distribution  = "+(distributionAllocationMap.get(ups.getSegment())-totalPercentageToRedeem));
						}
						System.out.println("while redeeming Amount to redeem = "+amountToRedeem);
						//understand this : stocksSubSegmentPercentageAllocationMap contains LC/MC/SC only for Stocks 
						System.out.println("Stock subSegmentPercentageAllocation Map = "+stocksSubSegmentPercentageAllocationMap);
						Double subSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(ups.getSubSegment());
						if(subSegmentPercentage==null)
						{
							throw new UnConditionalException("stocksSubSegmentPercentageAllocationMap should have contained "+ups.getSubSegment()+" subSegment.Verify its value comming from SchemeLevelSuggestion.");
						}
						else
						{
							subSegmentPercentage-=percentageToRedeemFromTotalPortfolio;
							stocksSubSegmentPercentageAllocationMap.put(ups.getSubSegment(),subSegmentPercentage);
						}
						System.out.println("Stock subSegmentPercentageAllocation Map after redeeming percent = "+percentageToRedeemFromTotalPortfolio+" from subSegment ="+ups.getSubSegment()+" subSegmentPercentage allocation  "+stocksSubSegmentPercentageAllocationMap);
					}
					else
						break;
				}
				Double sectorDistributionPercentage=distributionAllocationMap.get(stockSegment);
				if(sectorDistributionPercentage!=null)
				{
					sectorDistributionPercentage-=totalPercentageToRedeem;
					distributionAllocationMap.put(stockSegment,sectorDistributionPercentage);
				}
				else
					throw new FundexpertException("Sector Distribution Map does not contain stock segment from which we have to take money out.");
			}
			System.out.println("Sector distribution !@#= "+distributionAllocationMap);
			System.out.println("Amount to Redeem After blackList funds. = "+amountToRedeem);
			if(amountToRedeem>0 && blackList!=null && blackList.size()>0)
			{
				//order blackList on the basis of smallest assetValue
				//reason of doing such order is bcoz we need to get rid of such stocks which have low assetAlocation so that we can redeem whole units from it.
				blackList.sort(new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						UserPortfolioState ups1=(UserPortfolioState)o1;
						UserPortfolioState ups2=(UserPortfolioState)o2;
						if(ups1.getCurrentAssetValue()>ups2.getCurrentAssetValue())
							return 1;
						else
							return -1;
					}
				});
				//if still there are few Stocks in BlackList then take money == 'amountToRedeem' out from it even if amountToRdeem becomes (-)tive but make sure that total amountToRedeem is actually greater than asset.currentAssetValue
				Map<UserPortfolioState,Double> map1 = bestStockToTakeMoneyOutFromBlackList(blackList,amountToRedeem,usersStockMap);
				for(Map.Entry<UserPortfolioState, Double> map:map1.entrySet())
				{
					UserPortfolioState ups1=map.getKey();
					Stocks stock=usersStockMap.get((int)ups1.getAssetId());
					double amountToRedeemFromThisStock=map.getValue();
					percentageToRedeem=(amountToRedeemFromThisStock/totalAssetAmount)*100.0;
					percentageToRedeemFromTotalPortfolio=(amountToRedeemFromThisStock/totalPortfolioAmountBeforeSuggestion)*100.0;

					/*Double existingStockSubSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(stock.getType());
					if(existingStockSubSegmentPercentage!=null)
					{
						double idealStockSubSegmentValue=idealStockSubSegmentValueMap.get(stock.getType());
						System.out.println("1.3While selling , Segment = "+stockSegment+" Existing stockSubSegment "+stock.getType()+" has "+(existingStockSubSegmentPercentage*totalPortfolioAmountBeforeSuggestion/100.0)+" money, while  ideal allocation in this subgment is= "+idealStockSubSegmentValue);
						if(existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0 <= idealStockSubSegmentValue)
						{
							System.out.println("1.3cannot redeem amt due to already less subSegment allocation hence, New amountToSpendInEachStock = "+amountToRedeemFromThisStock);
							continue;
						}
						else if((existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0)-amountToRedeemFromThisStock<idealStockSubSegmentValue)
						{
							//loop through whiteList and throw exception if no stock found with this 'stock.getSubSegment()'
							boolean flag=false;//false means no Stock exists in blackList with subSegment=stock.getSibSegment()
							for(UserPortfolioState ups2:blackList)
							{
								System.out.println("stock1 type = "+ups2.getSubSegment()+" stock type ="+stock.getType());
								if(!ups2.getSubSegment().equals(stock.getType()) && (int)ups2.getAssetId()!=stock.getScCode())
								{
									flag=true;
									break;
								}
							}
							if(!flag)
								throw new FundexpertException("There should be atleast 3 stocks in each subSegment for each segment(sector) for proper allocation of Assets.");
							System.out.println("percentage to Redeem in totalPortfolio comes out to be = "+percentageToRedeemFromTotalPortfolio+" initially.");
						
							percentageToRedeemFromTotalPortfolio=((existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0-idealStockSubSegmentValue)*100.0)/totalPortfolioAmountBeforeSuggestion;
							System.out.println("Now total percentageToRedeemFromTotalPortfolio = "+percentageToRedeemFromTotalPortfolio);
							
							amountToRedeemFromThisStock=(totalPortfolioAmountBeforeSuggestion*percentageToRedeemFromTotalPortfolio)/100.0;
							percentageToRedeem=(amountToRedeemFromThisStock/totalAssetAmount)*100.0;
							System.out.println("1.-3)New Partial Percentage to redeem from blackList= "+percentageToRedeem+" new amountToSpendInEachStock = "+amountToRedeemFromThisStock);
						}
					}*/
					
					
					amountToRedeem-=amountToRedeemFromThisStock;
					System.out.println("3)Percentage to redeem = "+percentageToRedeem);
					Action action=new Action();
					action.setAction(2);
					action.setAssetType(ups1.getAssetType());
					action.setAssetId(ups1.getAssetId());
					action.setAmount(amountToRedeemFromThisStock);
					actionList.add(action);
					totalPercentageToRedeem+=percentageToRedeem;
					updateUserPortfolioState(stock, null, amountToRedeemFromThisStock, ups1, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
					
					//if(Math.round(amountToRedeemFromThisStock)==Math.round(ups1.getCurrentAssetValue()))
						//blackList.remove(ups1);
					Double subSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(ups1.getSubSegment());
					if(subSegmentPercentage==null)
					{
						throw new UnConditionalException("stocksSubSegmentPercentageAllocationMap should have contained "+ups1.getSubSegment()+" subSegment.Verify its value comming from SchemeLevelSuggestion.");
					}
					else
					{
						subSegmentPercentage-=percentageToRedeemFromTotalPortfolio;
						stocksSubSegmentPercentageAllocationMap.put(ups1.getSubSegment(),subSegmentPercentage);
					}
				}
				Double sectorDistributionPercentage=distributionAllocationMap.get(stockSegment);
				if(sectorDistributionPercentage!=null)
				{
					sectorDistributionPercentage-=percentageToRedeem;
					distributionAllocationMap.put(stockSegment,sectorDistributionPercentage);
				}
				else
					throw new FundexpertException("Sector Distribution Map does not contain stock segment from which we have to take money out.");
			}
			if(amountToRedeem>0)
			{
				//ordering white list by highest nav
				whiteList.sort(new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						UserPortfolioState s1=(UserPortfolioState)o1;
						UserPortfolioState s2=(UserPortfolioState)o2;
						if(s1.getCurrentAssetValue()>s2.getCurrentAssetValue())
							return 1;
						else
							return -1;
					}
				});
				//in whiteList we will give equal priority to recommended and reco funds
				double amountToRedeemFromEachAsset = amountToRedeem/whiteList.size();
				totalPercentageToRedeem=0;
				int scCode=0;
				for(int i=0;i<whiteList.size();i++)
				{
					if(amountToRedeem>0)
					{
						UserPortfolioState ups=whiteList.get(i);
						scCode=(int)ups.getAssetId();
						Stocks stock=usersStockMap.get(scCode);
						double currentAssetValue=ups.getCurrentAssetValue();
						System.out.println("scCode = "+scCode+"  CurrentAssetValue = "+currentAssetValue+" amountToRedeemFromEachAsset = "+amountToRedeemFromEachAsset);
						if(currentAssetValue >= amountToRedeemFromEachAsset)
						{
							//below if case decides if closePrice<amountToRedeemFromEachAsset
							//>=0 makes sure that atleast 1 units can be bought using amountToRedeemFromEachAsset 
							if(amountToRedeemFromEachAsset/stock.getClosePrice()>=1 && amountToRedeemFromEachAsset%stock.getClosePrice()>=0)
							{
								System.out.println("We will redeem this stock bcoz we can get whole units from it.");
								//this means we cannot redeem whole number units for this price 'amountToRedeemFromEachAsset'
								//but we can redeem whole number units by adjusting 'amountToRedeemFromEachAsset' on the basis of stock's closePrice
								if(amountToRedeemFromEachAsset%stock.getClosePrice()>0)
								{
									System.out.print("Need to find new amountToRedeemFromEachAsset so that we can redeem whole number units.New amountToRedeemFromEachAsset = ");
									//we will not redeem more than 'amountToRedeemFromEachAsset' money but can redeem less
									amountToRedeemFromEachAsset=Math.round(amountToRedeemFromEachAsset-(amountToRedeemFromEachAsset%stock.getClosePrice()));
									System.out.println(amountToRedeemFromEachAsset);
								}
								else
								{
									System.out.println("Close price is multiple of amountToRedeemFromEachAsset.");
									//here we will redeem complete 'amountToRedeemFromEachAsset' because of this condition 'amountToRedeemFromEachAsset%stock.getClosePrice()=0'
								}
							}
							else
							{
								System.err.println("Cannot redeem whole number units from this stock.ScCode="+scCode);
								amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1)-i);
								continue;
							}
							percentageToRedeem=(amountToRedeemFromEachAsset/totalAssetAmount)*100;
							percentageToRedeemFromTotalPortfolio=(amountToRedeemFromEachAsset/totalPortfolioAmountBeforeSuggestion)*100.0;
							

							/*
							 * while selling stocks on the basis of segments we also have to take care of the fact that subSegment allocation does not get faulty so
							 * if we have to sell 500rs from stock A whose subSegment is LC and LC have 9% allocation in UserPortfolio and if ideal is 10% for LC allocation then we cannot sell stock A hence we have to try with other stock in same segment but different subSegment
							 */
							/*Double existingSubSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(ups.getSubSegment());
							if(existingSubSegmentPercentage!=null)
							{
								double idealStockSubSegmentAllocation=idealStockSubSegmentAllocationMap.get(ups.getSubSegment());
								System.out.println("Segment = "+stockSegment+" Existing stockSubSegment+"+ups.getSubSegment()+" has "+existingSubSegmentPercentage+"%, while  ideal allocation in this subgment is= "+idealStockSubSegmentAllocation);
								if(existingSubSegmentPercentage.doubleValue()<=idealStockSubSegmentAllocation)
								{
									System.out.println("cannot redeem amt due to less subsegment allocation hence New amountToRedeemFrom each asset = "+amountToRedeemFromEachAsset);
									amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1)-i);
									continue;
								}
								else if(existingSubSegmentPercentage.doubleValue()-percentageToRedeemFromTotalPortfolio<idealStockSubSegmentAllocation)
								{
									System.out.println("percentage to redeem from totalportfolio comes out to be = "+percentageToRedeemFromTotalPortfolio);
									percentageToRedeemFromTotalPortfolio=existingSubSegmentPercentage.doubleValue()-idealStockSubSegmentAllocation;
									amountToRedeemFromEachAsset=(totalPortfolioAmountBeforeSuggestion*percentageToRedeemFromTotalPortfolio)/100.0;
									percentageToRedeem=(amountToRedeemFromEachAsset/totalAssetAmount)*100.0;
									System.out.println("1.-1)New Percentage to redeem from whiteList= "+percentageToRedeem);
								}
							}*/
							System.out.println("1)Percentage to Redeem from whiteList="+percentageToRedeem);
							Action action=new Action();
							action.setAction(2);
							action.setAssetType(ups.getAssetType());
							action.setAssetId(ups.getAssetId());
							action.setAmount(amountToRedeemFromEachAsset);
							actionList.add(action);
							
							//update ups by currentAssetValue-=amountToRedeemFromEachAsset; and this update value will be used in 'bestStockToTakeMoneyOutFromBlackList'
							updateUserPortfolioState(stock, null, amountToRedeemFromEachAsset, ups, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
							
							amountToRedeem-=amountToRedeemFromEachAsset;
							double newPercentage=distributionAllocationMap.get(ups.getSegment())-percentageToRedeem;
							System.out.println(" Old Percentage from Segment("+stockSegment+") Distribution = "+(distributionAllocationMap.get(ups.getSegment())-totalPercentageToRedeem));
							totalPercentageToRedeem+=percentageToRedeem;
							System.out.println(" New Percentage in Segement("+stockSegment+") Distribution  = "+(distributionAllocationMap.get(ups.getSegment())-totalPercentageToRedeem));
						}
						else
						{
							percentageToRedeem=(currentAssetValue/totalAssetAmount)*100.0;
							percentageToRedeemFromTotalPortfolio=(currentAssetValue/totalPortfolioAmountBeforeSuggestion)*100.0;
							System.out.println("2)For sector Percentage to Redeem from whiteList="+percentageToRedeem);
							System.out.println("2)For subSegment Percentage to redeem = "+percentageToRedeemFromTotalPortfolio);
							
							/*
							 * while selling stocks on the basis of segments we also have to take care of the fact that subSegment allocation does not get faulty so
							 * if we have to sell 500rs from stock A whose subSegment is LC and LC have 9% allocation in UserPortfolio and if ideal is 10% for LC allocation then we cannot sell stock A hence we have to try with other stock in same segment but different subSegment
							 */
							/*Double existingSubSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(ups.getSubSegment());
							if(existingSubSegmentPercentage!=null)
							{
								double idealStockSubSegmentAllocation=idealStockSubSegmentAllocationMap.get(ups.getSubSegment());
								System.out.println("2)Segment = "+stockSegment+" Existing stockSubSegment+"+ups.getSubSegment()+" has "+existingSubSegmentPercentage+"%, while  ideal allocation in this subgment is= "+idealStockSubSegmentAllocation);
								if(existingSubSegmentPercentage.doubleValue()<=idealStockSubSegmentAllocation)
								{
									System.out.println("2)cannot redeem amt due to less subsegment allocation hence New amountToRedeemFrom each asset = "+amountToRedeemFromEachAsset);
									amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1)-i);
									continue;
								}
								else if(existingSubSegmentPercentage.doubleValue()-percentageToRedeemFromTotalPortfolio<idealStockSubSegmentAllocation)
								{
									System.out.println("2)percentage to redeem from totalportfolio comes out to be = "+percentageToRedeemFromTotalPortfolio);
									percentageToRedeemFromTotalPortfolio=existingSubSegmentPercentage.doubleValue()-idealStockSubSegmentAllocation;
									amountToRedeemFromEachAsset=(totalPortfolioAmountBeforeSuggestion*percentageToRedeemFromTotalPortfolio)/100.0;
									percentageToRedeem=(amountToRedeemFromEachAsset/totalAssetAmount)*100.0;
									System.out.println("1.-1)New Percentage to redeem from whiteList= "+percentageToRedeem);
								}
							}*/
							
							
							Action action=new Action();
							action.setAction(2);
							action.setAssetType(ups.getAssetType());
							action.setAssetId(ups.getAssetId());
							action.setAmount(ups.getCurrentAssetValue());
							actionList.add(action);
							amountToRedeem-=currentAssetValue;
							
							updateUserPortfolioState(stock, null, currentAssetValue, ups, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
							//whiteList.remove(ups);
							amountToRedeemFromEachAsset=amountToRedeem/((whiteList.size()-1)-i);
							double newPercentage=distributionAllocationMap.get(ups.getSegment())-percentageToRedeem;
							System.out.println("Amount to Redeem  =  "+amountToRedeem+" Amount to Redeem From Each Asset = "+amountToRedeemFromEachAsset);
							System.out.println(" Old Percentage from Segment("+stockSegment+") Distribution = "+(distributionAllocationMap.get(ups.getSegment())-totalPercentageToRedeem));
							totalPercentageToRedeem+=percentageToRedeem;
							System.out.println(" New Percentage in Segement("+stockSegment+") Distribution  = "+(distributionAllocationMap.get(ups.getSegment())-totalPercentageToRedeem));
						}
						Double subSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(ups.getSubSegment());
						if(subSegmentPercentage==null)
							throw new UnConditionalException("stocksSubSegmentPercentageAllocationMap should have contained "+ups.getSubSegment()+" subSegment.Verify its value comming from SchemeLevelSuggestion.");
						else
						{
							subSegmentPercentage-=percentageToRedeemFromTotalPortfolio;
							stocksSubSegmentPercentageAllocationMap.put(ups.getSubSegment(),subSegmentPercentage);
						}
					}
					else
						break;
				}
				Double sectorDistributionPercentage=distributionAllocationMap.get(stockSegment);
				if(sectorDistributionPercentage!=null)
				{
					sectorDistributionPercentage-=totalPercentageToRedeem;
					distributionAllocationMap.put(stockSegment,sectorDistributionPercentage);
				}
				else
					throw new FundexpertException("Sector Distribution Map does not contain stock segment from which we have to take money out.");
			}
			System.out.println("After selling stocks subSegment percetage allocation ="+stocksSubSegmentPercentageAllocationMap);
			System.out.println("After selling stocks segment percentage allocation ="+distributionAllocationMap);
			return amountToRedeem;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Error Occurred.") ;
			
		}
	}
	
	//return that amount of amountToBuy which is still left after buying stocks because of higher nav than amount left at the time of buying stock
	//totalAssetAmount is totalDEAmount before suggestion
	//list contains those UserPortfolioState which have stocks in common segment
	//Objective of this function is to spend all 'amountToBuy' in stocks whose segments are common to one in 'list'
	public Double putStockSegmentMoneyIntoUserPortfolio(double totalPortfolioAmount,double totalAssetAmount,double amountToBuy,List<UserPortfolioState> list,Map<String,Double> distributionAllocationMap,List<Integer> stockRecommendedList,List<Long> mfRecoList,List<Integer> stockRecoList,String stockSegment,SchemeLevelSuggestion sls) throws Exception
	{
		//IntialPercentageSector Allocation
		System.out.println("After SchemeLevelSuggestion Sector Distribution Map ="+distributionAllocationMap);
		System.out.println("IdealStockSubSegment allocation Map = "+idealStockSubSegmentValueMap);
		for(Map.Entry<String, Double> map:stocksSubSegmentPercentageAllocationMap.entrySet())
		{
			System.out.print(map.getKey()+"="+(map.getValue()*totalPortfolioAmount/100.0));
		}
		System.out.println();
		List<Action> actionList=new ArrayList<Action>();
		Map<Integer,Stocks> usersStockMap=sls.getUsersStockMap();
		/*
		 * we need to make 'amountToBuy' as close to zero as possible
		 * but due to navOfStock > amountLeft we might not be able to spend money anywhere since we can only buy units in whole number
		 */
		//double totalCashLeftAfterBuyingStocks=totalAssetAmount;
		double totalAmountInvested=0;
		try
		{
			//existingwhitelist contains those stocks which are in list<UserPortfolioState> as well as in perSegmentStocks.
			List<Stocks> existingWhiteList=new ArrayList<Stocks>(10);
			//blacklist is list of scCode of those which are in userPortfolioState but not in perSegmentStocks 
			List<Stocks> blackList=new ArrayList<Stocks>(10);
			
			List<Stocks> stocksInThisSegment=segmentWiseStockMap.get(stockSegment);
			//newWhiteList contains those stocks which are not in userPortfolioState but are in perSegmentStocks
			List<Stocks> newWhiteList=new ArrayList<Stocks>(10);
			newWhiteList.addAll(stocksInThisSegment);
			if(newWhiteList.size()==0)
				throw new FundexpertException("No stocks are defined for segment = "+stockSegment+" in table StocksInStockSegment.");
			if(list!=null && list.size()!=0)
			{
				for(int i=0;i<list.size();i++)
				{
					int scCode=(int)((UserPortfolioState)list.get(i)).getAssetId();
					Stocks matchedStock=null;
					for(Stocks stock:stocksInThisSegment)
					{
						if(scCode==stock.getScCode())
						{
							matchedStock=stock;
							break;
						}
					}
					if(matchedStock!=null)
						existingWhiteList.add(matchedStock);
					else 
					{
						Stocks blackListStock=usersStockMap.get(scCode);
						blackList.add(blackListStock);
					}
				}
			}
			/*System.out.println("newWhiteList");
			newWhiteList.stream().forEach(s->System.out.println(s.getScCode()+" obj="+s));
			System.out.println("existingWhiteList");
			existingWhiteList.stream().forEach(s->System.out.println(s.getScCode()+" obj="+s));*/
			
			//now remove those stocks from newWhiteList which are in usersPortfolioState
			newWhiteList.removeAll(existingWhiteList);
			//Aappend newWhiteList in existingWhitelist
			//now we will deal only with existingWhiteList and blackList
			existingWhiteList.addAll(newWhiteList);
			
			/*System.out.println("newWhiteList");
			newWhiteList.stream().forEach(s->System.out.println(s.getScCode()+" obj="+s));
			System.out.println("existingWhiteList");
			existingWhiteList.stream().forEach(s->System.out.println(s.getScCode()+" obj="+s));
			blackList.stream().forEach(s->System.out.println(s.getScCode()));*/
			
			double 	amountToSpendInEachStock=0,totalPercentageBought=0,partialPercentageBought=0,percentageToBuyInTotalPortfolio=0;
			
			//order existingWhiteList on the basis of highest closePrice
			existingWhiteList.sort(new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					Stocks s1=(Stocks)o1;
					Stocks s2=(Stocks)o2;
					if(s1.getClosePrice()<s2.getClosePrice())
						return 1;
					else
						return -1;
				}
			});
			
			existingWhiteList.forEach(i->System.out.println("whitelist Stock name="+i.getScName()+" Stock ScCode = "+i.getScCode()));
			blackList.forEach(i->System.out.println("Blacklist Stock name="+i.getScName()+" Stock ScCode = "+i.getScCode()));
			/*
			 * Reason for below loop to loop 2 times
			 * in first loop we are buying stocks in order of stocks highest nav in whiteList
			 * because of stocks big nav it might me possible that we are unable to buy any stock for eg amounttobuy=1000 and stock closeprice=1015 we will invest 1000 in first round and in second loop we will spend 15 among remaining stocks
			 */
			Integer numberOfStocks=0;
			int timesKthLoopCanBeRanAtMax=4;
			for(short k=0;k<2;k++)
			{
				if(timesKthLoopCanBeRanAtMax<=0)
					break;
				System.out.println("While Buying Stocks , For k ="+k);
				//Now we need to decide in how many stocks we need to invest 'amountToBuy' by using 'threshholdPercentage'-see description of threshholdPercentage at the time of declaration
				numberOfStocks=getNumberOfAssetOnBasisOfThresholdPercent(amountToBuy, totalAssetAmount, stockThresholdPercentage);
				System.out.println("NumberOfStocks before adjustment="+numberOfStocks);
				if(numberOfStocks==null)
					throw new FundexpertException("Not able to identify No of Stocks to invest in.");
				
				//if numberOfStockss is greater than size(existingWhiteList+blackList) then put numberOfStocks= sumOf(existingWhiteList.size+blackList.size)
				if(numberOfStocks > existingWhiteList.size())
					numberOfStocks=existingWhiteList.size();
				System.out.println("NumberOfStocks="+numberOfStocks+" totalAssetAmount before suggestion = "+totalAssetAmount);
				//when k=1
				/*
				 * we might have achieve idealStockSubSegmentllocation i.e for LC in stocksSubSegmentAllocation and idealStockSubSegmentAllocation is 25% in both or close to 25 in stockSubSegmentAllocation
				 * in that case we need to reduce numberOfStocks by all those stocks which have that subSegment in this case stocks with 'LC' as subSegment,  since we'll not be investing in such subSegments
				 */
				int stocksWithIdealAllocationReachedForTheirSubSegments=idealReachedForSubSegment(existingWhiteList,stocksSubSegmentPercentageAllocationMap,idealStockSubSegmentValueMap,totalPortfolioAmountBeforeSuggestion);
				numberOfStocks-=stocksWithIdealAllocationReachedForTheirSubSegments;
				System.out.println("Number of Stocks after removing stocksWhichReached their idealSubSegmentAllocation = "+numberOfStocks);
				
				amountToSpendInEachStock=amountToBuy/numberOfStocks;
				System.out.println("Amount to spend in each stock = "+amountToSpendInEachStock);
				
				//reason of doing reversing is because in 2nd loop we are left with cash which were not able to be invested in 1st loop
				//and this left cash will be very small amount so number of Stocks will come out to be small,and we can invest whole money only when close price of stock is less.
				//remember existingWhiteList was sorted on the basis of closePrice before entering loop with variable 'k'
				if(k==1)
					Collections.reverse(existingWhiteList);
				
				//now loop through each stock and spend money into it
				for(int i=0;i<existingWhiteList.size();i++)
				{
					System.out.println("for existingWhiteList  I="+i);
					System.out.println("Amount to buy="+amountToBuy+" totalPercentageBiught="+totalPercentageBought+" remainingStocks to invest in="+numberOfStocks);
					if(numberOfStocks>0)
					{
						Stocks stock=existingWhiteList.get(i);
						System.out.println("ScCode="+stock.getScCode()+" closedPrice="+stock.getClosePrice()+" amountToSpendInEachStock="+amountToSpendInEachStock+" segment = "+stock.getSegment());
						if(stock.getClosePrice()>amountToSpendInEachStock)
						{
							System.out.println("Will not buy this stock bcoz of higher close price.");
							//below if tells that remaining stocks(not considering current stock at 'i') left in list are actually less than the number of stocks into which we wanted to spend
							if(((existingWhiteList.size()-1)-i)-numberOfStocks<0)
							{
								System.out.println("NumberOfStocks > ((existingWhiteList.size()-1)-i)");
								int remainingStocksToSpendMoneyOn=((existingWhiteList.size()-1)-i);
								//if(blackList.size()==0 && (existingWhiteList.size()-1)==i)
								if((existingWhiteList.size()-1)==i)
								{
									//this means existingWhiteList is all done and we don't even have any stock in 'blackList' and now we need to break this loop
									//this condition is boundary condition beacuse if 'i' is the last index of existingWhiteList then '(existingWhiteList.size()-1)-i' will give us 0 which will be less than 'numberOfStocks' in this case
									break;
								}
								//else if(blackList.size()>0 && (((existingWhiteList.size()-1)-i)+blackList.size())>=numberOfStocks)
								else if(((existingWhiteList.size()-1)-i)>=numberOfStocks)
								{
									remainingStocksToSpendMoneyOn=numberOfStocks;
									System.out.println("1)remainingStocksToSpendMoneyOn="+remainingStocksToSpendMoneyOn);
								}
								else
								{
									remainingStocksToSpendMoneyOn=(((existingWhiteList.size()-1)-i));
									System.out.println("2)remainingStocksToSpendMoneyOn="+remainingStocksToSpendMoneyOn);
								}
								
								amountToSpendInEachStock=amountToBuy/remainingStocksToSpendMoneyOn;
								numberOfStocks=remainingStocksToSpendMoneyOn;
								continue;
							}
							else
							{
								//below line can be uncommented : will not break the code but will change the amount to be invested in remaining stocks
								//amountToSpendInEachStock=totalCashLeftAfterBuyingStocks/numberOfStocks;
								System.out.println("NumberOfStocks < ((existingWhiteList.size()-1)-i)");
								continue;
							}
							
						}
						else
						{
							if(amountToSpendInEachStock/stock.getClosePrice()>=1 && amountToSpendInEachStock%stock.getClosePrice()>=0)
							{
								if(amountToSpendInEachStock%stock.getClosePrice()>0)
								{
									System.out.print("Need to find new amountToSpendInEachStock so that we can invest whole number units.New amountToSpendInEachStock = ");
									//we will not buy more than 'amountToSpendInEachAsset' money but can buy less
									amountToSpendInEachStock=Math.round(amountToSpendInEachStock-(amountToSpendInEachStock%stock.getClosePrice()));
									System.out.println(amountToSpendInEachStock);
								}
								else
								{
									System.out.println("Stocks close price is multiple of amountToSpendInEachStock.");
								}
							}
							else
							{
								System.err.println("Cannot buy this Stock because its closePrice is higher than the amountToSpendInEachStock.ScCode = "+stock.getScCode());
								continue;
							}
							partialPercentageBought=(amountToSpendInEachStock/totalAssetAmount)*100.0;
							percentageToBuyInTotalPortfolio=(amountToSpendInEachStock/totalPortfolioAmount)*100.0;
							
							/*
							 * while buying stocks on the basis of segments we also have to take care of the fact that subSegment allocation does not get faulty so
							 * if we have to buy 500rs (which is say 2%) from stock A whose subSegment is LC and LC have 11% allocation in UserPortfolio and if ideal is 10% for LC allocation then we cannot buy stock A hence we have to try with other stock in same segment but different subSegment
							 * but if LC current allocation is 9% in user portfolio then we still would invest the difference of 1%(10%-9%)
							 */
							Double existingStockSubSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(stock.getType());
							if(existingStockSubSegmentPercentage!=null)
							{
								double idealStockSubSegmentValue=idealStockSubSegmentValueMap.get(stock.getType());
								System.out.println("Segment = "+stockSegment+" Existing stockSubSegment "+stock.getType()+" has "+(existingStockSubSegmentPercentage*totalPortfolioAmount/100.0)+" money, while  ideal allocation in this subgment is= "+idealStockSubSegmentValue);
								if(existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0 >= idealStockSubSegmentValue)
								{
									System.out.println("cannot spend amt due to already more subSegment allocation hence, New amountToSpendInEachStock = "+amountToSpendInEachStock);
									amountToSpendInEachStock=amountToBuy/((existingWhiteList.size()-1)-i);
									continue;
								}
								else if((existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0)+amountToSpendInEachStock>idealStockSubSegmentValue)
								{
									//loop through whiteList and throw exception if no stock found with this 'stock.getSubSegment()'
									boolean flag=false;//no Stock exists in whitelist with subSegment=stock.getSibSegment()
									for(Stocks stock1:existingWhiteList)
									{
										System.out.println("stock1 type = "+stock1.getType()+" stock type ="+stock.getType());
										if(!stock1.getType().equals(stock.getType()) && stock1.getScCode()!=stock.getScCode())
										{
											flag=true;
											break;
										}
									}
									if(!flag)
										throw new FundexpertException("There should be atleast 3 stocks in each subSegment for each segment(sector) for proper allocation of Assets.");
									System.out.println("percentage to spend in totalPortfolio comes out to be = "+percentageToBuyInTotalPortfolio+" initially.");
								
									percentageToBuyInTotalPortfolio=((idealStockSubSegmentValue-(existingStockSubSegmentPercentage.doubleValue()*totalPortfolioAmountBeforeSuggestion/100.0))*100.0)/totalPortfolioAmountBeforeSuggestion;
									System.out.println("Now total percentageToBuyInTotalPortfolio = "+percentageToBuyInTotalPortfolio);
									
									amountToSpendInEachStock=(totalPortfolioAmountBeforeSuggestion*percentageToBuyInTotalPortfolio)/100.0;
									partialPercentageBought=(amountToSpendInEachStock/totalAssetAmount)*100.0;
									System.out.println("1.-1)New Partial Percentage to buy from whiteList= "+partialPercentageBought+" new amountToSpendInEachStock = "+amountToSpendInEachStock);
								}
							}
							
							System.out.println("Partial Percentage = "+partialPercentageBought);
							Action action=new Action();
							action.setAction(1);
							action.setAmount(amountToSpendInEachStock);
							action.setAssetId(stock.getScCode());
							action.setAssetType(Asset.STOCK_ASSET_TYPE);
							action.setPercentage(partialPercentageBought);
							actionList.add(action);
							updateUserPortfolioState(stock,null,amountToSpendInEachStock,list,totalPortfolioAmountBeforeSuggestion,totalAssetAmount);
							numberOfStocks--;
							if(distributionAllocationMap.containsKey(stockSegment))
							{
								System.out.println(" Old Percentage from Segment = "+(stockSegment)+" stock.getSegment = "+stock.getSegment());
								System.out.println("  Distribution = "+(distributionAllocationMap.get(stock.getSegment())+totalPercentageBought));
							}
							totalPercentageBought+=partialPercentageBought;
							totalAmountInvested+=amountToSpendInEachStock;
							if(distributionAllocationMap.containsKey(stockSegment))
								System.out.println(" New Percentage in Segement = "+(stockSegment)+"  Distribution  = "+(distributionAllocationMap.get(stock.getSegment())+totalPercentageBought));
							amountToBuy-=amountToSpendInEachStock;
						}
						System.out.println("Stock subSegmentPercentageAllocation Map = "+stocksSubSegmentPercentageAllocationMap);
						Double subSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(stock.getType());
						if(subSegmentPercentage==null)
						{
							//in stocksSubSegmentPercentageAllocationMap at this point it might not contain either of 'LC/MC/SC' because this allocation map gets updated after schemeLevelSuggestion which removes some stocks and mutualfunds hence might remove complete subSegment like 'SMALL CAL'
							subSegmentPercentage=percentageToBuyInTotalPortfolio;
						}
						else
						{
							subSegmentPercentage+=percentageToBuyInTotalPortfolio;
						}
						stocksSubSegmentPercentageAllocationMap.put(stock.getType(),subSegmentPercentage);
						System.out.println("Stock subSegmentPercentageAllocation Map after buying percent = "+percentageToBuyInTotalPortfolio+" from subSegment ="+stock.getType()+" subSegmentPercentage allocation  "+stocksSubSegmentPercentageAllocationMap);
					}
				}
				timesKthLoopCanBeRanAtMax--;
				//if amountToBuy is still left and can be invested then put k=k-1
				boolean isInvestable=findIfAmountCanStillBeInvested(amountToBuy,existingWhiteList,idealStockSubSegmentValueMap,stocksSubSegmentPercentageAllocationMap,totalPortfolioAmount);
				if(isInvestable)
					k--;
			}
			System.out.println("NumberOfStocks = "+numberOfStocks+" amountLeftToPurcahseStocks = "+amountToBuy+" no. of blackListStocks "+blackList.size());
			/*
			 * remember this point
			 * if control does not go in above 'for loop' then still we will be having to numberOfStocks>0 || amountToBuy>0 and so we dont have to calculate 'numberOfStocks' and 'amountToSpendInEachStock' again
			 * but if control goes in above for loop then 'numberOfStocks' gets modified inside if condition of (closePrice>amountToSpendInEachStock) considering the size of blacklist so again we dont need to calculate 'numberOfStocks' and 'amountToSpendInEachStock' again 
			 */
			
			//invest in blackList only when some amount is left after after buying stocks and that amount is not enough to buy any whole unit of stocks from whiteList
			if((numberOfStocks>0 || amountToBuy>0) && blackList.size()>0)
			{
				//numberOfStocks=getNumberOfAssetOnBasisOfThresholdPercent(amountToBuy, totalPortfolioAmountBeforeSuggestion, stockThresholdPercentage);
				amountToSpendInEachStock=amountToBuy/numberOfStocks;
				System.out.println("Amount to Spend in each Stock = "+amountToSpendInEachStock);
				//now we will loop through each of UsersPortfolioState BlackList which are not in recommended 'stocks under segment'
				
				for(int i=0;i<blackList.size();i++)
				{
					System.out.println("for blackList   I="+i);
					System.out.println("Amount to buy="+amountToBuy+" totalPercentageBiught="+totalPercentageBought+" remainingStocks to invest in="+numberOfStocks);
					if(numberOfStocks>0)
					{
						Stocks stock=blackList.get(i);
						System.out.println("ScCode="+stock.getScCode()+" closedPrice="+stock.getClosePrice()+" amountToSpendInEachStock="+amountToSpendInEachStock);
						if(stock.getClosePrice()>amountToSpendInEachStock)
						{
							System.out.println("Will not buy this stock bcoz of higher close price.");
							if((blackList.size()-(i+1))-numberOfStocks<0)
							{
								if((blackList.size()-1)==i)
								{
									//this means blackList is all done and now we need to break this loop
									//this condition is boundary condition because if 'i' is the last index of blackList then '(blackList.size()-1)-i' will give us 0 which will be less than 'numberOfStocks' in this case
									break;
								}
								System.out.println("NumberOfStocks > ((blackList.size()-1)-i)");
								int remainingStocksToSpendMoneyOn=(blackList.size()-(i+1));
								amountToSpendInEachStock=amountToBuy/remainingStocksToSpendMoneyOn;
								numberOfStocks=remainingStocksToSpendMoneyOn;
								continue;
							}
							else
							{
								System.out.println("NumberOfStocks < ((blackList.size()-1)-i)");
								continue;
							}
						}
						else
						{
							if(amountToSpendInEachStock/stock.getClosePrice()>=1 && amountToSpendInEachStock%stock.getClosePrice()>=0)
							{
								if(amountToSpendInEachStock%stock.getClosePrice()>0)
								{
									System.out.print("Need to find new amountToSpendInEachStock so that we can invest whole number units.New amountToSpendInEachStock = ");
									//we will not buy more than 'amountToSpendInEachAsset' money but we can buy less
									amountToSpendInEachStock=Math.round(amountToSpendInEachStock-(amountToSpendInEachStock%stock.getClosePrice()));
									System.out.println(amountToSpendInEachStock);
								}
								else
								{
									System.out.println("Stocks close price is multiple of amountToSpendInEachStock.");
								}
							}
							else
							{
								System.err.println("Cannot buy this Stock because its closePrice is higher than the amountToSpendInEachStock.ScCode = "+stock.getScCode());
								continue;
							}
							partialPercentageBought=(amountToSpendInEachStock/totalAssetAmount)*100.0;
							percentageToBuyInTotalPortfolio=(amountToSpendInEachStock/totalPortfolioAmount)*100.0;
							
							/*
							 * while buying stocks on the basis of segments we also have to take care of the fact that subSegment allocation does not get faulty so
							 * if we have to buy 500rs (which is say 2%) from stock A whose subSegment is LC and LC have 11% allocation in UserPortfolio and if ideal is 10% for LC allocation then we cannot buy stock A hence we have to try with other stock in same segment but different subSegment
							 * but if LC current allocation is 9% in user portfolio then we still would invest the difference of 1%(10%-9%)
							 */
							/*Double existingStockSubSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(stock.getType());
							if(existingStockSubSegmentPercentage!=null)
							{
								double idealStockSubSegmentAllocation=idealStockSubSegmentAllocationMap.get(stock.getType());
								System.out.println("Segment = "+stockSegment+" Existing stockSubSegment+"+stock.getType()+" has "+existingStockSubSegmentPercentage+"%, while  ideal allocation in this subgment is= "+idealStockSubSegmentAllocation);
								if(existingStockSubSegmentPercentage.doubleValue()>=idealStockSubSegmentAllocation)
								{
									System.out.println("cannot spend amt due to already more subSegment allocation hence, New amountToSpendInEachStock = "+amountToSpendInEachStock);
									existingStockSubSegmentPercentage=amountToBuy/((existingWhiteList.size()-1)-i);
									continue;
								}
								else if(existingStockSubSegmentPercentage.doubleValue()+percentageToBuyInTotalPortfolio>idealStockSubSegmentAllocation)
								{
									System.out.println("percentage to spend in totalportfolio comes out to be = "+percentageToBuyInTotalPortfolio);
									percentageToBuyInTotalPortfolio=idealStockSubSegmentAllocation-existingStockSubSegmentPercentage.doubleValue();
									amountToSpendInEachStock=(totalPortfolioAmountBeforeSuggestion*percentageToBuyInTotalPortfolio)/100.0;
									partialPercentageBought=(amountToSpendInEachStock/totalAssetAmount)*100.0;
									System.out.println("1.-1)New Partial Percentage to buy from whiteList= "+partialPercentageBought);
								}
							}*/
							
							Action action=new Action();
							action.setAction(1);
							action.setAmount(amountToSpendInEachStock);
							action.setAssetId(stock.getScCode());
							action.setAssetType(Asset.STOCK_ASSET_TYPE);
							action.setPercentage(partialPercentageBought);
							actionList.add(action);
							updateUserPortfolioState(stock,null,amountToSpendInEachStock,list,totalPortfolioAmountBeforeSuggestion,totalAssetAmount);
							numberOfStocks--;
							if(distributionAllocationMap.containsKey(stockSegment))
							{
								System.out.println(" Old Percentage from Segment = "+(stockSegment)+" stock.getSegment = "+stock.getSegment());
								System.out.println("  Distribution = "+(distributionAllocationMap.get(stock.getSegment())+totalPercentageBought));
							}
							totalPercentageBought+=partialPercentageBought;
							totalAmountInvested+=amountToSpendInEachStock;
							System.out.println(" New Percentage in Segement = "+(stockSegment)+"  Distribution  = "+(distributionAllocationMap.get(stock.getSegment())+totalPercentageBought));
							amountToBuy-=amountToSpendInEachStock;
						}
						
						Double subSegmentPercentage=stocksSubSegmentPercentageAllocationMap.get(stock.getType());
						if(subSegmentPercentage==null)
						{
							//in stocksSubSegmentPercentageAllocationMap at this point it might not contain either of 'LC/MC/SC' because this allocation map gets updated after schemeLevelSuggestion which removes some stocks and mutualfunds hence might remove complete subSegment like 'SMALL CAP'
							subSegmentPercentage=percentageToBuyInTotalPortfolio;
						}
						else
						{
							subSegmentPercentage+=percentageToBuyInTotalPortfolio;
							stocksSubSegmentPercentageAllocationMap.put(stock.getType(),subSegmentPercentage);
						}
					}
					
				}
			}
			
			System.out.println("After buying  stocks new StocksSubSegment percentage alocation = "+stocksSubSegmentPercentageAllocationMap);
			Double sectorDistributionPercentage=distributionAllocationMap.get(stockSegment);
			if(sectorDistributionPercentage!=null)
			{
				sectorDistributionPercentage+=totalPercentageBought;
				distributionAllocationMap.put(stockSegment,sectorDistributionPercentage);
			}
			else
				throw new UnConditionalException("Sector Distribution Map does not contain stock segment into which we have to invest our money.");
			
			//after buying stocks new Segment Percentage Allocation
			//IntialPercentageSector Allocation
			System.out.println("StockSegemnt allocation map="+distributionAllocationMap);
			
			System.out.println("No of Stocks left -"+numberOfStocks+" totalCashLeft="+amountToBuy);
			if(numberOfStocks>0 || amountToBuy>0)
			{
				//it is possible that either numberOfStocks==0 or amountToBuy is still left because of wholeNumber Units while buying stocks
			}
			return amountToBuy;
		}
		catch(UnConditionalException uce)
		{
			uce.printStackTrace();
			throw uce;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Error occurred while buying stocks.");
		}
	}
	
	public Double doMFSubSegmentDistribution(double totalPortfolioAmountBeforeSuggestion,double totalMFAmount,double totalCashLeftFromStocks,SchemeLevelSuggestion suggestion)
	{
		Map<String,Double> afterSubSegmentDistributionPercentageMap=null;//ew HashMap<String,Double>();
		Map<String,Double> afterSubSegmentDistributionValueMap=null;
		Map<String,List<UserPortfolioState>> mfSubSegmentWiseUserPortfolioStateMap=suggestion.getSubSegmentWiseUserPortfolioStateMFMap();
		System.out.println("Total cash left from stocks ="+totalCashLeftFromStocks+" totalCashLeftInMF after suggestion  ="+suggestion.getTotalCashAfterSuggestionForMF());
		double totalCashAfterRedemptionOfMF=suggestion.getTotalCashAfterSuggestionForMF()+totalCashLeftFromStocks;
		double totalCashAfterBuyingMF=0;
		try
		{
			System.out.println("Before doing MF subSegment ditribution .");
			System.out.println("Individual MF subsegment distribution = "+mfSubSegmentPercentageAllocationMap);
			System.out.println("Individual STOCK subsegment distribution = "+stocksSubSegmentPercentageAllocationMap);
			//since edelweiss didn't told us the perentage allocation of large cap in equity and MF individually we will consider large cap at portfolio level 
			afterSubSegmentDistributionPercentageMap = addValuesForSameKey(stocksSubSegmentPercentageAllocationMap,mfSubSegmentPercentageAllocationMap);
			afterSubSegmentDistributionValueMap = addValuesForSameKey(stocksSubSegmentValueMap,mfSubSegmentValueMap);
			
			System.out.println("AfterSubSegmentDistributionMap="+afterSubSegmentDistributionPercentageMap);
			
			/*if(mfSubSegmentPercentageAllocationMap==null || mfSubSegmentPercentageAllocationMap.size()==0)
				throw new UnConditionalException("User's portfolio Does not contains any subSegment in MF.");*/
			if(stocksSubSegmentPercentageAllocationMap==null || stocksSubSegmentPercentageAllocationMap.size()==0)
				System.out.println("No SubSegment in Stocks exists.");
			
			
			
			System.out.println("Distribution Map = "+afterSubSegmentDistributionPercentageMap);

			Map<String,Double> idealEquitySubSegmentAllocationMap=idealSubSegmentAllocationMap.containsKey("EQUITY")?idealSubSegmentAllocationMap.get("EQUITY"):null;
			Map<String,Double> idealDebtSubSegmentAllocationMap=idealSubSegmentAllocationMap.containsKey("DEBT")?idealSubSegmentAllocationMap.get("DEBT"):null;
			Map<String,Double> idealMFMap=idealEquitySubSegmentAllocationMap;
			idealMFMap.putAll(idealDebtSubSegmentAllocationMap);
			for(Map.Entry<String,Double> map:afterSubSegmentDistributionPercentageMap.entrySet())
			{
				String subSegment=map.getKey();
				System.out.println("In doMFSubSegmentDistribution for subSegment = "+subSegment);
				//here subSegmentAllocation is the allocation under one subSegment in MF and STOCK so subSegmentAllocation = 30(MF-Large Cap(20) + Stock-Large Cap(10))
				Double subSegmentAllocation=map.getValue();
				//one thing to remember is that we will not redeem or buy from/in stocks since we have already played with stocks while sector allocation distribution and we try with that then sector allocation will get affected
				if(mfSubSegmentPercentageAllocationMap.containsKey(subSegment))
				{
					if(idealMFMap.containsKey(subSegment))
					{
						Double idealSubSegmentAllocation=idealMFMap.get(subSegment);
						if(subSegmentAllocation > idealSubSegmentAllocation)
						{
							//get money out from this subSegment equal to differencePercentage
							//extra point to be noted is we can only take money out which is upto mfPercentage
							Double mfSubSegmentAllocation=mfSubSegmentPercentageAllocationMap.get(subSegment);
							double differencePercentage = subSegmentAllocation - idealSubSegmentAllocation;
							//amountToRedeem is dependent on differencePercentage and we don't want to take money out from stocks,hence differencePercentage can be maximum of mfPercentage in that subSegment
							if(differencePercentage > mfSubSegmentAllocation)
								differencePercentage = mfSubSegmentAllocation;
							double currentValueInThisSubSegment = subSegmentAllocation * totalPortfolioAmountBeforeSuggestion / 100.0;
							double amountToRedeem = totalPortfolioAmountBeforeSuggestion * differencePercentage / 100.0;
							
							//below list gives us UserPortfolioState depending on each subSegment
							List<UserPortfolioState> mfWiseUserPortfolioStateList = mfSubSegmentWiseUserPortfolioStateMap.get(subSegment);
							
							mfWiseUserPortfolioStateList.forEach(ups->System.out.println("SubSegment ="+subSegment+" MFid ="+ups.getAssetId()+" amount to redeem = "+amountToRedeem));
							//Now we need to decide from which MFs and how much percentage from each to redeem this value
							double amountNotAbleToRedeem = getMutualFundSubSegmentMoneyOutFromUserPortfolio(totalPortfolioAmountBeforeSuggestion,totalMFAmount,amountToRedeem,mfWiseUserPortfolioStateList,afterSubSegmentDistributionPercentageMap,mfRecoList,subSegment,suggestion);
							totalCashAfterRedemptionOfMF += amountToRedeem-amountNotAbleToRedeem;
							//we passed afterSubSegmentDistributionPercentageMap in getMutuaLFundMoneyOut so wwe also need to update mfSubSegmentPercentageAllocationMap
							double percentageDifference=subSegmentAllocation-afterSubSegmentDistributionPercentageMap.get(subSegment);
							//reason we are updating only mfSubSegmentPercentage and not stockSubSegmentpercentageMap is bcoz we don't want to change sector allocation which will gets changed if we do subSegmentAllocation on Stocks
							mfSubSegmentPercentageAllocationMap.put(subSegment, mfSubSegmentPercentageAllocationMap.get(subSegment)-percentageDifference);
						}
						else if(subSegmentAllocation < idealSubSegmentAllocation)
						{
							//we will not but MF here bcoz for buying we need money which we will get only after redemption from all MFs
						}
					}
					else
					{
						throw new UnConditionalException("Ideal Mf Allocation map does not contain allocation for subSegment :"+subSegment);
					}
				}
				
			}
			//since we have manipulated 'afterSubSegmentDistributionPercentageMap' in 'getMutualFundSubSegmentMoneyOutFromUserPortfolio' function we need to update its corresponding value map
			updateValueMapAsPerPercentageMap(totalPortfolioAmountBeforeSuggestion,afterSubSegmentDistributionValueMap,afterSubSegmentDistributionPercentageMap);
			
			totalCashAfterBuyingMF=totalCashAfterRedemptionOfMF;
			
			System.out.println("total cash after Selling MF  = "+totalCashAfterBuyingMF);
			
			//add those subSegments in 'afterSubSegmentDistributionPercentageMap' which are not in UserPortfolio but are in idealSubSegment
			addStockSegmentsWhichAreNotInUsersStockSegments(idealMFMap,afterSubSegmentDistributionPercentageMap);
			System.out.println("SubSegmnents to be added from ideal allocation map ="+afterSubSegmentDistributionPercentageMap);
			
			//loop again for buying MF
			for(Map.Entry<String,Double> map:afterSubSegmentDistributionPercentageMap.entrySet())
			{	
				String subSegment=map.getKey();
				
				//it might be possible that initially mfSubSegmentPercentageAllocationMap does not contained this subSegment so adding it as 0% allocation
				if(!mfSubSegmentPercentageAllocationMap.containsKey(subSegment))
					mfSubSegmentPercentageAllocationMap.put(subSegment, 0.0);
				
				//here subSegmentAllocation is the allocation under one subSegment in MF and STOCK both so subSegmentAllocation = 30(MF-Large Cap(20) + Stock-Large Cap(10))
				Double subSegmentAllocation=map.getValue();
				System.out.println("Buying MF in subSegment = "+subSegment+" Current allocation = "+subSegmentAllocation);
				//one thing to remember is that we will not buy from/in stocks since we have already played with stocks while sector allocation distribution and if we try with that then sector allocation will get affected
				if(idealMFMap.containsKey(subSegment))
				{
					Double idealSubSegmentAllocation=idealMFMap.get(subSegment);
					if(idealSubSegmentAllocation > subSegmentAllocation)
					{
						//invest more money in this subSegment, only through MutualFund
						double differencePercentage=idealSubSegmentAllocation - subSegmentAllocation;
						double currentValueInThisSubSegment = subSegmentAllocation * totalPortfolioAmountBeforeSuggestion / 100.0;
						double amountToSpend = totalPortfolioAmountBeforeSuggestion * differencePercentage / 100.0;
						if(amountToSpend>totalCashAfterBuyingMF)
							amountToSpend=totalCashAfterBuyingMF;
						//below list gives us UserPortfolioState depending on each subSegment
						List<UserPortfolioState> mfWiseUserPortfolioStateList=mfSubSegmentWiseUserPortfolioStateMap.get(subSegment);
						if(mfWiseUserPortfolioStateList==null)
						{
							mfWiseUserPortfolioStateList=new ArrayList<UserPortfolioState>();
							mfSubSegmentWiseUserPortfolioStateMap.put(subSegment, mfWiseUserPortfolioStateList);
							System.out.println("No MF exists in UserPortfolio for SubSegment = "+subSegment);
						}
						else
							mfWiseUserPortfolioStateList.forEach(i->System.out.println("UPS id ="+i.getId()+" MF ID ="+i.getAssetId()+" portfolioLevelAllocation = "+i.getPortfolioLevelAllocation()));
						//Now we need to decide into how many and how much percentage into each fund we need to invest
						double amountNotAbleToSpendAnywhere = putMutualFundSubSegmentMoneyIntoUserPortfolio(totalPortfolioAmountBeforeSuggestion, totalMFAmount, amountToSpend, mfWiseUserPortfolioStateList, afterSubSegmentDistributionPercentageMap, mfRecoList, subSegment, suggestion);
						totalCashAfterBuyingMF -= (amountToSpend-amountNotAbleToSpendAnywhere);
						
						//we passed afterSubSegmentDistributionPercentageMap in putMutuaLFundSubSegmentMoneyInto so we also need to update mfSubSegmentPercentageAllocationMap
						double percentageDifference=afterSubSegmentDistributionPercentageMap.get(subSegment)-subSegmentAllocation;
						mfSubSegmentPercentageAllocationMap.put(subSegment, mfSubSegmentPercentageAllocationMap.get(subSegment)+percentageDifference);
					}
				}
				else
				{
					throw new UnConditionalException("2)Ideal Mf Allocation map does not contain allocation for subSegment :"+subSegment);
				}
			}
			System.out.println("Total cash after buying MF = "+totalCashAfterBuyingMF);
			//since we have manipulated 'afterSubSegmentDistributionPercentageMap' in 'getMutualFundSubSegmentMoneyOutFromUserPortfolio' function we need to update its corresponding value map
			updateValueMapAsPerPercentageMap(totalPortfolioAmountBeforeSuggestion,afterSubSegmentDistributionValueMap,afterSubSegmentDistributionPercentageMap);
			
			//final cash that we are left with after redeemption and buying = totalCashAfterBuyingMF
			return totalCashAfterBuyingMF;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	//totalAssetAmount is Amount in only MF in User's portfolio since this is MF function
	public Double getMutualFundSubSegmentMoneyOutFromUserPortfolio(double totalPortfolioAmount,double totalAssetAmount,double amountToRedeem,List<UserPortfolioState> list,Map<String,Double> distributionAllocationMap,List<Long> mfRecoList,String subSegment,SchemeLevelSuggestion suggestion) throws FundexpertException
	{
		//below list contains those funds which are suggested by edelweiss under 'subSegment' -> passed argument
		List<MutualFund> mfInThisSubSegment=subSegmentWiseMFMap.get(subSegment);
		List<Action> actionList=new ArrayList<Action>();
		Map<Long,MutualFund> usersMFMap=sls.getUsersMutualFundMap();
		try
		{
			if(mfInThisSubSegment==null || mfInThisSubSegment.size()==0)
				throw new FundexpertException("No MF exists for ideal allocation for subSegment : "+subSegment+" in table AssetsInSubSegment.");
			List<UserPortfolioState> blackList=new ArrayList<UserPortfolioState>(list.size()/2);
			List<UserPortfolioState> whiteList=new ArrayList<UserPortfolioState>(list.size()/2);
			System.out.println("Making WhiteList and BlackList.");
			if(list!=null && list.size()!=0)
			{
				for(int i=0;i<list.size();i++)
				{
					UserPortfolioState ups=(UserPortfolioState)list.get(i);
					long mfId=(ups.getAssetId());
					MutualFund matchedMF=null;
					System.out.println("mfId="+mfId);
					if(mfInThisSubSegment!=null)
					{
						for(MutualFund mf:mfInThisSubSegment)
						{
							if(mfId==mf.getId())
							{
								matchedMF=mf;
								break;
							}
						}
					}
					if(matchedMF!=null)
					{
						System.out.println("Added to whiteList.");
						whiteList.add(ups);
					}
					else 
					{
						System.out.println("Added to blackList.");
						blackList.add(ups);
					}
				}
			}
			
			//order blackList on the basis of smallest currest asset value so that we can sellof those which are small
			blackList.sort(new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					UserPortfolioState ups1=(UserPortfolioState)o1;
					UserPortfolioState ups2=(UserPortfolioState)o2;
					
					if(ups1.getCurrentAssetValue()>ups2.getCurrentAssetValue())
						return 1;
					else
						return -1;
				}
			});
			
			if(!distributionAllocationMap.containsKey(subSegment))
				throw new FundexpertException("distributionAllocationMap does not conain MF SubSegment present in User's Portfolio while redeeming MF.");
			
			System.out.println("WhiteList.size()="+whiteList.size()+" blacklist.size()="+blackList.size());
			double percentageToRedeemFromTotalPortfolio=0;
			double totalPercentageToRedeem=0,amountToRedeemFromEachAsset=0;
			long mfId=0;
			if(blackList.size()!=0)
			{
				mfId=0;
				amountToRedeemFromEachAsset=amountToRedeem/blackList.size();
				System.out.println("Amount to redeem from each MutualFund in blackList="+amountToRedeemFromEachAsset);
				for(int i=0;i<blackList.size();i++)
				{
					if(amountToRedeem>0)
					{
						UserPortfolioState ups=blackList.get(i);
						mfId=ups.getAssetId();
						MutualFund mutualFund=usersMFMap.get(mfId);
						double currentAssetValue=ups.getCurrentAssetValue();
						System.out.println("MF id = "+mfId+" currentAsetValue = "+currentAssetValue+" amountToRedeemFromEachAsset = "+amountToRedeemFromEachAsset+" totalPortfolioAmount "+totalPortfolioAmount);
						if(currentAssetValue!=0)
						{
							if(currentAssetValue < amountToRedeemFromEachAsset)
							{
								percentageToRedeemFromTotalPortfolio=(currentAssetValue/totalPortfolioAmount)*100.0;
								System.out.println("1)Percentage to Redeem from totalPortfolio in MF in case of subSegment ="+percentageToRedeemFromTotalPortfolio);
								Action action=new Action();
								action.setAction(2);
								action.setAssetType(ups.getAssetType());
								action.setAssetId(ups.getAssetId());
								action.setAmount(currentAssetValue);
								actionList.add(action);
								amountToRedeem-=currentAssetValue;
								
								updateUserPortfolioState(null, mutualFund, currentAssetValue, ups, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
								
								double newPercentage=distributionAllocationMap.get(ups.getSubSegment())-percentageToRedeemFromTotalPortfolio;
								System.out.println(" Old Percentage from SubSegment("+subSegment+") Distribution = "+(distributionAllocationMap.get(ups.getSubSegment())-totalPercentageToRedeem));
								totalPercentageToRedeem+=percentageToRedeemFromTotalPortfolio;
								System.out.println(" New Percentage in Segement("+subSegment+") Distribution  = "+(distributionAllocationMap.get(ups.getSubSegment())-totalPercentageToRedeem));
								
								//now difference which we were not able to redeem from above MF will be shifted to upcoming mutualfunds.
								if(amountToRedeem>0 && i!=blackList.size()-1)
								{
									//one thing to understand is that we are redeeming max value from current mf i.e currentAssetValue , so the part which we were not able to redeem still stays in amountToRedeem hence we will not calculate the difference amount explicitly
									amountToRedeemFromEachAsset=amountToRedeem/((blackList.size()-1-i));
									System.out.println("New Amount to redeem from each asset = "+amountToRedeemFromEachAsset);
								}
							}
							else
							{
								percentageToRedeemFromTotalPortfolio=(amountToRedeemFromEachAsset/totalPortfolioAmount)*100.0;
								System.out.println("2)Percentage to Redeem from totalPortfolio in MF in case of subSegment ="+percentageToRedeemFromTotalPortfolio);
								Action action=new Action();
								action.setAction(2);
								action.setAssetType(ups.getAssetType());
								action.setAssetId(ups.getAssetId());
								action.setAmount(amountToRedeemFromEachAsset);
								actionList.add(action);
								amountToRedeem-=amountToRedeemFromEachAsset;
								
								updateUserPortfolioState(null, mutualFund, amountToRedeemFromEachAsset, ups, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
								
								double newPercentage=distributionAllocationMap.get(ups.getSubSegment())-percentageToRedeemFromTotalPortfolio;
								System.out.println(" Old Percentage from SubSegment("+subSegment+") Distribution = "+(distributionAllocationMap.get(ups.getSubSegment())-totalPercentageToRedeem));
								totalPercentageToRedeem+=percentageToRedeemFromTotalPortfolio;
								System.out.println(" New Percentage in Segement("+subSegment+") Distribution  = "+(distributionAllocationMap.get(ups.getSubSegment())-totalPercentageToRedeem));
							}
						}
						else
						{
							System.out.println("CurrentAsset value is 0.And this case should not arrive bcoz all 0 balance holdings will get eliminated in schemeLevelSuggestion at the time of size level removal.");
						}
					}
					else
						break;
				}
			}
			System.out.println("TtotalPercentage redeemed from blackList = "+totalPercentageToRedeem);
			System.out.println("Amount not able to redeem  = "+amountToRedeem);
			//update the subSegment percentage for passed argument 'subSegment'
			//we'll be using same patch after looping though whiteList funds so we can comment below one - used at the time of debugging
			//comment below code on live.Why?Used only for debugging.
			/*{
				Double subSegmentDistributionPercentage=distributionAllocationMap.get(subSegment);
				if(subSegmentDistributionPercentage!=null)
				{
					subSegmentDistributionPercentage-=totalPercentageToRedeem;
					distributionAllocationMap.put(subSegment,subSegmentDistributionPercentage);
				}
			}*/
			System.out.println("Distribution Allocation Map after first loop of selling = "+distributionAllocationMap);
			
			System.out.println("Now will start redeeming from whitelist funds.Current SubSegment allocation = "+distributionAllocationMap.toString());
			if(whiteList.size()!=0)
				amountToRedeemFromEachAsset=amountToRedeem/whiteList.size();
			else
				amountToRedeemFromEachAsset=amountToRedeem;
			if(amountToRedeem>0)
			{
				//order whiteList on the basis of highest NAV
				whiteList.sort(new Comparator() {
					@Override
					public int compare(Object o1, Object o2) {
						UserPortfolioState ups1=(UserPortfolioState)o1;
						UserPortfolioState ups2=(UserPortfolioState)o2;
						if(ups1.getCurrentAssetValue()>ups2.getCurrentAssetValue())
							return 1;
						else
							return -1;
					}
				});
			}
			System.out.println("Amount to redeem from each Asset = "+amountToRedeemFromEachAsset);
			for(int i=0;i<whiteList.size();i++)
			{
				if(amountToRedeem>0)
				{
					UserPortfolioState ups=whiteList.get(i);
					mfId=ups.getAssetId();
					MutualFund mutualFund = usersMFMap.get(mfId);
					double currentAssetValue=ups.getCurrentAssetValue();
					System.out.println("MF id = "+mfId+" currentAsetValue = "+currentAssetValue+" amountToRedeemFromEachAsset = "+amountToRedeemFromEachAsset+" totalPortfolioAmount "+totalPortfolioAmount);
					if(currentAssetValue < amountToRedeemFromEachAsset)
					{
						percentageToRedeemFromTotalPortfolio=(currentAssetValue/totalPortfolioAmount)*100.0;
						System.out.println("1)Percentage to Redeem from totalPortfolio in MF in case of subSegment ="+percentageToRedeemFromTotalPortfolio);
						Action action=new Action();
						action.setAction(2);
						action.setAssetType(ups.getAssetType());
						action.setAssetId(ups.getAssetId());
						action.setAmount(currentAssetValue);
						actionList.add(action);
						amountToRedeem-=currentAssetValue;
						updateUserPortfolioState(null, mutualFund, currentAssetValue, ups, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
						double newPercentage=distributionAllocationMap.get(ups.getSubSegment())-percentageToRedeemFromTotalPortfolio;
						System.out.println(" Old Percentage from SubSegment("+subSegment+") Distribution = "+(distributionAllocationMap.get(ups.getSubSegment())-totalPercentageToRedeem));
						totalPercentageToRedeem+=percentageToRedeemFromTotalPortfolio;
						System.out.println(" New Percentage in Segement("+subSegment+") Distribution  = "+(distributionAllocationMap.get(ups.getSubSegment())-totalPercentageToRedeem));
						
						//now the difference which we were not able to redeem from above MF will be shifted to upcoming mutualfunds in blackList and in whiteList.
						if(amountToRedeem>0 && whiteList.size()-1-i!=0)
						{
							//one thing to understand is that we are redeeming max value from current mf i.e currentAssetValue , so the part which we were not able to redeem still stays in amountToRedeem hence we will not calculate the difference amount explicitly
							amountToRedeemFromEachAsset=amountToRedeem/(whiteList.size()-1-i);
						}
					}
					else
					{
						percentageToRedeemFromTotalPortfolio=(amountToRedeemFromEachAsset/totalPortfolioAmount)*100.0;
						System.out.println("2)Percentage to Redeem from totalPortfolio in MF in case of subSegment ="+percentageToRedeemFromTotalPortfolio);
						Action action=new Action();
						action.setAction(2);
						action.setAssetType(ups.getAssetType());
						action.setAssetId(ups.getAssetId());
						action.setAmount(amountToRedeemFromEachAsset);
						actionList.add(action);
						amountToRedeem-=amountToRedeemFromEachAsset;
						
						updateUserPortfolioState(null, mutualFund, amountToRedeemFromEachAsset, ups, totalPortfolioAmountBeforeSuggestion, totalAssetAmount);
						
						double newPercentage=distributionAllocationMap.get(ups.getSubSegment())-percentageToRedeemFromTotalPortfolio;
						System.out.println(" Old Percentage from SubSegment("+subSegment+") Distribution = "+(distributionAllocationMap.get(ups.getSubSegment())-totalPercentageToRedeem));
						totalPercentageToRedeem+=percentageToRedeemFromTotalPortfolio;
						System.out.println(" New Percentage in Segement("+subSegment+") Distribution  = "+(distributionAllocationMap.get(ups.getSubSegment())-totalPercentageToRedeem));
					}
				}
			}
			//update the subSegment percentage for passed argument 'subSegment'
			Double subSegmentDistributionPercentage1=distributionAllocationMap.get(subSegment);
			if(subSegmentDistributionPercentage1!=null)
			{
				subSegmentDistributionPercentage1-=totalPercentageToRedeem;
				distributionAllocationMap.put(subSegment,subSegmentDistributionPercentage1);
			}
			
			if(amountToRedeem>0)
				System.out.println("Cannot redeem all from MF's for subSegment = "+subSegment+".Remaining to redeem = "+amountToRedeem);
			return amountToRedeem;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	//distributionAllocationMap contains LC,MC,SC,LT,ST with their corresponding % allocation in Portfolio for both MF and stocks.
	public Double putMutualFundSubSegmentMoneyIntoUserPortfolio(double totalPortfolioAmount,double totalAssetAmount,double amountToBuy,List<UserPortfolioState> list,Map<String,Double> distributionAllocationMap,List<Long> mfRecoList,String subSegment,SchemeLevelSuggestion suggestion)
	{
		List<Action> actionList=new ArrayList<Action>();
		Map<Long,MutualFund> usersMutualFundMap=sls.getUsersMutualFundMap();
		double totalAmountInvested=0;
		try
		{
			//existingwhitelist contains those MF which are in list<UserPortfolioState> as well as in perSubSegmentMutualFunds(idealMfInSubSegment ).
			List<MutualFund> existingWhiteList=new ArrayList<MutualFund>(10);
			//blacklist is list of MF of those which are in userPortfolioState but not in perSubSegmentMutualFunds 
			List<MutualFund> blackList=new ArrayList<MutualFund>(10);
			
			List<MutualFund> mfInThisSubSegment=subSegmentWiseMFMap.get(subSegment);
			
			//mfInThisSubSegment.forEach(i->System.out.println("MF list which are in Ideal MutualFunds in subSegment = "+subSegment+" mfId="+i.getId()));
			List<MutualFund> newWhiteList=new ArrayList<MutualFund>();
			newWhiteList.addAll(mfInThisSubSegment);
			if(newWhiteList.size()==0)
				throw new FundexpertException("No MF list exists for ideal allocation for subSegment : "+subSegment+" in table AssetsInSubSegment.");
			if(list!=null && list.size()>0)
			{
				for(int i=0;i<list.size();i++)
				{
					long mfId=((UserPortfolioState)list.get(i)).getAssetId();
					MutualFund matchedMF=null;
					for(MutualFund mf:mfInThisSubSegment)
					{
						if(mf.getId()==mfId)
						{
							matchedMF=mf;
							break;
						}
					}
					if(matchedMF!=null)
					{
						existingWhiteList.add(matchedMF);
					}
					else
					{
						MutualFund mf=usersMutualFundMap.get(mfId);
						if(mf==null)
							throw new Exception("This is not possible since usersMutualFundMap contains MF in users Portfolio and mfId comes from the list which is passed as argument and is also the same list of UserPortfolioState.");
						blackList.add(mf);
					}
				}
			}
			
			//now remove those MF from newWhiteList which are in usersPortfolioState so that we have only those MF which are in idealSubSegments
			//below lines helps in removing duplication of those MutualFunds which are in both userPortfolioState and idealSubSegment 
			newWhiteList.removeAll(existingWhiteList);
			//Append newWhiteList in existingWhitelist, so that existingWhiteList contains only those MF which are in userPortfolioState and idealSubSegment as well as those which are in idealSubSegment only
			//now we will deal only with existingWhiteList and blackList
			existingWhiteList.addAll(newWhiteList);
			
			//order existingWhiteList on the basis of highest closePrice
			existingWhiteList.sort(new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					MutualFund m1=(MutualFund)o1;
					MutualFund m2=(MutualFund)o2;
					if(m1.getNav()<m2.getNav())
						return 1;
					else
						return -1;
				}
			});
			
			existingWhiteList.forEach(i->System.out.println("Existing white list funds which are common in both UsersPortfolio & Ideal MutualFunds or are in Only ideal MutualFunds in subSegment = "+subSegment+" mfId="+i.getId()));
			blackList.forEach(i->System.out.println("BlackList funds which are in UsersPortfolio but are not in Ideal MutualFunds in subSegment = "+subSegment+" mfId="+i.getId()));
			
			//existingWhiteList=mf common in userPortfolioSate and idealSubSegment+mf in idealSubsegment
			//blacklist=mf in userportfolio but not in idealSubSegment
			
			//Now we need to decide in how many MF we need to invest 'amountToBuy' by using 'threshholdPercentage' -see description of threshholdPercentage at the time of declaration
			Integer numberOfMFs=getNumberOfAssetOnBasisOfThresholdPercent(amountToBuy, totalPortfolioAmount, mfThresholdPercentage);
			if(numberOfMFs==null)
				throw new FundexpertException("Not able to identify No. of MutualFunds to invest in.");
			System.out.println("1)NumberOf MFs to divide spending money="+numberOfMFs);
			//if numberOfMFs is greater than size(existingWhiteList+blackList) then put numberOfMFs = sumOf(existingWhiteList.size+blackList.size)
			if(numberOfMFs>=(existingWhiteList.size()+blackList.size()))
				numberOfMFs=(existingWhiteList.size()+blackList.size());
			
			System.out.println("2)NumberOf MFs to divide spending money="+numberOfMFs+" totalPortfolioAmount = "+totalPortfolioAmount);
			double amountToSpendInEachMF=amountToBuy/numberOfMFs,totalPercentageBought=0,percentageToBuyInTotalPortfolio=0;
			System.out.println("Amount to spend in each MF = "+amountToSpendInEachMF);
			
			//now loop through each MF and spend money into it
			for(int i=0;i<existingWhiteList.size();i++)
			{
				System.out.println("Amount to buy="+amountToBuy+" totalPercentageBiught="+totalPercentageBought+" remaining MutualFunds to invest in="+numberOfMFs);
				if(numberOfMFs>0)
				{
					MutualFund mf=existingWhiteList.get(i);
					System.out.println("MFid="+mf.getId()+" currentPrice="+mf.getNav()+" amountToSpendInEachMF="+amountToSpendInEachMF+" subSubSegment = "+mf.getBroaderType());
					if(mf.getNav()>amountToSpendInEachMF)
					{
						System.out.println("Will not buy this MF bcoz of higher nav value.");
						if(((existingWhiteList.size()-1)-i)-numberOfMFs<0)
						{
							System.out.println("NumberOfMFs > ((existingWhiteList.size()-1)-i)");
							int remainingMfToSpendMoneyOn=((existingWhiteList.size()-1)-i);
							//if(blackList.size()==0 && (existingWhiteList.size()-1)==i)
							if((existingWhiteList.size()-1)==i)
							{
								//this means existingWhiteList is all done and we don't even have any MF in 'blackList' and now we need to break this loop
								//this condition is boundary condition because if 'i' is the last index of existingWhiteList then '(existingWhiteList.size()-1)-i' will give us 0 which will be less than 'numberOfStocks' in this case
								break;
							}
							//else if(blackList.size()>0 && (((existingWhiteList.size()-1)-i)+blackList.size())>=numberOfMFs)
							else if(((existingWhiteList.size()-1)-i)>=numberOfMFs)
							{
								remainingMfToSpendMoneyOn=numberOfMFs;
								System.out.println("1)remainingMfToSpendMoneyOn="+remainingMfToSpendMoneyOn);
							}
							else
							{
								remainingMfToSpendMoneyOn=((existingWhiteList.size()-1)-i);
								System.out.println("2)remainingMfToSpendMoneyOn="+remainingMfToSpendMoneyOn);
							}
							amountToSpendInEachMF=amountToBuy/remainingMfToSpendMoneyOn;
							numberOfMFs=remainingMfToSpendMoneyOn;
							continue;
						}
						else
						{
							System.out.println("NumberOfMFs < ((existingWhiteList.size()-1)-i)");
							continue;
						}
					}
					else 
					{
						percentageToBuyInTotalPortfolio=(amountToSpendInEachMF/totalPortfolioAmount)*100.0;
						Action action=new Action();
						action.setAction(1);
						action.setAmount(amountToSpendInEachMF);
						action.setAssetId(mf.getId());
						action.setAssetType(Asset.MUTUALFUND_ASSET_TYPE);
						action.setPercentage(percentageToBuyInTotalPortfolio);
						actionList.add(action);
						updateUserPortfolioState(null,mf,amountToSpendInEachMF,list,totalPortfolioAmountBeforeSuggestion,totalAssetAmount);
						numberOfMFs--;
						System.out.println("While Buying MF, Old Percentage from SubSegment = "+subSegment+" Distribution = "+(distributionAllocationMap.get(subSegment)+totalPercentageBought));
						totalPercentageBought+=percentageToBuyInTotalPortfolio;
						totalAmountInvested+=amountToSpendInEachMF;
						System.out.println("While Buying MF, New Percentage in SubSegement = "+subSegment+"  Distribution  = "+(distributionAllocationMap.get(subSegment)+totalPercentageBought));
						amountToBuy-=amountToSpendInEachMF;
					}
				}
				
				Double subSegmentPercentage=distributionAllocationMap.get(subSegment);
				if(subSegmentPercentage==null)
				{
					//in distributionAllocationMap at this point it might not contain either of 'LC/MC/SC' because this allocation map gets updated after schemeLevelSuggestion which removes some stocks and mutualfunds hence might remove complete subSegment like 'SMALL CAP'
					subSegmentPercentage=percentageToBuyInTotalPortfolio;
				}
				else
				{
					subSegmentPercentage+=percentageToBuyInTotalPortfolio;
					distributionAllocationMap.put(subSegment,subSegmentPercentage);
				}
			}
			System.out.println("NumberOfMFs = "+numberOfMFs+" amountLeftToPurcahseMF = "+amountToBuy+" no. of blackListMF "+blackList.size());
			/*
			 * remember this point
			 * if control does not go in above 'for loop' then still we will be having numberOfMFs>0 || amountToBuy>0 and so we dont have to calculate 'numberOfMFs' and 'amountToSpendInEachMF' again
			 * but if control goes in above for loop then 'numberOfMFs' gets modified inside if condition of (closePrice>amountToSpendInEachMF) considering the size of blacklist so again we dont need to calculate 'numberOfMFs' and 'amountToSpendInEachMF' again 
			 */
			
			/*if((numberOfMFs>0 || amountToBuy>0) && blackList.size()>0)
			{
				//numberOfStocks=getNumberOfAssetOnBasisOfThresholdPercent(amountToBuy, totalPortfolioAmountBeforeSuggestion, stockThresholdPercentage);
				amountToSpendInEachMF=amountToBuy/numberOfMFs;
				System.out.println("Amount to Spend in each MutualFund = "+amountToSpendInEachMF);
				//now we will loop through each of UsersPortfolioState BlackList which are not in recommended 'MFs under subSegment' but are in UserPortfolioState
		
				for(int i=0;i<blackList.size();i++)
				{
					System.out.println("for blackList   I="+i);
					System.out.println("Amount to buy="+amountToBuy+" totalPercentageBiught="+totalPercentageBought+" remainingMFs to invest in="+numberOfMFs);
					if(numberOfMFs>0)
					{
						MutualFund mf=blackList.get(i);
						System.out.println("MutualFund Id="+mf.getId()+" NAV ="+mf.getNav()+" amountToSpendInEachMF="+amountToSpendInEachMF);
						if(mf.getNav() > amountToSpendInEachMF)
						{
							System.out.println("Will not buy this MF bcoz of higher Nav.");
							//below if tell if remainingMf in blackList are less than numberOfMf then calculate 'amountToSpendInEachMF' and 'numberOfMFs' again.
							if((blackList.size()-(i+1))-numberOfMFs<0)
							{
								if((blackList.size()-1)==i)
								{
									//this means blackList is all done and now we need to break this loop
									//this condition is boundary condition because if 'i' is the last index of blackList then '(blackList.size()-1)-i' will give us 0 which will be less than 'numberOfMFs' in this case
									break;
								}
								System.out.println("1)NumberOfMFs > ((blackList.size()-1)-i)");
								int remainingMfToSpendMoneyOn=(blackList.size()-(i+1));
								amountToSpendInEachMF=amountToBuy/remainingMfToSpendMoneyOn;
								numberOfMFs=remainingMfToSpendMoneyOn;
								continue;
							}
							else
							{
								System.out.println("2)NumberOfMFs < ((blackList.size()-1)-i)");
								continue;
							}
						}
						else
						{
							percentageToBuyInTotalPortfolio=(amountToSpendInEachMF/totalPortfolioAmount)*100.0;
							Action action=new Action();
							action.setAction(1);
							action.setAmount(amountToSpendInEachMF);
							action.setAssetId(mf.getId());
							action.setAssetType(Asset.MUTUALFUND_ASSET_TYPE);
							action.setPercentage(percentageToBuyInTotalPortfolio);
							actionList.add(action);
							numberOfMFs--;
							updateUserPortfolioState(null,mf,amountToSpendInEachMF,list,totalPortfolioAmountBeforeSuggestion,totalAssetAmount);
							System.out.println("1) Old Percentage from SubSegment = "+subSegment+" Distribution = "+(distributionAllocationMap.get(subSegment)+totalPercentageBought));
							totalPercentageBought+=percentageToBuyInTotalPortfolio;
							totalAmountInvested+=amountToSpendInEachMF;
							System.out.println("2) New Percentage in subSegement = "+subSegment+"  Distribution  = "+(distributionAllocationMap.get(subSegment)+totalPercentageBought));
							amountToBuy-=amountToSpendInEachMF;
						}
						Double subSegmentPercentage=distributionAllocationMap.get(subSegment);
						if(subSegmentPercentage==null)
						{
							//in distributionAllocationMap at this point it might not contain either of 'LC/MC/SC' because this allocation map gets updated after schemeLevelSuggestion which removes some stocks and mutualfunds hence might remove complete subSegment like 'SMALL CAP'
							subSegmentPercentage=percentageToBuyInTotalPortfolio;
						}
						else
						{
							subSegmentPercentage+=percentageToBuyInTotalPortfolio;
							distributionAllocationMap.put(subSegment, subSegmentPercentage);
						}
					}	
				}
			}*/
			
			System.out.println("SubSegment allocation map after buying MF ="+distributionAllocationMap);
			System.out.println("No of MFs left ="+numberOfMFs+" totalCashLeft for buying in this subSegment = "+amountToBuy+" subSegmet = "+subSegment);
			if(numberOfMFs>0 || amountToBuy>0)
			{
				//it is possible that either numberOfMFs==0 but amountToBuy is still left because of nav>amountToBuy
			}
			return amountToBuy;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/*//this function will give us those 2 stocks which after selling for one and buying of another, we get currentPercent as close to idealPercent
	public void attemptToGetBestIdealSectorAllocationInStocks(List<Stocks> buyingStockList,List<Stocks> sellingStockList,double totalAmount,double idealPercent,double currentPercent)
	{
		//for eg ideal is 30 % and current percent is 35 % then we need to bring current to 30 % or close to it by using best combination of selling and buying 
		
		 * In case of currentPercent>idealPercent
		 * a)selling is necessary
		 * b)buying depends on selling since after selling only it is possible that we get closest to idealPercent
		
		 * In case of currentPercent<idealPercent
		 * a)buying is necessary
		 * b)selling depends on buying since after buying only it is possible that we get closest to idealPercent
		 
		Map<String,Double> afterSectorDistributionPercentageMap=new HashMap<String,Double>();
		double amountToAdjustToIdealPercent=0;
		try
		{
			if(buyingStockList==null || buyingStockList.size()==0)
				throw new Exception("Buying Stocks List cannot be empty.");
			if(sellingStockList==null || sellingStockList.size()==0)
				throw new Exception("Selling Stocks List cannot be empty.");
			//first of all order both list in ascending order of their closePrice;
			//reason of sorting :- gives us best chance to reach idealPercent early
			buyingStockList.sort(new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					Stocks s1=(Stocks)o1;
					Stocks s2=(Stocks)o2;
					if(s1.getClosePrice()>s2.getClosePrice())
						return 1;
					else
						return -1;
				}
			});
			sellingStockList.sort(new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					Stocks s1=(Stocks)o1;
					Stocks s2=(Stocks)o2;
					if(s1.getClosePrice()>s2.getClosePrice())
						return 1;
					else
						return -1;
				}
			});
			if(currentPercent>idealPercent)
			{
				amountToAdjustToIdealPercent = ((currentPercent-idealPercent)/totalAmount)*100.0;
				//now make nested loop one for redemption and second for buying
				for(int i=0;i<sellingStockList.size();i++)
				{
					
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}*/

	//used when we are buying stocks
	public void addStockSegmentsWhichAreNotInUsersStockSegments(Map<String,Double> idealAllocationMap,Map<String,Double> usersAllocationMap)
	{
		for(Map.Entry<String, Double> map:idealAllocationMap.entrySet())
		{
			if(!usersAllocationMap.containsKey(map.getKey()))
			{
				usersAllocationMap.put(map.getKey(), 0.0);
			}
		}
	}
	
	//used when we are buying stocks 
	public void removeStockSegmentsWhichAreInUsersStockSegments(Map<String,Double> idealAllocationMap,Map<String,Double> usersAllocationMap)
	{
		Iterator keyItr=usersAllocationMap.keySet().iterator();
		while(keyItr.hasNext())
		{
			String key=(String)keyItr.next();
			if(!idealAllocationMap.containsKey(key))
			{
				keyItr.remove();
			}
		}
	}
	
	//used when we are buying MF
	public void addSubSegmentsWhichAreNotInUsersPortfolio(Map<String,Double> idealAllocationMap,Map<String,Double> usersAllocationMap)
	{
		for(Map.Entry<String, Double> map:idealAllocationMap.entrySet())
		{
			if(!usersAllocationMap.containsKey(map.getKey()))
			{
				usersAllocationMap.put(map.getKey(), 0.0);
			}
		}
	}
	
	public void updateValueMapAsPerPercentageMap(double amount, Map<String,Double> valueMap, Map<String,Double> percentageMap)
	{
		for(Map.Entry<String, Double> map:percentageMap.entrySet())
		{
			String key=map.getKey();
			valueMap.put(key, (map.getValue()*amount)/100.0);
		}
			
	}

	public Integer getNumberOfAssetOnBasisOfThresholdPercent(double amountToInvest,double totalPortfolioAmount,double threshold)
	{
		try
		{
			double percentageToBuy=(amountToInvest/totalPortfolioAmount)*100.0;
			//System.out.println(percentageToBuy+" thres="+(percentageToBuy/threshold)+" 4.0/3.0"+(4.0/3.0));
			int number=(int)(percentageToBuy/threshold);
			return number+1;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public Map<String,Double> addValuesForSameKey(Map<String,Double> map1,Map<String,Double> map2)
	{
		Map<String,Double> additionMap=null;
		try
		{
			additionMap=new HashMap<String,Double>();
			if(map1==null)
				return map2;
			if(map2==null)
				return map1;
			additionMap.putAll(map1);
			for(Map.Entry<String, Double> entry:map2.entrySet())
			{
				String subSegment=entry.getKey();
				if(additionMap.containsKey(subSegment))
				{
					Double val=entry.getValue();
					Double existingVal=additionMap.get(subSegment);
					additionMap.put(subSegment, (val+existingVal));
				}
				else
				{
					additionMap.put(subSegment, entry.getValue());
				}
			}
			return additionMap;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	//returns from which UserPortfolioState and how much to redeem
	//don't update UserPortfolioState which is passed in List of first argument in any case bcoz it is being used in getStockMoneyOutFromUserPortfolio() to remove UserPortfolioState from List using that object like - list.remove(userPortfolioState);
	public Map<UserPortfolioState,Double> bestStockToTakeMoneyOutFromBlackList(List<UserPortfolioState> list,double amountToRedeem,Map<Integer,Stocks> usersStockMap) throws Exception
	{
		//will loop through each list and decide which stock can be redeemed such that it is closest to 'pivotPoint' which is variable;
		final float pivotPoint=0;
		double minimumDifferenceFromPivotPoint=0;UserPortfolioState upsWithMinimumDifferenceFromPivotPoint=null;
		double amountToRedeem1=0,actualAmountToRedeem=0;
		Map<UserPortfolioState,Double> map=new HashMap<UserPortfolioState,Double>();
		for(int i=0;i<list.size();i++)
		{
			amountToRedeem1=amountToRedeem;
			UserPortfolioState ups1=(UserPortfolioState)list.get(i);
			System.out.println("ups1.id="+ups1.getId());
			if(ups1.getCurrentAssetValue()>=amountToRedeem1)
			{
				while(amountToRedeem1>0)
				{
					double closePrice=((Stocks)usersStockMap.get((int)ups1.getAssetId())).getClosePrice();
					amountToRedeem1 -= closePrice;
					if(Math.abs(amountToRedeem1)>ups1.getCurrentAssetValue())
					{
						amountToRedeem1+=closePrice;
						break;
					}
					System.out.println("amountToRedeem1="+amountToRedeem1);
					/*actualAmountToRedeem += closePrice;*/
					if(actualAmountToRedeem>ups1.getCurrentAssetValue())
						throw new Exception("This exception should not have arrived bcoz of if condition outside of while loop.");
				}
				if(Math.abs(amountToRedeem1)<minimumDifferenceFromPivotPoint || minimumDifferenceFromPivotPoint==0)
				{
					minimumDifferenceFromPivotPoint=Math.abs(amountToRedeem1);
					upsWithMinimumDifferenceFromPivotPoint=ups1;
					//understand that if amountToRedeem1<0 then -(amountToRedeem1) will automatically convert below equation to addition(+)
					actualAmountToRedeem=amountToRedeem-amountToRedeem1;
					System.out.println("Actual Amount to redeem = "+actualAmountToRedeem);
					break;
				}
			}
			else
			{
				//redeem all from this stocks since no point of keeping such small stock and hence also update amountToRedeem
				map.put(ups1, ups1.getCurrentAssetValue());
				amountToRedeem-=ups1.getCurrentAssetValue();
				System.out.println("when selling whole units amountToRedeem = "+ups1.getCurrentAssetValue());
			}
		}
		if(minimumDifferenceFromPivotPoint!=0 && upsWithMinimumDifferenceFromPivotPoint!=null)
		{
			map.put(upsWithMinimumDifferenceFromPivotPoint, actualAmountToRedeem);
		}
		return map;
	}
	
	//will return miscellaneous cash left after buying stock 
	public double miscellaneousFunctionToMakeIdealPercentageForStocksInSegments(double cash,Map<String,Double> afterSectorDistributionAllocationMap) throws FundexpertException
	{
		Map<String,List<UserPortfolioState>> stockSegmentWiseUserPortfolioMap=sls.getStockSegmentWiseUserPortfolioStateMap();
		try
		{
			if(cash>0 )
			{
				System.out.println("Entered miscellaneous.");
				//buy few stocks 
				for(Map.Entry<String, Double> map:afterSectorDistributionAllocationMap.entrySet())
				{
					String stockSegment=map.getKey();
					Double allocation=map.getValue();
					System.out.println("Segment = "+stockSegment+" and it's percentage allocation = "+allocation);
					List<UserPortfolioState> list=stockSegmentWiseUserPortfolioMap.get(stockSegment);
					double amountNotAbleToInvestAnywhere=putStockSegmentMoneyIntoUserPortfolio(totalPortfolioAmountBeforeSuggestion,sls.getTotalDE(),cash,list,stockSegmentAfterSuggestionAllocationMap,stockRecommendedList,mfRecoList,stockRecoList,stockSegment,sls);
					double amountInvested=cash-amountNotAbleToInvestAnywhere;
					cash-=amountInvested;
				}
			}
			if(cash<0)
			{
				
			}
			return cash;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new FundexpertException("Not able to handle extra cash(possible to be in (-)ive in which case we redeem from some stocks) which is left after selling and buying of Stocks.");
		}
	}

	//this function will be called only when buying stocks/MF
	//reason for passing list in this function and only UserPortfolioState in another overloaded function is beacuse list contains stocks in UserPortfolioState so if we buy new stock then it needs to be added back in list
	//if stock!=null then we are buying that Stock
	//if mf!=null then we are buying that MF
	void updateUserPortfolioState(Stocks stock,MutualFund mf,double amountToInvest,List<UserPortfolioState> list,double totalPortfolioAmount,double totalAssetAmount)
	{
		try
		{
			boolean foundAssetInList=false;
			if(stock!=null)
			{
				if(list!=null)
				{
					for(UserPortfolioState ups:list)
					{
						if((int)ups.getAssetId()==stock.getScCode())
						{
							ups.setCurrentAssetValue(ups.getCurrentAssetValue()+amountToInvest);
							double portfolioLevelAllocationIncreaseBy = (amountToInvest/totalPortfolioAmount)*100.0;
							double assetLevelAllocationIncreaseBy = (amountToInvest/totalAssetAmount)*100.0;
							ups.setAssetLevelAllocation(ups.getAssetLevelAllocation()+assetLevelAllocationIncreaseBy);
							ups.setPortfolioLevelAllocation(ups.getPortfolioLevelAllocation()+portfolioLevelAllocationIncreaseBy);
							foundAssetInList=true;
							break;
						}
					}	
				}
				if(!foundAssetInList)
				{
					UserPortfolioState ups=new UserPortfolioState();
					ups.setUserId(userId);
					ups.setPortfolioLevelAllocation((amountToInvest/totalPortfolioAmount)*100.0);
					ups.setLastUpdatedOn(new Date());
					ups.setAssetType(Asset.STOCK_ASSET_TYPE);
					ups.setAssetId((long)stock.getScCode());
					ups.setLiability(false);
					ups.setAssetLevelAllocation((amountToInvest/totalAssetAmount)*100.0);
					ups.setCurrentAssetValue(amountToInvest);
					ups.setSegment(stock.getSegment());
					ups.setSubSegment(stock.getType());
					list.add(ups);
				}
			}
			if(mf!=null)
			{
				if(list!=null)
				{
					for(UserPortfolioState ups:list)
					{
						if(mf.getId()==ups.getAssetId())
						{
							ups.setCurrentAssetValue(ups.getCurrentAssetValue()+amountToInvest);
							double portfolioLevelAllocationIncreaseBy = (amountToInvest/totalPortfolioAmount)*100.0;
							double assetLevelAllocationIncreaseBy = (amountToInvest/totalAssetAmount)*100.0;
							ups.setAssetLevelAllocation(ups.getAssetLevelAllocation()+assetLevelAllocationIncreaseBy);
							ups.setPortfolioLevelAllocation(ups.getPortfolioLevelAllocation()+portfolioLevelAllocationIncreaseBy);
							foundAssetInList=true;
							break;
						}
					}
				}
				if(!foundAssetInList)
				{
					UserPortfolioState ups=new UserPortfolioState();
					ups.setUserId(userId);
					ups.setPortfolioLevelAllocation((amountToInvest/totalPortfolioAmount)*100.0);
					ups.setLastUpdatedOn(new Date());
					ups.setAssetType(Asset.MUTUALFUND_ASSET_TYPE);
					ups.setAssetId(mf.getId());
					ups.setLiability(false);
					ups.setAssetLevelAllocation((amountToInvest/totalAssetAmount)*100.0);
					ups.setCurrentAssetValue(amountToInvest);
					ups.setSegment(mf.getBroaderSchemeType());
					ups.setSubSegment(mf.getBroaderType());
					list.add(ups);
				}
			}
				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//this will be called when redeeming from passed argument UserPortfolioState
	//argument amountToRedeem is (+)ive value
	//argument stock and mf cannot be !null at the same time
	void updateUserPortfolioState(Stocks stock,MutualFund mf,double amountToRedeem,UserPortfolioState ups,double totalPortfolioAmount,double totalAssetAmount) throws FundexpertException
	{
		try
		{
			if(stock==null && mf==null)
				throw new FundexpertException("Stock and MutualFund both cannot be null.");
			if(stock!=null)
			{
				if(stock.getScCode()!=(int)ups.getAssetId())
						throw new FundexpertException("Stock id differs.");
				double portfolioLevelAllocationReducedBy = (amountToRedeem/totalPortfolioAmount)*100.0;
				double assetLevelAllocationReducedBy = (amountToRedeem/totalAssetAmount)*100.0;
				ups.setCurrentAssetValue(ups.getCurrentAssetValue()-amountToRedeem);
				ups.setAssetLevelAllocation(ups.getAssetLevelAllocation()-assetLevelAllocationReducedBy);
				ups.setPortfolioLevelAllocation(ups.getPortfolioLevelAllocation()-portfolioLevelAllocationReducedBy);
			}
			else if(mf!=null)
			{
				if(mf.getId()!=ups.getAssetId())
					throw new FundexpertException("MutualFund id differs.");
				double portfolioLevelAllocationReducedBy = (amountToRedeem/totalPortfolioAmount)*100.0;
				double assetLevelAllocationReducedBy = (amountToRedeem/totalAssetAmount)*100.0;
				ups.setCurrentAssetValue(ups.getCurrentAssetValue()-amountToRedeem);
				ups.setAssetLevelAllocation(ups.getAssetLevelAllocation()-assetLevelAllocationReducedBy);
				ups.setPortfolioLevelAllocation(ups.getPortfolioLevelAllocation()-portfolioLevelAllocationReducedBy);
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
	//usage
	/*When buying stocks
	 * while investing money in particular Segment in stock we are also keeping track of stock subSegment allocation as per idealStockSubSegmentValueMap
	 * so while deciding noOfStocks to invest in we need not to consider those stocks whose subSegment allocation has or crossed its idealSubSegmentAllocation
	 */
	int idealReachedForSubSegment(List<Stocks> stocksInUsersListHavingCommonSegment,Map<String,Double> userSubSegmentMap,Map<String,Double> idealSubSegmentValueMap,double totalPortfolioAmount)
	{
		int counter=0;
		try
		{
			for(Map.Entry<String,Double> map:idealSubSegmentValueMap.entrySet())
			{
				String key=map.getKey();
				if(!userSubSegmentMap.containsKey(key))
					throw new FundexpertException("Users Stock SubSegment allocation does not contain subSegment = "+key);
				double idealValue=map.getValue();
				double userPercentage=userSubSegmentMap.get(key);
				if((userPercentage*totalPortfolioAmount)/100.0 >= idealValue)
				{
					System.out.println("User's Stock allocation for subSegment = "+key+" has reached it's ideal alocation of = "+idealValue);
					for(Stocks stock:stocksInUsersListHavingCommonSegment)
					{
						if(stock.getType().equals(key))
							counter++;
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return counter;
	}

	Map<String,Double> makeAlocationOnBaseOf100(Map<String,Double> idealAllocation,double totalPortfolioInAsset)
	{
		Map<String,Double> idealBase100ValueMap=new HashMap<String,Double>();
		//for eg if LC+MC+SC = 70% then currentBase is 70% and then we divide the 
		/*double currentBase=0;
		for(Map.Entry<String, Double> map:idealAllocation.entrySet())
		{
			currentBase+=map.getValue();
		}*/
		for(Map.Entry<String, Double> map:idealAllocation.entrySet())
		{
			idealBase100ValueMap.put(map.getKey(),map.getValue()*totalPortfolioInAsset/100.0);
		}
		return idealBase100ValueMap;
	}
	
	public boolean findIfAmountCanStillBeInvested(double amountToBuy, List<Stocks> existingWhiteList,Map<String, Double> idealStockSubSegmentValueMap,Map<String, Double> stocksSubSegmentPercentageAllocationMap, double totalPortfolioAmount) 
	{
		try
		{
			for(Stocks stock:existingWhiteList)
			{
				String subSegment=stock.getType();
				System.out.println("SubSegment = "+subSegment);
				double maxValueToMakeIdealValue=idealStockSubSegmentValueMap.get(subSegment);
				double currentValueInThisSubSegment=stocksSubSegmentPercentageAllocationMap.get(subSegment)*totalPortfolioAmount/100.0;
				System.out.println(subSegment+" | "+maxValueToMakeIdealValue+" | "+currentValueInThisSubSegment);
				if((currentValueInThisSubSegment+amountToBuy)<=maxValueToMakeIdealValue)
				{
					//make sure closePrice is less than amountToBuy
					if(amountToBuy>=stock.getClosePrice())
						return true;
					continue;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public Map<String,Map<String,List<UserPortfolioState>>> doRebalance() throws Exception
	{
		Map<String,Map<String,List<UserPortfolioState>>> rebalanceMap=null;
		Session hSession=null;
		Algo algo=new Algo(userId,consumerId,riskProfile);
		try
		{
			rebalanceMap=new HashMap<String,Map<String,List<UserPortfolioState>>>();
			algo.preRequsiteCheck();
			SchemeLevelSuggestion sls=algo.getFirstSchemeLevelSuggestion();
			double totalAmountInDE=sls.getTotalDE();
			double totalAmountInDEAfterSuggestion=sls.getTotalDEAfterSuggestion();
			Map<Integer,Stocks> usersStockMap=sls.getUsersStockMap();
			
			Map<String,Double> afterSectorDistributionAllocationMap=sls.getStockSegmentAfterSuggestionAllocationMap();
			System.out.println("In Algo Test before suggestion Allocation Map = "+afterSectorDistributionAllocationMap);
			
			double cashLeftFromStocks=algo.doStockSectorDistribution(totalAmountInDE, sls);
			System.out.println("Cash leftafter sector distribution = "+cashLeftFromStocks);
			
			afterSectorDistributionAllocationMap=sls.getStockSegmentAfterSuggestionAllocationMap();
			System.out.println("In Algo Test after suggestion Allocation Map = "+afterSectorDistributionAllocationMap);
			
			
			StocksController sc=new StocksController();
			System.out.println("Total amt in DE = "+totalAmountInDE+" totalAmountInDEAfterSuggestion = "+totalAmountInDEAfterSuggestion);
			Map<String,List<UserPortfolioState>> userPortfolioState=sls.getStockSegmentWiseUserPortfolioStateMap();
			for(Map.Entry<String, List<UserPortfolioState>> map1:userPortfolioState.entrySet())
			{
				System.out.println("Segment from AlgoTest After running Sector Distribution = "+map1.getKey());
				List<UserPortfolioState> list=map1.getValue();
				
				list.forEach(i->System.out.println("Stocks in this segment = "+sc.getStockByScCode((int)i.getAssetId()).getScName()+" id = "+i.getAssetId()+" AssetValue = "+i.getCurrentAssetValue()+" assetLevelAllocation = "+i.getAssetLevelAllocation()));
			}
			
			
			
			double totalPortfolioAmountBeforeSuggestion=sls.getTotalPortfolioAmount();
			double totalDebtMFAmount=sls.getTotalDMF();
			double totalEquityMFAmount=sls.getTotalEMF();
			
			System.out.println("*********************************\n*******************************\n**************************");
			System.out.println("*********************************");
			
			double cashLeftAfterMFSubSegmentDistribution=algo.doMFSubSegmentDistribution(totalPortfolioAmountBeforeSuggestion, (totalDebtMFAmount+totalEquityMFAmount),cashLeftFromStocks, sls);
			System.out.println("Cash left after subsegment after MF SubSegment Distribution = "+cashLeftAfterMFSubSegmentDistribution);
			Map<String,List<UserPortfolioState>> mfSubSegmentWiseUserPortfolioStateMap=sls.getSubSegmentWiseUserPortfolioStateMFMap();
			for(Map.Entry<String, List<UserPortfolioState>> map:mfSubSegmentWiseUserPortfolioStateMap.entrySet())
			{
				String subSegment = map.getKey();
				List<UserPortfolioState> list=map.getValue();
				System.out.println("!@#$SubSegment From AlgoTest = "+subSegment);
				list.forEach(i->System.out.println("UPS id="+i.getId()+" MF ID ="+i.getAssetId()+" currentAssetValue = "+i.getCurrentAssetValue()+" AssetLevelAllocation = "+i.getAssetLevelAllocation()+" portfolioLevelAllocation = "+i.getPortfolioLevelAllocation()));
			}
			Map<String,List<UserPortfolioState>> stockSegmentWiseUserPortfolioStateMap=sls.getStockSegmentWiseUserPortfolioStateMap();
			for(Map.Entry<String, List<UserPortfolioState>> map:stockSegmentWiseUserPortfolioStateMap.entrySet())
			{
				String subSegment = map.getKey();
				List<UserPortfolioState> list=map.getValue();
				System.out.println("!@#$SubSegment From AlgoTest = "+subSegment);
				list.forEach(i->System.out.println("UPS id="+i.getId()+" Stocks ID ="+i.getAssetId()+" currentAssetValue = "+i.getCurrentAssetValue()+" AssetLevelAllocation = "+i.getAssetLevelAllocation()+" portfolioLevelAllocation = "+i.getPortfolioLevelAllocation()));
			}
			rebalanceMap.put("stock", userPortfolioState);
			rebalanceMap.put("mf", mfSubSegmentWiseUserPortfolioStateMap);
			return rebalanceMap;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
}
