package com.apidoc.util;

import java.io.IOException;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.fundexpert.config.Config;

public class HibernateBridge 
{
	 private static volatile SessionFactory sessionFactory;

	    private HibernateBridge() {
	    }

	    public static SessionFactory getSessionFactory() {
	        if (sessionFactory == null) {
	            synchronized (HibernateBridge.class) {
	                if (sessionFactory == null) {
	                	try
	                	{
	                		Config config=new Config();
		                	if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
		                	{
			                    sessionFactory = (new Configuration().configure("hibernate.cfg.xml")).buildSessionFactory();
			                    System.out.println("Production Environment.");
		                	}
		                	else
		                	{
		                		 sessionFactory = (new Configuration().configure("development.cfg.xml")).buildSessionFactory();
				                 System.out.println("Development Environment.");
		                	}
	                	}
	                	catch(Exception e)
	                	{
	                		e.printStackTrace();
	                		sessionFactory = (new Configuration().configure("hibernate.cfg.xml")).buildSessionFactory();
	                	}
	                }
	            }
	        }
	        return sessionFactory;
	    }
	    
	    public static void buildSessionFactory(){
	    	sessionFactory.close();
	    	sessionFactory=null;
	    	sessionFactory = (new Configuration().configure()).buildSessionFactory();
	    }
}