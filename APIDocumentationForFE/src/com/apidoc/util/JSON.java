package com.apidoc.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.LazyInitializationException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fundexpert.controller.HoldingController;
import com.fundexpert.controller.MutualFundController;
import com.fundexpert.controller.StocksController;
import com.fundexpert.controller.UserController;
import com.fundexpert.controller.UserWiseAdvisorController;
import com.fundexpert.controller.UserWiseAdvisorController.Advise;
import com.fundexpert.controller.UserWiseAdvisorController.PortfolioAdvise;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Nav;
import com.fundexpert.dao.PortfolioRequest;
import com.fundexpert.dao.SIPDate;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.Transaction;
import com.fundexpert.dao.User;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.StocksHoldings;
import com.apidoc.util.JSON;
import com.fundexpert.dao.UserPortfolioState;

public class JSON {
	
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	static DecimalFormat df=new DecimalFormat("###.###");
	static final String MutualFund = "MutualFund";
	static final String FundHouse = "FundHouse";
	static final String User = "User";
	static final String Transaction = "Transaction";
	static final String Holding = "Holding";
	static final String StocksHoldings = "StocksHoldings";
	static final String UserPortfolioState = "UserPortfolioState";
	static final String Stocks="Stocks";

	public static JSONArray getMutualFundJsonForApi(List<MutualFund> mfList)
	{
		JSONArray array=new JSONArray();
		try
		{
			Iterator itr=mfList.iterator();
			while(itr.hasNext())
			{
				MutualFund mf=(MutualFund)itr.next();
				//System.out.println("mf name="+mf.getName());
				JSONObject obj = new JSONObject();
				
				obj.put("id", mf.getId());
				obj.put("name", mf.getName());
				obj.put("rtaSchemeCode",mf.getRtaSchemeCode());
				obj.put("schemeCode", mf.getSchemeCode());
				obj.put("roboSchemeCode",mf.getRoboSchemeCode());
				obj.put("type", mf.getType());
				obj.put("optionType", mf.getOptionType());
				obj.put("schemeType", mf.getSchemeType());
				obj.put("comment",mf.getComment());
				obj.put("isin",mf.getIsin());
				obj.put("camsSchemeCode",mf.getCamsSchemeCode());
				obj.put("settlementType", mf.getSettlementType());
				obj.put("sipFlag", mf.isSipFlag());
				obj.put("transacctionMode",mf.getTransactionMode());
				obj.put("purchasedAllowed",mf.getPurchaseAllowed());
				obj.put("dematPurchaseAllowed",mf.getDematPurchaseAllowed());
				obj.put("physicalPurchaseAllowed", mf.getPhysicalPurchaseAllowed());
				obj.put("minPurchase", mf.getMinPurchase());
				obj.put("high52Week",mf.getHigh52Week());
				obj.put("low52Week",mf.getLow52Week());
				obj.put("minAdditionalPurchase", mf.getMinAdditionalPurchase());
				obj.put("minSIPAmount", mf.getMinSIPAmount());
				obj.put("maxSIPAmount", mf.getMaxSIPAmount());
				obj.put("minNoOfMonths", mf.getMinNoOfMonths());
				obj.put("maxNoOfMonths", mf.getMaxNoOfMonths());
				obj.put("purchaseMultiple", mf.getPurchaseMultiple());
				obj.put("timeframe",mf.getTimeframe());
				obj.put("lockInPeriod",mf.getLockInPeriod());
				obj.put("maxPurchase", mf.getMaxPurchase());
				obj.put("minRedemptionAmt",mf.getMinRedemptionAmt());
				obj.put("minRedemptionQty",mf.getMinRedemptionQty());
				obj.put("nav", mf.getNav());
				obj.put("navChange", mf.getNavChange());
				obj.put("lastYearNav", mf.getLastYearNav());
				obj.put("oneYearReturns",mf.getOneYearReturns());
				obj.put("threeYearReturns", mf.getThreeYearReturns());
				obj.put("fiveYearReturns", mf.getFiveYearReturns());
				obj.put("overallReturns", mf.getOverallReturns());
				obj.put("sharpeRatio", mf.getSharpeRatio());
				obj.put("stdDeviation",mf.getStdDeviation());
				obj.put("alpha", mf.getAlpha());
				obj.put("beta", mf.getBeta());
				obj.put("rSquaredValue", mf.getrSquaredValue());
				obj.put("upsideCaptureRatio", mf.getUpsideCaptureRatio());
				obj.put("downsideCaptureRatio", mf.getDownsideCaptureRatio());
				obj.put("vroRating", mf.getVroRating());
				obj.put("ourRating", mf.getOurRating());
				obj.put("roboAvailable", mf.getRoboAvailable());
				obj.put("validated",mf.isValidated());
				obj.put("roboSuggested",mf.isRoboSuggested());
				obj.put("flexiSuggested", mf.isFlexiSuggested());
				obj.put("safeSuggested", mf.isSafeSuggested());
				obj.put("taxSuggested",mf.getTaxSuggested());
				obj.put("growthSuggested",mf.getGrowthSuggested());
				obj.put("active", mf.isActive());
				obj.put("addedOn", mf.getAddedOn());
				obj.put("roboSuggestedOn", mf.getRoboSuggestedOn());
				obj.put("flexiSuggestedOn", mf.getFlexiSuggestedOn());
				obj.put("safeSuggestedOn", mf.getSafeSuggestedOn());
				obj.put("navUpdatedOn", mf.getNavUpdatedOn());
				obj.put("startDate", mf.getStartDate());
				obj.put("endDate", mf.getEndDate());
				obj.put("liquidFundId",mf.getLiquidFundId());
				obj.put("amfiiCode", mf.getAmfiiCode());
				obj.put("benchmarkIndex",mf.getBenchmarkIndex());
				obj.put("schemeSize",mf.getSchemeSize());
				obj.put("exitLoad",mf.getExitLoad());
				obj.put("entryLoad",mf.getEntryLoad());
				obj.put("houseId", mf.getHouseId());
				obj.put("growthFundId", mf.getGrowthFundId());
				obj.put("nonAofGrowthSuggested",mf.isNonAofGrowthSuggested());
				obj.put("direct",mf.isDirect());
				obj.put("regularFundId",mf.getRegularFundId());
				obj.put("planType",mf.getPlanType());
				obj.put("openEnded", mf.getOpenEnded() != null ? mf.getOpenEnded() : "");
				obj.put("fundManager",mf.getFundManager());
				obj.put("stpAllowed", mf.getStpAllowed());
				obj.put("swpAllowed",mf.getSwpAllowed());
				obj.put("switchAllowed",mf.getSwitchAllowed());
				obj.put("nfoFlag",mf.getNfoFlag());
				obj.put("redemptionAllowed",mf.getRedemptionAllowed());
				obj.put("updatedOn",mf.getUpdatedOn());
				obj.put("morningStarAnalystRating",mf.getMorningStarAnalystRating());
				obj.put("morningStarRating",mf.getMorningStarRating());
				obj.put("ytm", mf.getYtm());
				obj.put("personalScore",mf.getPersonalScore());
				obj.put("technicalScore",mf.getTechnicalScore());
				obj.put("modifiedDurationInYears",mf.getModifiedDurationInYears());
				obj.put("dividendReinvestmentFlag",mf.getDividendReinvestmentFlag());
				obj.put("rtaAgentCode",mf.getRtaAgentCode());
				obj.put("bseSchemeType",mf.getBseSchemeType());
				obj.put("sipAllowed", mf.getSipAllowed());
				obj.put("minGap",mf.getMinGap());
				obj.put("maxGap",mf.getMaxGap());
				obj.put("expenseRatio", mf.getExpenseRatio());
				obj.put("broaderSchemeType", mf.getBroaderSchemeType());
				obj.put("broaderType", mf.getBroaderType());
				
				JSONArray sipArray=new JSONArray();
				Set<SIPDate> sipDate=mf.getSipDates();
				if(sipDate!=null && sipDate.size()>0)
				{
					JSONObject inner=null;
					SIPDate sipd;
					Iterator<SIPDate> i = sipDate.iterator();
					while (i.hasNext())
					{
						sipd = (SIPDate) i.next();
						inner=new JSONObject();
						inner.put("s", sipd.getSipDate());
						inner.put("f",sipd.getFrequencyType());
						sipArray.put(inner);
					}
					obj.put("sipDates",sipArray);
				}
				array.put(obj);
			}
			return array;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return array!=null?array:new JSONArray();
		}
	}

	//if 2nd argument is true then send MF data else send Stocks data
	public static JSONObject toJSONObject(User user,boolean mfPortfolio) throws JSONException
	{
		List<Holding> hList=null;
		Set<StocksHoldings> stocksList=null;
		double investmentValue=0;
		try
		{
			JSONObject obj = new JSONObject();
			obj.put("pan", user.getPan());
			obj.put("email", user.getEmail());
			if(mfPortfolio)
			{
				if(user.getConsumer().isFetchPortfolioService())
				{
					//System.out.println("HoldingList.size()="+user.getHoldings().size()+"  ");
					HoldingController ctrl=new HoldingController();
					hList=ctrl.getHoldings(user.getId());
					if(!user.getConsumer().isAdvisoryWithPortfolioService())
					{
						if(hList!=null && hList.size()>0)
							obj.put("portfolio",JSON.toJSONArray(hList));
					}
					else
					{
						if(hList!=null && hList.size()>0)
						{
							JSONArray jsonArray=new JSONArray();
							JSONObject jsonObject=null;
							UserWiseAdvisorController advisoryCtrl=new UserWiseAdvisorController(user.getId());
							Map<Long,Advise> map=advisoryCtrl.getAdvise();
							for(int i=0;i<hList.size();i++)
							{
								Holding holding=hList.get(i);
								jsonObject=toJSONObject(holding);
								Advise advise=map.get(holding.getId());
								if(advise!=null)
								{
									JSONObject jsonAdvise=new JSONObject(advise);
									for(String key:JSONObject.getNames(jsonAdvise))
									{
										String value=jsonAdvise.getString(key);
										jsonObject.put(key, value);
									}
									//below if condition is added for UnrealizedPL
									if(!jsonAdvise.has("unrealizedPL"))
									{
										jsonObject.remove("avgNav");
									}
								}
								jsonArray.put(jsonObject);
							}
							PortfolioAdvise portfolioAdvise=advisoryCtrl.getPortfolioAdvise();
							obj.put("portfolio", jsonArray);
							obj.put("investmentStyle",portfolioAdvise.getInvestmentStyle());
							obj.put("investmentHorizon",portfolioAdvise.getInvestmentHorizon());
							obj.put("investmentPreference",portfolioAdvise.getInvestmentPreference());
							obj.put("MFInvestmentValue",portfolioAdvise.getTotalInvestedMutualFundPortfolioValue());
							obj.put("MFPortfolioValue",portfolioAdvise.getTotalCurrentMutualFundPortfolioValue());
							advisoryCtrl.closeSession();
						}
					}
				}
			}
			else
			{
				if(user.getConsumer().isFetchStockPortfolioService())
				{
					stocksList=user.getStocksHoldings();
					System.out.println("stockHoldings size for userid : "+user.getId()+" = "+stocksList.size());
					if(stocksList!=null && stocksList.size()>0)
					{
						obj.put("transactions",JSON.toJSONArray(stocksList));
						obj.put("investmentStyle"," ");
						obj.put("investmentHorizon"," ");
						obj.put("investmentPreference"," ");
					}
				}
			}
			return obj;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject toJSONObject(Transaction t) throws JSONException
	{
		JSONObject obj = new JSONObject();
		obj.put("action", (t.getAction()==1?"Buy":"Sell"));
		obj.put("nav", String.valueOf(Math.round(t.getNav()*10000.0)/10000.0));
		obj.put("units", String.valueOf(Math.round(t.getUnits()*10000.0)/10000.0));
		obj.put("amount", String.valueOf(Math.round(t.getAmount()*10000.0)/10000.0));
		obj.put("date", new SimpleDateFormat("dd/MM/yyyy").format(t.getTransactionDate()));
		return obj;
	}
	
	public static JSONObject toJSONObject(Holding h) throws JSONException
	{
		JSONObject obj = new JSONObject();
		MutualFund mf=h.getMutualFund();
		obj.put("camsSchemeCode", mf.getCamsSchemeCode());
		obj.put("folioNumber", h.getFolioNumber());
		obj.put("avgNav", String.valueOf(Math.round(h.getAvgNav()*10000.0)/10000.0));
		obj.put("units", String.valueOf(Math.round(h.getUnits()*10000.0)/10000.0));
		obj.put("createdOn", new SimpleDateFormat("dd/MM/yyyy").format(h.getCreatedOn()));
		obj.put("updatedOn", new SimpleDateFormat("dd/MM/yyyy").format(h.getUpdatedOn()));
		obj.put("arn",(h.getArn()!=null && !h.getArn().equals(""))?h.getArn():"NA");
		obj.put("isin",mf.getIsin()!=null?mf.getIsin():"NA");
		obj.put("investmentValue",String.valueOf(Math.round(h.getAvgNav()*h.getUnits()*10000.0)/10000.0));
		obj.put("transaction", JSON.toJSONArray(h.getTransactions()));
		obj.put("class",(mf.getSchemeType()!=null && !mf.getSchemeType().equals(""))?mf.getSchemeType():"NA");
		obj.put("mClass", (mf.getType()!=null && !mf.getType().equals(""))?mf.getType():"NA");
		obj.put("arnName", (h.getArnName()!=null && !h.getArn().equals(""))?h.getArnName():"Null");
		obj.put("schemeName", h.getMfName()!=null?h.getMfName():"");
		return obj;
	}
	
	public static JSONObject toJSONObject(StocksHoldings h) throws JSONException
	{
		JSONObject obj = new JSONObject();
		Stocks stock=h.getStocks();
		obj.put("broker", h.getDematName());
		obj.put("stockISIN", stock.getIsinCode());
		obj.put("units", String.valueOf(Math.round(h.getNumberOfShares()*10000.0)/10000.0));
		obj.put("equityType", stock.getType()!=null?stock.getType():"");
		obj.put("stockName", h.getStockName()!=null?h.getStockName():"");
		return obj;
	}
	
	public static JSONObject toJSONObject(MutualFund mf) throws JSONException
	{
		JSONObject obj = new JSONObject();
		obj.put("camsCode", mf.getCamsSchemeCode());
		return obj;
	}
	
	public static JSONObject toJSONObject(UserPortfolioState ups,MutualFundController mfc,StocksController sc)
	{
		JSONObject obj=new JSONObject();
		if(ups.getAssetType()==Asset.MUTUALFUND_ASSET_TYPE)
		{
			if(mfc==null)
				mfc=new MutualFundController();
			MutualFund mf=mfc.getFundById(ups.getAssetId());
			obj.put("mfName", mf.getName());
		}
		else if(ups.getAssetType()==Asset.STOCK_ASSET_TYPE)
		{
			if(sc==null)
				sc=new StocksController();
			Stocks stocks=sc.getStockByScCode((int)ups.getAssetId());
			obj.put("stockName",stocks.getScName());
		}
		obj.put("assetType", ups.getAssetType());
		obj.put("allocation",ups.getPortfolioLevelAllocation());
		obj.put("currentValue", ups.getCurrentAssetValue());
		obj.put("segment", ups.getSegment());
		obj.put("subSegment", ups.getSubSegment());
		return obj;
	}
	
	public static JSONObject toJSONObject(Stocks stock)
	{
		JSONObject json=new JSONObject();
		try
		{
			json.put("name", stock.getScName());
			json.put("scCode", stock.getScCode());
			json.put("type", stock.getType());
			return json;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static JSONArray toJSONArray(List<?> s) throws JSONException
	{
		JSONArray arr = new JSONArray();
		Iterator<?> i = s.iterator();
		while (i.hasNext())
		{
			arr.put(JSON.toJSONObject(i.next()));
		}
		return arr;
	}
	
	public static JSONArray toJSONArray(Set<?> s) throws JSONException
	{
		JSONArray arr = new JSONArray();
		Iterator<?> i = s.iterator();
		while (i.hasNext())
		{
			arr.put(JSON.toJSONObject(i.next()));
		}
		return arr;
	}
	
	private static JSONObject toJSONObject(Object next) throws JSONException
	{
		try
		{
			if(next.getClass().toString().toLowerCase().contains(JSON.MutualFund.toLowerCase()))
			{
				return JSON.toJSONObject((MutualFund) next);
			}
			else if(next.getClass().toString().toLowerCase().contains(JSON.Transaction.toLowerCase()))
			{
				return JSON.toJSONObject((Transaction) next);
			}
			else if(next.getClass().toString().toLowerCase().contains(JSON.StocksHoldings.toLowerCase()))
			{
				return JSON.toJSONObject((StocksHoldings) next);
			}
			else if(next.getClass().toString().toLowerCase().contains(JSON.Holding.toLowerCase()))
			{
				return JSON.toJSONObject((Holding) next);
			}
			else if(next.getClass().toString().toLowerCase().contains(JSON.UserPortfolioState.toLowerCase()))
			{
				return JSON.toJSONObject((UserPortfolioState) next,null,null);
			}
			else if(next.getClass().toString().toLowerCase().contains(JSON.User.toLowerCase()))
			{
				return JSON.toJSONObject((User) next);
			}
			else if(next.getClass().toString().toLowerCase().contains(JSON.Stocks.toLowerCase()))
			{
				return JSON.toJSONObject((Stocks) next);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Return null+class name="+next.getClass().toString().toLowerCase());
		return null;
	}
}
