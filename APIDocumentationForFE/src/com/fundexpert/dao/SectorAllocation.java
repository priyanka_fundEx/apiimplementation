package com.fundexpert.dao;

import java.io.Serializable;

public class SectorAllocation implements Serializable {

	private static final long serialVersionUID=1L;

	long consumerId;
	int riskProfile;
	int allocation;
	String sector;
	public long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public int getRiskProfile() {
		return riskProfile;
	}
	public void setRiskProfile(int riskProfile) {
		this.riskProfile = riskProfile;
	}
	public int getAllocation() {
		return allocation;
	}
	public void setAllocation(int allocation) {
		this.allocation = allocation;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	

}
