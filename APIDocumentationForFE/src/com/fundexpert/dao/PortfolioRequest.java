package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;

public class PortfolioRequest implements Serializable{

private static final long serialVersionUID = 1L;
	
	Long id, userId;
	String password;
	User user;	
	Date requestOn;
	Boolean submitted;
	int counter;
	Boolean isKarvyRequest;//since karvy does not have CAPTCHA these request will be handled after 9 pm 
	Boolean isStock;
	Date submittedOn;
	/**
	 * @return the submitted
	 */
	public Boolean getSubmitted() {
		return submitted;
	}
	/**
	 * @param submitted the submitted to set
	 */
	public void setSubmitted(Boolean submitted) {
		this.submitted = submitted;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	public Date getRequestOn() {
		return requestOn;
	}
	public void setRequestOn(Date requestOn) {
		this.requestOn = requestOn;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public Boolean getIsKarvyRequest() {
		return isKarvyRequest;
	}
	public void setIsKarvyRequest(Boolean isKarvyRequest) {
		this.isKarvyRequest = isKarvyRequest;
	}
	public Boolean getIsStock() {
		return isStock;
	}
	public void setIsStock(Boolean isStock) {
		this.isStock = isStock;
	}
	public Date getSubmittedOn() {
		return submittedOn;
	}
	public void setSubmittedOn(Date submittedOn) {
		this.submittedOn = submittedOn;
	}
	
}
