package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;
import java.util.List;
import java.util.Set;

public class User implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, consumerId;
	private String userId;
	private int riskAppetite;
	private boolean active;
	private Date createdOn;
	List<Transaction> tList;
	Set<Holding> holdings;
	Set<StocksHoldings> stocksHoldings;
	List<PortfolioSubscription> pfsList;
	List<MutualFundSubscription> mfsList;
	List<ActionLog> actionLogList;
	String email,pan;
	Consumer consumer;
	Integer role;
	String password;
	
	public List<ActionLog> getActionLogList() {
		return actionLogList;
	}
	public void setActionLogList(List<ActionLog> actionLogList) {
		this.actionLogList = actionLogList;
	}
	public Set<Holding> getHoldings() {
		return holdings;
	}
	public void setHoldings(Set<Holding> holdings) {
		this.holdings = holdings;
	}
	public List<PortfolioSubscription> getPfsList() {
		return pfsList;
	}
	public void setPfsList(List<PortfolioSubscription> pfsList) {
		this.pfsList = pfsList;
	}
	public List<MutualFundSubscription> getMfsList() {
		return mfsList;
	}
	public void setMfsList(List<MutualFundSubscription> mfsList) {
		this.mfsList = mfsList;
	}
	public List<Transaction> gettList() {
		return tList;
	}
	public void settList(List<Transaction> tList) {
		this.tList = tList;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(Long consumerId) {
		this.consumerId = consumerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getRiskAppetite() {
		return riskAppetite;
	}
	public void setRiskAppetite(int riskAppetite) {
		this.riskAppetite = riskAppetite;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public Consumer getConsumer() {
		return consumer;
	}
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}
	public Set<StocksHoldings> getStocksHoldings() {
		return stocksHoldings;
	}
	public void setStocksHoldings(Set<StocksHoldings> stocksHoldings) {
		this.stocksHoldings = stocksHoldings;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getRole() {
		return role;
	}
	public void setRole(Integer role) {
		this.role = role;
	}
}


