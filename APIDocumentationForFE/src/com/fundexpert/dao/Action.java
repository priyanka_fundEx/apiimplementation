package com.fundexpert.dao;

import java.util.Date;
import java.util.Set;

public class Action {

	public final static int BLACKLIST=1,RECOMMENDED=2,RECO=3;
	public final static int ADD=1,REMOVE=2;
	public long id;
	public int action;//1 add 2 remove
	public long consumerId;
	public int tableType;//1 for blacklist, 2 for recommended and 3 for reco
	public Integer scCode;
	public Long mutualFundId;
	public Date createdOn;
	Set<String> set=null;
	
	public Set<String> getSet() {
		return set;
	}
	public void setSet(Set<String> set) {
		this.set = set;
	}
	public long getId() {
		return id;
	}
	public int getAction() {
		return action;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public int getTableType() {
		return tableType;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setAction(int action) {
		this.action = action;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public void setTableType(int tableType) {
		this.tableType = tableType;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Integer getScCode() {
		return scCode;
	}
	public void setScCode(Integer scCode) {
		this.scCode = scCode;
	}
	public Long getMutualFundId() {
		return mutualFundId;
	}
	public void setMutualFundId(Long mutualFundId) {
		this.mutualFundId = mutualFundId;
	}
	
}
