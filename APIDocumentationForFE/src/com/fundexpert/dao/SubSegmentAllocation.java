package com.fundexpert.dao;

import java.io.Serializable;

public class SubSegmentAllocation implements Serializable{

	long consumerId;
	int riskProfile;
	int allocation;
	String subSegment;
	public long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public int getRiskProfile() {
		return riskProfile;
	}
	public void setRiskProfile(int riskProfile) {
		this.riskProfile = riskProfile;
	}
	public int getAllocation() {
		return allocation;
	}
	public void setAllocation(int allocation) {
		this.allocation = allocation;
	}
	public String getSubSegment() {
		return subSegment;
	}
	public void setSubSegment(String subSegment) {
		this.subSegment = subSegment;
	}
	
}
