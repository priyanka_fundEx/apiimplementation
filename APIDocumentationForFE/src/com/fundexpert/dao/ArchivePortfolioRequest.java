package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;

public class ArchivePortfolioRequest implements Serializable{

private static final long serialVersionUID = 1L;
	
	Long userId;
	String password;
	Date requestOn;
	
	
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getRequestOn() {
		return requestOn;
	}
	public void setRequestOn(Date requestOn) {
		this.requestOn = requestOn;
	}
}
