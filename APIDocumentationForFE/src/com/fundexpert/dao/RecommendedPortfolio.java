package com.fundexpert.dao;

public class RecommendedPortfolio {

	long id,consumerId;
	Integer scCode;
	
	public long getId() {
		return id;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public Integer getScCode() {
		return scCode;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public void setScCode(Integer scCode) {
		this.scCode = scCode;
	}
	
}
