package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;

public class Nav implements Serializable
{
	private static final long serialVersionUID=1L;
	private long amfiiCode;
	private Date tradeDate;
	private Double nav;
	
	public Date getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}
	public Double getNav() {
		return nav;
	}
	public void setNav(Double nav) {
		this.nav = nav;
	}
	public long getAmfiiCode() {
		return amfiiCode;
	}
	public void setAmfiiCode(long amfiiCode) {
		this.amfiiCode = amfiiCode;
	}
}
