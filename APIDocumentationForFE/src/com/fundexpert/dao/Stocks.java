package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;

public class Stocks implements Serializable {

	int scCode;	
	String scName;
	String scGroup;
	String scType;
	String isinCode,type;
	double sharpeRatio;
	double rating;
	double closePrice;
	Date asOn;
	boolean block;
	boolean delisted;
	String segment,sector;
	Date updatedOn;
	
	public int getScCode() {
		return scCode;
	}
	public void setScCode(int scCode) {
		this.scCode = scCode;
	}
	public String getScName() {
		return scName;
	}
	public void setScName(String scName) {
		this.scName = scName;
	}
	public String getScGroup() {
		return scGroup;
	}
	public void setScGroup(String scGroup) {
		this.scGroup = scGroup;
	}
	public String getScType() {
		return scType;
	}
	public void setScType(String scType) {
		this.scType = scType;
	}
	public String getIsinCode() {
		return isinCode;
	}
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public double getSharpeRatio() {
		return sharpeRatio;
	}
	public void setSharpeRatio(double sharpeRatio) {
		this.sharpeRatio = sharpeRatio;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getClosePrice() {
		return closePrice;
	}
	public void setClosePrice(double closePrice) {
		this.closePrice = closePrice;
	}
	public Date getAsOn() {
		return asOn;
	}
	public void setAsOn(Date asOn) {
		this.asOn = asOn;
	}
	public boolean isBlock() {
		return block;
	}
	public void setBlock(boolean block) {
		this.block = block;
	}
	public boolean isDelisted() {
		return delisted;
	}
	public String getSegment() {
		return segment;
	}
	public void setDelisted(boolean delisted) {
		this.delisted = delisted;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}

