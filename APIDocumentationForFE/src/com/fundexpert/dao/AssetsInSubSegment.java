package com.fundexpert.dao;

import java.io.Serializable;

public class AssetsInSubSegment implements Serializable{

	long assetId,consumerId;
	String subSegment;
	int riskProfile,assetType;
	public long getAssetId() {
		return assetId;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public String getSubSegment() {
		return subSegment;
	}
	public int getRiskProfile() {
		return riskProfile;
	}
	public int getAssetType() {
		return assetType;
	}
	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public void setSubSegment(String subSegment) {
		this.subSegment = subSegment;
	}
	public void setRiskProfile(int riskProfile) {
		this.riskProfile = riskProfile;
	}
	public void setAssetType(int assetType) {
		this.assetType = assetType;
	}
}
