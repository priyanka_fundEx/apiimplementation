package com.fundexpert.dao;

import java.util.Date;

public class UserPortfolioState {

	
	long id,userId,assetId;//assetId represent mutualfundid,stockid
	int assetType;//represent mutualfund(1) type or stock(2) type
	boolean liability;//should not be considered while calculation of assetAllocation
	double assetLevelAllocation;//depending on whether assetType is mutualfund or stock or land property
	double portfolioLevelAllocation;//allAssetLevelAllocation
	double currentAssetValue;
	Date lastUpdatedOn;
	String segment,subSegment;
	String assetName;
	
	public long getId() {
		return id;
	}
	public long getUserId() {
		return userId;
	}
	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}
	public long getAssetId() {
		return assetId;
	}
	public int getAssetType() {
		return assetType;
	}
	public double getCurrentAssetValue() {
		return currentAssetValue;
	}
	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}
	public void setAssetType(int assetType) {
		this.assetType = assetType;
	}
	public void setCurrentAssetValue(double currentAssetValue) {
		this.currentAssetValue = currentAssetValue;
	}
	public boolean isLiability() {
		return liability;
	}
	public void setLiability(boolean liability) {
		this.liability = liability;
	}
	public double getAssetLevelAllocation() {
		return assetLevelAllocation;
	}
	public double getPortfolioLevelAllocation() {
		return portfolioLevelAllocation;
	}
	public void setAssetLevelAllocation(double assetLevelAllocation) {
		this.assetLevelAllocation = assetLevelAllocation;
	}
	public void setPortfolioLevelAllocation(double portfolioLevelAllocation) {
		this.portfolioLevelAllocation = portfolioLevelAllocation;
	}
	public String getSegment() {
		return segment;
	}
	public String getSubSegment() {
		return subSegment;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public void setSubSegment(String subSegment) {
		this.subSegment = subSegment;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	
}
