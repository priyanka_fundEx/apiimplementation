package com.fundexpert.dao;

import java.util.Date;

public class NoUserFound {

	long id;
	String email,pan,reason;
	Date createdOn;
	boolean stock;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public String getPan() {
		return pan;
	}
	public String getReason() {
		return reason;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public boolean isStock() {
		return stock;
	}
	public void setStock(boolean stock) {
		this.stock = stock;
	}
}
