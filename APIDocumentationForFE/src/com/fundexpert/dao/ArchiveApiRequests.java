package com.fundexpert.dao;

import java.util.Date;

public class ArchiveApiRequests {

	public final static short login=1,portfolioRequest=2,mutualFundPortfolioFromPdfPath=3,mutualFundPortfolioFromEmail=4;
	public final static short stockPortfolioFromPdfpath=5,stockPortfolioFromEmail=6,mutualFundPortfolioFromByteStream=8,stockPortfolioFromByteStream=9,emailReceived=10;//emailReceived represent api that we will call on receiving email through IMAP from edelweiss@fundexpert.in 
	public final static short archiveUserHoldingAndTransaction=11;
	long id,consumerId;
	short apiRequestType;
	Date requestTime;
	Consumer consumer;
	String email;
	String pan;
	public long getId() {
		return id;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public short getApiRequestType() {
		return apiRequestType;
	}
	public Date getRequestTime() {
		return requestTime;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public void setApiRequestType(short apiRequestType) {
		this.apiRequestType = apiRequestType;
	}
	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}
	public Consumer getConsumer() {
		return consumer;
	}
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}
	public String getEmail() {
		return email;
	}
	public String getPan() {
		return pan;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	
	
	
}
