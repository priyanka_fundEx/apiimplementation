package com.fundexpert.dao;

public class Reco {

	long id,consumerId;
	Long mutualFundId;
	Integer scCode;
	public long getId() {
		return id;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public Long getMutualFundId() {
		return mutualFundId;
	}
	public Integer getScCode() {
		return scCode;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public void setMutualFundId(Long mutualFundId) {
		this.mutualFundId = mutualFundId;
	}
	public void setScCode(Integer scCode) {
		this.scCode = scCode;
	}
}
