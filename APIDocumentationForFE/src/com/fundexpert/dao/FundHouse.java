package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class FundHouse implements Serializable {
	/**
	 * Author : Sourabh Bajaj
	 * Date : 2016-05-03
	 */
	
	private static final long serialVersionUID = 1L;
	long id;
	String name, logoUrl, description, website, AMCCode, amcPrefix, registrar;
	Set<MutualFund> mutualFunds=new HashSet<MutualFund>();
	Date createdOn;
	Boolean aofMandatory;
	Boolean isipSupported;
		
	public Boolean isIsipSupported() {
		return isipSupported;
	}
	public void setIsipSupported(Boolean isipSupported) {
		this.isipSupported = isipSupported;
	}
	
	/**
	 * @return the amcPrefix
	 */
	public String getAmcPrefix() {
		return amcPrefix;
	}
	/**
	 * @param amcPrefix the amcPrefix to set
	 */
	public void setAmcPrefix(String amcPrefix) {
		this.amcPrefix = amcPrefix;
	}
	/**
	 * @return the registrar
	 */
	public String getRegistrar() {
		return registrar;
	}
	/**
	 * @param registrar the registrar to set
	 */
	public void setRegistrar(String registrar) {
		this.registrar = registrar;
	}
	/**
	 * @return the aofMandatory
	 */
	public Boolean getAofMandatory() {
		return aofMandatory;
	}
	/**
	 * @param aofMandatory the aofMandatory to set
	 */
	public void setAofMandatory(Boolean aofMandatory) {
		this.aofMandatory = aofMandatory;
	}
	/**
	 * @return the aMCCode
	 */
	public String getAMCCode() {
		return AMCCode;
	}
	/**
	 * @param aMCCode the aMCCode to set
	 */
	public void setAMCCode(String aMCCode) {
		AMCCode = aMCCode;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the logoUrl
	 */
	public String getLogoUrl() {
		return logoUrl;
	}
	/**
	 * @param logoUrl the logoUrl to set
	 */
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}
	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}
	/**
	 * @return the mutualFunds
	 */
	public Set<MutualFund> getMutualFunds() {
		return mutualFunds;
	}
	/**
	 * @param mutualFunds the mutualFunds to set
	 */
	public void setMutualFunds(Set<MutualFund> mutualFunds) {
		this.mutualFunds = mutualFunds;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
}
