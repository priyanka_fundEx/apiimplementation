package com.fundexpert.dao;

import java.io.Serializable;

public class StocksInStockSegment implements Serializable{

	int scCode;
	Stocks stock;
	String segment;
	int riskProfile;
	long consumerId;
	String subSegment;
	
	public int getScCode() {
		return scCode;
	}
	public Stocks getStock() {
		return stock;
	}
	public String getSegment() {
		return segment;
	}
	public int getRiskProfile() {
		return riskProfile;
	}
	public void setScCode(int scCode) {
		this.scCode = scCode;
	}
	public void setStock(Stocks stock) {
		this.stock = stock;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public void setRiskProfile(int riskProfile) {
		this.riskProfile = riskProfile;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public String getSubSegment() {
		return subSegment;
	}
	public void setSubSegment(String subSegment) {
		this.subSegment = subSegment;
	}
}
