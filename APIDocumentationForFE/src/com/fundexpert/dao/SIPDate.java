package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;

public class SIPDate implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long mutualFundId;
	private Integer sipDate, frequencyType;
	
	/**
	 * @return the mutualFundId
	 */
	public Long getMutualFundId() {
		return mutualFundId;
	}
	/**
	 * @param mutualFundId the mutualFundId to set
	 */
	public void setMutualFundId(Long mutualFundId) {
		this.mutualFundId = mutualFundId;
	}
	public Integer getSipDate() {
		return sipDate;
	}
	public void setSipDate(Integer sipDate) {
		this.sipDate = sipDate;
	}
	/**
	 * @return the frequencyType
	 */
	public Integer getFrequencyType() {
		return frequencyType;
	}
	/**
	 * @param frequencyType the frequencyType to set
	 */
	public void setFrequencyType(Integer frequencyType) {
		this.frequencyType = frequencyType;
	}
	
}
