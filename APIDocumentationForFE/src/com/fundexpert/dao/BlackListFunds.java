package com.fundexpert.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class BlackListFunds implements Serializable {

	long id,consumerId;
	Long mutualFundId;
	Integer scCode;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Long getMutualFundId() {
		return mutualFundId;
	}
	public Integer getScCode() {
		return scCode;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public void setMutualFundId(Long mutualFundId) {
		this.mutualFundId = mutualFundId;
	}
	public void setScCode(Integer scCode) {
		this.scCode = scCode;
	}
	public long getConsumerId() {
		return consumerId;
	}
}
