package com.fundexpert.dao;

import java.io.Serializable;
import java.util.Date;

public class StocksHoldings implements Serializable {
							
	long id;
	//mapped with one-to-many from User to StocksHoldings
	long userId;
	int scCode;
	Stocks stocks;
	double numberOfShares;
	//status = 1 means ok , status = 0 means Trading Suspended
	int status;
	Date buyDate=null;
	double buyPrice;
	Date lastUpdatedOn;
	String dematId;
	String dematName;
	String stockName;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getScCode() {
		return scCode;
	}
	public void setScCode(int scCode) {
		this.scCode = scCode;
	}
	public Stocks getStocks() {
		return stocks;
	}
	public void setStocks(Stocks stocks) {
		this.stocks = stocks;
	}
	public double getNumberOfShares() {
		return numberOfShares;
	}
	public void setNumberOfShares(double numberOfShares) {
		this.numberOfShares = numberOfShares;
	}
	
	public double getBuyPrice() {
		return buyPrice;
	}
	public void setBuyPrice(double buyPrice) {
		this.buyPrice = buyPrice;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}
	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}
	public String getDematId() {
		return dematId;
	}
	public void setDematId(String dematId) {
		this.dematId = dematId;
	}
	public String getDematName() {
		return dematName;
	}
	public void setDematName(String dematName) {
		this.dematName = dematName;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
}
