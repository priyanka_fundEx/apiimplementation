package com.fundexpert.dao;

import java.io.Serializable; 
import java.util.Date;
import java.util.Set;

public class ArchiveHolding  implements Serializable
{
	private static final long serialVersionUID=1L;
	private Long id, userId, mutualfundId;
	private String folioNumber;
	private double units, avgNav;
	private Date createdOn,updatedOn;
	Set<ArchiveTransaction> archiveTransactions;
	String arn;
	private MutualFund mutualFund;
	private Date archivedOn;
	
	public Set<ArchiveTransaction> getArchiveTransactions() {
		return archiveTransactions;
	}
	public void setArchiveTransactions(Set<ArchiveTransaction> archiveTransactions) {
		this.archiveTransactions = archiveTransactions;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getMutualfundId() {
		return mutualfundId;
	}
	public void setMutualfundId(Long mutualfundId) {
		this.mutualfundId = mutualfundId;
	}
	public String getFolioNumber() {
		return folioNumber;
	}
	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public double getAvgNav() {
		return avgNav;
	}
	public void setAvgNav(double avgNav) {
		this.avgNav = avgNav;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getArn() {
		return arn;
	}
	public void setArn(String arn) {
		this.arn = arn;
	}
	public MutualFund getMutualFund() {
		return mutualFund;
	}
	public void setMutualFund(MutualFund mutualFund) {
		this.mutualFund = mutualFund;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Date getArchivedOn() {
		return archivedOn;
	}
	public void setArchivedOn(Date archivedOn) {
		this.archivedOn = archivedOn;
	}
	
}