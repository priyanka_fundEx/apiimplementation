package com.fundexpert.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {

	Properties properties=null;
	
	public static final String ENVIRONMENT="environment",PRODUCTION="production",DEVELOPMENT="development";
	public static final String ADD_NEW_FUND_LINK_PRODUCTION="add_new_fund_link_production",UPDATE_EXISTING_FUND_LINK_PRODUCTION="update_existing_fund_link_production";
	public static final String ADD_NEW_NAV_LINK_PRODUCTION="add_new_nav_link_production",UPDATE_EXISTING_NAV_LINK_PRODUCTION="update_existing_nav_link_production";
	
	public static final String ADD_NEW_FUND_LINK_DEVELOPMENT="add_new_fund_link_development",UPDATE_EXISTING_FUND_LINK_DEVELOPMENT="update_existing_fund_link_development";
	public static final String ADD_NEW_NAV_LINK_DEVELOPMENT="add_new_nav_link_development",UPDATE_EXISTING_NAV_LINK_DEVELOPMENT="update_existing_nav_link_development";
	public static final String MAP_MUTUALFUND_LINK_PRODUCTION="map_mfid_link_production",MAP_MUTUALFUND_LINK_DEVELOPMENT="map_mfid_link_development";
	
	public static final String UPDATE_MISSING_NAV_LINK_DEVELOPMENT="update_missing_nav_link_development",UPDATE_MISSING_NAV_LINK_PRODUCTION="update_missing_nav_link_production";
	
	public static final String PDF_NOT_READ="pdf_not_read_dir",PDF_READ="pdf_read_dir",NO_USER_PDF="no_user_for_these_pdf_dir";
	
	public static final String STOCK_PDF_NOT_READ="stock_pdf_not_read_dir",STOCK_PDF_READ="stock_pdf_read_dir",STOCK_NO_USER_PDF="stock_no_user_for_these_pdf_dir";
	public static final String DECRYPTED_PDF_FILE="decrypted_pdf_file_dir"; 
	public static final String CONFIG_TO_KEEP_API_SERVICE_ALIVE_PRODUCTION="config_to_keep_api_service_alive_production",CONFIG_TO_KEEP_API_SERVICE_ALIVE_DEVELOPMENT="config_to_keep_api_service_alive_development";
	public static final String MF_RATING="schemelevelsuggestion_mf_rating",STOCK_RATING="schemelevelsuggestion_stock_rating";
	public static final String DEMO_DIR="demo_dir";
	public Config() throws IOException
	{
		properties=new Properties();
		FileInputStream fis = new FileInputStream(Config.class.getResource("config.prop").getPath());
		properties.load(fis);
	}
	
	public String getProperty(String key)
	{
		return properties.getProperty(key);
	}
}
