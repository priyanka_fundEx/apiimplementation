package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.UserPortfolioState;
import com.fundexpert.exception.FundexpertException;
import com.rebalance.util.AssetUtil;

public class UserPortfolioStateController {

	public List<UserPortfolioState> getUserPortfolioState(long userId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<UserPortfolioState> list=hSession.createQuery("from UserPortfolioState where userId=?").setLong(0,userId).list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	//move each userPortfolioState to ArchiveUserPortfolioState for this user
	//delete each userPortfolioState for this user
	//insert new data into UserPortfolioSate
	public void insertDataIntoUserPortfolioState(long userId) throws FundexpertException
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			
			AssetUtil au=new AssetUtil(userId,hSession);
			Map<Long,Double> map=au.getMutualFundAssetAllocation();
			Map<Integer,Double> stockMap=au.getStockAssetAllocation();
			Map<Long,String> mfBroaderSegment=au.getMfBroaderSegmentMap();
			Map<Long,String> mfBroaderSubSegment=au.getMfBroaderSubSegmentMap();
			Map<Integer,String> stockSegment=au.getStockSegmentMap();
			Map<Integer,String> stockSubSegment=au.getStockSubSegmentMap();
			Map<Long,String> mfNameMap=au.getMfNameMap();
			Map<Integer,String> stockNameMap=au.getStockNameMap();
			double mfTotalPortfolioValue=au.getCurrentTotalMutualFundValue();
			double stockTotalPortfolioValue=au.getCurrentTotalStocksValue();
			double totalValue=au.getCurrentTotalPortfolioValue();
			double sumOfIndivdualMfAllocation=0,sumOfIndividualStockAllocation=0,sumOfIndividualAllAllocation=0;
			
			UserPortfolioState ups=null;
			tx=hSession.beginTransaction();
			for(Map.Entry<Long,Double> m:map.entrySet())
			{
				long mfId=m.getKey();
				double value=m.getValue();
				if(value==0)
					continue;
				sumOfIndivdualMfAllocation+=(value/mfTotalPortfolioValue)*100;
				sumOfIndividualAllAllocation+=(value/totalValue)*100;
				System.out.println("mfId="+mfId+"  value="+value+" mflevelAllocation="+(value/mfTotalPortfolioValue)+"  totalAllocation="+(value/totalValue));

				ups=new UserPortfolioState();
				ups.setAssetId(mfId);
				ups.setAssetLevelAllocation((value/mfTotalPortfolioValue)*100);
				ups.setAssetType(Asset.MUTUALFUND_ASSET_TYPE);
				ups.setCurrentAssetValue(value);
				ups.setLastUpdatedOn(new Date());
				ups.setLiability(false);
				ups.setPortfolioLevelAllocation((value/totalValue)*100);
				ups.setSegment(mfBroaderSegment.get(mfId));
				ups.setSubSegment(mfBroaderSubSegment.get(mfId));
				ups.setUserId(userId);
				ups.setAssetName(mfNameMap.get(mfId));
				hSession.save(ups);
			}
			tx.commit();
			tx=hSession.beginTransaction();
			System.out.println("===================================Sum of Mf portfolio = "+au.getCurrentTotalMutualFundValue()+"  sum of percentage of mflevelAllocation="+sumOfIndivdualMfAllocation);
			for(Map.Entry<Integer, Double> m:stockMap.entrySet())
			{
				long scCode=m.getKey();
				double value=m.getValue();
				if(value==0)
					continue;
				sumOfIndividualStockAllocation+=(value/stockTotalPortfolioValue)*100;
				sumOfIndividualAllAllocation+=(value/totalValue)*100;
				System.out.println("scCode="+scCode+"  value="+value+" stocklevelAllocation="+(value/stockTotalPortfolioValue)+"  totalAllocation="+(value/totalValue));
			
				ups=new UserPortfolioState();
				ups.setAssetId(scCode);
				ups.setAssetLevelAllocation((value/stockTotalPortfolioValue)*100);
				ups.setAssetType(Asset.STOCK_ASSET_TYPE);
				ups.setCurrentAssetValue(value);
				ups.setLastUpdatedOn(new Date());
				ups.setLiability(false);
				ups.setPortfolioLevelAllocation((value/totalValue)*100);
				ups.setUserId(userId);
				ups.setSegment(stockSegment.get((int)scCode));
				ups.setSubSegment(stockSubSegment.get((int)scCode));
				ups.setAssetName(stockNameMap.get((int)scCode));
				hSession.save(ups);
			}
			tx.commit();
			System.out.println("===================================Sum of Stock portfolio = "+au.getCurrentTotalStocksValue()+"   sum of percentage of stocklevelAllocation="+sumOfIndividualStockAllocation);
			System.out.println("===================================Sum of All portfolio = "+au.getCurrentTotalPortfolioValue()+"  sum of mf+stock out of portfolio Allocation= "+sumOfIndividualAllAllocation);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			throw new FundexpertException("Not able to perform SchemeLevelSuggestion.");
		}
		finally
		{
			hSession.close();
		}
	}
	
	//move each userPortfolioState to ArchiveUserPortfolioState for this user
	//delete each userPortfolioState for this user
	//insert new data into UserPortfolioSate
	public List<UserPortfolioState> getUserPortfolioStateList(long userId) throws FundexpertException
	{
		Session hSession=null;
		List<UserPortfolioState> list=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			list=new ArrayList<UserPortfolioState>();
			AssetUtil au=new AssetUtil(userId,hSession);
			Map<Long,Double> map=au.getMutualFundAssetAllocation();
			Map<Integer,Double> stockMap=au.getStockAssetAllocation();
			Map<Long,String> mfBroaderSegment=au.getMfBroaderSegmentMap();
			Map<Long,String> mfBroaderSubSegment=au.getMfBroaderSubSegmentMap();
			Map<Integer,String> stockSegment=au.getStockSegmentMap();
			Map<Integer,String> stockSubSegment=au.getStockSubSegmentMap();
			Map<Long,String> mfNameMap=au.getMfNameMap();
			Map<Integer,String> stockNameMap=au.getStockNameMap();
			double mfTotalPortfolioValue=au.getCurrentTotalMutualFundValue();
			double stockTotalPortfolioValue=au.getCurrentTotalStocksValue();
			double totalValue=au.getCurrentTotalPortfolioValue();
			double sumOfIndivdualMfAllocation=0,sumOfIndividualStockAllocation=0,sumOfIndividualAllAllocation=0;
			
			UserPortfolioState ups=null;
			for(Map.Entry<Long,Double> m:map.entrySet())
			{
				long mfId=m.getKey();
				double value=m.getValue();
				if(value==0)
					continue;
				sumOfIndivdualMfAllocation+=(value/mfTotalPortfolioValue)*100;
				sumOfIndividualAllAllocation+=(value/totalValue)*100;
				System.out.println("mfId="+mfId+"  value="+value+" mflevelAllocation="+(value/mfTotalPortfolioValue)+"  totalAllocation="+(value/totalValue));

				ups=new UserPortfolioState();
				ups.setAssetId(mfId);
				ups.setAssetLevelAllocation((value/mfTotalPortfolioValue)*100);
				ups.setAssetType(Asset.MUTUALFUND_ASSET_TYPE);
				ups.setCurrentAssetValue(value);
				ups.setLastUpdatedOn(new Date());
				ups.setLiability(false);
				ups.setPortfolioLevelAllocation((value/totalValue)*100);
				ups.setSegment(mfBroaderSegment.get(mfId));
				ups.setSubSegment(mfBroaderSubSegment.get(mfId));
				ups.setUserId(userId);
				ups.setAssetName(mfNameMap.get(mfId));
				list.add(ups);
			}
			System.out.println("===================================Sum of Mf portfolio = "+au.getCurrentTotalMutualFundValue()+"  sum of percentage of mflevelAllocation="+sumOfIndivdualMfAllocation);
			for(Map.Entry<Integer, Double> m:stockMap.entrySet())
			{
				long scCode=m.getKey();
				double value=m.getValue();
				if(value==0)
					continue;
				sumOfIndividualStockAllocation+=(value/stockTotalPortfolioValue)*100;
				sumOfIndividualAllAllocation+=(value/totalValue)*100;
				System.out.println("scCode="+scCode+"  value="+value+" stocklevelAllocation="+(value/stockTotalPortfolioValue)+"  totalAllocation="+(value/totalValue));
			
				ups=new UserPortfolioState();
				ups.setAssetId(scCode);
				ups.setAssetLevelAllocation((value/stockTotalPortfolioValue)*100);
				ups.setAssetType(Asset.STOCK_ASSET_TYPE);
				ups.setCurrentAssetValue(value);
				ups.setLastUpdatedOn(new Date());
				ups.setLiability(false);
				ups.setPortfolioLevelAllocation((value/totalValue)*100);
				ups.setUserId(userId);
				ups.setSegment(stockSegment.get((int)scCode));
				ups.setSubSegment(stockSubSegment.get((int)scCode));
				ups.setAssetName(stockNameMap.get((int)scCode));
				list.add(ups);
			}
			System.out.println("===================================Sum of All portfolio = "+au.getCurrentTotalPortfolioValue()+"  sum of mf+stock out of portfolio Allocation= "+sumOfIndividualAllAllocation);
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new FundexpertException("Not able to get User's portfolio.");
		}
		finally
		{
			hSession.close();
		}
	}
}
