package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge; 
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.User;

public class LumpsumInvestController {

	public User getRiskAppetiteOfUser(String userId)
	{
		User user=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			user=(User) session.createQuery("from User where userId=?").setString(0, userId).uniqueResult();
			return user;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}		
		return user;
	}

	public List<MutualFund> get5StarFundsOfEquity() 
	{
		List<MutualFund> list=new ArrayList<MutualFund>();
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			list=session.createQuery("from MutualFund where category=? and ourRating=?").setInteger(0, 1).setInteger(1, 5).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}		
		return list;
	}

	public List<MutualFund> get5StartFundsOfDEBT()
	{
		List<MutualFund> list=new ArrayList<MutualFund>();
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			list=session.createQuery("from MutualFund where category=? and ourRating=?").setInteger(0, 3).setInteger(1, 5).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}		
		return list;
	}
}