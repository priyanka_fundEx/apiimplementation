package com.fundexpert.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.ArchivePortfolioRequest;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.PortfolioRequest;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.exception.ValidationException;
import com.fundexpert.util.Verify;

public class PortfolioRequestController {

	
	public PortfolioRequest persistRequest(Long userId,String password) throws ValidationException,Exception
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			Calendar cal=Calendar.getInstance();
			hSession=HibernateBridge.getSessionFactory().openSession();
			
			tx=hSession.beginTransaction();
			PortfolioRequest pr=(PortfolioRequest)hSession.createQuery("from PortfolioRequest where userId=?").setLong(0, userId).setMaxResults(1).uniqueResult();
			if(pr==null)
			{
				pr=new PortfolioRequest();
				System.out.println("%$#@!Cal.getTime() while saving PortfolioRequest="+cal.getTime());
				pr.setRequestOn(cal.getTime());
				//submitted is setToTrue only when button is clicked on CamsBot
				pr.setSubmitted(false);
				pr.setUserId(userId);
				pr.setPassword(password);
				pr.setCounter(1);
				if(cal.get(Calendar.HOUR_OF_DAY)>21)
					pr.setIsKarvyRequest(true);
				else
					pr.setIsKarvyRequest(false);
				
			}
			else
			{
				pr.setRequestOn(cal.getTime());
				pr.setSubmitted(false);
				pr.setCounter(pr.getCounter()+1);
				if(cal.get(Calendar.HOUR_OF_DAY)>21)
					pr.setIsKarvyRequest(true);
				else
					pr.setIsKarvyRequest(false);
			}
			hSession.save(pr);
			tx.commit();
			return pr;
		}
		catch(Exception e)
		{
			if(tx!=null)
				tx.rollback();
			e.printStackTrace();
		}
		finally
		{
			if(hSession!=null)
				hSession.close();
		}
		return null;
	}

	public boolean persistArchivePortfolioRequest(PortfolioRequest pr)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			ArchivePortfolioRequest apr=new ArchivePortfolioRequest();
			apr.setPassword(pr.getPassword());
			apr.setUserId(pr.getUserId());
			apr.setRequestOn(pr.getRequestOn());
			hSession.save(apr);
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally{
			if(hSession.isConnected())
				hSession.close();
		}
		return false;
	}

	public void persistRequestWithPasswordChange(long userId,String password)
	{

		Session hSession=null;
		Transaction tx=null;
		try
		{
			Calendar cal=Calendar.getInstance();
			hSession=HibernateBridge.getSessionFactory().openSession();
			
			tx=hSession.beginTransaction();
			PortfolioRequest pr=(PortfolioRequest)hSession.createQuery("from PortfolioRequest where userId=?").setLong(0, userId).setMaxResults(1).uniqueResult();
			if(pr==null)
			{
				pr=new PortfolioRequest();
				System.out.println("%$#@!Cal.getTime() while saving PortfolioRequest="+cal.getTime());
				pr.setRequestOn(cal.getTime());
				//submitted is setToTrue only when button is clicked on CamsBot
				pr.setSubmitted(true);
				pr.setUserId(userId);
				pr.setPassword(password);
				pr.setCounter(1);
				if(cal.get(Calendar.HOUR_OF_DAY)>21)
					pr.setIsKarvyRequest(true);
				else
					pr.setIsKarvyRequest(false);
				
			}
			else
			{
				pr.setRequestOn(cal.getTime());
				pr.setSubmitted(true);
				pr.setPassword(password);
				pr.setCounter(pr.getCounter()+1);
				if(cal.get(Calendar.HOUR_OF_DAY)>21)
					pr.setIsKarvyRequest(true);
				else
					pr.setIsKarvyRequest(false);
			}
			hSession.save(pr);
			tx.commit();
			
		}
		catch(Exception e)
		{
			if(tx!=null)
				tx.rollback();
			e.printStackTrace();
		}
		finally
		{
			if(hSession!=null)
				hSession.close();
		}
		
	
	}

	public boolean SendDataTOFe(Long userId)
	{
		Session hSession=null;
		boolean check = false;
		OutputStreamWriter wr=null;
		BufferedReader br =null;
		try 
		{
			JSONObject json_response= new JSONObject();
			hSession=HibernateBridge.getSessionFactory().openSession();
			PortfolioRequest pr=(PortfolioRequest)hSession.createQuery("from PortfolioRequest where userId=?").setLong(0, userId).setMaxResults(1).uniqueResult();
			if(pr!=null) 
			{
				User user=(User)hSession.createQuery("from User where  id=?").setLong(0, userId).setMaxResults(1).uniqueResult();
				
				if(user!=null) 
				{				
					json_response.put("emailId", user.getEmail());
					json_response.put("Counter", pr.getCounter());
					json_response.put("Id", pr.getId());
					json_response.put("IsKarvyRequest", pr.getIsKarvyRequest());
					//json_response.put("IsStock", pr.getIsStock());
					json_response.put("Password", pr.getPassword());
					json_response.put("RequestOn", pr.getRequestOn());
					json_response.put("Submitted", pr.getSubmitted());
					//json_response.put("User", pr.getUser());
					json_response.put("UserId", pr.getUserId());
					json_response.put("ConsumerId", user.getConsumerId());
					Consumer consumer=(Consumer)hSession.createQuery("from Consumer where  id=?").setLong(0,user.getConsumerId()).setMaxResults(1).uniqueResult();
					if(consumer!=null) 
					{	
						json_response.put("AppId", consumer.getAppId());	
					}																
			        URL url = new URL("https://www.fundexpert.in/api1/app/portfoliorequest");
					HttpURLConnection con = (HttpURLConnection)url.openConnection();
					con.setDoOutput(true);
					con.setDoInput(true);
					con.setRequestProperty("Content-Type", "application/json");
					con.setRequestProperty("Accept", "application/json");
					con.setRequestMethod("POST");
					wr = new OutputStreamWriter(con.getOutputStream());
					wr.write(json_response.toString());
					wr.flush();
					//System.out.println("after url");
					StringBuilder responseSuccess = new StringBuilder();
				    br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				    String line;
				    while ( (line = br.readLine()) != null)
				    	responseSuccess.append(line);
				    JSONObject jsonObject =  new JSONObject(responseSuccess.toString());
				    System.out.println("Ouput returned after submitting data to api build on fundexpert = "+jsonObject.toString());
				    if(jsonObject.getBoolean("success")==true) 
				    {
				    	check =  true;				    	
				    }
				    else 
				    {
				    	check =  false;	
				    }
				 }
				else
				{
					System.out.println(" user null");
					check =  false;
				}										
			}			
			else 
			{
				System.out.println("null");
				check =  false;
			}						
			return check;
		}		
		catch(Exception e)
		{
			e.printStackTrace();	
			return check;
		}		
		finally
		{
			try {
				if(hSession!=null)
					hSession.close();
				if(br!=null)
					br.close();
				if(wr!=null)
					wr.close();	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
