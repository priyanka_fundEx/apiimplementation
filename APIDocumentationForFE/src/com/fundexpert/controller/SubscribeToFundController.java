package com.fundexpert.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.validation.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.MutualFundSubscription;
import com.fundexpert.dao.User;

public class SubscribeToFundController
{

	public boolean validUser(Long consumerId, String userId)
	{
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			User user = (User) session.createQuery("from User where userId=? and consumerId=?").setString(0, userId).setLong(1, consumerId).uniqueResult();
			if(user != null)
				return true;
			return false;
		}
		catch (ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return false;
	}

	public Boolean subscribeToFund(String userId, String amfiiCode) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Calendar cal = Calendar.getInstance();
		String str = sdf.format(cal.getTime());
		java.util.Date d = sdf.parse(str);
		java.sql.Date dateDB = new java.sql.Date(d.getTime());

		Session session = HibernateBridge.getSessionFactory().openSession();
		Transaction tx = null;
		try
		{
			tx = session.beginTransaction();
			
			//session.createQuery("from Holding where )
			
			MutualFund mf = (MutualFund) session.createQuery("from MutualFund where amfiiCode=?").setString(0, amfiiCode).setMaxResults(1).uniqueResult();
			User user = (User) session.createQuery("from User where userId=?").setString(0, userId).uniqueResult();

			MutualFundSubscription mfs = new MutualFundSubscription();
			mfs.setMutualfundId(mf.getId());
			mfs.setUserId(user.getId());
			mfs.setStartDate(dateDB);
			session.save(mfs);
			tx.commit();
			System.out.println("Mutualfund Subscribed Successfully.");
			return true;
		}
		catch (ValidationException e)
		{
			tx.rollback();
			System.out.println("validation Exception." + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return false;
	}

	public Boolean alreadySubscribed(Long mutualfundId, Long userID, Long consumerId)
	{
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			MutualFundSubscription mfs = (MutualFundSubscription) session.createQuery("from MutualFundSubscription where mutualfundId=? and userId=? ").setLong(0, mutualfundId).setLong(1, userID).uniqueResult();
			if(mfs != null)
			{ 
				return true;
			}
			else
				System.out.println("3 You are subscribed to this fund");
			return false;
		}
		catch (ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return false;
	}


	public Long getMutualfundID(String amfiiCode)
	{
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			System.out.println("Hello,,");
			MutualFund mf = (MutualFund) session.createQuery("from MutualFund where amfiiCode=? ").setString(0, amfiiCode).uniqueResult();
			if(mf != null)
				return mf.getId();
			System.out.println("Hellloooooo");
			return null;
		}
		catch (ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return null;
	}
}