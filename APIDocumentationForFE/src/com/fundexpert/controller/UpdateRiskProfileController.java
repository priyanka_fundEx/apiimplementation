package com.fundexpert.controller;

import java.util.Calendar;

import javax.validation.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Sessionn;
import com.fundexpert.dao.User;

public class UpdateRiskProfileController 
{
	public Boolean updateUser(String userId, String sessionId, int riskAppetite) 
	{
		Calendar nowCal=Calendar.getInstance();
		
		Session session=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=null;
		try{
			tx=session.beginTransaction();
			Sessionn sess=(Sessionn) session.createQuery("from Sessionn where sessionId=? order by id DESC")
					.setString(0, sessionId).setMaxResults(1).uniqueResult();
			
			User user=(User)session.createQuery("from User where userId=?").setString(0, userId).uniqueResult(); 
			user.setRiskAppetite(riskAppetite); 
			session.update(user); 
			
			tx.commit();
			return true;
		}
		catch(ValidationException e)
		{
			tx.rollback();
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return false;
	}
}
