package com.fundexpert.controller;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.util.NavStatus;
import com.fundexpert.util.SerializedObjectToFile;
import com.fundexpert.config.Config;
import com.fundexpert.dao.Nav;

public class NavController {

	public JSONArray getMissedNavJson(Date startDate)
	{
		Session hSession=null;
		try
		{
			JSONObject jsonObject=null;
			JSONArray jsonArray=new JSONArray();
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Object> idList=hSession.createQuery("select n.amfiiCode,max(n.tradeDate) from Nav n group by n.amfiiCode").list();
			
			long amfiiCode;
			Date maxDate;
			//Date sDate=null;
			Iterator itr=idList.iterator();
			while(itr.hasNext())
			{
				Object[] obj=(Object[])itr.next();
				amfiiCode=(Long)obj[0];
				maxDate=(Date)obj[1];
				jsonObject=new JSONObject();
				//System.out.println(sDate);
				if(startDate==null)
				{
					jsonObject.put("sDate",new SimpleDateFormat("yyyy-MM-dd").format(maxDate));
				}
				else
				{
					jsonObject.put("sDate",new SimpleDateFormat("yyyy-MM-dd").format(startDate));
				}
				jsonObject.put("amfiiCode", amfiiCode);
				//jsonObject.put("eDate",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				jsonArray.put(jsonObject);
			} 

			//System.out.println("input JSONArray"+jsonArray.toString(1));
			return jsonArray;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public boolean updateNav(Date startDate) throws Exception
	{
		StringBuilder builder=null;
		//String input = "[{\"amfiiCode\":\"125305\",\"sDate\":\"2017-09-09\",\"eDate\":\"2017-09-30\"}]";
					
		JSONObject errorObject=null;

		JSONArray jsonArray=null;
		JSONObject inputObject=new JSONObject();
		JSONArray inputJsonArray=null;
		SerializedObjectToFile sf=new SerializedObjectToFile();
		NavStatus navStatus=null;
		Map<String,String> updateExistingNavMap=null;
		File updateExistingNavFile=new File("/opt/feapi/Nav/navSyncStatus.txt");
		File dir=new File("/opt/feapi/Nav");
		String todayDate=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
		try
		{
			if(!dir.isDirectory())
			{
				dir.mkdirs();
			}
			if(!updateExistingNavFile.exists())
			{
				updateExistingNavFile.createNewFile();
			}
			jsonArray=getMissedNavJson(startDate);
			if(jsonArray==null)
				throw new Exception("Not able to get inputJson for api call.");
			System.out.println("Total Sending JSON Array size = "+jsonArray.length());
			for(int i=0;i<jsonArray.length();)
			{ 	
				if((i+500)<jsonArray.length())
				{
					i+=500;
				}
				else
					i+=jsonArray.length();
				inputJsonArray=new JSONArray();
				//System.out.println("j="+0+"i="+i);
				for(int k=0;k<i;k++)
				{
					JSONObject json=jsonArray.getJSONObject(0);
					jsonArray.remove(0);
					inputJsonArray.put(json);
				}
				i=0;
				inputObject.put("navArray", inputJsonArray);
				
				Config config=new Config();
				String path="";
				if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
				{
					path=config.getProperty(Config.UPDATE_EXISTING_NAV_LINK_PRODUCTION);
				}
				else
				{
					path=config.getProperty(Config.UPDATE_EXISTING_NAV_LINK_DEVELOPMENT);
				}
				
				URL url = new URL(path);
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				//System.out.println("After opening connection to URL to update nav's.");
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(inputObject.toString());
				writer.close(); 
				//System.out.println("Sending nav data : as "+inputObject.toString(1));
				System.out.println("Sending nav data size : "+inputJsonArray.length());
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
			
				builder=new StringBuilder();
				while(line!=null)
				{
					builder.append(line);
					line=br.readLine();
				}
				br.close();
				JSONObject receivedJson=new JSONObject(builder.toString());
				//System.out.println("Received JSON="+receivedJson.toString(1));
				System.out.println("Received JSON="+receivedJson.getJSONArray("data").length());
				if(receivedJson.has("success") && receivedJson.getBoolean("success")==true)
				{
					boolean insertSuccessfully=insertNav(receivedJson.getJSONArray("data"));
					if(insertSuccessfully==false)
						throw new Exception("Something went wrong while saving nav's.");
				}
				else
				{
					throw new Exception("Something Went Wrong while fetching data.");
				}
			} 
			try
			{
				navStatus=(NavStatus)sf.getSerializedFile(updateExistingNavFile);
			}
			catch(EOFException e)
			{
				e.printStackTrace();
				navStatus=new NavStatus();
			}
			
			updateExistingNavMap=navStatus.getUpdateExistingNavMap();
			updateExistingNavMap.put(todayDate, "true");
			sf.serializeObject(navStatus, updateExistingNavFile);
			System.out.println("Nav Updation Done.");
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				navStatus=(NavStatus)sf.getSerializedFile(updateExistingNavFile);
			}
			catch(EOFException e1)
			{
				e1.printStackTrace();
				navStatus=new NavStatus();
			}
			
			updateExistingNavMap=navStatus.getUpdateExistingNavMap();
			updateExistingNavMap.put(todayDate, "false|"+e.getMessage());
			sf.serializeObject(navStatus, updateExistingNavFile);
			throw e;
		}
	}
	
	public boolean addNewNav() throws Exception
	{
		Session hSession=null;
		JSONArray jsonArray=null;
		JSONArray inputJsonArray=null;
		JSONObject inputObject=null;
		Map<String,Object> resp=null;
		SerializedObjectToFile sf=new SerializedObjectToFile();
		NavStatus navStatus=null;
		Map<String,String> addNewNavMap=null;
		File addNewNavFile=new File("/opt/feapi/Nav/navSyncStatus.txt");
		File dir=new File("/opt/feapi/Nav");
		String todayDate=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
		try
		{
			if(!dir.isDirectory())
			{
				dir.mkdirs();
			}
			if(!addNewNavFile.exists())
			{
				addNewNavFile.createNewFile();
			}
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> amfiiCodeList=hSession.createQuery("select distinct amfiiCode from Nav").list();
			if((amfiiCodeList!=null && amfiiCodeList.size()==0) || amfiiCodeList==null)
				amfiiCodeList.add(1l);
			jsonArray=new JSONArray();
			for(Long amfii:amfiiCodeList)
			{
				jsonArray.put(amfii);
				//System.out.print(","+amfii);
			}
			System.out.println("AmfiiCode JSONArray size() : "+jsonArray.length());
			inputObject=new JSONObject();
			inputObject.put("navArray", jsonArray);
			//System.out.println(jsonArray.toString(1));
			if(jsonArray==null)
				throw new Exception("Not able to get inputJson for api call.");
			
			Config config=new Config();
			String path="";
			if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
			{
				path=config.getProperty(Config.ADD_NEW_NAV_LINK_PRODUCTION);
			}
			else
			{
				path=config.getProperty(Config.ADD_NEW_NAV_LINK_PRODUCTION);
			}
			URL url = new URL(path);
			URLConnection con=url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(500000);
			con.setReadTimeout(500000);
			
			//send data
			OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
			writer.write(inputObject.toString());
			writer.close();
			
			//received data
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line=br.readLine();
			StringBuilder builder=new StringBuilder();
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			br.close();
			//JSONObject json=new JSONObject(builder.toString());
			
			JSONObject receivedJson=new JSONObject(builder.toString());
			//System.out.println("Received JSON="+receivedJson.toString(1));
			System.out.println("Received JSONArray size : "+receivedJson.getJSONArray("data").length());
			if(receivedJson.has("success") && receivedJson.getBoolean("success")==true)
			{
				boolean insertSuccessfully=insertNav(receivedJson.getJSONArray("data"));
				if(insertSuccessfully==false)
					throw new Exception("Something went wrong while saving nav's.");
			}
			else
			{
				throw new Exception("Something Went Wrong while fetching data.");
			}
			try
			{
				navStatus=(NavStatus)sf.getSerializedFile(addNewNavFile);
			}
			catch(EOFException e)
			{
				e.printStackTrace();
				navStatus=new NavStatus();
			}
			addNewNavMap=navStatus.getAddNewNavMap();
			addNewNavMap.put(todayDate, "true");
			sf.serializeObject(navStatus, addNewNavFile);
			System.out.println("Inserting New Nav done.");
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			try
			{
				navStatus=(NavStatus)sf.getSerializedFile(addNewNavFile);
			}
			catch(EOFException e1)
			{
				e1.printStackTrace();
				navStatus=new NavStatus();
			}
			addNewNavMap=navStatus.getAddNewNavMap();
			addNewNavMap.put(todayDate, "false|"+e.getMessage());
			sf.serializeObject(navStatus, addNewNavFile);
			throw e;
		}
	}
	
	public boolean insertNav(JSONArray jsonArray)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Iterator itr=jsonArray.iterator();
			while(itr.hasNext())
			{
				JSONObject jsonObject=(JSONObject)itr.next();
				
				long amfiiCode=jsonObject.getLong("amfiiCode");
				//System.out.print("Amfii-"+amfiiCode+"/ ");
				JSONArray navArray=jsonObject.getJSONArray("nav");
				Iterator itr1=navArray.iterator();
				tx=hSession.beginTransaction();
				while(itr1.hasNext())
				{
					JSONObject json=(JSONObject)itr1.next();
					double price=json.getDouble("nav");
					String date=json.getString("date");
					Date navDate=new SimpleDateFormat("yyyy-MM-dd").parse(date);
					//System.out.println("Date="+navDate+"Price="+price+" amfiiCode = "+amfiiCode);
					Nav nav=new Nav();
					nav.setAmfiiCode(amfiiCode);
					nav.setNav(price);
					nav.setTradeDate(navDate);
					hSession.saveOrUpdate(nav);
				}
				tx.commit();
			}
			System.out.println("");
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			return false;
		}
		finally
		{
			if(hSession.isOpen())
				hSession.close();
		}
	}

	/*
	 * INPUT:
	 * {
	 * 	"data":[{"amfii":123,"date":"2012-03-04"},{},{}]
	 * }
	 * 
	 * OUTPUT:
		{
			success:true
			allData:[
				{
					"amfii"=123,
					"data":[{"nav":12,"date":"2012-03-04"},{nav:12,date:'2012-03-05'}]	
				},
				{					
					"amfii"=1231,
					"data":[{"nav":12,"date":"12-01-2000"},{}]	
				}
			]
		}
	 */	
	public JSONObject getNavData(JSONObject data) {
		
		Session hSession=null;
		JSONArray dataArray=null;
		JSONObject dataObject=null;
		JSONArray allDataArray=new JSONArray();
		JSONObject allDataObject=null;
		JSONObject mainObject=new JSONObject();
		JSONArray receivedArray=data.getJSONArray("data");
		long amfiiCode=0;
		Date date=null;
		List<Nav> navList=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Iterator itr=receivedArray.iterator();
			while(itr.hasNext())
			{
				data=(JSONObject)itr.next();
				amfiiCode=data.getLong("amfiiCode");
				
				//System.out.print("AmfiiCode="+amfiiCode);
				//sDate will always be there because we are updating existing funds
				date=new SimpleDateFormat("yyyy-MM-dd").parse(data.getString("sDate"));
				
				navList=hSession.createQuery("from Nav where amfiiCode=? and tradeDate>=? order by tradeDate").setLong(0, amfiiCode).setDate(1, date).list();
				if(navList!=null && navList.size()>0)
				{
					dataArray=new JSONArray();
					Iterator itr1=navList.iterator();
					while(itr1.hasNext())
					{
						dataObject=new JSONObject();
						Nav nav=(Nav)itr1.next();
						dataObject.put("date", new SimpleDateFormat("yyyy-MM-dd").format(nav.getTradeDate()));
						dataObject.put("nav",nav.getNav());
						dataArray.put(dataObject);
					}
					allDataObject=new JSONObject();
					allDataObject.put("amfii",amfiiCode);
					allDataObject.put("data",dataArray);
				}
				if(allDataObject!=null)
				{
					allDataArray.put(allDataObject);
				}
				//important instruction below
				allDataObject=null;
			} 
			
			System.out.println();
			mainObject.put("success", true);
			mainObject.put("allNavData", allDataArray);
			
			return mainObject;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			mainObject.put("success", false);
			mainObject.put("error", "Something went wrong");
			return mainObject;
		}
	}
	
	public JSONObject getNewNavData(JSONObject json)
	{
		Session hSession=null;
		JSONArray receivedArray=json.getJSONArray("data");
		long amfiiCode=0;
		Date date=null;
		List<Nav> navList=null;
		List<Long> amfiiCodeList=new ArrayList<Long>();
		JSONArray dataArray=null;
		JSONObject dataObject=null;
		JSONArray allDataArray=new JSONArray();
		JSONObject allDataObject=null;
		JSONObject mainObject=new JSONObject();
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Iterator itr=receivedArray.iterator();
			while(itr.hasNext())
			{
				amfiiCode=(Integer)itr.next();
				amfiiCodeList.add(amfiiCode);
				//System.out.print("AmfiiCode="+amfiiCode);
			} 
			
			Criteria criteria = hSession.createCriteria(Nav.class).setProjection(Projections.projectionList()
					.add(Projections.groupProperty("amfiiCode")));
			criteria.add(Restrictions.not(Restrictions.in(
					"amfiiCode", amfiiCodeList)));
			criteria.addOrder(Order.desc("amfiiCode"));
			
			amfiiCodeList=criteria.list();
			itr=amfiiCodeList.iterator();
			while(itr.hasNext())
			{
				amfiiCode=(Long)itr.next();
				navList=hSession.createQuery("from Nav where amfiiCode=? order by tradeDate").setLong(0, amfiiCode).list();
				if(navList!=null && navList.size()>0)
				{
					dataArray=new JSONArray();
					Iterator itr1=navList.iterator();
					while(itr1.hasNext())
					{
						dataObject=new JSONObject();
						Nav nav=(Nav)itr1.next();
						dataObject.put("date", new SimpleDateFormat("yyyy-MM-dd").format(nav.getTradeDate()));
						dataObject.put("nav",nav.getNav());
						dataArray.put(dataObject);
					}
					allDataObject=new JSONObject();
					allDataObject.put("amfii",amfiiCode);
					allDataObject.put("data",dataArray);
				}
				if(allDataObject!=null)
				{
					allDataArray.put(allDataObject);
				}
				//important instruction below to set to null
				allDataObject=null;
			}
			
			System.out.println();
			mainObject.put("success", true);
			mainObject.put("newNavData", allDataArray);
			
			return mainObject;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			mainObject.put("success", false);
			mainObject.put("error", "Something went wrong");
			return mainObject;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public boolean updateMissingNav() throws Exception
	{
		StringBuilder builder=null;
					
		JSONObject errorObject=null;

		JSONArray jsonArray=null;
		JSONObject inputObject=new JSONObject();
		JSONArray inputJsonArray=null;
		try
		{
			jsonArray=getNavCounterJson();
			if(jsonArray==null)
				throw new Exception("Not able to get inputJson for api call.");
			System.out.println("Total Sending JSON Array size = "+jsonArray.length());
			for(int i=0;i<jsonArray.length();)
			{ 	
				if((i+500)<jsonArray.length())
				{
					i+=500;
				}
				else
					i+=jsonArray.length();
				inputJsonArray=new JSONArray();
				System.out.println("j="+0+"i="+i);
				for(int k=0;k<i;k++)
				{
					JSONObject json=jsonArray.getJSONObject(0);
					jsonArray.remove(0);
					inputJsonArray.put(json);
				}
				i=0;
				inputObject.put("navArray", inputJsonArray);
				
				Config config=new Config();
				String path="";
				if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
				{
					path=config.getProperty(Config.UPDATE_MISSING_NAV_LINK_PRODUCTION);
				}
				else
				{
					path=config.getProperty(Config.UPDATE_MISSING_NAV_LINK_DEVELOPMENT);
				}
				
				URL url = new URL(path);
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				//System.out.println("After opening connection to URL to update nav's.");
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(inputObject.toString());
				writer.close(); 
				//System.out.println("Sending nav data : as "+inputObject.toString(1));
				System.out.println("Sending nav data size : "+inputJsonArray.length());
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
			
				builder=new StringBuilder();
				while(line!=null)
				{
					builder.append(line);
					line=br.readLine();
				}
				br.close();
				JSONObject receivedJson=new JSONObject(builder.toString());
				//System.out.println("Received JSON="+receivedJson.toString(1));
				System.out.println("Received JSON="+receivedJson.getJSONArray("data").length());
				if(receivedJson.has("success") && receivedJson.getBoolean("success")==true)
				{
					boolean insertSuccessfully=insertNav(receivedJson.getJSONArray("data"));
					if(insertSuccessfully==false)
						throw new Exception("Something went wrong while saving nav's.");
				}
				else
				{
					throw new Exception("Something Went Wrong while fetching data.");
				}
			} 
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}
	
	public JSONArray getNavCounterJson()
	{
		Session hSession=null;
		try
		{
			JSONObject jsonObject=null;
			JSONArray jsonArray=new JSONArray();
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Object> idList=hSession.createQuery("select n.amfiiCode,count(*) from Nav n group by n.amfiiCode").list();
			
			long amfiiCode;
			long count;
			Iterator itr=idList.iterator();
			while(itr.hasNext())
			{
				Object[] obj=(Object[])itr.next();
				amfiiCode=(Long)obj[0];
				count=(Long)obj[1];
				jsonObject=new JSONObject();
				jsonObject.put("count", count);
				jsonObject.put("amfiiCode", amfiiCode);
				jsonArray.put(jsonObject);
			} 

		//	System.out.println("input JSONArray"+jsonArray.toString(1));
			return jsonArray;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
}
