package com.fundexpert.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.ArchiveHolding;
import com.fundexpert.dao.ArchiveTransaction;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.Transaction;
import com.fundexpert.dao.User;

public class HoldingController {

	public List<Holding> getHoldings(long userId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Holding> holdingList=hSession.createQuery("from Holding where userId=?").setParameter(0, userId).list();
			return holdingList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	public boolean deleteAllHoldings(long userId)
	{
		Session hSession=null;
		org.hibernate.Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			int transactionCount=hSession.createQuery("delete from Transaction where userId=?").setLong(0, userId).executeUpdate();
			System.out.println("Transaction delete count="+transactionCount);
			int holdingCount=hSession.createQuery("delete from Holding where userId=?").setLong(0, userId).executeUpdate();
			System.out.println("Holding delete count="+holdingCount);
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public boolean archiveUserHolding(long userId) throws Exception
	{
		Session hSession=null;
		org.hibernate.Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Holding> userHoldings=hSession.createQuery("from Holding where userId=?").setLong(0, userId).list();
			if(userHoldings==null || userHoldings.size()==0)
				return true;
			List<ArchiveHolding> archiveHolding=hSession.createQuery("from ArchiveHolding where userId=?").setLong(0,userId).list();
			for(ArchiveHolding ah:archiveHolding)
			{
				hSession.delete(ah);
			}
			tx=hSession.beginTransaction();
			tx.commit();
			for(int i=0;i<userHoldings.size();i++)
			{
				Holding h=userHoldings.get(i);
				Set<ArchiveTransaction> archiveSet=new HashSet<ArchiveTransaction>();
				Iterator itr=h.getTransactions().iterator();
				while(itr.hasNext())
				{
					Transaction transaction = (Transaction)itr.next();
					ArchiveTransaction at = new ArchiveTransaction();
					BeanUtils.copyProperties(at, transaction);
					at.setArchivedOn(new Date());
					at.setId(null);//id will be set using generator class
					archiveSet.add(at);
				}
				ArchiveHolding ah=new ArchiveHolding();
				BeanUtils.copyProperties(ah, h);
				ah.setArchiveTransactions(archiveSet);
				ah.setId(null);
				ah.setArchivedOn(new Date());
				hSession.save(ah);
				hSession.delete(h);
				tx=hSession.beginTransaction();
				tx.commit();
			}
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null && tx.isActive())
				tx.rollback();
			throw e;
		}
		finally
		{
			hSession.close();
		}
	}
}
