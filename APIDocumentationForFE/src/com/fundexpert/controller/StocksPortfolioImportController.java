package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fundexpert.dao.PortfolioUpload;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.pojo.Holdings;
import com.fundexpert.pojo.StocksHoldings;
import com.apidoc.util.HibernateBridge;
import com.itextpdf.text.exceptions.BadPasswordException;

public class StocksPortfolioImportController {

	String path="";
	List<StocksHoldings> stocksHoldingsList=null;
	List<Map> missingStocksList=new ArrayList<Map>();
	
	public void sFactory(long userId,List<StocksHoldings> stocksHoldingsList) throws BadPasswordException, Exception
	{
		Session hSession=null;
		Transaction tx=null;
		User user=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			
			user=(User)hSession.get(User.class,userId);
			
			this.stocksHoldingsList = stocksHoldingsList;
			//persisting stocksHoldings
			existingUser(user);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}	
		finally
		{
			hSession.close();
		}
	}
	
	private void existingUser(User user)
	{
		Session hSession=null;
		Transaction tx=null;
		Stocks stocks=null;
		Map missingMap=null;
		
		try
		{
			double marketPrice=0;
			double numberOfShares=0;
			String dematId="",dematName="",stockNameFromPdf="";
			int scCode=0;
			com.fundexpert.dao.StocksHoldings persistStocksHolding=null;
			hSession=HibernateBridge.getSessionFactory().openSession();

			System.out.println(user.getId()+" : userId");
			Set<com.fundexpert.dao.StocksHoldings> existingStocksHoldingsList=user.getStocksHoldings();
			if(existingStocksHoldingsList==null)
			{
				System.out.println("existing holdings list = null");
			}
			else
			{
				System.out.println("existing holdings list size="+existingStocksHoldingsList.size());
			}
			
			Iterator itr=stocksHoldingsList.iterator();
			while(itr.hasNext())
			{
				stocks=null;
				StocksHoldings stocksHolding=(StocksHoldings)itr.next();
				dematId=stocksHolding.getDematId();
				dematName=stocksHolding.getDematName();
				stockNameFromPdf=stocksHolding.getCompanyName();
				stocks=(Stocks)hSession.createQuery("from Stocks where isinCode=?").setParameter(0, stocksHolding.getIsinCode()).setMaxResults(1).uniqueResult();
				boolean foundExistingStocksHoldings=false;
				if(stocks==null)
				{

					//make new stock in stock table only if ISIN contains 'INE'
					//why only 'INE' case is because in case 'INF', those are ETF funds which are also in CAMS,KARVY pdf so they gets hanled there automatcally
					String isinRegex="((INE)[0-9A-Z]{9})";
					if(stocksHolding.getIsinCode().matches(isinRegex))
					{
						System.out.println("Saving stock with isin = "+stocksHolding.getIsinCode());
						Integer id=(Integer)hSession.createQuery("select max(scCode) from Stocks where scCode<400000").setMaxResults(1).uniqueResult();
						if(id==null)
							id=1;
						else
							id+=1;
						stocks=new Stocks();
						stocks.setScCode(id);
						stocks.setScName(stocksHolding.getCompanyName());
						stocks.setIsinCode(stocksHolding.getIsinCode());
						tx=hSession.beginTransaction();
						hSession.save(stocks);
						tx.commit();
					}
				}
				if(stocks==null)
				{
					missingMap=new HashMap();
					missingMap.put("isin", stocksHolding.getIsinCode());
					missingMap.put("company",stocksHolding.getCompanyName());
					missingMap.put("dematId",dematId);
					missingStocksList.add(missingMap);
					System.out.println("missing stock="+stocksHolding.getCompanyName()+"  isin="+stocksHolding.getIsinCode());
				
				}
				else
				{
					tx=hSession.beginTransaction();
					System.out.println("Found isin="+stocks.getIsinCode()+"scCode"+ stocks.getScCode());
					numberOfShares=stocksHolding.getNumberOfShares();
					marketPrice=stocksHolding.getMarketPrice();
					
					double existingNumberOfShares=0;
					
					scCode=stocks.getScCode();
					
					Iterator itr1=existingStocksHoldingsList.iterator();
					while(itr1.hasNext())
					{
						persistStocksHolding=(com.fundexpert.dao.StocksHoldings)itr1.next();	
						
						double existingMarketPrice = persistStocksHolding.getStocks().getClosePrice();
						existingNumberOfShares=persistStocksHolding.getNumberOfShares();
						int existingScCode=persistStocksHolding.getScCode();
						String existingDematId=persistStocksHolding.getDematId();
						String existingDematName=persistStocksHolding.getDematName();
						if(existingScCode == scCode)
						{
							System.out.println("existingSCCODE=SCCODE"+existingScCode);
							System.out.println("Existing demat name = "+existingDematName);
							System.out.println("Current Demat name = "+dematName);
						}
						if(existingScCode == scCode && ((existingDematName==null || dematName==null) || (existingDematName.equalsIgnoreCase(dematName))))
						{
							foundExistingStocksHoldings=true;
							break;
						}
					}	
					if(foundExistingStocksHoldings==true)
					{
						System.out.print("(This StockHolding already exists)Existing StocksHoldings for scCode = "+stocks.getScCode()+"  for userId="+user.getId()+" dematId"+dematId);
						if(existingNumberOfShares==numberOfShares)
						{
							System.out.println("   with no change in numberOfShares");
							//StocksHoldings for this userId with this isinCode already exists which have same marketPricce and numberOfShares hence continue;
						}
						else
						{
							System.out.println("   with change in numberOfShares");
							persistStocksHolding.setNumberOfShares(numberOfShares);
							if(dematName!=null && !dematName.equals(""))
								persistStocksHolding.setDematName(dematName);
							persistStocksHolding.setLastUpdatedOn(new Date());
							System.out.println("scCode="+stocks.getScCode());
							if(stocksHolding.getScName().equalsIgnoreCase("Trading Suspended"))
							{
								persistStocksHolding.setStatus(0);
							}
							else
							{
								persistStocksHolding.setStatus(1);
							}
							persistStocksHolding.setStockName(stockNameFromPdf);
						}
					}
					else
					{
						Calendar cal=Calendar.getInstance();
						cal.add(Calendar.YEAR,-1);
						Date oneYearBackDate=cal.getTime();
						System.out.println("(Persisting this isin )StocksHoldings does not exists for isin = "+stocks.getIsinCode()+"  for userId="+user.getId()+" oneYearBackDate="+oneYearBackDate);
						persistStocksHolding=new com.fundexpert.dao.StocksHoldings();
						persistStocksHolding.setNumberOfShares(numberOfShares);
						persistStocksHolding.setStocks(stocks);
						persistStocksHolding.setUserId(user.getId());
						//persistStocksHolding.setBuyDate(oneYearBackDate);
						persistStocksHolding.setDematId(dematId);
						persistStocksHolding.setDematName(dematName);
						persistStocksHolding.setLastUpdatedOn(new Date());
						persistStocksHolding.setStockName(stockNameFromPdf);
						//trading suspended is in case of NSDL file
						if(stocksHolding.getScName() != null && stocksHolding.getScName().equalsIgnoreCase("Trading Suspended"))
						{
							persistStocksHolding.setStatus(0);
						}
						else
						{
							persistStocksHolding.setStatus(1);
						}
						System.out.println("done");
					}
					hSession.saveOrUpdate(persistStocksHolding);
					tx.commit();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	public List<Map> getMissingStocksList()
	{
		return missingStocksList;
	}
	
	public List<StocksHoldings> createStockHoldingsFromJsonArray(JSONArray jsonArray) throws FundexpertException
	{
		List<StocksHoldings> list=null;
		double units,nav;
		String isin,marketCap,name,sector;
		try
		{
			list=new ArrayList<StocksHoldings>();
			for(Object obj:jsonArray)
			{
				JSONObject json=(JSONObject)obj;
				if(!json.has("isin") || !json.has("units") || !json.has("sn"))
					throw new FundexpertException("Not enough data to generate Stock portfolio from JSON.");
				isin=json.getString("isin");
				units=Double.valueOf(json.getString("units"));
				name=json.getString("sn");
				StocksHoldings holding=new StocksHoldings();
				holding.setIsinCode(isin);
				holding.setNumberOfShares(units);
				holding.setScName(name);
				list.add(holding);
			}
			return list;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public List<StocksHoldings> createStockHoldingsFromJsonArrayWithScCode(JSONArray jsonArray) throws FundexpertException
	{
		List<StocksHoldings> list=null;
		double units,nav;
		int scCode;
		String isin=null;
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			list=new ArrayList<StocksHoldings>();
			for(Object obj:jsonArray)
			{
				JSONObject json=(JSONObject)obj;
				if(!json.has("scCode") || !json.has("units"))
					throw new FundexpertException("Not enough data to generate Stock portfolio from JSON.");
				scCode=json.getInt("scCode");
				units=json.getDouble("units");
				Object[] object=(Object[])hSession.createQuery("select isinCode,scName from Stocks where scCode=?").setInteger(0, scCode).setMaxResults(1).uniqueResult();
				StocksHoldings holding=new StocksHoldings();
				holding.setIsinCode((String)object[0]);
				holding.setNumberOfShares(units);
				holding.setScName((String)object[1]);
				list.add(holding);
			}
			return list;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
}