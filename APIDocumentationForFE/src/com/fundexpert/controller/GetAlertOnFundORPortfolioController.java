package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.ValidationException;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Alert;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.MutualFundSubscription;
import com.fundexpert.dao.PortfolioSubscription;
import com.fundexpert.dao.User;

public class GetAlertOnFundORPortfolioController 
{
	public boolean isPortfolioSubscribed(Long userID) 
	{
		boolean subscribed=false;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			PortfolioSubscription ps=(PortfolioSubscription)session.createQuery("from PortfolioSubscription where userId=?").setLong(0, userID).uniqueResult();
			if(ps!=null)
			{
				subscribed=true;
				return subscribed;
			}
			else
				return subscribed;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return subscribed;
	}
	
	public List<MutualFundSubscription> getMFList(Long userID) 
	{
		List<MutualFundSubscription> list=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			list =session.createQuery("from MutualFundSubscription where userId=?").setLong(0, userID).list();
			return list;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return list;
	}

	public boolean isMutualfundSubscribed(Long userID)
	{
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			MutualFundSubscription sbf=(MutualFundSubscription) session.createQuery("from MutualFundSubscription where userId=?").setLong(0, userID);			
			if(sbf!=null)
				return true;
			else 
				return false;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return false;
	}

	public Long getAmfiiCode(Long mutualfundId) 
	{
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			MutualFund mf=(MutualFund)session.createQuery("from MutualFund where id=?").setLong(0, mutualfundId).uniqueResult();
			return mf.getAmfiiCode();
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return null;
	}

	public List<Alert> getExitAlert(Long mutualfundId)
	{
		List<Alert> alert=new ArrayList<Alert>();
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			alert=session.createQuery("from Alert where mutualfundId=? and (type=? or type=?)")
					.setLong(0, mutualfundId).setInteger(1, 1).setInteger(2, 2).list();
			return alert;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return alert;
	}

	public List<Alert> getAlertt(Long mutualfundId)
	{
		Calendar cal=Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		List<Alert> alertList=new ArrayList<Alert>();
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			alertList=session.createQuery("from Alert where mutualfundId=? and tradeDate>=?")
					.setLong(0, mutualfundId).setDate(1, cal.getTime()).list();
			return alertList;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return alertList;
	}
	
	public Set<Long> getHoldingList(Long userID)
	{
		Set<Long> set=new HashSet<Long>();
		List<Holding> list=new ArrayList<Holding>();
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			list=session.createQuery("from Holding where userId=?").setLong(0, userID).list();
			for(Holding h:list)
				set.add(h.getMutualfundId());				
			return set;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return set;
	}
	
	public MutualFund getCategoryOfMF(Long mfId, Long amfiiCode) 
	{ 
		MutualFund mf=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			mf=(MutualFund)session.createQuery("from MutualFund where id=? and amfiiCode=?").setLong(0, mfId).setLong(1, amfiiCode).uniqueResult();
			return mf;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}	
		return mf;
	}

	public List<MutualFund> getListOfSameMFCategory(String category, String subCat)
	{
		List<MutualFund> list=new ArrayList<MutualFund>();
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			list=session.createQuery("from MutualFund where schemeType=? and type=? and ourRating=?")
						.setString(0, category).setString(1, subCat).setInteger(2, 5).list();
			return list;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}	
		return list;
	}

	public Alert getEntryAlertOfFund(Long id) 
	{
		Alert alert=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			alert=(Alert) session.createQuery("from Alert where mutualfundId=? and type=?").setLong(0, id).setInteger(1, 1).uniqueResult();			
			return alert;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}	
		return alert;
	}

	public Holding getHolding(Long mfId) 
	{
		Holding h=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			h=(Holding) session.createQuery("from Holding where mutualfundId=?").setLong(0, mfId).setMaxResults(1).uniqueResult();			
			return h;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}	
		return h;
	}
}