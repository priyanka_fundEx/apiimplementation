package com.fundexpert.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fundexpert.dao.Action;
import com.fundexpert.dao.BlackListFunds;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Reco;
import com.fundexpert.dao.RecommendedPortfolio;
import com.fundexpert.dao.Stocks;
import com.apidoc.util.HibernateBridge;

public class StocksController {

	public boolean updateStock(JSONArray array)
	{
		Session hSession=null;
		Transaction tx=null;
		double closePrice=0;
		Date asOn=null;
		String type="";
		Integer scCode=0;
		String scName="";
		String isinCode="",segment="";
		boolean block,delisted;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			for(int i=0;i<array.length();i++)
			{
				JSONObject json=array.getJSONObject(i);
				Stocks stock=null;
				if(json.has("scCode"))
				{
					scCode=json.getInt("scCode");
					//System.out.print("scCode = "+scCode+"   |");
					stock=(Stocks)hSession.get(Stocks.class,scCode);
					if(stock==null)
					{
						stock=new Stocks();
						stock.setScCode(scCode);
					}
				}
				else
					continue;
				if(json.has("closePrice"))
				{
					closePrice=Double.valueOf((String)json.get("closePrice"));
					stock.setClosePrice(closePrice);
				}
				if(json.has("type"))
				{
					type=((String)json.get("type")).toUpperCase().trim();
					if(!type.equals("NA"))
						stock.setType(type);
				}
				if(json.has("asOn"))
				{
					asOn=new SimpleDateFormat("dd-MM-yyyy").parse((String)json.get("asOn"));
					stock.setAsOn(asOn);
				}
				if(json.has("name"))
				{
					scName=(String)json.get("name");
					stock.setScName(scName);
				}
				if(json.has("isinCode"))
				{
					isinCode=json.getString("isinCode");
					stock.setIsinCode(isinCode);
				}
				if(json.has("block"))
				{
					block=json.getBoolean("block");
					//System.out.println(" blocked = "+block);
					stock.setBlock(block);
				}
				if(json.has("delisted"))
				{
					delisted=json.getBoolean("delisted");
					stock.setDelisted(delisted);
				}
				if(json.has("segment"))
				{
					segment=json.getString("segment");
					stock.setSegment(segment);
				}
				if(json.has("longTerm") && json.has("mediumTerm") && json.has("shortTerm"))
				{
					//System.out.println("has long term and mterm and sterm");
					String lT=(String)json.get("longTerm");
					String mT=(String)json.get("mediumTerm");
					String sT=(String)json.get("shortTerm");
					double lTWeight=0,mTWeight=0,sTWeight=0,sumOfWeight=0,rating=0;
					if(!lT.equals("-1"))
					{
						lTWeight=lT.equals("1")?1:.5;
					}
					if(!mT.equals("-1"))
					{
						mTWeight=mT.equals("1")?.5:.25;
					}
					if(!sT.equals("-1"))
					{
						sTWeight=sT.equals("1")?.25:.125;
					}
					sumOfWeight=lTWeight + mTWeight + sTWeight;
					rating=Math.round((sumOfWeight/1.75)*5);
					//System.out.println("RATING: "+rating);
					if(rating == 0){
						rating = 1;
					}
					stock.setRating(rating);
				}
				else if(json.has("rating"))
				{
					double rating=json.getDouble("rating");
					//System.out.println("Rating="+rating);
					stock.setRating(rating);
				}
				stock.setUpdatedOn(new Date());
				hSession.saveOrUpdate(stock);
			}
			System.out.println("Commit");
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null && tx.isActive())
				tx.rollback();
			return false;
		}
		finally
		{
			hSession.close();
		}
	}
	
	//make api call to /Indicators/get-suggestion
	//takes input of array of scCode : ["500002","500004"]
	public JSONObject getDetails(JSONArray scCodeArray)
	{
		JSONObject jsonObject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		try
		{
			System.out.println("Calling get-details of stock.");
			URL url = new URL("http://test.fundexpert.in/Indicators/get-details?stocksForApi=true");
			URLConnection con=url.openConnection();
			
			con.setRequestProperty("Accept", "application/json");
			con.setConnectTimeout(5000000);
			con.setReadTimeout(5000000);
			if(scCodeArray!=null && scCodeArray.length()>0)
			{
				con.setDoInput(true);
				con.setDoOutput(true);
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(scCodeArray.toString());
				writer.close();
			}
			//System.out.println("scCode="+scCodeArray.toString());
			
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line=br.readLine();
			//System.out.println("line="+line);
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			
			jsonObject=new JSONObject(builder.toString());
			br.close();
			return jsonObject;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			try 
			{
				jsonObject.put("success", "false");
				jsonObject.put("error",e.getMessage());
			}
			catch (JSONException e1) 
			{
				e1.printStackTrace();
			}
		}
		return jsonObject;
	}

	public Stocks getStockByScCode(int scCode)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			return (Stocks)hSession.createQuery("from Stocks where scCode=?").setInteger(0, scCode).uniqueResult();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Stocks> getBlackList(long consumerId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Stocks> list=hSession.createQuery("select s from Stocks s,BlackListFunds blf where s.scCode=blf.scCode and blf.consumerId=? order by s.scName").setLong(0, consumerId).list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	public boolean deleteSingleStockFromBlackList(int scCode,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			int row=hSession.createQuery("delete from BlackListFunds where scCode=? and consumerId=?").setInteger(0, scCode).setLong(1, consumerId).executeUpdate();
			System.out.println("Success of deleting single blacklist stock="+row);
			
			Action action=new Action();
			action.setAction(Action.REMOVE);
			action.setConsumerId(consumerId);
			action.setCreatedOn(new Date());
			action.setScCode(scCode);
			action.setTableType(Action.BLACKLIST);
			hSession.save(action);
			
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public boolean saveBlackList(String[] scCodeArray,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		List currentBlackList=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			currentBlackList=hSession.createQuery("select scCode from BlackListFunds where consumerId=? and scCode is not null").setLong(0, consumerId).list();
			for(String s:scCodeArray)
			{
				int scCode=Integer.valueOf(s);
				if(!currentBlackList.contains(scCode))
				{
					BlackListFunds blf=new BlackListFunds();
					blf.setConsumerId(consumerId);
					blf.setScCode(scCode);
					hSession.save(blf);
					
					Action action=new Action();
					action.setAction(Action.ADD);
					action.setConsumerId(consumerId);
					action.setCreatedOn(new Date());
					action.setScCode(scCode);
					action.setTableType(Action.BLACKLIST);
					hSession.save(action);
				}
			}
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public boolean deleteAllStocksFromBlackList(long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Integer> list=hSession.createQuery("select scCode from BlackListFunds where consumerId=? and scCode is not null").setLong(0, consumerId).list();
			for(int scCode:list)
			{
				tx=hSession.beginTransaction();
				int row=hSession.createQuery("delete from BlackListFunds where consumerId=? and scCode=?").setLong(0, consumerId).setLong(1, scCode).executeUpdate();
				
				Action action=new Action();
				action.setAction(Action.REMOVE);
				action.setConsumerId(consumerId);
				action.setCreatedOn(new Date());
				action.setScCode(scCode);
				action.setTableType(Action.BLACKLIST);
				hSession.save(action);
				tx.commit();
			}
			System.out.println("Success of deleting All blacklist funds for consumerId="+consumerId);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public List<Stocks> getAllStocks()
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Stocks> list=hSession.createQuery("from Stocks where delisted=0").list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}

	public List<Stocks> getRecommendedPortfolio(long consumerId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Stocks> list=hSession.createQuery("select s from Stocks s,RecommendedPortfolio rp where s.scCode=rp.scCode and rp.consumerId=? order by s.scName").setLong(0, consumerId).list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public boolean deleteSingleStockFromRecommended(Integer scCode,Long consumerId) 
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			int row=hSession.createQuery("delete from RecommendedPortfolio where scCode=? and consumerId=?").setInteger(0, scCode).setLong(1, consumerId).executeUpdate();
			System.out.println("Success of deleting single Recommended stock="+row);
			
			Action action=new Action();
			action.setAction(Action.REMOVE);
			action.setConsumerId(consumerId);
			action.setCreatedOn(new Date());
			action.setScCode(scCode);
			action.setTableType(Action.RECOMMENDED);
			hSession.save(action);
			
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public boolean saveRecommended(String[] scCodeArray,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		List currentList=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			currentList=hSession.createQuery("select scCode from RecommendedPortfolio where consumerId=? and scCode is not null").setLong(0, consumerId).list();
			for(String s:scCodeArray)
			{
				int scCode=Integer.valueOf(s);
				if(!currentList.contains(scCode))
				{
					RecommendedPortfolio rp=new RecommendedPortfolio();
					rp.setConsumerId(consumerId);
					rp.setScCode(scCode);
					hSession.save(rp);
					
					Action action=new Action();
					action.setAction(Action.ADD);
					action.setConsumerId(consumerId);
					action.setCreatedOn(new Date());
					action.setScCode(scCode);
					action.setTableType(Action.RECOMMENDED);
					hSession.save(action);
				}
			}
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public boolean deleteAllStocksFromRecommended(long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Integer> list=hSession.createQuery("select scCode from RecommendedPortfolio where consumerId=? and scCode is not null").setLong(0, consumerId).list();
			for(int scCode:list)
			{
				tx=hSession.beginTransaction();
				int row=hSession.createQuery("delete from RecommendedPortfolio where consumerId=? and scCode=?").setLong(0, consumerId).setLong(1, scCode).executeUpdate();
				
				Action action=new Action();
				action.setAction(Action.REMOVE);
				action.setConsumerId(consumerId);
				action.setCreatedOn(new Date());
				action.setScCode(scCode);
				action.setTableType(Action.RECOMMENDED);
				hSession.save(action);
				tx.commit();
			}
			System.out.println("Success of deleting All Recommended funds for consumerId="+consumerId);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public List<Stocks> getRecoList(long consumerId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Stocks> list=hSession.createQuery("select s from Stocks s,Reco blf where s.scCode=blf.scCode and blf.consumerId=? order by s.scName").setLong(0, consumerId).list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	public boolean deleteSingleStockFromReco(int scCode,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			int row=hSession.createQuery("delete from Reco where scCode=? and consumerId=?").setInteger(0, scCode).setLong(1, consumerId).executeUpdate();
			System.out.println("Success of deleting single Reco stock="+row);
			
			Action action=new Action();
			action.setAction(Action.REMOVE);
			action.setConsumerId(consumerId);
			action.setCreatedOn(new Date());
			action.setScCode(scCode);
			action.setTableType(Action.RECO);
			hSession.save(action);
			
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public boolean saveReco(String[] scCodeArray,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		List currentRecoList=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			currentRecoList=hSession.createQuery("select scCode from Reco where consumerId=? and scCode is not null").setLong(0, consumerId).list();
			for(String s:scCodeArray)
			{
				int scCode=Integer.valueOf(s);
				if(!currentRecoList.contains(scCode))
				{
					Reco reco=new Reco();
					reco.setConsumerId(consumerId);
					reco.setScCode(scCode);
					hSession.save(reco);
					
					Action action=new Action();
					action.setAction(Action.ADD);
					action.setConsumerId(consumerId);
					action.setCreatedOn(new Date());
					action.setScCode(scCode);
					action.setTableType(Action.RECO);
					hSession.save(action);
				}
			}
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public boolean deleteAllStocksFromReco(long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Integer> list=hSession.createQuery("select scCode from Reco where consumerId=? and scCode is not null").setLong(0, consumerId).list();
			for(int scCode:list)
			{
				tx=hSession.beginTransaction();
				int row=hSession.createQuery("delete from Reco where consumerId=? and scCode=?").setLong(0, consumerId).setLong(1, scCode).executeUpdate();
				
				Action action=new Action();
				action.setAction(Action.REMOVE);
				action.setConsumerId(consumerId);
				action.setCreatedOn(new Date());
				action.setScCode(scCode);
				action.setTableType(Action.RECO);
				hSession.save(action);
				tx.commit();
			}
			System.out.println("Success of deleting All Recommended funds for consumerId="+consumerId);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public List<Stocks> getStockByCapitalType(String capitalType) {
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			System.out.println("CapitalType="+capitalType);
			List<Stocks> list=hSession.createQuery("from Stocks where type=? and type is not null and type!='' and segment!='' and segment is not null").setString(0, capitalType.toUpperCase()).list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	public List<Stocks> getUpdatedStocks(JSONArray requestJsonArray)
	{
		Session hSession=null;
		List<Stocks> stocksList=new ArrayList<Stocks>();
		JSONObject json=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Integer> scCodeList=hSession.createQuery("select scCode from Stocks order by scCode").list();
			List<Stocks> stocksL=hSession.createQuery("from Stocks order by scCode").list();
			for(int i=0;i<requestJsonArray.length();i++)
			{
				json=(JSONObject)requestJsonArray.get(i);
				Integer scCode=json.getInt("scCode");
				//System.out.print("scCode"+scCode);
				Date lastUpdatedOn=null;
				if(json.has("updatedOn"))
				{
					lastUpdatedOn=new SimpleDateFormat("yyyy-MM-dd").parse(json.getString("updatedOn"));
				}
				else
				{
					lastUpdatedOn=null;
				}
				int indexOfStock=scCodeList.indexOf(scCode);
				if(indexOfStock!=-1)
				{
					//System.out.print("    scCodeList has");
					Stocks stock=stocksL.get(indexOfStock);
					if((stock.getUpdatedOn()!=null && lastUpdatedOn!=null && stock.getUpdatedOn().after(lastUpdatedOn)) || (stock.getUpdatedOn()!=null && lastUpdatedOn==null))
					{
						stocksList.add(stock);
						scCodeList.remove(indexOfStock);
						stocksL.remove(indexOfStock);
					}
				}
			}
			return stocksList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public List<Stocks> getNewStocks(JSONArray requestJsonArray) throws Exception
	{
		Session hSession=null;
		List scCodeList=new LinkedList();
		List<Stocks> stocks=new ArrayList<Stocks>();
		JSONObject json=null;
		try
		{
			scCodeList=requestJsonArray.toList();
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Integer> stocksL=hSession.createQuery("select scCode from Stocks order by scCode").list();
			stocksL.removeAll(scCodeList);
			if(stocksL.size()>0)
			{
				System.out.println("new stocks size="+stocksL.size());
				stocks=hSession.createQuery("from Stocks where scCode in (:scCodeList)").setParameterList("scCodeList", stocksL).list();
				return stocks;
			}
			else
				return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}
}
