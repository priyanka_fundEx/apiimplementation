package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;

public class PortfolioHealthScoreController
{

	/*public boolean checkRatingGTZero(Long mfId)
	{
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			MutualFund mf = (MutualFund) session.createQuery("from MutualFund where id=?").setLong(0, mfId).list();
			if(mf.getOurRating() > 0)
				return true;
			else
				return false;
		}
		catch (ValidationException e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return false;
	}*/

	public double getRating(Long mfId)
	{
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			MutualFund mf = (MutualFund) session.createQuery("from MutualFund where id=?").setLong(0, mfId).uniqueResult();
			if(mf != null)
			{
				//System.out.println("mutual Fund: "+mf.getAmfiiCode());
				return mf.getOurRating();
			}
			else
				return 0;
		}
		catch (ValidationException e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return 0;
	}

	public List<Holding> getHoldingList(Long userID)
	{
		List<Holding> list = new ArrayList<Holding>();
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			list = session.createQuery("from Holding where userId=?").setLong(0, userID).list();
			return list;
		}
		catch (ValidationException e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return list;
	}
	
	public List<Holding> getHoldingListInOrderOfRating(Long userID)
	{
		List<Holding> list = new ArrayList<Holding>();
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			list = session.createQuery("from Holding where userId=?").setLong(0, userID).list();
			return list;
		}
		catch (ValidationException e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return list;
	}
}