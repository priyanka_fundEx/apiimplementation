package com.fundexpert.controller;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.ArchiveApiRequests;

public class ArchiveApiRequestController {

	public void saveRequest(long consumerId,String email,String pan,short apiRequestType)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			ArchiveApiRequests aar=new ArchiveApiRequests();
			aar.setConsumerId(consumerId);
			aar.setApiRequestType(apiRequestType);
			aar.setRequestTime(new Date());
			aar.setEmail(email);
			aar.setPan(pan);
			hSession.save(aar);
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	public void saveRequest(long consumerId,short apiRequestType)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			ArchiveApiRequests aar=new ArchiveApiRequests();
			aar.setConsumerId(consumerId);
			aar.setApiRequestType(apiRequestType);
			aar.setRequestTime(new Date());
			hSession.save(aar);
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
}
