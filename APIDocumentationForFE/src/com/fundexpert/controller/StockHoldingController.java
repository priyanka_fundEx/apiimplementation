package com.fundexpert.controller;

import java.util.List;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.StocksHoldings;
import com.fundexpert.dao.Transaction;

public class StockHoldingController {

	public boolean deleteAllStockHoldings(long userId)
	{
		Session hSession=null;
		org.hibernate.Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			int holdingCount=hSession.createQuery("delete from StocksHoldings where userId=?").setLong(0, userId).executeUpdate();
			System.out.println("StockHolding delete count="+holdingCount);
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public List<StocksHoldings> getStockHoldings(long userId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<StocksHoldings> list=hSession.createQuery("from StocksHoldings where userId=?").setLong(0, userId).list();
			System.out.println("StockHolding list="+list);
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	public boolean archiveStockHoldings(long userId)
	{
		Session hSession=null;
		org.hibernate.Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			String insertQuery="insert into ArchiveStocksHoldings(userId,scCode,numberOfShares,buyDate,buyPrice,status,lastUpdatedOn,dematId,dematName,archivedOn)"+
					" select userId,scCode,numberOfShares,buyDate,buyPrice,status,lastUpdatedOn,dematId,dematName,now() from StocksHoldings where userId=?";
			tx=hSession.beginTransaction();
			int updatedRows=hSession.createQuery(insertQuery).setParameter(0, userId).executeUpdate();
			System.out.println("Updated Rows="+updatedRows);
			tx.commit();
			tx=hSession.beginTransaction();
			int deletedRows=hSession.createQuery("delete from StocksHoldings where userId=?").setLong(0,userId).executeUpdate();
			System.out.println("Deleted Rows="+deletedRows);
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null && tx.isActive())
				tx.rollback();
			throw e;
		}
		finally
		{
			hSession.close();
		}
	}
	
}
