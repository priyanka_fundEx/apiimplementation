package com.fundexpert.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.util.SerializedObjectToFile;
import com.fundexpert.util.MutualFundStatus;
import com.fundexpert.config.Config;
import com.fundexpert.dao.Action;
import com.fundexpert.dao.BlackListFunds;
import com.fundexpert.dao.FundHouse;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Reco;
import com.fundexpert.dao.SIPDate;
import com.fundexpert.util.Utilities;

public class MutualFundController {

	
	public List<FundHouse> getFundHouseList()
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<FundHouse> list=hSession.createQuery("from FundHouse order by name").list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	public boolean addNewMutualFund() throws Exception
	{
		/*Input : 
			{"code":11212,"maxId":21}
		  Ouput :
		    {mutualFund Object}
		*/
		
		Session hSession=null;
		StringBuilder builder=null;
		List<String> schemeList=null;
		File addNewMutualFundsFile=new File("/opt/feapi/MutualFund/mfSyncStatus.txt");
		File dir=new File("/opt/feapi/MutualFund");
		MutualFundStatus mfStatus=null;
		String todayDate=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
		SerializedObjectToFile sf=new SerializedObjectToFile();
		Map<String,String> addNewExistingMfMap=null;
		try
		{
			if(!dir.isDirectory())
			{
				dir.mkdirs();
			}
			if(!addNewMutualFundsFile.exists())
			{
				addNewMutualFundsFile.createNewFile();
			}
			Config config=new Config();
			String path="";
			if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
			{
				path=config.getProperty(Config.ADD_NEW_FUND_LINK_PRODUCTION);
			}
			else
			{
				path=config.getProperty(Config.ADD_NEW_FUND_LINK_DEVELOPMENT);
			}
			
			String currentDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			int privateKey=("1tradersCockpit.in4"+currentDate+"fe").hashCode();
			hSession=HibernateBridge.getSessionFactory().openSession();
			System.out.println("Session OPENED.Path is "+path);
			
			schemeList=hSession.createQuery("select distinct schemeCode from MutualFund").list();
			
			JSONArray schemeArray=new JSONArray(schemeList);
			
			JSONObject json=new JSONObject();
			json.put("code", privateKey);
			json.put("schemeCodes",schemeArray);
			
			URL url = new URL(path);
			URLConnection con=url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(500000);
			con.setReadTimeout(500000);
			System.out.println("After opening connection to URL to update MutualFunds.");
			
			OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
			writer.write(json.toString());
			writer.close(); 
			System.out.println("Sending mutualfund data of length : as "+schemeArray.length());
			
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line=br.readLine();
		
			builder=new StringBuilder();
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			br.close();
			json=new JSONObject(builder.toString());
			System.out.print("Received JSON size="+json.length());
			//System.out.println("When  addNewMutualFund  - Received JSON at apidoc="+json.toString(1));
			if(json.has("success") && json.getBoolean("success")==true && json.has("mf"))
			{
				System.out.println("  with success=true no of funds = "+json.getJSONArray("mf").length());
				boolean insertSuccessfully=insertMutualFund(json.getJSONArray("mf"));
				if(insertSuccessfully==false)
					throw new Exception("Something went wrong while saving MutualFund.");
			}
			else if(json.has("success") && json.getBoolean("success"))
			{
				return true;
			}
			else
			{
				System.out.println("  with success=false");
				throw new Exception("Something Went Wrong while fetching MF data.");
			}
			Utilities u=new Utilities();
			u.doMutualFundClassification();
			
			try
			{
				mfStatus=(MutualFundStatus)sf.getSerializedFile(addNewMutualFundsFile);
			}
			catch(EOFException e)
			{
				e.printStackTrace();
				mfStatus=new MutualFundStatus();
			}
			
			addNewExistingMfMap=mfStatus.getAddNewMFMap();
			addNewExistingMfMap.put(todayDate, "true");
			sf.serializeObject(mfStatus, addNewMutualFundsFile);
			
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			try
			{
				mfStatus=(MutualFundStatus)sf.getSerializedFile(addNewMutualFundsFile);
			}
			catch(EOFException e1)
			{
				e1.printStackTrace();
				mfStatus=new MutualFundStatus();
			}
			
			addNewExistingMfMap=mfStatus.getAddNewMFMap();
			addNewExistingMfMap.put(todayDate, "false|"+e.getMessage());
			sf.serializeObject(mfStatus, addNewMutualFundsFile);
			return false;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public boolean insertMutualFund(JSONArray jsonArray)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Iterator itr=jsonArray.iterator();
			while(itr.hasNext())
			{
				JSONObject json=(JSONObject)itr.next();
				MutualFund existing=getBySchemeCode(json.getString("schemeCode"));
				//System.out.println("For SchemeCode = "+existing.getSchemeCode());
				com.fundexpert.pojo.MutualFund mf=null;
				MutualFund mf1=null;
				Set<SIPDate> setSipDate=null;
				if(existing!=null)
				{
				    //System.out.println("existing not null");
					mf=getMutualFundFromJson(json,existing.getId());
				}
				else
				{
					//System.out.println("existing null");
					mf1=getMutualFundFromJsonUsingDAO(json,null);
					setSipDate=mf1.getSipDates();
					mf1.setSipDates(null);
				}
				tx=hSession.beginTransaction();
				if(existing==null)
				{
					//check if it is getting saved
					//System.out.println(mf1.getId());
					if(mf1.getType()==null) {
						//System.out.println("Mutualfund type is null for id = "+mf1.getId()+" schemeCode = "+mf1.getSchemeCode()+" name - "+mf1.getName());
					}
					else {
						//System.out.println("Mutualfund type is not null for id = "+mf1.getId()+" schemeCode = "+mf1.getSchemeCode()+" name - "+mf1.getName());
					}
					long id=(long)hSession.save(mf1);
					SIPDate sipDate=null;
					if(setSipDate!=null)
					{
						Iterator itr1=setSipDate.iterator();
						while(itr1.hasNext())
						{
							sipDate=(SIPDate)itr1.next();
							sipDate.setMutualFundId(id);
						}
						mf1.setSipDates(setSipDate);
						hSession.saveOrUpdate(mf1);
					}
				}
				else
				{
					//System.out.print("MutualFundAlready exists.with id="+mf.getId());
					
					if((mf.getUpdatedOn()!=null && existing.getUpdatedOn()!=null 
							&& mf.getUpdatedOn().after(existing.getUpdatedOn())) 
							|| (existing.getUpdatedOn()==null && mf.getUpdatedOn()!=null))
					{
						//System.out.println("Updating MutualFund existingUpdatedOn = "+existing.getUpdatedOn()+" newUpdatedOn = "+mf.getUpdatedOn());
						
						//int res=hSession.createQuery("delete from SIPDate where mutualFundId=?").setParameter(0, existing.getId()).executeUpdate();
						existing=(MutualFund)hSession.get(MutualFund.class,existing.getId());
					/*	System.out.println("HasCode from DB="+existing.getSipDates().hashCode());
						if(existing.getSipDates()!=null && existing.getSipDates().size()==0)
						{
							System.out.println("Existing sip is not null and size==0");
							//System.out.println("size = "+existing.getSipDates().size());
							//existing.getSipDates().forEach(i->System.out.println("0) i = "+i.getSipDate()+" | "+i.getFrequencyType()));
						}
						else
						{
							if(existing.getSipDates()==null)
								System.out.println("Existing sip is null.");
							else
								System.out.println("Existing sip size is >0.");
						}
						if(mf.getSipDatesForPojo()!=null && mf.getSipDatesForPojo().size()==0)
						{
							System.out.println("Received sip is not null and size==0");
						}
						else
						{
							if(mf.getSipDatesForPojo()==null)
								System.out.println("Received sip is null.");
							else
								System.out.println("Received sip size is >0.");
						}*/
						long existingId=existing.getId();
						String broaderSchemeType=existing.getBroaderSchemeType();
						String broaderType=existing.getBroaderType();
						BeanUtils.copyProperties(existing, mf);
						existing.setBroaderSchemeType(broaderSchemeType);
						existing.setBroaderType(broaderType);
					/*	if((existing.getSipDates()==null || existing.getSipDates().size()==0) && mf.getSipDatesForPojo()!=null && mf.getSipDatesForPojo().size()>0)
						{
							if(existing.getSipDates()==null)
							{
								System.out.println("STEP1");
								existing.setSipDates(mf.getSipDatesForPojo());
								//existing.getSipDates().forEach(i->System.out.println("1) i = "+i.getSipDate()+" | "+i.getFrequencyType()));
							}
							else
							{
								System.out.println("STEP2");
								System.out.println("hascode before updating "+existing.getSipDates().hashCode());
								existing.getSipDates().addAll(mf.getSipDatesForPojo());
								//existing.getSipDates().forEach(i->System.out.println("2) i = "+i.getSipDate()+" | "+i.getFrequencyType()));
							}
						}
						else if((existing.getSipDates()!=null && existing.getSipDates().size()!=0) && (mf.getSipDatesForPojo()==null || mf.getSipDatesForPojo().size()==0))
						{
							System.out.println("STEP3 "+existingId);
							int res=hSession.createQuery("delete from SIPDate where mutualFundId=?").setParameter(0, existingId).executeUpdate();
							System.out.println("No of sips deleted = "+res);
						}
						else
						{
							System.out.println("STEP4 "+existingId);
							int res=hSession.createQuery("delete from SIPDate where mutualFundId=?").setParameter(0, existingId).executeUpdate();
							System.out.println("deleted rows= "+res);
							hSession.flush();
							hSession.clear();
							tx.commit();
							tx=hSession.beginTransaction();
							existing.setSipDates(null);
							//existing.setSipDates(mf.getSipDatesForPojo());
							//existing.getSipDates().clear();
							//hSession.save(existing);
							//existing.getSipDates().addAll(mf.getSipDatesForPojo());
							//existing.setSipDates(mf.getSipDatesForPojo());
						}*/
						//System.out.println("Existingid = "+existingId);
						existing.setId(existingId);
						//System.out.println("     Saving updatedMf.");
						hSession.saveOrUpdate(existing);
					}
				}
				tx.commit();
			}
			System.out.println("Mutualfund Saving Done.");
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			return false;
		}
		finally
		{
			if(hSession!=null && hSession.isOpen())
				hSession.close();
		}
	}
	
	public com.fundexpert.pojo.MutualFund getMutualFundFromJson(JSONObject json,Long existingId)
	{
		try
		{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			com.fundexpert.pojo.MutualFund mf=new com.fundexpert.pojo.MutualFund();
			//System.out.println("Received id="+json.getLong("id"));
			/*mf.setId(json.getLong("id"));*/
			mf.setName(json.getString("name"));
			mf.setRtaSchemeCode(json.has("rtaSchemeCode")?json.getString("rtaSchemeCode"):null);
			mf.setSchemeCode(json.has("schemeCode")?json.getString("schemeCode"):null);
			mf.setRoboSchemeCode(json.has("roboSchemeCode")?json.getString("roboSchemeCode"):null);
			mf.setType(json.has("type")?json.getString("type"):null);
			mf.setOptionType(json.has("optionType")?json.getString("optionType"):null);
			mf.setSchemeType(json.has("schemeType")?json.getString("schemeType"):null);
			mf.setComment(json.has("comment")?json.getString("comment"):null);
			mf.setIsin(json.has("isin")?json.getString("isin"):null);
			mf.setCamsSchemeCode(json.has("camsSchemeCode")?json.getString("camsSchemeCode"):null);
			mf.setSettlementType(json.has("settlementType")?json.getString("settlementType"):null);
			mf.setSipFlag(json.has("sipFlag")?json.getBoolean("sipFlag"):null);
			mf.setTransactionMode(json.has("transacctionMode")?json.getString("transacctionMode"):null);
			mf.setPurchaseAllowed(json.has("purchasedAllowed")?json.getBoolean("purchasedAllowed"):null);
			mf.setDematPurchaseAllowed(json.has("dematPurchaseAllowed")?json.getBoolean("dematPurchaseAllowed"):null);
			mf.setPhysicalPurchaseAllowed(json.has("physicalPurchaseAllowed")?json.getBoolean("physicalPurchaseAllowed"):null);
			mf.setMinPurchase(json.has("minPurchase")?json.getDouble("minPurchase"):null);
			mf.setHigh52Week(json.has("high52Week")?json.getDouble("high52Week"):null);
			mf.setLow52Week(json.has("low52Week")?json.getDouble("low52Week"):null);
			mf.setMinAdditionalPurchase(json.has("minAdditionalPurchase")?json.getDouble("minAdditionalPurchase"):null);
			mf.setMinSIPAmount(json.has("minSIPAmount")?json.getDouble("minSIPAmount"):null);
			mf.setMaxSIPAmount(json.has("maxSIPAmount")?json.getDouble("maxSIPAmount"):null);
			mf.setMinNoOfMonths(json.has("minNoOfMonths")?json.getInt("minNoOfMonths"):null);
			mf.setMaxNoOfMonths(json.has("maxNoOfMonths")?json.getInt("maxNoOfMonths"):null);
			mf.setPurchaseMultiple(json.has("purchaseMultiple")?json.getDouble("purchaseMultiple"):null);
			mf.setTimeframe(json.has("timeframe")?json.getInt("timeframe"):null);
			mf.setLockInPeriod(json.has("lockInPeriod")?json.getInt("lockInPeriod"):null);
			mf.setMaxPurchase(json.has("maxPurchase")?json.getDouble("maxPurchase"):null);
			mf.setMinRedemptionAmt(json.has("minRedemptionAmt")?json.getDouble("minRedemptionAmt"):null);
			mf.setMinRedemptionQty(json.has("minRedemptionQty")?json.getDouble("minRedemptionQty"):null);
			mf.setNav(json.has("nav")?json.getDouble("nav"):null);
			mf.setNavChange(json.has("navChange")?json.getDouble("navChange"):null);
			mf.setLastYearNav(json.has("lastYearNav")?json.getDouble("lastYearNav"):null);
			mf.setOneYearReturns(json.has("oneYearReturns")?json.getDouble("oneYearReturns"):null);
			mf.setThreeYearReturns(json.has("threeYearReturns")?json.getDouble("threeYearReturns"):null);
			mf.setFiveYearReturns(json.has("fiveYearReturns")?json.getDouble("fiveYearReturns"):null);
			mf.setOverallReturns(json.has("overallReturns")?json.getDouble("overallReturns"):null);
			mf.setSharpeRatio(json.has("sharpeRatio")?json.getDouble("sharpeRatio"):null);
			mf.setStdDeviation(json.has("stdDeviation")?json.getDouble("stdDeviation"):null);
			mf.setAlpha(json.has("alpha")?json.getDouble("alpha"):null);
			mf.setBeta(json.has("beta")?json.getDouble("beta"):null);
			mf.setrSquaredValue(json.has("rSquaredValue")?json.getDouble("rSquaredValue"):null);
			mf.setUpsideCaptureRatio(json.has("upsideCaptureRatio")?json.getDouble("upsideCaptureRatio"):null);
			mf.setDownsideCaptureRatio(json.has("downsideCaptureRatio")?json.getDouble("downsideCaptureRatio"):null);
			mf.setVroRating(json.has("vroRating")?json.getDouble("vroRating"):null);
			mf.setOurRating(json.has("ourRating")?json.getDouble("ourRating"):null);
			mf.setRoboAvailable(json.has("roboAvailable")?json.getBoolean("roboAvailable"):null);
			mf.setValidated(json.has("validated")?json.getBoolean("validated"):null);
			mf.setRoboSuggested(json.has("roboSuggested")?json.getBoolean("roboSuggested"):null);
			mf.setFlexiSuggested(json.has("flexiSuggested")?json.getBoolean("flexiSuggested"):null);
			mf.setSafeSuggested(json.has("safeSuggested")?json.getBoolean("safeSuggested"):null);
			mf.setTaxSuggested(json.has("taxSuggested")?json.getBoolean("taxSuggested"):null);
			mf.setGrowthSuggested(json.has("growthSuggested")?json.getBoolean("growthSuggested"):null);
			mf.setActive(json.has("active")?json.getBoolean("active"):null);
			mf.setAddedOn(json.has("addedOn")?sdf.parse(json.getString("addedOn")):null);
			mf.setRoboSuggestedOn(json.has("roboSuggestedOn")?sdf.parse(json.getString("roboSuggestedOn")):null);
			mf.setFlexiSuggestedOn(json.has("flexiSuggestedOn")?sdf.parse(json.getString("flexiSuggestedOn")):null);
			mf.setSafeSuggestedOn(json.has("safeSuggestedOn")?sdf.parse(json.getString("safeSuggestedOn")):null);
			mf.setNavUpdatedOn(json.has("navUpdateOn")?sdf.parse(json.getString("navUpdatedOn")):null);
			mf.setStartDate(json.has("startDate")?sdf.parse(json.getString("startDate")):null);
			mf.setEndDate(json.has("endDate")?sdf.parse(json.getString("endDate")):null);
			mf.setLiquidFundId(json.has("liquidFundId")?json.getLong("liquidFundId"):null);
			mf.setAmfiiCode(json.has("amfiiCode")?json.getLong("amfiiCode"):null);
			mf.setBenchmarkIndex(json.has("benchmarkIndex")?json.getString("benchmarkIndex"):null);
			mf.setSchemeSize(json.has("schemeSize")?json.getDouble("schemeSize"):null);
			mf.setExitLoad(json.has("exitLoad")?json.getDouble("exitLoad"):null);
			mf.setEntryLoad(json.has("entryLoad")?json.getDouble("entryLoad"):null);
			mf.setHouseId(json.has("houseId")?json.getLong("houseId"):null);
			mf.setGrowthFundId(json.has("growthFundId")?json.getLong("growthFundId"):null);
			mf.setNonAofGrowthSuggested(json.has("nonAofGrowthSuggested")?json.getBoolean("nonAofGrowthSuggested"):null);
			mf.setDirect(json.has("direct")?json.getBoolean("direct"):null);
			mf.setRegularFundId(json.has("regularFundId")?json.getLong("regularFundId"):null);
			mf.setPlanType(json.has("planType")?json.getString("planType"):null);
			mf.setOpenEnded(json.has("openEnded")?json.getString("openEnded"):null);
			mf.setFundManager(json.has("fundManager")?json.getString("fundManager"):null);
			mf.setStpAllowed(json.has("stpAllowed")?json.getBoolean("stpAllowed"):null);
			mf.setSwpAllowed(json.has("swpAllowed")?json.getBoolean("swpAllowed"):null);
			mf.setSwitchAllowed(json.has("switchAllowed")?json.getBoolean("switchAllowed"):null);
			mf.setNfoFlag(json.has("nfoFlag")?json.getBoolean("nfoFlag"):null);
			mf.setRedemptionAllowed(json.has("redemptionAllowed")?json.getBoolean("redemptionAllowed"):null);
			mf.setUpdatedOn(new Date());
			mf.setBseSchemeType(json.has("bseSchemeType")?json.getString("bseSchemeType"):null);
			mf.setRtaAgentCode(json.has("rtaAgentCode")?json.getString("rtaAgentCode"):null);
			mf.setDividendReinvestmentFlag(json.has("dividendReinvestmentFlag")?json.getString("dividendReinvestmentFlag"):null);
			mf.setYtm(json.has("ytm")?json.getDouble("ytm"):null);
			mf.setModifiedDurationInYears(json.has("modifiedDurationInYears")?json.getDouble("modifiedDurationInYears"):null);
			mf.setTechnicalScore(json.has("technicalScore")?json.getDouble("technicalScore"):null);
			mf.setPersonalScore(json.has("personalScore")?json.getDouble("personalScore"):null);
			mf.setMorningStarAnalystRating(json.has("morningStarAnalystRating")?json.getLong("morningStarAnalystRating"):null);
			mf.setMorningStarRating(json.has("morningStarRating")?json.getLong("morningStarRating"):null);
			mf.setSipAllowed(json.has("sipAllowed")?json.getBoolean("sipAllowed"):null);
			mf.setMinGap(json.has("minGap")?json.getLong("minGap"):null);
			mf.setMaxGap(json.has("maxGap")?json.getLong("maxGap"):null);
			
			/*if(json.has("sipDates"))
			{
				System.out.println("JSON has sip date");
				Iterator itr=json.getJSONArray("sipDates").iterator();
				Set<SIPDate> set=new HashSet<SIPDate>();
				SIPDate sipDate=null;
				JSONObject inner=null;
				while(itr.hasNext())
				{
					inner=(JSONObject)itr.next();
					sipDate=new SIPDate();
					sipDate.setFrequencyType(inner.getInt("f"));
					if(existingId!=null)
						sipDate.setMutualFundId(existingId);
					sipDate.setSipDate(inner.getInt("s"));
					set.add(sipDate);
				}
				mf.setSipDatesForPojo(set);
			}*/
			return mf;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean updateExistingMutualFund() throws Exception
	{

		/*Input : 
			{"code":-124124,"mf":[{"id":12,"updatedOn":"dd-MM-yyyy"},{"id":123,"updatedOn":"dd-MM-yyyy"}]}
		  Ouput :
		    {"success":true,"mf":[{mfObject},{mfObject}]}
		*/
		
		Session hSession=null;
		StringBuilder builder=null;
		JSONObject sendingJsonObject;
		JSONArray sendingJsonArray;
		String todayDate=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
		SerializedObjectToFile sf=new SerializedObjectToFile();
		MutualFundStatus mfStatus=null;
		Map<String,String> updateExistingMfMap=null;
		File updateMutualFundsFile=new File("/opt/feapi/MutualFund/mfSyncStatus.txt");
		File dir=new File("/opt/feapi/MutualFund");
		try
		{
			if(!dir.isDirectory())
			{
				dir.mkdirs();
			}
			if(!updateMutualFundsFile.exists())
			{
				updateMutualFundsFile.createNewFile();
			}
			Config config=new Config();
			String path="";
			if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
			{
				path=config.getProperty(Config.UPDATE_EXISTING_FUND_LINK_PRODUCTION);
			}
			else
			{
				path=config.getProperty(Config.UPDATE_EXISTING_FUND_LINK_DEVELOPMENT);
			}
			String currentDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			int privateKey=("1tradersCockpit.in4"+currentDate+"fe").hashCode();
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Object[]> mainList= hSession.createQuery("select schemeCode,updatedOn from MutualFund order by id").list();
			List<Object[]> list;
			//reason of making below while loop is bcoz fundexpert was unable to respond(and so throws heap out of memory) with large set of text so we decided to get less text by sending less mutualFund data
			int start=0;
			int total=0;
			while(start<mainList.size())
			{
				int end=0;
				if((start+500)<mainList.size())
				{
					end=start+500;
				}
				else
				{
					end=mainList.size(); 
				}
				list=mainList.subList(start, end);
				start=end;
				total+=list.size();
			
				sendingJsonArray=new JSONArray();
				sendingJsonObject=new JSONObject();
				Iterator itr=list.iterator();
				while(itr.hasNext())
				{
					Object[] obj=(Object[])itr.next();
					JSONObject json=new JSONObject();
					json.put("schemeCode", (String)obj[0]);
					json.put("updatedOn",(Date)obj[1]);
					sendingJsonArray.put(json);
				}
				sendingJsonObject.put("code", privateKey);
				sendingJsonObject.put("mf",sendingJsonArray);
				
				URL url = new URL(path);
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setRequestProperty("Accept-Charset", "UTF-8");
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(500000);
				con.setReadTimeout(500000);
				System.out.println("After opening connection to URL to update MutualFunds.");
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(sendingJsonObject.toString());
				writer.close(); 
				//System.out.println("Sending mutualfund data : as "+sendingJsonArray.toString(1));
				System.out.println("Sending mutualfund data size : "+sendingJsonArray.length()+" total done yet = "+total);
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
				String line=br.readLine();
			
				builder=new StringBuilder();
				while(line!=null)
				{
					builder.append(line);
					line=br.readLine();
				}
				br.close();
				JSONObject json=new JSONObject(builder.toString());
				//System.out.println("Received JSON="+json.toString(1));
				if(json.has("success") && json.getBoolean("success")==true)
				{
					System.out.println("Received JSON with success with JSONArray size = "+json.getJSONArray("mf").length());
					boolean insertSuccessfully=insertMutualFund(json.getJSONArray("mf"));
					if(insertSuccessfully==false)
						throw new Exception("Something went wrong while saving MutualFund.");
				}
				else
				{
					throw new Exception("Something Went Wrong while fetching MF data.");
				}
			}
			Utilities u=new Utilities();
			u.doMutualFundClassification();
			
			try
			{
				mfStatus=(MutualFundStatus)sf.getSerializedFile(updateMutualFundsFile);
			}
			catch(EOFException e)
			{
				e.printStackTrace();
				mfStatus=new MutualFundStatus();
			}
			updateExistingMfMap=mfStatus.getUpdateExistingMFMap();
			updateExistingMfMap.put(todayDate, "true");
			sf.serializeObject(mfStatus, updateMutualFundsFile);
			
			System.out.println("Updation Done.");
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			try
			{
				mfStatus=(MutualFundStatus)sf.getSerializedFile(updateMutualFundsFile);
			}
			catch(EOFException e1)
			{
				e1.printStackTrace();
				mfStatus=new MutualFundStatus();
			}
			
			updateExistingMfMap=mfStatus.getUpdateExistingMFMap();
			updateExistingMfMap.put(todayDate, "false|"+e.getMessage());
			sf.serializeObject(mfStatus, updateMutualFundsFile);
			return false;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public MutualFund getFundById(long id)
	{
		Session hSession=null;
		MutualFund mf=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			mf=(MutualFund)hSession.get(MutualFund.class,id);
			return mf;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			hSession.close();
		}
	}

	public List<MutualFund> getListOfMutualFunds(List schemeCodeList)
	{
		List<MutualFund> list=new ArrayList<MutualFund>();
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			System.out.println("Getting List of MutualFunds");
			List<String> currentSchemeCodes=hSession.createQuery("select distinct schemeCode from MutualFund").list();
			System.out.println("Existing mf size="+currentSchemeCodes.size());
			currentSchemeCodes.removeAll(schemeCodeList);
			if(currentSchemeCodes.size()>0)
			{
				System.out.println("new mf size="+currentSchemeCodes.size());
				list=hSession.createQuery("from MutualFund where schemeCode in (:schemeCodeList) order by id").setParameterList("schemeCodeList", currentSchemeCodes).list();
			}
			else
				return null;
			System.out.println("Query fired to get list of mutualFunds.Size="+list.size());
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			if(hSession.isOpen())
				hSession.close();
		}
	}

	public List<MutualFund> getRecentlyUpdatedMutualFunds(JSONArray jsonArray) throws Exception 
	{
		List<MutualFund> list=new ArrayList<MutualFund>();
		Session hSession=null;
		MutualFund mf=null;
		MutualFund mf1=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			for(int i=0;i<jsonArray.length();i++)
			{
				//System.out.print("proccessed index="+i+"  with mutualfund schemeCode=");
				JSONObject json=jsonArray.getJSONObject(i);
				if(!json.has("schemeCode"))
					continue;
				
				String schemeCode=json.getString("schemeCode");
				//System.out.println(schemeCode+"    ");
				if(!json.has("updatedOn"))
				{
					mf=(MutualFund)hSession.createQuery("from MutualFund where schemeCode=? and updatedOn is not null").setString(0,schemeCode).setMaxResults(1).uniqueResult();
					if(mf!=null)
					{
						list.add(mf);
					}
				}
				else
				{
					Date updatedOn=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(json.getString("updatedOn"));
					mf=(MutualFund)hSession.createQuery("from MutualFund where schemeCode=? and updatedOn>?").setString(0,schemeCode).setTimestamp(1,updatedOn).setMaxResults(1).uniqueResult();
					if(mf!=null)
					{
						list.add(mf);
					}
				}
			}
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			if(hSession.isOpen())
				hSession.close();
		}
	}

	public JSONArray getMutualFundMappings() 
	{
		Session hSession=null;
		Map<Long,String> map=new HashMap<Long,String>();
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Object[]> idSchemeCodeList=hSession.createQuery("select id,schemeCode,liquidFundId,regularFundId,growthFundId from MutualFund order by id").list();
			System.out.println("size returned of mapping query = "+idSchemeCodeList.size());
			for(Object[] objArray:idSchemeCodeList)
			{
				Long id=(Long)objArray[0];
				String schemeCode=(String)objArray[1];
				map.put(id, schemeCode);
				//System.out.println("id = "+id+"schemeCode = "+schemeCode);
			}
			JSONObject json=null;
			JSONArray jsonArray=new JSONArray();
			for(Object[] objArray:idSchemeCodeList)
			{
				String schemeCode=(String)objArray[1];
				Long liquidFundId=(Long)objArray[2];
				Long regularFundId=(Long)objArray[3];
				Long growthFundId=(Long)objArray[4];
				if(liquidFundId!=null || regularFundId!=null || growthFundId!=null)
				{	
					System.out.println(" schemeCode = "+schemeCode+" liquidFundId = "+liquidFundId+" regularFundId = "+regularFundId+" growthFundId = "+growthFundId);
					json=new JSONObject();
					json.put("schemeCode", schemeCode);
					json.put("lSc",map.get(liquidFundId));
					json.put("rSc", map.get(regularFundId));
					json.put("gSc", map.get(growthFundId));
					jsonArray.put(json);
				}
			}
			return jsonArray;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public MutualFund getBySchemeCode(String schemeCode)
	{
		Session hSession = null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			if(schemeCode!=null)
				return (MutualFund)hSession.createQuery("from MutualFund where schemeCode=?").setParameter(0, schemeCode).setMaxResults(1).uniqueResult();
			else
				return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}	
	}

	public String mapId() throws Exception
	{
		StringBuilder builder=null;
		List<String> schemeList=null;
		String todayDate=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
		SerializedObjectToFile sf=new SerializedObjectToFile();
		MutualFundStatus mfStatus=null;
		Map<String,String> mappingExistingMfMap=null;
		File dir=new File("/opt/feapi/MutualFund");
		File mappingFundsFile=new File("/opt/feapi/MutualFund/mfSyncStatus.txt");
		try
		{
			if(!dir.isDirectory())
			{
				dir.mkdirs();
			}
			if(!mappingFundsFile.exists())
			{
				mappingFundsFile.createNewFile();
			}
			Config config=new Config();
			String path="";
			if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
			{
				path=config.getProperty(Config.MAP_MUTUALFUND_LINK_PRODUCTION);
			}
			else
			{
				path=config.getProperty(Config.MAP_MUTUALFUND_LINK_DEVELOPMENT);
			}
			
			String currentDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			int privateKey=("1tradersCockpit.in4"+currentDate+"fe").hashCode();
			
			JSONObject json=new JSONObject();
			json.put("code", privateKey);
			
			URL url = new URL(path);
			URLConnection con=url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(500000);
			con.setReadTimeout(500000);
			System.out.println("After opening connection to URL to update MutualFunds.");
			
			OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
			writer.write(json.toString());
			writer.close(); 
			System.out.println("Sending mutualfund data of length : as "+json.toString().length());
			
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line=br.readLine();
		
			builder=new StringBuilder();
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			br.close();
			json=new JSONObject(builder.toString());
			
			if(json.has("success") && json.getBoolean("success")==true && json.has("mf"))
			{
				System.out.println("  with success=true no of funds = "+json.getJSONArray("mf").length());
				boolean insertSuccessfully=insertMappingsInMutualFund(json.getJSONArray("mf"));
				if(insertSuccessfully==false)
					throw new Exception("Something went wrong while saving MutualFund's mapping data.");
			}
			else if(json.has("success") && json.getBoolean("success"))
			{
				return "";
			}
			else
			{
				System.out.println("  with success=false");
				throw new Exception("Something Went Wrong while fetching MF data.");
			}
			try
			{
				mfStatus=(MutualFundStatus)sf.getSerializedFile(mappingFundsFile);
			}
			catch(EOFException e)
			{
				e.printStackTrace();
				mfStatus=new MutualFundStatus();
			}
			
			mappingExistingMfMap=mfStatus.getUpdateMFIDMap();
			mappingExistingMfMap.put(todayDate, "true");
			sf.serializeObject(mfStatus, mappingFundsFile);
			return "";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			try
			{
				mfStatus=(MutualFundStatus)sf.getSerializedFile(mappingFundsFile);
			}
			catch(EOFException e1)
			{
				e1.printStackTrace();
				mfStatus=new MutualFundStatus();
			}
			
			mappingExistingMfMap=mfStatus.getUpdateMFIDMap();
			mappingExistingMfMap.put(todayDate, "false|"+e.getMessage());
			sf.serializeObject(mfStatus, mappingFundsFile);
			throw e;
		}
	}
	
	public boolean insertMappingsInMutualFund(JSONArray jsonArray)
	{
		Session hSession=null;
		Transaction tx=null;
		FileWriter fw=null;
		BufferedWriter bw=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
	
			for(int i=0;i<jsonArray.length();i++)
			{
				JSONObject json=jsonArray.getJSONObject(i);
				if(json.has("schemeCode"))
				{
					String schemeCode=json.getString("schemeCode");
					System.out.println("SchemeCode="+schemeCode);
					MutualFund mf=(MutualFund)hSession.createQuery("from MutualFund where schemeCode=?").setParameter(0, schemeCode).uniqueResult();
					String lSc=null,rSc=null,gSc=null;
					if(json.has("lSc"))
					{
						lSc=json.getString("lSc");
						//System.out.println("Liquid SchemeCode="+lSc);
						MutualFund liquidFund=(MutualFund)hSession.createQuery("from MutualFund where schemeCode = ?").setString(0, lSc).uniqueResult();
						mf.setLiquidFund(liquidFund);
					}
					if(json.has("rSc"))
					{
						rSc=json.getString("rSc");
						//System.out.println("Regular SchemeCode="+rSc);
						MutualFund regularFund=(MutualFund)hSession.createQuery("from MutualFund where schemeCode = ?").setString(0, rSc).uniqueResult();
						mf.setRegularFund(regularFund);
					}
					if(json.has("gSc"))
					{
						gSc=json.getString("gSc");
						//System.out.println("Growth SchemeCode="+gSc+".");
						MutualFund growthFund=(MutualFund)hSession.createQuery("from MutualFund where schemeCode = ?").setString(0, gSc).uniqueResult();
						mf.setGrowthFund(growthFund);
					}
				}
				if(i%50==0)
				{
					tx=hSession.beginTransaction();
					tx.commit();
					
				}
			}
			tx=hSession.beginTransaction();
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			return false;
		}
		finally
		{
			hSession.close();
		}
	}

	public List<MutualFund> getBlackList(long consumerId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<MutualFund> list=hSession.createQuery("select m from MutualFund m,BlackListFunds blf where m.id=blf.mutualFundId and blf.consumerId=? order by m.name").setLong(0, consumerId).list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	public List<MutualFund> getByFundHouse(long houseId,String type)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			if(type==null)
				return hSession.createQuery("from MutualFund where houseId=? and broaderSchemeType is not null and broaderType is not null and direct!=1").setLong(0, houseId).list();
			else
				return hSession.createQuery("from MutualFund where houseId=? and broaderType=? and broaderSchemeType is not null and broaderType is not null and direct!=1").setLong(0, houseId).setString(1, type).list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	public boolean saveBlackList(String[] fundIdArray,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		List currentBlackList=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			currentBlackList=hSession.createQuery("select mutualFundId from BlackListFunds where consumerId=?").setLong(0, consumerId).list();
			for(String mfId:fundIdArray)
			{
				long mid=Long.valueOf(mfId);
				if(!currentBlackList.contains(mid))
				{
					BlackListFunds blf=new BlackListFunds();
					blf.setConsumerId(consumerId);
					blf.setMutualFundId(mid);
					hSession.save(blf);
					
					Action action=new Action();
					action.setAction(Action.ADD);
					action.setConsumerId(consumerId);
					action.setCreatedOn(new Date());
					action.setMutualFundId(mid);
					action.setTableType(Action.BLACKLIST);
					hSession.save(action);
				}
			}
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public boolean deleteSingleFundFromBlackList(long mfId,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			int row=hSession.createQuery("delete from BlackListFunds where mutualFundId=? and consumerId=?").setLong(0, mfId).setLong(1, consumerId).executeUpdate();
			System.out.println("Success of deleting single blacklist funds="+row);
			
			Action action=new Action();
			action.setAction(Action.REMOVE);
			action.setConsumerId(consumerId);
			action.setCreatedOn(new Date());
			action.setMutualFundId(mfId);
			action.setTableType(Action.BLACKLIST);
			hSession.save(action);
			
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public boolean deleteAllFundFromBlackList(long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> list=hSession.createQuery("select mutualFundId from BlackListFunds where consumerId=? and mutualFundId is not null").setLong(0, consumerId).list();
			for(long mfId:list)
			{
				tx=hSession.beginTransaction();
				int row=hSession.createQuery("delete from BlackListFunds where consumerId=? and mutualFundId=?").setLong(0, consumerId).setLong(1, mfId).executeUpdate();
				
				Action action=new Action();
				action.setAction(Action.REMOVE);
				action.setConsumerId(consumerId);
				action.setCreatedOn(new Date());
				action.setMutualFundId(mfId);
				action.setTableType(Action.BLACKLIST);
				hSession.save(action);
				tx.commit();
			}
			System.out.println("Success of deleting All blacklist funds for consumerId="+consumerId);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public boolean deleteFromBlackListByFundHouse(long fundHouseId,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> list=hSession.createQuery("select b.mutualFundId from BlackListFunds b,MutualFund mf where mf.id=b.mutualFundId and mf.houseId=? and b.consumerId=?").setLong(0,fundHouseId).setLong(1, consumerId).list();
			for(long mfId:list)
			{
				System.out.println("MutualFund ID to delete on basis of fundhouse="+fundHouseId+" mfid="+mfId);
				tx=hSession.beginTransaction();
				int row=hSession.createQuery("delete from BlackListFunds where consumerId=? and mutualFundId=?").setLong(0, consumerId).setLong(1, mfId).executeUpdate();
				
				Action action=new Action();
				action.setAction(Action.REMOVE);
				action.setConsumerId(consumerId);
				action.setCreatedOn(new Date());
				action.setMutualFundId(mfId);
				action.setTableType(Action.BLACKLIST);
				hSession.save(action);
				tx.commit();
			}
			System.out.println("Success of deleting All blacklist with houseId="+fundHouseId+" funds for consumerId="+consumerId);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public List<MutualFund> getRecoList(long consumerId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<MutualFund> list=hSession.createQuery("select m from MutualFund m,Reco r where m.id=r.mutualFundId and r.consumerId=? order by m.name").setLong(0, consumerId).list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	public boolean saveReco(String[] fundIdArray,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		List currentBlackList=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			currentBlackList=hSession.createQuery("select mutualFundId from Reco where consumerId=?").setLong(0, consumerId).list();
			for(String mfId:fundIdArray)
			{
				long mid=Long.valueOf(mfId);
				if(!currentBlackList.contains(mid))
				{
					Reco reco=new Reco();
					reco.setConsumerId(consumerId);
					reco.setMutualFundId(mid);
					hSession.save(reco);
					
					Action action=new Action();
					action.setAction(Action.ADD);
					action.setConsumerId(consumerId);
					action.setCreatedOn(new Date());
					action.setMutualFundId(mid);
					action.setTableType(Action.RECO);
					hSession.save(action);
				}
			}
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public boolean deleteSingleFundFromReco(long mfId,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			int row=hSession.createQuery("delete from Reco where mutualFundId=? and consumerId=?").setLong(0, mfId).setLong(1, consumerId).executeUpdate();
			System.out.println("Success of deleting single Reco fund = "+row);
			
			Action action=new Action();
			action.setAction(Action.REMOVE);
			action.setConsumerId(consumerId);
			action.setCreatedOn(new Date());
			action.setMutualFundId(mfId);
			action.setTableType(Action.RECO);
			hSession.save(action);
			
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public boolean deleteAllFundFromReco(long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> list=hSession.createQuery("select mutualFundId from Reco where consumerId=? and mutualFundId is not null").setLong(0, consumerId).list();
			for(long mfId:list)
			{
				tx=hSession.beginTransaction();
				int row=hSession.createQuery("delete from Reco where consumerId=? and mutualFundId=?").setLong(0, consumerId).setLong(1, mfId).executeUpdate();
				
				Action action=new Action();
				action.setAction(Action.REMOVE);
				action.setConsumerId(consumerId);
				action.setCreatedOn(new Date());
				action.setMutualFundId(mfId);
				action.setTableType(Action.RECO);
				hSession.save(action);
				tx.commit();
			}
			System.out.println("Success of deleting All Reco funds for consumerId="+consumerId);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public boolean deleteFromRecoByFundHouse(long fundHouseId,long consumerId)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Long> list=hSession.createQuery("select b.mutualFundId from Reco b,MutualFund mf where mf.id=b.mutualFundId and mf.houseId=? and b.consumerId=?").setLong(0,fundHouseId).setLong(1, consumerId).list();
			for(long mfId:list)
			{
				System.out.println("MutualFund ID to delete on basis of fundhouse="+fundHouseId+" mfid="+mfId);
				tx=hSession.beginTransaction();
				int row=hSession.createQuery("delete from Reco where consumerId=? and mutualFundId=?").setLong(0, consumerId).setLong(1, mfId).executeUpdate();
				
				Action action=new Action();
				action.setAction(Action.REMOVE);
				action.setConsumerId(consumerId);
				action.setCreatedOn(new Date());
				action.setMutualFundId(mfId);
				action.setTableType(Action.RECO);
				hSession.save(action);
				tx.commit();
			}
			System.out.println("Success of deleting All blacklist with houseId="+fundHouseId+" funds for consumerId="+consumerId);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	
	public MutualFund getMutualFundFromJsonUsingDAO(JSONObject json,Long existingId)
	{
		try
		{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			MutualFund mf=new MutualFund();
			//System.out.println("Received id="+json.getLong("id"));
			/*mf.setId(json.getLong("id"));*/
			mf.setName(json.getString("name"));
			mf.setRtaSchemeCode(json.has("rtaSchemeCode")?json.getString("rtaSchemeCode"):null);
			mf.setSchemeCode(json.has("schemeCode")?json.getString("schemeCode"):null);
			mf.setRoboSchemeCode(json.has("roboSchemeCode")?json.getString("roboSchemeCode"):null);
			mf.setType(json.has("type")?json.getString("type"):null);
			mf.setOptionType(json.has("optionType")?json.getString("optionType"):null);
			mf.setSchemeType(json.has("schemeType")?json.getString("schemeType"):null);
			mf.setComment(json.has("comment")?json.getString("comment"):null);
			mf.setIsin(json.has("isin")?json.getString("isin"):null);
			mf.setCamsSchemeCode(json.has("camsSchemeCode")?json.getString("camsSchemeCode"):null);
			mf.setSettlementType(json.has("settlementType")?json.getString("settlementType"):null);
			mf.setSipFlag(json.has("sipFlag")?json.getBoolean("sipFlag"):null);
			mf.setTransactionMode(json.has("transacctionMode")?json.getString("transacctionMode"):null);
			mf.setPurchaseAllowed(json.has("purchasedAllowed")?json.getBoolean("purchasedAllowed"):null);
			mf.setDematPurchaseAllowed(json.has("dematPurchaseAllowed")?json.getBoolean("dematPurchaseAllowed"):null);
			mf.setPhysicalPurchaseAllowed(json.has("physicalPurchaseAllowed")?json.getBoolean("physicalPurchaseAllowed"):null);
			mf.setMinPurchase(json.has("minPurchase")?json.getDouble("minPurchase"):null);
			mf.setHigh52Week(json.has("high52Week")?json.getDouble("high52Week"):null);
			mf.setLow52Week(json.has("low52Week")?json.getDouble("low52Week"):null);
			mf.setMinAdditionalPurchase(json.has("minAdditionalPurchase")?json.getDouble("minAdditionalPurchase"):null);
			mf.setMinSIPAmount(json.has("minSIPAmount")?json.getDouble("minSIPAmount"):null);
			mf.setMaxSIPAmount(json.has("maxSIPAmount")?json.getDouble("maxSIPAmount"):null);
			mf.setMinNoOfMonths(json.has("minNoOfMonths")?json.getInt("minNoOfMonths"):null);
			mf.setMaxNoOfMonths(json.has("maxNoOfMonths")?json.getInt("maxNoOfMonths"):null);
			mf.setPurchaseMultiple(json.has("purchaseMultiple")?json.getDouble("purchaseMultiple"):null);
			mf.setTimeframe(json.has("timeframe")?json.getInt("timeframe"):null);
			mf.setLockInPeriod(json.has("lockInPeriod")?json.getInt("lockInPeriod"):null);
			mf.setMaxPurchase(json.has("maxPurchase")?json.getDouble("maxPurchase"):null);
			mf.setMinRedemptionAmt(json.has("minRedemptionAmt")?json.getDouble("minRedemptionAmt"):null);
			mf.setMinRedemptionQty(json.has("minRedemptionQty")?json.getDouble("minRedemptionQty"):null);
			mf.setNav(json.has("nav")?json.getDouble("nav"):null);
			mf.setNavChange(json.has("navChange")?json.getDouble("navChange"):null);
			mf.setLastYearNav(json.has("lastYearNav")?json.getDouble("lastYearNav"):null);
			mf.setOneYearReturns(json.has("oneYearReturns")?json.getDouble("oneYearReturns"):null);
			mf.setThreeYearReturns(json.has("threeYearReturns")?json.getDouble("threeYearReturns"):null);
			mf.setFiveYearReturns(json.has("fiveYearReturns")?json.getDouble("fiveYearReturns"):null);
			mf.setOverallReturns(json.has("overallReturns")?json.getDouble("overallReturns"):null);
			mf.setSharpeRatio(json.has("sharpeRatio")?json.getDouble("sharpeRatio"):null);
			mf.setStdDeviation(json.has("stdDeviation")?json.getDouble("stdDeviation"):null);
			mf.setAlpha(json.has("alpha")?json.getDouble("alpha"):null);
			mf.setBeta(json.has("beta")?json.getDouble("beta"):null);
			mf.setrSquaredValue(json.has("rSquaredValue")?json.getDouble("rSquaredValue"):null);
			mf.setUpsideCaptureRatio(json.has("upsideCaptureRatio")?json.getDouble("upsideCaptureRatio"):null);
			mf.setDownsideCaptureRatio(json.has("downsideCaptureRatio")?json.getDouble("downsideCaptureRatio"):null);
			mf.setVroRating(json.has("vroRating")?json.getDouble("vroRating"):null);
			mf.setOurRating(json.has("ourRating")?json.getDouble("ourRating"):null);
			mf.setRoboAvailable(json.has("roboAvailable")?json.getBoolean("roboAvailable"):null);
			mf.setValidated(json.has("validated")?json.getBoolean("validated"):null);
			mf.setRoboSuggested(json.has("roboSuggested")?json.getBoolean("roboSuggested"):null);
			mf.setFlexiSuggested(json.has("flexiSuggested")?json.getBoolean("flexiSuggested"):null);
			mf.setSafeSuggested(json.has("safeSuggested")?json.getBoolean("safeSuggested"):null);
			mf.setTaxSuggested(json.has("taxSuggested")?json.getBoolean("taxSuggested"):null);
			mf.setGrowthSuggested(json.has("growthSuggested")?json.getBoolean("growthSuggested"):null);
			mf.setActive(json.has("active")?json.getBoolean("active"):null);
			mf.setAddedOn(json.has("addedOn")?sdf.parse(json.getString("addedOn")):null);
			mf.setRoboSuggestedOn(json.has("roboSuggestedOn")?sdf.parse(json.getString("roboSuggestedOn")):null);
			mf.setFlexiSuggestedOn(json.has("flexiSuggestedOn")?sdf.parse(json.getString("flexiSuggestedOn")):null);
			mf.setSafeSuggestedOn(json.has("safeSuggestedOn")?sdf.parse(json.getString("safeSuggestedOn")):null);
			mf.setNavUpdatedOn(json.has("navUpdateOn")?sdf.parse(json.getString("navUpdatedOn")):null);
			mf.setStartDate(json.has("startDate")?sdf.parse(json.getString("startDate")):null);
			mf.setEndDate(json.has("endDate")?sdf.parse(json.getString("endDate")):null);
			mf.setLiquidFundId(json.has("liquidFundId")?json.getLong("liquidFundId"):null);
			mf.setAmfiiCode(json.has("amfiiCode")?json.getLong("amfiiCode"):null);
			mf.setBenchmarkIndex(json.has("benchmarkIndex")?json.getString("benchmarkIndex"):null);
			mf.setSchemeSize(json.has("schemeSize")?json.getDouble("schemeSize"):null);
			mf.setExitLoad(json.has("exitLoad")?json.getDouble("exitLoad"):null);
			mf.setEntryLoad(json.has("entryLoad")?json.getDouble("entryLoad"):null);
			mf.setHouseId(json.has("houseId")?json.getLong("houseId"):null);
			mf.setGrowthFundId(json.has("growthFundId")?json.getLong("growthFundId"):null);
			mf.setNonAofGrowthSuggested(json.has("nonAofGrowthSuggested")?json.getBoolean("nonAofGrowthSuggested"):null);
			mf.setDirect(json.has("direct")?json.getBoolean("direct"):null);
			mf.setRegularFundId(json.has("regularFundId")?json.getLong("regularFundId"):null);
			mf.setPlanType(json.has("planType")?json.getString("planType"):null);
			mf.setOpenEnded(json.has("openEnded")?json.getString("openEnded"):null);
			mf.setFundManager(json.has("fundManager")?json.getString("fundManager"):null);
			mf.setStpAllowed(json.has("stpAllowed")?json.getBoolean("stpAllowed"):null);
			mf.setSwpAllowed(json.has("swpAllowed")?json.getBoolean("swpAllowed"):null);
			mf.setSwitchAllowed(json.has("switchAllowed")?json.getBoolean("switchAllowed"):null);
			mf.setNfoFlag(json.has("nfoFlag")?json.getBoolean("nfoFlag"):null);
			mf.setRedemptionAllowed(json.has("redemptionAllowed")?json.getBoolean("redemptionAllowed"):null);
			mf.setUpdatedOn(new Date());
			mf.setBseSchemeType(json.has("bseSchemeType")?json.getString("bseSchemeType"):null);
			mf.setRtaAgentCode(json.has("rtaAgentCode")?json.getString("rtaAgentCode"):null);
			mf.setDividendReinvestmentFlag(json.has("dividendReinvestmentFlag")?json.getString("dividendReinvestmentFlag"):null);
			mf.setYtm(json.has("ytm")?json.getDouble("ytm"):null);
			mf.setModifiedDurationInYears(json.has("modifiedDurationInYears")?json.getDouble("modifiedDurationInYears"):null);
			mf.setTechnicalScore(json.has("technicalScore")?json.getDouble("technicalScore"):null);
			mf.setPersonalScore(json.has("personalScore")?json.getDouble("personalScore"):null);
			mf.setMorningStarAnalystRating(json.has("morningStarAnalystRating")?json.getLong("morningStarAnalystRating"):null);
			mf.setMorningStarRating(json.has("morningStarRating")?json.getLong("morningStarRating"):null);
			mf.setSipAllowed(json.has("sipAllowed")?json.getBoolean("sipAllowed"):null);
			mf.setMinGap(json.has("minGap")?json.getLong("minGap"):null);
			mf.setMaxGap(json.has("maxGap")?json.getLong("maxGap"):null);
			
			/*if(json.has("sipDates"))
			{
				Iterator itr=json.getJSONArray("sipDates").iterator();
				Set<SIPDate> set=new HashSet<SIPDate>();
				SIPDate sipDate=null;
				JSONObject inner=null;
				while(itr.hasNext())
				{
					inner=(JSONObject)itr.next();
					sipDate=new SIPDate();
					sipDate.setFrequencyType(inner.getInt("f"));
					//if(existingId!=null)
						//sipDate.setMutualFundId(existingId);
					sipDate.setSipDate(inner.getInt("s"));
					set.add(sipDate);
				}
				mf.setSipDates(set);
			}*/
			return mf;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
