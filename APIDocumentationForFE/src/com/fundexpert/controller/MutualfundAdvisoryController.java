package com.fundexpert.controller;

import javax.validation.ValidationException;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFund;

public class MutualfundAdvisoryController
{

	public double getAdviceOfMF(String amfiiCode)
	{
		
		
		double rating = 0;
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			MutualFund mf = (MutualFund) session.createQuery("from MutualFund where amfiiCode=?").setString(0, amfiiCode).uniqueResult();
			rating = mf.getOurRating();
			return rating;
		}
		catch (ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return rating;
	}
}