package com.fundexpert.controller;

import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.PortfolioRequest;
import com.fundexpert.dao.PortfolioUpload;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.pojo.Holdings;
import com.fundexpert.pojo.Transactions;
import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.exceptions.InvalidPdfException;

public class PortfolioImportController
{
	List<Holdings> holdingsList = null;
	long userId = 0;
	Session hSession = null;
	org.hibernate.Transaction tx = null;
	List<Map> missingFunds = new ArrayList<Map>();
	String path = null;
	String pdfPassword = null;

	public void sFactory(long userId, List<Holdings> holdingsList) throws BadPasswordException, Exception
	{
		try
		{
			hSession = HibernateBridge.getSessionFactory().openSession();
			this.holdingsList=holdingsList;
			updatePortfolio(hSession, userId);
			System.out.println("Portfolio Uploaded for user="+userId);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(tx != null && tx.isActive())
				tx.rollback();
			throw new Exception(e.getMessage());
		}
		finally
		{
			tx = null;
			hSession.close();
		}
	}

	void updatePortfolio(Session hSession, long userId)
	{
		int holdingSize = holdingsList.size();
		long folioNumber;
		String arn,arnName;
		String folioNumberString, camsCode, mutualFundName;
		double closingUnits, avgNav = 0;
		List<Transactions> transactionsList = null;
		Query q1, q2 = null;
		MutualFund mutualFund = null;
		com.fundexpert.dao.Holding holding = null;
		Iterator itr = holdingsList.iterator();
		Map missingMap = null;
		String contentMistmatchString="";
		long saveHoldingId=0;
		try
		{
			tx = hSession.beginTransaction();
			while (itr.hasNext())
			{
				Holdings tempHolding = (Holdings) itr.next();
				folioNumber = tempHolding.getFolioNumber();
				folioNumberString = tempHolding.getFolioNumberString();
				camsCode = tempHolding.getCamsCode();
				mutualFundName = tempHolding.getMutualFundName();
				closingUnits = tempHolding.getClosingUnits();
				arn=tempHolding.getArn();
				arnName=(String)hSession.createQuery("select name from ArnMapping where arn=?").setString(0,arn).setMaxResults(1).uniqueResult();
				transactionsList = tempHolding.getTransactionsList();
				System.out.println("FolioNumber="+folioNumber);
				System.out.println("FolioNumberString="+folioNumberString);
				System.out.println("cams code="+camsCode);
				System.out.println("mfnmae = "+mutualFundName);
				System.out.println("closing units = "+closingUnits);
				System.out.println("arn = "+arn);
				System.out.println("Tran list= "+transactionsList.size());
				
				Date date = null;
				String trans;
				double amount, units, price;
				//System.out.println("camsCode=" + camsCode);
				q1 = hSession.createQuery("FROM MutualFund where camsSchemeCode=? and settlementType not like ?").setString(0, camsCode).setString(1, "%" + "L" + "%");
				int sizeQ1 = q1.list().size();
				double totalAmount = 0;
				boolean arnUnderGumptionLabsFound=false;
				String arnUnderGumptionLabs="";
				int bseArnId=0;//index in bseIdList which is parallel to bseArnList and gives the id of matching arn at ith index
				Iterator itr1 = transactionsList.iterator();
				//System.out.println("start,mf=" + mutualFundName);
				q2 = null;
				if(sizeQ1 == 0)
				{
					System.out.println("missing mutualFund" + camsCode);
					missingMap = new HashMap();
					missingMap.put("mutualFund", mutualFundName);
					missingMap.put("camsCode", camsCode);
					missingMap.put("folioNumber", folioNumber);
					missingMap.put("closingUnits", closingUnits);
					missingFunds.add(missingMap);
				}
				else if(sizeQ1 == 1)
				{
					/*tx=hSession.beginTransaction();*/
					mutualFund = (MutualFund) q1.list().get(0);
					System.out.println("Found single MutualFund with camsCode="+camsCode+"userId="+userId);
					//System.out.println("Inside first=" + mutualFund.getName() + "folio=" + folioNumberString + "userId=" + userId);
					q2 = hSession.createQuery("from Holding where mutualFundId=? and folioNumber=? and userId=?").setLong(0, mutualFund.getId()).setString(1, folioNumberString).setLong(2, userId);
					//System.out.println("Inside Second=mfId=" + mutualFund.getId() + "   folioNumber=" + folioNumberString + "   Userid=" + userId);
					//System.out.println("NAN=" + q2);
					int sizeQ2 = q2.list().size();
					Set<com.fundexpert.dao.Transaction> setTransaction = new HashSet<com.fundexpert.dao.Transaction>();
					if(sizeQ2 == 0)
					{
						System.out.println("No Holdings Exist with mfId="+mutualFund.getId()+" for userId="+userId);
						Transactions transaction=null;
						while (itr1.hasNext())
						{
							transaction = (Transactions) itr1.next();
							date = transaction.getDate();
							trans = transaction.getTransaction();
							amount = transaction.getAmount();
							units = transaction.getUnits();
							price = transaction.getPrice();
							com.fundexpert.dao.Transaction fundTransaction = new com.fundexpert.dao.Transaction();
							if(amount < 0)
							{
								amount = amount - 2 * amount;
								units = units - 2 * units;
								totalAmount = totalAmount - amount;
								fundTransaction.setAmount(amount);
								fundTransaction.setUnits(units);
								fundTransaction.setAction(2);
							}
							else
							{
								totalAmount = totalAmount + amount;
								fundTransaction.setAmount(amount);
								fundTransaction.setUnits(units);
								fundTransaction.setAction(1);
							}
							fundTransaction.setUserId(userId);
							fundTransaction.setNav(price);
							fundTransaction.setTransactionDate(date);
							fundTransaction.setCreatedOn(new Date());
							setTransaction.add(fundTransaction);
							//System.out.println("UserI" + userId + "price=" + price + "folioNumber=" + folioNumberString + "date" + date);
						}
						holding = new com.fundexpert.dao.Holding();
						holding.setMutualFund(mutualFund);
						holding.setMfName(mutualFundName);
						DecimalFormat df=new DecimalFormat("######.#####");
						closingUnits=Double.valueOf(df.format(closingUnits));
						holding.setUnits(closingUnits);
						holding.setCreatedOn(new Date());
						holding.setUpdatedOn(new Date());
						holding.setUserId(userId);
						holding.setArn(arn);
						holding.setArnName(arnName);
						holding.setFolioNumber(folioNumberString);
						double avg = totalAmount / closingUnits;
						if(Double.isNaN(avg))
							avg = 0;
						if(Double.isInfinite(avg))
							avg = 0;
						//TODO remove
						//DecimalFormat df1=new DecimalFormat("######.#####");
						//avg=Double.valueOf(df1.format(avg));
						holding.setAvgNav(avg);
						if(!setTransaction.isEmpty())
						{
							System.out.println("setting transactions set in holdings with no of transations="+setTransaction.size()+" folioNumberString="+holding.getFolioNumber());
							holding.setTransactions(setTransaction);
						}
						System.out.println("closing=" + closingUnits + "avgNav=" + avg);
						
						saveHoldingId=(long)hSession.save(holding);
					}
					if(sizeQ2 == 1)
					{
						System.out.println("Holdings already exists for mutualfundid="+mutualFund.getId()+" for userId="+userId);
						holding = (com.fundexpert.dao.Holding) q2.list().get(0);
						double avNav = holding.getAvgNav();
						Set<com.fundexpert.dao.Transaction> checkTransaction = new HashSet<com.fundexpert.dao.Transaction>();
						// checkTransaction contains data of transactions in database
						checkTransaction = holding.getTransactions();
						boolean[] transactionPerHolding = null;
						transactionPerHolding = new boolean[checkTransaction.size()];
						if(checkTransaction.size() == 0)
						{
							System.out.println("No transaction exists for");
							Transactions transaction=null;
							while (itr1.hasNext())
							{
								transaction = (Transactions) itr1.next();
								date = transaction.getDate();
								trans = transaction.getTransaction();
								amount = transaction.getAmount();
								units = transaction.getUnits();
								price = transaction.getPrice();
								com.fundexpert.dao.Transaction fundTransaction = new com.fundexpert.dao.Transaction();
								if(amount < 0)
								{
									amount = amount + 2 * amount;
									units = units + 2 * units;
									totalAmount = totalAmount - amount;
									fundTransaction.setAmount(amount);
									fundTransaction.setUnits(units);
									fundTransaction.setAction(2);
								}
								else
								{
									totalAmount = totalAmount + amount;
									fundTransaction.setAmount(amount);
									fundTransaction.setUnits(units);
									fundTransaction.setAction(1);
								}
								fundTransaction.setUserId(userId);
								fundTransaction.setNav(price);
								fundTransaction.setTransactionDate(date);
								setTransaction.add(fundTransaction);
								
							}
							avgNav = totalAmount / closingUnits;
							if(Double.isNaN(avgNav))
								avgNav = 0;
							if(Double.isInfinite(avgNav))
								avgNav = 0;
							holding.setAvgNav(avgNav);
							holding.setUnits(closingUnits);
							holding.setMfName(mutualFundName);
							holding.setTransactions(setTransaction);
							holding.setArn(arn);
							holding.setArnName(arnName);
							saveHoldingId=(long)hSession.save(holding);
						}
						else
						{
							double totalUnits = holding.getUnits();
							double sum_ExistingTransactionAmount=0;
							totalAmount = holding.getAvgNav() * totalUnits;
							System.out.println("Holdings already exists for mutualfundid="+mutualFund.getId()+"Initial Total Amount="+totalAmount);
							avgNav = holding.getAvgNav();
							Set<com.fundexpert.dao.Transaction> transaction1 = checkTransaction;
							double newUnits = holding.getUnits();
							Transactions transaction=null;
							while (itr1.hasNext())
							{
								boolean redemptionFlag=false;
								transaction= (Transactions) itr1.next();
								date = transaction.getDate();
								trans = transaction.getTransaction();
								amount = transaction.getAmount();
								units = transaction.getUnits();
								price = transaction.getPrice();
								// fundTransaction is that transaction that we have to add as new in holdings from pdf
								com.fundexpert.dao.Transaction fundTransaction = new com.fundexpert.dao.Transaction();
								if(amount < 0)
								{
									redemptionFlag=true;
									sum_ExistingTransactionAmount+=amount;
									//System.out.println(amount);
									amount = amount - 2 * amount;
									units = units - 2 * units;
									//totalAmount = totalAmount - amount;
									fundTransaction.setAmount(amount);
									fundTransaction.setUnits(units);
									fundTransaction.setAction(2);
									System.out.println("Negative amount="+amount);
									
									//System.out.println("RedemptionMoney"+"AMount="+amount+"units="+units+"Date="+date+"total="+totalAmount);
								}
								else
								{
									sum_ExistingTransactionAmount+=amount;
									//totalAmount = totalAmount + amount;
									fundTransaction.setAmount(amount);
									fundTransaction.setUnits(units);
									fundTransaction.setAction(1);
									//System.out.println("InvestedMoney"+"AMount="+amount+"units="+units+"Date="+date+"total="+totalAmount);
								}
								fundTransaction.setUserId(userId);
								fundTransaction.setNav(price);
								fundTransaction.setTransactionDate(date);
								fundTransaction.setCreatedOn(new Date());
								Date currTranDate = fundTransaction.getTransactionDate();
								double currTranAmount = fundTransaction.getAmount();
								double currTranPrice = fundTransaction.getNav();
								double currTranUnits = fundTransaction.getUnits();
								Iterator iterate = checkTransaction.iterator();
								boolean transactionNotFound = false;
								int index = 0;
								while (iterate.hasNext())
								{
									com.fundexpert.dao.Transaction tempTransaction = (com.fundexpert.dao.Transaction) iterate.next();
									Date storedTranDate = tempTransaction.getTransactionDate();
									double storedTranAmount = tempTransaction.getAmount();
									double storedTranPrice = tempTransaction.getNav();
									double storedTranUnits = tempTransaction.getUnits();
									
									//System.out.println("currTranDate: "+currTranDate.getTime()+" storedTranDate: "+storedTranDate.getTime());
									//System.out.println("currTranDate: "+currTranDate+" storedTranDate: "+storedTranDate);
									if(transactionPerHolding[index] == false)
									{
										if(storedTranDate.compareTo(currTranDate)==0 && storedTranAmount == currTranAmount && storedTranUnits == currTranUnits && storedTranPrice == currTranPrice)
										{
											transactionPerHolding[index] = true;
											index++;
											transactionNotFound = false;
											//System.out.println("Transaction Matched Found"+storedTranDate);
											// break is used to increase efficiency since if match occurs then we dont want to go further to compare with other transactions in Holdings
											break;
										}
										else
										{
											//System.out.println("o"+storedTranDate);
											// if match does not occur then iterate through last transaction and if no match found then add current transaction i.e fundTransaction to database
											transactionNotFound = true;
											index++;
											continue;
										}
									}
									else
									{
										if(storedTranDate.compareTo(currTranDate)==0 && storedTranAmount == currTranAmount && storedTranUnits == currTranUnits && storedTranPrice == currTranPrice)
										{
											//System.out.println("2nd transaction which is a copy of 1st transaction and 1st transaction was match already and this 2nd transaction has got another match but for the first time with the same transaction with which 1st transaction was matched so don't save it ");
											transactionNotFound = false;
											//suppose db has 3 transactions (pur1,pur2,pur3) and pdf has 5 (p1,p2,p3,p3,r1) : pur/p means purchase
											//(pur1 from db==p1 means purchase from pdf) so there are two transaction in pdf with p3 but only one transaction in db with p3
											//if transactionPerHolding[indexofp3]==true : - this case happens when pur3 is compared with first occurrence of p3
											//and indexOfp3 == no. of transactions in db in this case size of db==3
											//then to persist last(second p3 in this case) p3, below patch of if is included
											if(index==transactionPerHolding.length)
												transactionNotFound = true;
											index++;
											continue;
										}
										else
										{
											//System.out.println("h"+storedTranDate);
											transactionNotFound = true;
											index++;
											continue;
										}
									}
								}
								if(transactionNotFound == true)
								{
									setTransaction.add(fundTransaction);
									newUnits = newUnits + fundTransaction.getUnits();
									if(redemptionFlag==false)
									{
										totalAmount = totalAmount + currTranAmount;
										totalUnits = totalUnits + currTranUnits;
									}
									else
									{
										totalAmount = totalAmount - currTranAmount;
										totalUnits = totalUnits - currTranUnits;
									}
									
									avgNav = totalAmount / totalUnits;
									
									//System.out.println("Total Amount="+totalAmount+"  total units="+totalUnits);
								}
							}
						/*	// send email if sum(transaction amount)-holdingAmount >1 or <-1
							//if pdf has less transaction but holding in db has more, then also we get this email since setTransaction's size>0 only if new transaction is found in pdf
							if(setTransaction!=null && setTransaction.size()==0)
							{
								System.out.println("Holdingid="+holding.getId());
								//1 is used bcoz there might be round of issue but should not be more than 1rs.
								if(totalAmount-sum_ExistingTransactionAmount>1 || totalAmount-sum_ExistingTransactionAmount<-1)
								{
									System.out.println("TotalAmount="+totalAmount+" Sum_ExistingTransactionAmount="+sum_ExistingTransactionAmount);
									//send lead mail to rashi to make respective changes in db on live server
									
									contentMistmatchString+="HoldingId : "+holding.getId()+"\n";
									contentMistmatchString+="MutualFund : "+mutualFund.getName()+"\n";
									contentMistmatchString+="FolioNumber : "+holding.getFolioNumberString()+"\n";
									contentMistmatchString+="Holdings Total Amount : "+totalAmount+"\n";
									contentMistmatchString+="Transactions Total Amount : "+sum_ExistingTransactionAmount+"\n";
									contentMistmatchString+="\n\n";
									
								}
							}*/
							transaction1.addAll(setTransaction);
							if(Double.isNaN(avgNav))
								avgNav = 0;
							if(Double.isInfinite(avgNav))
								avgNav = 0;
							holding.setAvgNav(avgNav);
							holding.setUnits(totalUnits);
							
							if(!transaction1.isEmpty())
							{
								holding.setTransactions(transaction1);
							}
							holding.setArn(arn);	
							holding.setMfName(mutualFundName);
							holding.setArnName(arnName);
							saveHoldingId=(long)hSession.save(holding);
						}
					}
					/*tx.commit();*/
				}
				else if(sizeQ1 > 1)
				{
					System.out.println("Found more than one MutualFund with camsCode="+camsCode);
					//System.out.println("lol");
					List<MutualFund> mfList = q1.list();
					Iterator itr2 = mfList.iterator();
					String mfNameFromPdf = mutualFundName;
					MutualFund mf = null;
					//below two variables are added because of the case in which mutualFund Name in pdf does not contain reinvestment or payout
					boolean notFoundMatchingMutualFund=false;
					MutualFund reinvestMutualFund=null,growthFund=null;
					int flagNameMatching = 0;
					int flagOptionMatching = 0;
					// this while loop checks out for Reinvest/Payout in name of mutualFund from both end i.e database and pdf
					while (itr2.hasNext())
					{
						mf = ((MutualFund) itr2.next());
						//System.out.println("mf.getName()="+mf.getName());
						//below if case is when new
						if(mf.getName().toLowerCase().contains("reinvest"))
						{
							reinvestMutualFund=mf;
							//System.out.println("contains reinvest");
						}
						// System.out.println("Inside While Loop of Duplication Property mf name=" + mf.getName() + "  pdfMf=" + mfNameFromPdf);
						// System.out.println("mfName:   " + mf.getName().toLowerCase());
						// System.out.println("mfPdf:   " + mfNameFromPdf.toLowerCase());
						if(mf.getName().toLowerCase().contains("reinvest") && mfNameFromPdf.toLowerCase().contains("reinvest"))
						{
							// System.out.println("Inside reinvest");
							flagNameMatching = 1;
						}
						if(mf.getName().toLowerCase().contains("payout") && mfNameFromPdf.toLowerCase().contains("payout"))
						{
							// System.out.println("Inside payout");
							flagNameMatching = 2;
						}
						if(mf.getName().toLowerCase().contains("growth") && mfNameFromPdf.toLowerCase().contains("growth"))
						{
							flagNameMatching=3;
							System.out.println("flag name matching in name type of growth");
						}
						if(flagNameMatching == 1 || flagNameMatching == 2 || flagNameMatching==3)
						{
							// the name of pdf mutualFund and database mutualFund both contains either Reinvest/Payout
							// System.out.println("or condition");
							break;
						}
					}
					if(flagNameMatching == 0)
					{
						itr2 = mfList.iterator();
						String growthOption;
						while (itr2.hasNext())
						{
							mf = (MutualFund) itr2.next();
							//below if case is when mfname in pdf does not contain reinvestment or payout
							if(reinvestMutualFund==null && mf.getName().toLowerCase().contains("reinvest"))
							{
								reinvestMutualFund=mf;
							}
							if(mfNameFromPdf.toLowerCase().contains("reinvest") && mf.getOptionType().toLowerCase().contains("reinvest"))
							{
								flagOptionMatching = 1;
							}
							else if(mfNameFromPdf.toLowerCase().contains("payout") && mf.getOptionType().toLowerCase().contains("payout"))
							{
								flagOptionMatching = 2;
							}
							else if(mfNameFromPdf.toLowerCase().contains("growth") && mf.getOptionType().toLowerCase().contains("growth"))
							{
								flagNameMatching=3;
								System.out.println("flag name matching in option type of growth");
							}
							if(flagOptionMatching == 1 || flagOptionMatching == 2 || flagNameMatching==3)
							{
								// this if means either of (Reinvest/Payout) is found in pdfName and pair is found in optionType parameter of MutualFund class
								break;
							}
						}
					}
					if(flagOptionMatching == 0 && flagNameMatching == 0)
					{						
						try
						{						
							//below 2 assignment is when mfname in pdf does not contain reinvestment or payout
							notFoundMatchingMutualFund=true;
							
							//set to 1 so that we can enter below if case
							flagNameMatching=1;
							throw new Exception("Not able to deal with camsCode duplication properly!Since not able to find payout or reinvest term in mutual fund name from database or mutual fund name from pdf");
						}
						catch (Exception e)
						{
							System.out.println(e.getMessage());
						}
					}
					if(flagNameMatching!=0 || flagOptionMatching!=0)
					{
						mutualFund = mf;
						//below if is new and is for when mfname in pdf does not contain reinvestment or payout
						if(notFoundMatchingMutualFund==true && reinvestMutualFund!=null)
						{
							mutualFund=reinvestMutualFund;
						}
						if(flagNameMatching==3 || flagOptionMatching==3)
						{
							q2=null;
							for(int i=0;i<mfList.size();i++)
							{
								mutualFund = ((MutualFund)mfList.get(i)); 
								long mfId = mutualFund.getId();
								if(q2==null || q2.list().size()==0)
								{
									q2 = hSession.createQuery("from Holding where mutualFundId=? and folioNumber=? and userId=?").setLong(0, mfId).setString(1, folioNumberString).setLong(2, userId);
									System.out.println("Growth mfId="+mfId+"q2="+q2+"folioNumberString="+folioNumberString+"userid="+userId+"q2.list.size()="+q2.list().size());
								}
							}
							//System.out.println("After loop q2.size()="+q2.list().size());
						}
						if(mutualFund==null)
						{
							System.out.println("not able to handle mutualfund in pdf with name="+mfNameFromPdf);
							continue;
						}
						if(q2 == null)
						{
							System.out.println("q2 is still null");
							q2 = hSession.createQuery("from Holding where mutualFundId=? and folioNumber=? and userId=?").setLong(0, mutualFund.getId()).setString(1, folioNumberString).setLong(2, userId);
						}
						int sizeQ2 = q2.list().size();
						Set<com.fundexpert.dao.Transaction> setTransaction = new HashSet<com.fundexpert.dao.Transaction>();
						if(sizeQ2 == 0)
						{
							System.out.println("No Holdings exists for mutualfund="+mutualFund.getId());
							Transactions transaction=null;
							while (itr1.hasNext())
							{
								transaction = (Transactions) itr1.next();
								date = transaction.getDate();
								trans = transaction.getTransaction();
								amount = transaction.getAmount();
								units = transaction.getUnits();
								price = transaction.getPrice();
								com.fundexpert.dao.Transaction fundTransaction = new com.fundexpert.dao.Transaction();
								if(amount < 0)
								{
									amount = amount - 2 * amount;
									units = units - 2 * units;
									totalAmount = totalAmount - amount;
									fundTransaction.setAmount(amount);
									fundTransaction.setUnits(units);
									fundTransaction.setAction(2);
								}
								else
								{
									totalAmount = totalAmount + amount;
									fundTransaction.setAmount(amount);
									fundTransaction.setUnits(units);
									fundTransaction.setAction(1);
								}
								fundTransaction.setUserId(userId);
								fundTransaction.setNav(price);
								fundTransaction.setTransactionDate(date);
								fundTransaction.setCreatedOn(new Date());
								setTransaction.add(fundTransaction);
								
							}
							holding = new com.fundexpert.dao.Holding();
							holding.setMutualFund(mutualFund);
							holding.setUnits(closingUnits);
							holding.setCreatedOn(new Date());
							holding.setUpdatedOn(new Date());
							holding.setUserId(userId);
							holding.setFolioNumber(folioNumberString);
							double avg = totalAmount / closingUnits;
							if(Double.isNaN(avg))
								avg = 0;
							if(Double.isInfinite(avg))
								avg = 0;
							holding.setAvgNav(avg);
							if(!setTransaction.isEmpty())
							{
								System.out.println("Setting transactions.");
								holding.setTransactions(setTransaction);
							}
							
							holding.setArn(arn);	
							holding.setArnName(arnName);
							holding.setMfName(mutualFundName);
							saveHoldingId=(long)hSession.save(holding);
						}
						if(sizeQ2 == 1)
						{
							System.out.println("Holdings exists already.muutalfundid="+mutualFund.getId()+" userId="+userId);
							holding = (com.fundexpert.dao.Holding) q2.list().get(0);
							double avNav = holding.getAvgNav();
							Set<com.fundexpert.dao.Transaction> checkTransaction = new HashSet<com.fundexpert.dao.Transaction>();
							// checkTransaction contains data of transactions in database
							checkTransaction = holding.getTransactions();
							boolean[] transactionPerHolding = transactionPerHolding = new boolean[checkTransaction.size()];
							if(checkTransaction.size() == 0)
							{
								System.out.println("No transactions exist for userid="+userId+" mutualfundId="+mutualFund.getId());
								Transactions transaction=null;
								while (itr1.hasNext())
								{
									transaction = (Transactions) itr1.next();
									date = transaction.getDate();
									trans = transaction.getTransaction();
									amount = transaction.getAmount();
									units = transaction.getUnits();
									price = transaction.getPrice();
									com.fundexpert.dao.Transaction fundTransaction = new com.fundexpert.dao.Transaction();
									if(amount < 0)
									{
										amount = amount - 2 * amount;
										units = units - 2 * units;
										totalAmount = totalAmount - amount;
										fundTransaction.setAmount(amount);
										fundTransaction.setUnits(units);
										fundTransaction.setAction(2);
									}
									else
									{
										totalAmount = totalAmount + amount;
										fundTransaction.setAmount(amount);
										fundTransaction.setUnits(units);
										fundTransaction.setAction(1);
									}
									fundTransaction.setUserId(userId);
									fundTransaction.setNav(price);
									fundTransaction.setTransactionDate(date);
									fundTransaction.setCreatedOn(new Date());
									setTransaction.add(fundTransaction);
									
								}
								avgNav = totalAmount / closingUnits;
								if(Double.isNaN(avgNav))
									avgNav = 0;
								if(Double.isInfinite(avgNav))
									avgNav = 0;
								holding.setAvgNav(avgNav);
								holding.setUnits(closingUnits);
								holding.setArn(arn);	
								holding.setArnName(arnName);
								holding.setTransactions(setTransaction);
								holding.setMfName(mutualFundName);
								saveHoldingId=(long)hSession.save(holding);
							}
							else
							{
								System.out.println("Transaction exists already with mutualfundid="+mutualFund.getId()+" userdId="+userId);
								double totalUnits = holding.getUnits();
								double sum_ExistingTransactionAmount=0;
								double newUnits = holding.getUnits();
								totalAmount = holding.getAvgNav() * totalUnits;
								avgNav = holding.getAvgNav();
								Set<com.fundexpert.dao.Transaction> transaction1 = checkTransaction;
								// double newUnits = holding.getUnits();
								Transactions transaction=null;
								while (itr1.hasNext())
								{
									boolean redemptionFlag=false;
									transaction = (Transactions) itr1.next();
									date = transaction.getDate();
									trans = transaction.getTransaction();
									amount = transaction.getAmount();
									units = transaction.getUnits();
									price = transaction.getPrice();
									// fundTransaction is that transaction that we have to add as new in holdings from pdf
									com.fundexpert.dao.Transaction fundTransaction = new com.fundexpert.dao.Transaction();
									if(amount < 0)
									{
										redemptionFlag=true;
										sum_ExistingTransactionAmount+=amount;
										amount = amount - 2 * amount;
										units = units - 2 * units;
									//	totalAmount = totalAmount - amount;
										fundTransaction.setAmount(amount);
										fundTransaction.setUnits(units);
										fundTransaction.setAction(2);
									}
									else
									{
										sum_ExistingTransactionAmount+=amount;
									//	totalAmount = totalAmount + amount;
										fundTransaction.setAmount(amount);
										fundTransaction.setUnits(units);
										fundTransaction.setAction(1);
									}
									fundTransaction.setUserId(userId);
									fundTransaction.setNav(price);
									fundTransaction.setTransactionDate(date);
									fundTransaction.setCreatedOn(new Date());
									Date currTranDate = fundTransaction.getTransactionDate();
									double currTranAmount = fundTransaction.getAmount();
									double currTranPrice = fundTransaction.getNav();
									double currTranUnits = fundTransaction.getUnits();
									Iterator iterate = checkTransaction.iterator();
									boolean transactionNotFound = false;
									int index = 0;
									while (iterate.hasNext())
									{
										com.fundexpert.dao.Transaction tempTransaction = (com.fundexpert.dao.Transaction) iterate.next();
										Date storedTranDate = tempTransaction.getTransactionDate();
										double storedTranAmount = tempTransaction.getAmount();
										double storedTranPrice = tempTransaction.getNav();
										double storedTranUnits = tempTransaction.getUnits();
										System.out.println("storedTranDate="+storedTranDate+"currTranDate="+currTranDate+" storedTraAmount="+storedTranAmount+" currTranAmount="+currTranAmount+" storedTranUnits="+storedTranUnits+" currTranUnits="+currTranUnits+"storedTranPrice="+storedTranPrice+" currTranPrice="+currTranPrice);
										System.out.println("storedTranTime="+storedTranDate.getTime()+" currTranTime="+currTranDate.getTime());
										if(transactionPerHolding[index] == false)
										{
											if(storedTranDate.compareTo(currTranDate)==0 && storedTranAmount == currTranAmount && storedTranUnits == currTranUnits && storedTranPrice == currTranPrice)
											{
												// if match occures then we dont want to add transaction which matches at the first tym hence commented below
												System.out.println("Match 1.1=date"+currTranDate+"  units="+currTranUnits+"totalAmount="+totalAmount);
												transactionPerHolding[index] = true;
												index++;
												transactionNotFound = false;
												// break is used to increase efficiency since if match occurs then we dont want to go further to compare with other transactions in Holdings
												break;
											}
											else
											{
												// if match does not occur then iterate through last transaction and if no match found then add fundTransaction to database
												System.out.println("Match 1.2=date"+currTranDate+"  units="+currTranUnits+"totalAmount="+totalAmount);
												transactionNotFound = true;
												index++;
												continue;
											}
										}
										else
										{
											if(storedTranDate.compareTo(currTranDate)==0 && storedTranAmount == currTranAmount && storedTranUnits == currTranUnits && storedTranPrice == currTranPrice)
											{
												//System.out.println("2nd transaction which is a copy of 1st transaction and 1st transaction was match already and this 2nd transaction has got  match but for the first time with the same transaction(from database) with which 1st transaction(from pdf) was matched so don't save it ");
												System.out.println("Match 2.1=date"+currTranDate+"  units="+currTranUnits+"totalAmount="+totalAmount);
												
												transactionNotFound = false;
												//suppose db has 3 transactions (pur1,pur2,pur3) and pdf has 5 (p1,p2,p3,p3,r1)
												//(pur1 means purchase from db==p1 means purchase from pdf) so there are two transaction in pdf with p3 but only one transaction in db with p3
												//if transactionPerHolding[indexofp3]==true : - this case happens when pur3 is compared with first occurrence of p3
												//and indexOfp3 == no. of transactions in db in this case size of db==3
												//then to persist last(in this case second) p3, below patch of if is included
												if(index==transactionPerHolding.length)
													transactionNotFound = true;
												index++;
												continue;
											}
											else
											{
												 //System.out.println("match already occured and after no match found" + "totalUnits=" + totalUnits + "totalAMount=" + totalAmount);
												System.out.println("Match 2.2=date"+currTranDate+"  units="+currTranUnits+"totalAmount="+totalAmount);
												transactionNotFound = true;
												index++;
												continue;
											}
										}
									}
									if(transactionNotFound == true)
									{
										setTransaction.add(fundTransaction);
										newUnits = newUnits + fundTransaction.getUnits();
										if(redemptionFlag==false)
										{
											totalAmount = totalAmount + currTranAmount;
											totalUnits = totalUnits + currTranUnits;
										}
										else
										{
											totalAmount = totalAmount - currTranAmount;
											totalUnits = totalUnits - currTranUnits;
										}
										
										avgNav = totalAmount / totalUnits;
										
										/*setTransaction.add(fundTransaction);
										// newUnits = newUnits + fundTransaction.getUnits();
										System.out.println("currTransAmount="+currTranAmount);
										totalAmount = totalAmount + currTranAmount;
										totalUnits = totalUnits + currTranUnits;
										avgNav = totalAmount / totalUnits;
										System.out.println("tA="+totalAmount+" totalUnits="+totalUnits);
										if(bseArnList.contains(transaction.getArn()) &&  arnUnderGumptionLabsFound==false)
										{
											arnUnderGumptionLabsFound=true;
											arnUnderGumptionLabs=transaction.getArn();
											bseArnId=bseArnList.indexOf(arnUnderGumptionLabs);
										}*/
									}
								}
								// send email if sum(transaction amount)-holdingAmount >1 or <-1
								//if pdf has less transaction but holding in db has more, then also we get this email since setTransaction's size>0 only if new transaction is found in pdf
								/*if(setTransaction!=null && setTransaction.size()==0)
								{
									System.out.println("Holdingid="+holding.getId());
									//1 is used bcoz there might be round of issue but should not be more than 1rs.
									if(totalAmount-sum_ExistingTransactionAmount>1 || totalAmount-sum_ExistingTransactionAmount<-1)
									{
										System.out.println("TotalAmount="+totalAmount+" Sum_ExistingTransactionAmount="+sum_ExistingTransactionAmount);
										//send lead mail to rashi to make respective changes in db on live server
										contentMistmatchString+="HoldingId : "+holding.getId()+"\n";
										contentMistmatchString+="MutualFund : "+mutualFund.getName()+"\n";
										contentMistmatchString+="FolioNumber : "+holding.getFolioNumberString()+"\n";
										contentMistmatchString+="Holdings Total Amount : "+totalAmount+"\n";
										contentMistmatchString+="Transactions Total Amount : "+sum_ExistingTransactionAmount+"\n";
										contentMistmatchString+="\n\n";
									}
								}*/
								transaction1.addAll(setTransaction);
								if(Double.isNaN(avgNav))
									avgNav = 0;
								if(Double.isInfinite(avgNav))
									avgNav = 0;
								holding.setAvgNav(avgNav);
								holding.setUnits(totalUnits);

								if(!transaction1.isEmpty())
								{
									holding.setTransactions(transaction1);
								}
								holding.setArn(arn);	
								holding.setArnName(arnName);
								holding.setMfName(mutualFundName);
								saveHoldingId=(long)hSession.save(holding);
							}
						}
					}
				}
				/*System.out.println("NEW HOLDING ID = "+saveHoldingId);
				Set<com.fundexpert.dao.Transaction> list=holding.getTransactions();
				for(com.fundexpert.dao.Transaction t:list)
				{
					System.out.println("T.getId()="+t.getId());
				}*/
			}
			/*if(contentMistmatchString!=null && !contentMistmatchString.equals(""))
			{
				final long uId=userId;
				final String mismatchHoldings=contentMistmatchString;
				Thread thread=new Thread(new Runnable(){
					
					public void run()
					{
						try {
							SendMail sendMail=new SendMail();
							sendMail.setRecipient("somesh.c@gumptionlabs.com");
							sendMail.setSubject("Holdings Amount and sum of Transaction Amount Mismatch for userId - "+uId);
							sendMail.setMessage(mismatchHoldings);
							sendMail.setAddedBy(addedBy);
							sendMail.send();
							
							SendMail sendMail1=new SendMail();
							sendMail1.setRecipient("vikas.sharma@gumptionlabs.com");
							sendMail1.setSubject("Holdings Amount and sum of Transaction Amount Mismatch for userId - "+uId);
							sendMail1.setMessage(mismatchHoldings);
							sendMail1.setAddedBy(addedBy);
							sendMail1.send();
							
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						} catch (MessagingException e) {
							e.printStackTrace();
						}
					}
				});
				thread.start();
			}*/
			System.out.println("Commiting Transaction");
			tx.commit();
		}
		catch (Exception e)
		{
			if(tx != null)
				tx.rollback();
			e.printStackTrace();
			throw e;
		}
	}

	public List<Map> getMissingFunds()
	{
		return missingFunds;
	}

	public Map<String,List<Holdings>> createHoldingFromJsonArray(JSONArray jsonArray) throws FundexpertException
	{
		List<Holdings> list=null;
		List<Holdings> missingList=null;
		double units;
		String isin,marketCap,name;
		Session hSession=null;
		Map<String,List<Holdings>> map=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			list=new ArrayList<Holdings>();
			missingList=new ArrayList<Holdings>();
			map=new HashMap<String,List<Holdings>>();
			map.put("success", list);
			map.put("missing", missingList);
			for(Object obj:jsonArray)
			{
				JSONObject json=(JSONObject)obj;
				if(!json.has("isin") || !json.has("units") || !json.has("sn"))
				{
					continue;
				}
				isin=json.getString("isin").trim();
				units=Double.valueOf(json.getString("units").trim());
				//marketCap=json.getString("mc").trim();
				
				name=json.getString("sn").trim();
				MutualFund mf=(MutualFund)hSession.createQuery("from MutualFund where isin=?").setString(0, isin).setMaxResults(1).uniqueResult();
				if(mf==null)
				{
					Holdings missingHolding=new Holdings();
					missingHolding.setClosingUnits(units);
					missingHolding.setIsin(isin);
					missingList.add(missingHolding);
					continue;
				}
				Holdings holding=new Holdings();
				holding.setAvgNav(0.0);
				holding.setClosingUnits(units);
				holding.setMutualFundName(name);
				holding.setCamsCode(mf.getCamsSchemeCode());
				holding.setFolioNumberString("");
				list.add(holding);
			}
			return map;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	public Map<String,List<Holdings>> createHoldingFromJsonArrayWithId(JSONArray jsonArray) throws FundexpertException
	{
		List<Holdings> list=null;
		List<Holdings> missingList=null;
		double units;
		String isin=null,marketCap,name;
		Long mfId=null;
		Session hSession=null;
		Map<String,List<Holdings>> map=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			list=new ArrayList<Holdings>();
			missingList=new ArrayList<Holdings>();
			map=new HashMap<String,List<Holdings>>();
			map.put("success", list);
			map.put("missing", missingList);
			for(Object obj:jsonArray)
			{
				JSONObject json=(JSONObject)obj;
				if(!json.has("id") || !json.has("units"))
				{
					continue;
				}
				mfId=json.getLong("id");
				units=Double.valueOf(json.getDouble("units"));
				MutualFund mf=(MutualFund)hSession.createQuery("from MutualFund where id=?").setLong(0, mfId).setMaxResults(1).uniqueResult();
				
				if(mf==null)
				{
					Holdings missingHolding=new Holdings();
					missingHolding.setClosingUnits(units);
					missingHolding.setIsin(isin);
					missingList.add(missingHolding);
					continue;
				}
				Holdings holding=new Holdings();
				holding.setAvgNav(0.0);
				holding.setClosingUnits(units);
				holding.setMutualFundName(mf.getName());
				holding.setCamsCode(mf.getCamsSchemeCode());
				holding.setFolioNumberString("");
				list.add(holding);
			}
			return map;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
}