package com.fundexpert.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holding;

public class PortfolioCleanController
{
	public void cleanPortfolio(Long mutualfundId, String folioNumber, Calendar date) 
	{
		Session session=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=null;
		try
		{
			tx=session.beginTransaction();
			
			String qry="from Holding where";
			if(mutualfundId!=null)
				qry+=" amfiiCode=?";
			else if(folioNumber!=null)
				qry+=" and folioNumber=? "; 
			
			Query q=session.createQuery(qry);
			
			int i=0;
			if(mutualfundId!=null)
			{
				q.setParameter(i, mutualfundId);
				i++;
			}	
			else if(folioNumber!=null)
			{
				q.setParameter(i, folioNumber);
				i++;
			} 
			
			Holding holding=(Holding)q.uniqueResult();			
			if(holding!=null)
			{
				System.out.println("holding Exist.");
				String qry1="from Transaction where holdingId=? ";
				 if(date!=null)
						qry1+="and transactionDate<=?";
				 Query q1=session.createQuery(qry1).setLong(0, holding.getId());
					int j=1;
					if(date!=null)
					{
						q1.setParameter(j, date.getTime());
						j++;
					}
				 
				List<com.fundexpert.dao.Transaction> list=q1.list();
				if(list.size()!=0)
				for(com.fundexpert.dao.Transaction tr: list)
				{
					session.delete(tr);
				}	
				
				session.delete(holding);
				tx.commit();
			}
			else
			{	
				System.out.println("NO Holding Exist.");
			}
		}catch(Exception e){
			if(tx!=null)
				tx.rollback();
			e.printStackTrace();
		}finally{
			session.close();
		}			
	}

	public Boolean isExistInPortfolio(Long mfId, Long userID) 
	{

		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{ 
			Holding holding = (Holding) session.createQuery("from Holding where mutualfundId=? and userId=? and folioNumber=?").setLong(0, mfId).setLong(1, userID).setMaxResults(1).uniqueResult();
			if(holding != null)
			{
				System.out.println("Mutual; Fund Exist in PF with Mutual Fund ID: "+mfId);
				return true;
			}
			else
			{
				System.out.println("Mutual; Fund does not Exist in PF with Mutual Fund ID: "+mfId); 
				return false;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return false;		
	}
}