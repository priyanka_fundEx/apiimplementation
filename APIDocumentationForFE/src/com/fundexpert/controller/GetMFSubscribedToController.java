package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFundSubscription;
import com.fundexpert.dao.User;

public class GetMFSubscribedToController 
{
	public List<String> getMutualfundSubscribedTo(String userId) 
	{
		List<String> list=new ArrayList<String>();
		
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			User user=(User)session.createQuery("from User where userId=?").setString(0, userId).uniqueResult();
			List<MutualFundSubscription> li=session.createQuery("from MutualFundSubscription where userId=?").setLong(0, user.getId()).list();
			for(MutualFundSubscription mfs:li)
			{
				String amfiiCode=(String)session.createQuery("select amfiiCode from MutualFund where id=?").setLong(0, mfs.getMutualfundId()).uniqueResult();
				list.add(amfiiCode);
			}
			return list;
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return list;
	}
}
