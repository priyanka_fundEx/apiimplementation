package com.fundexpert.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.validation.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.PortfolioSubscription;
import com.fundexpert.dao.User;

public class SubscribePortfolioManagementController {

	public boolean subscribeToProtfolio(int noOfMonths, String userId, Long consumerId) throws ParseException
	{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Session session=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=null;
		try{
			tx=session.beginTransaction();
			User user=(User)session.createQuery("from User where userId=? and consumerId=?").setString(0, userId).setLong(1, consumerId).uniqueResult();
			PortfolioSubscription ps=new PortfolioSubscription();
			ps.setUserId(user.getId());
			
			Calendar endCal=Calendar.getInstance();
			endCal.add(Calendar.MONTH, 12);
			String st=sdf.format(endCal.getTime());
			java.util.Date d1=sdf.parse(st);
			java.sql.Date endDate = new java.sql.Date(d1.getTime());
			ps.setEndDate(endDate);
			
			Calendar cal=Calendar.getInstance();
			String str=sdf.format(cal.getTime());
			java.util.Date d=sdf.parse(str);
			java.sql.Date dateDB = new java.sql.Date(d.getTime());			
			ps.setStartDate(dateDB);
			
			session.save(ps);
			tx.commit();
			System.out.println("Portfolio Subscription is done successfully.");
			return true;
		}
		catch(ValidationException e)
		{
			tx.rollback();
			System.out.println("Exception. "+e.getMessage());
			e.printStackTrace();
		}
		finally{
			session.close();
		}		
		return false;
	}
}