package com.fundexpert.controller;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Asset;
import com.fundexpert.dao.AssetsInSubSegment;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.SectorAllocation;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.StocksInStockSegment;
import com.fundexpert.dao.SubSegmentAllocation;

public class SectorAllocationForRebalance {
	
	public List<SectorAllocation> getDistinctSegment(int riskProfile)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Query query=hSession.createQuery("from SectorAllocation where riskProfile=?").setInteger(0, riskProfile);
			List<SectorAllocation> list=query.list();
			System.out.println("list is is"+list);
			return list;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally {
			if(hSession!=null)
				hSession.close();
		}
	}
	public List<Stocks> getStocks(String segment)
	{
		Session hSession=null;
		
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Query query=hSession.createQuery("from Stocks where segment=?").setString(0,segment);
			List<Stocks> list=query.list();
			
			System.out.println("LIST is"+list);
			
			return list;
			
			
			
			
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally {
			if(hSession!=null)
				hSession.close();
		}
		
	} 
	public boolean submit(String search_stock, int riskProfile,String[] scCode,long consumerId)
	{
		Session hSession=null;
		org.hibernate.Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			
			for(int i=0;i<scCode.length;i++) {
		    StocksInStockSegment s=	(StocksInStockSegment)hSession.createQuery("from StocksInStockSegment where scCode=? and consumerId=? and riskProfile=?").setInteger(0, Integer.parseInt(scCode[i])).setLong(1, consumerId).setInteger(2, riskProfile).setMaxResults(1).uniqueResult();
		    System.out.println("s is..................................."+s);
		    if(s==null) {
			StocksInStockSegment stocksInStockSegment = new StocksInStockSegment();
			stocksInStockSegment.setConsumerId(2);
			System.out.println("riskProfile"+riskProfile);
			System.out.println("Integer.parseInt(scCode[i])"+Integer.parseInt(scCode[i]));
			System.out.println("search_stock"+search_stock);
			
			stocksInStockSegment.setRiskProfile(riskProfile);
			stocksInStockSegment.setScCode(Integer.parseInt(scCode[i]));
			stocksInStockSegment.setSegment(search_stock);
			Stocks sc=(Stocks)hSession.createQuery("from Stocks where scCode=?").setInteger(0, Integer.parseInt(scCode[i])).setMaxResults(1).uniqueResult();
			stocksInStockSegment.setStock(sc);
			hSession.save(stocksInStockSegment);
		    }
			
			}
			tx.commit();	
			return true;	
		}
			
			
		
				
			
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			return false;
		}
		finally
		{
			hSession.close();
		}
		
	}
	public List<Object[]> getAllExistingStocks()
	{
		Session hSession=null;
		
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
	
			Query query=hSession.createQuery("select sc.scCode,sc.segment,sc.scName ,sc.type from Stocks sc ,StocksInStockSegment sisg where sc.scCode=sisg.scCode order by sc.segment");
			if(query!=null)
			{
			List<Object[]> rows=query.list();
			return rows;
			}
			
			
			
			
			
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			
		}
		finally {
			if(hSession!=null)
				hSession.close();
		}
		return null;
	}
	/*********************************************AssestsInSubSegment***********************************************************/
	 
	public List<MutualFund> getFunds(String type,long id)
	{
		Session hSession=null;
		
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Query query=hSession.createQuery("from MutualFund where type=? and houseId=?").setString(0,type).setLong(1, id);
			List<MutualFund> list=query.list();
			
			System.out.println("LIST is"+list);
			
			return list;
			
			
			
			
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally {
			if(hSession!=null)
				hSession.close();
		}
		
	} 
	public List<Stocks> getStocksOfType(String type)
	{
		Session hSession=null;
		
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Query query=hSession.createQuery("from Stocks where type=? and closePrice is not null and closePrice!=0 and block=0 and delisted=0").setString(0,type);
			List<Stocks> list=query.list();
			
			System.out.println("LIST is"+list);
			
			return list;
			
			
			
			
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally {
			if(hSession!=null)
				hSession.close();
		}
		
	} 
	public boolean assestsStockSubmit(String subSegment , int riskProfile,String[] scCode,long consumerId)
	{
		Session hSession=null;
		org.hibernate.Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			
			for(int i=0;i<scCode.length;i++) 
			{
				AssetsInSubSegment assetsInSubSegment=	(AssetsInSubSegment)hSession.createQuery("from AssetsInSubSegment where assetId=? and consumerId=? and riskProfile=?").setLong(0, Long.parseLong(scCode[i])).setLong(1, consumerId).setInteger(2, riskProfile).setMaxResults(1).uniqueResult();
				System.out.println("s is..................................."+assetsInSubSegment);
			    if( assetsInSubSegment==null) 
			    {
					System.out.println("riskProfile"+riskProfile);
					System.out.println("Integer.parseInt(scCode[i])"+Long.parseLong(scCode[i]));
					System.out.println("search_stock"+subSegment);
					
					AssetsInSubSegment assetsInSubSegment1 = new AssetsInSubSegment();
					assetsInSubSegment1.setRiskProfile(riskProfile);
					assetsInSubSegment1.setAssetId(Long.parseLong(scCode[i]));
					assetsInSubSegment1.setSubSegment(subSegment);
					assetsInSubSegment1.setAssetType(Asset.STOCK_ASSET_TYPE);
					assetsInSubSegment1.setConsumerId(consumerId);
					hSession.save(assetsInSubSegment1);
			    }
			}
			tx.commit();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
	public boolean assestsFundsSubmit(String subSegment , int riskProfile,String[] id,long consumerId)
	{
		Session hSession=null;
		org.hibernate.Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			
			for(int i=0;i<id.length;i++)
			{
				AssetsInSubSegment assetsInSubSegment=	(AssetsInSubSegment)hSession.createQuery("from AssetsInSubSegment where assetId=? and consumerId=? and riskProfile = ?").setLong(0, Long.parseLong(id[i])).setLong(1, consumerId).setInteger(2, riskProfile).setMaxResults(1).uniqueResult();
			    System.out.println("s is..................................."+assetsInSubSegment);
			    if( assetsInSubSegment==null)
			    {
			    	System.out.println("riskProfile"+riskProfile);
			    	System.out.println("Integer.parseInt(id[i])"+Long.parseLong(id[i]));
			    	System.out.println("search_stock"+subSegment);
				
			    	AssetsInSubSegment assetsInSubSegment1 = new AssetsInSubSegment();
			    	assetsInSubSegment1.setRiskProfile(riskProfile);
			    	assetsInSubSegment1.setAssetId(Long.parseLong(id[i]));
			    	assetsInSubSegment1.setSubSegment(subSegment);
			    	assetsInSubSegment1.setAssetType(Asset.MUTUALFUND_ASSET_TYPE);
			    	assetsInSubSegment1.setConsumerId(consumerId);
				
			    	hSession.save(assetsInSubSegment1);
			    }
			
			}
			tx.commit();	
			return true;	
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			return false;
		}
		finally
		{
			hSession.close();
		}
		
	}
	public List<Object[]> getViewStocks(int riskProfile) 
	{
		
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			System.out.println("riskProfile is"+riskProfile);
			Query query=hSession.createQuery("select asts.subSegment, asts.assetId ,st.scName from AssetsInSubSegment asts ,Stocks st where asts.assetId=st.scCode and asts.riskProfile=? and assetType=?").setInteger(0, riskProfile).setInteger(1, 2);
			if(query!=null)
			{
			List<Object[]> rows=query.list();
			return rows;
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			hSession.close();
		}
		
	return null;
	}
	public List<Object[]> getViewFunds(int riskProfile) 
	{
		
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			System.out.println("riskProfile is"+riskProfile);
			Query query=hSession.createQuery("select asts.subSegment, asts.assetId ,mf.name from AssetsInSubSegment asts ,MutualFund mf where asts.assetId=mf.id and asts.riskProfile=? and assetType=?").setInteger(0, riskProfile).setInteger(1, 1);
			if(query!=null)
			{
			List<Object[]> rows=query.list();
			return rows;
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			hSession.close();
		}
		
	return null;
	}
	public boolean deleteView(long assetId)
	{
		 Session hSession=null;
		 Transaction tx=null;
		try {
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			Query query = hSession.createQuery("DELETE from AssetsInSubSegment WHERE assetId=?").setLong(0, assetId);
			int i=query.executeUpdate();
			tx.commit();
			System.out.println("Deleted rows = "+i+" for assetId="+assetId);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			if(tx!=null && tx.isActive())
				tx.rollback();
			return false;
		}finally {
			hSession.close();
		}
	}
	/* List<Integer> list1=sectorAllocationForRebalance.getViewRiskProfile(assetType);*/
	public List<Integer> getViewRiskProfile(int assetType,long consumerId){
		Session hSession=null;
		try {
			hSession=HibernateBridge.getSessionFactory().openSession();
			Query query2= hSession.createQuery("select distinct riskProfile from AssetsInSubSegment where consumerId=? and assetType=?").setLong(0,consumerId).setInteger(1, assetType);  
    		
     	    List<Integer> list1=query2.list();
     	    return list1;
		}
		catch(Exception e) {
			return null;
		}
		finally {
			hSession.close();
		}
	}
	/*--------------------SectorAllocation------------------------------------------------------------*/
	public boolean submitSectorAllocation(long consumerId,int riskProfile,int allocation,String sector,int i) {
	 Session hSession=null;
		org.hibernate.Transaction tx=null;
		try
		{
			System.out.println("controller.......................");
			System.out.println("j is"+i);
			hSession=HibernateBridge.getSessionFactory().openSession();
			tx=hSession.beginTransaction();
			
		    Stocks sc= (Stocks)hSession.createQuery("from Stocks where segment=?").setString(0, sector).setMaxResults(1).uniqueResult();
		    if(sc!=null) 
		    {
		    	if(i==1) 
				{
					System.out.println("i"+i);
				    Query query = hSession.createQuery("DELETE SectorAllocation WHERE riskProfile=?").setInteger(0, riskProfile);
					query.executeUpdate();
				}
			SectorAllocation sectorAllocation =	(SectorAllocation)hSession.createQuery("from SectorAllocation where consumerId=? and riskProfile=? and sector=?").setLong(0, consumerId).setInteger(1,  riskProfile).setString(2,sector).setMaxResults(1).uniqueResult();
			if(sectorAllocation!=null)
			{/*
				System.out.println("sectorAllocation!=null");
				sectorAllocation.setAllocation(allocation);
				sectorAllocation.setConsumerId(consumerId);
				sectorAllocation.setRiskProfile(riskProfile);
				sectorAllocation.setSector(sector);
				hSession.update(sectorAllocation);*/
				
			}
			if(sectorAllocation==null)
				
			{
				System.out.println("sectorAllocation==null");
				SectorAllocation sectorAllocation1= new SectorAllocation();
				sectorAllocation1.setAllocation(allocation);
				sectorAllocation1.setConsumerId(consumerId);
				sectorAllocation1.setRiskProfile(riskProfile);
				sectorAllocation1.setSector(sector);
				hSession.save(sectorAllocation1);
				
			}
			System.out.println("end..................");
			tx.commit();
			}
			return true;
			
		}
		
		catch(Exception e) {
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			return false;
		}
		finally {
			hSession.close();
		}
		
	}
	public List<SectorAllocation> viewSectorAllocation(int riskProfile)
	{
		 Session hSession=null;
		try {
			hSession=HibernateBridge.getSessionFactory().openSession();
			Query query=hSession.createQuery("from SectorAllocation where riskProfile=?").setInteger(0, riskProfile);
			List<SectorAllocation> list=query.list();
			return list;
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			hSession.close();
		}
		
	}
	public List<SubSegmentAllocation> viewSubSegmentAllocation(int riskProfile)
	{
		 Session hSession=null;
		try {
			hSession=HibernateBridge.getSessionFactory().openSession();
			Query query=hSession.createQuery("from SubSegmentAllocation where riskProfile=?").setInteger(0, riskProfile);
			List<SubSegmentAllocation> list=query.list();
			return list;
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			hSession.close();
		}
		
	}
	/*-----------------------------------------------------SubSegmentAllocation-----------------------------------*/
	public boolean submitsubSegmentAllocation(long consumerId,int riskProfile,int allocation,String subSegment,int i) {
		 Session hSession=null;
			org.hibernate.Transaction tx=null;
			try
			{
				System.out.println("controller.......................");
				hSession=HibernateBridge.getSessionFactory().openSession();
				tx=hSession.beginTransaction();
				
				if(i==1) 
				{
					System.out.println("i"+i);
				    Query query = hSession.createQuery("DELETE SubSegmentAllocation WHERE riskProfile=?").setInteger(0, riskProfile);
					query.executeUpdate();
				}
				SubSegmentAllocation subSegmentAllocation =	(SubSegmentAllocation)hSession.createQuery("from SubSegmentAllocation where consumerId=? and riskProfile=? and subSegment=?").setLong(0, consumerId).setInteger(1,  riskProfile).setString(2, subSegment).setMaxResults(1).uniqueResult();
				if(subSegmentAllocation!=null)
				{
					/*subSegmentAllocation.setAllocation(allocation);
					subSegmentAllocation.setConsumerId(consumerId);
					subSegmentAllocation.setRiskProfile(riskProfile);
					subSegmentAllocation.setSubSegment(subSegment);;
					hSession.update(subSegmentAllocation);*/
					
				}
				if(subSegmentAllocation==null)
					
				{
					System.out.println("hello");
					SubSegmentAllocation subSegmentAllocation1=new SubSegmentAllocation();
					subSegmentAllocation1.setAllocation(allocation);
					subSegmentAllocation1.setConsumerId(consumerId);
					subSegmentAllocation1.setRiskProfile(riskProfile);
					subSegmentAllocation1.setSubSegment(subSegment);;
					hSession.save(subSegmentAllocation1);
					
				}
				System.out.println("end..................");
				tx.commit();
				return true;
				
			}
			
			catch(Exception e) {
				e.printStackTrace();
				if(tx!=null)
					tx.rollback();
				return false;
			}
			finally {
				hSession.close();
			}
			
		}
	
}
