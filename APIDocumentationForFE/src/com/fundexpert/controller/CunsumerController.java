package com.fundexpert.controller;

import java.util.List;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;

public class CunsumerController 
{
	// This Function will check Login Details AppID and and SecretKey, will verify details
	public Boolean checkLoginDetails(String appId, String secretKey, String ipAddress)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Consumer c=(Consumer)hSession.createQuery("from Consumer where appId=? and secretKey=?")
					.setString(0, appId).setString(1, secretKey).uniqueResult();
			
			if(c.isIpHardcoded())
			{
				if(c.getIpAddress().equals(ipAddress) && c.getSecretKey().equals(secretKey))
					return true;
			}
			else if(!c.isIpHardcoded())
			{
				if(c.getSecretKey().equals(secretKey))
					return true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}

	public List<Consumer> getDistinctConsumers()
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<Consumer> list=hSession.createQuery("from Consumer").list();
			return list;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	// This Function will check Login Details AppID and and SecretKey, will verify details
	public Consumer getConsumer(String appId)
	{
		Session hSession=HibernateBridge.getSessionFactory().openSession();
		try
		{
			Consumer c=(Consumer)hSession.createQuery("from Consumer where appId=?")
					.setString(0, appId).uniqueResult();
			return c;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
}