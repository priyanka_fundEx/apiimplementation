package com.fundexpert.controller;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.User;

public class PortfolioCreateController
{

	public Long getMFId(String amfiiCode, int optionType)
	{
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			MutualFund mf = (MutualFund) session.createQuery("from MutualFund where amfiiCode=? and optionType=?").setString(0, amfiiCode).setInteger(1, optionType).uniqueResult();
			if(mf != null)
				return mf.getId();
			else
				return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return null;
	}
	
	public List<MutualFund> getMFIdList(String amfiiCode)
	{
		List<MutualFund> list=null;
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			list = session.createQuery("from MutualFund where amfiiCode=?").setString(0, amfiiCode).list();
			if(list.size() != 0)
				return list;
			else
				return list;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return list;
	}

	public Long getUserId(String userId, Long consumerId)
	{
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			User user = (User) session.createQuery("from User where userId=? and consumerId=? ").setString(0, userId).setLong(1, consumerId).uniqueResult();
			if(user != null)
				return user.getId();
			else
				return null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return null;
	}

	public boolean isHoldingExist(String folioNumber, Long mfId, Long userID)
	{
		Session session = HibernateBridge.getSessionFactory().openSession();
		try
		{
			// System.out.println("MFID: "+mfId+", folioNumber: "+folioNumber+", userID: "+userID);
			Holding holding = (Holding) session.createQuery("from Holding where mutualfundId=? and userId=? and folioNumber=?").setLong(0, mfId).setLong(1, userID).setString(2, folioNumber).setMaxResults(1).uniqueResult();
			if(holding != null)
			{
				//System.out.println("holding Exist.");
				return true;
			}
			else
			{
				//System.out.println("holding does not Exist.");
				return false;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return false;
	}

	public Boolean updateHolding(String folioNumber, Long mfId, Long userID, int action, Double units, Double nav, Date transactionDate) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Session session = HibernateBridge.getSessionFactory().openSession();

		Calendar cal = Calendar.getInstance();
		String str = sdf.format(cal.getTime());
		java.util.Date d = sdf.parse(str);
		java.sql.Date dateDB = new java.sql.Date(d.getTime());

		java.sql.Date date = new java.sql.Date(d.getTime());

		Transaction tx = null;
		try
		{
			tx = session.beginTransaction();
			Holding holding = (Holding) session.createQuery("from Holding where mutualfundId=? and userId=? and folioNumber=?").setLong(0, mfId).setLong(1, userID).setString(2, folioNumber).setMaxResults(1).uniqueResult();
			if(holding != null)
			{
				System.out.println("Avg NAV: "+holding.getAvgNav()+", units:  "+holding.getUnits()+", MFID:  "+holding.getMutualfundId());
				com.fundexpert.dao.Transaction tr = new com.fundexpert.dao.Transaction();
				tr.setUserId(userID);
				tr.setCreatedOn(dateDB);
				tr.setTransactionDate(date);
				tr.setHoldingId(holding.getId());

				double value = holding.getAvgNav() * holding.getUnits();
				double amount = nav * units;
				tr.setAmount(amount);		
				tr.setNav(nav);
				tr.setUnits(units);

				if(action == 1) 
				{
					tr.setAction(action);

					units = holding.getUnits() + units;

					value += amount;
					nav = value / units;
					//System.out.println("old NAV: " + holding.getAvgNav() + ",   New NAV:  " + nav);
					holding.setAvgNav(nav);
					//System.out.println("old Units: " + holding.getUnits() + ",   New Units:  " + units);
					holding.setUnits(units);
					//System.out.println("Avg NAV: "+holding.getAvgNav()+", units:  "+holding.getUnits()+", MFID:  "+holding.getMutualfundId());
					
					session.save(holding);
					session.save(tr);
					// tx.commit();
					System.out.println("Holding updated and a new Transaction saved successfully. Here Action is invest");
				}
				else if(action == 2)
				{
					tr.setAction(action);

					units = holding.getUnits() - units;

					value = value - amount;
					nav = value / units;
					if(value > 0)
					{
						//System.out.println("old NAV: " + holding.getAvgNav() + ",   New NAV:  " + nav);
						holding.setAvgNav(value / units);
						//System.out.println("old Units: " + holding.getUnits() + ",   New Units:  " + units);
						holding.setUnits(units);

						session.save(holding);
						session.save(tr);
						// tx.commit();
						System.out.println("Holding updated and a new Transaction saved successfully.Here Action is REDEEM");
					}
					else
						throw new ValidationException("You do not have enough Amount in your Portfolio to redeem.");
				}
				return true;
			}
		}
		catch (Exception e)
		{
			if(tx != null)
				tx.rollback();
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return false;
	}

	public Boolean saveHolding(String folioNumber, Long userID, Long mfId, int action, Double units, Double nav, Date transactionDate) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Session session = HibernateBridge.getSessionFactory().openSession();

		Calendar cal = Calendar.getInstance();
		String str = sdf.format(cal.getTime());
		java.util.Date d = sdf.parse(str);
		java.sql.Date dateDB = new java.sql.Date(d.getTime());

		java.sql.Date date = new java.sql.Date(d.getTime());

		Transaction tx = null;
		try
		{
			tx = session.beginTransaction();

			Holding holding = new Holding();
			holding.setMutualfundId(mfId);
			holding.setFolioNumber(folioNumber);
			holding.setUserId(userID);
			holding.setCreatedOn(dateDB);
			holding.setAvgNav(nav);
			holding.setUnits(units);
			Long holdingId = (Long) session.save(holding);
			
			double amount = nav * units;
			if(action == 1)
			{
				com.fundexpert.dao.Transaction tr = new com.fundexpert.dao.Transaction();
				tr.setUserId(userID);
				tr.setCreatedOn(dateDB);
				tr.setTransactionDate(date);
				tr.setHoldingId(holdingId);
	 
				tr.setAction(action);
				tr.setAmount(amount);
				tr.setNav(nav);
				tr.setUnits(units);
				session.save(tr);

				// tx.commit();
				System.out.println("Holding and Transaction saved successfully.");
			}
			else if(action == 2)
			{
				throw new ValidationException("You do not have any transactions till now in your Portfolio to Redeem.");
			}
		}
		catch (Exception e)
		{
			if(tx != null)
				tx.rollback();
			e.printStackTrace();
		}
		finally
		{
			session.close();
		}
		return false;
	}
}