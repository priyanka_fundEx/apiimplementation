package com.fundexpert.controller;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.StocksHoldings;
import com.fundexpert.dao.Transaction;
import com.fundexpert.dao.User;


//Important

//this class opens Hibernate session only once per so use carefully and call closeSession(); when we are done with this instance
public class UserWiseAdvisorController {

	Set<Holding> holdings=new HashSet<Holding>();
	Session hSession;
	PortfolioAdvise portfolioAdvise=null;
	User user=null;
	double totalInvestedMutualfundPortfolioValue=0;
	double totalMFCurrentEquityValue=0;//sum of only MF equity
	double totalCurrentMutualFundPortflioValue=0,totalCurrentStockPortfolioValue=0;
	double totalEquityPortfolioValue;//sum(totalMFCurrentEquityValue+totalCurrentStockPortfolioValue)
	double totalPortfolioValue=0;//sum(totalCurrentMutualFundPortflioValue+totalCurrentStockPortfolioValue)
	double equityPercentage=0;
	DecimalFormat df=new DecimalFormat("0.00000");
	double sum_of_no_of_days_mul_curr_hol_value=0;
	double totalLargeCapEquity=0,totalMidCapEquity=0,totalSmallCapEquity=0;
	Advise advise=null;
	
	public UserWiseAdvisorController(long userId)
	{
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			user=(User)hSession.get(User.class, userId);
			holdings.addAll(user.getHoldings());
			portfolioAdvise=new PortfolioAdvise();
			advise=new Advise();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			closeSession();
		}
		
	}

	public Map<Long,Advise> getAdvise() 
	{
		Map<Long,Advise> map=new HashMap<Long,Advise>();
		try
		{
			for(Holding h:holdings)
			{
				double currentHoldingValue=(h.getUnits()*(h.getMutualFund().getNav()));
				//System.out.println("Holding id="+h.getId()+" current Holding value="+currentHoldingValue);
				//System.out.println(df.format(currentHoldingValue)+" newCurrentHoldingValue h.getId="+h.getId());
				totalCurrentMutualFundPortflioValue+=currentHoldingValue;
				//System.out.println("Total Current MutualFundPortfolio="+df.format(totalCurrentMutualFundPortflioValue));
				advise=new Advise();
				double array[]=getRealisedUnrealisedPL(h);
				double realizedPL=Double.isNaN(array[0])?0:array[0];
				double unrealizedPL=Double.isNaN(array[1])?0:array[1];
				advise.realizedPL=String.valueOf(realizedPL);
				advise.unrealizedPL=(unrealizedPL<1 && unrealizedPL>-1)?null:String.valueOf(unrealizedPL);
				//System.out.println("Realised PL="+realisedPL);
				//System.out.println("UnRealized PL="+advise.getUnrelizedPL());
				double investmentValue=Math.round(h.getAvgNav()*h.getUnits());
				totalInvestedMutualfundPortfolioValue+=investmentValue;
				map.put(h.getId(), advise);
				
				MutualFund mf=h.getMutualFund();
				if(mf.getSchemeType().toLowerCase().contains("hybrid"))
				{
					//System.out.println("mf.getId()="+mf.getId()+" schemeType="+mf.getSchemeType()+" type="+mf.getType());
					totalMFCurrentEquityValue+=currentHoldingValue*.65;
					if(mf.getType().toLowerCase().contains("large cap & mid cap"))
					{
						totalLargeCapEquity+=currentHoldingValue*.65*.5;
						totalMidCapEquity+=currentHoldingValue*.65*.5;
					}
					else if(mf.getType().toLowerCase().contains("large cap"))
					{
						totalLargeCapEquity+=currentHoldingValue;
					}
					else if(mf.getType().toLowerCase().contains("mid cap"))
					{
						totalMidCapEquity+=currentHoldingValue;
					}
					else if(mf.getType().toLowerCase().contains("small cap"))
					{
						totalSmallCapEquity+=currentHoldingValue;
					}
				}
				if(mf.getSchemeType().toLowerCase().contains("equity"))
				{
					//System.out.println("mf.getId()="+mf.getId()+" schemeType="+mf.getSchemeType()+" type="+mf.getType());
					totalMFCurrentEquityValue+=currentHoldingValue;
				}
				if(mf.getBroaderSchemeType()!=null && mf.getBroaderSchemeType().toLowerCase().contains("equity"))
				{
					if(mf.getBroaderType()!=null && mf.getBroaderType().toLowerCase().contains("large cap & mid cap"))
					{
						totalLargeCapEquity+=currentHoldingValue*.5;
						totalMidCapEquity+=currentHoldingValue*.5;
					}
					else if(mf.getBroaderType()!=null && mf.getBroaderType().toLowerCase().contains("large cap"))
					{
						totalLargeCapEquity+=currentHoldingValue;
					}
					else if(mf.getBroaderType()!=null && mf.getBroaderType().toLowerCase().contains("mid cap"))
					{
						totalMidCapEquity+=currentHoldingValue;
					}
					else if(mf.getBroaderType()!=null && mf.getBroaderType().toLowerCase().contains("small cap"))
					{
						totalSmallCapEquity+=currentHoldingValue;
						//System.out.println("SMALL CAP FUND="+totalSmallCapEquity);
					}
				}
				//System.out.println("Total of Small Cap = "+totalSmallCapEquity);
				if(currentHoldingValue>0)
				{
					Date minDate=(Date)hSession.createQuery("select min(transactionDate) from Transaction where holdingId=?").setLong(0,h.getId()).uniqueResult();
					Date maxDate=new Date();
					//System.out.println(minDate+"  "+maxDate+" min & max = h.getId()="+h.getId());
					Calendar minCal=Calendar.getInstance();
					minCal.setTime(minDate);
	
					Calendar maxCal=Calendar.getInstance();
					maxCal.setTime(maxDate);
					
					long min=minCal.getTimeInMillis();
					long max=maxCal.getTimeInMillis();
					
					long days=(max-min)/(1000*60*60*24);
					sum_of_no_of_days_mul_curr_hol_value+=days*currentHoldingValue;
				}
				//System.out.println(days+" days");
			}
			return map;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			closeSession();
		}
		return null;
	}
	
	//returns realised in first index and unrelised in second one
	double[] getRealisedUnrealisedPL(Holding holding)
	{
		/*
		 * 3) investment value for each holding - Sum of all the buy investment value of all transactions in this folio/holdings . Please note investment values and not current value
			
			4) RealizedPL  : for this first calculate Average NAV of the holding that is InvestmentValue as calculated above divided by total NAV of all Buy transactions.

			Realized Profit needs to be calculated if any redeem is there else 0. If redeem is there than this is nothing but TotalRedeemValue - (AvergaeNav*totalunitssold)

		 */
		Set<Transaction> transactions=holding.getTransactions();
		boolean redeemTransaction=false;
		double totalRedeemUnits=0.0,totalRedeemValue=0.0,totalBuyUnits=0.0;
		double buyTotalNav=0,buyHoldingInvestmentValue=0;
		double[] array=new double[2];
		double currentMutualFundNav=holding.getMutualFund().getNav();
		try
		{
			
			for(Transaction t:transactions)
			{
				if(t.getAction()==2)
				{
					redeemTransaction=true;
					double u=t.getUnits();
					totalRedeemUnits+=u;
					totalRedeemValue+=t.getAmount();
				}
				else if(t.getAction()==1)
				{
					buyHoldingInvestmentValue+=t.getAmount();
					totalBuyUnits+=t.getUnits();
				}
			}
			double averageBuyNav=buyHoldingInvestmentValue/totalBuyUnits;
			double averageSellNav=totalRedeemValue/totalRedeemUnits;
			
			//System.out.println("MFID="+holding.getMutualfundId()+" Avg Buy NAV="+averageBuyNav+" Avg Sell Nav="+averageSellNav);
			array[0] = totalRedeemUnits*(averageSellNav-averageBuyNav);
			array[1] = (totalBuyUnits-totalRedeemUnits)*(currentMutualFundNav-averageBuyNav);
			return array;

		}
		catch(Exception e)
		{
			e.printStackTrace();
			closeSession();
		}
		return null;
	}
	
	Double getCurrentHoldingValue(Holding holding)
	{
		//assuming nav from MutualFund gives current Value
		try
		{
			return (holding.getUnits()*holding.getMutualFund().getNav());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			closeSession();
		}
		return 0d;
	}
	//holding level advise
	public class Advise
	{
		public String realizedPL="0";
		public String unrealizedPL="0";
		public String getRealizedPL() {
			return realizedPL;
		}
		public String getUnrealizedPL() {
			return unrealizedPL;
		}
	}
	//portfolio level advise
	public class PortfolioAdvise
	{
		public String investmentStyle="";//aggresive,conservative,moderate
		public String investmentHorizon="";//Large Cap,Small Cap,Mid Cap
		public String investmentPreference="";//Large Cap,Small Cap,Mid Cap
		public String totalInvestedMutualFundPortfolioValue="";
		public String totalCurrentMutualFundPortfolioValue="";
		public String getInvestmentStyle(){
			return investmentStyle;
		}
		public String getInvestmentHorizon() {
			return investmentHorizon;
		}
		public String getInvestmentPreference() {
			return investmentPreference;
		}
		public String getTotalInvestedMutualFundPortfolioValue() {
			return totalInvestedMutualFundPortfolioValue;
		}
		public String getTotalCurrentMutualFundPortfolioValue() {
			return totalCurrentMutualFundPortfolioValue;
		}
	}
	
	public double getInvestmentStyle() 
	{
		Set<StocksHoldings> stockHoldingsList=null;
		try
		{
			stockHoldingsList=user.getStocksHoldings();
			if(stockHoldingsList!=null && stockHoldingsList.size()!=0)
			{
				for(StocksHoldings h:stockHoldingsList)
				{
					totalCurrentStockPortfolioValue+=h.getStocks().getClosePrice()*h.getNumberOfShares();
				}
			}
			System.out.println("totalCurrentStockPortfolioValue="+totalCurrentStockPortfolioValue);
			System.out.println("totalCurrentMutualFundPortflioValue"+df.format(totalCurrentMutualFundPortflioValue));
			System.out.println("totalMFCurrentEquityValue="+totalMFCurrentEquityValue);
			
			totalPortfolioValue=totalCurrentStockPortfolioValue+totalCurrentMutualFundPortflioValue;
			totalEquityPortfolioValue=totalCurrentStockPortfolioValue+totalMFCurrentEquityValue;
			System.out.println("totalEquityPortfolioValue="+totalEquityPortfolioValue);
			System.out.println("totalPortfolioValue="+df.format(totalPortfolioValue));
			
			equityPercentage=(totalEquityPortfolioValue/totalPortfolioValue)*100.0;
			System.out.println("EquityPercentage="+equityPercentage);
			return equityPercentage;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			closeSession();
			return 0;
		}
	}
	
	public Double getInvestmentHorizon()
	{
		try
		{
			System.out.println("sum_of_no_of_days_mul_curr_hol_value"+sum_of_no_of_days_mul_curr_hol_value);
			System.out.println("totalCurrentMutualFundPortflioValue"+totalCurrentMutualFundPortflioValue);
			return sum_of_no_of_days_mul_curr_hol_value/totalCurrentMutualFundPortflioValue;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			closeSession();
			return null;
		}
	}
	
	public String getInvestmentPreference()
	{
		try
		{
			if(totalLargeCapEquity > totalMidCapEquity && totalLargeCapEquity > totalSmallCapEquity)
				return "Large Cap";
			if(totalMidCapEquity > totalLargeCapEquity && totalMidCapEquity > totalSmallCapEquity)
				return "Mid Cap";
			if(totalSmallCapEquity > totalLargeCapEquity && totalSmallCapEquity > totalMidCapEquity)
				return "Small Cap";
			return "NA";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			closeSession();
			return null;
		}
	}
	
	public PortfolioAdvise getPortfolioAdvise()
	{
		try
		{
			double style=getInvestmentStyle();
			String investmentStyle="";
			if(style>=80.0)
				investmentStyle="Aggresive";
			if(style>=40.0 && style<80.0)
				investmentStyle="Moderate";
			if(style<40.0)
				investmentStyle="Conservative";
			
			double horizon=getInvestmentHorizon();
			String investmentHorizon="";
			if(horizon<365)
				investmentHorizon="Short Term";
			else if(horizon>=365 && horizon<1095)
				investmentHorizon="Medium Term";
			else if(horizon>=1095)
				investmentHorizon="Long Term";
			
			portfolioAdvise.investmentStyle=investmentStyle;
			portfolioAdvise.investmentHorizon=investmentHorizon;
			portfolioAdvise.investmentPreference=getInvestmentPreference();
			portfolioAdvise.totalInvestedMutualFundPortfolioValue=df.format(totalInvestedMutualfundPortfolioValue);
			portfolioAdvise.totalCurrentMutualFundPortfolioValue=df.format(totalCurrentMutualFundPortflioValue);
			return portfolioAdvise;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			closeSession();
		}
		return null;
	}
	
	public void closeSession()
	{
		try
		{
			if(hSession!=null && hSession.isConnected())
				hSession.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
