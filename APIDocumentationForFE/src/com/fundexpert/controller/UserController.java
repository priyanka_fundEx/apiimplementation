package com.fundexpert.controller;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.User;

public class UserController {

	public User getUser(String userId,String password)
	{
		Session hSession=null;
		
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			User user=(User)hSession.createQuery("from User where userId=? and password=?").setString(0, userId).setString(1, password.hashCode()+"").setMaxResults(1).uniqueResult();
			return user;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	
	public User getAdminUser(String userId,String password)
	{
		Session hSession=null;
		
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			User user=(User)hSession.createQuery("from User where userId=? and password=? and role!=0").setString(0, userId).setString(1, password.hashCode()+"").setMaxResults(1).uniqueResult();
			return user;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}

	public User getUser(Long userId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			return (User)hSession.get(User.class, userId);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
}
