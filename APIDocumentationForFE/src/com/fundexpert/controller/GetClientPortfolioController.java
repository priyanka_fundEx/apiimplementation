package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Transaction;
import com.fundexpert.dao.User;

public class GetClientPortfolioController 
{
	public Long getUserId(String userId, Long consumerId)
	{
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{	System.out.println(userId+",   ID: "+consumerId);
			User user=(User)session.createQuery("from User where userId=? and consumerId=? ").setString(0, userId).setLong(1, consumerId).uniqueResult();
			if(user!=null)
				return user.getId();
			else
				return null;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}		
		return null;
	}
	public MutualFund getMutualFund(Long mfId)
	{
		MutualFund mf=null;

		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			mf=(MutualFund)session.createQuery("from MutualFund where id=?").setLong(0, mfId).uniqueResult();
			if(mf!=null)
			{
				return mf;
			}
			else
				return mf;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}		
		return mf;
	}
	
	public List<Holding> getListOfHoldings(Long userID)
	{
		List<Holding> list=new ArrayList<Holding>();
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			list=session.createQuery("from Holding where userId=?").setLong(0, userID).list();
			if(list.size()!=0)
				return list;
			else
				return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}		
		return list;
	}
	public List<Transaction> getTransactions(Holding holding)
	{
		List<Transaction> list=new ArrayList<Transaction>();
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			list=session.createQuery("from Transaction where holdingId=?").setLong(0, holding.getId()).list();
			if(list.size()!=0)
				return list;
			else
				return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}		
		return null;
	}
}