package com.fundexpert.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fundexpert.exception.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.Sessionn;
import com.fundexpert.dao.User; 
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.util.Verify;

public class RegisterUserController
{
	public Long saveUser(Long consumerId,String emailId, String pan, String sessionId) throws ParseException, ValidationException
	{
		String userId=null;
		Long uId=null;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Calendar cal=Calendar.getInstance();
		String str=sdf.format(cal.getTime());
		java.util.Date d=sdf.parse(str);
		java.sql.Date dateDB = new java.sql.Date(d.getTime());
		
		Calendar limitCal=Calendar.getInstance();
		Calendar nowCal=Calendar.getInstance();
		nowCal.add(Calendar.MINUTE, -15);
		System.out.println(limitCal.getTime()+", 	Now Cal before 15 minutes: "+sdf.format(nowCal.getTime()));
		
		Session session=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=null;
		try{
			if((emailId!=null && !Verify.emailValidate(emailId)))
			{
				throw new com.fundexpert.exception.ValidationException("EmaiId does not satisfy standard pattern.");
			}
			if((pan!=null && !Verify.panValidate(pan)))
			{
				throw new com.fundexpert.exception.ValidationException("Pan does not satisfy standard pattern.");
			}
			tx=session.beginTransaction();
			Consumer consumer=(Consumer)session.createQuery("from Consumer where id=?").setLong(0, consumerId).uniqueResult();
			Sessionn sess=(Sessionn) session.createQuery("from Sessionn where consumerId=? and sessionId=? order by id DESC")
					.setLong(0, consumerId).setString(1, sessionId).setMaxResults(1).uniqueResult();
			// List<Sessionn> list=session.createQuery("from Sessionn where consumerId=?").setLong(0, consumerId).list();
			
			if(sess!=null)
			{
				System.out.println("inside session!=null : "+sess.getId()+",  "+sess.getLastUsedOn());
				if((sess.getLastUsedOn()).compareTo(nowCal.getTime())>0)
				{
					User user=(User)session.createQuery("from User where email=? and pan=? and consumerId=?").setString(0,emailId).setString(1, pan).setLong(2,consumerId).setMaxResults(1).uniqueResult();
					if(user==null)
					{
						user=new User();
						user.setConsumerId(consumerId);
						
						UUID uuid=UUID.randomUUID();
						userId=uuid.toString().substring(15);
						user.setUserId(userId);

						user.setEmail(emailId);
						user.setPan(pan);
						user.setCreatedOn(dateDB);
						user.setActive(true);
						user.setRole(0);
						session.save(user);
						sess.setLastUsedOn(dateDB);
						session.save(sess);
						System.out.println("Session updated seccessfully.");
						
						consumer.setTokensConsumed(consumer.getTokensConsumed()+1);
						session.update(consumer);
						System.out.println("Consumer has been updated Successfully.");
						tx.commit();
						System.out.println("New User has been Saved Successfully.");
						// System.out.println("loop SI: "+sess.getSessionId()+", CI "+sess.getConsumerId()+", Co: "+sess.getCreatedOn()+", LUO: "+sess.getLastUsedOn());
					}
					uId=user.getId();
				}
				else
				{
					throw new ValidationException("Session Expired.");
				}		
			}
			else
				throw new ValidationException("Invalid Session ID.");
			return uId;
		}
		catch(ValidationException ve)
		{
			ve.printStackTrace();
			throw ve;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally{
			session.close();
		}
		return null;
	}	
	
	public User getUser(String emailId,String pan,long consumerId)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			User user=(User)hSession.createQuery("from User where email=? and pan=? and consumerId=?").setString(0, emailId).setString(1, pan).setLong(2,consumerId).uniqueResult();
			return user;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
	public User getUser(long id)
	{
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			User user=(User)hSession.get(User.class,id);
			return user;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		return null;
	}
}