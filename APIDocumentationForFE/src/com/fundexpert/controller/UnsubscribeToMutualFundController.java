package com.fundexpert.controller;

import javax.validation.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.MutualFundSubscription;
import com.fundexpert.dao.User;

public class UnsubscribeToMutualFundController 
{

	public Boolean unsubscribeToMF(String amfiiCode, String userId, Long consumerId) 
	{
		Session session=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=null;
		try{
			tx=session.beginTransaction();
			User user=(User)session.createQuery("from User where userId=? and consumerId=?").setString(0, userId).setLong(1, consumerId).uniqueResult();
			MutualFund mf=(MutualFund)session.createQuery("from MutualFund where amfiiCode=?").setString(0, amfiiCode).uniqueResult();
			MutualFundSubscription mfs=(MutualFundSubscription)session.createQuery("from MutualFundSubscription where userId=? and mutualfundId=?").setLong(0, user.getId()).setLong(1, mf.getId()).uniqueResult();
			if(mfs!=null)
			{
				session.delete(mfs);
				tx.commit();
				return true;
			}
			else{
				throw new ValidationException("Not a valid Amfii Code.");
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception. "+e.getMessage()); 
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return false;
	}
	
}
