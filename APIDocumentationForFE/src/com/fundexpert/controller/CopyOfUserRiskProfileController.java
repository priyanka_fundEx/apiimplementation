package com.fundexpert.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.Sessionn;
import com.fundexpert.dao.User;


public class CopyOfUserRiskProfileController 
{
	public Long getClient(String appId, String ipAddress)
	{
		Long consumerId=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			Consumer c=(Consumer) session.createQuery("from Consumer where appId=?").setString(0, appId).uniqueResult();
			if(c!=null)
			{
				consumerId=c.getId();
				if(c.isIpHardcoded())
				{
					if(c.getIpAddress().equals(ipAddress))
						return consumerId;
					else
						throw new ValidationException("Requested from a different IP Address and IP Hardcoded is true.");
				}
				else if(!c.isIpHardcoded())
					return consumerId;			
			}
			else
				throw new ValidationException("Not a valid Consumer! OR Enter Valid AppId.");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return consumerId;
	}
	
	public Boolean getValidSession(Long consumerId, String sessionId) 
	{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Calendar limitCal=Calendar.getInstance();
		Calendar nowCal=Calendar.getInstance();
		nowCal.add(Calendar.MINUTE, -15);
		System.out.println(limitCal.getTime()+", 	Now Cal before 15 minutes: "+sdf.format(nowCal.getTime()));
		
		Session session=HibernateBridge.getSessionFactory().openSession();
		try{
			Sessionn sess=(Sessionn) session.createQuery("from Sessionn where consumerId=? and sessionId=? order by id DESC")
					.setLong(0, consumerId).setString(1, sessionId).setMaxResults(1).uniqueResult();
			if(sess!=null)
			{
				System.out.println("inside session!=null : "+sess.getId()+",  "+sess.getLastUsedOn());
				if((sess.getLastUsedOn()).compareTo(nowCal.getTime())>0)
				{
					return true;
				}
				else
					throw new ValidationException("Session Expired.");
				}
		}
		catch(ValidationException e)
		{
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return false;
	}
	
	public String getSessionClient(String ipAddress, Long consumerId, String sessionId) throws ParseException
	{
		String userId=null;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Calendar cal=Calendar.getInstance();
		String str=sdf.format(cal.getTime());
		java.util.Date d=sdf.parse(str);
		java.sql.Date dateDB = new java.sql.Date(d.getTime());
		
		Calendar limitCal=Calendar.getInstance();
		Calendar nowCal=Calendar.getInstance();
		nowCal.add(Calendar.MINUTE, -15);
		System.out.println(limitCal.getTime()+", 	Now Cal before 15 minutes: "+sdf.format(nowCal.getTime()));
		
		Session session=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=null;
		try{
			tx=session.beginTransaction();
			Consumer consumer=(Consumer)session.createQuery("from Consumer where id=?").setLong(0, consumerId).uniqueResult();
			Sessionn sess=(Sessionn) session.createQuery("from Sessionn where consumerId=? and sessionId=? order by id DESC")
					.setLong(0, consumerId).setString(1, sessionId).setMaxResults(1).uniqueResult();
			// List<Sessionn> list=session.createQuery("from Sessionn where consumerId=?").setLong(0, consumerId).list();
			
			if(sess!=null)
			{
				System.out.println("inside session!=null : "+sess.getId()+",  "+sess.getLastUsedOn());
				if((sess.getLastUsedOn()).compareTo(nowCal.getTime())>0)
				{
					if(sess.getIpAddress().equals(ipAddress))
					{
						User user=new User();
						user.setConsumerId(consumerId);
						
						UUID uuid=UUID.randomUUID();
						userId=uuid.toString().substring(15);
						user.setUserId(userId);
						
						user.setCreatedOn(dateDB);
						user.setActive(true);
						session.save(user);
						
						sess.setLastUsedOn(limitCal.getTime());
						session.save(sess);
						System.out.println("Session updated seccessfully.");
						
						consumer.setTokensConsumed(consumer.getTokensConsumed()+1);
						session.update(consumer);
						System.out.println("Consumer has been updated Successfully.");
						tx.commit();
						System.out.println("New User has been Saved Successfully.");
						// System.out.println("loop SI: "+sess.getSessionId()+", CI "+sess.getConsumerId()+", Co: "+sess.getCreatedOn()+", LUO: "+sess.getLastUsedOn());
					}
					else
						throw new ValidationException("Requested from a Different Ip Address and IP hardcoded is ");
				}
				else
				{
					throw new ValidationException("Session Expired");
				}		
			}
			else
				throw new ValidationException("Invalid Session ID.");
		}
		catch(ValidationException e)
		{
			tx.rollback();
			System.out.println("validation Exception.");
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return userId;
	}
}