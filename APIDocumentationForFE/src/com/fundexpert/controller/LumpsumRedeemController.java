package com.fundexpert.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Transaction;
import com.fundexpert.dao.User;

public class LumpsumRedeemController 	
{

	public boolean isEquityType(Long mutualfundId) 
	{
		MutualFund mf=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			mf=(MutualFund) session.createQuery("from MutualFund where id=? and category=?").setLong(0, mutualfundId).setInteger(1, 1).uniqueResult();
			if(mf!=null)	
				return true;
			else
				return false;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return false;
	}

	public boolean isDebtType(Long mutualfundId) 
	{
		MutualFund mf=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			mf=(MutualFund) session.createQuery("from MutualFund where id=? and category=?").setLong(0, mutualfundId).setInteger(1, 3).uniqueResult();
			if(mf!=null)	
				return true;
			else
				return false;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return false;
	}
	
	public boolean isETFType(Long mutualfundId) 
	{
		MutualFund mf=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			mf=(MutualFund) session.createQuery("from MutualFund where id=? and category=?").setLong(0, mutualfundId).setInteger(1, 6).uniqueResult();
			if(mf!=null)	
				return true;
			else
				return false;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return false;
	}
	
	public boolean isLiquidType(Long mutualfundId) 
	{
		MutualFund mf=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			mf=(MutualFund) session.createQuery("from MutualFund where id=? and category=?").setLong(0, mutualfundId).setInteger(1, 4).uniqueResult();
			if(mf!=null)	
				return true;
			else
				return false;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return false;
	}
	
	public boolean isBalancedType(Long mutualfundId)
	{
		MutualFund mf=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			mf=(MutualFund) session.createQuery("from MutualFund where id=? and category=?").setLong(0, mutualfundId).setInteger(1, 5).uniqueResult();
			if(mf!=null)	
				return true;
			else
				return false;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return false;	
	}
	public boolean isELSSType(Long mutualfundId) 
	{
		MutualFund mf=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			mf=(MutualFund) session.createQuery("from MutualFund where id=? and category=?").setLong(0, mutualfundId).setInteger(1, 2).uniqueResult();
			if(mf!=null)	
				return true;
			else
				return false;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return false;	
	}
	public List<Transaction> getTransactionsOfHolding(Long id) 
	{
		List<Transaction> list=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			list=session.createQuery("from Transaction where holdingId=?").setLong(0, id).list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return list;
	}

	public double getRating(Long mutualfundId)
	{
		MutualFund mf=null;
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			mf=(MutualFund) session.createQuery("from MutualFund where id=?").setLong(0, mutualfundId).uniqueResult();
			if(mf!=null)	
				return mf.getOurRating();
			else
				return 0;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return 0;		
	}

	public List<Long> getSortedListOfMFID(List<Long> mfList) 
	{
		List<Long> list=new ArrayList<Long>(); 
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			list=session.createQuery("select id from MutualFund where id in (:idx) order by ourRating").setParameterList("idx", mfList).list();
			
			if(list.size()!=0)
			{				
				// System.out.println(mfList+",    Sorted : "+list1);
			}
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return list;
	}
	public List<Transaction> getNonTaxableTransactionsOfHolding(Long id) 
	{
		List<Transaction> list=new ArrayList();
		Calendar nowCal=Calendar.getInstance();
		nowCal.add(Calendar.YEAR, -1);
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			list=session.createQuery("from Transaction where holdingId=? and transactionDate<=?").setLong(0, id).setDate(1, nowCal.getTime()).list();
			//System.out.println("NON Taxable TR Size of List: "+list.size()+",   ");
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return list;
	
	}

	public List<Transaction> getTaxableTransactionsOfHolding(Long id) 
	{
		List<Transaction> list=null;
		Calendar nowCal=Calendar.getInstance();
		nowCal.add(Calendar.YEAR, -1);
		Session session=HibernateBridge.getSessionFactory().openSession();
		try
		{
			list=session.createQuery("from Transaction where holdingId=? and transactionDate>?").setLong(0, id).setDate(1, nowCal.getTime()).list();
			//System.out.println("Taxable TR Size of List: "+list.size()+",   ");
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return list;	
	}

	

	
	
	
/*
	public List<Integer> swapHoldingList(List<Holding> holdingList)
	{
		List<Integer> list=new ArrayList<>();
		for(int i=0;i<holdingList.size();i++)
		{
			for(int j=1;j<holdingList.size()-1;j++)
			{
				Holding h1=holdingList.get(j);
				Holding h2=holdingList.get(j-1);
				int r1= getRating(holdingList.get(j).getMutualfundId());
				int r2= getRating(holdingList.get(j-1).getMutualfundId());
				
				System.out.println("r1: "+r1+", r2: "+r2);
				
				if(r2>r1)
				{
					int t=r2;
					r2=r1;
					r1=t;
					double d1=h1.getAvgNav();
					String s1=h1.getFolioNumber();
					double d2=h1.getUnits();
					Long l1=h1.getMutualfundId();
					Long l2=h1.getUserId();
					Date dt1=h1.getCreatedOn();
					Long l3=h1.getId();
					
					h1.setAvgNav(h2.getAvgNav());
					h1.setCreatedOn(h2.getCreatedOn());
					h1.setFolioNumber(h2.getFolioNumber());
					h1.setId(h2.getId());
					h1.setMutualfundId(h2.getMutualfundId());
					h1.setUnits(h2.getUnits());
					h1.setUserId(h2.getUserId());
					 
					h2.setAvgNav(d1);
					h2.setCreatedOn(dt1);
					h2.setFolioNumber(s1);
					h2.setMutualfundId(l1);
					h2.setUnits(d2);
					h2.setUserId(l2);
					h2.setId(l3);
					
					Holding o=holdingList.get(j-1);
					holdingList.set(j-1, holdingList.get(j));
					holdingList.set(j, o);
				}
			}
		}
		return holdingList;
	}	
*/

	
}