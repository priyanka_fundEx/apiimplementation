package com.fundexpert.exception;

public class FundexpertException extends Exception {
	String message;
	public FundexpertException(String message) {
		this.message=message;
	}
	
	public String getMessage() {
		return message;
	}
}
