package com.fundexpert.exception;

import org.json.JSONException;

public class InputFormatException extends FundexpertException{

	String message;
	public InputFormatException(String message) {
		super(message);
		this.message=message;
	}

	public String toString()
	{
		return message;
	}
}
