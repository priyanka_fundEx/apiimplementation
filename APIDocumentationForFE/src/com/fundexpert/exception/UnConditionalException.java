package com.fundexpert.exception;

public class UnConditionalException extends Exception{

	String message;
	public UnConditionalException(String message) {
		this.message=message;
	}
	
	public String getMessage() {
		return message;
	}
}
