package com.fundexpert.exception;

public class ValidationException extends FundexpertException{

	String message;
	
	public ValidationException(String message) {
		super(message);
		this.message=message;
	}

	public String toString(){
		return message;
	}
}
