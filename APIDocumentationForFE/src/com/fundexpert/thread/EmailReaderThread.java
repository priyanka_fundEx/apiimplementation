package com.fundexpert.thread;

import com.fundexpert.config.Config;
import com.fundexpert.dao.ArchiveApiRequests;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.util.EmailReader;
import com.fundexpert.util.PdfReaderAndPersist;

public class EmailReaderThread implements Runnable{

	public void run() {
		try
		{
			System.out.println("Inside Thread 1.");
			EmailReader emailReader=new EmailReader();
			PdfReaderAndPersist pdfReader=new PdfReaderAndPersist();
			Config config=new Config();
			while(true)
			{
				try
				{
				
					//System.out.println("SYSTEM : "+(String)System.getProperties().get("os.name"));
					if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION) && !((String)System.getProperties().get("os.name")).toLowerCase().contains("windows"))
					{
						System.out.println("Email Thread exec. for MF");
						emailReader.savePdf();
						System.out.println("Starting Parsing and Persisting PDF for MF.");
						pdfReader.setRequestCommingFrom(ArchiveApiRequests.mutualFundPortfolioFromEmail);
						pdfReader.savepdf(null, null, null, 2);
						System.out.println("Parsing PDFThread Done for Mf.\n");
						System.out.println("Email Thread exec. for STOCK");
						
						emailReader.saveStockPdf();
						System.out.println("Starting Parsing and Persisting for STOCK.");
						pdfReader.setRequestCommingFrom(ArchiveApiRequests.stockPortfolioFromEmail);
						pdfReader.saveStockPdf(null, null, null, 2);
						System.out.println("Parsing PDFThread Done for Stock.");
					}
					else
					{
						System.out.println("Email Thread not starting in Development Environment.");
					}
					System.out.println("Thread 1 success.");
				}
				catch(Exception e)
				{
					e.printStackTrace();
					//TODO send sms as exception with timestamp
				}
				Thread.sleep(180000);//3 min timer
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
}
