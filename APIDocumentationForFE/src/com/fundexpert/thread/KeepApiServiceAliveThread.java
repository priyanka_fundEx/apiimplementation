package com.fundexpert.thread;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.fundexpert.config.Config;
import com.fundexpert.controller.CunsumerController;
import com.fundexpert.dao.Consumer;
import com.fundexpert.util.KeepApiServiceAlive;

public class KeepApiServiceAliveThread implements Runnable{

	@Override
	public void run() {
		CunsumerController cc=new CunsumerController();
		KeepApiServiceAlive service=new KeepApiServiceAlive();
		while(true)
		{
			System.out.println("Inside Thread2.");
			Calendar cal=Calendar.getInstance();
			Calendar cal1=Calendar.getInstance();
			Calendar current=Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 17);
			cal.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.HOUR_OF_DAY, 23);
			cal1.set(Calendar.MINUTE, 0);
			
			try
			{
				//System.out.println("Current="+current.getTime()+" cal1="+cal1.getTime()+" cal="+cal.getTime());
				if(current.after(cal) && current.before(cal1))
				{
					List<Consumer> list=cc.getDistinctConsumers();
					Iterator itr=list.iterator();
					while(itr.hasNext())
					{
						Consumer consumer = (Consumer)itr.next();
						if(consumer.getId()==2)
						{
							boolean active = service.getConfigForApiService(consumer.getAppId());
							boolean success=service.updateConfigStatus(consumer.getAppId(), active);
							System.out.println("Success!!In thread 2)");
						}
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			try 
			{
				Thread.sleep(1000*60*60*5);//5 hrs
			} catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}

	
}
