package com.fundexpert.thread;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fundexpert.config.Config;
import com.fundexpert.controller.StocksController;

public class StockUpdationThread implements Runnable{

	@Override
	public void run() {
		
		try
		{
			Config config=new Config();
			if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
			{
				System.out.println("Inside Thread3");
				while(true)
				{
					try
					{
						boolean updatedStocks=false;
						boolean allSuccess=false;
						JSONObject json=new JSONObject();
						StocksController shc=new StocksController();
						//System.out.println("getting details in a moment from api server");
						json=shc.getDetails(null);
						if(json.has("success") && json.getString("success").equals("true"))
						{
							JSONArray jsonArray=json.getJSONArray("stockData");
							//System.out.println("received JSON size="+jsonArray.length());
							allSuccess=shc.updateStock(jsonArray);
						}
						else
						{
							throw new Exception("Error Occurred in Thread 3 ERROR="+json.getString("error"));
						}
						System.out.println("Success from thread 3 = "+allSuccess);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					try
					{
						Thread.sleep(1000*60*60*24);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

}
