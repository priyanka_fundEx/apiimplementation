package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.apache.commons.beanutils.BeanUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.apidoc.util.JSON;
import com.fundexpert.config.Config;
import com.fundexpert.controller.ArchiveApiRequestController;
import com.fundexpert.controller.HoldingController;
import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.PortfolioImportController;
import com.fundexpert.controller.PortfolioRequestController;
import com.fundexpert.controller.RegisterUserController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.StockHoldingController;
import com.fundexpert.controller.StocksPortfolioImportController;
import com.fundexpert.dao.ArchiveApiRequests;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.PortfolioRequest;
import com.fundexpert.dao.StocksHoldings;
import com.fundexpert.dao.User;
import com.fundexpert.dao.UserPortfolioState;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.exception.InputFormatException;
import com.fundexpert.exception.ValidationException;
import com.fundexpert.pojo.Holdings;
import com.fundexpert.util.PdfReaderAndPersist;
import com.fundexpert.util.Utilities;
import com.rebalance.main.Algo;


@Path("/request")
public class RequestPortfolio {
	
	@POST
	@Path("/RequestMutualfundPortfolio")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPortfolio(InputStream is,@Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		try
		{
			//System.out.println("In Request Portfolio.");
			BufferedReader br=new BufferedReader(new InputStreamReader(is));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject;
			try
			{
				 jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<4 || jObject.length()>5)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("emailId") || jObject.get("emailId").equals("") || jObject.get("emailId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Email ID is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("pan") || jObject.get("pan").equals("") || jObject.get("pan")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Pan is mandatory.");
				jobject.put("error", failure);
			}
			/*else if(!jObject.has("removeUserData") || jObject.get("removeUserData").equals("") || jObject.get("removeUserData")==null)
			{
				
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","RemoveUserData field cannot be empty.");
				jobject.put("error", failure);
			}*/
			else
			{
				appId= jObject.getString("appId");		
			    sessionId = jObject.getString("sessionId"); 
			    LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","App Id does not exist.");
					jobject.put("error", failure);
					flag=false;
				}			
				else if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(flag==false)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					boolean removeUserDataFlag=jObject.has("removeUserData")?jObject.getBoolean("removeUserData"):false;
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
						System.out.println("Remove User Data Flag="+removeUserDataFlag);
						String emailId=jObject.getString("emailId");
						String pan=jObject.getString("pan");
						//Create A User with given email and pan under this consumer considering sessionId is not expire and if expire return 'Session Expire'
						RegisterUserController userCtrl=new RegisterUserController();
						Long userId=userCtrl.saveUser(con.getId(), emailId, pan, sessionId);
							
						if(!removeUserDataFlag)
						{
							//Make a portfolioRequest for user with received emailId,pan
							PortfolioRequestController prCtrl=new PortfolioRequestController();
							PortfolioRequest pr=prCtrl.persistRequest(userId, pan);
							
							if(pr!=null)
							{
								boolean success=prCtrl.persistArchivePortfolioRequest(pr);
								if(success)
								{
									Thread t1=new Thread(new Runnable()
									{
										public void run() {
											PortfolioRequestController prc=new PortfolioRequestController();
											prc.SendDataTOFe(pr.getUserId());
										}
									});
									t1.start();
								}
								if(!success)
								{
									//TODO send SMS regarding not able to insert request in ArchivePortfolioRequest
									//since insertion into ArchivePortfolioRequest is not crucial will keep the process as it is and not throwing the Exception
								}
								jobject.put("success", true);
								jobject.put("message", "Your Mutualfund Portfolio has been sent through email.");
								ArchiveApiRequestController aarc=new ArchiveApiRequestController();
								aarc.saveRequest(con.getId(),emailId,pan,ArchiveApiRequests.portfolioRequest);
							}
							else
							{
								throw new Exception();
							}
						}
						else
						{
							//here edelweiss request to archieve user's holdings
							HoldingController hc=new HoldingController();
							hc.archiveUserHolding(userId);
							
							StockHoldingController shc=new StockHoldingController();
							shc.archiveStockHoldings(userId);
							
							jobject.put("success", true);
							jobject.put("message", "You have successfully removed user's Stock and MutualFund portfolio.");
							ArchiveApiRequestController aarc=new ArchiveApiRequestController();
							aarc.saveRequest(con.getId(),emailId,pan,ArchiveApiRequests.archiveUserHoldingAndTransaction);
						}
			    	}
					else
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		
					}
				}
			}
		}
		catch(JSONException je)
		{
			je.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message","Not a proper JSON format.");
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch(FundexpertException feException)
		{
			feException.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message",feException.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "771");
				failure.put("message","Request Failed,Please try again after some time.");
				jobject.put("error", failure);
				
				//TODO send SMS
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@Path("/GetMFPortfolioFromPDF")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnPortfolio(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		String pdfPath=null;
		System.out.println("Fetch Portfolio.");
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject;
			try
			{
				jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<5 || jObject.length()>5)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals(""))
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("emailId") || jObject.get("emailId").equals("") || jObject.get("emailId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Email ID is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("pan") || jObject.get("pan").equals("") || jObject.get("pan")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Pan is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("mutualfundPDFPath") || jObject.get("mutualfundPDFPath").equals(""))
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","MutualfundPDFPath is mandatory.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
				sessionId=jObject.getString("sessionId");
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);

				Long consumerId=null;
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				System.out.println("ip"+ipAddress);
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","Invalid Credentials.");
					jobject.put("error", failure);
					flag=false;
				}
				if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(flag==false)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
						String emailId=jObject.getString("emailId");
						String pan=jObject.getString("pan");
						pdfPath=jObject.getString("mutualfundPDFPath");

						//Get persisted User with received emailId,pan
						RegisterUserController userCtrl=new RegisterUserController();
						long userId=userCtrl.saveUser(con.getId(), emailId, pan, sessionId);
						User user=userCtrl.getUser(userId);
						if(user==null)
							throw new FundexpertException("No User exists with given email and pan found.");
						emailId=user.getEmail();
						PdfReaderAndPersist pdfReader=new PdfReaderAndPersist();
						pdfReader.setRequestCommingFrom(ArchiveApiRequests.mutualFundPortfolioFromPdfPath);
						pdfReader.savepdf(pdfPath, emailId, pan, con.getId());
						
						
						jobject.put("success", true);
						jobject.put("user", JSON.toJSONObject(user,true));
						ArchiveApiRequestController aarc=new ArchiveApiRequestController();
						aarc.saveRequest(con.getId(),emailId,pan,ArchiveApiRequests.mutualFundPortfolioFromPdfPath);
			    	}
					else
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		
					}
				}
			}
		}
		catch(FundexpertException feException)
		{
			feException.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message",feException.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message","Internal Server Error.");
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@Path("/GetStockPortfolioFromPDF")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnStockPortfolio(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		String pdfPath=null;
		System.out.println("Fetch Portfolio.");
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject;
			try
			{
				jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<5 || jObject.length()>5)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals(""))
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("emailId") || jObject.get("emailId").equals("") || jObject.get("emailId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Email ID is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("pan") || jObject.get("pan").equals("") || jObject.get("pan")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Pan is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("stockPortfolioPDFPath") || jObject.get("stockPortfolioPDFPath").equals("") || jObject.get("stockPortfolioPDFPath")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Stock pdf path is mandatory.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
				sessionId=jObject.getString("sessionId");
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);

				Long consumerId=null;
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				System.out.println("ip"+ipAddress);
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","Invalid Credentials.");
					jobject.put("error", failure);
					flag=false;
				}
				if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(flag==false)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
						String emailId=jObject.getString("emailId");
						String pan=jObject.getString("pan");
						RegisterUserController userCtrl=new RegisterUserController();
						long userId=userCtrl.saveUser(con.getId(), emailId, pan, sessionId);
						
						User user=userCtrl.getUser(userId);
						if(user==null)
							throw new ValidationException("No User with given emailId and pan found.");
						emailId=user.getEmail();
						pdfPath=jObject.getString("stockPortfolioPDFPath");
						PdfReaderAndPersist pdfReader=new PdfReaderAndPersist(true);
						pdfReader.setRequestCommingFrom(ArchiveApiRequests.stockPortfolioFromPdfpath);
						pdfReader.saveStockPdf(pdfPath, emailId, pan, con.getId());
						
						//Get persisted User with received emailId,pan
						System.out.println("!@#$%EMAILID : "+emailId+" PAN : "+pan);
						jobject.put("success", true);
						jobject.put("user", JSON.toJSONObject(user,false));
						ArchiveApiRequestController aarc=new ArchiveApiRequestController();
						aarc.saveRequest(con.getId(),emailId,pan,ArchiveApiRequests.stockPortfolioFromPdfpath);
			    	}
					else
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		
					}
				}
			}
		}
		catch(FundexpertException feException)
		{
			feException.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message",feException.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message","Internal Server Error.");
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).entity(jobject.toString()).build();
	}

	@POST
	@Path("/returnMFPortfolio")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnMFPortfolioViaEmail(InputStream incomingData, @Context HttpServletRequest request)
	{

		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		try
		{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject;
			try
			{
				 jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<4 || jObject.length()>4)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("emailId") || jObject.get("emailId").equals("") || jObject.get("emailId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Email ID is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("pan") || jObject.get("pan").equals("") || jObject.get("pan")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Pan is mandatory.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
			    sessionId = jObject.getString("sessionId"); 
			    LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","App Id does not exist.");
					jobject.put("error", failure);
					flag=false;
				}			
				else if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(flag==false)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
						String emailId=jObject.getString("emailId");
						String pan=jObject.getString("pan");
						//Get A User with given email and pan under this consumer
						RegisterUserController userCtrl=new RegisterUserController();
						User user=userCtrl.getUser(emailId, pan, con.getId());
						if(user==null)
							throw new FundexpertException("No User exists with given email and pan.");
						emailId=user.getEmail();
						//Get a Portfolio JSON of User
						HoldingController holdingCtrl=new HoldingController();
						List<Holding> holdingsList = holdingCtrl.getHoldings(user.getId());
						jobject.put("success", true);
						jobject.put("portfolio", JSON.toJSONObject(user, true));
						ArchiveApiRequestController aarc=new ArchiveApiRequestController();
						aarc.saveRequest(con.getId(),emailId,pan,ArchiveApiRequests.mutualFundPortfolioFromEmail);
			    	}
					else
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		
					}
				}
			}
		}
		catch(FundexpertException feException)
		{
			feException.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message",feException.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "771");
				failure.put("message","Request Failed,Please try again after some time.");
				jobject.put("error", failure);
				
				//TODO send SMS
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@POST
	@Path("/returnStockPortfolio")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnStockPortfolioViaEmail(InputStream incomingData, @Context HttpServletRequest request)
	{

		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		try
		{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject;
			try
			{
				 jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<4 || jObject.length()>4)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("emailId") || jObject.get("emailId").equals("") || jObject.get("emailId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Email ID is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("pan") || jObject.get("pan").equals("") || jObject.get("pan")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Pan is mandatory.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
			    sessionId = jObject.getString("sessionId"); 
			    LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","App Id does not exist.");
					jobject.put("error", failure);
					flag=false;
				}			
				else if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(flag==false)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
						String emailId=jObject.getString("emailId");
						String pan=jObject.getString("pan");
						//Get A User with given email and pan under this consumer
						RegisterUserController userCtrl=new RegisterUserController();
						User user=userCtrl.getUser(emailId, pan, con.getId());
						if(user==null)
							throw new FundexpertException("No User exists with given email and pan.");
						emailId=user.getEmail();
						//Get a StockPortfolio JSON of User
						//Set<StocksHoldings> holdingsSet = user.getStocksHoldings();
						jobject.put("success", true);
						jobject.put("portfolio", JSON.toJSONObject(user, false));
						ArchiveApiRequestController aarc=new ArchiveApiRequestController();
						aarc.saveRequest(con.getId(),emailId,pan,ArchiveApiRequests.stockPortfolioFromEmail);
			    	}
					else
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		
					}
				}
			}
		}
		catch(FundexpertException feException)
		{
			feException.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message",feException.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "771");
				failure.put("message","Request Failed,Please try again after some time.");
				jobject.put("error", failure);
				
				//TODO send SMS
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@POST
	@Path("/returnMFPortfolioViaByteStream")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnMFPortfolioViaByteStream(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		try
		{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject;
			try
			{
				 jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<5 || jObject.length()>5)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("emailId") || jObject.get("emailId").equals("") || jObject.get("emailId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Email ID is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("pan") || jObject.get("pan").equals("") || jObject.get("pan")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Pan is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("byteData") || jObject.get("byteData").equals("") || jObject.get("byteData")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Byte Data is mandatory.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
			    sessionId = jObject.getString("sessionId"); 
			    LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","App Id does not exist.");
					jobject.put("error", failure);
					flag=false;
				}			
				else if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(flag==false)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
						String emailId=jObject.getString("emailId");
						String pan=jObject.getString("pan");
						String byteStream=jObject.getString("byteData");
						//Get A User with given email and pan under this consumer
						RegisterUserController userCtrl=new RegisterUserController();
						long userId=userCtrl.saveUser(con.getId(), emailId, pan, sessionId);
						User user=userCtrl.getUser(userId);
						if(user==null)
							throw new FundexpertException("No User exists with given email and pan.");
						emailId=user.getEmail();
						Config config=new Config();
						String pdfPath=config.getProperty(Config.PDF_NOT_READ);
						Utilities utilities=new Utilities();
						String fileName=utilities.getRandomString()+".pdf";
						pdfPath+=File.separator+fileName;
						boolean result=utilities.decodeBase64ByteStreamToPdf(byteStream, pdfPath);
						if(!result)
							throw new FundexpertException("Unable to decode byteStream.");
						
						System.out.println("PAN received ="+pan +" email changed to ="+emailId);
						
						PdfReaderAndPersist pdfReader=new PdfReaderAndPersist();
						pdfReader.setRequestCommingFrom(ArchiveApiRequests.mutualFundPortfolioFromByteStream);
						pdfReader.savepdf(pdfPath, emailId, pan, con.getId());
						//Get a Portfolio JSON of User
						HoldingController holdingCtrl=new HoldingController();
						List<Holding> holdingsList = holdingCtrl.getHoldings(user.getId());
						jobject.put("success", true);
						jobject.put("portfolio", JSON.toJSONObject(user, true));
						ArchiveApiRequestController aarc=new ArchiveApiRequestController();
						aarc.saveRequest(con.getId(),emailId,pan,ArchiveApiRequests.mutualFundPortfolioFromByteStream);
			    	}
					else
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		
					}
				}
			}
		}
		catch(FundexpertException feException)
		{
			feException.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message",feException.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "771");
				failure.put("message","Request Failed,Please try again after some time.");
				jobject.put("error", failure);
				
				//TODO send SMS
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@POST
	@Path("/returnStockPortfolioViaByteStream")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnStockPortfolioViaByteStream(InputStream incomingData, @Context HttpServletRequest request)
	{

		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		try
		{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject;
			try
			{
				 jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<5 || jObject.length()>5)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("emailId") || jObject.get("emailId").equals("") || jObject.get("emailId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Email ID is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("pan") || jObject.get("pan").equals("") || jObject.get("pan")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Pan is mandatory.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("byteData") || jObject.get("byteData").equals(""))
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Byte Data is mandatory.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
			    sessionId = jObject.getString("sessionId"); 
			    LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","App Id does not exist.");
					jobject.put("error", failure);
					flag=false;
				}			
				else if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(flag==false)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
						String emailId=jObject.getString("emailId");
						String pan=jObject.getString("pan");
						String byteStream=jObject.getString("byteData");
						//Get A User with given email and pan under this consumer
						RegisterUserController userCtrl=new RegisterUserController();
						long userId=userCtrl.saveUser(con.getId(), emailId, pan, sessionId);
						User user=userCtrl.getUser(userId);
						if(user==null)
							throw new FundexpertException("No User exists with given email and pan.");
						emailId=user.getEmail();
						Config config=new Config();
						String pdfPath=config.getProperty(Config.STOCK_PDF_NOT_READ);
						Utilities utilities=new Utilities();
						String fileName=utilities.getRandomString()+".pdf";
						pdfPath+=File.separator+emailId+"-"+fileName;
						boolean result=utilities.decodeBase64ByteStreamToPdf(byteStream, pdfPath);
						if(!result)
							throw new FundexpertException("Unable to decode byteStream.");
						PdfReaderAndPersist pdfReader=new PdfReaderAndPersist(true);
						pdfReader.saveStockPdf(pdfPath, emailId, pan, con.getId());
						//Get a StockPortfolio JSON of User
						//Set<StocksHoldings> holdingsSet = user.getStocksHoldings();
						user=userCtrl.getUser(userId);
						jobject.put("success", true);
						jobject.put("portfolio", JSON.toJSONObject(user, false));
						ArchiveApiRequestController aarc=new ArchiveApiRequestController();
						aarc.saveRequest(con.getId(),emailId,pan,ArchiveApiRequests.stockPortfolioFromByteStream);
			    	}
					else
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		
					}
				}
			}
		}
		catch(FundexpertException feException)
		{
			feException.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message",feException.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "771");
				failure.put("message","Request Failed,Please try again after some time.");
				jobject.put("error", failure);
				
				//TODO send SMS
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).entity(jobject.toString()).build();
	
	}

	@POST
	@Path("/optimizePortfolio")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnOptimizedPortfolioFor(InputStream incomingData,@Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		try
		{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject;
			JSONArray mfArray;
			JSONArray stockArray;
			try
			{
				 jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(!jObject.has("portfolio"))
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Your portfolio is missing.");
				jobject.put("error", failure);
			}
			else if(!jObject.getJSONObject("portfolio").has("mf") && !jObject.getJSONObject("portfolio").has("st"))
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Atleast one asset class in neccessary.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("rp"))
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Risk Profile is mandatory.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
			    sessionId = jObject.getString("sessionId"); 
			    LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","App Id does not exist.");
					jobject.put("error", failure);
					flag=false;
				}			
				else if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(flag==false)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
						Utilities utility=new Utilities();
						String emailId="optimizer_"+utility.getRandomString()+"@email.com";
						String pan="EDCPS3731K";
						//Create A User with given email and pan under this consumer considering sessionId is not expire and if expire return 'Session Expire'
						RegisterUserController userCtrl=new RegisterUserController();
						Long userId=userCtrl.saveUser(con.getId(), emailId, pan, sessionId);
							
						mfArray=jObject.getJSONObject("portfolio").getJSONArray("mf");
						stockArray=jObject.getJSONObject("portfolio").getJSONArray("st");
						if(mfArray.length()>0)
						{
							PortfolioImportController pic=new PortfolioImportController();
							Map<String,List<Holdings>> result = pic.createHoldingFromJsonArray(mfArray);
							List<Holdings> holdingList=result.get("success");
							List<Holdings> missingList=result.get("missing");
							pic.sFactory(userId, holdingList);
							if(missingList!=null && missingList.size()>0)
							{
								JSONArray jsonArray=new JSONArray();
								for(Holdings missing:missingList)
								{
									jsonArray.put(missing.getIsin());
								}
								jobject.put("missingMF", jsonArray);
							}
						}
						if(stockArray.length()>0)
						{
							StocksPortfolioImportController spic=new StocksPortfolioImportController();
							List<com.fundexpert.pojo.StocksHoldings> stocksHoldingsList=spic.createStockHoldingsFromJsonArray(stockArray);
							spic.sFactory(userId, stocksHoldingsList);
							List<Map> missingMap=spic.getMissingStocksList();
							if(missingMap!=null && missingMap.size()>0)
							{
								JSONArray jsonArray=new JSONArray();
								for(Map map:missingMap)
								{
									jsonArray.put(map.get("isin"));
								}
								jobject.put("missingST", jsonArray);
							}
						}
						int riskProfile=jObject.getInt("rp");
						Algo algo=new Algo(userId,con.getId(),riskProfile);
						Map<String,Map<String,List<UserPortfolioState>>> map=algo.doRebalance();
						
						mfArray=com.rebalance.util.JSON.makeJSONForHoldingsOnly(map.get("mf"), userId);
						stockArray=com.rebalance.util.JSON.makeJSONForStockHoldingsOnly(map.get("stock"), userId);
						jobject.put("success", true);
						jobject.put("mf", mfArray);
						jobject.put("stock", stockArray);
						jobject.put("success", true);
			    	}
					else
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		
					}
				}
			}
			
		}
		catch(JSONException je)
		{
			je.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message","Not a proper JSON format.");
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch(FundexpertException feException)
		{
			feException.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message","Not able to rebalance the portfolio.");
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "771");
				failure.put("message","Request Failed,Please try again after some time.");
				jobject.put("error", failure);
				
				//TODO send SMS
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).entity(jobject.toString()).build();
	}


}
