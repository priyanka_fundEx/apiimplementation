package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.fundexpert.controller.GetAlertOnFundORPortfolioController;
import com.fundexpert.controller.GetClientPortfolioController;
import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;
import com.fundexpert.dao.Alert;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.MutualFundSubscription;

@Path("/user/alert")
public class GetAlertOnFundORPortfolio {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String sessionId=null;
		String appId=null;
		String userId=null;
		String ipAddress=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject=new JSONObject(builder.toString());
			
			boolean flag=false;
			
			if(jObject.has("appId") && jObject.has("sessionId")	&& jObject.has("userId"))
			{
				if(!jObject.getString("appId").equals("") && !jObject.getString("sessionId").equals("")	&& !jObject.getString("userId").equals(""))
				{
					appId= jObject.getString("appId");		
					sessionId = jObject.getString("sessionId");  
				    userId=jObject.getString("userId");
				    flag=true;
				}
				else{
					jobject.put("success", false);
					JSONObject failure=new JSONObject();
					failure.put("message", "Invalid or Empty parameters.");
					failure.put("code", "705");
					jobject.put("error", failure);
					System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
				}
			}
			else{
				System.out.println("All Key parameters does not exist in input JSON Object.");
			}
			
			ipAddress=request.getRemoteAddr();
		    if(flag)
		    { 
			    SessionController sessionCtrl=new SessionController();
				SubscribeToFundController fundCtrl=new SubscribeToFundController();
				GetClientPortfolioController pfCtrl=new GetClientPortfolioController();
				GetAlertOnFundORPortfolioController ctrl=new GetAlertOnFundORPortfolioController();

				Long consumerId=null;
				Long amfiiCode = null;
				
			     consumerId=sessionCtrl.getClient(appId, ipAddress); 
				 Long userID=pfCtrl.getUserId(userId, consumerId);
				 if(consumerId!=null)
				 {
				    	if(sessionCtrl.getValidSession(consumerId, sessionId))
				    	{
				    		 if(fundCtrl.validUser(consumerId, userId))
							{	
				    			 if(ctrl.isPortfolioSubscribed(userID))			    			
				    			 {
		    							System.out.println("inside Portfolio SUbscribed");
				    				jobject.put("success", true);	
						    		JSONArray array=new JSONArray();
						    		 
				    				Set<Long> set = ctrl.getHoldingList(userID);
				    				System.out.println("Set of Mutualfund ID: "+set);
				    				for(Long mfId:set)
				    				{
				    					amfiiCode=ctrl.getAmfiiCode(mfId);
				    					Holding holding=ctrl.getHolding(mfId);
				    					List<Alert> alertList=ctrl.getAlertt(mfId);
				    					for(Alert alert: alertList)
				    					{
			    							System.out.println("inside Alert List");
				    						if(alert.getType()==1)
				    						{
				    							System.out.println("inside Alert ==1");
				    							JSONObject entryObject=new JSONObject();
				    							entryObject.put("amfiiCode", amfiiCode);
				    							entryObject.put("type", "ENTRY");  
					    						entryObject.put("comments", "Invest any exited amount of large cap equity funds in this mutualfund.");
					    						array.put(entryObject);
					    						
					    						MutualFund mf=ctrl.getCategoryOfMF(mfId, amfiiCode);
					    						List<MutualFund> catList=ctrl.getListOfSameMFCategory(mf.getSchemeType(), mf.getType());
					    						for(MutualFund fund: catList)
					    						{
					    							System.out.println("inside Category MF");
							    					Alert a=ctrl.getEntryAlertOfFund(fund.getId());
							    					if(a!=null)
							    					{ 
						    							System.out.println("inside Category MF Alert ==1");
						    							JSONObject entryObj=new JSONObject();
						    							entryObj.put("amfiiCode", amfiiCode);
						    							entryObj.put("type", "ENTRY");
						    							entryObj.put("comments", "Invest any exited amount of large cap equity funds in this mutualfund.");
							    						array.put(entryObj);
							    					}
					    						}
				    						}
				    						if(alert.getType()==2)
				    						{
				    							System.out.println("inside Alert ==2");
				    							JSONObject exitObject=new JSONObject();
					    						exitObject.put("amfiiCode", amfiiCode);
					    						exitObject.put("type", "EXIT");
					    						exitObject.put("Amount", 5000);
					    						exitObject.put("folioNumber", holding.getFolioNumber());
					    						exitObject.put("comments", "Reinvest the exited amount in equal 6 installments for the next 6 months.");
					    						array.put(exitObject);				    						
				    						}
				    					}
					    				jobject.put("alerts", array);
				    				}
				    			 }	
				    			 else if(!ctrl.isPortfolioSubscribed(userID))	
				    			 {
						    		 jobject.put("success", true);
						    		 JSONArray array=new JSONArray();
						    		 
				    				 List<MutualFundSubscription> list = ctrl.getMFList(userID);
				    				 for(MutualFundSubscription mfs: list)
				    				 {
				    					System.out.println("inside MFS List: "+mfs);
				    					amfiiCode=ctrl.getAmfiiCode(mfs.getMutualfundId());
				    					Holding holding=ctrl.getHolding(mfs.getMutualfundId());
				    					List<Alert> alert=ctrl.getExitAlert(mfs.getMutualfundId());
				    					if(alert.size()!=0)
				    					{
				    						for(Alert a:alert)
					    					{
					    						JSONObject arrayObject=new JSONObject();
						    					arrayObject.put("amfiiCode", amfiiCode);
						    					
						    					if(a.getType()==1)
						    					{	
						    						arrayObject.put("type", "ENTRY");
						    						arrayObject.put("comments", "Invest any exited amount of large cap equity funds in this mutualfund.");
						    					}
					    						else if(a.getType()==2)
					    						{
					    							arrayObject.put("type", "EXIT");
						    						arrayObject.put("amount", 50000);
						    						arrayObject.put("folioNumber", holding.getFolioNumber());
						    						arrayObject.put("comments", "Reinvest the exited amount in equal 6 installments for the next 6 months.");
					    						}
												array.put(arrayObject);
												jobject.put("alerts", array);
											}		
				    					}
				    				 }
				    			 }
				    			 else
				    			 {
				    				jobject.put("success", false);
				    				JSONObject failure=new JSONObject();
									failure.put("message", "User not subscribed to any service.");
									failure.put("code", "751");
									jobject.put("error", failure);  
									System.out.println("Get Alert On Fumd or Portfolio API not invoked Successfully.");	
				    			 }
							}
				    		 else{
								jobject.put("success", false);
								JSONObject failure=new JSONObject();
								failure.put("message", "User ID does not exist.");
								failure.put("code", "704");
								jobject.put("error", failure);  
								System.out.println("Get Alert On Fumd or Portfolio API not invoked Successfully.");
							}			    		
				    	}
				    	else{
			    		    jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			    			System.out.println("Get Alert On Fumd or Portfolio API Service has not invoked Successfully.   "+jobject.toString());
			    		}
				    }
				    else{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
		    			System.out.println("Get Alert On Fumd or Portfolio API Service has not invoked Successfully.   "+jobject.toString());
		    		}   
		   }
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}