package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fundexpert.controller.PortfolioCreateController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;

@Path("/user/mutualfund/subscription/create")
public class SubscribeToFund
{
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject = new JSONObject();
		StringBuilder builder = new StringBuilder();
		String sessionId = null;
		String appId = null;
		String userId = null;
		JSONArray array=null;
		String ipAddress = null;
		try
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(incomingData));
			String line = "";
			while ((line = br.readLine()) != null)
			{
				builder.append(line);
			}
			JSONObject jObject = new JSONObject(builder.toString());
			 
			boolean flag=false;
			
			if(jObject.getString("appId")!=null && !jObject.getString("appId").equals("") && jObject.getString("sessionId")!=null  
						&& !jObject.getString("sessionId").equals("") && jObject.getString("userId")!=null && !jObject.getString("userId").equals(""))
			{
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId");  
			    userId=jObject.getString("userId");
				array = jObject.getJSONArray("mutualfund");
			    flag=true;
			}
			else{
				jobject.put("success", false);
				JSONObject failure=new JSONObject();
				failure.put("message", "Invalid or Empty parameters.");
				failure.put("code", "705");
				jobject.put("error", failure);
				System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
			}
			
			ipAddress=request.getRemoteAddr();
		    if(flag)
		    { 
		    	Map<String, Boolean> map=new HashMap<String, Boolean>();
			
				SessionController sessionCtrl = new SessionController();
				SubscribeToFundController ctrl = new SubscribeToFundController();
				PortfolioCreateController pcCtrl = new PortfolioCreateController();				

				Long consumerId = null;
				Boolean subscribed = false;
				Boolean isSubscribed = false;
				Long mutualfundId = 0l;
				Long userID = 0l;
				String amfiiCode = null;
				
				consumerId = sessionCtrl.getClient(appId, ipAddress);
				if(consumerId != null)
				{
					if(sessionCtrl.getValidSession(consumerId, sessionId))
					{
						if(ctrl.validUser(consumerId, userId))
						{
							userID = pcCtrl.getUserId(userId, consumerId);
							for (int i = 0; i < array.length(); i++)
							{
								System.out.println(array.get(i));
								JSONObject obj = (JSONObject) array.get(i);
								amfiiCode = obj.getString("amfiiCode");
								System.out.println("amfiiCode: " + amfiiCode);
	
								mutualfundId = ctrl.getMutualfundID(amfiiCode);
								System.out.println("2th Condition.");
								if(!ctrl.alreadySubscribed(mutualfundId, userID, consumerId))
								{
									map.put(amfiiCode, ctrl.subscribeToFund(userId, amfiiCode));
									
									isSubscribed = ctrl.subscribeToFund(userId, amfiiCode);
									System.out.println("4th Condition.");
									if(isSubscribed && subscribed==false)
									{
										subscribed = true;
									}
								}
								else
								{
									System.out.println("You are already subscribed to this Mutual Fund: " + amfiiCode);
								}
							}
							if(subscribed)
								jobject.put("success", true);
							else
							{
								jobject.put("success", false);
								JSONObject failure = new JSONObject();
								failure.put("message", "AMFII Code does not exist.");		// Change the error message.
								failure.put("code", "706");
								jobject.put("error", failure);
								System.out.println("SubscribeToFund not Service invoked Successfully.");
							}
						}
						else
						{
							jobject.put("success", false);
							JSONObject failure = new JSONObject();
							failure.put("message", "User ID does not exist.");
							failure.put("code", "704");
							jobject.put("error", failure);
							System.out.println("SubscribeToFund not invoked Successfully.");
						}
					}
					else
					{
						jobject.put("success", false);
						JSONObject failure = new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");
					}
				}
				else
				{
					jobject.put("success", false);
					JSONObject failure = new JSONObject();
					failure.put("message", "App ID does not exist.");
					failure.put("code", "701");
					jobject.put("error", failure);
					System.out.println("Subscribe to fund Service not invoked Successfully.");
				}
		    }
		}
		catch (Exception e)
		{
			System.out.println("Error Parsing: - " + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}

	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService()
	{
		String result = "APIDocumentationForFE Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}