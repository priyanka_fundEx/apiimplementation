package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.GetAlertOnFundORPortfolioController;
import com.fundexpert.controller.GetClientPortfolioController;
import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribePortfolioManagementController;
import com.fundexpert.controller.SubscribeToFundController;

@Path("/user/portfolio/subscription/create")
public class SubscribePortfolioManagement {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String sessionId=null;
		String appId=null;
		String userId=null;
		Integer noOfMonths = 0;
		String ipAddress=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject=new JSONObject(builder.toString());
			
			boolean flag=false;
			
			if(jObject.getString("appId")!=null && !jObject.getString("appId").equals("") && jObject.getString("sessionId")!=null  
						&& !jObject.getString("sessionId").equals("") && jObject.getString("userId")!=null && !jObject.getString("userId").equals("")
						&& jObject.getString("noOfMonths")!=null && !jObject.getString("noOfMonths").equals(""))
			{
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId");  
			    userId=jObject.getString("userId");
				 noOfMonths = Integer.parseInt(jObject.getString("noOfMonths"));
			    flag=true;
			}
			else{
				jobject.put("success", false);
				JSONObject failure=new JSONObject();
				failure.put("message", "Invalid or Empty parameters.");
				failure.put("code", "705");
				jobject.put("error", failure);
				System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
			}
			
			ipAddress=request.getRemoteAddr(); 
		    
		    if(flag && noOfMonths>=12)
		    {		    
			    SessionController sessionCtrl=new SessionController();
				GetClientPortfolioController pfCtrl=new GetClientPortfolioController();
				SubscribeToFundController fundCtrl=new SubscribeToFundController();
				GetAlertOnFundORPortfolioController alertCtrl=new GetAlertOnFundORPortfolioController();
				SubscribePortfolioManagementController ctrl=new SubscribePortfolioManagementController(); 
			
				Long consumerId=null;
				Long userID=null;
				
			    consumerId=sessionCtrl.getClient(appId, ipAddress);
				 if(consumerId!=null)
				    {
				    	if(sessionCtrl.getValidSession(consumerId, sessionId))
				    	{
				    		if(fundCtrl.validUser(consumerId, userId))
							{	
				    			 userID=pfCtrl.getUserId(userId, consumerId);
				    			if(!alertCtrl.isPortfolioSubscribed(userID))
				    			{
				    				if(ctrl.subscribeToProtfolio(noOfMonths, userId, consumerId))
						    		{
						    			jobject.put("success", true);
										System.out.println("Subscribe To Portfolio invoked Successfully.");
						    		}
				    			}
				    			else
				    			{										
										System.out.println("This User is Already Subscribed for Portfolio.");			    				 
				    			 }				    				
							}
				    		else{
									jobject.put("success", false);
									JSONObject failure=new JSONObject();
									failure.put("message", "User ID does not exist.");
									failure.put("code", "704");
									jobject.put("error", failure);  
									System.out.println("Subscribe To Portfolio not invoked Successfully.");
							}
				    	}
				    	else
			    		{
			    		    jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			    			System.out.println("Subscribe To Portfolio not invoked Successfully."+jobject.toString());
			    		}
				    }
				    else
		    		{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
		    			System.out.println("Subscribe To Portfolio not invoked Successfully."+jobject.toString());
		    		} 
		    }
		    else
    		{
    		    jobject.put("success", false);
				JSONObject failure=new JSONObject();
				failure.put("message", "Number of months should be greater than 12.");
				failure.put("code", "741");
				jobject.put("error", failure); 
    			System.out.println("Subscribe To Portfolio not invoked Successfully."+jobject.toString());
    		} 			
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+ e.getLocalizedMessage());
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}