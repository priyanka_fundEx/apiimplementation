package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.RegisterUserController;

public class RegisterUserClient 
{
	String appId, sessionid;
	public RegisterUserClient(String appId, String sessionId)
	{
		this.appId=appId;
		this.sessionid=sessionId;
	}
	public static void main(String[] args) 
	{
		StringBuilder builder=new StringBuilder();
		String input = "";
		try {	
			input="{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"8cf-8082-7\"}";
			JSONObject jsonObject=new JSONObject(input);
			System.out.println("JSON Object: "+jsonObject);
			
			try {
				URL url=new URL("http://localhost:8080/APIDocumentationForFE/rest/user/create");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					System.out.println(line);
					builder.append(line);
					line=br.readLine();
				}

				JSONObject jObject=new JSONObject(builder.toString());
				
				System.out.println("ApiDOc REST Service for Register User Invoked Successfully.. "+jObject);
				br.close(); 
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST Register User Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/*public String run() 
	{
		String userId=null;
		//String input = "";
		try 
		{
			//input="{\"appId\": \"qwertyuiop12345\", \"sessionId\": \"lksjdafkjslfkjakfjsa\", \"userId\": \"user1\",\"riskAppetite\": \"01\"}";
			//JSONObject jsonObject=new JSONObject(input);
			
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("appId", appId);
			jsonObject.put("sessionId", sessionid);
			System.out.println("JSON Object: "+jsonObject.toString());
			
			StringBuilder builder=new StringBuilder();
			try {
				
				URL url=new URL("http://localhost:8080/APIDocumentationForFE/rest/user/create");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(5000);
				con.setReadTimeout(5000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					System.out.println(line);
					builder.append(line);
					line=br.readLine();
				}

				JSONObject jObject=new JSONObject(builder.toString());
				
				Map<String,String> map = new HashMap<String,String>();
			    Iterator iter = jObject.keys();
			    while(iter.hasNext()){
			        String key = (String)iter.next();
			        String value = jObject.getString(key);
			        map.put(key,value);
			    }
			    
			    userId=map.get("userId");
				System.out.println("ApiDOc REST Service for LOGIN Invoked Successfully..");
				br.close();
				return userId;
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST LOGIN Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return userId;
	}*/
}
