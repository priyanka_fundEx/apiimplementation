package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.codehaus.jettison.json.JSONObject;

public class PortfolioCreateClient
{

	public static void main(String[] args)
	{
		StringBuilder builder = new StringBuilder();
		String input = "";											// 103196,111644,100848
		try
		{
					//		// a8a-89f1-6b79bb878b47 for userID 5   // 77a-bcd0-bb3a3fa63855 for userID 3	 	// 1f2-aac3-fb3648d36c7b for userID 6
			input = "{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"ae6-9d22-e\" ,\"userId\": \"7f5-87a9-23059fcbd6ba\",\"transactions\": "+
				    "[{\"folioNumber\": \"10200153/3\",\"amfiiCode\": \"102001\", \"optionType\": \"DIVIDEND PAYOUT\", \"units\": \"200.03\",\"nav\": \"53.02\",\"action\": \"invest\",\"date\": \"2017-05-03\"},"+
					"{\"folioNumber\": \"12882053/3\",\"amfiiCode\": \"128820\", \"optionType\": \"DIVIDEND REINVEST\", \"units\": \"200.03\",\"nav\": \"52.01\",\"action\": \"invest\",\"date\": \"2017-07-03\"},"+
					"{\"folioNumber\": \"12530553/3\",\"amfiiCode\": \"125305\", \"optionType\": \"GROWTH\", \"units\": \"200.03\",\"nav\": \"54.0\",\"action\": \"invest\",\"date\": \"2017-05-03\"},"+
					  "{\"folioNumber\": \"10276076/3\", \"amfiiCode\": \"102760\",\"optionType\": \"GROWTH\", \"units\": \"150.43\", \"nav\": \"56.01\", \"action\": \"invest\", \"date\": \"2017-08-27\"}]}";

			JSONObject jsonObject = new JSONObject(input);

			System.out.println("INPUT JSON Object: " + jsonObject);

			try
			{
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/portfolio/create");
				URLConnection con = url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);

				OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				System.out.println("Output Strem Writer.");

				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line = br.readLine();
				while (line != null)
				{
					builder.append(line);
					line = br.readLine();
				}
				System.out.println("ApiDOc REST Service for Portfolio Create Invoked Successfully..");
				br.close();
				// System.out.println("Session Id: "+sessionId +", and ConsumerId: "+consumerId);
			}
			catch (Exception e)
			{
				System.out.println("Error while calling ApiDOc REST Subscribe to fund Service");
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}

/*
 // Equity TYPE Taxable
input = "{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"f89-b33d-2\" ,\"userId\": \"1f2-aac3-fb3648d36c7b\",\"transactions\": "+
					"[ {\"folioNumber\": \"10200153/3\",\"amfiiCode\": \"102001\", \"optionType\": \"DIVIDEND PAYOUT\", \"units\": \"200.03\",\"nav\": \"53.02\",\"action\": \"invest\",\"date\": \"2017-05-03\"},"+
					"{\"folioNumber\": \"12882053/3\",\"amfiiCode\": \"128820\", \"optionType\": \"DIVIDEND REINVEST\", \"units\": \"200.03\",\"nav\": \"52.01\",\"action\": \"invest\",\"date\": \"2017-07-03\"},"+
					"{\"folioNumber\": \"12530553/3\",\"amfiiCode\": \"125305\", \"optionType\": \"GROWTH\", \"units\": \"200.03\",\"nav\": \"54.0\",\"action\": \"invest\",\"date\": \"2017-05-03\"},"+
					  "{\"folioNumber\": \"10276076/3\", \"amfiiCode\": \"102760\",\"optionType\": \"GROWTH\", \"units\": \"150.43\", \"nav\": \"56.01\", \"action\": \"invest\", \"date\": \"2017-08-27\"}]}";
			

 // Equity  TYPE non Taxable
input = "{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"f89-b33d-2\" ,\"userId\": \"1f2-aac3-fb3648d36c7b\",\"transactions\": "+
"[ {\"folioNumber\": \"10200153/3\",\"amfiiCode\": \"102001\", \"optionType\": \"DIVIDEND PAYOUT\", \"units\": \"200.03\",\"nav\": \"53.02\",\"action\": \"invest\",\"date\": \"2015-05-03\"},"+
"{\"folioNumber\": \"12882053/3\",\"amfiiCode\": \"128820\", \"optionType\": \"DIVIDEND REINVEST\", \"units\": \"200.03\",\"nav\": \"52.01\",\"action\": \"invest\",\"date\": \"2015-07-03\"},"+
"{\"folioNumber\": \"12530553/3\",\"amfiiCode\": \"125305\", \"optionType\": \"GROWTH\", \"units\": \"200.03\",\"nav\": \"54.0\",\"action\": \"invest\",\"date\": \"2016-05-03\"},"+
  "{\"folioNumber\": \"10276076/3\", \"amfiiCode\": \"102760\",\"optionType\": \"GROWTH\", \"units\": \"150.43\", \"nav\": \"56.01\", \"action\": \"invest\", \"date\": \"2016-08-27\"}]}";

// DEBT Type NonTaxable
input = "{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"f89-b33d-2\" ,\"userId\": \"1f2-aac3-fb3648d36c7b\",\"transactions\": "+
					"[ {\"folioNumber\": \"10187353/3\",\"amfiiCode\": \"101873\", \"optionType\": \"DIVIDEND PAYOUT\", \"units\": \"120.03\",\"nav\": \"50.02\",\"action\": \"invest\",\"date\": \"2013-05-03\"},"+
					"{\"folioNumber\": \"11907576/3\", \"amfiiCode\": \"119075\",\"optionType\": \"GROWTH\", \"units\": \"80.43\", \"nav\": \"52.01\", \"action\": \"invest\", \"date\": \"2013-08-27\"}]}";

// DEBT Type  Taxable
input = "{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"f89-b33d-2\" ,\"userId\": \"1f2-aac3-fb3648d36c7b\",\"transactions\": "+
					"[ {\"folioNumber\": \"10187353/3\",\"amfiiCode\": \"101873\", \"optionType\": \"DIVIDEND PAYOUT\", \"units\": \"120.03\",\"nav\": \"50.02\",\"action\": \"invest\",\"date\": \"2015-05-03\"},"+
					"{\"folioNumber\": \"11907576/3\", \"amfiiCode\": \"119075\",\"optionType\": \"GROWTH\", \"units\": \"80.43\", \"nav\": \"52.01\", \"action\": \"invest\", \"date\": \"2016-08-27\"}]}";

DEBT Type non taxable
input = "{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"f89-b33d-2\" ,\"userId\": \"77a-bcd0-bb3a3fa63855\",\"transactions\": "+
					"[{\"folioNumber\": \"10187353/3\",\"amfiiCode\": \"101873\", \"optionType\": \"DIVIDEND PAYOUT\", \"units\": \"200.03\",\"nav\": \"53.02\",\"action\": \"invest\",\"date\": \"2013-05-03\"},"+
					"{\"folioNumber\": \"10187476/3\", \"amfiiCode\": \"101874\",\"optionType\": \"DIVIDEND PAYOUT\", \"units\": \"80.03\", \"nav\": \"52.02\", \"action\": \"invest\", \"date\": \"2013-08-27\"},"+
					"{\"folioNumber\": \"11907576/3\", \"amfiiCode\": \"119075\",\"optionType\": \"GROWTH\", \"units\": \"200.03\", \"nav\": \"53.02\", \"action\": \"invest\", \"date\": \"2013-08-07\"}]}";
			
*/