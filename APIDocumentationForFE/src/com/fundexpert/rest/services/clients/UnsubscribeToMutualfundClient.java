package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.codehaus.jettison.json.JSONObject;

public class UnsubscribeToMutualfundClient {

	public static void main(String[] args) 
	{
		StringBuilder builder=new StringBuilder();
		String input = "";
		try {
			input="{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"783-9ba7-5\" ,\"userId\": \"32e-a067-b28f9dbf8271\" ,\"amfiiCode\": \"112323\"}";
			JSONObject jsonObject=new JSONObject(input);
			
			/*JSONObject jsonObject=new JSONObject();
			jsonObject.put("appId", appId);
			jsonObject.put("secretKey", secretKey);*/
			System.out.println("INPUT JSON Object: "+jsonObject);
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/mutualfund/subscription/destroy");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				System.out.println("Output Strem Writer.");
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					//System.out.println(line);
					builder.append(line);
					line=br.readLine();
				}
				System.out.println("ApiDOc REST Service for Subscribe to fund Invoked Successfully..");
				br.close();
				//System.out.println("Session Id: "+sessionId +", and ConsumerId: "+consumerId);
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST Subscribe to fund Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}