package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.LoginController;

public class LoginClient
{
	public static void main(String[] args) 
	{
		StringBuilder builder=new StringBuilder();
		String input = "";
		try {			
			input="{\"appId\": \"fundexpert\",\"secretKey\": \"fundexpert\"}";
			JSONObject jsonObject=new JSONObject(input);
			System.out.println("JSON Object: "+jsonObject);
			
			try {
				URL url = new URL("http://api.fundexpert.in/rest/consumer/login");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					System.out.println(line);
					builder.append(line);
					line=br.readLine();
				}

				JSONObject jObject=new JSONObject(builder.toString());
				
				System.out.println("ApiDOc REST Service for LOGIN Invoked Successfully.. "+jObject);
				br.close(); 
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST LOGIN Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}