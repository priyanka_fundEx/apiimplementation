package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;

public class SubscribeToFundClient 
{
	public static void main(String[] args) 
	{
		StringBuilder builder=new StringBuilder();
		String input = "";
		try {
			input="{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"9b6-9446-5\" ,\"userId\": \"32e-a067-b28f9dbf8271\" ,\"mutualfund\":[{\"amfiiCode\": \"111644\"},{\"amfiiCode\": \"107578\"}]}";
						
			JSONObject jsonObject=new JSONObject(input);
			System.out.println(jsonObject.get("appId"));
			System.out.println(jsonObject.get("sessionId"));
			System.out.println(jsonObject.get("userId"));
			System.out.println(jsonObject.get("mutualfund"));
			
			
			System.out.println("INPUT JSON Object: "+jsonObject);
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/mutualfund/subscription/create");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				System.out.println("Output Strem Writer.");
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					//System.out.println(line);
					builder.append(line);
					line=br.readLine();
				}
				System.out.println("ApiDOc REST Service for Subscribe to fund Invoked Successfully.."+builder.toString());
				br.close();
				//System.out.println("Session Id: "+sessionId +", and ConsumerId: "+consumerId);
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST Subscribe to fund Service: 	"+e.getMessage());
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}