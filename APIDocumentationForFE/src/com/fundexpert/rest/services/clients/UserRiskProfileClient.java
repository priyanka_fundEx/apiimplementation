package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.RegisterUserController; 

public class UserRiskProfileClient 
{
	String appId, sessionid, userId;
	public UserRiskProfileClient(String appId, String sessionId, String userId)
	{
		this.appId=appId;
		this.sessionid=sessionId;
		this.userId=userId;
	}
	public static void main(String[] args)
	{
		String userId=null;
		String input = "";
		try 
		{
			input="{\"appId\": \"qwertyuiop12345\", \"sessionId\": \"29852568\", \"userId\": \"11e-beaf-25a4b0158e1b\",\"riskAppetite\": \"01\"}";
			JSONObject jsonObject=new JSONObject(input);
			
			/*JSONObject jsonObject=new JSONObject();
			jsonObject.put("appId", appId);
			jsonObject.put("sessionId", sessionid);*/
			System.out.println("JSON Object: "+jsonObject.toString());
			
			StringBuilder builder=new StringBuilder();
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/riskprofile/update");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null)
				{
					builder.append(line);
					System.out.println(line);
					line=br.readLine();
				}
 
				/*JSONObject jObject=new JSONObject(builder.toString());
				
				Map<String,String> map = new HashMap<String,String>();
			    Iterator iter = jObject.keys();
			    while(iter.hasNext()){
			        String key = (String)iter.next();
			        String value = jObject.getString(key);
			        map.put(key,value);
			    }
			    
			    userId=map.get("userId");*/
				System.out.println("User Updated Successfully");
				br.close();
				//return userId;
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST LOGIN Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//return userId;
	}
}