package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.codehaus.jettison.json.JSONObject;

public class LumpsumRedeemClient 
{
	public static void main(String[] args) 
	{
		StringBuilder builder=new StringBuilder();
		String input = "";
		try {
			input="{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"a03-869e-1\" ,\"userId\": \"a8a-89f1-6b79bb878b47\" ,\"amount\": \"70000\" }";
			JSONObject jsonObject=new JSONObject(input);
			 
			System.out.println("INPUT JSON Object: "+jsonObject);
			
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/lumpsum/redeem");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				System.out.println("Output Stream Writer.");
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){ 
					builder.append(line);
					line=br.readLine();
				}
				System.out.println("ApiDOc REST Service for Lump sum Redeem Invoked Successfully..");
				br.close(); 
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST Lump sum Redeem Service");
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}