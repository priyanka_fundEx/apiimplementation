package com.fundexpert.rest.services.clients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.dao.User;

public class ListUserClient 
{
	public static void main(String[] args)
	{
		String input = "";
		try {
			input="{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"349-9286-a\"}";
			JSONObject jsonObject=new JSONObject(input);
			System.out.println("JSON Object: "+jsonObject);
			
			StringBuilder builder=new StringBuilder();
			try {
				URL url = new URL("http://localhost:8080/APIDocumentationForFE/rest/user/list");
				URLConnection con=url.openConnection();
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(50000);
				con.setReadTimeout(50000);
				
				OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
				writer.write(jsonObject.toString());
				writer.close();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line=br.readLine();
				while(line!=null){
					System.out.println(line);
					builder.append(line);
					line=br.readLine();
				}
				
				JSONObject jObject=new JSONObject(builder.toString()); 

				System.out.println("ApiDOc REST Service for Register User Invoked Successfully.. "+jObject.toString(1));
				//System.out.println("ApiDOc REST Service for List User Invoked Successfully..");
				br.close();
			} catch (Exception e) {
				System.out.println("Error while calling ApiDOc REST List User Invoked Service");
				e.printStackTrace();
			} 
		}
		catch(Exception e)
		{
			e.printStackTrace();
		} 
	}
}