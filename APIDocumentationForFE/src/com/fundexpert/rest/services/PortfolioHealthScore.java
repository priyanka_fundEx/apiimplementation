package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.fundexpert.controller.GetAlertOnFundORPortfolioController;
import com.fundexpert.controller.GetClientPortfolioController;
import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.PortfolioHealthScoreController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;
import com.fundexpert.dao.Holding;

@Path("/user/portfolio/healthscore")
public class PortfolioHealthScore 
{
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		Long consumerId=null;
		String sessionId=null;
		String appId=null;
		String userId=null;
		String ipAddress=null;
		
		List<Holding> modifiedHoldingList=new ArrayList<Holding>(); 
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject=new JSONObject(builder.toString()); 
			
		    boolean flag=false;
			
			if(jObject.getString("appId")!=null && !jObject.getString("appId").equals("") && jObject.getString("sessionId")!=null  
						&& !jObject.getString("sessionId").equals("") && jObject.getString("userId")!=null && !jObject.getString("userId").equals(""))
			{
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId");  
				userId = jObject.getString("userId"); 
			    flag=true;
			}
			else{
				jobject.put("success", false);
				JSONObject failure=new JSONObject();
				failure.put("message", "Invalid or Empty parameters.");
				failure.put("code", "705");
				jobject.put("error", failure);
				System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
			}
			
			ipAddress=request.getRemoteAddr();
			
		    if(flag) 
		    {		    
			    SessionController sessionCtrl=new SessionController();
				GetClientPortfolioController pfCtrl=new GetClientPortfolioController();
				GetAlertOnFundORPortfolioController alertCtrl=new GetAlertOnFundORPortfolioController();
				SubscribeToFundController fundCtrl=new SubscribeToFundController();
				PortfolioHealthScoreController ctrl=new PortfolioHealthScoreController();

				Long userID=null;
				Long amfiiCode = null;
				
			    consumerId=sessionCtrl.getClient(appId, ipAddress);
				 if(consumerId!=null)
				    {
				    	if(sessionCtrl.getValidSession(consumerId, sessionId))
				    	{
				    		 if(fundCtrl.validUser(consumerId, userId))
							{
				    			 userID=pfCtrl.getUserId(userId, consumerId);
				    			 
				    			 if(alertCtrl.isPortfolioSubscribed(userID))
				    			 {
					    			 System.out.println("User ID: "+userID);
				    				 List<Holding> holdingList  = ctrl.getHoldingList(userID);
				    				 System.out.println("Holding List: "+holdingList);
				    				 
				    				 
				    				 List<Double> fvList=new ArrayList<Double>();      
				    				 List<Double> weightList=new ArrayList<Double>();       
				    				 List<Double> rankList=new ArrayList<Double>();
			    					 double totalValue=0.0;
			    					 
				    				 for(Holding h:holdingList)
				    				 {
				    					 double rank=ctrl.getRating(h.getMutualfundId());
				    					 if(rank>0)
				    					 {
				    						 rankList.add(rank);			    						 
				    						 fvList.add(h.getAvgNav()*h.getUnits());
				    						 totalValue= h.getAvgNav()*h.getUnits();
				    						 //System.out.println(h.getId()+": totalValue: "+totalValue);	
				    						 
				    						 modifiedHoldingList.add(h);
				    					 }
				    				 }
				    				 
				    				 for(int i=0;i<fvList.size();i++)
			    					 {
			    						 weightList.add((fvList.get(i)*100)/totalValue);
			    					 }
			    					 System.out.println("Weieght List: "+weightList);
			    					 System.out.println("Rank List: "+rankList);
			    					 System.out.println("fv List: "+fvList);
			    					 double totalWeight=0.0, totalWeightRate=0.0;
			    					 for(int i=0;i<weightList.size();i++)
			    					 {			
			    						 /* System.out.print("Rank: "+rankList.get(i)+",  ");
			    						 System.out.print("Weight: "+weightList.get(i)+",  ");*/
			    						 
			    						 totalWeight+=weightList.get(i);
			    						 
			    						 totalWeightRate+=weightList.get(i)*rankList.get(i);
			    					 }
			    					 
			    					 System.out.println("Modified Holding List: "+modifiedHoldingList);
			    					 System.out.println("Total Weight: "+totalWeight+",   Total Value: "+totalValue+", totalWeightRate: "+totalWeightRate);
			    					 double healthScore=totalWeightRate/totalWeight;
			    					 System.out.println("healthScore: "+healthScore); 
			    					 
					    			 jobject.put("success", true);
					    			 jobject.put("portfolio", healthScore);				    			 
					    			 
					    			 JSONArray array=new JSONArray();
					    			 for(int i=0;i<modifiedHoldingList.size();i++)
					    			 {
					    				 Holding h=modifiedHoldingList.get(i);
						    			 JSONObject obj=new JSONObject();
						    			 amfiiCode=alertCtrl.getAmfiiCode(h.getMutualfundId());
					    					
						    			 obj.put("amfiiCode", amfiiCode);
						    			 obj.put("rank", rankList.get(i));
						    			 array.put(obj);
					    			 }
	
					    			 jobject.put("mutualfund", array);		
				    			 }
				    			 else
				    			 {
										jobject.put("success", false);
										JSONObject failure=new JSONObject();
										failure.put("message", "User portfolio not subscribed.");
										failure.put("code", "709");
										jobject.put("error", failure);
										System.out.println("Portfolio Health Score Service not invoked Successfully.");			    				 
				    			 }
							}else{
								jobject.put("success", false);
								JSONObject failure=new JSONObject();
								failure.put("message", "User ID does not exist.");
								failure.put("code", "704");
								jobject.put("error", failure);  
								System.out.println("Portfolio Health Score Service not invoked Successfully.");
							}
				    	}
				    	else
			    		{
			    		    jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			    			System.out.println("Portfolio Health Score Service has not invoked Successfully.   "+jobject.toString());
			    		}
				    }
				    else
		    		{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
		    			System.out.println("Portfolio Health Score Service has not invoked Successfully.   "+jobject.toString());
		    		}		
		    }
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}