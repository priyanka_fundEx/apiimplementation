package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.NavController;
import com.fundexpert.controller.RegisterUserController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.dao.Consumer;

@Path("/nav")
public class NavService {

	@Path("/update")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNav(InputStream incomingData, @Context HttpServletRequest request)
	{

		JSONObject jObject=null;
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		boolean flag=false;
		JSONObject outputJson=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			jObject=new JSONObject(builder.toString());
			//System.out.println("Received Json!@="+jObject.toString(1)+"!@");
			 
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jObject=new JSONObject();
				jObject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","App Id empty.");
				jObject.put("error", failure);
				return Response.status(400).entity(jObject.toString()).build();
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jObject=new JSONObject();
				jObject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","Secret Key does not exist.");
				jObject.put("error", failure);
				return Response.status(400).entity(jObject.toString()).build();
			}
			else if(!jObject.has("data") || jObject.getJSONArray("data").length()==0)
			{
				jObject=new JSONObject();
				jObject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","Nav data array is empty.");
				jObject.put("error", failure);
				return Response.status(400).entity(jObject.toString()).build();
			}
			else
			{
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId"); 
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				ipAddress=request.getRemoteAddr();
			//	System.out.println("ipAddress:"+ipAddress);
				if(con==null)
				{
					jObject=new JSONObject();
					jObject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("error","App Id does not exist.");
					jObject.put("error", failure);
				}
				else if(con.isIpHardcoded())
				{	
					flag=false;
					if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
					{	
						flag=false;
						jObject=new JSONObject();
						jObject.put("success", false);
						
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("error","Invalid Request.");
						jObject.put("error", failure);
						return Response.status(400).entity(jObject.toString()).build();
					}	
					else
					{
						flag=true;
					}
				}
				else if(!con.isIpHardcoded())
				{
					flag=true;
				}
				NavController navController=new NavController();				
				SessionController sessionCtrl=new SessionController();
				if(flag)
				{
					if(sessionCtrl.getValidApiSyncSession(con.getId(), sessionId))
			    	{
					    jObject = navController.getNavData(jObject);
					    if(jObject==null)
					    {
					    //	System.out.println("Exception occurred in Nav service.");
					    	throw new Exception("Exception occurred internally.");
					    }
			    	}
			    	else
			    	{
			    		jObject=new JSONObject();
				    	jObject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jObject.put("error", failure);
						System.out.println("Nav Data not received successfully.");		    		
			    	}
				}
			}
			//System.out.println("Sent Nav="+jObject.toString(1));
			return Response.status(200).entity(jObject.toString()).build();
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
			jObject=new JSONObject();
			jObject.put("success", false);
			jObject.put("error", e.getMessage().equals("Exception occurred internally.")?jObject.put("error", e.getMessage()):jObject.put("error", "Exception occurred."));
			return Response.status(400).entity(jObject.toString()).build();
		}
	}
	
	@Path("/add")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNewNav(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jObject=null;
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null;
		String ipAddress=null;
		boolean flag=false;
		JSONObject outputJson=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			jObject=new JSONObject(builder.toString());
			//System.out.println("Received Json!@="+jObject.toString(1)+"!@");
			 
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jObject=new JSONObject();
				jObject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","App Id empty.");
				jObject.put("error", failure);
				return Response.status(400).entity(jObject.toString()).build();
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jObject=new JSONObject();
				jObject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","Secret Key does not exist.");
				jObject.put("error", failure);
				return Response.status(400).entity(jObject.toString()).build();
			}
			else if(!jObject.has("data") || jObject.getJSONArray("data").length()==0)
			{
				jObject=new JSONObject();
				jObject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","Nav data array is empty.");
				jObject.put("error", failure);
				return Response.status(400).entity(jObject.toString()).build();
			}
			else
			{
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId"); 
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				ipAddress=request.getRemoteAddr();
			//	System.out.println("ipAddress:"+ipAddress);
				if(con==null)
				{
					jObject=new JSONObject();
					jObject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("error","App Id does not exist.");
					jObject.put("error", failure);
				}
				else if(con.isIpHardcoded())
				{	
					flag=false;
					if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
					{	
						flag=false;
						jObject=new JSONObject();
						jObject.put("success", false);
						
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("error","Invalid Request.");
						jObject.put("error", failure);
						return Response.status(400).entity(jObject.toString()).build();
					}	
					else
					{
						flag=true;
					}
				}
				else if(!con.isIpHardcoded())
				{
					flag=true;
				}
				NavController navController=new NavController();				
				SessionController sessionCtrl=new SessionController();
				if(flag)
				{
					if(sessionCtrl.getValidApiSyncSession(con.getId(), sessionId))
			    	{
					    jObject = navController.getNewNavData(jObject);
					    if(jObject==null)
					    {
					    //	System.out.println("Exception occurred in Nav service.");
					    	throw new Exception("Exception occurred internally.");
					    }
			    	}
			    	else
			    	{
			    		jObject=new JSONObject();
				    	jObject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jObject.put("error", failure);
						System.out.println("Nav Data not received successfully.");		    		
			    	}
				}
			}
			//System.out.println("Sent Nav="+jObject.toString(1));
			return Response.status(200).entity(jObject.toString()).build();
		} catch (Exception e) {
			e.printStackTrace();
			jObject=new JSONObject();
			jObject.put("success", false);
			jObject.put("error", e.getMessage());
			return Response.status(400).entity(jObject.toString()).build();
		}
	}

}
