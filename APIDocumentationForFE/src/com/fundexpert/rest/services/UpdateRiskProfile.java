package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;
import com.fundexpert.controller.UpdateRiskProfileController; 

@Path("/user/riskprofile/update")
public class UpdateRiskProfile 
{ 
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response apiDocREST(InputStream incomingData, @Context HttpServletRequest request)throws JSONException 
	{
		JSONObject jobject=new JSONObject();
		String sessionId=null;
		String appId=null;
		String userId = null;
		String ipAddress=null;
		String riskProfile=null;
		StringBuilder builder=new StringBuilder();
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
		    JSONObject jObject  = new JSONObject(builder.toString());
 
			boolean flag=false;
		    
		    if(jObject.getString("appId")!=null && !jObject.getString("appId").equals("") && jObject.getString("sessionId")!=null  
					&& !jObject.getString("sessionId").equals("") && jObject.getString("userId")!=null && !jObject.getString("userId").equals("") 
					&& jObject.getString("riskProfile")!=null && !jObject.getString("riskProfile").equals(""))
			{
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId");  
				userId = jObject.getString("userId");	
				riskProfile=jObject.getString("riskProfile");   
			    flag=true;
			}
			else{
				jobject.put("success", false);
				JSONObject failure=new JSONObject();
				failure.put("message", "Invalid or Empty parameters.");
				failure.put("code", "705");
				jobject.put("error", failure);
				System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
			}
			
			ipAddress=request.getRemoteAddr();
			
		    if(flag) 
		    {
			    SessionController ctrl=new SessionController();
				SubscribeToFundController fundCtrl=new SubscribeToFundController();
			    UpdateRiskProfileController ctrl1=new UpdateRiskProfileController();

				Long consumerId=null;
				int riskAppetite=0;
				
			    if(riskProfile.equals("Conservative"))
					riskAppetite=1;
				else if(riskProfile.equals("Moderately Conservative"))
					riskAppetite=2;
				else if(riskProfile.equals("Moderate"))
					riskAppetite=3;
				else if(riskProfile.equals("Moderately Aggressive"))
					riskAppetite=4;
				else if(riskProfile.equals("Aggressive"))
					riskAppetite=5;
			    
			    System.out.println("appId: "+appId+",  SessionId: "+sessionId+", UserID: "+userId+", Risk Appetite: "+riskAppetite);
				
			    consumerId=ctrl.getClient(appId, ipAddress);
			    if(consumerId!=null)
			    {
			    	if(ctrl.getValidSession(consumerId, sessionId))
			    	{
			    		if(fundCtrl.validUser(consumerId, userId))
			    		{
				    		if(ctrl1.updateUser(userId, sessionId, riskAppetite))
			    		    {
				    		     jobject.put("success", true);
				    			System.out.println("User updated seccessfully.   "+jobject.toString());
			    		    }
				    		else
				    			System.out.println("User not updated successfully.");
			    		}
			    		else
			    		{
			    		    jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "User ID does not exist.");
							failure.put("code", "704");
							jobject.put("error", failure); 
			    			System.out.println("Update Risk Profile Service has not invoked Successfully.   "+jobject.toString());
			    		}
			    	}
			    	else
		    		{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure); 
		    			System.out.println("Update Risk Profile Service has not invoked Successfully.   "+jobject.toString());
		    		}
			    }
			    else
	    		{
	    		    jobject.put("success", false);
					JSONObject failure=new JSONObject();
					failure.put("message", "App ID does not exist.");
					failure.put("code", "701");
					jobject.put("error", failure); 
	    			System.out.println("Update Risk Profile Service has not invoked Successfully.   "+jobject.toString());
	    		}
		    }
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+e.getMessage());
			e.printStackTrace();
		}		
		// return HTTP response 200 in case of success
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData)
	{
		String result = "APIDocumentationForFE Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}   