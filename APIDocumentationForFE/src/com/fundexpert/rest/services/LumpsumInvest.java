package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.LumpsumInvestController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.User;

@Path("/user/lumpsum/invest")
public class LumpsumInvest {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
	    SessionController sessionCtrl=new SessionController();
		SubscribeToFundController fundCtrl=new SubscribeToFundController();
		LumpsumInvestController ctrl=new LumpsumInvestController();

		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		Long consumerId=null;
		String sessionId=null;
		String appId=null;
		String userId=null;
		Double amount = null;
		String ipAddress=null; 
		
		int riskAppetite=0;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject=new JSONObject(builder.toString());
			
			boolean flag=false;
			if(jObject.has("appId") && jObject.has("sessionId") && jObject.has("userId") && jObject.has("amount"))
			{
				if(!jObject.getString("appId").equals("") && !jObject.getString("sessionId").equals("") && 
						!jObject.getString("userId").equals("") && !jObject.getString("amount").equals("") )
				{
					appId= jObject.getString("appId");				
					sessionId = jObject.getString("sessionId");  
				    userId=jObject.getString("userId");
					amount = Double.parseDouble(jObject.getString("amount")); 
				    flag=true;
				}
				else{
					jobject.put("success", false);
					JSONObject failure=new JSONObject();
					failure.put("message", "Invalid or Empty parameters.");
					failure.put("code", "705");
					jobject.put("error", failure);
					System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
				}
			}
			else{
				System.out.println("All Key parameters does not exist in input JSON Object.");
			}  
			
			ipAddress=request.getRemoteAddr();
		    if(flag && amount>0 && !amount.isNaN()) 
		    {		    
			    consumerId=sessionCtrl.getClient(appId, ipAddress);
				 if(consumerId!=null)
				    {
				    	if(sessionCtrl.getValidSession(consumerId, sessionId))
				    	{	 
				    		if(fundCtrl.validUser(consumerId, userId))
							{  
				    			 JSONArray array=new JSONArray();
				    			
				    			// System.out.println(appId+",  "+sessionId+",   "+userId+",   "+amount);
				    			 User user=ctrl.getRiskAppetiteOfUser(userId);
				    			 riskAppetite=user.getRiskAppetite();
				    			 
				    			 String appetite;
				    			 Double equityAmount=0.0, debtAmount=0.0;
				    			 
				    			 if(riskAppetite==0)
				    			 {
				    				 System.out.println("Risk Appetite is: "+riskAppetite);
				    				 appetite="";			    				 
				    				 equityAmount=amount*50/100;
				    				 debtAmount=amount*50/100;
				    			 }
				    			 else if(riskAppetite==1)
				    			 {
				    				 System.out.println("Risk Appetite is: "+riskAppetite);
				    				 appetite="Conservative";
				    				 equityAmount=amount*10/100;
				    				 debtAmount=amount*90/100;
				    			 }
				    			 else  if(riskAppetite==2)
				    			 {
				    				 System.out.println("Risk Appetite is: "+riskAppetite);
				    				 appetite="Moderately Conservative";
				    				 equityAmount=amount*30/100;
				    				 debtAmount=amount*70/100;
				    			 }
				    			 else  if(riskAppetite==3)
				    			 {
				    				 System.out.println("Risk Appetite is: "+riskAppetite);
				    				 appetite="Moderate";
				    				 equityAmount=amount*60/100;
				    				 debtAmount=amount*40/100;
				    			 }
				    			 else  if(riskAppetite==4)
				    			 {
				    				 System.out.println("Risk Appetite is: "+riskAppetite);
				    				 appetite="Moderately aggressive";
				    				 equityAmount=amount*70/100;
				    				 debtAmount=amount*30/100;
				    			 }
				    			 else  if(riskAppetite==5)
				    			 {
				    				 System.out.println("Risk Appetite is: "+riskAppetite);
				    				 appetite="Aggressive";
				    				 equityAmount=amount*90/100;
				    				 debtAmount=amount*10/100;
				    			 }
				    			 
				    			 List<MutualFund> equityList=ctrl.get5StarFundsOfEquity();			    			 
				    			 for(MutualFund mf: equityList)
				    			 {
				    				 JSONObject equityMF=new JSONObject();
				    				 equityMF.put("amfiiCode", mf.getAmfiiCode());
				    				 equityMF.put("amount", equityAmount/equityList.size());
				    				 equityMF.put("date", "");
									 array.put(equityMF);  	
				    			 }
				    			 
				    			 List<MutualFund> debtList=ctrl.get5StartFundsOfDEBT();
				    			 for(MutualFund mf: debtList)
				    			 {
				    				 JSONObject debtMF=new JSONObject();
				    				 debtMF.put("amfiiCode", mf.getAmfiiCode());
				    				 debtMF.put("amount", debtAmount/equityList.size());
				    				 debtMF.put("date", "");
									 array.put(debtMF);  	
				    			 }
				    			 jobject.put("success", true);
				    			 jobject.put("mutualfunds", array);
							}
				    		else{
								jobject.put("success", false);
								JSONObject failure=new JSONObject();
								failure.put("message", "User ID does not exist.");
								failure.put("code", "704");
								jobject.put("error", failure);  
								System.out.println("Lumpsum Invest not invoked Successfully.");
							}			    		
				    	}
				    	else
			    		{
			    		    jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			    			System.out.println("Lumpsum Invest Service has not invoked Successfully.   "+jobject.toString());
			    		}
				    }
				    else
		    		{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
		    			System.out.println("Lumpsum Invest Service has not invoked Successfully.   "+jobject.toString());
		    		} 
			    }
		    else
    		{
    		    jobject.put("success", false);
				JSONObject failure=new JSONObject();
				failure.put("message", "Invalid Amount.");
				failure.put("code", "708");
				jobject.put("error", failure); 
    			System.out.println("Lumpsum Invest Service has not invoked Successfully.   "+jobject.toString());
    		}   
			
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+e.getMessage());
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}