package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.ArchiveApiRequestController;
import com.fundexpert.controller.LoginController;
import com.fundexpert.dao.ArchiveApiRequests;
import com.fundexpert.dao.Consumer;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.exception.InputFormatException;
@Path("/consumer/login")
public class Login 
{	 

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
		System.out.println("LOGIN API");
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String secretKey = null;
		String ipAddress=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			JSONObject jObject;
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
				
			try
			{
				jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<2 || jObject.length()>2)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("secretKey") || jObject.get("secretKey").equals("") || jObject.get("secretKey")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Secret Key does not exist.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
			    secretKey = jObject.getString("secretKey"); 
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				System.out.println("con="+con.getSecretKey()+"|"+secretKey.hashCode());
				Long consumerId=null;
				String sessionId=null;
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","App Id does not exist.");
					jobject.put("error", failure);
					flag=false;
					System.out.println("CON is null");
				}
				else if(!con.isActive())
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","Contact support.");
					jobject.put("error", failure);
					flag=false;
					System.out.println("CONSUMER is not active.");
				}
				else if(flag && !con.getSecretKey().equals(secretKey.hashCode()+""))
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","Secret Key does not exist.");
					jobject.put("error", failure);
					flag=false;
				}
				if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(!flag)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					System.out.println(con.getIpAddress());
					consumerId=con.getId();
					sessionId=ctrl.getSessionClient(ipAddress, consumerId);
					System.out.println("Session ID:"+sessionId);
					if(sessionId!=null)
					{
						
						jobject.put("success", true);
						jobject.put("sessionId", sessionId);
						ArchiveApiRequestController aarc=new ArchiveApiRequestController();
						aarc.saveRequest(consumerId, ArchiveApiRequests.login);
						System.out.println("Login Service in Consumer invoked Successfully.");
					}
				}
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message", fe.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message", "Internal Server Occurred.");
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).header("Access-Control-Allow-Origin", "*")
			      .entity(jobject.toString())
			      .build();
	}
	@Path("/syncUpLogin")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionIdForLogin(InputStream incomingData, @Context HttpServletRequest request)
	{
		System.out.println("LOGIN API For API sync up");
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String secretKey = null;
		String ipAddress=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			JSONObject jObject;
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
				
			try
			{
				jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new InputFormatException("Invalid input format.");
			}
			if(jObject.length()<2 || jObject.length()>2)
				throw new FundexpertException("Number of parameters mismatch.");
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("secretKey") || jObject.get("secretKey").equals("") || jObject.get("secretKey")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("message","Secret Key does not exist.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
			    secretKey = jObject.getString("secretKey"); 
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				//System.out.println("con="+con.getSecretKey()+"|"+secretKey.hashCode());
				Long consumerId=null;
				String sessionId=null;
				boolean flag=true;
				ipAddress=request.getRemoteAddr();
				
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","App Id does not exist.");
					jobject.put("error", failure);
					flag=false;
					System.out.println("CON is null");
				}
				else if(!con.isActive())
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","Contact support.");
					jobject.put("error", failure);
					flag=false;
					System.out.println("CONSUMER is not active.");
				}
				else if(flag && !con.getAutoSyncSecretKey().equals(secretKey.hashCode()+""))
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("message","Secret Key does not exist.");
					jobject.put("error", failure);
					flag=false;
				}
				if(flag && con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(!flag)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("message","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				if(flag)
				{
					System.out.println(con.getIpAddress());
					consumerId=con.getId();
					sessionId=ctrl.getApiSyncSessionClient(ipAddress, consumerId);
					System.out.println("Session ID:"+sessionId);
					if(sessionId!=null)
					{
						
						jobject.put("success", true);
						jobject.put("sessionId", sessionId);
						System.out.println("Login Service in Consumer invoked Successfully.");
					}
				}
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message", fe.getMessage());
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			try 
			{
				jobject.put("success", false);
				failure.put("code", "701");
				failure.put("message", "Internal Server Occurred.");
				jobject.put("error", failure);
			}
			catch (JSONException e1)
			{
				e1.printStackTrace();
			}
		}
		return Response.status(200).header("Access-Control-Allow-Origin", "*")
			      .entity(jobject.toString())
			      .build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}