package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.PortfolioCleanController;
import com.fundexpert.controller.PortfolioCreateController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;
import com.fundexpert.dao.MutualFund;

@Path("/user/portfolio/clean")
public class PortfolioClean {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
	    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String sessionId=null;
		String appId=null;
		String userId=null;
		String amfiiCode = null;
		String ipAddress=null;
		String folioNumber = null;
		String d=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject=new JSONObject(builder.toString());
			
			boolean flag=false;
			if(jObject.has("appId") && jObject.has("sessionId") && jObject.has("userId") && jObject.has("amfiiCode") && jObject.has("folioNumber") && jObject.has("date"))
			{
				if(!jObject.getString("appId").equals("") && !jObject.getString("sessionId").equals("") && !jObject.getString("userId").equals("") 
							&& !jObject.getString("amfiiCode").equals("") && !jObject.getString("folioNumber").equals("") && !jObject.getString("date").equals(""))
				{
					appId= jObject.getString("appId");		
					sessionId = jObject.getString("sessionId");  
					userId = jObject.getString("userId");	
				    amfiiCode=jObject.getString("amfiiCode"); 
					folioNumber = jObject.getString("folioNumber"); 
					d=jObject.getString("date");
				    flag=true;
				}
				else{
					jobject.put("success", false);
					JSONObject failure=new JSONObject();
					failure.put("message", "Invalid or Empty parameters.");
					failure.put("code", "705");
					jobject.put("error", failure);
					System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
				}
			}
			else{
				System.out.println("All Key parameters does not exist in input JSON Object.");
			}  
			
			ipAddress=request.getRemoteAddr();
			
		    if(flag) 
		    {
			    SessionController sessionCtrl=new SessionController();
				SubscribeToFundController fundCtrl=new SubscribeToFundController();
				PortfolioCleanController ctrl=new PortfolioCleanController();
				PortfolioCreateController pfCtrl=new PortfolioCreateController();

				Long consumerId=null;
				Long userID=null;
				Calendar date=Calendar.getInstance();
				date.setTime(sdf.parse(d));
				date.setTime(new Date(Long.parseLong(d)));
				
			    consumerId=sessionCtrl.getClient(appId, ipAddress);
				if(consumerId!=null)
				{
			    	if(sessionCtrl.getValidSession(consumerId, sessionId))
			    	{
			    		 if(fundCtrl.validUser(consumerId, userId))
						{ 
			    			userID=pfCtrl.getUserId(userId, consumerId);
			    			System.out.println(appId+",  "+sessionId+",   "+userId+",   "+amfiiCode+",   "+folioNumber+",   "+d);
			    			 
			    			 if(amfiiCode!=null || folioNumber!=null || date!=null)
			    			 {
			    				 List<MutualFund> mfList=pfCtrl.getMFIdList(amfiiCode);
			    				 for(MutualFund mf: mfList)
			    				 {
			    					// Long mfId=pfCtrl.get
			    					 if(ctrl.isExistInPortfolio(mf.getId(), userID))
			    						 ctrl.cleanPortfolio(mf.getId(), folioNumber, date);
			    					 else
			    						 throw new ValidationException("No Holdings Exist in Portfolio for this AmfiiCode ");
			    				 }
			    			 }
						}
			    		 else{
							jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "User ID does not exist.");
							failure.put("code", "704");
							jobject.put("error", failure);  
							System.out.println("SubscribeToFund not invoked Successfully.");
						}
			    	}
			    	else
		    		{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure); 
		    			System.out.println("Update Risk Profile Service has not invoked Successfully.   "+jobject.toString());
		    		}
			    }
			    else
	    		{
	    		    jobject.put("success", false);
					JSONObject failure=new JSONObject();
					failure.put("message", "App ID does not exist.");
					failure.put("code", "701");
					jobject.put("error", failure); 
	    			System.out.println("Update Risk Profile Service has not invoked Successfully.   "+jobject.toString());
	    		}    
		    }
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+e.getMessage());
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}