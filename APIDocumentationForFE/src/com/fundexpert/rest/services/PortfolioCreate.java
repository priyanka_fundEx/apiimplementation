package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
 






import org.json.JSONArray;
import org.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.PortfolioCreateController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;
import com.fundexpert.dao.Holding;

@Path("/user/portfolio/create")
public class PortfolioCreate 
{
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	    
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String sessionId=null;
		String appId=null;
		String userId=null;
		String ipAddress=null;
		JSONArray array=new JSONArray();
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject=new JSONObject(builder.toString());
			 
		    boolean flag=false;
			
			if(jObject.getString("appId")!=null && !jObject.getString("appId").equals("") && jObject.getString("sessionId")!=null  
				&& !jObject.getString("sessionId").equals("") && jObject.getString("userId")!=null && jObject.getString("userId")!=null)
			{
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId");  
			     userId=jObject.getString("userId");
				 array=jObject.getJSONArray("transactions");
			    flag=true;
			}
			else{
				jobject.put("success", false);
				JSONObject failure=new JSONObject();
				failure.put("message", "Invalid or Empty parameters.");
				failure.put("code", "705");
				jobject.put("error", failure);
				System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
			}
			
			ipAddress=request.getRemoteAddr();
			
		    if(flag) 
		    {	 
			    SessionController sessionCtrl=new SessionController();
				SubscribeToFundController fundCtrl=new SubscribeToFundController();
				PortfolioCreateController ctrl=new PortfolioCreateController();

				Long consumerId=null;
				
			    consumerId=sessionCtrl.getClient(appId, ipAddress);
				 if(consumerId!=null )
				    {
				    	if(sessionCtrl.getValidSession(consumerId, sessionId))
				    	{
				    		 if(fundCtrl.validUser(consumerId, userId))
							{
				    			 for(int i=0;i<array.length();i++)
				    			 {
				    			    JSONObject obj=(JSONObject) array.get(i);
				    				if(obj.getString("folioNumber")!=null && !obj.getString("folioNumber").equals("") && obj.getString("amfiiCode")!=null && 
				    						!obj.getString("amfiiCode").equals("") && obj.getString("optionType")!=null && !obj.getString("optionType").equals("") && 
				    						obj.getString("units")!=null && !obj.getString("units").equals("") && !obj.getString("nav").equals("") && 
				    						obj.getString("nav")!=null && obj.getString("action")!=null && !obj.getString("action").equals("") && 
				    						!obj.getString("date").equals("") &&  obj.getString("date")!=null )
					    			{	
				    						String folioNumber=obj.getString("folioNumber");
					    			    	String amfiiCode=obj.getString("amfiiCode");
					    			    	int optionType=0;
											if(obj.getString("optionType").equals("GROWTH"))
					    			    		optionType=1;
											else if(obj.getString("optionType").equals("DIVIDEND PAYOUT"))
												optionType=2;
											else if(obj.getString("optionType").equals("DIVIDEND REINVEST"))
												optionType=3;
											else if(obj.getString("optionType").equals("DIVIDEND SWEEP"))
												optionType=4;
		
					    			    	Double units=Double.parseDouble(obj.getString("units"));
					    			    	Double nav=Double.parseDouble(obj.getString("nav"));
					    			    	String option=obj.getString("action");
					    			    	int action=0;
					    			    	if(option.equalsIgnoreCase("invest"))
					    			    		action=1;
					    			    	else if(option.equalsIgnoreCase("redeem"))
					    			    		action=2;
					    			    
					    			    	Calendar date=Calendar.getInstance();
					    			    	date.setTime(sdf.parse(obj.getString("date")));
				    			    	
					    			    	if(!nav.isNaN() && !units.isNaN() )
					    			    	{
						    			    	Long mfId=ctrl.getMFId(amfiiCode, optionType);
						    			    	Long userID=ctrl.getUserId(userId, consumerId);
						    			    		//System.out.print("MFID: "+mfId+",    amfiiCode: "+amfiiCode+",  :  , ");
						    			    	
						    			    	if(ctrl.isHoldingExist(folioNumber, mfId, userID))
						    			    	{
						    			    		ctrl.updateHolding(folioNumber, mfId, userID, action, units, nav, date.getTime());
			
													jobject.put("success", true);
													System.out.println("Holding Exist! Portfolio Create API invoked Successfully.");
						    			    	}
						    			    	else if(!ctrl.isHoldingExist(folioNumber, mfId, userID))
						    			    	{
						    			    		ctrl.saveHolding(folioNumber, userID, mfId, action, units, nav, date.getTime() );
			
													jobject.put("success", true);
													System.out.println("Holding does not Exist! New Holding Created and Portfolio Create API invoked Successfully.");
						    			    	}
					    			    	}else{
												jobject.put("success", false);
												JSONObject failure=new JSONObject();
												failure.put("message", "Invalid/Empty parameters.");
												failure.put("code", "705");
												jobject.put("error", failure);  
												System.out.println("Portfolio Create API not invoked Successfully.");
											}
				    			    	}else{
											jobject.put("success", false);
											JSONObject failure=new JSONObject();
											failure.put("message", "Invalid/Empty parameters.");
											failure.put("code", "705");
											jobject.put("error", failure);  
											System.out.println("Portfolio Create API not invoked Successfully.");
										}
				    			  } 
							}else{
								jobject.put("success", false);
								JSONObject failure=new JSONObject();
								failure.put("message", "User ID does not exist.");
								failure.put("code", "704");
								jobject.put("error", failure);  
								System.out.println("Portfolio Create API not invoked Successfully.");
							}
				    	}
				    	else
			    		{
				    		jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			    			System.out.println("Portfolio Create API Service has not invoked Successfully.   "+jobject.toString());
			    		}
				    }
				    else
		    		{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
		    			System.out.println("Portfolio Create API Service has not invoked Successfully.   "+jobject.toString());
		    		}		
		    }
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}