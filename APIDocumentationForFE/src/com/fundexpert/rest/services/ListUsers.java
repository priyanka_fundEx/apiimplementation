package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response; 

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.ListUsersController;
import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.SessionController; 
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.User;

@Path("/user/list")
public class ListUsers 
{
	@POST
	/*@Path("/")*/
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response apiDocREST(InputStream incomingData, @Context HttpServletRequest request) throws JSONException 
	{  
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null,userId=null;
		String ipAddress=null;
		boolean flag=false;
		try{

			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject=new JSONObject(builder.toString());
			System.out.println("Response from client: "+jObject);
			
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else
			{	System.out.println("fit here");
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId"); 
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("error","App Id does not exist.");
					jobject.put("error", failure);
				}
				if(con.isIpHardcoded())
				{	
					if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
					{	
						flag=false;
						jobject.put("success", false);
						
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("error","Invalid Request.");
						jobject.put("error", failure);
					}	
					else{
						flag=true;
					}
				}
				else if(!con.isIpHardcoded())
				{	
					flag=true;
				}
				
				if(flag)
				{	
				    ListUsersController uCtrl=new ListUsersController();
				    List<User> list;
					list=uCtrl.getList(appId, sessionId);		    	
			    	if(list.size()!=0)
			    	{
			    		JSONArray jsonArray=new JSONArray();
			    		for(User user:list)
			    		{
			    			JSONObject detailsJson = new JSONObject();
			    			detailsJson.put("userId", user.getUserId());
			    			detailsJson.put("riskAppetite", user.getRiskAppetite());
			    			jsonArray.put(detailsJson);
			    			System.out.println("user ID: "+detailsJson.toString());
			    		}
						jobject.put("success", true);
			    		jobject.put("user", jsonArray);
						System.out.println("Login Service invoked Successfully.");
			    	}
				}
			}
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+e.getMessage());
		}
		 
		
			
			/*ipAddress=request.getRemoteAddr();
		    if(flag)
		    {
			    SessionController ctrll=new SessionController();
			    ListUsersController ctrl=new ListUsersController();

				Long consumerId=null;
				List<User> list=new ArrayList<User>();
				
			    consumerId=ctrll.getClient(appId, ipAddress);
			    System.out.println("Consumer ID: "+consumerId);
			    if(consumerId!=null)
			    {
			    	if(ctrll.getValidSession(consumerId, sessionId))
			    	{
			    		list=ctrl.getList(appId, sessionId);		    	
				    	if(list.size()!=0)
				    	{
				    		for(User user:list)
				    		{
				    			JSONObject detailsJson = new JSONObject();
				    			detailsJson.put("userId", user.getUserId());
				    			detailsJson.put("riskAppetite", user.getRiskAppetite());
				    			jsonArray.put(detailsJson);
				    			System.out.println("user ID: "+detailsJson.toString());
				    		}
				    		jobject.put("user", jsonArray);
							jobject.put("success", true);
							System.out.println("Login Service invoked Successfully.");
				    	}
			    	}
			    	else{
			   		    jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			   			System.out.println("Lumpsum Redeem API Service has not invoked Successfully.   "+jobject.toString());
			   		}
				    }
				    else{
					    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
						System.out.println("Lumpsum Redeem API Service has not invoked Successfully.   "+jobject.toString());
					}
		    }			*/
		
		System.out.println("Data Received: " + jobject.toString(1));

		// return HTTP response 200 in case of success
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}