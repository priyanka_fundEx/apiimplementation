package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader; 

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.RegisterUserController;
import com.fundexpert.controller.SessionController;  
import com.fundexpert.dao.Consumer;

@Path("/user/create")
public class RegisterUser 
{
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response apiDocREST(InputStream incomingData, @Context HttpServletRequest request) 
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null,userId=null;
		Long uId=null;
		String ipAddress=null;
		boolean flag=false;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject=new JSONObject(builder.toString());
			System.out.println(jObject);
			 
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","Secret Key does not exist.");
				jobject.put("error", failure);
			}
			else
			{
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId"); 
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("error","App Id does not exist.");
					jobject.put("error", failure);
				}
				if(con.isIpHardcoded())
				{	flag=false;
					if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
					{	flag=false;
						jobject.put("success", false);
						
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("error","Invalid Request.");
						jobject.put("error", failure);
					}	
					else{
						flag=true;
					}
				}
				else if(!con.isIpHardcoded())
				{
					flag=true;
				}
				
				RegisterUserController userCtrl=new RegisterUserController();				
				SessionController sessionCtrl=new SessionController();
				if(flag)
				{
					if(sessionCtrl.getValidSession(con.getId(), sessionId))
			    	{
					    uId=userCtrl.saveUser(con.getId(), null, null, sessionId);
					    if(uId!=null)
					    {
					    	jobject.put("success", true);
					    	jobject.put("userId", userId);
							System.out.println("Register User Service invoked Successfully.");
					    }
			    	}
			    	else
			    	{
				    	jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Register User not invoked Successfully.");		    		
			    	}
				}
			}
			
		   /* if(flag) 
		    {	
				RegisterUserController ctrl=new RegisterUserController();
				SessionController sessionCtrl=new SessionController();
				
				String userId = null;
			    if(consumerId!=null)
			    {
			    	if(sessionCtrl.getValidSession(consumerId, sessionId))
			    	{
					    userId=ctrl.saveUser(consumerId, sessionId);
					    if(userId!=null)
					    {
					    	jobject.put("success", true);
					    	jobject.put("userId", userId);
							System.out.println("Register User Service invoked Successfully.");
					    }
			    	}
			    	else
			    	{
				    	jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Register User not invoked Successfully.");		    		
			    	}
			    }
			    else{
				    jobject.put("success", false);
					JSONObject failure=new JSONObject();
					failure.put("message", "App ID does not exist.");
					failure.put("code", "701");
					jobject.put("error", failure);
					System.out.println("Register User not invoked Successfully.");		    		
			    }
		    }*/
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		// return HTTP response 200 in case of success
		return Response.status(200).entity(jobject.toString()).build();
	}
}