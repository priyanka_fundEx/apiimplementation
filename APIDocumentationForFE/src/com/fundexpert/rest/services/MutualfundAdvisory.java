package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONObject;

import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.MutualfundAdvisoryController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;

@Path("/consumer/mutualfund/advisory")
public class MutualfundAdvisory
{
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String sessionId=null;
		String appId=null; 
		String amfiiCode = null;
		String ipAddress=null;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject=new JSONObject(builder.toString());
			
			boolean flag=false;
			if(jObject.has("appId") && jObject.has("sessionId")	&& jObject.has("userId") && jObject.has("amount"))
			{
				if(!jObject.getString("appId").equals("") && !jObject.getString("sessionId").equals("")	&& 
						!jObject.getString("userId").equals("") && !jObject.getString("amount").equals(""))
				{
					appId= jObject.getString("appId");		
					sessionId = jObject.getString("sessionId");   
				    amfiiCode=jObject.getString("amfiiCode"); 
				    flag=true;
				}
				else{
					jobject.put("success", false);
					JSONObject failure=new JSONObject();
					failure.put("message", "Invalid or Empty parameters.");
					failure.put("code", "705");
					jobject.put("error", failure);
					System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
				}
			}
			else{
				System.out.println("All Key parameters does not exist in input JSON Object.");
			}
			ipAddress=request.getRemoteAddr();
			
		    if(flag) 
		    {	 
			    SessionController sessionCtrl=new SessionController();
				SubscribeToFundController fundCtrl=new SubscribeToFundController();
				MutualfundAdvisoryController ctrl=new MutualfundAdvisoryController();
			    
				Long consumerId=null;
			    consumerId=sessionCtrl.getClient(appId, ipAddress);
				 if(consumerId!=null)
				    {
				    	if(sessionCtrl.getValidSession(consumerId, sessionId))
				    	{
				    		double rating=ctrl.getAdviceOfMF(amfiiCode);
				    		if(rating!=0)
				    		{
					    		jobject.put("success", true);			    		
					    		jobject.put("rating", rating);
					    		jobject.put("comments", "If you have Rs. 100, you can invest Rs. 100 in 3 months in equal installments.");
					    		 
				    			System.out.println("MutuanFund Advisory Service has not invoked Successfully.   "+jobject.toString());
				    		}
				    		else
				    		{
				    			jobject.put("success", false);
								JSONObject failure=new JSONObject();
								failure.put("message", "AMFI Code does not exist.");
								failure.put("code", "706");
								jobject.put("error", failure); 
				    			System.out.println("MutuanFund Advisory Service has not invoked Successfully.   "+jobject.toString());
				    		}
				    	}
				    	else
			    		{
			    		    jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			    			System.out.println("Update Risk Profile Service has not invoked Successfully.   "+jobject.toString());
			    		}
				    }
				    else
		    		{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
		    			System.out.println("Update Risk Profile Service has not invoked Successfully.   "+jobject.toString());
		    		} 
		    }
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}