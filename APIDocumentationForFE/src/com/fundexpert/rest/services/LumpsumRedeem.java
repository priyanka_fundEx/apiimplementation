package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.fundexpert.controller.GetAlertOnFundORPortfolioController;
import com.fundexpert.controller.GetClientPortfolioController;
import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.LumpsumInvestController;
import com.fundexpert.controller.LumpsumRedeemController;
import com.fundexpert.controller.PortfolioHealthScoreController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.Transaction;
import com.fundexpert.dao.User;
import com.sun.org.apache.bcel.internal.generic.SWAP;

@Path("/user/lumpsum/redeem")
public class LumpsumRedeem {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnSessionId(InputStream incomingData, @Context HttpServletRequest request)
	{	    
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String sessionId=null;
		String appId=null;
		String userId=null;
		Double redeemAmount = null;
		String ipAddress=null;
		
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject=new JSONObject(builder.toString()); 
			
			boolean flag=false;
			if(jObject.has("appId") && jObject.has("sessionId")	&& jObject.has("userId") && jObject.has("amount"))
			{
				if(!jObject.getString("appId").equals("") && !jObject.getString("sessionId").equals("")	&& 
						!jObject.getString("userId").equals("") && !jObject.getString("amount").equals(""))
				{
					appId= jObject.getString("appId");		
					sessionId = jObject.getString("sessionId");  
				    userId=jObject.getString("userId");
					redeemAmount = Double.parseDouble(jObject.getString("amount"));
				    flag=true;
				}
				else{
					jobject.put("success", false);
					JSONObject failure=new JSONObject();
					failure.put("message", "Invalid or Empty parameters.");
					failure.put("code", "705");
					jobject.put("error", failure);
					System.out.println("GetAlertOnFundORPortfolio Service not invoked Successfully.");
				}
			}
			else{
				System.out.println("All Key parameters does not exist in input JSON Object.");
			} 
			
			ipAddress=request.getRemoteAddr();
			
		    if(flag ) 
		    {	
			    SessionController sessionCtrl=new SessionController();
				SubscribeToFundController fundCtrl=new SubscribeToFundController();
				GetClientPortfolioController pfCtrl=new GetClientPortfolioController();
				GetAlertOnFundORPortfolioController alertCtrl=new GetAlertOnFundORPortfolioController();
				PortfolioHealthScoreController phsCtrl=new PortfolioHealthScoreController();
				LumpsumInvestController liCtrl =new LumpsumInvestController();
				LumpsumRedeemController ctrl=new LumpsumRedeemController();				

				Long consumerId=null;
				int riskAppetite=0;
				Long userID=null;		
				
		    	double totalPortfolio=0.0, newPortfolio=0.0, nontaxableEquityAmount=0.0, nontaxableDebtAmount=0.0;		

				List<Transaction> nonTaxableEquityTr = new ArrayList<Transaction>();
				List<Transaction> nonTaxableDebtTr = new ArrayList<Transaction>();
				List<Transaction> trEquityList=new ArrayList<Transaction>();
				List<Transaction> trDebtList=new ArrayList<Transaction>();		
				 	
				List<Holding> holdingEquityList=new ArrayList<Holding>();
				List<Holding> holdingDebtList=new ArrayList<Holding>();
				List<Holding> holdingBalancedList=new ArrayList<Holding>();
				List<Holding> holdingElssList=new ArrayList<Holding>();
				
				double equityAmt=0.0, debtAmt=0.0, balancedAmt=0.0, elssAmt=0.0;
				double portfolio=0.0; 
				
			    consumerId=sessionCtrl.getClient(appId, ipAddress);
				 if(consumerId!=null)
				    {
				    	if(sessionCtrl.getValidSession(consumerId, sessionId))
				    	{
				    		 if(fundCtrl.validUser(consumerId, userId))
							 {
				    			User user=liCtrl.getRiskAppetiteOfUser(userId);
				    			userID=user.getId();
				    				//System.out.println(userID+"userID: "+userId+", SID: "+sessionId+", APPID: "+appId);
				    			if(alertCtrl.isPortfolioSubscribed(userID))
				    			{
				    				System.out.println("PF subscribed.");
				    				System.out.println("SI: "+sessionId+", UI: "+userId+", AMount: "+redeemAmount );
				    				
									// Getting List of Holding(Equity + Debt type funds)
									List<Holding> holdingList = phsCtrl.getHoldingList(userID);
									System.out.println("User ID: "+userID+", use ID: "+userId);
									if(holdingList.size()!=0)
									{
										for(Holding h:holdingList)
					    				{				 
											if(ctrl.isEquityType(h.getMutualfundId()) || ctrl.isETFType(h.getMutualfundId()))
											{										
												
												//System.out.println(holdingList.size()+"  Holding is of Equity type."+h.getId());
												trEquityList=ctrl.getTransactionsOfHolding(h.getId());
												for(Transaction tr: trEquityList)
												{ 
													//currentEquity+=tr.getAmount();
													Calendar nowCal=Calendar.getInstance();
													nowCal.add(Calendar.YEAR, -1);
													if(tr.getTransactionDate().compareTo(nowCal.getTime())<=0)
													{	
														nonTaxableEquityTr.add(tr);
														nontaxableEquityAmount+=tr.getAmount();
													}
												}
											}
											else if(ctrl.isDebtType(h.getMutualfundId()) || ctrl.isLiquidType(h.getMutualfundId()))
											{											
												for(Transaction tr: trDebtList)
												{
													//currentDebt+=tr.getAmount();
													Calendar nowCal=Calendar.getInstance();
													nowCal.add(Calendar.YEAR, -3);
													if(tr.getTransactionDate().compareTo(nowCal.getTime())<=0)
													{	
														nonTaxableDebtTr.add(tr);
														nontaxableDebtAmount+=tr.getAmount();
													}
												}								
											}
					    				}
										System.out.println("NON Taxable Equity "+nontaxableEquityAmount+", NONTaxable Debt Amt: "+nontaxableDebtAmount);
					    			}
									else{
										System.out.println("Do not have ENOUGH Amount.");
									}
				    				
									if(holdingList.size()!=0)
									{
										for(Holding h:holdingList)
					    				{	 
											if(ctrl.isEquityType(h.getMutualfundId()) || ctrl.isETFType(h.getMutualfundId()))
											{
												holdingEquityList.add(h);
												equityAmt+=h.getAvgNav()*h.getUnits();
											}
											else if(ctrl.isDebtType(h.getMutualfundId()) || ctrl.isLiquidType(h.getMutualfundId()))
											{
												holdingDebtList.add(h);		
												debtAmt+=h.getAvgNav()*h.getUnits();																
											}
											else if(ctrl.isBalancedType(h.getMutualfundId()))
											{
												holdingBalancedList.add(h);		
												balancedAmt+=h.getAvgNav()*h.getUnits();																
											}
											else if(ctrl.isELSSType(h.getMutualfundId()))
											{
												holdingElssList.add(h);	
												elssAmt+=h.getAvgNav()*h.getUnits();																	
											}
					    				}
									}
									else{
										System.out.println("No holding Exist in your Portfolio.");
									}
									
									System.out.println("EquityAMT: "+equityAmt+", DEBTAMT: "+debtAmt+", BalancedAMT: "+balancedAmt+", ELSS AMT: "+elssAmt);
									
									// Calculate Total Portfolio Amount.
									portfolio=equityAmt+debtAmt+balancedAmt;
									newPortfolio=portfolio-redeemAmount;			// (Y=PF-X)
									
									//totalPortfolio=currentEquity+currentDebt;
									//newPortfolio=totalPortfolio-redeemAmount;		// (Y=PF-X)
					    			System.out.println("TPF: "+portfolio+",  NEWPF: "+newPortfolio+", equityAmt: "+equityAmt+", debtAmt: "+debtAmt+", redeemAmount: "+redeemAmount);
									
									// check weather User is having Enough Amount in his portfolio to redeem.
									if(portfolio>redeemAmount)
									{
										 riskAppetite=user.getRiskAppetite();
						    			 
						    			 Double idealEquity=0.0, idealDebt=0.0; 
						    			 
						    			 if(riskAppetite==0)
						    			 {
						    				 System.out.println(riskAppetite+" Risk Appetite is: "+"not given");
						    				 idealEquity=newPortfolio*50/100;
						    				 idealDebt=newPortfolio*50/100; 
						    			 }
						    			 else if(riskAppetite==1)
						    			 {
						    				 System.out.println(riskAppetite+" Risk Appetite is: "+" Conservative"); 
						    				 idealEquity=newPortfolio*10/100;
						    				 idealDebt=newPortfolio*90/100; 
						    			 }
						    			 else if(riskAppetite==2)
						    			 {
						    				 System.out.println(riskAppetite+" Risk Appetite is: "+" Moderately Conservative"); 
						    				 idealEquity=newPortfolio*30/100;
						    				 idealDebt=newPortfolio*70/100; 
						    			 }
						    			 else if(riskAppetite==3)
						    			 {
						    				 System.out.println(riskAppetite+" Risk Appetite is: "+" Moderate");  
						    				 idealEquity=newPortfolio*60/100;
						    				 idealDebt=newPortfolio*40/100; 
						    			 }
						    			 else if(riskAppetite==4)
						    			 {
						    				 System.out.println(riskAppetite+" Risk Appetite is: "+" Moderately Aggressive");  
						    				 idealEquity=newPortfolio*70/100;
						    				 idealDebt=newPortfolio*30/100; 
						    			 }
						    			 else if(riskAppetite==5)
						    			 {
						    				 System.out.println(riskAppetite+" Risk Appetite is: "+" Aggressive");  
						    				 idealEquity=newPortfolio*90/100;
						    				 idealDebt=newPortfolio*10/100; 
						    			 }
						    			 System.out.println("Ideal Equity: "+idealEquity+", Ideal Debt: "+idealDebt);
									 
						    			 // Double deductEquity=0.0, deductDebt = 0.0;
						    			 double y=0.0, x=redeemAmount;
						    			 double ae=0.0, ad=0.0;			//( AE= Amount From Equity) 
						    			 
						    			JSONArray array=new JSONArray();
						    			
						    			if(equityAmt>idealEquity)
						    			{
						    				y=equityAmt-idealEquity;
						    				if(y>x)
						    				{
						    					ae=x;
						    					equityAmt=equityAmt-x;
						    					x=0;		System.out.println("Equity y>x:   ae: "+ae+", x: "+x+", y: "+y);
						    				}
						    				else
						    				{
						    					ae=y;
						    					x=x-y;		System.out.println("Equity y<x:   ae: "+ae+", x: "+x+", y: "+y);
						    					equityAmt=equityAmt-y;
						    				}
						    			}
						    			if(debtAmt>idealDebt)
						    			{
						    				y=debtAmt-idealDebt;
						    				if(y>x)
						    				{
						    					ad=y;
						    					debtAmt=debtAmt-x;
						    					x=0;		System.out.println("Debt y>x:   ad: "+ae+", x: "+x+", y: "+y);
						    				}
						    				else
						    				{
						    					ad=y;
						    					x=x-y;		System.out.println("Debt y<x:   ad: "+ae+", x: "+x+", y: "+y);
						    					debtAmt=debtAmt-y;
						    				}
						    			}
	
						    			Double idealEquityOfX=0.0, idealDebtOfX=0.0; 
						    			if(x>0)
						    			{
						    				if(riskAppetite==0)
							    			 { 
							    				 idealEquityOfX=x*50/100;
							    				 idealDebtOfX=x*50/100; 
							    			 }
							    			 else if(riskAppetite==1)
							    			 {
							    				 idealEquityOfX=x*10/100;
							    				 idealDebtOfX=x*90/100; 
							    			 }
							    			 else if(riskAppetite==2)
							    			 {
							    				 idealEquityOfX=x*30/100;
							    				 idealDebtOfX=x*70/100; 
							    			 }
							    			 else if(riskAppetite==3)
							    			 {
							    				 idealEquityOfX=x*60/100;
							    				 idealDebtOfX=x*40/100; 
							    			 }
							    			 else if(riskAppetite==4)
							    			 {
							    				 idealEquityOfX=x*70/100;
							    				 idealDebtOfX=x*30/100; 
							    			 }
							    			 else if(riskAppetite==5)
							    			 {
							    				 idealEquityOfX=x*90/100;
							    				 idealDebtOfX=x*10/100; 
							    			 }
						    				System.out.println("Ideal Eq of X: "+idealEquityOfX+",  idealDebtOfX: "+idealDebtOfX);
						    			}
						    			
						    			if(idealEquityOfX < equityAmt)
						    			{
						    				equityAmt=equityAmt-idealEquityOfX;
						    				ae=ae+idealEquityOfX;
						    				x=x-idealEquityOfX;
						    			}
						    			if(idealDebtOfX < debtAmt)
						    			{
						    				debtAmt=debtAmt-idealDebtOfX;
						    				ad=ad+idealDebtOfX;
						    				x=x-idealDebtOfX;
						    			}
						    			
						    			if(x>0)
						    			{
						    				if(equityAmt>x)
						    				{
						    					equityAmt-=x;
						    					ae+=x;
						    					x=0;
						    				}
						    				if(debtAmt>x)
						    				{
						    					debtAmt-=x;
						    					ad+=x;
						    					x=0;
						    				}
						    			}
						    	        
						    			System.out.println("AE: "+ae+", AD: "+ad);
						    			
						    	     // Sort HoldingList of According to Rating of Mutual fund 
						    	        Collections.sort(holdingEquityList, new Comparator<Holding>()
						    	        {
											public int compare(Holding o1, Holding o2) { 
												Double i1=ctrl.getRating(o1.getMutualfundId());
												Double i2=ctrl.getRating(o2.getMutualfundId());
												return i1.compareTo(i2);
											}
						    	        } );
						    	        
						    	        Collections.sort(holdingDebtList, new Comparator<Holding>()
						    	        {
											public int compare(Holding o1, Holding o2) { 
												Double i1=ctrl.getRating(o1.getMutualfundId());
												Double i2=ctrl.getRating(o2.getMutualfundId());
												return i1.compareTo(i2);
											}
						    	        } );
						    	       
					    	        	System.out.println("Inside Equity");
					    	        	if(ae>0)
					    	        	{   
					    	        		for(int i=0;i<holdingEquityList.size();i++)
							    	        {
							    	        	Holding h=holdingEquityList.get(i);
						    	        		MutualFund mf=pfCtrl.getMutualFund(h.getMutualfundId());
							    	        	
							    	        		List<Transaction> nonTaxableTrList=ctrl.getNonTaxableTransactionsOfHolding(h.getId());
							    	        		
									    	        Iterator<Transaction> nonTaxableIt=nonTaxableTrList.iterator();
								    	        	if(nonTaxableTrList.size()!=0 && ae>0)
								    	        	{
									    	        		double amt=0.0;
									    	        		while(nonTaxableIt.hasNext() && ae>0)
									    	        		{
										    	        		Transaction tx=nonTaxableIt.next();
								    	        				//System.out.println(tx.getHoldingId()+"EQUITY Non Taxable Amount: "+tx.getAmount());
								    	        				if(ae<=tx.getAmount())
										    	        		{	
								    	        					amt+=ae;
								    	        					ae=0;
								    	        					//System.out.println("ae<=tx.getAmount() : "+ae);
										    	        		}
								    	        				else if(ae>tx.getAmount())
										    	        		{	
								    	        					ae-=tx.getAmount();
								    	        					amt+=tx.getAmount();
								    	        					//System.out.println("ae>tx.getAmount() : "+ae);
										    	        		}
								    	        				//System.out.println("AE: "+ae+",  AMT: "+amt); 
										    	        	}
									    	        		if(amt!=0)
									    	        		{
										    	        		JSONObject obj=new JSONObject();							    	        		
												    			obj.put("amfiiCode", mf.getAmfiiCode());
												    			obj.put("amount", amt);
												    			obj.put("date", "");
												    			array.put(obj);
												    			System.out.println("non taxable Equity TR: "+obj);
									    	        		}
									    	        		//continue;
								    	        	}
							    	        }
					    	        		//continue;							    	        	
						    	        } 	
					    	        	System.out.println("Equity NON Taxable finished, moving to Taxable Amount; ");
					    	        	if(ae>0)
					    	        	{   
					    	        		for(int i=0;i<holdingEquityList.size();i++)
							    	        {
							    	        	Holding h=holdingEquityList.get(i);
						    	        		MutualFund mf=pfCtrl.getMutualFund(h.getMutualfundId());
								    	        	List<Transaction> taxableTrList=ctrl.getTaxableTransactionsOfHolding(h.getId());
								    	        	Iterator<Transaction> taxableIt=taxableTrList.iterator();
								    	        	if(taxableTrList.size()!=0 && ae>0)
								    	        	{
									    	        	double amt=0.0;
								    	        		while(taxableIt.hasNext() && ae>0)
								    	        		{
									    	        		Transaction tx=taxableIt.next();
							    	        				//System.out.println(tx.getHoldingId()+"EQUITY Taxable Amount: "+tx.getAmount());
							    	        				if(ae<=tx.getAmount())
									    	        		{	
							    	        					amt+=ae;
							    	        					ae=0;
							    	        					//System.out.println("Taxable: ae<=tx.getAmount() : "+ae);
									    	        		}
							    	        				else if(ae>tx.getAmount())
									    	            	{	
							    	        					ae-=tx.getAmount();
							    	        					amt+=tx.getAmount();
							    	        					//System.out.println("Taxable : ae>tx.getAmount() : "+ae);
									    	        		}
							    	        				
							    	        				//System.out.println("AE: "+ae+",  AMT: "+amt); 
									    	        	}
								    	        		if(amt!=0)
								    	        		{
									    	        		JSONObject obj=new JSONObject();							    	        		
											    			obj.put("amfiiCode", mf.getAmfiiCode());
											    			obj.put("amount", amt);
											    			obj.put("date", "");
											    			array.put(obj);
											    			System.out.println("taxable Equity TR: "+obj);
								    	        		}
								    	        		//continue;
								    	        	}
						    	        	}
					    	        		//continue;
						    	        }
					    	        	
					    	        	System.out.println("Equity Finished: "+ae+"  and Entering into DEBT   "+ad);
					    	        	if(ad>0)
					    	        	{
							    	        for(int i=0;i<holdingDebtList.size();i++)
							    	        { 
							    	        	Holding h=holdingDebtList.get(i);
						    	        		MutualFund mf=pfCtrl.getMutualFund(h.getMutualfundId());
							    	        	if(ctrl.isDebtType(h.getMutualfundId()))
												{ 
								    	        	List<Transaction> nonTaxableTrList=ctrl.getNonTaxableTransactionsOfHolding(h.getId());
								    	        	Iterator<Transaction> nonTaxableIt=nonTaxableTrList.iterator();
								    	        	if(nonTaxableTrList.size()!=0)
								    	        	{
									    	        	double amt=0.0;
									    	        		while(nonTaxableIt.hasNext() && ad>0)
									    	        		{
										    	        		Transaction tx=nonTaxableIt.next();
								    	        				//System.out.println(tx.getHoldingId()+"DEBT Non Taxable Amount: "+tx.getAmount());
								    	        				if(ad<=tx.getAmount())
										    	        		{	
								    	        					amt+=ad;
								    	        					ad=0;
								    	        					//System.out.println("ad<=tx.getAmount() : "+ad);
										    	        		}
								    	        				else if(ad>tx.getAmount())
										    	        		{	
								    	        					ad-=tx.getAmount();
								    	        					amt+=tx.getAmount();
								    	        					//System.out.println("ad>tx.getAmount() : "+ad);
										    	        		}
								    	        				//System.out.println("AD: "+ad+",  AMT: "+amt); 
										    	        	}
									    	        		if(amt!=0)
									    	        		{
										    	        		JSONObject obj=new JSONObject();							    	        		
												    			obj.put("amfiiCode", mf.getAmfiiCode());
												    			obj.put("amount", amt);
												    			obj.put("date", "");
												    			array.put(obj);
												    			System.out.println("non taxable Debt TR: "+obj);
									    	        		}
								    	        	}
												}
							    	        }System.out.println("Debt NON Taxable finished, moving to Taxable Amount; ");
							    	        if(ad>0)
						    	        	{
								    	        for(int i=0;i<holdingDebtList.size();i++)
								    	        { 
								    	        	Holding h=holdingDebtList.get(i);
							    	        		MutualFund mf=pfCtrl.getMutualFund(h.getMutualfundId());
								    	        	List<Transaction> taxableTrList=ctrl.getTaxableTransactionsOfHolding(h.getId());
								    	        	Iterator<Transaction> taxableIt=taxableTrList.iterator();
								    	        	if(taxableTrList.size()!=0)
								    	        	{
									    	        	double amt=0.0;
								    	        		while(taxableIt.hasNext() && ad>0)    
								    	        		{
									    	        		Transaction tx=taxableIt.next();
							    	        				//System.out.println(tx.getHoldingId()+"DEBT Taxable Amount: "+tx.getAmount()+",  ad: "+ad);
							    	        				if(ad<=tx.getAmount())
									    	        		{	
							    	        					amt+=ad;
							    	        					ad=0;
							    	        					//System.out.println("ad<=tx.getAmount() : "+ad);
									    	        		}
							    	        				else if(ad>tx.getAmount())
									    	        		{	
							    	        					ad-=tx.getAmount();
							    	        					amt+=tx.getAmount();
							    	        					//System.out.println("ad>tx.getAmount() : "+ad);
									    	        		}
							    	        				//System.out.println("AD: "+ad+",  AMT: "+amt); 
									    	        	}
								    	        		if(amt!=0)
								    	        		{
								    	        			JSONObject obj=new JSONObject();							    	        		
											    			obj.put("amfiiCode", mf.getAmfiiCode());
											    			obj.put("amount", amt);
											    			obj.put("date", "");
											    			array.put(obj);
											    			System.out.println("taxable Debt TR: "+obj);
								    	        		}
								    	        	}
												}  
						    	        	}
						    	        }                    
	
						    	        System.out.println("Requested Amount hasbeen deducted! "+"AE: "+ae+", AD: "+ad);
						    	        
						    			jobject.put("success", true);
						    			jobject.put("mutualfunds", array);		
									}
									else{
										jobject.put("success", false);
										JSONObject failure=new JSONObject();
										failure.put("message", "User does not have enough holding to take out given amount. ");
										failure.put("code", "761");
										jobject.put("error", failure);  
										System.out.println("Lumpsum Redeem API not invoked Successfully.");
					    			}
									System.out.println("Lumpsum Redeem API invoked Successfully.");
				    			}else{
									jobject.put("success", false);
									JSONObject failure=new JSONObject();
									failure.put("message", "User portfolio not uploaded.");
									failure.put("code", "707");
									jobject.put("error", failure);  
									System.out.println("Lumpsum Redeem API not invoked Successfully.");
				    			}
							}
				    		 else{
								jobject.put("success", false);
								JSONObject failure=new JSONObject();
								failure.put("message", "User ID does not exist.");
								failure.put("code", "704");
								jobject.put("error", failure);  
								System.out.println("Lumpsum Redeem API not invoked Successfully.");
							}			    		
				    	}
				    	else{
			    		    jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			    			System.out.println("Lumpsum Redeem API Service has not invoked Successfully.   "+jobject.toString());
			    		}
				    }
				    else{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
		    			System.out.println("Lumpsum Redeem API Service has not invoked Successfully.   "+jobject.toString());
		    		} 			
		    }
		} catch (Exception e) {
			System.out.println("Error Parsing: - "+e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Data Received: " + jobject.toString());
		return Response.status(200).entity(jobject.toString()).build();
	} 
	
 
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "APIDocumentationForFE Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
}