package com.fundexpert.rest.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.ValidationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fundexpert.controller.ListUsersController;
import com.fundexpert.controller.LoginController;
import com.fundexpert.controller.MutualFundController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.User;
import com.apidoc.util.JSON;

@Path("/mf")
public class MutualFundService {

	@Path("getUpdatedMf")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUpdatedMf(InputStream incomingData, @Context HttpServletRequest request)
	{     
		System.out.println("Updated MF called.");
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null,userId=null;
		String ipAddress=null;
		boolean flag=false;
		try
		{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject=new JSONObject(builder.toString());
			
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","Session ID does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("mf") || jObject.getJSONArray("mf").length()==0)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","No/Empty Array.");
				jobject.put("error", failure);
			}
			else
			{	
				//System.out.println("fit here");
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId"); 
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				ipAddress=request.getRemoteAddr();
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("error","App Id does not exist.");
					jobject.put("error", failure);
				}
				if(con.isIpHardcoded())
				{	
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(!flag)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("error","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				else if(!con.isIpHardcoded())
				{	
					flag=true;
				}
				
				if(flag)
				{
					JSONArray mfArray=jObject.getJSONArray("mf");
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidApiSyncSession(con.getId(), sessionId))
			    	{
						MutualFundController mfController=new MutualFundController();
						System.out.println("received array size="+mfArray.length());
						List<MutualFund> list=mfController.getRecentlyUpdatedMutualFunds(mfArray);
						if(list==null || list.size()<1)
						{
							jobject.put("success",true);
							/*JSONObject failure=new JSONObject();
							failure.put("message", "Already Updated.");
							jobject.put("error",failure);*/
						}
						else
						{
							jobject.put("success",true);
							jobject.put("mf",JSON.getMutualFundJsonForApi(list));
						}
						System.out.println("Done sending newly updated MutualFunds.");
			    	}
			    	else
			    	{
				    	jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Session Expired.");		    		
			    	}
				}
			}
		}
		catch(JSONException jsonE)
		{
			jsonE.printStackTrace();
			JSONObject failure=new JSONObject();
			jobject.put("success", false);
			failure.put("message", jsonE.getMessage());
			failure.put("code", "703");
			jobject.put("error", failure);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			jobject.put("success", false);
			failure.put("message", e.getMessage());
			failure.put("code", "703");
			jobject.put("error", failure);
			
		}
		return Response.status(200).entity(jobject.toString()).build();
	}
	
	@Path("/apiMf")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNewlyAddedFunds(InputStream incomingData, @Context HttpServletRequest request)
	{
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null,userId=null;
		String ipAddress=null;
		boolean flag=false;
		try
		{
			System.out.println("Entered apiMf.");
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			
			JSONObject jObject=new JSONObject(builder.toString());
			System.out.println("schemeCde="+jObject.length());
			Iterator<String> ir=jObject.keys();
			while(ir.hasNext())
			{
				System.out.println(ir.next());
			}
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				System.out.println("Appi id is null = "+jObject.getString("appId"));
				JSONObject failure=new JSONObject();
				failure.put("success", false);
				failure.put("code", "701");
				failure.put("error","App Id empty.");
				return Response.status(400).entity(failure.toString()).build();
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				
				JSONObject failure=new JSONObject();
				failure.put("success", false);
				failure.put("code", "701");
				failure.put("error","Session ID does not exist.");
				return Response.status(400).entity(failure.toString()).build();
			}
			else if(!jObject.has("schemeCodes") || jObject.getJSONArray("schemeCodes").length()<1)
			{
				System.out.println("error SchemeCodes not passed.");
				
				JSONObject failure=new JSONObject();
				failure.put("success", false);
				failure.put("code", "701");
				failure.put("error","SchemeCodes not specified.");
				return Response.status(400).entity(failure.toString()).build();
			}
			else
			{	
				System.out.println("Getting appId,sessionId and schemeCodes array.");
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId"); 
				
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				ipAddress=request.getRemoteAddr();
				System.out.println("IP Address : "+ipAddress);
				if(con==null)
				{
					jobject.put("success", false);
					
					jobject.put("code", "701");
					jobject.put("error","App Id does not exist.");
					return Response.status(400).entity(jobject.toString()).build();
				}
				if(con.isIpHardcoded())
				{	
					//System.out.println(con.getIpAddress()+"  ipAddress="+ipAddress);
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								if(str.equals(con.getIpAddress()))
								{
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(!flag)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("error","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				else if(!con.isIpHardcoded())
				{	
					flag=true;
				}
				
				if(flag)
				{
					List<Object> listOfSchemeCodes=jObject.getJSONArray("schemeCodes").toList();
					System.out.println("Received SchemeCode List Size = "+listOfSchemeCodes.size());
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidApiSyncSession(con.getId(), sessionId))
			    	{
						MutualFundController mfController=new MutualFundController();
						List<MutualFund> list=mfController.getListOfMutualFunds(listOfSchemeCodes);
						jobject.put("success",true);
						if(list!=null && list.size()>0)
						{
							jobject.put("mf",JSON.getMutualFundJsonForApi(list));
						}
						System.out.println("Done sending new MutualFunds.");
			    	}
			    	else
			    	{
				    	jobject.put("success", false);
				    	jobject.put("message", "Session expired.");
				    	jobject.put("code", "703");
						System.out.println("Register User not invoked Successfully.");		    		
			    	}
				}
			}
		}
		catch(JSONException jsonE)
		{
			jsonE.printStackTrace();
			jobject.put("success", false);
			jobject.put("error", "Not a proper JSON.");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			jobject.put("success", false);
			jobject.put("error", "Undefined error.");
		}
		return Response.status(200).entity(jobject.toString()).build();
	}

	@Path("/mapId")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSchemeCodeForMappingId(InputStream incomingData, @Context HttpServletRequest request)
	{     
		JSONObject jobject=new JSONObject();
		StringBuilder builder=new StringBuilder();
		String appId=null;
		String sessionId = null,userId=null;
		String ipAddress=null;
		boolean flag=false;
		try
		{
			BufferedReader br=new BufferedReader(new InputStreamReader(incomingData));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject=new JSONObject(builder.toString());
			System.out.println("JSONObject="+jObject.toString());
			
			if(!jObject.has("appId") || jObject.get("appId").equals("") || jObject.get("appId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","App Id does not exist.");
				jobject.put("error", failure);
			}
			else if(!jObject.has("sessionId") || jObject.get("sessionId").equals("") || jObject.get("sessionId")==null)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","Session ID does not exist.");
				jobject.put("error", failure);
			}
			/*else if(!jObject.has("mf") || jObject.getJSONArray("mf").length()==0)
			{
				jobject.put("success", false);
				
				JSONObject failure=new JSONObject();
				failure.put("code", "701");
				failure.put("error","No/Empty Array.");
				jobject.put("error", failure);
			}*/
			else
			{	
				//System.out.println("fit here");
				appId= jObject.getString("appId");		
				sessionId = jObject.getString("sessionId"); 
				
				LoginController ctrl=new LoginController();
				Consumer con=ctrl.getConsumer(appId);
				
				ipAddress=request.getRemoteAddr();
				
				if(con==null)
				{
					jobject.put("success", false);
					
					JSONObject failure=new JSONObject();
					failure.put("code", "701");
					failure.put("error","App Id does not exist.");
					jobject.put("error", failure);
				}
				if(con.isIpHardcoded())
				{	
					//System.out.println(con.getIpAddress()+"  ipAddress="+ipAddress);
					if(!ipAddress.equals("127.0.0.1"))
					{
						if(!con.getIpAddress().equalsIgnoreCase(ipAddress))
						{	
							flag=false;
						}	
						else
						{
							flag=true;
						}
					}
					else
					{
						String ip;
						if ((ip = request.getHeader("x-forwarded-for")) != null) 
						{
							System.out.println("Main Ip="+ip);
							String[] ipArray=ip.split(",");
							for(String str:ipArray)
							{
								System.out.println("IP's : "+str+" hardCodedIp : "+con.getIpAddress());
								if(str.equals(con.getIpAddress()))
								{
									System.out.println("Ip Matched.");
									flag=true;
									break;
								}
								flag=false;
							}
					    }
					}
					if(!flag)
					{
						jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("code", "701");
						failure.put("error","Invalid Request.");
						jobject.put("error", failure);
					}
				}
				else if(!con.isIpHardcoded())
				{	
					flag=true;
				}
				
				if(flag)
				{
					SessionController sessionCtrl=new SessionController();
					if(sessionCtrl.getValidApiSyncSession(con.getId(), sessionId))
			    	{
						MutualFundController mfController=new MutualFundController();
						
						JSONArray jsonArray=mfController.getMutualFundMappings();
						System.out.println(jsonArray.length());
						if(jsonArray==null || jsonArray.length()<1)
						{
							jobject.put("success",false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Something Failed at api server.");
							jobject.put("error",failure);
						}
						else
						{
							jobject.put("success",true);
							jobject.put("mf",jsonArray);
						}
						System.out.println("Done sending mapping of schemeCode with schemeCode of liquidFundId,regularFundId,growthFundId.");
			    	}
			    	else
			    	{
				    	jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "Session expired.");
						failure.put("code", "703");
						jobject.put("error", failure);
						System.out.println("Register User not invoked Successfully.");		    		
			    	}
				}
			}
		}
		catch(JSONException jsonE)
		{
			jsonE.printStackTrace();
			JSONObject failure=new JSONObject();
			jobject.put("success", false);
			failure.put("message", jsonE.getMessage());
			failure.put("code", "703");
			jobject.put("error", failure);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JSONObject failure=new JSONObject();
			jobject.put("success", false);
			failure.put("message", e.getMessage());
			failure.put("code", "703");
			jobject.put("error", failure);
			
		}
		return Response.status(200).entity(jobject.toString()).build();
	}
}
