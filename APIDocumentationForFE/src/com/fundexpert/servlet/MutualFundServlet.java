package com.fundexpert.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.apidoc.util.JSON;
import com.fundexpert.controller.MutualFundController;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;

/**
 * Servlet implementation class MutualFundServlet
 */
@WebServlet("/mf")
public class MutualFundServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MutualFundServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JSONObject resp=new JSONObject();
		try
		{
			if(request.getParameterMap().isEmpty())
			{
				throw new FundexpertException("No Parameters.");
			}
			if(request.getParameterMap().containsKey("action"))
			{
				String action=request.getParameter("action");
				if(action.equals("getFundByHouseId"))
				{
					long houseId=Long.valueOf(request.getParameter("houseId"));
					String capitalType=request.getParameter("capitalType");
					MutualFundController mfc=new MutualFundController();
					List<MutualFund> list=mfc.getByFundHouse(houseId,capitalType);
					resp=resp.put("mfData",JSON.getMutualFundJsonForApi(list));
					resp.put("success", true);
				}
				else
				{
					throw new FundexpertException("Not a valid action.");
				}
			}
			else
			{
				throw new FundexpertException("Action not passed.");
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			resp.put("success", false);
			resp.put("error", fe.getMessage());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("success", false);
			resp.put("error", "Error Occurred.");
		}
		finally
		{
			response.getWriter().write(resp.toString());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		JSONObject resp=new JSONObject();
		User user=null;
		try
		{
			if(request.getParameterMap().isEmpty())
			{
				throw new FundexpertException("No Parameters.");
			}
			if(request.getParameterMap().containsKey("action"))
			{
				user=(User)request.getSession().getAttribute("user");
				String loggedIn=(String)request.getSession().getAttribute("login");
				if(loggedIn==null || loggedIn.equals("false") || user==null)
					throw new FundexpertException("You are not logged in.");
			
				if(request.getParameter("action").equals("saveBlackList"))
				{
					 response.setContentType("application/Json");
					 String[] fundidarray=request.getParameterValues("fundidarray[]");
					 System.out.println("save black list mutualfund.");
					
					 MutualFundController mfc=new MutualFundController();
					 boolean success=mfc.saveBlackList(fundidarray,user.getConsumerId());
			    	 resp.put("success", success);
			    	 if(!success)
			    		 resp.put("error", "Not able to persist Funds.");
				}
				else if(request.getParameter("action").equals("deleteSingleBlackList"))
				{
					String mfId=request.getParameter("mfid");
					if(mfId==null)
						throw new FundexpertException("Parameter mfid is missing.");
					System.out.println("delete single black list with mfid="+mfId);
					MutualFundController mfc=new MutualFundController();
					boolean success=mfc.deleteSingleFundFromBlackList(Long.valueOf(mfId),user.getConsumerId());
					resp.put("success", success);
			    	 if(!success)
			    		 resp.put("error", "Not able to Delete Fund.");
				}
				else if(request.getParameter("action").equals("deleteAllBlackList"))
				{
					System.out.println("Delete all blacklist.");
					MutualFundController mfc=new MutualFundController();
					boolean success=mfc.deleteAllFundFromBlackList(user.getConsumerId());
					resp.put("success", success);
			    	 if(!success)
			    		 resp.put("error", "Not able to Delete Fund.");
				}
				else if(request.getParameter("action").equals("deleteBlackListByFundHouse"))
				{
					String fundHouseId=request.getParameter("fhdelete");
					System.out.println("delete black list by FundhouseId="+fundHouseId);
					if(fundHouseId==null)
						throw new FundexpertException("Parameter missing.");
					MutualFundController mfc=new MutualFundController();
					boolean success=mfc.deleteFromBlackListByFundHouse(Long.valueOf(fundHouseId),user.getConsumerId());
					resp.put("success", success);
			    	 if(!success)
			    		 resp.put("error", "Not able to Delete Fund.");
				}
				else if(request.getParameter("action").equals("saveReco"))
				{
					 response.setContentType("application/Json");
					 String[] fundidarray=request.getParameterValues("fundidarray[]");
					 System.out.println("save Reco mutualfund.");
					
					 MutualFundController mfc=new MutualFundController();
					 boolean success=mfc.saveReco(fundidarray, user.getConsumerId());
			    	 resp.put("success", success);
			    	 if(!success)
			    		 resp.put("error", "Not able to persist Funds.");
				}
				else if(request.getParameter("action").equals("deleteSingleReco"))
				{
					String mfId=request.getParameter("mfid");
					if(mfId==null)
						throw new FundexpertException("Parameter mfid is missing.");
					System.out.println("Deleting single Reco with mfid="+mfId);
					MutualFundController mfc=new MutualFundController();
					boolean success=mfc.deleteSingleFundFromReco(Long.valueOf(mfId),user.getConsumerId());
					resp.put("success", success);
			    	 if(!success)
			    		 resp.put("error", "Not able to Delete Fund.");
				}
				else if(request.getParameter("action").equals("deleteAllReco"))
				{
					System.out.println("Deleting all Reco.");
					MutualFundController mfc=new MutualFundController();
					boolean success=mfc.deleteAllFundFromReco(user.getConsumerId());
					resp.put("success", success);
			    	 if(!success)
			    		 resp.put("error", "Not able to Delete Fund.");
				}
				else if(request.getParameter("action").equals("deleteRecoByFundHouse"))
				{
					String fundHouseId=request.getParameter("fhdelete");
					System.out.println("Deleting Reco by FundhouseId="+fundHouseId);
					if(fundHouseId==null)
						throw new FundexpertException("Parameter missing.");
					MutualFundController mfc=new MutualFundController();
					boolean success=mfc.deleteFromRecoByFundHouse(Long.valueOf(fundHouseId),user.getConsumerId());
					resp.put("success", success);
			    	 if(!success)
			    		 resp.put("error", "Not able to Delete Fund.");
				}
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			resp.put("success", false);
			resp.put("error", fe.getMessage());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("success", false);
			resp.put("error", "Internal Error.");
		}
		finally
		{
			response.getWriter().write(resp.toString());
		}
	}

}
