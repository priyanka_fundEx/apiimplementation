package com.fundexpert.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.json.JSONException;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.StocksHoldings;
import com.fundexpert.dao.User;



public class AddStocksInPortfolio extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject json_response= new JSONObject();
		response.setContentType("application/Json");
		Session session2=null;
	    Transaction transaction=null;
		try 
		{
			/*Enumeration<String> s1=request.getParameterNames();
			while(s1.hasMoreElements())
			{
				System.out.println("elements"+s1.nextElement());
			}*/
			session2=HibernateBridge.getSessionFactory().openSession();
			transaction=session2.beginTransaction();
			long userId=0;
			int  scCode=0;
			double noOfShares=0;
			String type=null;
			String[] array1=request.getParameterValues("array1[]");
			System.out.println("lengtharray"+array1.length);
			for(int i=0;i<array1.length;i++) {
			if(i==0) {
			userId=Long.parseLong(request.getParameter("userId"));	
			scCode=Integer.parseInt(request.getParameter("scCode"));
		    noOfShares=Double.parseDouble(request.getParameter("noOfShares"));
		    type=request.getParameter("type");
		    System.out.println("i==0"+userId);
			}else if(i==1)
			{
				
				userId=Long.parseLong(request.getParameter("userId1"));	
				scCode=Integer.parseInt(request.getParameter("scCode1"));
			    noOfShares=Double.parseDouble(request.getParameter("noOfShares1"));
			    type=request.getParameter("type1");
			    System.out.println("i==1"+userId);
			    
			}else if(i==2)
			{
				userId=Long.parseLong(request.getParameter("userId2"));	
				scCode=Integer.parseInt(request.getParameter("scCode2"));
			    noOfShares=Double.parseDouble(request.getParameter("noOfShares2"));	
			    type=request.getParameter("type2");
			    System.out.println("i==2"+userId);
			}else if(i==3)
			{
				userId=Long.parseLong(request.getParameter("userId3"));	
				scCode=Integer.parseInt(request.getParameter("scCode3"));
			    noOfShares=Double.parseDouble(request.getParameter("noOfShares3"));	
			    type=request.getParameter("type3");
			    System.out.println("i==3"+userId);
			}else if(i==4) {
				userId=Long.parseLong(request.getParameter("userId4"));	
				scCode=Integer.parseInt(request.getParameter("scCode4"));
			    noOfShares=Double.parseDouble(request.getParameter("noOfShares4"));
			    type=request.getParameter("type4");
			    System.out.println("i==4"+userId);
			}else {
				
			}
			
			StocksHoldings sh=(StocksHoldings)session2.createQuery("from StocksHoldings  where userId=? and scCode=?").setLong(0,userId).setInteger(1,scCode).setMaxResults(1).uniqueResult();
			if(sh==null) 
			{
				StocksHoldings sh1=new StocksHoldings();
				sh1.setScCode(scCode);
				sh1.setNumberOfShares(noOfShares);
				User us=(User)session2.createQuery("from User where id=?").setLong(0,userId).setMaxResults(1).uniqueResult();
				if(us!=null) {
				sh1.setUserId(us.getId());
				}
				sh1.setId(1);
				sh1.setBuyDate(new Date() );
				sh1.setBuyPrice(1);
				sh1.setDematId("dematId");
				sh1.setDematName("DematName");
				sh1.setLastUpdatedOn(new Date());
				Stocks sc=(Stocks)session2.createQuery("from Stocks where scCode=?").setLong(0, scCode).setMaxResults(1).uniqueResult();
				sh1.setStocks(sc);
				session2.save(sh1);
			}
			else 
			{
				
				if(type.equals("purchased"))
				{
					double AddedShares=sh.getNumberOfShares()+noOfShares;
					sh.setNumberOfShares(AddedShares);
				}
				else
				{   System.out.println("else");
				    System.out.println("before subtractedShares"+sh.getNumberOfShares());
				    if(sh.getNumberOfShares()>=noOfShares)
				    {
				    	
				    	double subtractedShares=sh.getNumberOfShares()-noOfShares;
				    	System.out.println("after subtraction"+subtractedShares);
				    	sh.setNumberOfShares(subtractedShares);	
					}else 
					{
						sh.setNumberOfShares(0);		
					}
					
					
					
				}
			}
			
		System.out.println("hello");
			
		
			}
			transaction.commit();
			
		}
		catch(Exception e){
			 if( transaction!=null)
				  transaction.rollback();
			   // throw new IOException("Error parsing JSON request string");
			    e.printStackTrace();
			    try {
					json_response.put("success", false);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			  }		  	
			
		
		finally {
			session2.close();
		}
		 response.getWriter().write(json_response.toString());
	}

}
