package com.fundexpert.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.config.Config;
import com.fundexpert.controller.PortfolioRequestController;
import com.fundexpert.dao.User;
import com.fundexpert.util.PdfReaderAndPersist;

/**
 * Servlet implementation class PortfolioUpload
 */
@WebServlet("/PortfolioUpload")
public class PortfolioUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject json_response= new JSONObject();
		response.setContentType("application/Json");
		Session session2=null;
	    Transaction transaction=null;
		try {
			session2=HibernateBridge.getSessionFactory().openSession();
			transaction=session2.beginTransaction();
			Config config=new Config();
			String path=config.getProperty(Config.PDF_NOT_READ);
			long userId=Long.parseLong(request.getParameter("userId"));	
			String filename=(request.getParameter("filename"));
		    String password=(request.getParameter("password"));
			System.out.println("userid"+userId);
			System.out.println("filename"+filename);
			System.out.println("password"+password);
			User us=(User)session2.createQuery("from User where id=?").setLong(0,userId).setMaxResults(1).uniqueResult();
			if(us!=null) {
				PortfolioRequestController portfolioRequestController =new PortfolioRequestController();
				portfolioRequestController.persistRequestWithPasswordChange(userId,password);
				String emailId =us.getEmail();
				String pan=us.getPan();
				long consumerId=us.getConsumerId();
				String fullPath=path+"/"+filename+ ".pdf";
				System.out.println("fullpath is"+fullPath);
				PdfReaderAndPersist pdfReaderAndPersist = new PdfReaderAndPersist();
				
				pdfReaderAndPersist.savepdf(fullPath, emailId, pan, consumerId);
				
				
				
			}else
			{
				throw new IOException("user does not exists");
			}
			transaction.commit();
		
		}
		catch(Exception e){
			e.printStackTrace();	
			json_response.put("success", false);
		}
		finally {
		
		}
		 response.getWriter().write(json_response.toString());
}
}
