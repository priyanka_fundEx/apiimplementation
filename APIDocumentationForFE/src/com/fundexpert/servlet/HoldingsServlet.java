package com.fundexpert.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fundexpert.controller.CunsumerController;
import com.fundexpert.controller.HoldingController;
import com.fundexpert.controller.PortfolioImportController;
import com.fundexpert.controller.RegisterUserController;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.pojo.Holdings;

/**
 * Servlet implementation class Holdings
 */
public class HoldingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HoldingsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JSONObject resp=new JSONObject();
		PrintWriter out=response.getWriter();
		try
		{
			if(!request.getParameterMap().containsKey("appId"))
			{
				throw new FundexpertException("Empty parameter.");
			}
			else
			{
				String appId=request.getParameter("appId");
				CunsumerController cc=new CunsumerController();
				Consumer c=cc.getConsumer(appId);
				if(c!=null)
				{
					if(request.getParameterMap().containsKey("action"))
					{
						if(request.getParameter("action").equals("deleteHolding"))
						{
							if(!request.getParameterMap().containsKey("userId"))
							{
								throw new FundexpertException("Empty parameter.");
							}
							else
							{
								long userId=Long.valueOf(request.getParameter("userId"));
								RegisterUserController ruc=new RegisterUserController();
								User user=ruc.getUser(userId);
								System.out.println("u appid="+user.getConsumer().getAppId());
								if(!user.getConsumer().getAppId().equals(c.getAppId()))
								{
									throw new FundexpertException("You are not elegible to delete the holding.");
								}
								else
								{
									HoldingController hc=new HoldingController();
									boolean success=hc.deleteAllHoldings(userId);
									resp.put("success", success);
								}
							}
						}
						else if(request.getParameter("action").equals("createHolding"))
						{
							System.out.println("Creating Holding.");
							String userId=request.getParameter("userId");
							if(userId==null)
								throw new Exception("UserId not passed.");
							StringBuilder builder=new StringBuilder();
							BufferedReader br=new BufferedReader(new InputStreamReader(request.getInputStream()));
							String line="";
							try
							{
								while((line=br.readLine())!=null){  
									System.out.print(line);
									builder.append(line);
								}  
								System.out.println("Builder string = "+builder.toString());
								JSONArray jsonArray=new JSONArray(builder.toString());
								PortfolioImportController pic=new PortfolioImportController();
								Map<String,List<Holdings>> map=pic.createHoldingFromJsonArrayWithId(jsonArray);
								List<Holdings> list=map.get("success");
								System.out.println("List size = "+list.size());
								list.forEach(i->System.out.println("MFID="+i.getCamsCode()+" mfname "+i.getMutualFundName()));
								pic.sFactory(Long.valueOf(userId), list);
								List<Map> missingFundMap=pic.getMissingFunds();
								JSONArray arr=new JSONArray(missingFundMap);
								
								resp.put("success", true);
								resp.put("mssingFunds", arr.toString());
							}
							catch(Exception e)
							{
								e.printStackTrace();
								throw new FundexpertException("Can't store Holdings.");
							}
							finally
							{
								br.close();    								
							}
						}
						else
						{
							throw new FundexpertException("Invalid Action.");
						}
					}
					else
					{
						throw new FundexpertException("Action not specified.");
					}
				}
				else
				{
					throw new FundexpertException("Need a paramter.");
				}
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			resp.put("success", false);
			resp.put("error", fe.getMessage());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("success", false);
			resp.put("error", e.getMessage());
		}
		out.write(resp.toString());
	}
}
