package com.fundexpert.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.JSON;
import com.fundexpert.controller.MutualFundController;
import com.fundexpert.controller.StocksController;
import com.fundexpert.dao.Stocks;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;

/**
 * Servlet implementation class StocksServlet
 */
@WebServlet("/stocks")
public class StocksServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StocksServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		JSONObject resp=new JSONObject();
		User user=null;
		PrintWriter pw=response.getWriter();
		try
		{
			if(request.getParameterMap().isEmpty() || !request.getParameterMap().containsKey("action"))
			{
				throw new FundexpertException("Action not passed.");
			}
			else
			{
				if(request.getParameter("action").equalsIgnoreCase("getStocksByCapitalType"))
				{
					String capitalType=request.getParameter("capitalType");
					if(capitalType==null || capitalType.equals(""))
						throw new FundexpertException("Capital Type not passed.");
					/*user=(User)request.getSession().getAttribute("user");
					String loggedIn=(String)request.getSession().getAttribute("login");
					if(loggedIn==null || loggedIn.equals("false") || user==null)
						throw new FundexpertException("You are not logged in.");*/
					StocksController stocksController=new StocksController();
					List<Stocks> list=stocksController.getStockByCapitalType(capitalType);
					System.out.println("List.size = "+list.size());
					resp.put("stocks", JSON.toJSONArray(list));
					resp.put("success", true);
				}
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			resp.put("success", false);
			resp.put("error", fe.getMessage());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("success", false);
			resp.put("error", e.getMessage());
		}
		pw.write(resp.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject resp=new JSONObject();
		User user=null;
		try
		{
			if(request.getParameterMap().isEmpty())
			{
				throw new FundexpertException("No Parameters.");
			}
			if(request.getParameterMap().containsKey("action"))
			{
				user=(User)request.getSession().getAttribute("user");
				String loggedIn=(String)request.getSession().getAttribute("login");
				if(loggedIn==null || loggedIn.equals("false") || user==null)
					throw new FundexpertException("You are not logged in.");
				if(request.getParameter("action").equals("deleteSingleStockFromBlackList"))
				{
					String scCode=request.getParameter("scCode");
					if(scCode==null)
						throw new FundexpertException("Parameter Missing.");
					StocksController sc=new StocksController();
					boolean success=sc.deleteSingleStockFromBlackList(Integer.valueOf(scCode),user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Delete Fund.");
				}
				else if(request.getParameter("action").equals("saveStockBlackList"))
				{
					String[] scCodeArray=request.getParameterValues("stockidarray[]");
					if(scCodeArray==null)
						throw new FundexpertException("Parameter Missing.");
					StocksController sc=new StocksController();
					boolean success=sc.saveBlackList(scCodeArray, user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Persist Funds.");
				}
				else if(request.getParameter("action").equals("deleteAllStockFromBlackList"))
				{
					StocksController sc=new StocksController();
					boolean success=sc.deleteAllStocksFromBlackList(user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Delete all Stocks.");
				}
				if(request.getParameter("action").equals("deleteSingleStockFromRecommendedList"))
				{
					String scCode=request.getParameter("scCode");
					if(scCode==null)
						throw new FundexpertException("Parameter Missing.");
					StocksController sc=new StocksController();
					boolean success=sc.deleteSingleStockFromRecommended(Integer.valueOf(scCode),user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Delete Fund.");
				}
				else if(request.getParameter("action").equals("saveStockRecommended"))
				{
					String[] scCodeArray=request.getParameterValues("stockidarray[]");
					if(scCodeArray==null)
						throw new FundexpertException("Parameter Missing.");
					StocksController sc=new StocksController();
					boolean success=sc.saveRecommended(scCodeArray, user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Persist Funds.");
				}
				else if(request.getParameter("action").equals("deleteAllStockFromRecommended"))
				{
					StocksController sc=new StocksController();
					boolean success=sc.deleteAllStocksFromRecommended(user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Delete all Stocks.");
				}
				else if(request.getParameter("action").equals("deleteSingleStockFromReco"))
				{
					String scCode=request.getParameter("scCode");
					if(scCode==null)
						throw new FundexpertException("Parameter Missing.");
					StocksController sc=new StocksController();
					boolean success=sc.deleteSingleStockFromReco(Integer.valueOf(scCode),user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Delete Fund.");
				}
				else if(request.getParameter("action").equals("saveStockReco"))
				{
					String[] scCodeArray=request.getParameterValues("stockidarray[]");
					if(scCodeArray==null)
						throw new FundexpertException("Parameter Missing.");
					StocksController sc=new StocksController();
					boolean success=sc.saveReco(scCodeArray, user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Persist Funds.");
				}
				else if(request.getParameter("action").equals("deleteAllStockFromReco"))
				{
					StocksController sc=new StocksController();
					boolean success=sc.deleteAllStocksFromReco(user.getConsumerId());
					resp.put("success", success);
					if(!success)
						resp.put("error", "Not able to Delete all Stocks.");
				}
			}
			else if(request.getParameterMap().containsKey("updateStocks"))
			{
				if(request.getParameter("updateStocks").equals("getUpdatedStocks"))
				{
					System.out.println("updating existing stocks.");
					JSONArray array=null;
					StringBuilder builder=new StringBuilder();
					BufferedReader br=new BufferedReader(new InputStreamReader(request.getInputStream()));
					String line=br.readLine();
					JSONObject json=null;
					JSONObject responseJsonObject=null;
					while(line!=null)
					{
						builder.append(line);
						line=br.readLine();
					}
					//System.out.println("received = "+builder.toString());
					JSONArray requestJsonArray=new JSONArray(builder.toString());
					StocksController sc=new StocksController();
					List<Stocks> list=sc.getUpdatedStocks(requestJsonArray);
					if(list!=null && list.size()>0)
					{
						SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
						array=new JSONArray();
						for(Stocks stock:list)
						{
							json=new JSONObject();
							json.put("scCode", stock.getScCode());
							json.put("closePrice", stock.getClosePrice());
							json.put("rating", stock.getRating());
							json.put("asOn", stock.getAsOn()!=null?sdf.format(stock.getAsOn()):null);
							json.put("isinCode", stock.getIsinCode());
							json.put("scGroup", stock.getScGroup());
							json.put("name", stock.getScName());
							json.put("scType", stock.getScType());
							json.put("sector", stock.getSector());
							json.put("segment", stock.getSegment());
							json.put("type", stock.getType());
							json.put("block",stock.isBlock());
							json.put("delisted", stock.isDelisted());
							json.put("updatedOn", stock.getUpdatedOn()!=null?sdf.format(stock.getUpdatedOn()):null);
							array.put(json);
						}
						resp.put("success", true);
						resp.put("stockData", array);
					}
					else
					{
						resp.put("success", true);
					}
				}
				else if(request.getParameter("updateStocks").equals("getNewStocks"))
				{
					System.out.println("Getting new Stocks");
					JSONArray array=null;
					StringBuilder builder=new StringBuilder();
					BufferedReader br=new BufferedReader(new InputStreamReader(request.getInputStream()));
					String line=br.readLine();
					JSONObject json=null;
					JSONObject responseJsonObject=null;
					while(line!=null)
					{
						builder.append(line);
						line=br.readLine();
					}
					//System.out.println("received"+builder.toString());
					JSONArray requestJsonArray=null;
					if(builder.toString()!=null && builder.toString().equals(""))
					{
						requestJsonArray=new JSONArray();
						requestJsonArray.put(1);
					}
					else
					{
						requestJsonArray=new JSONArray(builder.toString());
					}
					StocksController sc=new StocksController();
					List<Stocks> list=sc.getNewStocks(requestJsonArray);
					if(list!=null && list.size()>0)
					{
						SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
						array=new JSONArray();
						for(Stocks stock:list)
						{
							json=new JSONObject();
							json.put("scCode", stock.getScCode());
							json.put("closePrice", stock.getClosePrice());
							json.put("rating", stock.getRating());
							json.put("asOn", stock.getAsOn()!=null?sdf.format(stock.getAsOn()):null);
							json.put("isinCode", stock.getIsinCode());
							json.put("scGroup", stock.getScGroup());
							json.put("name", stock.getScName());
							json.put("scType", stock.getScType());
							json.put("sector", stock.getSector());
							json.put("segment", stock.getSegment());
							json.put("type", stock.getType());
							json.put("block",stock.isBlock());
							json.put("delisted", stock.isDelisted());
							json.put("updatedOn", stock.getUpdatedOn()!=null?sdf.format(stock.getUpdatedOn()):null);
							array.put(json);
						}
						resp.put("success", true);
						resp.put("stockData", array);
					}
					else
					{
						resp.put("success", true);
					}
				}
				else
				{
					resp.put("error", "Not a valid request.");
					resp.put("success", false);
				}
					
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			resp.put("success", false);
			resp.put("error", fe.getMessage());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("success", false);
			resp.put("error", "Internal Error.");
		}
		finally
		{
			response.getWriter().write(resp.toString());
		}
	}

}
