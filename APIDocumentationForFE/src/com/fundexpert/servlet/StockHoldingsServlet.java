package com.fundexpert.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fundexpert.controller.CunsumerController;
import com.fundexpert.controller.PortfolioImportController;
import com.fundexpert.controller.StockHoldingController;
import com.fundexpert.controller.StocksPortfolioImportController;
import com.fundexpert.controller.RegisterUserController;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.pojo.Holdings;
import com.fundexpert.pojo.StocksHoldings;

/**
 * Servlet implementation class StockHoldingsServlet
 */
public class StockHoldingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StockHoldingsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject resp=new JSONObject();
		PrintWriter out=response.getWriter();
		long userId=0;
		try
		{
			if(!request.getParameterMap().containsKey("appId"))
			{
				throw new FundexpertException("Empty parameter.");
			}
			else
			{
				String appId=request.getParameter("appId");
				CunsumerController cc=new CunsumerController();
				Consumer c=cc.getConsumer(appId);
				if(c!=null)
				{
					if(request.getParameterMap().containsKey("action"))
					{
						if(request.getParameter("action").equals("deleteStockHoldings"))
						{
							if(!request.getParameterMap().containsKey("userId"))
							{
								throw new FundexpertException("Empty parameter.");
							}
							else
							{
								userId=Long.valueOf(request.getParameter("userId"));
								RegisterUserController ruc=new RegisterUserController();
								User user=ruc.getUser(userId);
								System.out.println("u appid="+user.getConsumer().getAppId());
								if(!user.getConsumer().getAppId().equals(c.getAppId()))
								{
									throw new FundexpertException("You are not elegible to delete the holding.");
								}
								else
								{
									StockHoldingController hc=new StockHoldingController();
									boolean success=hc.deleteAllStockHoldings(userId);
									resp.put("success", success);
								}
							}
						}
						else if(request.getParameter("action").equals("createStockHoldings"))
						{
							if(!request.getParameterMap().containsKey("userId"))
							{
								throw new FundexpertException("UserId not passed.");
							}
							userId=Long.valueOf(request.getParameter("userId"));
							StringBuilder builder=new StringBuilder();
							BufferedReader br=new BufferedReader(new InputStreamReader(request.getInputStream()));
							String line="";
							try
							{
								while((line=br.readLine())!=null){  
									builder.append(line);
								}  
								System.out.println("Builder string = "+builder.toString());
								JSONArray jsonArray=new JSONArray(builder.toString());
								StocksPortfolioImportController spic=new StocksPortfolioImportController();
								List<StocksHoldings> list=spic.createStockHoldingsFromJsonArrayWithScCode(jsonArray);
								System.out.println("Stocks List size = "+list.size());
								list.forEach(i->System.out.println("Stocks isin ="+i.getIsinCode()+" stock name  "+i.getScName()));
								spic.sFactory(Long.valueOf(userId), list);
								resp.put("success", true);
							}
							catch(Exception e)
							{
								e.printStackTrace();
								throw new FundexpertException("Can't store StockHoldings.");
							}
							finally
							{
								br.close();    								
							}
							
						}
						else
						{
							throw new FundexpertException("Invalid Action.");
						}
					}
					else
					{
						throw new FundexpertException("Action not specified.");
					}
				}
				else
				{
					throw new FundexpertException("Need a paramter.");
				}
			}
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			resp.put("success", false);
			resp.put("error", fe.getMessage());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.put("success", false);
			resp.put("error", "Something Failed");
		}
		out.write(resp.toString());
	}

}
