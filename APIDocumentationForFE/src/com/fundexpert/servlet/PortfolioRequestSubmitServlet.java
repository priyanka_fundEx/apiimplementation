package com.fundexpert.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.PortfolioRequest;
import com.fundexpert.dao.PortfolioUpload;
import com.fundexpert.dao.User;

public class PortfolioRequestSubmitServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("PortfolioRequestSubmitServlet called.");
		JSONObject res=new JSONObject();
		Session hSession=HibernateBridge.getSessionFactory().openSession();
		response.addHeader("Access-Control-Allow-Origin", "*");
		PrintWriter out=response.getWriter();
		try{
			if(!request.getParameterMap().containsKey("action")){
				throw new ValidationException("Action not passed.");
			}else if(request.getParameter("action").equalsIgnoreCase("getRequest")){
				PortfolioRequest portfolioRequest=(PortfolioRequest)hSession.createQuery("from PortfolioRequest where submitted=? order by rand()").setBoolean(0, false).setMaxResults(1).uniqueResult();
				if(portfolioRequest!=null){
					if(portfolioRequest.getPassword()==null || portfolioRequest.getPassword().equals(""))
					{
						throw new Exception("Pan is not given in password field of PortfolioRequest.");
					}
					res=PortfolioRequestSubmitServlet.toJSONObject(portfolioRequest);
					if(portfolioRequest.getUser().getPan()!=null && !portfolioRequest.getUser().getPan().equals(""))
					{
						res.put("panNumber", portfolioRequest.getUser().getPan());
					}
				}else
				{
					throw new EntityNotFoundException("No new requests found.");
				}
			}else if(request.getParameter("action").equalsIgnoreCase("requestSubmitted")){
				if(!request.getParameterMap().containsKey("id")){
					throw new ValidationException("Id not passed.");
				}else{
					Long id=Long.parseLong(request.getParameter("id"));
					PortfolioRequest portfolioRequest=(PortfolioRequest)hSession.get(PortfolioRequest.class, id);
					if(portfolioRequest!=null){
						Transaction transaction=hSession.beginTransaction();
						portfolioRequest.setSubmitted(true);
						portfolioRequest.setSubmittedOn(new Date());
						hSession.save(portfolioRequest);
						transaction.commit();
						res.put("success", true);
					}else{
						throw new ValidationException("Request not found.");					
					}
				}
			}
			else if(request.getParameter("action").equalsIgnoreCase("getAllRequest")){
				System.out.println("getAllRequest action called in PortfolioRequestSubmitServlet.java");
				List<PortfolioRequest> portfolioRequestList=hSession.createQuery("from PortfolioRequest").list();
				if(portfolioRequestList!=null && portfolioRequestList.size()>0){
					JSONArray jsonArray=new JSONArray();
					for(PortfolioRequest portfolioRequest:portfolioRequestList)
					{
						//PortfolioUpload portfolioUpload=(PortfolioUpload)hSession.createQuery("from PortfolioUpload where userId=? and uploadTime>?").setLong(0, portfolioRequest.getUserId()).setTimestamp(1, portfolioRequest.getRequestOn()).setMaxResults(1).uniqueResult();
						//below if case means either there is no portfolioUploaded for first portfolioRequest or if there is not portfolioUploded after PortfolioRequest
						//if(portfolioUpload==null)
						//{
							if(portfolioRequest.getPassword()==null || portfolioRequest.getPassword().equals(""))
							{
								System.out.println("Password field is empty in PortfolioRequest table for userid="+portfolioRequest.getUserId());
								continue;
								//throw new Exception("Pan is not given in password field of PortfolioRequest.");
							}
							res=PortfolioRequestSubmitServlet.toJSONObject(portfolioRequest);
							res.put("submitted", portfolioRequest.getSubmitted());
							jsonArray.put(res);
						//}
					}
					res=new JSONObject();
					res.put("success", true);
					res.put("pr", jsonArray);
				}else
				{
					throw new EntityNotFoundException("No new requests found.");
				}
			}
			else{
				throw new ValidationException("Invalid action.");				
			}
		}catch(EntityNotFoundException e){
			try{
				res.put("success", false);
				res.put("errorCode", 404);
				res.put("error", e.getMessage());			
			}catch(JSONException e2){
				e2.printStackTrace();
			}
		}catch(ValidationException e){
			try{
				res.put("success", false);
				res.put("error", e.getMessage());			
			}catch(JSONException e2){
				e2.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
			try{
				res.put("success", false);
				res.put("error", "Please contact system administrator.");
			}catch(JSONException e2){
				e2.printStackTrace();
			}
		}finally{
			out.write(res.toString());
			hSession.close();
		}
	}
	
	public static JSONObject toJSONObject(PortfolioRequest p) throws JSONException
	{
		JSONObject obj = new JSONObject();
		obj.put("id", p.getId());
		obj.put("userId", p.getUserId());
		obj.put("email", p.getUser().getEmail());
		obj.put("createdOn", p.getRequestOn());
		obj.put("password", p.getPassword());
		obj.put("submitted", p.getSubmitted());
		obj.put("panNumber",p.getUser().getPan());
		return obj;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("post function called at the time of submitting cams request for apirequest from build made temporarily on Fundexpert with name api.");
		JSONObject json_response= new JSONObject();
		response.setContentType("application/Json");
		StringBuffer jb = new StringBuffer();		
	    String line = null;		
	    try 
	    {
	    	BufferedReader reader = request.getReader();
	    	while ((line = reader.readLine()) != null)
	    		jb.append(line);		    
	    } 		
	    catch (Exception e) 
	    { 		  
	    	e.printStackTrace();
	    	try 
	    	{
	    		json_response.put("success", false);
	    	}
	    	catch (JSONException e1)
	    	{
	    		e1.printStackTrace();
	    	}
	    }	
	    Session hSession=null;
	    Transaction transaction=null;
	    try 
	    {
	    	hSession=HibernateBridge.getSessionFactory().openSession();
	    	transaction=hSession.beginTransaction();				
	    	JSONObject jsonObject =  new JSONObject(jb.toString());
	    	System.out.println("Successflly submitted data of portfoliorequest for userid="+jsonObject.getLong("userId")+" and portfolioRequestID = "+jsonObject.getLong("apiId")+" appId= "+jsonObject.getString("appId"));
    		Consumer consumer =(Consumer)hSession.createQuery("select c from Consumer c,User u where c.appId=? and u.id=? and u.consumerId=c.id").setString(0,jsonObject.getString("appId")).setLong(1, jsonObject.getLong("userId")).setMaxResults(1).uniqueResult();
	    	if(consumer!=null) 
	    	{
	    		System.out.println("Consumer is not null");
    			PortfolioRequest pr=(PortfolioRequest)hSession.createQuery("from PortfolioRequest where id=?").setLong(0,jsonObject.getLong("apiId")).setMaxResults(1).uniqueResult();
    			if(pr!=null)
    			{
    				System.out.println("Successflly submitted data of portfoliorequest for userid="+jsonObject.getLong("userId")+" and portfolioRequestID = "+jsonObject.getLong("apiId"));
    				pr.setSubmitted(true);
    				pr.setSubmittedOn(new Date());
    				json_response.put("success", true);
    			}
    			else
				{
    				throw new Exception("PortfolioRequest is empty at id = "+jsonObject.getLong("apiId"));
				}
			}
			else
			{
				throw new Exception("Consumer id not found at the level of userId="+jsonObject.getLong("userId")+" and consumer appid = "+jsonObject.getString("appId"));
			}
    		transaction.commit();
	    }		  
	    catch (Exception e) 
	    {
	    	if( transaction!=null)
	    		transaction.rollback();
    		e.printStackTrace();
    		try 
    		{
    			json_response.put("success", false);
    			json_response.put("error", e.getMessage());
    		} catch (JSONException e1)
    		{
    			e1.printStackTrace();
    		}
	    }		  
	    finally
	    {
	    	hSession.close();
	    }
	    response.getWriter().write(json_response.toString());
	}
}
