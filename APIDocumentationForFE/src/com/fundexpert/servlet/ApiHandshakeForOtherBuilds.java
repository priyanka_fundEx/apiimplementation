package com.fundexpert.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.json.JSONException;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;
import com.fundexpert.exception.InputFormatException;

/**
 * Servlet implementation class ApiHandshakeForOtherBuilds
 */
@WebServlet("/letmego")
public class ApiHandshakeForOtherBuilds extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApiHandshakeForOtherBuilds() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("Api HandshakeForOtherBuils.java called.");
		Session hSession=null;
		JSONObject jobject = new JSONObject();
		try
		{
			
			StringBuilder builder=new StringBuilder();
			BufferedReader br=new BufferedReader(new InputStreamReader(request.getInputStream()));
			String line="";
			
			while ((line=br.readLine())!=null){
				builder.append(line);
			}
			JSONObject jObject;
			try
			{
				 jObject=new JSONObject(builder.toString());
			}
			catch(JSONException jsonE)
			{
				throw new Exception("Invalid input format.");
			}
			if(!jObject.has("appId"))
			{
				throw new Exception("AppId does not exists.");
			}
			if(!jObject.has("pass"))
			{
				throw new Exception("Pass is mandatory.");
			}
			else
			{
				String currentDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				if(!jObject.getString("pass").equals(String.valueOf(currentDate.hashCode())))
				{
					throw new Exception("Try it out later.");
				}
			}
			
			hSession=HibernateBridge.getSessionFactory().openSession();
			Consumer consumer=(Consumer)hSession.createQuery("from Consumer where appId=?").setString(0, jObject.getString("appId")).uniqueResult();
			if(consumer==null)
			{
				System.out.println("appid="+jObject.getString("appId").hashCode());
				throw new Exception("No Consumer;");
			}
			jobject.put("success", true);
			jobject.put("config", consumer.isActive());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			jobject.put("success", false);
			jobject.put("error", e.getMessage());
		}
		finally
		{
			if(hSession!=null)
				hSession.close();
		}
		response.getWriter().write(jobject.toString());
	}

}
