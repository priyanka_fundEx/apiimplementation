package com.fundexpert.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class OptimizerDummyResponse
 */
@WebServlet("/OptResp")
public class OptimizerDummyResponse extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OptimizerDummyResponse() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String var="{\r\n" + 
				"    \"success\": true,\r\n" + 
				"    \"mf\": [\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 0,\r\n" + 
				"            \"nav\": 100.6259,\r\n" + 
				"            \"unitsOut\": 0,\r\n" + 
				"            \"aIn\": 5,\r\n" + 
				"            \"valueOut\": 0,\r\n" + 
				"            \"segment\": \"DEBT\",\r\n" + 
				"            \"mCap\": \"SHORT TERM\",\r\n" + 
				"            \"valueIn\": 1036.447,\r\n" + 
				"            \"unitsIn\": 10.3,\r\n" + 
				"            \"name\": \"DHFL PRAMERICA INSTA CASH FUND - WEEKLY DIVIDEND - DIVIDEND REINVESTMENT\",\r\n" + 
				"            \"action\": \"sell\",\r\n" + 
				"            \"state\": \"existing\",\r\n" + 
				"            \"arn\": \"ARN-70892\",\r\n" + 
				"            \"isin\": \"INF223J01BU6\",\r\n" + 
				"            \"arnName\": \"EDELWEISS BROKING LIMITED\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 7.5,\r\n" + 
				"            \"nav\": 1401.0666,\r\n" + 
				"            \"unitsOut\": 1.1096606740536104,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 1554.709,\r\n" + 
				"            \"segment\": \"DEBT\",\r\n" + 
				"            \"mCap\": \"LONG TERM\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"INVESCO INDIA CREDIT RISK FUND - REGULAR PLAN DISCRETIONARY DIVIDEND REINVESTMENT\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INF205K01I75\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 7.5,\r\n" + 
				"            \"nav\": 13.6731,\r\n" + 
				"            \"unitsOut\": 113.7056342563135,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 1554.709,\r\n" + 
				"            \"segment\": \"DEBT\",\r\n" + 
				"            \"mCap\": \"LONG TERM\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"DHFL PRAMERICA CREDIT RISK FUND - DIRECT PLAN - ANNUAL DIVIDEND - REINVESTMENT\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INF663L01GH5\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 7.5,\r\n" + 
				"            \"nav\": 12.9456,\r\n" + 
				"            \"unitsOut\": 120.09551567714126,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 1554.709,\r\n" + 
				"            \"segment\": \"DEBT\",\r\n" + 
				"            \"mCap\": \"SHORT TERM\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"PRINCIPAL SHORT TERM DEBT FUND - DIRECT PLAN DIVIDEND MONTHLY REINVESTMENT\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INF173K01GR6\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 7.5,\r\n" + 
				"            \"nav\": 10.16,\r\n" + 
				"            \"unitsOut\": 153.0224909202756,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 1554.709,\r\n" + 
				"            \"segment\": \"DEBT\",\r\n" + 
				"            \"mCap\": \"SHORT TERM\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"DSP Banking and PSU Debt Fund - Reg - Weekly Dividend Reinvestment\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INF123BSE005\"\r\n" + 
				"        }\r\n" + 
				"    ],\r\n" + 
				"    \"stock\": [\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 0,\r\n" + 
				"            \"nav\": 1969.3,\r\n" + 
				"            \"unitsOut\": 0,\r\n" + 
				"            \"aIn\": 95,\r\n" + 
				"            \"broker\": \"EDELWEISS BROKING LIMITED\",\r\n" + 
				"            \"valueOut\": 0,\r\n" + 
				"            \"segment\": \"Housing Finance \",\r\n" + 
				"            \"mCap\": \"LARGE CAP\",\r\n" + 
				"            \"valueIn\": 19693,\r\n" + 
				"            \"unitsIn\": 10,\r\n" + 
				"            \"name\": \"HOUSING DEVELOPMENT FINANCE CORP.LT\",\r\n" + 
				"            \"action\": \"sell\",\r\n" + 
				"            \"state\": \"existing\",\r\n" + 
				"            \"isin\": \"INE001A01036\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 3.932,\r\n" + 
				"            \"nav\": 271.65,\r\n" + 
				"            \"unitsOut\": 3,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 815,\r\n" + 
				"            \"segment\": \"Oil Marketing & Distribution\",\r\n" + 
				"            \"mCap\": \"SMALL CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"GOCL CORPORATION LIMITED\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE077F01035\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 3.054,\r\n" + 
				"            \"nav\": 158.3,\r\n" + 
				"            \"unitsOut\": 4,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 633,\r\n" + 
				"            \"segment\": \"Oil Marketing & Distribution\",\r\n" + 
				"            \"mCap\": \"MID CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"CASTROL INDIA LTD\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE172A01027\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 1.91,\r\n" + 
				"            \"nav\": 132.15,\r\n" + 
				"            \"unitsOut\": 3,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 396,\r\n" + 
				"            \"segment\": \"Oil Marketing & Distribution\",\r\n" + 
				"            \"mCap\": \"LARGE CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"INDIAN OIL CORPORATION LTD.\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE242A01010\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 9.392,\r\n" + 
				"            \"nav\": 1814.4,\r\n" + 
				"            \"unitsOut\": 1,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 1946.889,\r\n" + 
				"            \"segment\": \"IT Consulting & Software\",\r\n" + 
				"            \"mCap\": \"LARGE CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"TATA CONSULTANCY SERVICES LTD.\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE467B01029\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 4.886,\r\n" + 
				"            \"nav\": 1187.2,\r\n" + 
				"            \"unitsOut\": 1,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 1012.834,\r\n" + 
				"            \"segment\": \"IT Consulting & Software\",\r\n" + 
				"            \"mCap\": \"SMALL CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"NIIT TECHNOLOGIES LIMITED\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE591G01017\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 8.698,\r\n" + 
				"            \"nav\": 300.5,\r\n" + 
				"            \"unitsOut\": 6,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 1803,\r\n" + 
				"            \"segment\": \"Banks\",\r\n" + 
				"            \"mCap\": \"LARGE CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"STATE BANK OF INDIA,\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE062A01020\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 9.957,\r\n" + 
				"            \"nav\": 98.25,\r\n" + 
				"            \"unitsOut\": 21,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 2064,\r\n" + 
				"            \"segment\": \"Banks\",\r\n" + 
				"            \"mCap\": \"SMALL CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"ORIENTAL BANK OF COMMERCE\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE141A01014\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 9.682,\r\n" + 
				"            \"nav\": 62.7,\r\n" + 
				"            \"unitsOut\": 32,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 2007,\r\n" + 
				"            \"segment\": \"Banks\",\r\n" + 
				"            \"mCap\": \"MID CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"IDBI BANK LTD\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE008A01015\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 7.264,\r\n" + 
				"            \"nav\": 762.45,\r\n" + 
				"            \"unitsOut\": 2,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 1505.889,\r\n" + 
				"            \"segment\": \"Auto Parts & Equipment\",\r\n" + 
				"            \"mCap\": \"MID CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"AMARA RAJA BATTERIES LTD\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE885A01032\"\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"            \"aOut\": 11.226,\r\n" + 
				"            \"nav\": 178.95,\r\n" + 
				"            \"unitsOut\": 13,\r\n" + 
				"            \"aIn\": 0,\r\n" + 
				"            \"valueOut\": 2327,\r\n" + 
				"            \"segment\": \"Auto Parts & Equipment\",\r\n" + 
				"            \"mCap\": \"SMALL CAP\",\r\n" + 
				"            \"valueIn\": 0,\r\n" + 
				"            \"unitsIn\": 0,\r\n" + 
				"            \"name\": \"BANCO PRODUCTS (INDIA) LTD.,\",\r\n" + 
				"            \"action\": \"buy\",\r\n" + 
				"            \"state\": \"new\",\r\n" + 
				"            \"isin\": \"INE213C01025\"\r\n" + 
				"        }\r\n" + 
				"    ]\r\n" + 
				"}";
		
		PrintWriter pw=response.getWriter();
		pw.write(var);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
