package com.fundexpert.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONObject;

import com.fundexpert.config.Config;
import com.fundexpert.controller.HoldingController;
import com.fundexpert.controller.PortfolioImportController;
import com.fundexpert.controller.StockHoldingController;
import com.fundexpert.controller.StocksPortfolioImportController;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.pojo.Holdings;
import com.fundexpert.pojo.StocksHoldings;
import com.fundexpert.util.PdfReaderAndPersist;
import com.fundexpert.util.PdfReaderAndPersist.PdfParser;
import com.fundexpert.util.PdfReaderAndPersist.StockPdfParser;
import com.itextpdf.text.exceptions.BadPasswordException;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;


/**
 * Servlet implementation class FileUpload
 */
public class FileUpload extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject json=new JSONObject();
		PrintWriter pw=response.getWriter();
		try 
		{
			System.out.println("1");
			Config config=new Config();
			String demoDir=config.getProperty(Config.DEMO_DIR);
			File mfFile=null,stockFile=null;
			File mfFile1=new File(demoDir);
			File stockFile1=new File(demoDir);
			if(!mfFile1.exists())
			{
				mfFile1.mkdirs();
			}
			if(!stockFile1.exists())
			{
				stockFile1.mkdirs();
			}
			System.out.println("2");
			String mfPan=null,stockPan=null;
			ServletFileUpload sf=new ServletFileUpload(new DiskFileItemFactory());
			List<FileItem> multifiles=sf.parseRequest(request);
			boolean flag=false;
			for(FileItem item:multifiles)
			{
				if(!item.isFormField())
				{
					System.out.println("item.name = "+item.getName());
					if(item.getName()==null || item.getName().equals(""))
						continue;
					if(!item.getName().substring(item.getName().length()-3, item.getName().length()).toLowerCase().equals("pdf"))
						throw new FundexpertException("Please upload Pdf type file only.");
					if(item.getFieldName().equals("mffile"))
					{
						mfFile = new File(demoDir+File.separator+item.getName());
						String name=mfFile.getName();
						item.write(mfFile);
					}
					if(item.getFieldName().equals("stockfile"))
					{
						stockFile = new File(demoDir+File.separator+item.getName());
						String name=stockFile.getName();
						item.write(stockFile);
					}
				}
				else
				{
					if(item.getFieldName().equals("mpan"))
						mfPan=item.getString();
					if(item.getFieldName().equals("span"))
						stockPan=item.getString();
				}
			}
			if(mfFile==null && stockFile==null)
				throw new FundexpertException("Please choose atleast one file.");
			if((mfFile!=null && (mfPan==null || mfPan.equals(""))))
				throw new FundexpertException("MF Password is mandatory.");
			if(stockFile!=null && (stockPan==null || stockPan.equals("")))
				throw new FundexpertException("Stock Password is mandatory.");
			if(mfFile!=null)
			{
				json.put("mfUpload", true);
				try
				{
					HoldingController hc=new HoldingController();
					hc.deleteAllHoldings(3l);
					if(stockFile==null)
					{
						StockHoldingController shc=new StockHoldingController();
						shc.deleteAllStockHoldings(3l);
					}
					
					PdfParser pdfParser=new PdfReaderAndPersist().new PdfParser();
					List<Holdings> holdingsList=pdfParser.getHoldingsList(mfFile.getAbsolutePath(), mfPan);
					
					PortfolioImportController pic=new PortfolioImportController();
					pic.sFactory(3l, holdingsList);
					json.put("mfSuccess", true);
					
				}
				catch(BadPasswordException bpe)
				{
					bpe.printStackTrace();
					throw new FundexpertException("Please enter correct password associated with MutualFund File.");
				}
				catch(FundexpertException fe)
				{
					throw fe;
				}
				catch(Exception e)
				{
					e.printStackTrace();
					json.put("mfSuccess", false);
				}
			}
			if(stockFile!=null)
			{
				json.put("stockUpload", true);
				try
				{
					StockHoldingController shc=new StockHoldingController();
					shc.deleteAllStockHoldings(3l);
					
					if(mfFile==null)
					{
						HoldingController hc=new HoldingController();
						hc.deleteAllHoldings(3l);
					}
					
					StockPdfParser stockReader=new PdfReaderAndPersist().new StockPdfParser(stockFile.getAbsolutePath(),stockPan);
					List<StocksHoldings> stocksHoldingsList=stockReader.getStocksHoldingsList();
					StocksPortfolioImportController spic=new StocksPortfolioImportController();
					spic.sFactory(3l, stocksHoldingsList);
					json.put("stockSuccess", true);
				}
				catch(BadPasswordException bpe)
				{
					bpe.printStackTrace();
					throw new FundexpertException("Please enter correct password associated with stock file.");
				}
				catch(FundexpertException fe)
				{
					throw fe;
				}
				catch(Exception e)
				{
					e.printStackTrace();
					json.put("stockSuccess", false);
				}
			}
			
			json.put("success", true);
		}
		catch(BadPasswordException bpe)
		{
			bpe.printStackTrace();
			json.put("success", false);
			json.put("basicerror", "Please enter pan associated with selected statement.");
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			json.put("success", false);
			json.put("basicerror", fe.getMessage());
		}
		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			json.put("success", false);
			json.put("mainerror", "Internal Error occurred.");
		}
		pw.write(json.toString());
	}
}
