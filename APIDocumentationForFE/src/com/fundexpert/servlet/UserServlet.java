package com.fundexpert.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.JSON;
import com.fundexpert.controller.UserController;
import com.fundexpert.controller.UserPortfolioStateController;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/user")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JSONObject json_response1= new JSONObject();
		HttpSession session = request.getSession();
		ServletContext ctxt=null;
		Map<String,HttpSession> loginMap=null;
		try
		{
			System.out.println("Reached in userServlet.");
			String method = request.getParameter("method");
			if(method==null)
				throw new FundexpertException("Method not passed.");
			if(method.equals("adminLogin"))
			{
				String user_id=request.getParameter("app_id");
				String password_id=request.getParameter("password_id");
				if(user_id==null || password_id==null)
				{
					throw new FundexpertException("all fields are mandatory.");
				}
				String alreadyLoggedIn=(String)session.getAttribute("login");
				String loggedInUserId=(String)session.getAttribute("userId");
				if(alreadyLoggedIn!=null && alreadyLoggedIn.equals("true") && loggedInUserId!=null && loggedInUserId.equals(user_id))
				{
					throw new FundexpertException("Already LoggedIn.");
				}
				System.out.println("appid="+user_id+" password="+password_id);
				
				UserController uCtrl=new UserController();
				User user=uCtrl.getAdminUser(user_id, password_id);
				if(user==null)
				{
					throw new FundexpertException("User does not exists.");
				}
				else
				{
					System.out.println("user exists.");
					json_response1.put("success", true);
					json_response1.put("role", user.getRole());
					try
					{
						ctxt=getServletConfig().getServletContext();
						loginMap=(Map)ctxt.getAttribute("loginMap");
						
						if(loginMap==null)
						{
							System.out.println("Login map is null.Adding user with id = "+user_id);
							loginMap=new HashMap<String,HttpSession>();
							System.out.println("Setting Session for user = "+user_id);
							session.setAttribute("login","true");
							session.setAttribute("userId", user_id);
							session.setAttribute("role", user.getRole());
							session.setMaxInactiveInterval(60*15);
							session.setAttribute("user", user);
							loginMap.put(user_id, session);
							ctxt.setAttribute("loginMap", loginMap);
						}
						else
						{
							System.out.println("Login map is not null.Existing loggedin ids are : ");
							for(Map.Entry<String, HttpSession> m:loginMap.entrySet())
							{
								System.out.println("m.getKey()="+m.getKey()+"  m.getValue()="+m.getValue());
							}
							HttpSession existingSessionRelatedToThisUser=loginMap.remove(user_id);
							if(existingSessionRelatedToThisUser!=null)
							{
								System.out.println("Invalidating session for user_id="+user_id+" "+existingSessionRelatedToThisUser);
								existingSessionRelatedToThisUser.invalidate();
							}
							System.out.println("Setting Session for user = "+user_id);
							session.setAttribute("login","true");
							session.setAttribute("userId", user_id);
							session.setAttribute("role", user.getRole());
							session.setMaxInactiveInterval(60*15);
							session.setAttribute("user", user);
							loginMap.put(user_id,session);
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					
				 }
			 }
			else if(method.equals("logout"))
			{
				System.out.println("Logged OUT.");
				ctxt=getServletConfig().getServletContext();
				loginMap=(Map)ctxt.getAttribute("loginMap");
				if(loginMap!=null)
				{
					String currentUserId=(String)session.getAttribute("userId");
					HttpSession existingSessionRelatedToThisUser=loginMap.remove(currentUserId);
				}
				session.invalidate();
				json_response1.put("success", true);
			}
			else if(method.equals("getUserPortfolioState"))
			{
				Long userId=null;Long.valueOf(request.getParameter("userId"));
				User user=null;
				if(userId==null)
				{
					user=(User)session.getAttribute("user");
					if(user==null)
						throw new FundexpertException("Not a valid User.");
					if(user.getRole()<4)
						throw new FundexpertException("You are not authorized to view the content.");
				}
				else
					user=new UserController().getUser(userId);
				if(user==null)
					throw new FundexpertException("You are not logged In");
				
				
				UserPortfolioStateController upsc=new UserPortfolioStateController();
				JSONArray array=JSON.toJSONArray(upsc.getUserPortfolioState(user.getId()));
				json_response1.put("portfolio", array);
				json_response1.put("userName", user.getEmail());
			}
				
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			json_response1.put("success", false);
			json_response1.put("error", fe.getMessage());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			json_response1.put("success", false);
			json_response1.put("error", "Error Occurred.");
		}
		finally
		{
			response.getWriter().write(json_response1.toString());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
