package com.fundexpert.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.fundexpert.config.Config;
import com.fundexpert.thread.EmailReaderThread;
import com.fundexpert.thread.KeepApiServiceAliveThread;
import com.fundexpert.thread.StockUpdationThread;

public class Listener implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		Config config=null;
		try
		{
			config=new Config();
			if(config.getProperty(Config.ENVIRONMENT).equals(Config.PRODUCTION))
			{
				/*Thread thread=new Thread(new EmailReaderThread());
				thread.start();*/

				Thread keepApiServiceAliveThread = new Thread(new KeepApiServiceAliveThread());
				keepApiServiceAliveThread.start();
				
				Thread stockUpdationThread = new Thread(new StockUpdationThread());
				stockUpdationThread.start();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	
}
