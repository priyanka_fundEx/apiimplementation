package com.fundexpert.test;

import java.util.List;

import com.fundexpert.controller.ListUsersController;
import com.fundexpert.controller.SessionController; 
import com.fundexpert.dao.User;

public class ListUserControllerTest 
{
	public static void main(String[] args) 
	{
	    SessionController sessionCtrl=new SessionController();
	    ListUsersController ctrl=new ListUsersController();
	    
		String appId =		"qwertyuiop12345";
		String secretKey = "mnbvcxz09876";
		String ipAddress = "127.0.0.1";
		Long consumerId =  1l;
		String sessionId = "6fe7ae5e";
		System.out.println("started.");	
		
	    consumerId=sessionCtrl.getClient(appId, ipAddress);
	    System.out.println("Consumer ID: "+consumerId);
	     
		List<User> list=ctrl.getList(appId, sessionId);
		for(User s:list)
		{
			System.out.println("User ID: "+s.getUserId()+",   Risk Appetite: "+s.getRiskAppetite());
		}
	}
}