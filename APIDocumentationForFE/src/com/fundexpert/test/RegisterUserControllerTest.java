package com.fundexpert.test;
 
import java.text.ParseException;

import com.fundexpert.controller.RegisterUserController; 
import com.fundexpert.controller.SessionController; 
import com.fundexpert.exception.ValidationException;

public class RegisterUserControllerTest 
{
	public static void main(String[] args) throws ParseException 
	{
	    SessionController sessionCtrl=new SessionController();
	    RegisterUserController ctrl=new RegisterUserController();
	    
		String appId="qwertyuiop12345";
		Long userId=null;
		String ipAddress="127.0.0.1";
		String sessionId="3f3-bb65-8";
		Long consumerId=null; 
		
		consumerId=sessionCtrl.getClient(appId, ipAddress);
		if(sessionCtrl.getValidSession(consumerId, sessionId))
			try {
				userId=ctrl.saveUser(consumerId, null, null, sessionId);
			} catch (ValidationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else
			System.out.println("not valid session id.");
		//ipHardcoded=cd.isIpHardcoded();
		System.out.println("started, CID: "+consumerId+",   User ID:  "+userId);
		
		//ctrl.checkUserLogin(appId, sessionId, ipAddress);
		System.out.println("Done.");
	}
}