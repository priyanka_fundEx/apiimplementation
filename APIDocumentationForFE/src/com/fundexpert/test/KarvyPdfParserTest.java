package com.fundexpert.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

import com.fundexpert.controller.PortfolioImportController;
import com.fundexpert.pojo.Holdings;
import com.fundexpert.pojo.Transactions;
import com.fundexpert.util.PdfReaderAndPersist;
import com.fundexpert.util.PdfReaderAndPersist.PdfParser;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class KarvyPdfParserTest {

	public static void main(String[] args) throws IOException, DocumentException {
		
		String sourcePath = "F:/PdfFiles/edelweiss/BTZPR9426A-Sankalp cams.pdf";
		String targetPath = "F:/PdfFiles/karvy/BTZPR9426A-Sankalp cams.pdf";
		String pass = "BTZPR9426A";
		/*String sourcePath = "F:/PdfFile/karvy/788876-wealthLasserUser.pdf";
		String targetPath = "F:/PdfFile/karvy/karvy/788876-wealthLasserUser.pdf";
		String pass = "788876";*/
//		String sourcePath="F:/Equity PDF/Karvy/KARVY.pdf";
//		String targetPath="F:/Equity PDF/Karvy/123456-KARVY.pdf";
//		String pass="123456";
		KarvyPdfParserTest k=new KarvyPdfParserTest();
		try {
			
			System.out.println("output of decrypted File="+k.getDecryptedFile(sourcePath, targetPath, pass));
			
			PdfReaderAndPersist prap=new PdfReaderAndPersist();
			PdfReaderAndPersist.PdfParser pdfParser=prap.new PdfParser();
			List<Holdings> holdingsList=pdfParser.getKarvyHoldingsList(targetPath, pass);
			Iterator itr = holdingsList.iterator();
			String write = "";
			while (itr.hasNext())
			{
				Holdings h = (Holdings) itr.next();

				write = write + h.getCamsCode() + "-" + h.getMutualFundName() + "\n" + h.getFolioNumberString() + "\n" + h.getArn() + "\n";

				List<Transactions> transList = h.getTransactionsList();
				Iterator itr1 = transList.iterator();
				while (itr1.hasNext())
				{
					Transactions trans = (Transactions) itr1.next();
					write = write + trans.getDate() + "\t" + trans.getAmount() + "\t" + trans.getUnits() + "\t" + trans.getPrice() + "\n";
				}
				write = write + h.getClosingUnits() + "\n\n\n";
			}
			System.out.println(write);
			PortfolioImportController pic=new PortfolioImportController();
			pic=new PortfolioImportController();
			pic.sFactory(3l, holdingsList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private String getDecryptedFile(String sourceFilePath,String targetFilePath,String password) throws IOException
	{
		String command = "";
		try
		{
			File f=new File(targetFilePath);
			if(f.exists())
				System.out.println("Success of deleting existing targetFile = "+f.delete());
			if(((String)System.getProperties().get("os.name")).contains("Windows"))
				command="C:/Program Files/qpdf-8.2.1/bin/qpdf.exe";
			else
				command="qpdf";
			ProcessBuilder pb = new ProcessBuilder(command, "--password="+password, "--decrypt", sourceFilePath, targetFilePath);
			pb. redirectErrorStream(true);
			
			StringBuffer output = new StringBuffer();

			Process p=pb.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";			
			while ((line = reader.readLine())!= null) 
			{
				output.append(line);
			}
			if(output.toString().toLowerCase().contains("invalid password"))
				throw new BadPasswordException("Not able to decrypt file.");
			return output.toString();
		}
		catch(BadPasswordException be)
		{
			throw be;
		}
		catch(IOException ioe)
		{
			throw ioe;
		}
	}

}
