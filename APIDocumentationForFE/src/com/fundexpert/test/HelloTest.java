package com.fundexpert.test;

import org.hibernate.Session;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.JSON;
import com.fundexpert.dao.User;
import com.fundexpert.rest.services.Hello;

public class HelloTest 
{
	public static void main(String[] args) 
	{
		/*Hello hello=new Hello();
		// System.out.println(hello.sayPlainTextHello("Fund", "Expert"));
		System.out.println(hello.sayWelcome("Java"));
		
		System.out.println(hSession.getClass());
		hSession.close();
		System.out.println();*/
		Session hSession=HibernateBridge.getSessionFactory().openSession();
		User user=(User)hSession.get(User.class, 13l);
		System.out.println(JSON.toJSONObject(user,false));
		hSession.close();
		///JSONObject json=new JSONObject();
		//json.put("re", "true");
		
	}
}
