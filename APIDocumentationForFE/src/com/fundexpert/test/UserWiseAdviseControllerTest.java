package com.fundexpert.test;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.fundexpert.controller.UserWiseAdvisorController;
import com.fundexpert.controller.UserWiseAdvisorController.Advise;
import com.fundexpert.dao.Holding;

public class UserWiseAdviseControllerTest {

	public static void main(String[] args) {

		UserWiseAdvisorController u=new UserWiseAdvisorController(10l);
		Map<Long,Advise> map=u.getAdvise();
		double perc=u.getInvestmentStyle();
		double n=u.getInvestmentHorizon();
		JSONObject json=new JSONObject(map);
		System.out.println(json.toString(1));
		System.out.println(n);
		
	}

}
