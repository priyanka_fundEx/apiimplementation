package com.fundexpert.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFund;

public class MutualfundTest 
{
	public static void main(String[] args) 
	{
		Session session=HibernateBridge.getSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		
		@SuppressWarnings("unchecked")
		List<MutualFund> list=session.createQuery("from MutualFund").list();
		for(MutualFund mf:list)
		{
			mf.setDirect(false);
			session.update(mf);
			tx.commit();
			System.out.println("Saved.");
		}
	}
}