package com.fundexpert.test;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import com.fundexpert.util.HashKeyGeneration;

public class HashKeyGenerationTest {

	public static void main(String[] args) {

		HashKeyGeneration hkg=new HashKeyGeneration();
		try {
			System.out.println("TEST_APPID_STRING="+hkg.EDELWEISS_TEST_APPID_STRING);
			String appKey=hkg.getAppKey(HashKeyGeneration.EDELWEISS_TEST_APPID_STRING);
			System.out.println("app key generated="+appKey);
			String secretKey=hkg.getSecretKey(appKey);
			System.out.println("secret key generated="+secretKey);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
