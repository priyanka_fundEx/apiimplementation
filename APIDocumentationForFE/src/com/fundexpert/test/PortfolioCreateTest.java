package com.fundexpert.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.Response;
 
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.controller.PortfolioCreateController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.Transaction;

public class PortfolioCreateTest 
{
	public static void main(String[] args) throws ParseException 
	{
		//StringBuilder builder = new StringBuilder();
		String input = "";											// 103196,111644,100848
		try
		{
							// a8a-89f1-6b79bb878b47 for userID 5   // 77a-bcd0-bb3a3fa63855 for userID 3	 	// 1f2-aac3-fb3648d36c7b for userID 6
			input = "{\"appId\": \"qwertyuiop12345\",\"sessionId\": \"ae6-9d22-e\" ,\"userId\": \"7f5-87a9-23059fcbd6ba\",\"transactions\": "+
					"[ {\"folioNumber\": \"10200153/3\",\"amfiiCode\": \"102001\", \"optionType\": \"DIVIDEND PAYOUT\", \"units\": \"200.03\",\"nav\": \"53.02\",\"action\": \"invest\",\"date\": \"2017-05-03\"},"+
					"{\"folioNumber\": \"12882053/3\",\"amfiiCode\": \"128820\", \"optionType\": \"DIVIDEND REINVEST\", \"units\": \"200.03\",\"nav\": \"52.01\",\"action\": \"invest\",\"date\": \"2017-07-03\"},"+
					"{\"folioNumber\": \"12530553/3\",\"amfiiCode\": \"125305\", \"optionType\": \"GROWTH\", \"units\": \"200.03\",\"nav\": \"54.0\",\"action\": \"invest\",\"date\": \"2017-05-03\"},"+
					"{\"folioNumber\": \"10276076/3\", \"amfiiCode\": \"102760\",\"optionType\": \"GROWTH\", \"units\": \"150.43\", \"nav\": \"56.01\", \"action\": \"invest\", \"date\": \"2017-08-27\"}]}";
		
			JSONObject jObject = new JSONObject(input);

			System.out.println("INPUT JSON Object: " + jObject);
		

			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		    SessionController sessionCtrl=new SessionController();
			SubscribeToFundController fundCtrl=new SubscribeToFundController();
			PortfolioCreateController ctrl=new PortfolioCreateController();
		    
			JSONObject jobject=new JSONObject();
			StringBuilder builder=new StringBuilder();
			Long consumerId=null;
			String sessionId=null;
			String appId=null;
			String userId=null; 
			String ipAddress=null;
			JSONArray array=new JSONArray();
			  
				if(jObject.getString("appId")!=null && jObject.getString("sessionId")!=null && jObject.getString("userId")!=null)
				 {
					 appId= jObject.getString("appId");	
				     sessionId = jObject.getString("sessionId");	
				     userId=jObject.getString("userId");
					 array=jObject.getJSONArray("transactions");
				 }
				 else
					 throw new InvalidParameterException("Enter a valid parameter.");

			    ipAddress="127.0.0.1";		    
			    
			    consumerId=sessionCtrl.getClient(appId, ipAddress);
				 if(consumerId!=null )
				    {
				    	if(sessionCtrl.getValidSession(consumerId, sessionId))
				    	{
				    		 if(fundCtrl.validUser(consumerId, userId))
							{
				    			 for(int i=0;i<array.length();i++)
				    			 {
				    			    JSONObject obj=(JSONObject) array.get(i);
				    				if(obj.getString("folioNumber")!=null && obj.getString("amfiiCode")!=null && obj.getString("optionType")!=null && 
				    						obj.getString("units")!=null && obj.getString("nav")!=null && obj.getString("action")!=null && obj.getString("date")!=null )
					    			{	
				    						String folioNumber=obj.getString("folioNumber");
					    			    	String amfiiCode=obj.getString("amfiiCode");
					    			    	int optionType=0;
											if(obj.getString("optionType").equals("GROWTH"))
					    			    		optionType=1;
											else if(obj.getString("optionType").equals("DIVIDEND PAYOUT"))
												optionType=2;
											else if(obj.getString("optionType").equals("DIVIDEND REINVEST"))
												optionType=3;
											else if(obj.getString("optionType").equals("DIVIDEND SWEEP"))
												optionType=4;
		
											
											Double units=Double.parseDouble(obj.getString("units"));
					    			    	Double nav=Double.parseDouble(obj.getString("nav"));
					    			    	String option=obj.getString("action");
					    			    	int action=0;
					    			    	if(option.equalsIgnoreCase("invest"))
					    			    		action=1;
					    			    	else if(option.equalsIgnoreCase("redeem"))
					    			    		action=2;
					    			    
					    			    	Calendar date=Calendar.getInstance();
					    			    	date.setTime(sdf.parse(obj.getString("date")));
					    			    	
					    			    	System.out.println("option type: "+optionType+", userID: "+userId+", Session ID: "+sessionId+", folioNumber: "+folioNumber+", nav: "+nav+", units: "+units);
					    			    	
					    			    	if(!nav.isNaN() && !units.isNaN() )
					    			    	{
						    			    	Long mfId=ctrl.getMFId(amfiiCode, optionType);
						    			    	Long userID=ctrl.getUserId(userId, consumerId);
						    			    		//System.out.print("MFID: "+mfId+",    amfiiCode: "+amfiiCode+",  :  , ");
						    			    	
						    			    	if(ctrl.isHoldingExist(folioNumber, mfId, userID))
						    			    	{
						    			    		 ctrl.updateHolding(folioNumber, mfId, userID, action, units, nav, date.getTime());
			
													jobject.put("success", true);
													System.out.println("Holding Exist! Portfolio Create API invoked Successfully.");
						    			    	}
						    			    	else if(!ctrl.isHoldingExist(folioNumber, mfId, userID))
						    			    	{
						    			    		ctrl.saveHolding(folioNumber, userID, mfId, action, units, nav, date.getTime() );
			
													jobject.put("success", true);
													System.out.println("Holding does not Exist! New Holding Created and Portfolio Create API invoked Successfully.");
						    			    	}
					    			    	}else{
												jobject.put("success", false);
												JSONObject failure=new JSONObject();
												failure.put("message", "Invalid/Empty parameters.");
												failure.put("code", "705");
												jobject.put("error", failure);  
												System.out.println("Portfolio Create API not invoked Successfully.");
											}
				    			    	}else{
											jobject.put("success", false);
											JSONObject failure=new JSONObject();
											failure.put("message", "Invalid/Empty parameters.");
											failure.put("code", "705");
											jobject.put("error", failure);  
											System.out.println("Portfolio Create API not invoked Successfully.");
										}
				    			  } 
							}else{
								jobject.put("success", false);
								JSONObject failure=new JSONObject();
								failure.put("message", "User ID does not exist.");
								failure.put("code", "704");
								jobject.put("error", failure);  
								System.out.println("Portfolio Create API not invoked Successfully.");
							}
				    	}
				    	else
			    		{
				    		jobject.put("success", false);
							JSONObject failure=new JSONObject();
							failure.put("message", "Session expired.");
							failure.put("code", "703");
							jobject.put("error", failure); 
			    			System.out.println("Portfolio Create API Service has not invoked Successfully.   "+jobject.toString());
			    		}
				    }
				    else
		    		{
		    		    jobject.put("success", false);
						JSONObject failure=new JSONObject();
						failure.put("message", "App ID does not exist.");
						failure.put("code", "701");
						jobject.put("error", failure); 
		    			System.out.println("Portfolio Create API Service has not invoked Successfully.   "+jobject.toString());
		    		}			
					//System.out.println("Data Received: " + jObject.toString());
			} catch (Exception e) {
				System.out.println("Error Parsing: - "+e.getMessage());
				e.printStackTrace();
			} 
		 
		
		
		/*String folioNumber="53125305/3";
		Long userID=6l,  mfId=2l;
		int action=2;
		Double units=50.43,  nav=50.0;
		Calendar transactionDate=Calendar.getInstance();

		PortfolioCreateController ctrl=new PortfolioCreateController();
		
		//boolean b=ctrl.updateHolding(folioNumber, mfId, userID, action, units, nav, transactionDate.getTime());
		//System.out.println(b);

		Session session = HibernateBridge.getSessionFactory().openSession();		
		
		List<Holding> list=session.createQuery("from Holding where userId=?").setLong(0, userID).list();
		double pf=0.0, pf1=0.0;
		for(Holding h: list)
		{
			double pf2=0.0;
			pf+=h.getAvgNav()*h.getUnits();
			List<Transaction> li=session.createQuery("from Transaction where holdingId=?").setLong(0, h.getId()).list();
			for(Transaction tr:li)
			{
				pf1+=tr.getAmount();
				pf2+=tr.getNav()*tr.getUnits();
				System.out.println("pf: "+h.getAvgNav()*h.getUnits()+", pf1: "+tr.getNav()*tr.getUnits()+", pf2: "+tr.getAmount());	
			}
		}

		System.out.println("AA: "+pf+", AA: "+pf1);*/
		
		
		
		/*Double amount=10.0;
		 if(amount>0 && !amount.isNaN())
		 {
			 	System.out.println(amount+"  yes  "+!amount.isNaN());
		 }
		 else
			 System.out.println("No");
		 */
		/*Holding holding = (Holding) session.createQuery("from Holding where mutualfundId=? and userId=? and folioNumber=?").setLong(0, mfId).setLong(1, userID).setString(2, folioNumber).setMaxResults(1).uniqueResult();
		if(holding!=null)
			System.out.println(holding+",  not null.");
	    else
	    	System.out.println("holding is null");*/
		
	    /*SimpleDateFormat sdf=new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
	    SessionController sessionCtrl=new SessionController();
		SubscribeToFundController fundCtrl=new SubscribeToFundController();
		PortfolioCreateController ctrl=new PortfolioCreateController();
		
		 String appId= "qwertyuiop12345";
		 String sessionId = "b6c-8f5e-d";
		 String userId ="cc8-a22e-885707b9d5fa";
		 Long  consumerId=sessionCtrl.getClient(appId, "127.0.0.1");
		    
		String folioNumber="54644776/3";
    	String amfiiCode="897654";
    	//String optionType=obj.getString("optionType");
    	Double units=1500.43;
    	Double nav=55.45;
    	Integer action=2;
    	
    	Calendar date=Calendar.getInstance();
    	
    	Long mfId=ctrl.getMFId(amfiiCode);
    	Long userID=ctrl.getUserId(userId, consumerId);
    	System.out.println("userId: "+userID+", mfID: "+mfId);

		Session session=HibernateBridge.getSessionFactory().openSession();
    	Holding holding=(Holding)session.createQuery("from Holding where mutualfundId=? and userId=? and folioNumber=?")
				.setLong(0, mfId).setLong(1, userID).setString(2,folioNumber).uniqueResult();
		if(holding!=null)
		{	
			System.out.println("holding Exist.");
		}
		else
		{	
			System.out.println("holding does not Exist.");
		}
    	
    	if(ctrl.isHoldingExist(folioNumber, mfId, userID))
    	{
    		//ctrl.updateHolding(folioNumber, mfId, userID, action, units, nav, date.getTime());
    		System.out.println("Holding and transaction updated successfully.");
    	}
    	else if(!ctrl.isHoldingExist(folioNumber, mfId, userID))
    	{
    		//ctrl.saveHolding(folioNumber, mfId, userID, action, units, nav, date.getTime());
    		System.out.println("Holding and transaction saved successfully.");
    	} 	*/
	}
}