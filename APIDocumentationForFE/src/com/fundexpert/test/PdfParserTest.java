package com.fundexpert.test;

import java.util.Iterator;
import java.util.List;

import com.fundexpert.controller.PortfolioImportController;
import com.fundexpert.pojo.Holdings;
import com.fundexpert.pojo.Transactions;
import com.fundexpert.util.PdfReaderAndPersist;
import com.fundexpert.util.PdfReaderAndPersist.PdfParser;

public class PdfParserTest {

	public static void main(String[] args) {

		String path = "F:/PdfFiles/edelweiss/BTZPR9426A-Sankalp cams.pdf";
		String pass = "BTZPR9426A";
		try {
			PdfReaderAndPersist prap=new PdfReaderAndPersist();
			PdfReaderAndPersist.PdfParser pdfParser=prap.new PdfParser();
			List<Holdings> holdingsList=pdfParser.getHoldingsList(path, pass);
			Iterator itr = holdingsList.iterator();
			String write = "";
			while (itr.hasNext())
			{
				Holdings h = (Holdings) itr.next();
				/*if(!h.getFolioNumberString().contains("405132896694"))
					continue;*/
				write = write + h.getCamsCode() + "-" + h.getMutualFundName() + "\n" + h.getFolioNumberString() + "\n" + h.getArn() + "\n";

				List<Transactions> transList = h.getTransactionsList();
				Iterator itr1 = transList.iterator();
				while (itr1.hasNext())
				{
					Transactions trans = (Transactions) itr1.next();
					write = write + trans.getDate() + "\t" + trans.getAmount() + "\t" + trans.getUnits() + "\t" + trans.getPrice() + "\n";
				}
				write = write + h.getClosingUnits() + "\n\n\n";
			}
			System.out.println(write);
			
			PortfolioImportController pic=new PortfolioImportController();
			//pic=new PortfolioImportController();
			//pic.sFactory(3l, holdingsList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
