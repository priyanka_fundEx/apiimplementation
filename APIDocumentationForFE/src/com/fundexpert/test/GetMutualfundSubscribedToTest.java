package com.fundexpert.test;

import java.util.ArrayList;
import java.util.List;

import com.fundexpert.controller.GetMFSubscribedToController;
import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;

public class GetMutualfundSubscribedToTest
{
	public static void main(String[] args) {
		SessionController sessionCtrl=new SessionController();
		SubscribeToFundController ctrl=new SubscribeToFundController();
		GetMFSubscribedToController mfCtrl=new GetMFSubscribedToController();

		String appId="qwertyuiop123c45";
		String userId="32e-a067-b28f9dbf8271";
		String ipAddress="127.0.0.1";
		String sessionId="3f3-bb65-8";
		Long consumerId=1l; 
		String amfiiCode="112323";
		boolean result=false;
		
		List<String> list=new ArrayList<String>();
		if(ctrl.validUser(consumerId, userId))
			list=mfCtrl.getMutualfundSubscribedTo(userId);
		System.out.println("List of Subscribed Mutual Funds  :  "+list);
	}
}