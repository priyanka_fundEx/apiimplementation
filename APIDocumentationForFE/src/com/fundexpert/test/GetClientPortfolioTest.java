package com.fundexpert.test;

import java.util.List;

import com.fundexpert.controller.GetClientPortfolioController;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.Transaction;

public class GetClientPortfolioTest 
{
	public static void main(String[] args) 
	{
		GetClientPortfolioController ctrl=new GetClientPortfolioController();

		String appId= "qwertyuiop12345";
		String sessionId = "7a0-a8ec-7";
		String userId ="32e-a067-b28f9dbf8271";
		Long consumerId=1l;
		Long userID=ctrl.getUserId(userId, consumerId);
		System.out.println(userID);
		List<Holding> list=ctrl.getListOfHoldings(userID);
		for(Holding h:list)
		{
			List<Transaction> tr=ctrl.getTransactions(h);
			System.out.println(tr);
		}
		
		System.out.println(list);
	}
}
