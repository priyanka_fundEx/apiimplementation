package com.fundexpert.test;

import java.util.List;




import com.fundexpert.controller.StocksPortfolioImportController;
import com.fundexpert.pojo.StocksHoldings;
import com.fundexpert.util.PdfReaderAndPersist;
import com.fundexpert.util.PdfReaderAndPersist.StockPdfParser;

public class StockPdfParserTest {

	public static void main(String[] args) {

		try {
			PdfReaderAndPersist p=new PdfReaderAndPersist();
			StockPdfParser spp=p.new StockPdfParser("F:/Equity PDF/edelweiss/AULPK7117R-CDSL_Rishi.pdf", "AULPK7117R");
			List<StocksHoldings> l=spp.getStocksHoldingsList();
			
			if(l!=null)
			{
				for(int i=0;i<l.size();i++)
				{
					StocksHoldings sh=l.get(i);
					System.out.println(sh.getIsinCode()+" "+sh.getDematName()+" "+sh.getNumberOfShares()+" "+sh.getCompanyName());
				}
			}
			System.out.println("No of stocks = "+l.size());
			StocksPortfolioImportController stocksPortfolioImportController = new StocksPortfolioImportController();
			stocksPortfolioImportController.sFactory(8l, l);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
