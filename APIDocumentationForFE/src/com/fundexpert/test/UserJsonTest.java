package com.fundexpert.test;

import java.util.List;

import org.hibernate.Session;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.apidoc.util.JSON;
import com.fundexpert.dao.Holding;
import com.fundexpert.dao.User;

public class UserJsonTest {

	public static void main(String[] args) {

		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			User u=(User)hSession.createQuery("from User where id=?").setLong(0, 10).uniqueResult();
			List<Holding> hl=hSession.createQuery("from Holding where userId=?").setLong(0, u.getId()).list();
			JSONObject j=JSON.toJSONObject(u,true);
			System.out.println("JSON="+j.toString(1));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

}
