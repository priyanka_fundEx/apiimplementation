package com.fundexpert.test;

import java.text.ParseException;

import com.fundexpert.controller.SessionController;
import com.fundexpert.controller.SubscribeToFundController;

public class SubscribeToFundTest 
{
	public static void main(String[] args) throws ParseException 
	{
		SessionController sessionCtrl=new SessionController();
		SubscribeToFundController ctrl=new SubscribeToFundController();

		String appId="qwertyuiop12345";
		String userId="32e-a067-b28f9dbf8271";
		String ipAddress="127.0.0.1";
		String sessionId="3f3-bb65-8";
		Long consumerId=1l; 
		String amfiiCode="112323";
		boolean result=false;
		
		if(ctrl.validUser(consumerId, userId))
			result=ctrl.subscribeToFund(userId, amfiiCode);
		System.out.println("User Saved  :  "+result);
	}
}