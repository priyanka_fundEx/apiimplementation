package com.fundexpert.test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.Consumer1;
import com.fundexpert.dao.User1;

public class CipherTest {

	static Cipher cipher=null;
	public static void main(String[] args) {
		String data=null;
		CipherTest test=new CipherTest();
		String key = "Bar12345Bar91n38"; // 128 bit key
		Session hSession=null;
		Transaction tx=null;
		try {
			cipher=Cipher.getInstance("AES");
			Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
			hSession=HibernateBridge.getSessionFactory().openSession();
			//to generate Consumer1 table uncomment below comments
		/*	tx=hSession.beginTransaction();
			int count=hSession.createSQLQuery("Insert into Consumer1 select * from Consumer").executeUpdate();
			System.out.println("No. of rows inserted into consumer1 = "+count);
			tx.commit();
			tx=hSession.beginTransaction();
			List<Consumer1> consumer1List=hSession.createQuery("from Consumer1").list();
			Iterator itr=consumer1List.iterator();
			while(itr.hasNext())
			{
				Consumer1 consumer=(Consumer1)itr.next();
				String secret=consumer.getSecretKey();
				String encryptedSecret=test.encryptData(secret, aesKey);
				System.out.println("Secret key = "+encryptedSecret);
				consumer.setSecretKey(encryptedSecret);
				hSession.save(consumer);
			}
			tx.commit();
			consumer1List=hSession.createQuery("from Consumer1").list();
			itr=consumer1List.iterator();
			while(itr.hasNext())
			{
				Consumer1 consumer=(Consumer1)itr.next();
				String secret=consumer.getSecretKey();
				String actualSecretKey=test.decrypt(secret, aesKey);
				System.out.println("Actual Passwrod = "+actualSecretKey);
			}*/
			//to generate User1 table uncomment below comments
			tx=hSession.beginTransaction();
			int count=hSession.createSQLQuery("insert into User1 select * from User").executeUpdate();
			tx.commit();
			tx=hSession.beginTransaction();
			List<User1> list=hSession.createQuery("from User1").list();
			Iterator itr=list.iterator();
			while(itr.hasNext())
			{
				User1 user=(User1)itr.next();
				String email = user.getEmail();
				String pan=user.getPan();
				String encryptedEmail=test.encryptData(email, aesKey);
				String encryptedPan=test.encryptData(pan, aesKey);
				user.setEmail(encryptedEmail);
				user.setPan(encryptedPan);
				hSession.save(user);
			}
			tx.commit();
			list=hSession.createQuery("from User1").list();
			itr=list.iterator();
			while(itr.hasNext())
			{
				User1 user=(User1)itr.next();
				String email = user.getEmail();
				String pan = user.getPan();
				String decryptedEmail=test.decrypt(email, aesKey);
				String decryptedPan=test.decrypt(pan, aesKey);
				System.out.println("Decrypted Email id ="+decryptedEmail +" decryptedpan ="+decryptedPan);
			}
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		
		
	}
	String encryptData(String data,Key secretKey)
	{
		try {
			
			
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] byteData=data.getBytes();
			byte[] cipheredBytes=cipher.doFinal(byteData);
			String d=new sun.misc.BASE64Encoder().encode(cipheredBytes);
			
			return d;
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	String decrypt(String encryptedString,Key secretKey)
	{
		
		try {
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			byte[] encryptedByte=new sun.misc.BASE64Decoder().decodeBuffer(encryptedString);	
			byte[] decryptedByte=cipher.doFinal(encryptedByte);
			String decryptedString=new String(decryptedByte);
			return decryptedString;
			
		}catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
