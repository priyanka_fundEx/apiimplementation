package com.fundexpert.pojo;

public class StocksHoldings {

	String isinCode,scName,companyName,dematId,dematName;
	double numberOfShares,marketPrice;
	
	/**
	 * @return the isinCode
	 */
	public String getIsinCode() {
		return isinCode;
	}
	/**
	 * @param isinCode the isinCode to set
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	/**
	 * @return the numberOfShares
	 */
	public double getNumberOfShares() {
		return numberOfShares;
	}
	/**
	 * @param numberOfShares the numberOfShares to set
	 */
	public void setNumberOfShares(double numberOfShares) {
		this.numberOfShares = numberOfShares;
	}
	/**
	 * @return the marketPrice
	 */
	public double getMarketPrice() {
		return marketPrice;
	}
	/**
	 * @param marketPrice the marketPrice to set
	 */
	public void setMarketPrice(double marketPrice) {
		this.marketPrice = marketPrice;
	}
	/**
	 * @return the scName
	 */
	public String getScName() {
		return scName;
	}
	/**
	 * @param scName the scName to set
	 */
	public void setScName(String scName) {
		this.scName = scName;
	}
	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getDematId() {
		return dematId;
	}
	
	public void setDematId(String dematId) {
		this.dematId = dematId;
	}
	public String getDematName() {
		return dematName;
	}
	public void setDematName(String dematName) {
		this.dematName = dematName;
	}
	
	
}
