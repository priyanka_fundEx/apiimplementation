package com.fundexpert.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fundexpert.dao.SIPDate;

public class MutualFund implements Serializable
{
	private static final long serialVersionUID = 1L;
	private int timeframe, lockInPeriod;
	private long idPojo, houseId;
	private String name, schemeCode, type, optionType, schemeType, comment, roboSchemeCode, settlementType, isin, rtaSchemeCode, transactionMode;
	private double nav, navChange, lastYearNav;
	private boolean roboSuggested, flexiSuggested, safeSuggested, active, validated, nonAofGrowthSuggested;
	private Date navUpdatedOn, roboSuggestedOn, flexiSuggestedOn, safeSuggestedOn, addedOn, startDate, endDate;
	private double oneYearReturns, threeYearReturns, fiveYearReturns, overallReturns, sharpeRatio, stdDeviation, beta, alpha, rSquaredValue, upsideCaptureRatio, downsideCaptureRatio, vroRating, ourRating, maxPurchaseAmount;
	Boolean nfoFlag, redemptionAllowed;
	Double minSIPAmount, maxSIPAmount, high52Week, low52Week;
	//removed Fundhouse from here but not from hbm//FundHouse fundhouse;
	MutualFund liquidFund;
	Long liquidFundId;
	MutualFund growthFund;
	Long growthFundId;
	Boolean roboAvailable, taxSuggested, growthSuggested, purchaseAllowed, dematPurchaseAllowed, physicalPurchaseAllowed, stpAllowed, swpAllowed, switchAllowed;
	private boolean sipFlag;
	private double minPurchase, minAdditionalPurchase, purchaseMultiple, maxPurchase, minRedemptionAmt, minRedemptionQty;
	Set<SIPDate> sipDatesForPojo=new HashSet<SIPDate>();
	Integer minNoOfMonths, maxNoOfMonths;

	String camsSchemeCode;
	Long amfiiCode;
	//removed list of SIPDates
	String benchmarkIndex;
	String openEnded;
	Double schemeSize, exitLoad, entryLoad;
	
	String fundManager;
	
	//column went missing when compare with table now added as below
	boolean direct;
	String planType;
	MutualFund regularFund;
	Long regularFundId;
	Date updatedOn;
	
	String bseSchemeType, rtaAgentCode, dividendReinvestmentFlag;
	Double ytm, modifiedDurationInYears, technicalScore, personalScore;
	
	Long morningStarAnalystRating;
	Long morningStarRating;
	Boolean sipAllowed;
	Long minGap,maxGap;
	Double expenseRatio;
	
	String broaderSchemeType,broaderType;
	
	/**
	 * @return the switchAllowed
	 */
	public Boolean getSwitchAllowed() {
		return switchAllowed;
	}

	/**
	 * @param switchAllowed the switchAllowed to set
	 */
	public void setSwitchAllowed(Boolean switchAllowed) {
		this.switchAllowed = switchAllowed;
	}

	/**
	 * @return the regularFund
	 */
	public MutualFund getRegularFund() {
		return regularFund;
	}

	/**
	 * @param regularFund the regularFund to set
	 */
	public void setRegularFund(MutualFund regularFund) {
		this.regularFund = regularFund;
	}

	/**
	 * @param regularFundId the regularFundId to set
	 */
	public void setRegularFundId(Long regularFundId) {
		this.regularFundId = regularFundId;
	}
	
	

	public Boolean getRedemptionAllowed() {
		return redemptionAllowed;
	}

	public void setRedemptionAllowed(Boolean redemptionAllowed) {
		this.redemptionAllowed = redemptionAllowed;
	}

	public Boolean getNfoFlag() {
		return nfoFlag;
	}

	public Boolean isNfoFlag() {
		return nfoFlag;
	}

	public void setNfoFlag(Boolean nfoFlag) {
		this.nfoFlag = nfoFlag;
	}

	/**
	 * @return the swpAllowed
	 */
	public Boolean getSwpAllowed() {
		return swpAllowed;
	}

	/**
	 * @param swpAllowed the swpAllowed to set
	 */
	public void setSwpAllowed(Boolean swpAllowed) {
		this.swpAllowed = swpAllowed;
	}

	/**
	 * @return the stpAllowed
	 */
	public Boolean getStpAllowed() {
		return stpAllowed;
	}

	/**
	 * @param stpAllowed the stpAllowed to set
	 */
	public void setStpAllowed(Boolean stpAllowed) {
		this.stpAllowed = stpAllowed;
	}

	/**
	 * @return the high52Week
	 */
	public Double getHigh52Week()
	{
		return high52Week;
	}

	/**
	 * @param high52Week the high52Week to set
	 */
	public void setHigh52Week(Double high52Week)
	{
		this.high52Week = high52Week;
	}

	/**
	 * @return the low52Week
	 */
	public Double getLow52Week()
	{
		return low52Week;
	}

	/**
	 * @param low52Week the low52Week to set
	 */
	public void setLow52Week(Double low52Week)
	{
		this.low52Week = low52Week;
	}

	public boolean isNonAofGrowthSuggested()
	{
		return nonAofGrowthSuggested;
	}

	public void setNonAofGrowthSuggested(boolean nonAofGrowthSuggested)
	{
		this.nonAofGrowthSuggested = nonAofGrowthSuggested;
	}

	/**
	 * @return the amfiiCode
	 */
	public Long getAmfiiCode()
	{
		return amfiiCode;
	}

	/**
	 * @param amfiiCode the amfiiCode to set
	 */
	public void setAmfiiCode(Long amfiiCode)
	{
		this.amfiiCode = amfiiCode;
	}

	/**
	 * @return the transactionMode
	 */
	public String getTransactionMode()
	{
		return transactionMode;
	}

	/**
	 * @param transactionMode the transactionMode to set
	 */
	public void setTransactionMode(String transactionMode)
	{
		this.transactionMode = transactionMode;
	}

	/**
	 * @return the purchaseAllowed
	 */
	public Boolean getPurchaseAllowed()
	{
		return purchaseAllowed;
	}

	/**
	 * @param purchaseAllowed the purchaseAllowed to set
	 */
	public void setPurchaseAllowed(Boolean purchaseAllowed)
	{
		this.purchaseAllowed = purchaseAllowed;
	}

	/**
	 * @return the dematPurchaseAllowed
	 */
	public Boolean getDematPurchaseAllowed()
	{
		return dematPurchaseAllowed;
	}

	/**
	 * @param dematPurchaseAllowed the dematPurchaseAllowed to set
	 */
	public void setDematPurchaseAllowed(Boolean dematPurchaseAllowed)
	{
		this.dematPurchaseAllowed = dematPurchaseAllowed;
	}

	/**
	 * @return the physicalPurchaseAllowed
	 */
	public Boolean getPhysicalPurchaseAllowed()
	{
		return physicalPurchaseAllowed;
	}

	/**
	 * @param physicalPurchaseAllowed the physicalPurchaseAllowed to set
	 */
	public void setPhysicalPurchaseAllowed(Boolean physicalPurchaseAllowed)
	{
		this.physicalPurchaseAllowed = physicalPurchaseAllowed;
	}

	/**
	 * @return the rtaSchemeCode
	 */
	public String getRtaSchemeCode()
	{
		return rtaSchemeCode;
	}

	/**
	 * @param rtaSchemeCode the rtaSchemeCode to set
	 */
	public void setRtaSchemeCode(String rtaSchemeCode)
	{
		this.rtaSchemeCode = rtaSchemeCode;
	}

	/**
	 * @return the maxPurchaseAmount
	 */
	public double getMaxPurchaseAmount()
	{
		return maxPurchaseAmount;
	}

	/**
	 * @param maxPurchaseAmount the maxPurchaseAmount to set
	 */
	public void setMaxPurchaseAmount(double maxPurchaseAmount)
	{
		this.maxPurchaseAmount = maxPurchaseAmount;
	}

	/**
	 * @return the isin
	 */
	public String getIsin()
	{
		return isin;
	}

	public String getCamsSchemeCode()
	{
		return camsSchemeCode;
	}

	public void setCamsSchemeCode(String camsSchemeCode)
	{
		this.camsSchemeCode = camsSchemeCode;
	}

	/**
	 * @param isin the isin to set
	 */
	public void setIsin(String isin)
	{
		this.isin = isin;
	}

	/**
	 * @return the validated
	 */
	public boolean isValidated()
	{
		return validated;
	}

	/**
	 * @param validated the validated to set
	 */
	public void setValidated(boolean validated)
	{
		this.validated = validated;
	}


	/**
	 * @return the growthFund
	 */
	public MutualFund getGrowthFund()
	{
		return growthFund;
	}

	/**
	 * @param growthFund the growthFund to set
	 */
	public void setGrowthFund(MutualFund growthFund)
	{
		this.growthFund = growthFund;
	}

	/**
	 * @return the growthFundId
	 */
	public Long getGrowthFundId()
	{
		return growthFundId;
	}

	/**
	 * @param growthFundId the growthFundId to set
	 */
	public void setGrowthFundId(Long growthFundId)
	{
		this.growthFundId = growthFundId;
	}

	/**
	 * @return the lastYearNav
	 */
	public double getLastYearNav()
	{
		return lastYearNav;
	}

	/**
	 * @param lastYearNav the lastYearNav to set
	 */
	public void setLastYearNav(double lastYearNav)
	{
		this.lastYearNav = lastYearNav;
	}

	/**
	 * @return the growthSuggested
	 */
	public Boolean getGrowthSuggested()
	{
		return growthSuggested;
	}

	/**
	 * @param growthSuggested the growthSuggested to set
	 */
	public void setGrowthSuggested(Boolean growthSuggested)
	{
		this.growthSuggested = growthSuggested;
	}

	/**
	 * @return the taxSuggested
	 */
	public Boolean getTaxSuggested()
	{
		return taxSuggested;
	}

	/**
	 * @param taxSuggested the taxSuggested to set
	 */
	public void setTaxSuggested(Boolean taxSuggested)
	{
		this.taxSuggested = taxSuggested;
	}

	/**
	 * @return the minNoOfMonths
	 */
	public Integer getMinNoOfMonths()
	{
		return minNoOfMonths;
	}

	/**
	 * @param minNoOfMonths the minNoOfMonths to set
	 */
	public void setMinNoOfMonths(Integer minNoOfMonths)
	{
		this.minNoOfMonths = minNoOfMonths;
	}

	/**
	 * @return the maxNoOfMonths
	 */
	public Integer getMaxNoOfMonths()
	{
		return maxNoOfMonths;
	}

	/**
	 * @param maxNoOfMonths the maxNoOfMonths to set
	 */
	public void setMaxNoOfMonths(Integer maxNoOfMonths)
	{
		this.maxNoOfMonths = maxNoOfMonths;
	}

	/**
	 * @return the minSIPAmount
	 */
	public Double getMinSIPAmount()
	{
		return minSIPAmount;
	}

	/**
	 * @param minSIPAmount the minSIPAmount to set
	 */
	public void setMinSIPAmount(Double minSIPAmount)
	{
		this.minSIPAmount = minSIPAmount;
	}

	/**
	 * @return the maxSIPAmount
	 */
	public Double getMaxSIPAmount()
	{
		return maxSIPAmount;
	}

	/**
	 * @param maxSIPAmount the maxSIPAmount to set
	 */
	public void setMaxSIPAmount(Double maxSIPAmount)
	{
		this.maxSIPAmount = maxSIPAmount;
	}

	/**
	 * @return the roboAvailable
	 */
	public Boolean getRoboAvailable()
	{
		return roboAvailable;
	}

	/**
	 * @param roboAvailable the roboAvailable to set
	 */
	public void setRoboAvailable(Boolean roboAvailable)
	{
		this.roboAvailable = roboAvailable;
	}

	/**
	 * @return the liquidFund
	 */
	public MutualFund getLiquidFund()
	{
		return liquidFund;
	}

	/**
	 * @param liquidFund the liquidFund to set
	 */
	public void setLiquidFund(MutualFund liquidFund)
	{
		this.liquidFund = liquidFund;
	}

	/**
	 * @return the liquidFundId
	 */
	public Long getLiquidFundId()
	{
		return liquidFundId;
	}

	/**
	 * @param liquidFundId the liquidFundId to set
	 */
	public void setLiquidFundId(Long liquidFundId)
	{
		this.liquidFundId = liquidFundId;
	}

	/**
	 * @return the lockInPeriod
	 */
	public int getLockInPeriod()
	{
		return lockInPeriod;
	}

	/**
	 * @param lockInPeriod the lockInPeriod to set
	 */
	public void setLockInPeriod(int lockInPeriod)
	{
		this.lockInPeriod = lockInPeriod;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate()
	{
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate()
	{
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	/**
	 * @return the timeframe
	 */
	public int getTimeframe()
	{
		return timeframe;
	}

	/**
	 * @param timeframe the timeframe to set
	 */
	public void setTimeframe(int timeframe)
	{
		this.timeframe = timeframe;
	}

	/**
	 * @return the settlementType
	 */
	public String getSettlementType()
	{
		return settlementType;
	}

	/**
	 * @param settlementType the settlementType to set
	 */
	public void setSettlementType(String settlementType)
	{
		this.settlementType = settlementType;
	}

	/**
	 * @return the sipFlag
	 */
	public boolean isSipFlag()
	{
		return sipFlag;
	}

	/**
	 * @param sipFlag the sipFlag to set
	 */
	public void setSipFlag(boolean sipFlag)
	{
		this.sipFlag = sipFlag;
	}

	/**
	 * @return the minPurchase
	 */
	public double getMinPurchase()
	{
		return minPurchase;
	}

	/**
	 * @param minPurchase the minPurchase to set
	 */
	public void setMinPurchase(double minPurchase)
	{
		this.minPurchase = minPurchase;
	}

	/**
	 * @return the minAdditionalPurchase
	 */
	public double getMinAdditionalPurchase()
	{
		return minAdditionalPurchase;
	}

	/**
	 * @param minAdditionalPurchase the minAdditionalPurchase to set
	 */
	public void setMinAdditionalPurchase(double minAdditionalPurchase)
	{
		this.minAdditionalPurchase = minAdditionalPurchase;
	}

	/**
	 * @return the purchaseMultiple
	 */
	public double getPurchaseMultiple()
	{
		return purchaseMultiple;
	}

	/**
	 * @param purchaseMultiple the purchaseMultiple to set
	 */
	public void setPurchaseMultiple(double purchaseMultiple)
	{
		this.purchaseMultiple = purchaseMultiple;
	}

	/**
	 * @return the maxPurchase
	 */
	public double getMaxPurchase()
	{
		return maxPurchase;
	}

	/**
	 * @param maxPurchase the maxPurchase to set
	 */
	public void setMaxPurchase(double maxPurchase)
	{
		this.maxPurchase = maxPurchase;
	}

	/**
	 * @return the minRedemptionAmt
	 */
	public double getMinRedemptionAmt()
	{
		return minRedemptionAmt;
	}

	/**
	 * @param minRedemptionAmt the minRedemptionAmt to set
	 */
	public void setMinRedemptionAmt(double minRedemptionAmt)
	{
		this.minRedemptionAmt = minRedemptionAmt;
	}

	/**
	 * @return the minRedemptionQty
	 */
	public double getMinRedemptionQty()
	{
		return minRedemptionQty;
	}

	/**
	 * @param minRedemptionQty the minRedemptionQty to set
	 */
	public void setMinRedemptionQty(double minRedemptionQty)
	{
		this.minRedemptionQty = minRedemptionQty;
	}

	/**
	 * @return the redemptionSchemeCode
	 */
	public String getRoboSchemeCode()
	{
		return roboSchemeCode;
	}

	/**
	 * @param redemptionSchemeCode the redemptionSchemeCode to set
	 */
	public void setRoboSchemeCode(String roboSchemeCode)
	{
		this.roboSchemeCode = roboSchemeCode;
	}

	/**
	 * @return the comment
	 */
	public String getComment()
	{
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment)
	{
		this.comment = comment;
	}

	/**
	 * @return the roboSuggested
	 */
	public boolean isRoboSuggested()
	{
		return roboSuggested;
	}

	/**
	 * @param roboSuggested the roboSuggested to set
	 */
	public void setRoboSuggested(boolean roboSuggested)
	{
		this.roboSuggested = roboSuggested;
	}

	/**
	 * @return the flexiSuggested
	 */
	public boolean isFlexiSuggested()
	{
		return flexiSuggested;
	}

	/**
	 * @param flexiSuggested the flexiSuggested to set
	 */
	public void setFlexiSuggested(boolean flexiSuggested)
	{
		this.flexiSuggested = flexiSuggested;
	}

	/**
	 * @return the safeSuggested
	 */
	public boolean isSafeSuggested()
	{
		return safeSuggested;
	}

	/**
	 * @param safeSuggested the safeSuggested to set
	 */
	public void setSafeSuggested(boolean safeSuggested)
	{
		this.safeSuggested = safeSuggested;
	}

	/**
	 * @return the roboSuggestedOn
	 */
	public Date getRoboSuggestedOn()
	{
		return roboSuggestedOn;
	}

	/**
	 * @param roboSuggestedOn the roboSuggestedOn to set
	 */
	public void setRoboSuggestedOn(Date roboSuggestedOn)
	{
		this.roboSuggestedOn = roboSuggestedOn;
	}

	/**
	 * @return the flexiSuggestedOn
	 */
	public Date getFlexiSuggestedOn()
	{
		return flexiSuggestedOn;
	}

	/**
	 * @param flexiSuggestedOn the flexiSuggestedOn to set
	 */
	public void setFlexiSuggestedOn(Date flexiSuggestedOn)
	{
		this.flexiSuggestedOn = flexiSuggestedOn;
	}

	/**
	 * @return the safeSuggestedOn
	 */
	public Date getSafeSuggestedOn()
	{
		return safeSuggestedOn;
	}

	/**
	 * @param safeSuggestedOn the safeSuggestedOn to set
	 */
	public void setSafeSuggestedOn(Date safeSuggestedOn)
	{
		this.safeSuggestedOn = safeSuggestedOn;
	}

	/**
	 * @return the oneYearReturns
	 */
	public double getOneYearReturns()
	{
		return oneYearReturns;
	}

	/**
	 * @param oneYearReturns the oneYearReturns to set
	 */
	public void setOneYearReturns(double oneYearReturns)
	{
		this.oneYearReturns = oneYearReturns;
	}

	/**
	 * @return the threeYearReturns
	 */
	public double getThreeYearReturns()
	{
		return threeYearReturns;
	}

	/**
	 * @param threeYearReturns the threeYearReturns to set
	 */
	public void setThreeYearReturns(double threeYearReturns)
	{
		this.threeYearReturns = threeYearReturns;
	}

	/**
	 * @return the fiveYearReturns
	 */
	public double getFiveYearReturns()
	{
		return fiveYearReturns;
	}

	/**
	 * @param fiveYearReturns the fiveYearReturns to set
	 */
	public void setFiveYearReturns(double fiveYearReturns)
	{
		this.fiveYearReturns = fiveYearReturns;
	}

	/**
	 * @return the overallReturns
	 */
	public double getOverallReturns()
	{
		return overallReturns;
	}

	/**
	 * @param overallReturns the overallReturns to set
	 */
	public void setOverallReturns(double overallReturns)
	{
		this.overallReturns = overallReturns;
	}

	/**
	 * @return the sharpeRatio
	 */
	public double getSharpeRatio()
	{
		return sharpeRatio;
	}

	/**
	 * @param sharpeRatio the sharpeRatio to set
	 */
	public void setSharpeRatio(double sharpeRatio)
	{
		this.sharpeRatio = sharpeRatio;
	}

	/**
	 * @return the stdDeviation
	 */
	public double getStdDeviation()
	{
		return stdDeviation;
	}

	/**
	 * @param stdDeviation the stdDeviation to set
	 */
	public void setStdDeviation(double stdDeviation)
	{
		this.stdDeviation = stdDeviation;
	}

	/**
	 * @return the beta
	 */
	public double getBeta()
	{
		return beta;
	}

	/**
	 * @param beta the beta to set
	 */
	public void setBeta(double beta)
	{
		this.beta = beta;
	}

	/**
	 * @return the alpha
	 */
	public double getAlpha()
	{
		return alpha;
	}

	/**
	 * @param alpha the alpha to set
	 */
	public void setAlpha(double alpha)
	{
		this.alpha = alpha;
	}

	/**
	 * @return the rSquaredValue
	 */
	public double getrSquaredValue()
	{
		return rSquaredValue;
	}

	/**
	 * @param rSquaredValue the rSquaredValue to set
	 */
	public void setrSquaredValue(double rSquaredValue)
	{
		this.rSquaredValue = rSquaredValue;
	}

	/**
	 * @return the upsideCaptureRatio
	 */
	public double getUpsideCaptureRatio()
	{
		return upsideCaptureRatio;
	}

	/**
	 * @param upsideCaptureRatio the upsideCaptureRatio to set
	 */
	public void setUpsideCaptureRatio(double upsideCaptureRatio)
	{
		this.upsideCaptureRatio = upsideCaptureRatio;
	}

	/**
	 * @return the downsideCaptureRatio
	 */
	public double getDownsideCaptureRatio()
	{
		return downsideCaptureRatio;
	}

	/**
	 * @param downsideCaptureRatio the downsideCaptureRatio to set
	 */
	public void setDownsideCaptureRatio(double downsideCaptureRatio)
	{
		this.downsideCaptureRatio = downsideCaptureRatio;
	}

	/**
	 * @return the vroRating
	 */
	public double getVroRating()
	{
		return vroRating;
	}

	/**
	 * @param vroRating the vroRating to set
	 */
	public void setVroRating(double vroRating)
	{
		this.vroRating = vroRating;
	}

	/**
	 * @return the ourRating
	 */
	public double getOurRating()
	{
		return ourRating;
	}

	/**
	 * @param ourRating the ourRating to set
	 */
	public void setOurRating(double ourRating)
	{
		this.ourRating = ourRating;
	}

	/**
	 * @return the change
	 */
	public double getNavChange()
	{
		return navChange;
	}

	/**
	 * @param change the change to set
	 */
	public void setNavChange(double navChange)
	{
		this.navChange = navChange;
	}

	/**
	 * @return the id
	 */

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the houseId
	 */
	public long getHouseId()
	{
		return houseId;
	}

	/**
	 * @param houseId the houseId to set
	 */
	public void setHouseId(long houseId)
	{
		this.houseId = houseId;
	}

	/**
	 * @return the schemeCode
	 */
	public String getSchemeCode()
	{
		return schemeCode;
	}

	/**
	 * @param schemeCode the schemeCode to set
	 */
	public void setSchemeCode(String schemeCode)
	{
		this.schemeCode = schemeCode;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @return the optionType
	 */
	public String getOptionType()
	{
		return optionType;
	}

	/**
	 * @param optionType the optionType to set
	 */
	public void setOptionType(String optionType)
	{
		this.optionType = optionType;
	}

	/**
	 * @return the schemeType
	 */
	public String getSchemeType()
	{
		return schemeType;
	}

	/**
	 * @param schemeType the schemeType to set
	 */
	public void setSchemeType(String schemeType)
	{
		this.schemeType = schemeType;
	}

	/**
	 * @return the nav
	 */
	public double getNav()
	{
		return nav;
	}

	/**
	 * @param nav the nav to set
	 */
	public void setNav(double nav)
	{
		this.nav = nav;
	}

	/**
	 * @return the active
	 */
	public boolean isActive()
	{
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active)
	{
		this.active = active;
	}

	/**
	 * @return the navUpdatedOn
	 */
	public Date getNavUpdatedOn()
	{
		return navUpdatedOn;
	}

	/**
	 * @param navUpdatedOn the navUpdatedOn to set
	 */
	public void setNavUpdatedOn(Date navUpdatedOn)
	{
		this.navUpdatedOn = navUpdatedOn;
	}

	/**
	 * @return the addedOn
	 */
	public Date getAddedOn()
	{
		return addedOn;
	}

	/**
	 * @return the regularFundId
	 */
	public Long getRegularFundId() {
		return regularFundId;
	}

	/**
	 * @param addedOn the addedOn to set
	 */
	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public String getBenchmarkIndex()
	{
		return benchmarkIndex;
	}

	public void setBenchmarkIndex(String benchmarkIndex)
	{
		this.benchmarkIndex = benchmarkIndex;
	}

	public String getOpenEnded()
	{
		return openEnded;
	}

	public void setOpenEnded(String openEnded)
	{
		this.openEnded = openEnded;
	}

	public Double getSchemeSize()
	{
		return schemeSize;
	}

	public void setSchemeSize(Double schemeSize)
	{
		this.schemeSize = schemeSize;
	}

	public Double getExitLoad()
	{
		return exitLoad;
	}

	public void setExitLoad(Double exitLoad)
	{
		this.exitLoad = exitLoad;
	}

	public Double getEntryLoad()
	{
		return entryLoad;
	}

	public void setEntryLoad(Double entryLoad)
	{
		this.entryLoad = entryLoad;
	}

	public String getFundManager() {
		return fundManager;
	}

	public void setFundManager(String fundManager) {
		this.fundManager = fundManager;
	}

	public boolean isDirect() {
		return direct;
	}

	public void setDirect(boolean direct) {
		this.direct = direct;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	
	public Set<SIPDate> getSipDatesForPojo() {
		return sipDatesForPojo;
	}

	public void setSipDatesForPojo(Set<SIPDate> sipDatesForPojo) {
		this.sipDatesForPojo = sipDatesForPojo;
	}

	public String getBseSchemeType() {
		return bseSchemeType;
	}

	public void setBseSchemeType(String bseSchemeType) {
		this.bseSchemeType = bseSchemeType;
	}

	public String getRtaAgentCode() {
		return rtaAgentCode;
	}

	public void setRtaAgentCode(String rtaAgentCode) {
		this.rtaAgentCode = rtaAgentCode;
	}

	public String getDividendReinvestmentFlag() {
		return dividendReinvestmentFlag;
	}

	public void setDividendReinvestmentFlag(String dividendReinvestmentFlag) {
		this.dividendReinvestmentFlag = dividendReinvestmentFlag;
	}

	public Double getYtm() {
		return ytm;
	}

	public void setYtm(Double ytm) {
		this.ytm = ytm;
	}

	public Double getModifiedDurationInYears() {
		return modifiedDurationInYears;
	}

	public void setModifiedDurationInYears(Double modifiedDurationInYears) {
		this.modifiedDurationInYears = modifiedDurationInYears;
	}

	public Double getTechnicalScore() {
		return technicalScore;
	}

	public void setTechnicalScore(Double technicalScore) {
		this.technicalScore = technicalScore;
	}

	public Double getPersonalScore() {
		return personalScore;
	}

	public void setPersonalScore(Double personalScore) {
		this.personalScore = personalScore;
	}

	public Long getMorningStarAnalystRating() {
		return morningStarAnalystRating;
	}

	public void setMorningStarAnalystRating(Long morningStarAnalystRating) {
		this.morningStarAnalystRating = morningStarAnalystRating;
	}

	public Long getMorningStarRating() {
		return morningStarRating;
	}

	public void setMorningStarRating(Long morningStarRating) {
		this.morningStarRating = morningStarRating;
	}

	public Boolean getSipAllowed() {
		return sipAllowed;
	}

	public void setSipAllowed(Boolean sipAllowed) {
		this.sipAllowed = sipAllowed;
	}

	public Long getMinGap() {
		return minGap;
	}

	public void setMinGap(Long minGap) {
		this.minGap = minGap;
	}

	public Long getMaxGap() {
		return maxGap;
	}

	public void setMaxGap(Long maxGap) {
		this.maxGap = maxGap;
	}

	public Double getExpenseRatio() {
		return expenseRatio;
	}

	public void setExpenseRatio(Double expenseRatio) {
		this.expenseRatio = expenseRatio;
	}

	public String getBroaderSchemeType() {
		return broaderSchemeType;
	}

	public String getBroaderType() {
		return broaderType;
	}

	public void setBroaderSchemeType(String broaderSchemeType) {
		this.broaderSchemeType = broaderSchemeType;
	}

	public void setBroaderType(String broaderType) {
		this.broaderType = broaderType;
	}

	public long getIdPojo() {
		return idPojo;
	}

	public void setIdPojo(long idPojo) {
		this.idPojo = idPojo;
	}
	
	
}
