package com.fundexpert.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.config.Config;
import com.fundexpert.controller.PortfolioImportController;
import com.fundexpert.controller.StocksPortfolioImportController;
import com.fundexpert.controller.UserPortfolioStateController;
import com.fundexpert.dao.ArchiveApiRequests;
import com.fundexpert.dao.Consumer;
import com.fundexpert.dao.NoUserFound;
import com.fundexpert.dao.PortfolioRequest;
import com.fundexpert.dao.PortfolioUpload;
import com.fundexpert.dao.User;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.exception.ValidationException;
import com.fundexpert.pojo.Holdings;
import com.fundexpert.pojo.StocksHoldings;
import com.fundexpert.pojo.Transactions;
import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.sun.javafx.collections.SetListenerHelper;

public class PdfReaderAndPersist {

	Session hSession=null;
	Transaction tx=null;
	List<String> emailList;
	Config config;
	static String readDir;//read pdf files from this dir
	static String saveDir;//save pdf's which were successfully parsed and persisted in DB
	static String noUserDir;//directory where those pdfs are saved which have no user(in DB) with given email in pdf filename
	static String stockReadDir;
	static String stockSaveDir;
	static String stockNoUserDir;
	static String decryptedFileDir;
	short pdfFromWhichApiRequest=0;//this field gives us hint from pdf file about from which api request pdf have come for eg, mutualfund pdf path or via email or via bytestream api request
	
	public PdfReaderAndPersist() throws Exception
	{
		try
		{
			config=new Config();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Not able to initialize PdfReader class.");
		}
	}
	
	public PdfReaderAndPersist(boolean stock) throws Exception
	{
		try
		{
			config=new Config();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Not able to initialize PdfReader class.");
		}
	}
	
	public boolean savepdf(String pdfPath,String emailId,String pan,long consumerId) throws Exception
	{
		File[] files;
		File readFile;
		int count=0;
		String email=null;
		boolean thisMethodIsCalledFromApi=false;
		try
		{
			readDir=config.getProperty(Config.PDF_NOT_READ);
			saveDir=config.getProperty(Config.PDF_READ);
			noUserDir=config.getProperty(Config.NO_USER_PDF);
			decryptedFileDir=config.getProperty(Config.DECRYPTED_PDF_FILE);
			hSession=HibernateBridge.getSessionFactory().openSession();
			Consumer con=(Consumer)hSession.createQuery("from Consumer where id=?").setLong(0, consumerId).uniqueResult();
			if(!con.isFetchPortfolioService())
				throw new FundexpertException("Buy Subscription For PortfolioService.");
			//get emails of those users whose consumer has subscribed to 'portfolioFetchService' 
			emailList=hSession.createQuery("select distinct u.email from User u,Consumer c where u.consumerId=c.id and c.id=?").setLong(0, consumerId).list();
			if(emailList==null || emailList.size()==0)
				throw new FundexpertException("No user exists under your type of request.");
			if(pdfPath==null)
			{
				readFile=new File(readDir);
				files=readFile.listFiles();
				if(files==null || files.length==0)
					return true;
				Arrays.sort(files, new Comparator<File>(){
				    public int compare(File f1, File f2)
				    {
				        return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
				    } 
				    });
				//System.out.println("files.length="+files.length);
			}
			else
			{
				//System.out.println("PDF PATH="+pdfPath);
				//System.out.println("is file : "+new File(pdfPath).isFile());
				if(pdfPath.equals(""))
				{
					throw new ValidationException("Not a valid pdf path.");
				}
				else if(!pdfPath.substring(pdfPath.length()-4).equalsIgnoreCase(".pdf"))
				{
					throw new ValidationException("Not a valid pdf file.");
				}
				else if(!new File(pdfPath).isFile())
				{
					throw new ValidationException("Not a valid pdf path.");
				}
				
				files = new File[1];
				files[0]=new File(pdfPath);
				String fileName=files[0].getName();
				fileName=fileName.replace("-", "_");
				//System.out.println("PDF FileName="+fileName);
				File newFile=new File(readDir+File.separator+emailId+"-"+fileName);
				Path newPath=Files.copy(files[0].toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
				if(newPath!=null)
				{
					System.out.println("Success in Copying File from pdf path to readDir : for email id="+emailId+" targetPath : "+newPath);
					if(files[0].delete());
						System.out.println("Deleting mutual fund file = "+fileName);
					files[0]=newFile;
				}
				else
				{
					System.out.println("Not able to copy pdf from pdf path to readDir : for email id="+emailId+"  targetPath : "+newFile.getAbsolutePath());
					throw new Exception("File copying error.");
				}
				thisMethodIsCalledFromApi=true;
			}
			for (File file : files)
			{	
				count++;
				String savedName = "";
				String path = "";
				try
				{
					if(file.isFile())
					{
						String fileName = file.getName();
						System.out.println("File Name "+fileName);
						String[] emailArray = fileName.split("-");
						//bypass those filenames which does not contains '-' in fileName.
						if(emailArray.length<2)
							continue;
						email = emailArray[0];
						if(!emailList.contains(email))
						{
							//do not store portfolio of those users(for some consumer like Edelweiss users are created at the time of PortfolioRequest for some through RegisterUser API call) whose consumer have not subscribed to 'portfolioFetchService'
							//TODO move such files to dir /opt/noUserPdf
							fileName+="-"+pdfFromWhichApiRequest+".pdf";
							File f=new File(noUserDir+File.separator+fileName);
							boolean success=file.renameTo(f);
							System.out.println("No User with email = "+email+" found hence dumping file into noUserDir with success = "+success+" and dir = "+f.getAbsolutePath());
							file.delete();
							tx=hSession.beginTransaction();
							NoUserFound noUserFound=new NoUserFound();
							noUserFound.setEmail(email);
							noUserFound.setReason("No User found with email.");
							noUserFound.setStock(false);
							noUserFound.setCreatedOn(new Date());
							hSession.save(noUserFound);
							tx.commit();
							
							//reason of throwing exception is 
							//when api call is made there will be only one file which might not be the case with email thing
							if(thisMethodIsCalledFromApi)
								throw new FundexpertException("No user found with email "+email+".");
							continue;
						}
						path=file.getAbsolutePath();
						List<User> usersList=null;
						if(thisMethodIsCalledFromApi)
						{
							usersList=hSession.createQuery("from User where email=? and pan=? and consumerId=?").setString(0,email).setString(1,pan).setLong(2,consumerId).list();
						}
						else
						{
							usersList=hSession.createQuery("from User where email=? and consumerId=?").setString(0,email).setLong(1,consumerId).list();
						}
						if(usersList==null || usersList.size()==0)
						{
							throw new FundexpertException("No user found with email : "+email+" and pan : "+pan);
						}
						PdfParser pdfParser=new PdfParser();
						List<Holdings> holdingsList=null;
						//userForPdfFileFoundFlag is changed to true when we want to move the file to pdfRead dir on successfull parsing of pdf
						//but if given file is not pdf or password mismatch then we want to move it to noUserPdf
						boolean userForPdfFileFoundFlag=true;
						long userId=0;
						String panActingAsPassword=null;
						PortfolioRequest pr=null;
						String error="";
						for(User u:usersList)
						{
							System.out.println("User id="+u.getUserId());
							if(u.getPan()==null || u.getPan().equals(""))
							{
								continue;
							}
							try
							{
								//check if there is PortfolioRequest for this User and its not submitted , then only store the Portfolio in DB
								pr = (PortfolioRequest)hSession.createQuery("from PortfolioRequest where userId=?").setLong(0, u.getId()).uniqueResult();
								userId=u.getId();
								if(pr==null && ArchiveApiRequests.mutualFundPortfolioFromEmail==pdfFromWhichApiRequest)
								{
									System.out.println("PR is null.");
									userForPdfFileFoundFlag=false;
									error="No request found.";
									continue;
								}
								if(ArchiveApiRequests.mutualFundPortfolioFromEmail==pdfFromWhichApiRequest && !pr.getSubmitted())
								{
									System.out.println("MF PortfolioRequest is still pending to be submitted.");
									userForPdfFileFoundFlag=false;
									error="Request still pending.";
									continue;
								}
								if(pr==null)
									panActingAsPassword=u.getPan();
								else
									panActingAsPassword=pr.getPassword();
								System.out.println("Decrypting pdf File at path = "+path+" with password = "+panActingAsPassword);
								holdingsList=pdfParser.getHoldingsList(path, panActingAsPassword);
								
								userForPdfFileFoundFlag=true;
								panActingAsPassword=u.getPan();
								break;
							}
							catch(BadPasswordException be)
							{
								be.printStackTrace();
								try
								{
									System.out.println(decryptedFileDir+" - decrypting file dir.");
									System.out.println(email);
									System.out.println(panActingAsPassword);
									System.out.println(emailArray[1]);
									String targetDecryptedPdfPath=decryptedFileDir+File.separator+email+"-"+panActingAsPassword+"-"+emailArray[1];
									String output=getDecryptedFile(path, targetDecryptedPdfPath, panActingAsPassword);
									System.out.println("Ouptut after decrypting pdf file for user email = "+email+" = "+output);
									
									holdingsList=pdfParser.getKarvyHoldingsList(targetDecryptedPdfPath, pr.getPassword());
									userForPdfFileFoundFlag=true;
									panActingAsPassword=u.getPan();
									break;
								}
								catch(BadPasswordException bpe)
								{
									bpe.printStackTrace();
									error=bpe.getMessage();
									userForPdfFileFoundFlag=false;
								}
								catch(FundexpertException fe)
								{
									error=fe.getMessage();
									userForPdfFileFoundFlag=false;
								}
								catch(Exception be1)
								{
									be1.printStackTrace();
									error="Something Failed.";
									if(be1.getMessage().contains("Not able to decrypt file."))
										error=be1.getMessage();
									System.out.println("ERROR : "+error+", so moving to noUserDir for emailId = "+email);
									userForPdfFileFoundFlag=false;
								}
							}
							catch(InvalidPdfException ipe)
							{
								ipe.printStackTrace();
								userForPdfFileFoundFlag=false;
								error="Not a valid pdf format.";
							}
							catch(FundexpertException e)
							{
								e.printStackTrace();
								userForPdfFileFoundFlag=false;
								error=e.getMessage();
							}
							catch(Exception e)
							{
								//call send sms here.
								e.printStackTrace();
								throw new Exception("Error Occurred in MF for email="+email+",pan="+panActingAsPassword+",file="+path+"at="+new Date());
							}
						}
						if(!userForPdfFileFoundFlag)
						{
							fileName=fileName+"-"+pdfFromWhichApiRequest+".pdf";
							File f=new File(noUserDir+File.separator+fileName);
							System.out.println("ERROR : "+error+", so moving to noUserDir for emailId = "+email+" and dir. = "+f.getAbsolutePath());
							boolean success=file.renameTo(f);
							file.delete();
							//Set field PortfolioUpload
							tx=hSession.beginTransaction();
							PortfolioUpload pu=new PortfolioUpload();
							pu.setFile(fileName);
							pu.setPassword(pr!=null?pr.getPassword():"");
							pu.setStock(false);
							pu.setUploadTime(new Date());
							pu.setUserId(userId);
							pu.setSuccess(false);
							pu.setError(error);
							hSession.save(pu);
							tx.commit();
							throw new FundexpertException(error);
						}
						//we have the holdingList here now persist it in DB
						PortfolioImportController pic=null;
						try
						{
							pic=new PortfolioImportController();
							pic.sFactory(userId, holdingsList);
							System.out.println("Successfully Parsed Data, also move the file to saveDir.");
							//make a new Directory with directoryName as PAN if not exists already
							File panDirectory=new File(saveDir+File.separator+panActingAsPassword);
							if(!panDirectory.exists())
							{
								panDirectory.mkdir();
							}
							String saveFileNameAs=email+"-"+panActingAsPassword+"-"+emailArray[1]+"-"+pdfFromWhichApiRequest+".pdf";
							file.renameTo(new File(panDirectory+File.separator+saveFileNameAs));
							file.delete();
							
							//Set field PortfolioUpload
							tx=hSession.beginTransaction();
							PortfolioUpload pu=new PortfolioUpload();
							pu.setFile(file.getName());
							pu.setPassword(panActingAsPassword);
							pu.setStock(false);
							pu.setUploadTime(new Date());
							pu.setUserId(userId);
							pu.setSuccess(true);
							hSession.save(pu);
							tx.commit();
							
						}
						catch(Exception e)
						{
							//TODO SEND SMS regarding not able to store users holdings in DB
							System.out.println("Error occurred while persisting pdf for email="+email+",pan="+panActingAsPassword);
							e.printStackTrace();
							throw new Exception("Error occurred while persisting pdf for email="+email+",pan="+panActingAsPassword);
						}
					}
				}
				catch(FundexpertException fe)
				{
					fe.printStackTrace();
					if(thisMethodIsCalledFromApi)
						throw fe;
				}
				catch(Exception e)
				{
					e.printStackTrace();
					if(thisMethodIsCalledFromApi)
						throw e;
				}
			}
			return true;
		}
		catch(FundexpertException e)
		{
			e.printStackTrace();
			if(tx!=null && tx.isActive())
				tx.rollback();
			throw e;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			throw new Exception("Error Occurred in PdfReaderAndPersist.");
		}
		finally
		{
			if(hSession!=null && hSession.isOpen())
				hSession.close();
		}
	}
	
	public class PdfParser
	{
		//String path = "";
		//String password = "";
		String fh = null;
		int numberSize = 0;// stores the size of number array
		int cnt = 0;// counts total number of transactions
		int po = 0;
		// int fhnIndex = 0;
		// int numberOfMfIndex = 0;// numberOfMfindex used to iterate through numberOfMfUnderOneHouse
		double[][] doub = null;
		String[][] portTable = null;
		// number of transactions under one mf is stored in number array which is created below
		public int[] number = null;// stores number of transactions under one folio(at one index)
		// A holding (1)list which again has elements as (2)list,where(2)list contains all the transactions under that holding
		// Holdings class contains list for Transactions
		ArrayList<Holdings> holdingsList = new ArrayList<Holdings>();
		ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();
		Holdings holding = null;
		Transactions transactions = null;
		Map<Holdings, ArrayList<Transactions>> holdingsMap = new HashMap<Holdings, ArrayList<Transactions>>();

		public List getHoldingsList(String path,String password) throws BadPasswordException, InvalidPdfException, Exception
		{
			int n = 0;
			String myLine = "", demo[];
			String name = null;
			String tempFolio[] = new String[2];
			PdfReader reader = null;
			InputStream is=null;
			try
			{
				File f=new File(path);
				is=new FileInputStream(f);
				System.out.println("password="+password+" path = "+path);
				reader=new PdfReader(is,password.getBytes());
				//reader = new PdfReader(path, password.getBytes());
				reader.unethicalreading = true;
				n = reader.getNumberOfPages();
				// below for loop stores whatever is in the pdf to the string myLine
				for (int i = 1; i <= n; i++)
				{
					myLine = myLine + PdfTextExtractor.getTextFromPage(reader, i);
				}
				//System.out.println("myline="+myLine);
				reader.close();
			}
			catch (InvalidPdfException ipe)
			{
				ipe.printStackTrace();
				throw new InvalidPdfException("Not a pdf file");
			}
			catch (BadPasswordException bpe)
			{
				bpe.printStackTrace();
				throw new BadPasswordException("Pdf-password mistmatch");
			}
			finally
			{
				if(is!=null)
				{
					is.close();
				}
			}
			try
			{
				Matcher m = Pattern.compile("\r\n|\r|\n").matcher(myLine);
				// demo array stores individual lines from myLine
				demo = myLine.split("\r|\n|\r\n|\n\r");
				if(!demo[0].contains("Consolidated Account Statement") ||  (demo[0].contains("Consolidated Account Statement") && demo[1].contains("Summary")))
					throw new FundexpertException("Please upload CAS statements only.");
				
				// i the index of transaction
				int i = 0;
				// openingFlag is set to true when it sees Opening as word(to know tranasactions are from next line)
				// flag is set to true when we are ready to process transactions after seeing openingFlag and false when we see valuation
				boolean flag = false, openingFlag = false;
				String transaction[] = new String[20];
				int b = 0;
				int add = 0;
				// counts total transactions in the pdf
				int totalTransaction = 0;
				// numberOfTransaction counts number of transaction under one folio and then stores it in number array
				int numberOfTransaction = 0;
				int index1 = 0;
				// the size to number is given on assumption but it will never go out of memory
				number = new int[demo.length / 2];
				// value of indexNumber tells current index of holding in pdf
				int indexOfNumber = 0;
				for (i = 0; i < number.length; i++)
				{
					number[i] = -1;
				}
				i = 0;
				// System.out.println("after pdf=" + myLine);
				for (int l = 0; l < demo.length; l++)
				{
					//System.out.println("demo[" + l + "]=" + demo[l]);
				}
				for (int k = 0; k < demo.length; k++)
				{
					if(openingFlag == true && demo[k].indexOf("*** No transactions ") != -1)
					{
						continue;
					}
					if((demo[k].length() > 10 && demo[k].substring(0, 9).equals("Valuation")) || (demo[k].length() == 11 && demo[k].substring(0, 4).equals("Page")))
					{
						b++;
					}
					if(openingFlag && (demo[k].indexOf("Valuation") != -1 || demo[k].indexOf("Page") != -1))
					{
						if(demo[k].indexOf("Valuation") != -1)
						{
							// below code is to fetch closing bal,nav val,valuation amt
							int q = 0;// q describes last 4 columns of pdf i.e amount,price etc
							int fromIndex = 0;
							// when closing,valutaion,nav values are all in one line in demo use below if()
							if(demo[k].indexOf("Closing") != -1)
							{
								while (q != 1)
								{
									fromIndex = demo[k].indexOf(":", fromIndex);
									Matcher mo = Pattern.compile("[0-9]+[,]*[0-9]*[/.][0-9]+").matcher(demo[k].substring(fromIndex, demo[k].length()));
									if(mo.find())
									{
										if(q == 0)
										{
											if(mo.group(0).indexOf(',') != -1)
											{
												StringBuilder sb = new StringBuilder(mo.group(0));
												sb.deleteCharAt(mo.group(0).indexOf(','));
												// the holding object is created below
												holding.setClosingUnits(Double.valueOf(sb.toString()));
											}
											else
											{
												holding.setClosingUnits(Double.valueOf(mo.group(0)));
											}
										}
									}
									q++;
								}
							}
							// this else solves when valuation comes in k line and closing,nav comes in k+1
							else
							{
								String merge = demo[k + 1] + " " + demo[k];
								while (q != 1)
								{
									fromIndex = merge.indexOf(":", fromIndex);
									Matcher mo = Pattern.compile("[0-9]+[,]*[0-9]*[/.][0-9]+").matcher(merge.substring(fromIndex, merge.length()));
									if(mo.find())
									{
										if(q == 0)
										{
											if(mo.group(0).indexOf(',') != -1)
											{
												StringBuilder sb = new StringBuilder(mo.group(0));
												sb.deleteCharAt(mo.group(0).indexOf(','));
												holding.setClosingUnits(Double.valueOf(sb.toString()));
											}
											else
											{
												holding.setClosingUnits(Double.valueOf(mo.group(0)));
											}
										}
									}
									q++;
								}
							}
							// below code is to help with differentiating only transactions with other part in demo array by setting flag=false
							flag = false;
							openingFlag = false;
							number[indexOfNumber] = numberOfTransaction;
							indexOfNumber++;
							// now a add the holding to list
							holdingsList.add(holding);
						}
						else
						{
							flag = false;
						}
						b++;
					}
					if(flag == true)
					{
						numberOfTransaction++;
						// below if is used in case if transaction index goes beyond its length
						if(i > transaction.length - 1)
						{
							add = transaction.length;
							String temporary[] = new String[add];
							temporary = transaction;
							transaction = new String[transaction.length + 20];
							System.arraycopy(temporary, 0, transaction, 0, temporary.length);
						}
						// transaction achieved from demo
						transaction[i] = demo[k];
						i++;
					}
					if(demo[k].length() > 10 && demo[k].substring(1, 8).equals("Opening") || (demo[k].length() > 10 && demo[k].substring(0, 7).equals("Opening")))
					{
						// as soon as we get OpeningFlag==true we get new holdings
						holding = new Holdings();
						openingFlag = true;
						// after seeing flag==true store all transactions in different string say transaction[i]=demo[k]
						flag = true;
						numberOfTransaction = 0;
						// folio number occurs two lines above 'opening' word in demo hence [k-2]
						// below if else find folioNumber considering different use cases as per 'demo'
						if(demo[k - 2].matches("(.)+[ ][0-9]+[ ]/[ ][0-9]+(.)+") || demo[k - 2].matches("(.)+[ ][0-9]+(.)+"))
						{
							Matcher match1 = Pattern.compile("[ ][0-9]+[ ][/][ ][0-9]+").matcher(demo[k - 2]);
							if(match1.find())
							{
								tempFolio = match1.group(0).split(" / ");
								if(tempFolio[1].trim().equals("0"))
								{
									holding.setFolioNumber(Long.valueOf(tempFolio[0].trim()));
									holding.setFolioNumberString(tempFolio[0].trim());
								}
								else
								{
									holding.setFolioNumber(Long.valueOf(tempFolio[0].trim() + tempFolio[1].trim()));
									holding.setFolioNumberString((tempFolio[0].trim() + "/" + tempFolio[1].trim()).replaceAll(" ", ""));
								}
							}
							else if(demo[k - 2].matches("(.)+[0-9]+(.)+"))
							{
								Matcher match2 = Pattern.compile("[ ][A-Za-z]*[0-9]+[ ]").matcher(demo[k - 2]);
								if(match2.find())
								{
									// holding.setFolioNumber(Long.valueOf(match2.group(0).trim()));
									holding.setFolioNumberString(match2.group(0).trim());
								}
							}
						}
						else
						{
							
							int backwards = k;
							while (!demo[backwards].substring(0, 5).equals("Folio"))
							{
								backwards--;
							}
							Matcher match = Pattern.compile("[ ][.0-9]+[ ]/[ ][0-9]+").matcher(demo[backwards]);
							if(match.find())
							{
								// after changes
								tempFolio = match.group(0).split(" / ");
								if(tempFolio[1].trim().equals("0"))
								{
									holding.setFolioNumber(Long.valueOf(tempFolio[0].trim()));
									holding.setFolioNumberString(tempFolio[0].trim());
								}
								else
								{
									holding.setFolioNumber(Long.valueOf(tempFolio[0].trim() + tempFolio[1].trim()));
									holding.setFolioNumberString((tempFolio[0].trim() + "/" + tempFolio[1].trim()).replaceAll(" ", ""));
								}
								// holding.setFolioNumberString(match.group(0).trim().replaceAll(" ", ""));
							}
							else if(demo[backwards].matches("(.)+[0-9]+(.)+"))
							{
								Matcher match2 = Pattern.compile("[ ][A-Za-z]*[0-9]+[ ]").matcher(demo[backwards]);
								if(match2.find())
								{
									// holding.setFolioNumber(Long.valueOf(match2.group(0).trim()));
									holding.setFolioNumberString(match2.group(0).trim());
								}
								else
									throw new Exception("No Match of FolioNumber");
							}
							//System.out.println("backwards="+backwards+" k="+k);
						}
						// when the page change and opening is true and opening unit balance comes after "(INR) (INR)"
						if(demo[k - 1].contains("(INR) (INR)"))
						{
							//System.out.println("!@#$");
							if(demo[k - 7].substring(0, demo[k - 7].indexOf("-") + 1).matches("[a-zA-Z0-9]+[-]"))
							{
								//System.out.println("!@#$1");
								int camsCodeEndIndex = demo[k - 7].indexOf("-");
								String camsCode = demo[k - 7].substring(0, camsCodeEndIndex);
								String mutualFundName = demo[k - 7].substring(camsCodeEndIndex + 1);
								if(mutualFundName.contains("ARN"))
								{
									int indexOfARN=mutualFundName.indexOf("ARN");
									int indexOfClosedBracket=mutualFundName.indexOf(")", indexOfARN);
									if(indexOfClosedBracket != -1)
									{
										String arn=mutualFundName.substring(indexOfARN,indexOfClosedBracket);
										//System.out.println(mutualFundName+" : ARN = "+arn);
										holding.setArn(arn);
									}
								}
								holding.setCamsCode(camsCode);
								holding.setMutualFundName(mutualFundName);
								
								//System.out.println("1."+camsCode + "-" + mutualFundName);
							}
							else
							{
								//System.out.println("!@#$2");
								int backwards = k - 7;
								while (!demo[backwards].substring(0, demo[backwards].indexOf("-") + 1).matches("[a-zA-Z0-9]+[-]"))
								{
									backwards--;
								}
								int camsCodeEndIndex = demo[backwards].indexOf("-");
								String camsCode = demo[k - 1].substring(0, camsCodeEndIndex);
								String mutualFundName = demo[backwards].substring(camsCodeEndIndex + 1);
								while (backwards != k - 7)
								{
									backwards++;
									mutualFundName = mutualFundName + demo[backwards];
								}
							}
						}
						//in below else if first condition satisfy 'camsCode-Mutualfund name' starting from index 0 where mutualfund name should start with [a-z,A-z]
						//2nd condition states that if demo[k-1] has pattern like '[a-zA-z]-Mutualfund name' starting from index 0 then mutualfund name should not start with name '[0-9]' which is the case when 'ARN-234324)' is at line demo[k-1] and if it starts with [0-9]+ then check if demo[k-1] has 'ARN'
						//else if(demo[k - 1].substring(0, demo[k - 1].indexOf("-") + 1).matches("[a-zA-z0-9]+[-]") && !demo[k-1].substring(demo[k - 1].indexOf("-"),demo[k - 1].indexOf("-")+2).matches("[/-][0-9]"))
						else if(demo[k - 1].substring(0, demo[k - 1].indexOf("-") + 1).matches("[a-zA-z0-9\\s]+[-]") && ((demo[k-1].substring(demo[k - 1].indexOf("-"),demo[k - 1].indexOf("-")+2).matches("[/-][0-9]") && !demo[k-1].substring(0,demo[k - 1].indexOf("-")).toLowerCase().contains("arn")) || (!demo[k-1].substring(demo[k - 1].indexOf("-"),demo[k - 1].indexOf("-")+2).matches("[/-][0-9]"))))
						{
							int camsCodeEndIndex = demo[k - 1].indexOf("-");
							String camsCode = demo[k - 1].substring(0, camsCodeEndIndex);
							String mutualFundName = demo[k - 1].substring(camsCodeEndIndex + 1);
							//usually Advisor:ARN-[0-9]* comes in case of regular funds and Advisor:[a-z0-9]* comes in case of direct funds
							if(mutualFundName.contains("ARN"))
							{
								int indexOfARN=mutualFundName.indexOf("ARN");
								int indexOfClosedBracket=mutualFundName.indexOf(")", indexOfARN);
								if(indexOfClosedBracket!=-1)
								{
									String arn=mutualFundName.substring(indexOfARN,indexOfClosedBracket).trim();
									//System.out.println(mutualFundName+" : ARN = "+arn);
									holding.setArn(arn);
								}
							}
							else if(mutualFundName.contains("Direct"))
							{
								if(mutualFundName.contains("Advisor:"))
								{
									int indexOfARN=mutualFundName.indexOf("Advisor:");
									int indexOfClosedBracket=mutualFundName.indexOf(")",indexOfARN);
									if(indexOfClosedBracket!=-1)
									{
										int indexOfActualArn=indexOfARN+8;//8 represent length of 'Advisor:'
										String arn=mutualFundName.substring(indexOfActualArn,indexOfClosedBracket).trim();
										holding.setArn(arn);
									}
								}
							}
							holding.setCamsCode(camsCode);
							holding.setMutualFundName(mutualFundName);
							//System.out.println("2."+camsCode + "-" + mutualFundName);
	
						}
						else if(demo[k - 2].substring(0, demo[k - 2].indexOf("-") + 1).matches("[a-zA-z0-9]+[-]"))
						{
							//System.out.println("!@#$4");
							int camsCodeEndIndex = demo[k - 2].indexOf("-");
							String camsCode = demo[k - 2].substring(0, camsCodeEndIndex);
							String mutualFundName = demo[k - 2].substring(camsCodeEndIndex + 1) + demo[k - 1];
							//System.out.println("this mutualfundname should not come");
						}
						else if(demo[k - 3].substring(0, demo[k - 3].indexOf("-") + 1).matches("[a-zA-z0-9]+[-]"))
						{
							//System.out.println("!@#$5");
							int camsCodeEndIndex = demo[k - 3].indexOf("-");
							String camsCode = demo[k - 3].substring(0, camsCodeEndIndex);
							String mutualFundName = demo[k - 3].substring(camsCodeEndIndex + 1);
							//as soon as we saw 'ARN' look for '-[0-9]+)' in concatenation of all the lines from demo[k-3] to demo[k-1]
							String actualMutualfundNameInPdf=mutualFundName+demo[k-2]+demo[k-1];
							if(actualMutualfundNameInPdf.contains("ARN"))
							{
								//System.out.println("New String!@# : ");
								int indexOfARN=actualMutualfundNameInPdf.indexOf("ARN");
								int indexOfHyphen=actualMutualfundNameInPdf.indexOf("-", indexOfARN);
								//now we have index of '-' which comes after 'ARN'. So now we look for '[0-9]+)' pattern.
								if(indexOfHyphen!=-1)
								{
									//System.out.println("Index of hyphen="+indexOfHyphen+" indexOfARN="+indexOfARN);
									//get index of first occurrence of digit in ARN no.
									int indexOfFirstDigit=-1;
									int indexOfClosedBracket=actualMutualfundNameInPdf.indexOf(")", indexOfHyphen);
									Pattern p=Pattern.compile("[0-9]");
									String substring=actualMutualfundNameInPdf.substring(indexOfHyphen+1,indexOfClosedBracket+1);
									//System.out.println("SUBSTRING="+substring);
									Matcher matcher=p.matcher(substring);
									if(matcher.find())
									{
										indexOfFirstDigit=matcher.start();
										//System.out.println("Index of first digit="+indexOfFirstDigit);
									}
									if(indexOfFirstDigit!=-1)
									{
										String arnContainingOnlyNumbers=substring.substring(indexOfFirstDigit,substring.indexOf(")"));
										if(arnContainingOnlyNumbers.matches("[0-9]+"))
										{
											String newArn="ARN-"+arnContainingOnlyNumbers;
											holding.setArn(newArn);
										}
									}
								}
							}
							holding.setMutualFundName(mutualFundName);
							holding.setCamsCode(camsCode);
							//System.out.println("this mutualfundname should not come but came camsCode = "+camsCode+" mfName="+mutualFundName);
						}
					}
					if(demo[k].indexOf("(INR) (INR)") != -1)
					{
						if(openingFlag == true)
						{
							if(demo[k + 1].length() > 11 && demo[k + 1].substring(0, 11).matches("[0-9]{2}-[a-zA-Z]{3}-[0-9]{4}"))
							{
								flag = true;
							}
							else if(demo[k + 1].substring(0, demo[k + 1].length() - 1).matches("[0-9,]+[/.][0-9]+"))
							{
								flag = true;
							}
							else
							{
								flag = false;
							}
						}
						else
						{
							flag = false;
						}
					}
				}
				int start = 0;
				// below for loop helps to find number of transactions under one holding/Mutual Fund
				// condition:number of transactions under one holding might be different here since different parts(date,amount etc) might not be in one transaction
				int lol = 0;
				for (int j = 0; j < number.length; j++)
				{
					// System.out.println("number[" + j + "] = " + number[j]);
				}
				String transaction1[] = new String[transaction.length];
				int index = 0;
				String append = null;
				int j = 0;
				int k = 0, q = 0;
				int indexM = 0;
				String cut[];
				StringBuffer sb;
				indexOfNumber = 0;
				numberOfTransaction = number[0];
				int count = 0;// counts total transactions under one mf then set to 0 for next mutualfund
				int indexOfTransaction = 0;
				// Iterator itr = holdingsList.iterator();
				/*while (itr.hasNext())
				{
					Holdings hol = (Holdings) itr.next();
					System.out.println(hol.getMutualFundName());
					System.out.println(hol.getClosingUnits() + "\t\t" + hol.getFolioNumber());
				}*/
				while (j < number.length && number[j] != -1)
				{
					Holdings h = holdingsList.get(j);
					List<String> txList = new ArrayList<String>();
					int l = 0;
					for (l = 0; l < number[j]; l++)
					{
						//System.out.println("!@#$#@!"+transaction[indexOfTransaction + l]);
						txList.add(transaction[indexOfTransaction + l]);
					}
					// System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5");
					List<Transactions> transactionList=stringToTransactions(txList);
					if(transactionList!=null && transactionList.size()>0)
					{
						h.setTransactionsList(transactionList);
					}
					// holdingsList.add(j, h);
					indexOfTransaction = indexOfTransaction + l;
					j++;
				}
				//System.out.println("Holding with no transactions");
				for(int l=0;l<holdingsList.size();l++)
				{
					Holdings holding=holdingsList.get(l);
					//we'll count units using units from each transaction for eg (if pdf is from 1-1-2007-5-1-2017 but user has holding of 5000 before these date then it will be shown only as closing units in holding and not as transactions)
					double units=0;
					if(holding.getTransactionsList()==null || holding.getTransactionsList().size()==0)
					{
						holdingsList.remove(l);
						//System.out.println("missing="+holding.getMutualFundName());
						l--;
					}
					else
					{
						/*System.out.println(holding.getFolioNumber());
						System.out.println(holding.getCamsCode()+"  "+holding.getMutualFundName());*/
						Iterator itr=holding.getTransactionsList().iterator();
						
						while(itr.hasNext())
						{
							Transactions tx=(Transactions)itr.next();
							units+=tx.getUnits();
							units=Math.round(units*100000.0)/100000.0;
							//System.out.println(tx.getDate()+"\t"+tx.getAmount()+"\t"+tx.getUnits()+"\t"+tx.getPrice());
						}
						/*System.out.println("total units="+units);*/
					}
					if(units<0.0)
						units=0.0;
					String mfName=holding.getMutualFundName();
					if(mfName!=null)
					{
						int indexOfOpeningBracket=mfName.contains("(Advi")?mfName.indexOf("(Advi"):-1;
						if(indexOfOpeningBracket!=-1)
						{
							holding.setMutualFundName(mfName.substring(0,indexOfOpeningBracket).trim());
						}
						else
						{
							//if '(Advi' is not in mfName string in pdf then we have to explicitly remove 'Registrar' from mfName
							int indexOfRegistrar=mfName.contains("Registrar")?mfName.indexOf("Registrar"):-1;
							if(indexOfRegistrar!=-1)
							{
								holding.setMutualFundName(mfName.substring(0,indexOfRegistrar).trim());
							}
						}
					}
					holding.setClosingUnits(units);
				}
				return holdingsList;
			}
			catch(FundexpertException fe)
			{
				fe.printStackTrace();
				throw fe;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new Exception("Error Occurred while parsing PDF.");
			}
		}
		
		public List<Transactions> stringToTransactions(List<String> tx) throws ParseException
		{
			
			List<Transactions> transactions = new ArrayList<Transactions>();
			// String datePattern = "[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}";
			// String numRegex = "\\([0-9]+(\\.)[0-9]{2,3}\\)|[0-9]+(\\.)[0-9]{2,3}";
			// Regex for standard transaction
			
			//changes in txPattern1
			//a)sometimes price comes with value as 21.100000321 which has upto 9 decimal places hence replacing {2,4} with {2,9} in price regex of txPattern1
			//changes in txPattern2
			//a)sometimes price comes with value as 21.100000321 which has upto 9 decimal places hence replacing {2,4} with {2,9} in price regex of txPattern2
			//changes in txPattern3
			//a)sometimes price comes with value as 21.100000321 which has upto 9 decimal places hence replacing {2,4} with {2,9} in price regex of txPattern3
			
			String txPattern1 = "^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(.)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,9}\\)|[0-9]+(\\.)[0-9]{2,9})(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})(\\s)*$";
			// Regex for transaction where unit balance in missing
			String txPattern2 = "^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(.)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,9}\\)|[0-9]+(\\.)[0-9]{2,9})(\\s)*$";
			// Regex for transaction where text and unit balance in missing
			String txPattern3 = "^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,9}\\)|[0-9]+(\\.)[0-9]{2,9})(\\s)*$";
			try
			{
				Iterator<String> txI = tx.iterator();
				while (txI.hasNext())
				{
					String amount = "", price = "", units = "", date = "", content = "";
					double amt = 0, p = 0, u = 0;
					Date txDate = null;
					String tr = txI.next();
					//System.out.println(tr+"#@!");
					tr = tr.replaceAll(",", "");
					//System.out.println("transa" + tr);
					boolean flag = false;
					
					if(tr.matches(txPattern1))
					{
						// Check last 4 value, ignore 4th, take other 3
						//System.out.println(tr + " - Standard pattern");
						tr = tr.substring(0, tr.lastIndexOf(" "));
						price = tr.substring(tr.lastIndexOf(" "), tr.length());
						tr = tr.substring(0, tr.lastIndexOf(" "));
						units = tr.substring(tr.lastIndexOf(" "), tr.length());
						tr = tr.substring(0, tr.lastIndexOf(" "));
						amount = tr.substring(tr.lastIndexOf(" "), tr.length());
						tr=tr.substring(0,tr.lastIndexOf(" "));
						content = tr.substring(tr.indexOf(" ")+1,tr.length());
						date = tr.substring(0, 11);
						flag = true;
					}
					else if(tr.matches(txPattern2))
					{
						// Check last 3 value
						//System.out.println(tr + "!@#$%^&* - Unit balance missing");
						
						if(tr.substring(12, (12+11)).equals("Bonus Units"))
						{
							//System.out.println("Indside Bonus Points.");
							tr=tr.substring(0, tr.lastIndexOf(" "));
							units = tr.substring(tr.lastIndexOf(" "), tr.length());
							date = tr.substring(0, 11);
							/*System.out.print("Units="+units+" date="+date);*/
							price=".1";
							amount=String.valueOf(Double.valueOf(units)*Double.valueOf(price));
							/*System.out.println("price="+price+" amount="+amount);*/
							flag = true;
						}
						else
						{
							price = tr.substring(tr.lastIndexOf(" "), tr.length());
							tr = tr.substring(0, tr.lastIndexOf(" "));
							units = tr.substring(tr.lastIndexOf(" "), tr.length());
							tr = tr.substring(0, tr.lastIndexOf(" "));
							amount = tr.substring(tr.lastIndexOf(" "), tr.length());
							date = tr.substring(0, 11);
							flag = true;
						
						}
					}
					else if(tr.matches(txPattern3))
					{
						// Check last 3 value
						//System.out.println(tr + " - Text and Unit balance missing");
						price = tr.substring(tr.lastIndexOf(" "), tr.length());
						tr = tr.substring(0, tr.lastIndexOf(" "));
						units = tr.substring(tr.lastIndexOf(" "), tr.length());
						tr = tr.substring(0, tr.lastIndexOf(" "));
						amount = tr.substring(tr.lastIndexOf(" "), tr.length());
						date = tr.substring(0, 11);
						flag = true;
					}
					else
					{
						// System.out.println(tr + " - Does not match");
					}
					// System.out.println(date);
					if(flag == true)
					{
						Transactions transaction = new Transactions();
						txDate = new SimpleDateFormat("dd-MMM-yyyy").parse(date);
						if(amount.contains("("))
						{
							amount = amount.replace("(", "-");
							amount = amount.replace(")", "");
							amt = Double.valueOf(amount);
							units = units.replace("(", "-");
							units = units.replace(")", "");
							u = Double.valueOf(units);
							p = Double.valueOf(price);
							
							transaction.setAmount(amt);
							transaction.setPrice(p);
							transaction.setUnits(u);
							transaction.setDate(txDate);
							transaction.setType("2");
							transactions.add(transaction);
						}
						else
						{
							amt = Double.valueOf(amount);
							u = Double.valueOf(units);
							p = Double.valueOf(price);
							//if(amt>1 || amt<-1)
							{
								transaction.setAmount(amt);
								transaction.setPrice(p);
								transaction.setUnits(u);
								transaction.setDate(txDate);
								transaction.setType("1");
								transactions.add(transaction);
							}
						}
						
						//Below arn code should stay only on FUNDEXPERT
						Pattern pattern=Pattern.compile("((ARN[\\-])([0-9]+))(([\\/][a-zA-Z0-9]+))|((ARN[\\-])([0-9]+))");
						Matcher m=pattern.matcher(content);
						//System.out.println(m.groupCount());
						while(m.find())
						{
							/*System.out.println("Match found for ARN : "+m.group(3)+"  "+m.group(8));*/
							if(m.group(0).matches("(ARN[\\-][0-9]+)"))
							{
								transaction.setArn(m.group(3));
							}
							else if(m.group(1).matches("(ARN[\\-][0-9]+)"))
							{
								transaction.setArn(m.group(8));
							}
							
							//System.out.println(m.group(0)+" "+m.group(1)+" "+m.group(2)+" $ "+m.group(3)+" $ !"+m.group(4)+"@ $  "+m.group(5)+" "+m.group(6)+" "+m.group(7)+" "+m.group(8));
						}
						//System.out.println(txDate + "\t\t" + content + "\t\t" + amt + "\t\t\t" + u + "\t\t\t" + p);
					}
				}
				return transactions;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw e;
			}
			
		}
	
		public List getKarvyHoldingsList(String path,String password) throws BadPasswordException, InvalidPdfException, Exception
		{
			int n = 0;
			String myLine = "", demo[];
			String name = null;
			String tempFolio[] = new String[2];
			PdfReader reader = null;
			InputStream is=null;
			try
			{
				File f=new File(path);
				is=new FileInputStream(f);
				reader = new PdfReader(is, password.getBytes());
				reader.unethicalreading = true;
				
				n = reader.getNumberOfPages();
				//System.out.println("No Of Pages="+n);
				// below for loop stores whatever is in the pdf to the string myLine
				for (int i = 1; i <= n; i++)
				{
					myLine = myLine + PdfTextExtractor.getTextFromPage(reader, i);
				}
				//System.out.println("myline="+myLine);
				reader.close();
			}
			catch (InvalidPdfException ipe)
			{
				ipe.printStackTrace();
				throw new InvalidPdfException("Not a pdf file");
			}
			catch (BadPasswordException bpe)
			{
				bpe.printStackTrace();
				throw new BadPasswordException("pdf-password mistmatch");
			}
			finally
			{
				if(is!=null)
				{
					is.close();
				}
			}
			Matcher m = Pattern.compile("\r\n|\r|\n").matcher(myLine);
			// demo array stores individual lines from myLine
			demo = myLine.split("\r|\n|\r\n|\n\r");
			if(!demo[0].contains("Consolidated Account Statement"))
				throw new FundexpertException("Please upload CAS statements only.");
			
			
			// i the index of transaction
			int i = 0;
			// openingFlag is set to true when it sees Opening as word(to know tranasactions are from next line)
			// flag is set to true when we are ready to process transactions after seeing openingFlag and false when we see valuation
			boolean flag = false, openingFlag = false;
			String transaction[] = new String[20];
			int b = 0;
			int add = 0;
			// counts total transactions in the pdf
			int totalTransaction = 0;
			// numberOfTransaction counts number of transaction under one folio and then stores it in number array
			int numberOfTransaction = 0;
			int index1 = 0;
			// the size to number is given on assumption but it will never go out of memory
			number = new int[demo.length / 2];
			// value of indexNumber tells current index of holding in pdf
			int indexOfNumber = 0;
			for (i = 0; i < number.length; i++)
			{
				number[i] = -1;
			}
			i = 0;
			// System.out.println("after pdf=" + myLine);
			for (int l = 0; l < demo.length; l++)
			{
				//System.out.println("demo[" + l + "]=" + demo[l]);
			}
			for (int k = 0; k < demo.length; k++)
			{
				if(openingFlag == true && demo[k].indexOf("*** No transactions ") != -1)
				{
					continue;
				}
				if((demo[k].length() > 10 && demo[k].substring(0, 9).equals("Valuation")) || (demo[k].length() == 11 && demo[k].substring(0, 4).equals("Page")))
				{
					b++;
				}
				if(openingFlag && (demo[k].indexOf("Valuation") != -1 || demo[k].indexOf("Page") != -1))
				{
					if(demo[k].indexOf("Valuation") != -1)
					{
						// below code is to fetch closing bal,nav val,valuation amt
						int q = 0;// q describes last 4 columns of pdf i.e amount,price etc
						int fromIndex = 0;
						// when closing,valutaion,nav values are all in one line in demo use below if()
						if(demo[k].indexOf("Closing") != -1)
						{
							while (q != 1)
							{
								fromIndex = demo[k].indexOf(":", fromIndex);
								Matcher mo = Pattern.compile("[0-9]+[,]*[0-9]*[/.][0-9]+").matcher(demo[k].substring(fromIndex, demo[k].length()));
								if(mo.find())
								{
									if(q == 0)
									{
										if(mo.group(0).indexOf(',') != -1)
										{
											StringBuilder sb = new StringBuilder(mo.group(0));
											sb.deleteCharAt(mo.group(0).indexOf(','));
											// the holding object is created below
											holding.setClosingUnits(Double.valueOf(sb.toString()));
										}
										else
										{
											holding.setClosingUnits(Double.valueOf(mo.group(0)));
										}
									}
								}
								q++;
							}
						}
						// this else solves when valuation comes in k line and closing,nav comes in k+1
						else
						{
							String merge = demo[k + 1] + " " + demo[k];
							while (q != 1)
							{
								fromIndex = merge.indexOf(":", fromIndex);
								Matcher mo = Pattern.compile("[0-9]+[,]*[0-9]*[/.][0-9]+").matcher(merge.substring(fromIndex, merge.length()));
								if(mo.find())
								{
									if(q == 0)
									{
										if(mo.group(0).indexOf(',') != -1)
										{
											StringBuilder sb = new StringBuilder(mo.group(0));
											sb.deleteCharAt(mo.group(0).indexOf(','));
											holding.setClosingUnits(Double.valueOf(sb.toString()));
										}
										else
										{
											holding.setClosingUnits(Double.valueOf(mo.group(0)));
										}
									}
								}
								q++;
							}
						}
						// below code is to help with differentiating only transactions with other part in demo array by setting flag=false
						flag = false;
						openingFlag = false;
						number[indexOfNumber] = numberOfTransaction;
						indexOfNumber++;
						// now a add the holding to list
						holdingsList.add(holding);
					}
					else
					{
						flag = false;
					}
					b++;
				}
				if(flag == true)
				{
					numberOfTransaction++;
					// below if is used in case if transaction index goes beyond its length
					if(i > transaction.length - 1)
					{
						add = transaction.length;
						String temporary[] = new String[add];
						temporary = transaction;
						transaction = new String[transaction.length + 20];
						System.arraycopy(temporary, 0, transaction, 0, temporary.length);
					}
					// transaction achieved from demo
					transaction[i] = demo[k];
					i++;
				}
				if(demo[k].length() > 10 && demo[k].substring(0, 7).equals("Opening"))
				{
					// as soon as we get OpeningFlag==true we get new holdings
					holding = new Holdings();
					openingFlag = true;
					// after seeing flag==true store all transactions in different string say transaction[i]=demo[k]
					flag = true;
					numberOfTransaction = 0;
					// folio number occurs two lines above 'opening' word in demo hence [k-2]
					// below if else find folioNumber considering different use cases as per 'demo'
					//below if is modified as per karvy pdf parsing requirement since cams gives Folio No: 17714203573 / 0 
					//but karvy gives 17714203573/ 0
					if(demo[k - 2].matches("(.)+[ ][0-9]+[/][ ][0-9]+(.)+") || demo[k - 2].matches("(.)+[ ][0-9]+(.)+"))
					{
						Matcher match1 = Pattern.compile("[ ][0-9]+[/][ ][0-9]+").matcher(demo[k - 2]);
						if(match1.find())
						{
							tempFolio = match1.group(0).split("/ ");
							if(tempFolio[1].trim().equals("0"))
							{
								holding.setFolioNumber(Long.valueOf(tempFolio[0].trim()));
								holding.setFolioNumberString(tempFolio[0].trim());
							}
							else
							{
								holding.setFolioNumber(Long.valueOf(tempFolio[0].trim() + tempFolio[1].trim()));
								holding.setFolioNumberString((tempFolio[0].trim() + "/" + tempFolio[1].trim()).replaceAll(" ", ""));
							}
						}
						//chances of going in this else is less as compare to if case
						else if(demo[k - 2].matches("(.)+[0-9]+(.)+"))
						{
							Matcher match2 = Pattern.compile("[ ][A-Za-z]*[0-9]+[ ]").matcher(demo[k - 2]);
							if(match2.find())
							{
								// holding.setFolioNumber(Long.valueOf(match2.group(0).trim()));
								holding.setFolioNumberString(match2.group(0).trim());
							}
						}
					}
					else
					{
						
						int backwards = k;
						while (!demo[backwards].substring(0, 5).equals("Folio"))
						{
							backwards--;
						}
						Matcher match = Pattern.compile("[ ][.0-9]+/[ ][0-9]+").matcher(demo[backwards]);
						if(match.find())
						{
							// after changes
							tempFolio = match.group(0).split("/ ");
							if(tempFolio[1].trim().equals("0"))
							{
								holding.setFolioNumber(Long.valueOf(tempFolio[0].trim()));
								holding.setFolioNumberString(tempFolio[0].trim());
							}
							else
							{
								holding.setFolioNumber(Long.valueOf(tempFolio[0].trim() + tempFolio[1].trim()));
								holding.setFolioNumberString((tempFolio[0].trim() + "/" + tempFolio[1].trim()).replaceAll(" ", ""));
							}
							// holding.setFolioNumberString(match.group(0).trim().replaceAll(" ", ""));
						}
						else if(demo[backwards].matches("(.)+[0-9]+(.)+"))
						{
							Matcher match2 = Pattern.compile("[ ][A-Za-z]*[0-9]+[ ]").matcher(demo[backwards]);
							if(match2.find())
							{
								// holding.setFolioNumber(Long.valueOf(match2.group(0).trim()));
								holding.setFolioNumberString(match2.group(0).trim());
							}
							else
								throw new Exception("No Match of FolioNumber");
						}
						//System.out.println("backwards="+backwards+" k="+k);
					}
					// when the page change and opening is true and opening unit balance comes after "(INR) (INR)"
					if(demo[k - 1].contains("(INR) (INR)"))
					{
						//System.out.println("!@#$");
						if(demo[k - 7].substring(0, demo[k - 7].indexOf("-") + 1).matches("[a-zA-Z0-9]+[-]"))
						{
							//System.out.println("!@#$1");
							int camsCodeEndIndex = demo[k - 7].indexOf("-");
							String camsCode = demo[k - 7].substring(0, camsCodeEndIndex);
							String mutualFundName = demo[k - 7].substring(camsCodeEndIndex + 1);
							if(mutualFundName.contains("ARN"))
							{
								int indexOfARN=mutualFundName.indexOf("ARN");
								int indexOfClosedBracket=mutualFundName.indexOf(")", indexOfARN);
								if(indexOfClosedBracket != -1)
								{
									String arn=mutualFundName.substring(indexOfARN,indexOfClosedBracket);
									//System.out.println(mutualFundName+" : ARN = "+arn);
									holding.setArn(arn);
								}
							}
							holding.setCamsCode(camsCode);
							holding.setMutualFundName(mutualFundName);
							
							//System.out.println("1."+camsCode + "-" + mutualFundName);
						}
						else
						{
							//System.out.println("!@#$2");
							int backwards = k - 7;
							while (!demo[backwards].substring(0, demo[backwards].indexOf("-") + 1).matches("[a-zA-Z0-9]+[-]"))
							{
								backwards--;
							}
							int camsCodeEndIndex = demo[backwards].indexOf("-");
							String camsCode = demo[k - 1].substring(0, camsCodeEndIndex);
							String mutualFundName = demo[backwards].substring(camsCodeEndIndex + 1);
							while (backwards != k - 7)
							{
								backwards++;
								mutualFundName = mutualFundName + demo[backwards];
							}
						}
					}
					//in below else if first condition satisfy 'camsCode-Mutualfund name' starting from index 0 where mutualfund name should start with [a-z,A-z]
					//2nd condition states that if demo[k-1] has pattern like '[a-zA-z]-Mutualfund name' starting from index 0 then mutualfund name should not start with name '[0-9]' which is the case when 'ARN-234324)' is at line demo[k-1] and if it starts with [0-9]+ then check if demo[k-1] has 'ARN'
					else if(demo[k - 1].substring(0, demo[k - 1].indexOf("-") + 1).matches("[a-zA-z0-9\\s]+[-]") && ((demo[k-1].substring(demo[k - 1].indexOf("-"),demo[k - 1].indexOf("-")+2).matches("[/-][0-9]") && !demo[k-1].substring(0,demo[k - 1].indexOf("-")).toLowerCase().contains("arn")) || (!demo[k-1].substring(demo[k - 1].indexOf("-"),demo[k - 1].indexOf("-")+2).matches("[/-][0-9]"))))
					{
						int camsCodeEndIndex = demo[k - 1].indexOf("-");
						String camsCode = demo[k - 1].substring(0, camsCodeEndIndex);
						String mutualFundName = demo[k - 1].substring(camsCodeEndIndex + 1);
						if(mutualFundName.contains("ARN"))
						{
							int indexOfARN=mutualFundName.indexOf("ARN");
							int indexOfClosedBracket=mutualFundName.indexOf(")", indexOfARN);
							if(indexOfClosedBracket!=-1)
							{
								String substring=mutualFundName.substring(indexOfARN,indexOfClosedBracket+1);
								//System.out.println("substring="+substring);
								Pattern p=Pattern.compile("[0-9]");
								Matcher matcher=p.matcher(substring);
								int indexOfFirstDigit=0;
								if(matcher.find())
								{
									indexOfFirstDigit=matcher.start();
									//System.out.println(")(*Index of first digit="+indexOfFirstDigit);
								}
								if(indexOfFirstDigit!=-1)
								{
									//System.out.println("substring="+substring+" indexOfFirstDigit="+indexOfFirstDigit+" indexOf )="+substring.indexOf(")"));
									String arnContainingOnlyNumbers=substring.substring(indexOfFirstDigit,substring.indexOf(")"));
									if(arnContainingOnlyNumbers.matches("[0-9]+"))
									{
										String newArn="ARN-"+arnContainingOnlyNumbers;
										holding.setArn(newArn);
									}
								}
							}
						}
						else if(mutualFundName.contains("Direct"))
						{
							if(mutualFundName.contains("Advisor:"))
							{
								int indexOfARN=mutualFundName.indexOf("Advisor:");
								int indexOfClosedBracket=mutualFundName.indexOf(")",indexOfARN);
								if(indexOfClosedBracket!=-1)
								{
									int indexOfActualArn=indexOfARN+8;//8 represent length of 'Advisor:'
									String arn=mutualFundName.substring(indexOfActualArn,indexOfClosedBracket).trim();
									holding.setArn(arn);
								}
							}
						}
						holding.setCamsCode(camsCode);
						//System.out.println("camsCode="+camsCode);
						holding.setMutualFundName(mutualFundName);
						//System.out.println("2."+camsCode + "-" + mutualFundName);

					}
					else if(demo[k - 2].substring(0, demo[k - 2].indexOf("-") + 1).matches("[a-zA-z0-9]+[-]"))
					{
						//System.out.println("!@#$4");
						/*int camsCodeEndIndex = demo[k - 2].indexOf("-");
						String camsCode = demo[k - 2].substring(0, camsCodeEndIndex);
						String mutualFundName = demo[k - 2].substring(camsCodeEndIndex + 1) + demo[k - 1];*/
						//System.out.println("!@#this mutualfundname should not come");
						
						int camsCodeEndIndex = demo[k - 2].indexOf("-");
						String camsCode = demo[k - 2].substring(0, camsCodeEndIndex);
						String mutualFundName = demo[k - 2].substring(camsCodeEndIndex + 1);
						//as soon as we saw 'ARN' look for '-[0-9]+)' in concatenation of all the lines from demo[k-3] to demo[k-1]
						String actualMutualfundNameInPdf=mutualFundName+demo[k-1];
						//System.out.println("ActualMutualfundName()*."+actualMutualfundNameInPdf);
						if(actualMutualfundNameInPdf.contains("ARN"))
						{
							//System.out.println("New String!@# : ");
							int indexOfARN=actualMutualfundNameInPdf.indexOf("ARN");
							int indexOfHyphen=actualMutualfundNameInPdf.indexOf("-", indexOfARN);
							//now we have index of '-' which comes after 'ARN'. So now we look for '[0-9]+)' pattern.
							if(indexOfHyphen!=-1)
							{
								//System.out.println("Index of hyphen="+indexOfHyphen+" indexOfARN="+indexOfARN);
								//get index of first occurrence of digit in ARN no.
								int indexOfFirstDigit=-1;
								int indexOfClosedBracket=actualMutualfundNameInPdf.indexOf(")", indexOfHyphen);
								Pattern p=Pattern.compile("[0-9]");
								String substring=actualMutualfundNameInPdf.substring(indexOfHyphen+1,indexOfClosedBracket+1);
								//System.out.println("SUBSTRING="+substring);
								Matcher matcher=p.matcher(substring);
								if(matcher.find())
								{
									indexOfFirstDigit=matcher.start();
									//System.out.println(")(*Index of first digit="+indexOfFirstDigit);
								}
								if(indexOfFirstDigit!=-1)
								{
									String arnContainingOnlyNumbers=substring.substring(indexOfFirstDigit,substring.indexOf(")"));
									if(arnContainingOnlyNumbers.matches("[0-9]+"))
									{
										String newArn="ARN-"+arnContainingOnlyNumbers;
										holding.setArn(newArn);
									}
								}
							}
						}
						else if(actualMutualfundNameInPdf.contains("Direct"))
						{
							if(actualMutualfundNameInPdf.contains("Advisor:"))
							{
								int indexOfARN=actualMutualfundNameInPdf.indexOf("Advisor:");
								int indexOfClosedBracket=actualMutualfundNameInPdf.indexOf(")",indexOfARN);
								if(indexOfClosedBracket!=-1)
								{
									int indexOfActualArn=indexOfARN+8;//8 represent length of 'Advisor:'
									String arn=actualMutualfundNameInPdf.substring(indexOfActualArn,indexOfClosedBracket).trim();
									holding.setArn(arn);
								}
							}
						}
						holding.setMutualFundName(mutualFundName);
						holding.setCamsCode(camsCode);
					}
					else if(demo[k - 3].substring(0, demo[k - 3].indexOf("-") + 1).matches("[a-zA-Z0-9]+[-]"))
					{
						//System.out.println("!@#$5");
						int camsCodeEndIndex = demo[k - 3].indexOf("-");
						String camsCode = demo[k - 3].substring(0, camsCodeEndIndex);
						String mutualFundName = demo[k - 3].substring(camsCodeEndIndex + 1);
						//as soon as we saw 'ARN' look for '-[0-9]+)' in concatenation of all the lines from demo[k-3] to demo[k-1]
						String actualMutualfundNameInPdf=mutualFundName+demo[k-2]+demo[k-1];
						//System.out.println("ActualMutualfundName()*."+actualMutualfundNameInPdf);
						if(actualMutualfundNameInPdf.contains("ARN"))
						{
							//System.out.println("New String!@# : ");
							int indexOfARN=actualMutualfundNameInPdf.indexOf("ARN");
							int indexOfHyphen=actualMutualfundNameInPdf.indexOf("-", indexOfARN);
							//now we have index of '-' which comes after 'ARN'. So now we look for '[0-9]+)' pattern.
							if(indexOfHyphen!=-1)
							{
								//System.out.println("Index of hyphen="+indexOfHyphen+" indexOfARN="+indexOfARN);
								//get index of first occurrence of digit in ARN no.
								int indexOfFirstDigit=-1;
								int indexOfClosedBracket=actualMutualfundNameInPdf.indexOf(")", indexOfHyphen);
								Pattern p=Pattern.compile("[0-9]");
								String substring=actualMutualfundNameInPdf.substring(indexOfHyphen+1,indexOfClosedBracket+1);
								//System.out.println("SUBSTRING="+substring);
								Matcher matcher=p.matcher(substring);
								if(matcher.find())
								{
									indexOfFirstDigit=matcher.start();
									//System.out.println(")(*Index of first digit="+indexOfFirstDigit);
								}
								if(indexOfFirstDigit!=-1)
								{
									String arnContainingOnlyNumbers=substring.substring(indexOfFirstDigit,substring.indexOf(")"));
									if(arnContainingOnlyNumbers.matches("[0-9]+"))
									{
										String newArn="ARN-"+arnContainingOnlyNumbers;
										holding.setArn(newArn);
									}
								}
							}
						}
						holding.setMutualFundName(mutualFundName);
						holding.setCamsCode(camsCode);
						//System.out.println("this mutualfundname should not come but came camsCode = "+camsCode+" mfName="+mutualFundName);
					}
				}
				if(demo[k].indexOf("(INR) (INR)") != -1)
				{
					if(openingFlag == true)
					{
						if(demo[k + 2].length() >= 11 && demo[k + 2].trim().substring(0, 11).matches("[0-9]{2}-[a-zA-Z]{3}-[0-9]{4}"))
						{
							flag=true;
						}
						else if(demo[k + 1].length() > 11 && demo[k + 1].trim().substring(0, 11).matches("[0-9]{2}-[a-zA-Z]{3}-[0-9]{4}"))
						{
							flag = true;
						}
						else if(demo[k + 1].trim().substring(0, demo[k + 1].length() - 1).matches("[0-9,]+[/.][0-9]+"))
						{
							flag = true;
						}
						else
						{
							flag = false;
						}
					}
					else
					{
						flag = false;
					}
				}
			}
			int start = 0;
			// below for loop helps to find number of transactions under one holding/Mutual Fund
			// condition:number of transactions under one holding might be different here since different parts(date,amount etc) might not be in one transaction
			int lol = 0;
			int sum=0;
			/*for (int j = 0; j < number.length; j++)
			{
				 //System.out.println("number[" + j + "] = " + number[j]);
				 
			}*/
			/*for(int l=0;l<transaction.length;l++){
				System.out.println("Transaction  : "+transaction[l]);
			}*/
			/*for (int j = 0; j < 7; j++)
			{
				 //System.out.println("number[" + j + "] = " + number[j]);
				 
				sum+=number[j];
			}
			for(int ik=sum;ik<sum+number[8];ik++)
				 System.out.println("O T ="+transaction[ik]);*/
			String transaction1[] = new String[transaction.length];
			int index = 0;
			String append = null;
			int j = 0;
			int k = 0, q = 0;
			int indexM = 0;
			String cut[];
			StringBuffer sb;
			indexOfNumber = 0;
			numberOfTransaction = number[0];
			int count = 0;// counts total transactions under one mf then set to 0 for next mutualfund
			int indexOfTransaction = 0;
			// Iterator itr = holdingsList.iterator();
			/*while (itr.hasNext())
			{
				Holdings hol = (Holdings) itr.next();
				System.out.println(hol.getMutualFundName());
				System.out.println(hol.getClosingUnits() + "\t\t" + hol.getFolioNumber());
			}*/
			while (j < number.length && number[j] != -1)
			{
				Holdings h = holdingsList.get(j);
				//System.out.println(h.getFolioNumber()+"----"+h.getFolioNumberString());
				//System.out.println(h.getCamsCode()+"-"+h.getMutualFundName());
				List<String> txList = new ArrayList<String>();
				int l = 0;
				for (l = 0; l < number[j]; l++)
				{
					//System.out.println("!@#$#@!"+transaction[indexOfTransaction + l]);
					String tempTran=transaction[indexOfTransaction + l];
					//System.out.println(tempTran);
					txList.add(transaction[indexOfTransaction + l]);
				}
				//System.out.println(h.getClosingUnits());
				//System.out.println("\n");
				// System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5");
				List<Transactions> transactionList=stringToTransactionsForKarvy(txList);
				if(transactionList!=null && transactionList.size()>0)
				{
					h.setTransactionsList(transactionList);
				}
				// holdingsList.add(j, h);
				indexOfTransaction = indexOfTransaction + l;
				j++;
			}
			//System.out.println("Holding with no transactions");
			for(int l=0;l<holdingsList.size();l++)
			{
				Holdings holding=holdingsList.get(l);
				//we'll count units using units from each transaction for eg (if pdf is from 1-1-2007-5-1-2017 but user has holding of 5000 before these date then it will be shown only as closing units in holding and not as transactions)
				double units=0;
				if(holding.getTransactionsList()==null || holding.getTransactionsList().size()==0)
				{
					holdingsList.remove(l);
					//System.out.println("missing="+holding.getMutualFundName());
					l--;
				}
				else
				{
					//System.out.println("Holding id="+holding.getMutualFundName()+" !@# "+holding.getFolioNumber());
					//System.out.println(holding.getCamsCode()+"  "+holding.getMutualFundName());
					Iterator itr=holding.getTransactionsList().iterator();
					
					while(itr.hasNext())
					{
						Transactions tx=(Transactions)itr.next();
						units+=tx.getUnits();
						//System.out.println(tx.getDate()+"\t"+tx.getAmount()+"\t"+tx.getUnits()+"\t"+tx.getPrice()+" totalUnitsBalance="+units);
					}
					DecimalFormat df=new DecimalFormat(".000000");
					units=Double.valueOf(df.format(units));
					if(units>-.00009 && units<.00009)
						units=0;
					//System.out.println("total units="+units);
				}
				String mfName=holding.getMutualFundName();
				if(mfName!=null)
				{
					int indexOfOpeningBracket=mfName.contains("(Advi")?mfName.indexOf("(Advi"):-1;
					if(indexOfOpeningBracket!=-1)
					{
						holding.setMutualFundName(mfName.substring(0,indexOfOpeningBracket).trim());
					}
					else
					{
						//if '(Advi' is not in mfName string in pdf then we have to explicitly remove 'Registrar' from mfName
						int indexOfRegistrar=mfName.contains("Registrar")?mfName.indexOf("Registrar"):-1;
						if(indexOfRegistrar!=-1)
						{
							holding.setMutualFundName(mfName.substring(0,indexOfRegistrar).trim());
						}
					}
				}
				holding.setClosingUnits(units);
			}
			return holdingsList;
		}
		
		public List<Transactions> stringToTransactionsForKarvy(List<String> tx) throws ParseException
		{
			List<Transactions> transactions = new ArrayList<Transactions>();
			// String datePattern = "[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}";
			// String numRegex = "\\([0-9]+(\\.)[0-9]{2,3}\\)|[0-9]+(\\.)[0-9]{2,3}";
			// Regex for standard transaction
			
			//changes in txPattern1
			//a)sometimes price comes with value as 21.100000321 which has upto 9 decimal places hence replacing {2,4} with {2,9} in price regex of txPattern1
			//changes in txPattern2
			//a)sometimes price comes with value as 21.100000321 which has upto 9 decimal places hence replacing {2,4} with {2,9} in price regex of txPattern2
			//changes in txPattern3
			//a)sometimes price comes with value as 21.100000321 which has upto 9 decimal places hence replacing {2,4} with {2,9} in price regex of txPattern3
			
			String txPattern1 = "^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(.)+(\\([0-9]+((\\.)[0-9]{2,4})*\\)|[0-9]+((\\.)[0-9]{2,4})*|\\-[0-9]+((\\.)[0-9]{2,4})*)(\\s)+(\\([0-9]+((\\.)[0-9]{2,4})*\\)|[0-9]+((\\.)[0-9]{2,4})*|\\-[0-9]+((\\.)[0-9]{2,4})*)(\\s)+(\\([0-9]+(\\.)[0-9]{2,9}\\)|[0-9]+(\\.)[0-9]{2,9})(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})(\\s)*$";
			// Regex for transaction where unit balance in missing
			String txPattern2 = "^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(.)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4}|\\-[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4}|\\-[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,9}\\)|[0-9]+(\\.)[0-9]{2,9})(\\s)*$";
			// Regex for transaction where text and unit balance in missing
			String txPattern3 = "^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4}|\\-[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4}|\\-[0-9]+(\\.)[0-9]{2,4})(\\s)+(\\([0-9]+(\\.)[0-9]{2,9}\\)|[0-9]+(\\.)[0-9]{2,9})(\\s)*$";
			
			//for karvy new type of transaction arrives for eg transaction in two lines as follows
			//05-Mar-2018 80.018 37.4916  - this represent txpattern4 - as date units price
			//Purchase- Systematic 3,000.00 80.018 - this represent txPattern5 as - text amount unitbalance
			//since these txpattern4 and 5 does not satisfy any of the txPatterns1,2,3 we can safely insert it after them
			//TODO used * in txpatern4 for amount
			String txPattern4="^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(\\([0-9]+((\\.)[0-9]{2,4})*\\)|[0-9]+((\\.)[0-9]{2,4})*|\\-[0-9]+((\\.)[0-9]{2,4})*)(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})";
			String txPattern5="^(.)+(\\s)+(\\([0-9]+((\\.)[0-9]{2,4})*\\)|[0-9]+((\\.)[0-9]{2,4})*|\\-[0-9]+((\\.)[0-9]{2,4})*)(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})$";
			
			//this txPattern is copy of txPattern3 except extra regex added at the end for following transaction
			// 20-Feb-2017 -49000.00 -262.4950 186.67 0.000
			String txPattern6 = "^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(\\([0-9]+((\\.)[0-9]{2,4})*\\)|[0-9]+((\\.)[0-9]{2,4})*|\\-[0-9]+((\\.)[0-9]{2,4})*)(\\s)+(\\([0-9]+((\\.)[0-9]{2,4})*\\)|[0-9]+((\\.)[0-9]{2,4})*|\\-[0-9]+((\\.)[0-9]{2,4})*)(\\s)+(\\([0-9]+(\\.)[0-9]{2,9}\\)|[0-9]+(\\.)[0-9]{2,9})(\\s)([0-9]+(\\.)[0-9]{2,9})$";
			
			//similar to txPattern4 and txPattern5 exception extra date where second date is the actual date
			//20-Apr-2017 25-Apr-2017 (4,009.204) 34.92 - where 25 april is the actual date
			//*Redemption - ELECTRONIC PAYMENT (140,000.00) 10,215.988 - there is no change compare to txPattern5
			String txPattern7="^[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+[0-9]{2}\\-[A-Za-z]{3}\\-[0-9]{4}(\\s)+(\\([0-9]+((\\.)[0-9]{2,4})*\\)|[0-9]+((\\.)[0-9]{2,4})*|\\-[0-9]+((\\.)[0-9]{2,4})*)(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})";
			String txPattern8="^(.)+(\\s)+(\\([0-9]+((\\.)[0-9]{2,4})*\\)|[0-9]+((\\.)[0-9]{2,4})*|\\-[0-9]+((\\.)[0-9]{2,4})*)(\\s)+(\\([0-9]+(\\.)[0-9]{2,4}\\)|[0-9]+(\\.)[0-9]{2,4})$";
			
			Iterator<String> txI = tx.iterator();
			while (txI.hasNext())
			{
				String amount = "", price = "", units = "", date = "", content = "";
				double amt = 0, p = 0, u = 0;
				Date txDate = null;
				String tr = txI.next();
				//System.out.println(tr+"#@!");
				tr = tr.replaceAll(",", "");
				tr = tr.trim();
				//System.out.println("transaction=" + tr);
				boolean flag = false;
				
				if(tr.matches(txPattern6))
				{
					//System.out.println("tran="+tr+" txPattern6");
					//Check last 3 value
					//System.out.println(tr + " - Text missing");
					tr=tr.substring(0, tr.lastIndexOf(" "));
					//System.out.println(tr);
					price = tr.substring(tr.lastIndexOf(" "), tr.length());
					tr = tr.substring(0, tr.lastIndexOf(" "));
					units = tr.substring(tr.lastIndexOf(" "), tr.length());
					tr = tr.substring(0, tr.lastIndexOf(" "));
					amount = tr.substring(tr.lastIndexOf(" "), tr.length());
					date = tr.substring(0, 11);
					flag = true;
				}			
				else if(tr.matches(txPattern1))
				{
					//System.out.println("txPattern1");
					// Check last 4 value, ignore 4th, take other 3
					//System.out.println(tr + " - Standard pattern");
					tr = tr.substring(0, tr.lastIndexOf(" "));
					price = tr.substring(tr.lastIndexOf(" "), tr.length());
					tr = tr.substring(0, tr.lastIndexOf(" "));
					units = tr.substring(tr.lastIndexOf(" "), tr.length());
					tr = tr.substring(0, tr.lastIndexOf(" "));
					amount = tr.substring(tr.lastIndexOf(" "), tr.length());
					tr=tr.substring(0,tr.lastIndexOf(" "));
					content = tr.substring(tr.indexOf(" ")+1,tr.length());
					date = tr.substring(0, 11);
					flag = true;
				}
				else if(tr.matches(txPattern2))
				{
					//System.out.println("txPattern2");
					// Check last 3 value
					//System.out.println(tr + "!@#$%^&* - Unit balance missing");
					
					if(tr.substring(12, (12+11)).equals("Bonus Units"))
					{
						//System.out.println("Indside Bonus Points.");
						tr=tr.substring(0, tr.lastIndexOf(" "));
						units = tr.substring(tr.lastIndexOf(" "), tr.length());
						date = tr.substring(0, 11);
						/*System.out.print("Units="+units+" date="+date);*/
						price=".1";
						amount=String.valueOf(Double.valueOf(units)*Double.valueOf(price));
						/*System.out.println("price="+price+" amount="+amount);*/
						flag = true;
					}
					else
					{
						price = tr.substring(tr.lastIndexOf(" "), tr.length());
						tr = tr.substring(0, tr.lastIndexOf(" "));
						units = tr.substring(tr.lastIndexOf(" "), tr.length());
						tr = tr.substring(0, tr.lastIndexOf(" "));
						amount = tr.substring(tr.lastIndexOf(" "), tr.length());
						date = tr.substring(0, 11);
						flag = true;
					}
				}
				else if(tr.matches(txPattern3))
				{
					//System.out.println("txPattern3");
					// Check last 3 value
					//System.out.println(tr + " - Text and Unit balance missing");
					price = tr.substring(tr.lastIndexOf(" "), tr.length());
					tr = tr.substring(0, tr.lastIndexOf(" "));
					units = tr.substring(tr.lastIndexOf(" "), tr.length());
					tr = tr.substring(0, tr.lastIndexOf(" "));
					amount = tr.substring(tr.lastIndexOf(" "), tr.length());
					date = tr.substring(0, 11);
					flag = true;
				}
				else if(tr.matches(txPattern4))
				{
					//System.out.println("TxPattern 4 matched");
					String tempTran=tr;
					if(txI.hasNext())
					{
						tr = txI.next();
						tr = tr.replaceAll(",", "");
						//System.out.println("tr for 5="+tr);
						if(tr.matches(txPattern5))
						{
							//System.out.println("TxPattern 5 matched = "+tr);
							price = tempTran.substring(tempTran.lastIndexOf(" "), tempTran.length());
							tempTran = tempTran.substring(0, tempTran.lastIndexOf(" "));
							units = tempTran.substring(tempTran.lastIndexOf(" "), tempTran.length());
							date = tempTran.substring(0, 11);
							tr = tr.substring(0,tr.lastIndexOf(" "));
							amount = tr.substring(tr.lastIndexOf(" "), tr.length());
							flag=true;
						}
					}
				}
				else if(tr.matches(txPattern7))
				{
					//System.out.println("TxPattern 7 matched");
					String tempTran=tr;
					if(txI.hasNext())
					{
						tr = txI.next();
						tr = tr.replaceAll(",", "");
						//System.out.println("tr for 8="+tr);
						if(tr.matches(txPattern8))
						{
							//System.out.println("TxPattern 8 matched = "+tr);
							price = tempTran.substring(tempTran.lastIndexOf(" "), tempTran.length());
							tempTran = tempTran.substring(0, tempTran.lastIndexOf(" "));
							units = tempTran.substring(tempTran.lastIndexOf(" "), tempTran.length());
							tempTran = tempTran.substring(0, tempTran.lastIndexOf(" "));
							date = tempTran.substring(tempTran.lastIndexOf(" "), tempTran.length());
							tr = tr.substring(0,tr.lastIndexOf(" "));
							amount = tr.substring(tr.lastIndexOf(" "), tr.length());
							flag=true;
						}
					}
				}
				else
				{
					// System.out.println(tr + " - Does not match");
				}
				// System.out.println(date);
				if(flag == true)
				{
					Transactions transaction = new Transactions();
					txDate = new SimpleDateFormat("dd-MMM-yyyy").parse(date);
					if(amount.contains("("))
					{
						amount = amount.replace("(", "-");
						amount = amount.replace(")", "");
						amt = Double.valueOf(amount);
						units = units.replace("(", "-");
						units = units.replace(")", "");
						u = Double.valueOf(units);
						p = Double.valueOf(price);
						
						transaction.setAmount(amt);
						transaction.setPrice(p);
						transaction.setUnits(u);
						transaction.setDate(txDate);
						transaction.setType("2");
						transactions.add(transaction);
					}
					else
					{
						amt = Double.valueOf(amount);
						u = Double.valueOf(units);
						p = Double.valueOf(price);
						//if(amt>1 || amt<-1)
						{
							transaction.setAmount(amt);
							transaction.setPrice(p);
							transaction.setUnits(u);
							transaction.setDate(txDate);
							transaction.setType("1");
							transactions.add(transaction);
						}
					}
					//System.out.println(date+"\t"+amt+"\t"+u+"\t"+p);
					
					//Below arn code should stay only on FUNDEXPERT
					Pattern pattern=Pattern.compile("((ARN[\\-])([0-9]+))(([\\/][a-zA-Z0-9]+))|((ARN[\\-])([0-9]+))");
					Matcher m=pattern.matcher(content);
					//System.out.println(m.groupCount());
					while(m.find())
					{
						/*System.out.println("Match found for ARN : "+m.group(3)+"  "+m.group(8));*/
						if(m.group(0).matches("(ARN[\\-][0-9]+)"))
						{
							transaction.setArn(m.group(3));
						}
						else if(m.group(1).matches("(ARN[\\-][0-9]+)"))
						{
							transaction.setArn(m.group(8));
						}
						
						//System.out.println(m.group(0)+" "+m.group(1)+" "+m.group(2)+" $ "+m.group(3)+" $ !"+m.group(4)+"@ $  "+m.group(5)+" "+m.group(6)+" "+m.group(7)+" "+m.group(8));
					}
					//System.out.println(txDate + "\t\t" + content + "\t\t" + amt + "\t\t\t" + u + "\t\t\t" + p);
				}
			}
			return transactions;
		}
	
	}

	public boolean saveStockPdf(String pdfPath,String emailId,String pan,long consumerId) throws Exception
	{
		File readFile;
		File[] files;
		int count=0;
		boolean thisMethodIsCalledFromApi=false;
		try
		{
			stockReadDir=config.getProperty(Config.STOCK_PDF_NOT_READ);
			stockSaveDir=config.getProperty(Config.STOCK_PDF_READ);
			stockNoUserDir=config.getProperty(Config.STOCK_NO_USER_PDF);
			hSession=HibernateBridge.getSessionFactory().openSession();
			if(pdfPath==null)
			{
				readFile=new File(stockReadDir);
				files=readFile.listFiles();
				if(files==null || files.length==0)
					return true;
				Arrays.sort(files, new Comparator<File>(){
				    public int compare(File f1, File f2)
				    {
				        return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
				    } 
				    });
				//System.out.println("files.length="+files.length);
			}
			else
			{
				//System.out.println("PDF PATH="+pdfPath);
				if(pdfPath.equals(""))
				{
					throw new ValidationException("Not a valid pdf path.");
				}
				else if(!pdfPath.substring(pdfPath.length()-4).equalsIgnoreCase(".pdf"))
				{
					throw new ValidationException("Not a valid pdf file.");
				}
				else if(!new File(pdfPath).isFile())
				{
					throw new ValidationException("Not a valid pdf path.");
				}
				
				files = new File[1];
				files[0]=new File(pdfPath);
				String fileName=files[0].getName();
				fileName=fileName.replace("-", "_");
				System.out.println("STOCK PDF FileName="+fileName);
				File newFile=new File(stockReadDir+File.separator+emailId+"-"+fileName);
				Path newPath=Files.copy(files[0].toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
				
				if(newPath!=null)
				{
					System.out.println("Success in Copying File from stock pdf path to stockReadDir : for email id="+emailId+" targetPath : "+newPath);
					if(files[0].delete());
						System.out.println("Deleting stock file = "+fileName);
					files[0]=newFile;
				}
				else
				{
					System.out.println("Not able to copy pdf from stock pdf path to stockReadDir : for email id="+emailId+"  targetPath : "+newFile.getAbsolutePath());
					return false;
				}
				thisMethodIsCalledFromApi=true;
			}
			
			for (File file : files)
			{	
				count++;
				String savedName = "";
				String path = "";
				String error="";
				try
				{
					if(file.isFile())
					{
						String fileName = file.getName();
						System.out.println("File Name "+fileName);
						String[] emailArray = fileName.split("-");	
						String email=emailArray[0];
						List<User> userList=null;
						boolean passwordMatch=true;
						if(!thisMethodIsCalledFromApi)
						{
							userList=hSession.createQuery("from User where email=? and consumerId=?").setString(0,email).setLong(1,consumerId).list();
						}	
						else
						{
							userList=hSession.createQuery("from User where email=? and pan=? and consumerId=?").setString(0,email).setString(1,pan).setLong(2, consumerId).list();
							//coming from api call
							if(userList==null || userList.size()<1)
							{
								tx=hSession.beginTransaction();
								NoUserFound noUserFound=new NoUserFound();
								noUserFound.setEmail(email);
								noUserFound.setReason("No User found with email.");
								noUserFound.setStock(true);
								noUserFound.setCreatedOn(new Date());
								hSession.save(noUserFound);
								tx.commit();
								throw new ValidationException("No User available with given email and pan.");
							}
						}
						//coming from email forwarding 
						if(userList==null || userList.size()==0)
						{
							tx=hSession.beginTransaction();
							NoUserFound noUserFound=new NoUserFound();
							noUserFound.setEmail(email);
							noUserFound.setReason("No User found with email.");
							noUserFound.setStock(true);
							noUserFound.setCreatedOn(new Date());
							hSession.save(noUserFound);
							tx.commit();
							System.out.println("No user found.");
							//move current file to stockNoUserDir
							fileName+="-"+pdfFromWhichApiRequest+".pdf";
							File f=new File(stockNoUserDir+File.separator+fileName);
							boolean success=file.renameTo(f);
							file.delete();
							continue;
						}
						long userId=0;
						List<StocksHoldings> stocksHoldingsList=null;
						for(User user:userList)
						{
							StockPdfParser parser=new StockPdfParser(file.getAbsolutePath(),user.getPan());
							try
							{
								userId=user.getId();
								pan=user.getPan();
								stocksHoldingsList=parser.getStocksHoldingsList();
								passwordMatch=true;
							}
							catch(BadPasswordException bpe)
							{
								bpe.printStackTrace();
								passwordMatch=false;
								error=bpe.getMessage();
								continue;
							}
							catch(FundexpertException fe)
							{
								fe.printStackTrace();
								passwordMatch=false;
								error=fe.getMessage();
								continue;
							}
							catch(InvalidPdfException ipe)
							{
								ipe.printStackTrace();
								passwordMatch=false;
								error="Not a valid pdf format.";
								continue;
							}
							catch(Exception e)
							{
								e.printStackTrace();
								throw e;
							}
							break;
						}
						System.out.println("passwordmatch = "+passwordMatch+" path = "+path);
						//below to be called only in case of email reader process.
						if(!passwordMatch && pdfPath==null)
						{
							//call api to edelweiss to get the pan since we did't got the pan saved in our DB
							System.out.println("Getting pan from edelweiss using email.");
							Utilities utilities=new Utilities();
							for(User user:userList)
							{
								pan=utilities.getPanForEdelweissEmail(user.getEmail());
								if(pan==null)
								{
									System.out.println("Didn't get pan from edelweiss for email = "+user.getEmail());
									continue;
								}
								StockPdfParser parser=new StockPdfParser(file.getAbsolutePath(),pan);
								try
								{
									userId=user.getId();
									stocksHoldingsList=parser.getStocksHoldingsList();
									passwordMatch=true;
									
									final String pan1=pan;
									new Thread(new Runnable() {
										@Override
										public void run() {
											//tell edelweiss that password received from them is correct
											utilities.postEmailPanCombination(user.getEmail(), pan1);
										}
									}).start();
								}
								catch(BadPasswordException bpe)
								{
									bpe.printStackTrace();
									passwordMatch=false;
									error=bpe.getMessage();
									continue;
								}
								catch(FundexpertException fe)
								{
									fe.printStackTrace();
									passwordMatch=false;
									error=fe.getMessage();
									continue;
								}
								catch(InvalidPdfException ipe)
								{
									ipe.printStackTrace();
									passwordMatch=false;
									error="Not a valid pdf format.";
									continue;
								}
								catch(Exception e)
								{
									e.printStackTrace();
									throw e;
								}
								break;
							}
						}
						if(!passwordMatch)
						{
							//move current file to stockNoUserDir
							fileName+="-"+pdfFromWhichApiRequest+".pdf";
							File f=new File(stockNoUserDir+File.separator+fileName);
							System.out.println("Password not match for stock pdf for emailId = "+email+" and dir. = "+f.getAbsolutePath());
							boolean success=file.renameTo(f);
							file.delete();
							
							//Set field PortfolioUpload
							tx=hSession.beginTransaction();
							PortfolioUpload pu=new PortfolioUpload();
							pu.setFile(file.getName());
							pu.setPassword(pan);
							pu.setStock(true);
							pu.setUploadTime(new Date());
							pu.setUserId(userId);
							pu.setSuccess(false);
							pu.setError(error);
							hSession.save(pu);
							tx.commit();
							throw new FundexpertException(error);
						}
						StocksPortfolioImportController stocksPortfolioImportController = null;
						try
						{
							stocksPortfolioImportController = new StocksPortfolioImportController();
							stocksPortfolioImportController.sFactory(userId, stocksHoldingsList);
							//make a new Directory with directoryName as PAN if not exists already
							File panDirectory=new File(stockSaveDir+File.separator+pan);
							if(!panDirectory.exists())
							{
								panDirectory.mkdir();
							}
							String saveFileNameAs=email+"-"+pan+"-"+emailArray[1]+"-"+pdfFromWhichApiRequest+".pdf";
							file.renameTo(new File(panDirectory+File.separator+saveFileNameAs));
							file.delete();
							
							//Set field PortfolioUpload
							tx=hSession.beginTransaction();
							PortfolioUpload pu=new PortfolioUpload();
							pu.setFile(file.getName());
							pu.setPassword(pan);
							pu.setUploadTime(new Date());
							pu.setUserId(userId);
							pu.setStock(true);
							pu.setSuccess(true);
							hSession.save(pu);
							tx.commit();
							
						}
						catch(Exception e)
						{
							e.printStackTrace();
							throw e;
						}
					}
				}
				catch(FundexpertException fe)
				{
					if(thisMethodIsCalledFromApi)
						throw fe;
				}
				catch(Exception e)
				{
					e.printStackTrace();
					if(thisMethodIsCalledFromApi)
						throw e;
				}
			}
			return true;
		}
		catch(FundexpertException fe)
		{
			fe.printStackTrace();
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			hSession.close();
		}
	}

	public class StockPdfParser
	{
		String path="";
		String password="";
		List<StocksHoldings> stocksHoldingsList=new ArrayList<StocksHoldings>();
		
		public StockPdfParser(String path, String password)
		{
			this.path = path;
			this.password = password;
		}
		
		public List getStocksHoldingsList() throws BadPasswordException, InvalidPdfException, Exception
		{
			/*String path = "F:/Equity PDF/New folder (3)/NSDLe-CAS_107542584_NOV_2017.PDF";
			String password = "AOVPS6703D";*/
			// TODO Auto-generated method stub
			String myLine = "";
			PdfReader reader = null;
			InputStream is=null;
			try
			{
				System.out.println(path+" pass = "+password);
				File f=new File(path);
				is=new FileInputStream(f);
				reader = new PdfReader(is, password.getBytes());
				reader.unethicalreading = true;
				int nop = reader.getNumberOfPages();
				for (int i = 1; i <= nop; i++)
				{
					String pdfLine = PdfTextExtractor.getTextFromPage(reader, i);
					//do not remove this "\n" - used to separate the 1 line at end of page from 1 line at the begining of next page
					myLine += "\n"+pdfLine;
					//System.out.println("pdfLine=" + pdfLine);
				}
				reader.close();
				// Matcher matcher = Pattern.compile("\r|\n|\r\n|\n\r").matcher(myLine);
				String[] equityLines = myLine.split("\r|\n|\r\n|\n\r");
				List<String> lines = Arrays.asList(equityLines);
				String firstLineInPdf=lines.get(1);
				String secondLineInPdf=lines.get(2);
				if(firstLineInPdf.equals("Central Depository Services (India) Limited"))
				{
					return readCDSLFile(lines);
				}
				else if(firstLineInPdf.equals("Consolidated Account Statement Consolidated Account Statement") && secondLineInPdf.contains("NSDL"))
				{
					readNSDLFileForDifferentEquities(lines);//readNSDLFile(lines);
					readNSDLFile(lines);
					return stocksHoldingsList;
					//list.addAll(readNSDLFileForDifferentEquities(lines));
				}
				else
				{
					throw new FundexpertException("Please upload CAS statements only.");
				}
			}
			catch(FundexpertException fe)
			{
				fe.printStackTrace();
				throw fe;
			}
			catch(InvalidPdfException ipe)
			{
				ipe.printStackTrace();
				throw new InvalidPdfException("Not a pdf file.");
			}
			catch(BadPasswordException bpe)
			{
				bpe.printStackTrace();
				throw new BadPasswordException("Please upload CAS linked with PAN.");
			}
			finally
			{
				if(is!=null)
				{
					is.close();
				}
			}
		}
		
		//equities under NSDL demat account have different syntax than cdsl demat account which is true at the time of writing readNSDLFileForDifferentEquities() hence this assumption might fall false in future
		//readNSDLFile is for equities under NSDL demat a/c
		private List<StocksHoldings> readNSDLFile(List<String> data) throws Exception
		{
			List<String> actualEquityLines = new ArrayList<String>();
			Iterator itr = data.iterator();
			boolean equitySharesFlag = false;
			int i=0;
			String dematId="",clientId="";
			while (itr.hasNext())
			{
				String pdfLine = (String) itr.next();
				
				if(equitySharesFlag == true)
				{
					if(pdfLine.length() >= 9 && pdfLine.substring(0, 9).toLowerCase().equals("sub total"))
					{
						equitySharesFlag = false;
						//considering there's only one section of Equity Shares in one nsdl pdf we will break the loop
						
						//since some files contains more than 1 equities section we will not break out of the loop as it was previously in above comment
						//break;
					}
					else
					{
						actualEquityLines.add(pdfLine);
						//System.out.println("pdfLine:: " + pdfLine);
					}
				}
				
				if(pdfLine.toLowerCase().equals("equity shares") && equitySharesFlag == false && data.get(i+1).length()>12?data.get(i+1).substring(0, 12).equals("ISIN Company"):false)
				{
					//System.out.println("Equities is true.nextLine="+data.get(i+1).substring(0, 12).equals("ISIN Company"));
					equitySharesFlag = true;
					int goBackwards=i;
					String dematLine="";
					//System.out.println("previousString="+data.get(goBackwards));
					while(!data.get(goBackwards).equals(" NSDL Demat Account"))
					{
						//5 is just a number we could also use 0
						//5 will work bcoz we have lots of unnecessary lines in nsdl pdf at inital pages
						if(goBackwards<5)
						{
							throw new Exception("Content Format not supported.");
						}
						else
						{
							//System.out.println(data.get(goBackwards));
							goBackwards--;
							continue;
						}
					}
					int countLines=0;
					while(!data.get(++goBackwards).substring(0,6).equals("DP ID:"))
					{
						countLines++;
						if(countLines>10)
						{
							countLines=0;
							break;
						}
					}
					if(countLines!=0)
					{
						//System.out.println("demat line="+data.get(goBackwards));
						dematLine=data.get(goBackwards);
						int dematIdIndex=dematLine.indexOf(":")+2;
						dematId=dematLine.substring(dematIdIndex,dematLine.indexOf(" ", dematIdIndex));
						actualEquityLines.add("#DEMAT ID:"+dematId);
						//System.out.println("demat id="+dematId);
						String dematName=data.get(goBackwards-2);
						actualEquityLines.add("#DEMAT NAME:"+dematName);
						//System.out.println("@Demat Name="+dematName);
					}
					else
					{
						actualEquityLines.add("#DEMAT ID:");
						actualEquityLines.add("#DEMAT NAME:");
					}
				}
				i++;
			}
			String isinString="";
			String stockSymbolString="";
			String companyNameString="",secondPartOfCompanyNameString="";
			Double nSharesDouble=0.0;
			Double marketPriceDouble=0.0;
			
			/*List<String> isin = new ArrayList();
			List<String> stockForm = new ArrayList();
			List<String> companyName = new ArrayList();
			List<String> faceValue = new ArrayList();
			List<String> nShares = new ArrayList();
			List<String> marketPrice=new ArrayList();*/
			
			// where one,two,three,four,five represents spaces b/w two column values
			int spaceIndex = 0, oneSpace = 0, twoSpace = 0, threeSpace = 0, fourSpace = 0, fiveSpace = 0;
			String currentDematId="",currentDematName="";
			itr = actualEquityLines.iterator();
			int noOfStocks=0;
			while (itr.hasNext())
			{
				String line = (String) itr.next();
				int counter = 0;
				List<Integer> indexOfSpaces = new ArrayList();
				if(line.contains("#DEMAT ID:"))
				{
					if(!currentDematId.equals(line.substring(10)))
					{
						currentDematId=line.substring(10);
					}
				}
				if(line.contains("#DEMAT NAME:"))
				{
					if(!currentDematId.equals(line.substring(12)))
					{
						currentDematName=line.substring(12);
					}
				}
				
				if(line.substring(0, 3).equals("INE") || line.substring(0, 3).equals("INF") || line.substring(0, 3).equals("IN9") || line.substring(0, 3).equals("IN0"))
				{
					oneSpace = line.indexOf(" ");
					spaceIndex = oneSpace;
					indexOfSpaces.add(oneSpace);
					//isin.add(line.substring(0, oneSpace));

					// converting line of format(INE527H01019 UFO MOVIEZ INDIA LIMITED 10.00 240 441.45 1,05,948.00Consolidated Account Statement Consolidated Account Statement) from to(INE527H01019 UFO MOVIEZ INDIA LIMITED 10.00 240 441.45 1,05,948.00)
					if(!line.substring(line.length() - 4, line.length() - 1).matches("[0-9](\\.)[0-9]{1,2}"))
					{
						int indexOfFirstDigitFromRight = line.length() - 1;
						int l = 0;

						for (l = line.length() - 1; l > 0; l--)
						{
							if(String.valueOf(line.charAt(l)).matches("[0-9]"))
							{
								indexOfFirstDigitFromRight = l;
								break;
							}
						}
						line = line.substring(0, l + 1);
					}
					// this while loop stores the indexes of spaces present in a line hence starting from back we will get spaces betwen two columns
					while (spaceIndex >= 0 && spaceIndex <= line.length())
					{
						spaceIndex = line.indexOf(" ", spaceIndex + 1);

						if(spaceIndex != -1)
						{
							indexOfSpaces.add(spaceIndex);
						}
					}
					int size = indexOfSpaces.size() - 1;
					while (counter != 4)
					{
						switch (counter)
						{
							case 0:
								fiveSpace = indexOfSpaces.get(size);
								size--;
								counter++;
								break;
							case 1:
								fourSpace = indexOfSpaces.get(size);
								size--;
								counter++;
								break;
							case 2:
								threeSpace = indexOfSpaces.get(size);
								size--;
								counter++;
								break;
							case 3:
								twoSpace = indexOfSpaces.get(size);
								size--;
								counter++;
								break;
						}

					}
					//System.out.print(line.substring(0, oneSpace) + ",,,,");
					if(itr.hasNext())
					{
						String lineAfterINE = (String) itr.next();
						if(lineAfterINE.contains("."))
						{
							stockSymbolString=lineAfterINE.substring(0, lineAfterINE.indexOf("."));
							if(lineAfterINE.contains(" "))
							{
								secondPartOfCompanyNameString=lineAfterINE.substring(lineAfterINE.indexOf(" "));
								if(secondPartOfCompanyNameString.toLowerCase().contains("trading"))
									secondPartOfCompanyNameString.replaceAll("trading", "");
								if(secondPartOfCompanyNameString.contains("-"))
									secondPartOfCompanyNameString=secondPartOfCompanyNameString.substring(0,secondPartOfCompanyNameString.indexOf("-"));
							}
							//stockForm.add(lineAfterINE.substring(0, lineAfterINE.indexOf(".")));
							//System.out.print(lineAfterINE.substring(0, lineAfterINE.indexOf(".")) + ",,,,");
						}
						else if(lineAfterINE.toLowerCase().contains("trading"))
						{
							//this case considers when Trading Suspended is found under the ISIN value
							System.out.print("*******");
							stockSymbolString="Trading Suspended";
							//stockForm.add("*******");
							if(!lineAfterINE.toLowerCase().contains("suspended"))
							{
								if(itr.hasNext())
									itr.next();
							}
						}
						else
						{
							if(lineAfterINE.toLowerCase().contains("page"))
							{
								int counterIndex = 0;
								while (counterIndex != 4)
								{
									itr.hasNext();
									// System.out.println(lineAfterINE + "=lineAfterINE");
									lineAfterINE = (String) itr.next();
									counterIndex++;
								}
							}
							else
							{
								//considering only 1 pdf there can be only 2 strings below ISIN value i.e company name(eg.TCS.NSE) or Trading(in which suspended is in next line to Trading or in the same line)
								while ((lineAfterINE.contains(".")) || lineAfterINE.toLowerCase().contains("trading"))
								{
									//System.out.println(lineAfterINE + "vikas");
									itr.hasNext();
									lineAfterINE = (String) itr.next();
									//System.out.println(lineAfterINE + "vikas&&&&&&&%%%%%%%");
								}
							}
							if(lineAfterINE.contains("."))
							{
								stockSymbolString=lineAfterINE.substring(0, lineAfterINE.indexOf("."));
								if(lineAfterINE.contains(" "))
								{
									secondPartOfCompanyNameString=lineAfterINE.substring(lineAfterINE.indexOf(" "));
									if(secondPartOfCompanyNameString.toLowerCase().contains("trading"))
										secondPartOfCompanyNameString.replaceAll("trading", "");
									if(secondPartOfCompanyNameString.contains("-"))
										secondPartOfCompanyNameString=secondPartOfCompanyNameString.substring(0,secondPartOfCompanyNameString.indexOf("-"));
								}
								
								//stockForm.add(lineAfterINE.substring(0, lineAfterINE.indexOf(".")));
								//System.out.print(lineAfterINE.substring(0, lineAfterINE.indexOf(".")) + ",,,,");
							}
							else if(lineAfterINE.toLowerCase().contains("trading"))
							{
								//System.out.print("*******");
								stockSymbolString="Trading Suspended";
								//stockForm.add("*******");
								if(!lineAfterINE.toLowerCase().contains("suspended"))
								{
									if(itr.hasNext())
										itr.next();
								}
							}
						}
					}
					companyNameString=line.substring(oneSpace + 1,twoSpace)+secondPartOfCompanyNameString;
					marketPriceDouble=Double.parseDouble((line.substring(fourSpace + 1, fiveSpace)).replace(",",""));
					nSharesDouble=Double.parseDouble((line.substring(threeSpace + 1, fourSpace).replace(",", "")));
					
					StocksHoldings stocksHolding=new StocksHoldings();
					stocksHolding.setIsinCode(line.substring(0, oneSpace));
					
					stocksHolding.setMarketPrice(marketPriceDouble);
					stocksHolding.setNumberOfShares(nSharesDouble);
					stocksHolding.setScName(stockSymbolString);
					stocksHolding.setCompanyName(companyNameString);
					stocksHolding.setDematId(currentDematId);
					stocksHolding.setDematName(currentDematName);
					stocksHoldingsList.add(stocksHolding);
					noOfStocks++;
					
					
					/*String temp = "";
					temp = line.substring(0, oneSpace);
					isin.add(temp);
					System.out.print(temp + ",,,,,");
					temp=line.substring(oneSpace + 1,twoSpace);
					companyName.add(temp);
					System.out.print(temp+ ",,,,,");
					temp = line.substring(twoSpace + 1, threeSpace);
					faceValue.add(temp);
					System.out.print(temp + ",,,,,");
					temp = line.substring(threeSpace + 1, fourSpace);
					nShares.add(temp);
					System.out.print(temp + ",,,,");
					temp = line.substring(fourSpace + 1, fiveSpace);
					marketPrice.add(temp);
					System.out.println(temp);*/
				}
			}
			System.out.println("Total stocks in readNSDLFile = "+noOfStocks);
			return stocksHoldingsList;
		}
		//readNSDLFileForDifferentEquities is for equities under CDSL demat account
		private List<StocksHoldings> readNSDLFileForDifferentEquities(List<String> data) throws Exception
		{
			List<String> actualEquityLines = new ArrayList<String>();
			boolean equitiesAndIsinSecurityFlag = false;
			try
			{
				Iterator itr = data.iterator();
				for(int i=0;i<data.size();i++)
				{
					String line=data.get(i);
					//System.out.println("1@#"+line);
					
					if(equitiesAndIsinSecurityFlag == true)
					{
						if(line.length() >= 9 && line.substring(0, 9).toLowerCase().equals("sub total") && line.substring(10, line.length()).matches("[0-9\\,]*(\\.)[0-9]*"))
						{
							equitiesAndIsinSecurityFlag = false;
							//considering there's only one section of Equity Shares in one nsdl pdf we will break the loop
							
							//since some files contains more than 1 equities section we will not break out of the loop as it was previously in above comment
							//break;
						}
						else
						{
							actualEquityLines.add(line);
							//System.out.println("pdfLine:: " + line);
						}
					}
					
					if(line.equals("Equities (E)") && equitiesAndIsinSecurityFlag == false && data.get(i+1).length()>13?data.get(i+1).substring(0, 13).equals("ISIN SECURITY"):false)
					{
						int goBackwards=i;
						String dematLine="",dematId="";
						//System.out.println("previousString="+data.get(goBackwards));
						equitiesAndIsinSecurityFlag=true;
						int accountHolderIndex=0;
						while(!data.get(goBackwards).equals(" CDSL Demat Account"))
						{
							//5 is just a number we could also use 0
							//5 will work bcoz we have lots of unnecessary lines in nsdl pdf at inital pages
							if(data.get(goBackwards).toUpperCase().equals("ACCOUNT HOLDER"))
								accountHolderIndex=goBackwards;
							if(goBackwards<5)
							{
								throw new Exception("Content Format not supported.");
							}
							else
							{
								//System.out.println(data.get(goBackwards));
								goBackwards--;
								continue;
							}
						}
						int countLines=0;
						while(!data.get(++goBackwards).substring(0,6).equals("DP ID:"))
						{
							countLines++;
							if(countLines>10)
							{
								countLines=0;
								break;
							}
						}
						if(countLines!=0)
						{
							//System.out.println("demat line="+data.get(goBackwards));
							dematLine=data.get(goBackwards);
							int dematIdIndex=dematLine.indexOf(":")+2;
							dematId=dematLine.substring(dematIdIndex,dematLine.indexOf(" ", dematIdIndex));
						
							actualEquityLines.add("#DEMAT ID:"+dematId);
							//System.out.println("demat id="+dematId);
							String dematName="";
							if(accountHolderIndex!=0)
							{
								//below line gives broker name if broker name is present only after accountHolder string 
								String firstBrokerPart = data.get(accountHolderIndex+1);
								String secondBrokerPart = "";
								if(goBackwards==accountHolderIndex+4)
								{
									secondBrokerPart=data.get(goBackwards-1);
								}
								dematName = firstBrokerPart +" "+ secondBrokerPart; 
							}
							else
							{
								dematName = data.get(goBackwards-2);
								if(dematName.contains("PAN"))
								{
									dematName = data.get(goBackwards-3);
								}
							}
							actualEquityLines.add("#DEMAT NAME:"+dematName);
							//System.out.println("@Demat Name="+dematName);
						}
						else
						{
							actualEquityLines.add("#DEMAT ID:");
							actualEquityLines.add("#DEMAT NAME:");
						}
					}
				}
				String isinString="";
				String stockSymbolString="";
				String companyNameString="",secondPartOfCompanyString="";
				boolean getAllCompleteCompanyName=false;
				Double nSharesDouble=0.0;
				Double marketPriceDouble=0.0;
				
				int spaceIndex = 0, oneSpace = 0, twoSpace = 0, threeSpace = 0, fourSpace = 0, fiveSpace = 0;
				String currentDematId="",currentDematName="";
				itr = actualEquityLines.iterator();
				
				//below regex matches pattern  like 'INE742F01042 ADANI PORTS AND SPECIAL 1.000 0.000 0.000 319.10 319.10'
				String regex="^((INE|INF|IN9|IN0).{9})(.*)\\s([0-9\\,]+\\.[0-9]{1,5})\\s([0-9\\,]+\\.[0-9]{1,5})\\s([0-9\\,]+\\.[0-9]{1,5})\\s([0-9\\,]+\\.[0-9]{1,5})\\s([0-9\\,]+\\.[0-9]{1,5})$";
				Pattern p=Pattern.compile(regex);
				Matcher matcher=null;
				int noOfStocks=0;
				//& in below regex is used for stock name like 'L&T'
				Pattern p1=Pattern.compile("[(&a-zA-Z\\s)]+");
				String regex1="(.*)\\s([0-9\\,]+\\.[0-9]{1,5})\\s([0-9\\\\,]+\\.[0-9]{1,5})\\s([0-9\\,]+\\.[0-9]{1,5})";
				Pattern p2=Pattern.compile(regex1);
				while(itr.hasNext())
				{
					String line = (String) itr.next();
					int counter = 0;
					List<Integer> indexOfSpaces = new ArrayList();
					if(line.contains("#DEMAT ID:"))
					{
						if(!currentDematId.equals(line.substring(10)))
						{
							currentDematId=line.substring(10);
						}
						//System.out.print("dmeat id= "+currentDematId);
					}
					if(line.contains("#DEMAT NAME:"))
					{
						if(!currentDematId.equals(line.substring(12)))
						{
							currentDematName=line.substring(12);
						}
						//System.out.println(" : Demat Name = "+currentDematName);
					}
					matcher=p.matcher(line);
					if(matcher.find())
					{
						noOfStocks++;
						String isin=matcher.group(1);
						String noOfShares=matcher.group(4).replace(",", "");
						String marketPrice=matcher.group(7).replace(",", "");
						companyNameString=matcher.group(3).trim();
						Matcher m1=p1.matcher(companyNameString);
						if(m1.find())
						{
							//System.out.println("Start ="+m1.start()+" end = "+(m1.end()-1));
							companyNameString=companyNameString.substring(m1.start(), m1.end());
							//System.out.println("Comp name "+companyNameString+"|   isin = "+isin);
							//System.out.println("group length = "+matcher.group(3).trim().length()+" lengtj = "+companyNameString.trim().length());
							if(matcher.group(3).trim().length()==companyNameString.trim().length())
								getAllCompleteCompanyName=false;
							else
								getAllCompleteCompanyName=true;
							if(!getAllCompleteCompanyName)
							{
								//from next 2 lines get remaining words of company name;
								if(itr.hasNext())
								{
									line = (String) itr.next();
									//System.out.println("line = "+line);
									Matcher m2=p2.matcher(line);
									if(m2.find())
									{
										String secondPartOfCompany=m2.group(0);
										m1=p1.matcher(secondPartOfCompany);
										if(m1.find())
										{
											int i1=m1.start();
											int i2=m1.end()-1;
											secondPartOfCompanyString=line.substring(i1,i2+1).trim();
											//System.out.println("Second part of Cmp string = "+secondPartOfCompanyString);
											companyNameString=companyNameString+secondPartOfCompanyString;
										}
									}
								}
							}
						}
						
						if(!noOfShares.matches("[0-9\\.]+"))
							throw new FundexpertException("Number of shares is not a valid number.");
						if(!marketPrice.matches("[0-9\\.]+"))
							throw new FundexpertException("MarketPrice is not valid number.");
						//System.out.println(isin+" : "+noOfShares+" : "+marketPrice);
						StocksHoldings sh=new StocksHoldings();
						sh.setCompanyName(companyNameString);
						sh.setDematId(currentDematId);
						sh.setDematName(currentDematName);
						sh.setIsinCode(isin);
						sh.setMarketPrice(Double.valueOf(marketPrice));
						sh.setNumberOfShares(Double.valueOf(noOfShares));
						stocksHoldingsList.add(sh);
					}
					
				}
				System.out.println("Total stocks in readNSDLFileForDifferentEquities = "+noOfStocks);
				return stocksHoldingsList;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new FundexpertException("Not able to fetch data from NSDL file.");
			}
		}
		
		private List<StocksHoldings> readCDSLFile(List<String> data) throws Exception
		{
			int incr=0;
			List<StocksHoldings> stocksList=new ArrayList<StocksHoldings>();
			String currentDematName=null;
			boolean duplicateDemat=false;
			String previousDemat="";
			//noOfLinesAboveIsin is the number of lines above isin line which beloing that isin similarly for noOfLinesBelowIsin
			int noOfLinesAboveIsin=0,noOfLinesBelowIsin=0,noOfLinesBetweenTwoIsin=0;
			final String txPattern1="^((INF|INE|IN9|INE0)[0-9A-Z]{9})(\\s)(.)+(\\s)([0-9]+(\\.)[0-9]+)\\s--\\s--\\s--\\s([0-9]+(\\.)[0-9]+)(\\s)([0-9]+(\\.)[0-9]+)$";
			final String txPattern2="^((INF|INE|IN9|INE0)[0-9A-Z]{9})(\\$)*(\\s)([0-9]+(.)+[0-9]+)\\s--\\s--\\s--\\s([0-9]+(\\.)[0-9]+)(\\s)([0-9]+(\\.)[0-9]+)$";
			final String txPattern3="^((INF|INE|IN9|INE0)[0-9A-Z]{9})(\\s)(.)+\\s--\\s--\\s--\\s--\\s--\\s([0]+(\\.)[0]+)$";
			try
			{
				boolean stockFoundFlag=false;
				String currentISIN="",companyName="";
				double units=0;
				Pattern companyNamePattern=Pattern.compile("([a-zA-Z&\\s])+");
				for(int i=0;i<data.size();i++)
				{
					String line=data.get(i);
					incr=i;
					stockFoundFlag=false;
					if(line.contains("STATEMENT OF TRANSACTIONS FOR")||line.contains("HOLDING STATEMENT"))
					{
						int index=i;
						while(!data.get(index).contains("DP Name :") && index>0)
						{
							index--;
						}
						if(index<=0)
						{
							break;
						}
						else
						{
							String[] dp=data.get(index).split(":");
							if(dp.length==3)
							{
								currentDematName=dp[1].replace("BO ID", "").trim();
							}
							else if(dp.length==2)
							{
								currentDematName=dp[1].trim();
							}
							if(previousDemat.equals(currentDematName))
							{
								duplicateDemat=true;
							}
							else
							{
								duplicateDemat=false;
								previousDemat=currentDematName;
							}
						}
					}
					if(line.matches(txPattern3))
					{
						int linesAboveIsinThatBelongToThisIsin=0;
						//below if case will be true only when demat account changes in pdf
						if(noOfLinesBetweenTwoIsin>6)
						{
							noOfLinesAboveIsin=0;
							noOfLinesBelowIsin=0;
						}
						//this if will hit first tym for each demat account
						if(noOfLinesAboveIsin==0)
						{
							noOfLinesBetweenTwoIsin=0;
							for(int k=1;k<4;k++)
							{
								String lineAbove=data.get(i-k);
								if(lineAbove.contains("Bal"))
								{
									noOfLinesAboveIsin=k-1;
									noOfLinesBelowIsin=k-1;
									//System.out.println("First time noOfLinesAboveIsin  = "+noOfLinesAboveIsin+" when i = "+i);
									break;
								}
							}
						}
						else
						{
							//System.out.println("noOfLinesBetweenTwoIsin = "+noOfLinesBetweenTwoIsin+" noOfLinesBelowIsin = "+noOfLinesBelowIsin);
							//-1 is bcoz we are considering isin line also in noOfLinesBetweenTwoIsin
							linesAboveIsinThatBelongToThisIsin=noOfLinesBetweenTwoIsin-noOfLinesBelowIsin-1;
							noOfLinesAboveIsin=linesAboveIsinThatBelongToThisIsin;
							noOfLinesBelowIsin=linesAboveIsinThatBelongToThisIsin;
							noOfLinesBetweenTwoIsin=0;
						}
						//here units in this isin = 0.0 so we will not think about storing this isin
						//but above code will be used
					}
					if(line.matches(txPattern1))
					{
						int linesAboveIsinThatBelongToThisIsin=0;
						//below if case will be true only when demat account changes in pdf
						if(noOfLinesBetweenTwoIsin>6)
						{
							noOfLinesAboveIsin=0;
							noOfLinesBelowIsin=0;
						}
						//this if will hit first tym for each demat account
						if(noOfLinesAboveIsin==0)
						{
							noOfLinesBetweenTwoIsin=0;
							for(int k=1;k<4;k++)
							{
								String lineAbove=data.get(i-k);
								if(lineAbove.contains("Bal"))
								{
									noOfLinesAboveIsin=k-1;
									noOfLinesBelowIsin=k-1;
									//System.out.println("First time noOfLinesAboveIsin  = "+noOfLinesAboveIsin+" when i = "+i);
									break;
								}
							}
						}
						else
						{
							//System.out.println("noOfLinesBetweenTwoIsin = "+noOfLinesBetweenTwoIsin+" noOfLinesBelowIsin = "+noOfLinesBelowIsin);
							//-1 is bcoz we are considering isin line also in noOfLinesBetweenTwoIsin
							linesAboveIsinThatBelongToThisIsin=noOfLinesBetweenTwoIsin-noOfLinesBelowIsin-1;
							noOfLinesAboveIsin=linesAboveIsinThatBelongToThisIsin;
							noOfLinesBelowIsin=linesAboveIsinThatBelongToThisIsin;
							noOfLinesBetweenTwoIsin=0;
						}
						boolean isCompleteCompanyNameFound=false;
						for(int l=noOfLinesAboveIsin;l>0;l--)
						{
							String lLinesBack=data.get(i-l);
							Matcher companyMatch=companyNamePattern.matcher(lLinesBack);
							if(companyMatch.find())
							{
								int start=companyMatch.start();
								int end=companyMatch.end();
								//System.out.println("0.Start = "+start+" end = "+end+" for compName="+lLinesBack);
								if(companyName.equals(""))
								{
									companyName=lLinesBack.substring(start,end).trim();
								}
								else
								{
									companyName+=" "+lLinesBack.substring(start,end).trim();
								}
								if(lLinesBack.trim().length()!=lLinesBack.substring(start,end).trim().length())
								{
									isCompleteCompanyNameFound=true;
									break;
								}
							}
						}
						if(!isCompleteCompanyNameFound)
						{
							//here we are taking company name from line having isin itself
							//get remaining name of stock from line
							String lineAfterISIN=line.substring(12);
							Matcher secondCompanyMatch=companyNamePattern.matcher(lineAfterISIN);
							if(secondCompanyMatch.find())
							{
								int start=secondCompanyMatch.start();
								int end=secondCompanyMatch.end();
								//System.out.println("Second part of companyName Start = "+start+" end = "+end);
								isCompleteCompanyNameFound=true;
								if(companyName.equals(""))
								{
									companyName=lineAfterISIN.substring(start,end).trim();
								}
								else
								{
									companyName+=" "+lineAfterISIN.substring(start,end).trim();
								}
							}
							
						}
						int indexOfLastSpace=line.lastIndexOf(" ");
						String mid=line.substring(0,indexOfLastSpace);
						int indexOfSecondLastSpace=mid.lastIndexOf(" ");
						String isin=line.substring(0, 12); 
						currentISIN=isin;
						String unitsInString=mid.substring(indexOfSecondLastSpace).trim();
						units=Double.valueOf(unitsInString);
						//System.out.println("Indexof Second last index="+indexOfLastSpace+" for isin="+isin+" units="+units);
						stockFoundFlag=true;
					}
					if(line.matches(txPattern2))
					{
						int linesAboveIsinThatBelongToThisIsin=0;
						//below if case will be true only when demat account changes in pdf
						if(noOfLinesBetweenTwoIsin>6)
						{
							noOfLinesAboveIsin=0;
							noOfLinesBelowIsin=0;
						}
						//this if will hit first tym for each demat account
						if(noOfLinesAboveIsin==0)
						{
							noOfLinesBetweenTwoIsin=0;
							for(int k=1;k<4;k++)
							{
								String lineAbove=data.get(i-k);
								if(lineAbove.contains("Bal"))
								{
									noOfLinesAboveIsin=k-1;
									noOfLinesBelowIsin=k-1;
									//System.out.println("1First time noOfLinesAboveIsin  = "+noOfLinesAboveIsin+" when i = "+i);
									break;
								}
							}
						}
						else
						{
							//System.out.println("1noOfLinesBetweenTwoIsin = "+noOfLinesBetweenTwoIsin+" noOfLinesBelowIsin = "+noOfLinesBelowIsin);
							//-1 is bcoz we are considering isin line also in noOfLinesBetweenTwoIsin
							linesAboveIsinThatBelongToThisIsin=noOfLinesBetweenTwoIsin-noOfLinesBelowIsin-1;
							noOfLinesAboveIsin=linesAboveIsinThatBelongToThisIsin;
							noOfLinesBelowIsin=linesAboveIsinThatBelongToThisIsin;
							noOfLinesBetweenTwoIsin=0;
						}
						boolean isCompleteCompanyNameFound=false;
						for(int l=noOfLinesAboveIsin;l>0;l--)
						{
							String lLinesBack=data.get(i-l);
							Matcher companyMatch=companyNamePattern.matcher(lLinesBack);
							if(companyMatch.find())
							{
								int start=companyMatch.start();
								int end=companyMatch.end();
								//System.out.println("1.Start = "+start+" end = "+end+" for compName="+lLinesBack);
								if(companyName.equals(""))
								{
									companyName=lLinesBack.substring(start,end).trim();
								}
								else
								{
									companyName+=" "+lLinesBack.substring(start,end).trim();
								}
								if(lLinesBack.trim().length()!=lLinesBack.substring(start,end).trim().length())
								{
									isCompleteCompanyNameFound=true;
									break;
								}
							}
						}
						if(!isCompleteCompanyNameFound)
						{
							//here we are taking company name from line having isin itself
							//get remaining name of stock from line
							String lineAfterISIN=line.substring(12);
							Matcher secondCompanyMatch=companyNamePattern.matcher(lineAfterISIN);
							if(secondCompanyMatch.find())
							{
								int start=secondCompanyMatch.start();
								int end=secondCompanyMatch.end();
								//System.out.println("1Second part of companyName Start = "+start+" end = "+end);
								isCompleteCompanyNameFound=true;
								if(companyName.equals(""))
								{
									companyName=lineAfterISIN.substring(start,end).trim();
								}
								else
								{
									companyName+=" "+lineAfterISIN.substring(start,end).trim();
								}
							}
							
						}
						
						int indexOfLastSpace=line.lastIndexOf(" ");
						String mid=line.substring(0,indexOfLastSpace);
						int indexOfSecondLastSpace=mid.lastIndexOf(" ");
						String isin=line.substring(0, 12);
						currentISIN=isin;
						String unitsInString=mid.substring(indexOfSecondLastSpace).trim();
						units=Double.valueOf(unitsInString);
						//System.out.println("Indexof Second last index="+indexOfLastSpace+" for isin="+isin+"  units="+units);
						stockFoundFlag=true;
					}
					if(stockFoundFlag)
					{
						StocksHoldings sh=new StocksHoldings();
						sh.setIsinCode(currentISIN);
						sh.setNumberOfShares(units);
						sh.setDematName(currentDematName);
						sh.setCompanyName(companyName);
						companyName="";
						System.out.println("DEMAT NAME="+currentDematName);
						stocksList.add(sh);
					}
					noOfLinesBetweenTwoIsin++;
					//System.out.println("noOfLinesBetweenTwoIsin = "+noOfLinesBetweenTwoIsin+" line = "+line);
				}
				if(stocksList.isEmpty())
				{
					//throw new FundexpertException("No stocks data in given pdf.");
				}
				return stocksList;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new Exception(e.getMessage());
			}
		}
	}
	
	private String getDecryptedFile(String sourceFilePath,String targetFilePath,String password) throws IOException
	{
		String command = "";
		try
		{
			if(((String)System.getProperties().get("os.name")).contains("Windows"))
				command="C:/Program Files/qpdf-8.2.1/bin/qpdf.exe";
			else
				command="qpdf";
			ProcessBuilder pb = new ProcessBuilder(command, "--password="+password, "--decrypt", sourceFilePath, targetFilePath);
			pb. redirectErrorStream(true);
			
			StringBuffer output = new StringBuffer();

			Process p=pb.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";			
			while ((line = reader.readLine())!= null) 
			{
				output.append(line);
			}
			if(output.toString().toLowerCase().contains("invalid password"))
				throw new BadPasswordException("Please upload CAS linked with PAN.");
			return output.toString();
		}
		catch(BadPasswordException be)
		{
			throw be;
		}
		catch(IOException ioe)
		{
			throw ioe;
		}
	}

	public void setRequestCommingFrom(short value) {
		pdfFromWhichApiRequest=value;
	}
}
