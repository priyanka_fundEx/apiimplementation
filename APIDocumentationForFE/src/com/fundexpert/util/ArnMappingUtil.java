package com.fundexpert.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.ArnMapping;

public class ArnMappingUtil {

	public boolean persistExcel(String path)
	{
		Session hSession=null;
		Transaction tx=null;
		FileInputStream input=null;
		List<String> existingArnList=new ArrayList<String>();
		XSSFWorkbook wb=null;
		try {
			hSession=HibernateBridge.getSessionFactory().openSession();
			existingArnList=hSession.createQuery("select arn from ArnMapping order by arn").list();
			tx=hSession.beginTransaction();
			input = new FileInputStream(path); 
			System.out.println("Path = "+path);
			System.out.println("input"+input);
			// XSSFWorkbook wb= new XSSFWorkbook(item.getInputStream()); 
			wb = new XSSFWorkbook(input);
			System.out.println("input1");
		    XSSFSheet sheet = wb.getSheetAt(0);  
		    System.out.println("input2");
		    for(int i=1; i<=sheet.getLastRowNum(); i++)
		    { 
		    	XSSFRow  row = sheet.getRow(i); 
		    	if(row.getCell(0).getStringCellValue()!=null) 
		    	{
			    	String arn=row.getCell(0).getStringCellValue().trim();
		    	 	System.out.print("ARN : "+arn+" | ");
			    	System.out.println("ARN NAME : "+row.getCell(1).getStringCellValue());
			    	if(!existingArnList.contains(arn)) 
			    	{
			    		ArnMapping arnmapping1=new ArnMapping();
				    	arnmapping1.setArn(arn);
				    	arnmapping1.setName(row.getCell(1).getStringCellValue());
				    	hSession.save(arnmapping1);
				    	existingArnList.add(arn);
			    	}
		    	}
		    }
			tx.commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
			return false;
		}
		finally {
			try {
				if(wb!=null)
					wb.close();
				input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			hSession.close();
		}
	}
}
