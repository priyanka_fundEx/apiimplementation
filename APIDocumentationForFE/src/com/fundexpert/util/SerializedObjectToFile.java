package com.fundexpert.util;

import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

public class SerializedObjectToFile {

	FileOutputStream fout = null;
	ObjectOutputStream oos = null;
	
	public void serializeObject(Object obj,File file) throws Exception
	{
		try {

			fout = new FileOutputStream(file);
			oos = new ObjectOutputStream(fout);
			
			oos.writeObject(obj);
			
			System.out.println("File Writing Done.");

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new Exception("Not able to write new MutualFunds to file : Exception : "+ex.getMessage());
		} finally {

			if (fout != null) {
				try {
					fout.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public Object getSerializedFile(File file) throws Exception
	{
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {

			fis = new FileInputStream(file);
		    ois = new ObjectInputStream(fis);
		    System.out.println("File Reading Done.");
		    return ois.readObject();

		}
		catch(EOFException e)
		{
			e.printStackTrace();
			throw e;
		}
		catch (Exception ex) {

			ex.printStackTrace();
			throw new Exception("Not able to Read new Object from file : Exception : "+ex.getMessage());
		} finally {

			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (ois != null) {
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
}
