package com.fundexpert.util;

public class Verify {

	private static final String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String PAN_REGEX = "^([A-Z]){5}([0-9]){4}([A-Z]){1}?$";
	
	public static boolean emailValidate(String email)
	{
		return email.matches(Verify.EMAIL_REGEX);
	}

	public static boolean panValidate(String pan)
	{
		return pan.matches(Verify.PAN_REGEX);
	}
}
