package com.fundexpert.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.dao.MutualFund;
import com.fundexpert.exception.FundexpertException;

public class Utilities {

	public boolean decodeBase64ByteStreamToPdf(String byteStream,String path)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(path);
            fos.write(Base64.getDecoder().decode(byteStream));
            fos.close();
            return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public String getRandomString()
	{
		try
		{
			String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	        StringBuilder salt = new StringBuilder();
	        Random rnd = new Random();
	        while (salt.length() < 18) { // length of the random string.
	            int index = (int) (rnd.nextFloat() * s.length()-1);
	            salt.append(s.charAt(index));
	        }
	        //below "80" helps to identify that the file is made using api request of bytestream
	        return salt.toString()+"80";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "023571A"+Calendar.getInstance().getTimeInMillis();
	}
	
	public String getByteStream(String pdfPath) throws IOException
	{
		File originalFile = new File(pdfPath);
		String encodedBase64 = null;
		FileInputStream fileInputStreamReader=null;
		try
		{
			fileInputStreamReader = new FileInputStream(originalFile);
            byte[] bytes = new byte[(int)originalFile.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.getEncoder().encodeToString(bytes));
            System.out.println( encodedBase64);
            return encodedBase64;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			fileInputStreamReader.close();	
		}
		return null;
	}

	//broad classification under EQUITY/DEBT-schemeType
	//under equity it is LC,MC,SC and under DEBT it is ST,LT-type
	public void doMutualFundClassification()
	{
		Map<String,Map<String,String>> schemeTypeMap=getBroaderSchemeTypeClassificationMap();
		Map<String,String> typeMap=getBroaderTypeClassificationMap();
		Session hSession=null;
		Transaction tx=null;
		String schemeType,type,broaderType,broaderSchemeType;
		try
		{
			int inGroupOf=300;
			long start=0,end=0;
			hSession=HibernateBridge.getSessionFactory().openSession();
			List<MutualFund> list=hSession.createQuery("from MutualFund order by id").setMaxResults(inGroupOf).list();
			Iterator itr=list.iterator();
			while(itr.hasNext())
			{
				if(start==0)
					start=System.nanoTime();
				MutualFund mf=(MutualFund)itr.next();
				//System.out.println("MutualFund="+mf.getId()+" | schemeType="+mf.getSchemeType()+" | type="+mf.getType());
				schemeType=mf.getSchemeType().trim();
				type=mf.getType().trim();
				broaderType=typeMap.get(type);
				//System.out.println("BroaderType="+broaderType);
				if(schemeType!=null && (schemeType.equals("EQUITY") || schemeType.equals("DEBT")))
				{
					//System.out.println("Inside SchemeType1)");
					if(broaderType!=null)
					{
						//System.out.println("1)New Broader type = "+broaderType);
						mf.setBroaderType(broaderType);
					}
					mf.setBroaderSchemeType(schemeType);
				}
				else if(schemeType!=null && (schemeType.equals("HYBRID") || schemeType.equals("SOLUTION ORIENTED") || schemeType.equals("OTHER")))
				{
					Map<String,String> map=schemeTypeMap.get(schemeType);
					broaderSchemeType=map.get(type);
					mf.setBroaderSchemeType(broaderSchemeType);
					//System.out.println("New broader schemeType = "+broaderSchemeType);
					if(broaderType!=null)
					{
						//System.out.println("2)New Broader type = "+broaderType);
						mf.setBroaderType(broaderType);
					}
				}
				else
				{
					//System.out.println("No SchemeType defined.");
					if(broaderType!=null && !broaderType.equals(type))
					{
						//System.out.println("3)New Broader type = "+broaderType);
						mf.setBroaderType(broaderType);
					}
				}
				itr.remove();
				if(!itr.hasNext())
				{
					end=System.nanoTime();
					double diff=end-start;
					System.out.println("Time taken to process "+inGroupOf+" mf ="+(diff/1000000));
					
					tx=hSession.beginTransaction();
					tx.commit();
					
					start=System.nanoTime();
					list=hSession.createQuery("from MutualFund where id>? order by id").setLong(0, mf.getId()).setMaxResults(inGroupOf).list();
					itr=list.iterator();
				}
			}
			tx=hSession.beginTransaction();
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
	}
	
	private Map<String,Map<String,String>> getBroaderSchemeTypeClassificationMap()
	{
		Map<String,Map<String,String>> map=new HashMap<String,Map<String,String>>();
		
		try
		{
			Map<String,String> hybridMapSchemeClassification=new HashMap<String,String>();
			hybridMapSchemeClassification.put("CONSERVATIVE", "DEBT");
			hybridMapSchemeClassification.put("BALANCED", "DEBT");
			hybridMapSchemeClassification.put("AGGRESSIVE", "EQUITY");
			hybridMapSchemeClassification.put("DYNAMIC ASSET ALLOCATION", "EQUITY");
			hybridMapSchemeClassification.put("ARBITRAGE", "EQUITY");
			hybridMapSchemeClassification.put("MULTI ASSET ALLOCATION", "DEBT");
			hybridMapSchemeClassification.put("EQUITY SAVINGS", "EQUITY");
			map.put("HYBRID", hybridMapSchemeClassification);
			
			Map<String,String> solutionOrientedSchemeClassification=new HashMap<String,String>();
			solutionOrientedSchemeClassification.put("RETIREMENT", "EQUITY");
			solutionOrientedSchemeClassification.put("CHILDREN", "EQUITY");
			map.put("SOLUTION ORIENTED", solutionOrientedSchemeClassification);
			
			Map<String,String> otherSchemeClassification=new HashMap<String,String>();
			otherSchemeClassification.put("FOF", "EQUITY");
			otherSchemeClassification.put("INDEX/ETF", "EQUITY");
			map.put("OTHER", otherSchemeClassification);
			
			return map;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	private Map<String,String> getBroaderTypeClassificationMap()
	{
		Map<String,String> map=new HashMap<String,String>();
		try
		{
			map.put("LARGE & MID CAP","LARGE CAP");
			map.put("MULTI CAP","LARGE CAP");
			map.put("DIVIDEND YIELD","LARGE CAP");
			map.put("ELSS","LARGE CAP");
			map.put("BALANCED","LARGE CAP");
			map.put("AGGRESSIVE","LARGE CAP");
			map.put("ARBITRAGE","LARGE CAP");
			map.put("INDEX/ETF","LARGE CAP");
			map.put("FOF","LARGE CAP");
			map.put("LARGE CAP","LARGE CAP");
			map.put("FOCUSSED","LARGE CAP");
			
			map.put("VALUE","MID CAP");
			map.put("DYNAMIC ASSET ALLOCATION","MID CAP");
			map.put("MULTI ASSET ALLOCATIONqw","MID CAP");
			map.put("RETIREMENT","MID CAP");
			map.put("CHILDREN","MID CAP");
			map.put("MID CAP","MID CAP");
			
			map.put("CONTRA","SMALL CAP");
			map.put("SECTORAL & THEMATIC","SMALL CAP");
			map.put("SMALL CAP","SMALL CAP");
			
			map.put("MEDIUM DURATION", "LONG TERM");
			map.put("MEDIUM TO LONG DURATION", "LONG TERM");
			map.put("LONG DURATION", "LONG TERM");
			map.put("DYNAMIC BOND", "LONG TERM");
			map.put("CORPORATE BOND", "LONG TERM");
			map.put("CREDIT RISK", "LONG TERM");
			map.put("BANKING AND PSU", "LONG TERM");
			map.put("GILT", "LONG TERM");
			map.put("GILT WITH 10 DURATION", "LONG TERM");
			map.put("FLOATER", "LONG TERM");
			map.put("CONSERVATIVE", "LONG TERM");
			map.put("EQUITY SAVINGS", "LONG TERM");
			map.put("LONG TERM", "LONG TERM");
			
			
			map.put("OVERNIGHT", "SHORT TERM");
			map.put("LIQUID", "SHORT TERM");
			map.put("ULTRA SHORT DURATION", "SHORT TERM");
			map.put("LOW DURATION", "SHORT TERM");
			map.put("MONEY MARKET", "SHORT TERM");
			map.put("SHORT DURATION", "SHORT TERM");
			map.put("SHORT TERM", "SHORT TERM");
			return map;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public String getPanForEdelweissEmail(String email)
	{
		if(email==null || email.equals(""))
			return null;
		try
		{
			JSONObject json=new JSONObject();
			json.put("emailId", email);
			json.put("stype","EQ");
			
			String path="https://ewuat.edelbusiness.in/EWReports/api/preader/masterrequest";
			URL url = new URL(path);
			URLConnection con=url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(500000);
			con.setReadTimeout(500000);
			con.setRequestProperty("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1NDUyOTgzMTQsImV4cCI6MTU3NjgzNDMxNywiYXVkIjoiZWRlbHdlaXNzIiwic3ViIjoiIiwic3JjIjoicHJlYWRlciJ9.8m0SwLy4zv3Fxr6n-2y7hCf9F2v1hkXnzoWDwsIx4-Q");
			con.setRequestProperty("Source", "preader");
			System.out.println("After opening connection to URL to get pan for email = "+email+" : path = "+path+"");
			
			OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
			writer.write(json.toString());
			writer.close(); 
			
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line=br.readLine();
			
			StringBuilder builder=new StringBuilder();
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			br.close();
			json=new JSONObject(builder.toString());
			System.out.println("Response from getting pan for email from edelweiss  =  "+json.toString());
			if(json!=null && json.has("success"))
			{
				if((Boolean)json.getBoolean("success") && json.has("data"))
				{
					JSONObject data=json.getJSONObject("data");
					if(data.has("pan"))
					{
						return data.getString("pan");
					}
					else
						return null;
				}
				else
					return null;
			}
			else
				return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean postEmailPanCombination(String email,String pan)
	{
		try
		{
			if(email==null || email.equals(""))
				throw new FundexpertException("Email cannot be null while sending email and pan to edelweiss.");
			if(pan==null || pan.equals(""))
				throw new FundexpertException("Pan cannot be null while sending email and pan to edelweiss.");
			JSONObject json=new JSONObject();
			json.put("emailId", email);
			json.put("stype","EQ");
			json.put("pan", pan);
			
			String path="https://ewuat.edelbusiness.in/EWReports/api/preader/reqprocessed";
			URL url = new URL(path);
			URLConnection con=url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(500000);
			con.setReadTimeout(500000);
			con.setRequestProperty("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1NDUyOTgzMTQsImV4cCI6MTU3NjgzNDMxNywiYXVkIjoiZWRlbHdlaXNzIiwic3ViIjoiIiwic3JjIjoicHJlYWRlciJ9.8m0SwLy4zv3Fxr6n-2y7hCf9F2v1hkXnzoWDwsIx4-Q");
			con.setRequestProperty("Source", "preader");
			System.out.println("After opening connection to URL to get pan for email = "+email+" : path = "+path+"");
			
			OutputStreamWriter writer=new OutputStreamWriter(con.getOutputStream());
			writer.write(json.toString());
			writer.close(); 
			
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line=br.readLine();
			
			StringBuilder builder=new StringBuilder();
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			br.close();
			json=new JSONObject(builder.toString());
			System.out.println("Response from sending pan & email from edelweiss  =  "+json.toString());
			if(json.has("success") && json.getBoolean("success"))
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
