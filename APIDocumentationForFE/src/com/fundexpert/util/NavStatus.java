package com.fundexpert.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class NavStatus implements Serializable{

	Map<String,String> addNewNavMap=new HashMap<String,String>();
	Map<String,String> updateExistingNavMap=new HashMap<String,String>();
	public Map<String, String> getAddNewNavMap() {
		return addNewNavMap;
	}
	public void setAddNewNavMap(Map<String, String> addNewNavMap) {
		this.addNewNavMap = addNewNavMap;
	}
	public Map<String, String> getUpdateExistingNavMap() {
		return updateExistingNavMap;
	}
	public void setUpdateExistingNavMap(Map<String, String> updateExistingNavMap) {
		this.updateExistingNavMap = updateExistingNavMap;
	}
	
}
