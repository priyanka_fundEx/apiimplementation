package com.fundexpert.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;







import java.net.URLEncoder;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.apidoc.util.HibernateBridge;
import com.fundexpert.config.Config;
import com.fundexpert.dao.Consumer;
import com.fundexpert.exception.FundexpertException;

public class KeepApiServiceAlive {

	
	public boolean getConfigForApiService(String appId) throws Exception
	{
		System.out.println("Calling Thread1");
		Config config=null;
		String path=null;
		try
		{
			config=new Config();
			if(config.getProperty(Config.ENVIRONMENT).equals(Config.DEVELOPMENT))
			{
				path=config.getProperty(Config.CONFIG_TO_KEEP_API_SERVICE_ALIVE_DEVELOPMENT);
			}
			else
			{
				path="https://fundexpert.in/letmego";
					
			}
			
			String charset="UTF-8";
			String query = String.format("?action=%s&config=%s", 
					URLEncoder.encode("getConfig", charset), 
					URLEncoder.encode(appId, charset));
			path+=query;
			//System.out.println("Path = "+path);
			URL url = new URL(path);
			URLConnection con=url.openConnection();
			
			//con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(500000);
			con.setReadTimeout(500000);
			
			BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line=br.readLine();
		
			StringBuilder builder=new StringBuilder();
			while(line!=null)
			{
				builder.append(line);
				line=br.readLine();
			}
			JSONObject json=new JSONObject(builder.toString());
			//System.out.println(json.toString(1));
			if(json.has("config"))
			{
				//System.out.println("Output="+json.getBoolean("config"));
				return json.getBoolean("config");
			}
			else if(json.has("error"))
			{
				throw new FundexpertException(json.getString("error"));
			}
			else
			{
				throw new Exception("Exception Occurred.");
			}
		}
		catch(FundexpertException fe)
		{
			throw fe;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	public boolean updateConfigStatus(String appId,boolean active)
	{
		Session hSession=null;
		Transaction tx=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			Consumer c=(Consumer)hSession.createQuery("from Consumer where appid=?").setString(0, appId).uniqueResult();
			if(c.isActive()!=active)
			{
				tx=hSession.beginTransaction();
				c.setActive(active);
				tx.commit();
			}
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(tx!=null)
				tx.rollback();
		}
		finally
		{
			hSession.close();
		}
		return false;
	}
}
