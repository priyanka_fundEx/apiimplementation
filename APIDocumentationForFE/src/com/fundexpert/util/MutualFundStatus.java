package com.fundexpert.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MutualFundStatus implements Serializable {

	
	Map<String,String> addNewMFMap=new HashMap<String,String>();
	Map<String,String> updateExistingMFMap=new HashMap<String,String>();
	Map<String,String> updateMFIDMap=new HashMap<String,String>();
	public Map<String, String> getAddNewMFMap() {
		return addNewMFMap;
	}
	public void setAddNewMFMap(Map<String, String> addNewMFMap) {
		this.addNewMFMap = addNewMFMap;
	}
	public Map<String, String> getUpdateExistingMFMap() {
		return updateExistingMFMap;
	}
	public void setUpdateExistingMFMap(Map<String, String> updateExistingMFMap) {
		this.updateExistingMFMap = updateExistingMFMap;
	}
	public Map<String, String> getUpdateMFIDMap() {
		return updateMFIDMap;
	}
	public void setUpdateMFIDMap(Map<String, String> updateMFIDMap) {
		this.updateMFIDMap = updateMFIDMap;
	}
	
}
