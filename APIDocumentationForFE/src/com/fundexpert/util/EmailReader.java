package com.fundexpert.util;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Message.RecipientType;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;
import javax.mail.search.SearchTerm;

import com.fundexpert.config.Config;
import com.fundexpert.exception.FundexpertException;
import com.fundexpert.exception.ValidationException;

public class EmailReader {

	static final String EMAIL_ID="edelweiss@fundexpert.in",PORT="993",HOST="imappro.zoho.com",PASSWORD="Trad123@0912#@",PROTOCOL="imap";
	static Config CONFIG;
	static Properties properties;
	static String path,stockPath;
	public EmailReader() throws FundexpertException
	{
		try
		{
			CONFIG=new Config();
			path=CONFIG.getProperty(Config.PDF_NOT_READ);
			stockPath=CONFIG.getProperty(Config.STOCK_PDF_NOT_READ);
			
			properties = new Properties();
			 // server setting
			properties.put(String.format("mail.%s.host", PROTOCOL), HOST);
			properties.put(String.format("mail.%s.port", PROTOCOL), HOST);
	 
	        // SSL setting
			properties.setProperty(
	                String.format("mail.%s.socketFactory.class", PROTOCOL),
	                "javax.net.ssl.SSLSocketFactory");
			properties.setProperty(
	                String.format("mail.%s.socketFactory.fallback", PROTOCOL),
	                "false");
			properties.setProperty(
	                String.format("mail.%s.socketFactory.port", PROTOCOL),
	                String.valueOf(PORT));
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
			throw new FundexpertException("Not Able to initialize EmailReader.");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new FundexpertException("Not Able to initialize EmailReader.");
		}
	}
	
	public boolean savePdf() throws Exception
	{
		String pathToSaveFile;
		Folder folderInbox = null;//,moveToFolder=null;
		Store store=null;
		Message[] messages;
		try
		{
			
			Session session = Session.getDefaultInstance(properties);
			
			// connects to the message store
            store = session.getStore(PROTOCOL);
            store.connect(EMAIL_ID, PASSWORD);
            
            //opens the Portfolio folder
            folderInbox= store.getFolder("NewPortfolio");
            folderInbox.open(Folder.READ_WRITE);
           
            
            // creates a search criterion
            SearchTerm searchCondition = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {
                        if (message.getSubject().contains("CAMS Mailback")) {
                            return true;
                        }
                    } catch (MessagingException ex) {
                        ex.printStackTrace();
                    }
                    return false;
                }
            };
            messages = folderInbox.search(searchCondition);
            
            for (int i = 0; i < messages.length; i++) 
            {
                Message msg = messages[i];
                
                msg.setFlags(new Flags(Flags.Flag.SEEN), true);
                Address[] fromAddress = msg.getFrom();
                String from = fromAddress[0].toString();
                String subject = msg.getSubject();
                
                String contentType = msg.getContentType();
                String messageContent = "";
 
                String attachFiles="";
                if(contentType.contains("multipart"))
                {
                	Multipart multipart=(Multipart)msg.getContent();
                	int numberOfParts=multipart.getCount();
                	for(int count=0;count<numberOfParts;count++)
                	{
                		MimeBodyPart part=(MimeBodyPart)multipart.getBodyPart(count);
                		if(Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition()))
                		{
                			String fileName=part.getFileName();
                			fileName=MimeUtility.decodeText(fileName);
                			String emailId=from.substring(from.indexOf("<")+1, from.indexOf(">"));
                			if(!Verify.emailValidate(emailId))
                			{
                				throw new ValidationException("From emailId fetched from IMAP server does not validate.");
                			}
                			//System.out.println("From : "+from+" Email="+emailId+" FileName="+fileName);
                			fileName.replaceAll("-", "_");
                			part.saveFile(new File(path+File.separator+emailId+"-"+fileName));
                			//System.out.println("Saved File(PDF) : "+fileName);
                		}
                	}
                }
                msg.setFlag(Flags.Flag.DELETED, true);
            }
            //Bcoz of the Edelweiss security we are not keeping the emails in zoho OldPortfolio folder
            //move all of the messages to OldPortfolio folder
            //moveToFolder=store.getFolder("OldPortfolio");
            //folderInbox.copyMessages(messages, moveToFolder);
           
            return true;
		}
		catch(ValidationException ve)
		{
			System.out.println("");
			ve.printStackTrace();
			throw new FundexpertException(ve.getMessage());
		}
		catch (NoSuchProviderException ex) 
		{
			System.out.println("No provider for protocol: " + PROTOCOL);
	        ex.printStackTrace();
	        throw new NoSuchProviderException("IMAP Protocol not working.");
	    }
		catch (MessagingException ex) 
		{
			System.out.println("Could not connect to the message store");
			ex.printStackTrace();
			throw new MessagingException("Could not connect to message Store.");
	    }
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Error occurred while reading IMAP server.");
		}
		finally
		{
			// disconnect
			if(folderInbox.isOpen())
			{
				//System.out.println("OPEN1");
				folderInbox.close(true);
			}
			/*if(moveToFolder.isOpen())
			{
				//System.out.println("OPEN2");
				moveToFolder.close(true);
			}*/
            store.close();
		}
	}
	
	public boolean saveStockPdf() throws Exception
	{

		String pathToSaveFile;
		Folder folderInbox = null,moveToFolder=null;
		Store store=null;
		Message[] messages;
		try
		{
			
			Session session = Session.getDefaultInstance(properties);
			
			// connects to the message store
            store = session.getStore(PROTOCOL);
            store.connect(EMAIL_ID, PASSWORD);
            
            //opens the Portfolio folder
            folderInbox= store.getFolder("NewStockPortfolio");
            folderInbox.open(Folder.READ_WRITE);
           
            System.out.println("Saving Stock PDF.");
            // creates a search criterion
            SearchTerm searchCondition = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {
                        if (message.getSubject().contains("Your NSDL CAS") || message.getSubject().contains("CDSL Consolidated Account Statement")) {
                            return true;
                        }
                    } catch (MessagingException ex) {
                        ex.printStackTrace();
                    }
                    return false;
                }
            };
            messages = folderInbox.search(searchCondition);
            //System.out.println("Messages legth = "+messages.length);
            for (int i = 0; i < messages.length; i++) 
            {
                Message msg = messages[i];
                
                msg.setFlags(new Flags(Flags.Flag.SEEN), true);
                Address[] fromAddress = msg.getFrom();
                String from = fromAddress[0].toString();
                String subject = msg.getSubject();
                
                String contentType = msg.getContentType();
                String messageContent = "";
 
                String attachFiles="";
                if(contentType.contains("multipart"))
                {
                	Multipart multipart=(Multipart)msg.getContent();
                	int numberOfParts=multipart.getCount();
                	for(int count=0;count<numberOfParts;count++)
                	{
                		MimeBodyPart part=(MimeBodyPart)multipart.getBodyPart(count);
                		if(Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition()))
                		{
                			String fileName=part.getFileName();
                			fileName=MimeUtility.decodeText(fileName);
                			String emailId=from.substring(from.indexOf("<")+1, from.indexOf(">"));
                			if(!Verify.emailValidate(emailId))
                			{
                				throw new ValidationException("From emailId fetched from IMAP server does not validate.");
                			}
                			//System.out.println("From : "+from+" Email="+emailId+" FileName="+fileName);
                			fileName=fileName.replaceAll("-", "_");
                			part.saveFile(new File(stockPath+File.separator+emailId+"-"+fileName));
                			//System.out.println("Saved Stock File(PDF) : "+fileName);
                		}
                	}
                }
               msg.setFlag(Flags.Flag.DELETED, true);
            }
            //Bcoz of the Edelweiss security we are not keeping the emails in zoho OldPortfolio folder
            //move all of the messages to OldPortfolio folder
            //moveToFolder=store.getFolder("OldStockPortfolio");
            //folderInbox.copyMessages(messages, moveToFolder);
           
            return true;
		}
		catch(ValidationException ve)
		{
			System.out.println(".");
			ve.printStackTrace();
			throw new FundexpertException(ve.getMessage());
		}
		catch (NoSuchProviderException ex) 
		{
			System.out.println("No provider for protocol: " + PROTOCOL);
	        ex.printStackTrace();
	        throw new NoSuchProviderException("IMAP Protocol not working.");
	    }
		catch (MessagingException ex) 
		{
			System.out.println("Could not connect to the message store");
			ex.printStackTrace();
			throw new MessagingException("Could not connect to message Store.");
	    }
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Error occurred while reading IMAP server.");
		}
		finally
		{
			// disconnect
			if(folderInbox.isOpen())
			{
				//System.out.println("OPEN1");
				folderInbox.close(true);
			}
			/*if(moveToFolder.isOpen())
			{
				//System.out.println("OPEN2");
				moveToFolder.close(true);
			}*/
            store.close();
		}
	
	}
	
	private String parseAddresses(Address[] address) {
        String listAddress = "";
 
        if (address != null) {
            for (int i = 0; i < address.length; i++) {
                listAddress += address[i].toString() + ", ";
            }
        }
        if (listAddress.length() > 1) {
            listAddress = listAddress.substring(0, listAddress.length() - 2);
        }
 
        return listAddress;
    }
}
