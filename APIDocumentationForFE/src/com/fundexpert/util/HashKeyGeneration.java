package com.fundexpert.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;

public class HashKeyGeneration {
	
	
	public static String EDELWEISS_TEST_APPID_STRING = "edelweiss|test|";
	public static String EDELWEISS_TEST_SECRET_KEY_STRING = ""; //value for this is the result of EDELWEISS_TEST_APPID_STRING after generation of key and is passed in parameter in function getSecretKey(String) 
	
	public static String EDELWEISS_PRODUCTION_APPID_STRING = "edelweiss|production|";
	public static String EDELWEISS_PRODUCTION_SECRET_KEY_STRING = ""; //value for this is the result of EDELWEISS_PRODUCTION_APPID_STRING after generation of key and is passed in parameter in function getSecretKey(String)
	Calendar cal=null;
	
	public HashKeyGeneration()
	{
		cal=Calendar.getInstance();
		cal.set(Calendar.DATE, 29);
		cal.set(Calendar.MONTH, 7);//AUGUST
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.HOUR_OF_DAY,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		EDELWEISS_TEST_APPID_STRING += cal.getTime();
		EDELWEISS_PRODUCTION_APPID_STRING += cal.getTime();
		
		System.out.println("Time="+cal.getTime());
	}
	
	public String getAppKey(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();
        byte[] buffer = input.getBytes("UTF-8");
        md.update(buffer);
        byte[] digest = md.digest();

        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
        	
            hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
            //System.out.println("byte="+digest[i]+" hexStr="+hexStr+"    |  "+( digest[i] & 0xff)+"   |  16 radix="+Integer.toString( ( digest[i] & 0xff ) + 0x100, 16));
        }
        return hexStr;
	}
	//appId in parameter is the string returned by getAppKey(String)
	public String getSecretKey(String appId) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();
        byte[] buffer = appId.getBytes("UTF-8");
        md.update(buffer);
        byte[] digest = md.digest();

        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
        	
            hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 32).substring( 1 );
            //System.out.println("byte="+digest[i]+" hexStr="+hexStr+"    |  "+( digest[i] & 0xff)+"   |  16 radix="+Integer.toString( ( digest[i] & 0xff ) + 0x100, 32));
        }
        return hexStr;
	}
}
